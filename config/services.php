<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    
    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'google' => [
		'client_id' => '424526320446-9sc2ousfg4ii7u0gqe2cgre6kfenpc01.apps.googleusercontent.com',
		'client_secret' => 'e9GVN2-6lG_IeXL2pAU6o0DY',
		'redirect' => 'https://version.pride-limited.com/login/google/callback',
	],
	
// 	 'google' => [
// 		'client_id' => '368187051828-vpgo48coc87l66aefc2032d3ogi8hmhr.apps.googleusercontent.com',
// 		'client_secret' => '2zLWPmkY9lmd6m-Ui30a_4rQ',
// 		'redirect' => 'https://pride-limited.com/auth/google/callback',
// 	],
	
	'facebook' => [
		'client_id' => '2420447694694195',
		'client_secret' => '6b6946818c1bd32f111952ca19b3da18',
		'redirect' => 'https://version.pride-limited.com/login/facebook/callback',
	],

];
