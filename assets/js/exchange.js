           function getCodeBoxElement(index) {
                return document.getElementById('codeBox' + index);
            }
            function onKeyUpEvent(index, event) {
                const eventCode = event.which || event.keyCode;
                if (getCodeBoxElement(index).value.length === 1) {
                    if (index !== 4) {
                        getCodeBoxElement(index + 1).focus();
                    } else {
                        getCodeBoxElement(index).blur();
                        var pin = document.getElementById('codeBox' + 1).value;
                        var pin2 = document.getElementById('codeBox' + 2).value;
                        var pin3 = document.getElementById('codeBox' + 3).value;
                        var pin4 = document.getElementById('codeBox' + 4).value;
                        var pin_collect = pin + pin2 + pin3 + pin4;
                        // check otp
                        var pin = getCodeBoxElement(index).value;
                        var number = document.getElementById("mobile-no").value;
                        if (window.XMLHttpRequest)
                        {
                            xmlhttp = new XMLHttpRequest();
                        } else
                        {
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange = function ()
                        {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                            {
                                document.getElementById("otpstatus").value = xmlhttp.responseText;
								document.getElementById('otp_show').style.display = "none";
								document.getElementById("otp_success").innerHTML = '<p style="padding:10px;color:green;">OTP varified.</p>';
                            }
                        }
                        xmlhttp.open("GET", base_url + "/checkOTP/" + number + '/' + pin_collect, true);
                        xmlhttp.send();
                        // check otp end
						document.getElementById('book_now-v').style.display = "none";
						document.getElementById('order_details').style.display = "block";
						
                        console.log('submit code');
                    }
                }
                if (eventCode === 8 && index !== 1) {
                    getCodeBoxElement(index - 1).focus();
                }
            }
            function onFocusEvent(index) {
                for (item = 1; item < index; item++) {
                    const currentElement = getCodeBoxElement(item);
                    if (!currentElement.value) {
                        currentElement.focus();
                        break;
                    }
                }
            }
            
            $(document).ready(function () {
				var product_id=$("#product_id_1").val();
				var order_id=$("#order_id_1").val();
				var field_no=1;
				var pre_color = $("#ColorId_" + order_id + "_"+ field_no +" option:selected").val();
		        var pre_size = $("#SizeId_"+ order_id + "_"+ field_no +" option:selected").val();
				getQtyByColorSize(product_id, pre_color, pre_size, order_id, field_no);
				
                $("#order_details").click(function (e) {
                    e.preventDefault();
					$("#loading-image").css("display", "block");
					$("#loading-image").show();
					$(".ng-scope").css("display", "block");
                    var otpcheck = $("#otpstatus").val();
                    if (otpcheck != '') {
                        if (otpcheck == 1) {
							$("#loading-image").css("display", "none");
							var trackingnumber = $("#tracking_number").val();
							var mobilenumber = $("#mobile-no").val();
							var url = base_url + '/exchnage/order-details';
								$.ajax({
									url: url,
									type: 'GET',
									data: {tracknumber: trackingnumber,mobile: mobilenumber},
									success: function (data) {
										if(data==1){
											$("#invalid-t-m").html('<p style="padding:10px;color:red;">Your tracking number & mobile number not match.</p>');
											$("#otp_show").css("display", "block");
											$("#otp_success").text("");
											$("#otpsms").text("");
											$('#mobile-no').val('');
											$('#tracking_number').val('');
											$('#codeBox1').val('');
											$('#codeBox2').val('');
											$('#codeBox3').val('');
											$('#codeBox4').val('');
										}else if(data==2){
											$("#invalid-t-m").html('<p style="padding:10px;color:red;">Your tracking number not found.</p>');
											$("#otp_show").css("display", "block");
											$("#otp_success").text("");
											$("#otpsms").text("");
											$('#mobile-no').val('');
											$('#tracking_number').val('');
											$('#codeBox1').val('');
											$('#codeBox2').val('');
											$('#codeBox3').val('');
											$('#codeBox4').val('');
										}else{
										  $(".ng-scope").css("display", "none");
										   $("#order_details_show").html(data);
										}
									}
								});
                            //alert('ok');
                        } else {
                            alert('OTP not matched.');
							$("#loading-image").css("display", "none");
                        }
                    } else {
                        alert('Need mobile varification.');
						$("#loading-image").css("display", "none");
                    }
                });
				
				$("#search_submit").click(function (e) {
					e.preventDefault();
					alert('ok');
				});

            });
		
	function HideDetails(conforder_id){
		$(".orderdetails_"+conforder_id).fadeOut();
		$("#show_button_"+conforder_id).show();
	    $("#hide_button_"+conforder_id).hide();
	}
	
	function exit(order_id,field_no){
		event.preventDefault();
		$("#content_edit_"+order_id+"_"+field_no).hide();
		$("#content_"+order_id+"_"+field_no).show();
		$("#buttons_"+order_id+"_"+field_no).show();
		$("#message_"+order_id+"_"+field_no).hide();
	}
	
	function exchangeexit(order_id,field_no){
		event.preventDefault();
		$("#content_edit_"+order_id+"_"+field_no).hide();
		$("#exchangecontent_"+order_id+"_"+field_no).hide();
		$("#content_"+order_id+"_"+field_no).show();
		$("#content_img_"+order_id+"_"+field_no).show();
		$("#buttons_"+order_id+"_"+field_no).show();
		$("#message_"+order_id+"_"+field_no).hide();
		$("#product_details_show").hide();
		$("#product-details").hide();
	}
	
	function exchange(order_id,product_id){
		event.preventDefault();
		$("#content_edit_"+order_id+"_"+product_id).show();
		$("#content_"+order_id+"_"+product_id).hide();
		$("#buttons_"+order_id+"_"+product_id).hide();
	}
	
	function Productcolor(product_id,order_id,field_no){
		//alert(product_id);
		var pre_color = $("#ColorId_" + order_id + "_"+ field_no +" option:selected").val();
		getSizeByColor(product_id, pre_color, order_id, field_no);
	}
	
	function ExchangeProductcolor(product_id,order_id,field_no){
		var pre_color = $("#exchangeColorId_" + order_id + "_"+ field_no +" option:selected").val();
		ExchangegetSizeByColor(product_id, pre_color, order_id, field_no);
		var pre_size = $("#exchangeSizeId_"+ order_id + "_"+ field_no +" option:selected").val();
		ExchangegetQtyByColorSize(product_id, pre_color, pre_size, order_id, field_no);
	}
	
	function getSizeByColor(product_id, color_name, order_id, field_no) {
	        $("#loading-image").show();
            var url_op = base_url + "/getSizeByColor/" + product_id + '/' + color_name;
            $.ajax({
                url: url_op,
                type: 'GET',
				dataType: 'json',
                data: '',
                success: function (data)
                {
                    $("#loading-image").hide();
                    $("#SizeId_" + order_id +"_"+field_no).empty();
                    $("#SizeId_" + order_id+"_"+field_no).append('<option value="">Select Size</option>');
                    $.each(data, function (index, subcatobj) {
                        $("#SizeId_" + order_id +"_"+field_no).append('<option value="' + subcatobj.productsize_size + '">' + subcatobj.productsize_size + '</option>');
                    });
                }
            });
        }
		
		function ExchangegetSizeByColor(product_id, color_name, order_id, field_no) {
		    $("#loading-image").show();
            var url_op = base_url + "/getSizeByColor/" + product_id + '/' + color_name;
            $.ajax({
                url: url_op,
                type: 'GET',
				dataType: 'json',
                data: '',
                success: function (data)
                {
                    $("#loading-image").hide();
                    $("#exchangeSizeId_" + order_id +"_"+field_no).empty();
                    //$("#exchangeSizeId_" + order_id+"_"+field_no).append('<option value="">Select Size</option>');
                    $.each(data, function (index, subcatobj) {
                        $("#exchangeSizeId_" + order_id +"_"+field_no).append('<option value="' + subcatobj.productsize_size + '">' + subcatobj.productsize_size + '</option>');
                    });
                }
            });
        }
	
	function Productsize(product_id,order_id,field_no){
		var pre_color = $("#ColorId_" + order_id + "_"+ field_no +" option:selected").val();
		var pre_size = $("#SizeId_"+ order_id + "_"+ field_no +" option:selected").val();
		getQtyByColorSize(product_id, pre_color, pre_size, order_id, field_no);
	}
	
	function ExchangeProductsize(product_id,order_id,field_no){
		var pre_color = $("#exchangeColorId_" + order_id + "_"+ field_no +" option:selected").val();
		var pre_size = $("#exchangeSizeId_"+ order_id + "_"+ field_no +" option:selected").val();
		ExchangegetQtyByColorSize(product_id, pre_color, pre_size, order_id, field_no);
	}
	
	function  getQtyByColorSize(productid, ProColor, ProSize, order_id, field_no) {
	        $("#loading-image").show();
            var url_op = base_url + "/ajaxcall-getQuantityByColor/" + productid + '/' + ProSize + '/' + ProColor;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (html) {
                    //console.log(html);
                    $("#loading-image").hide();
                    var qty = html;
                    if (qty >= 1) {
                        $('#QtyId_' + order_id+"_"+field_no).find('option').remove();
                        for (var v = 1; v <= qty; v++) {
                            $('#QtyId_' + order_id+"_"+field_no).append('<option value="' + v + '">' + v + '</option>');
							$("#update_"+order_id+"_"+field_no).show();
							$("#stockmessage_"+order_id+"_"+field_no).html("");
                        }
                    } else {
                        $('#QtyId_' + order_id+"_"+field_no).find('option').remove();
                        $('#QtyId_' + order_id+"_"+field_no).append('<option value="' + 0 + '">' + 0 + '</option>');
						$("#update_"+order_id+"_"+field_no).hide();
						$("#stockmessage_"+order_id+"_"+field_no).html("<span style='color:red;'>Stock not available.Select different size</span>");
                    }
                }
            });
        }
		
	function  ExchangegetQtyByColorSize(productid, ProColor, ProSize, order_id, field_no) {
	        $("#loading-image").show();
            var url_op = base_url + "/ajaxcall-getQuantityByColor/" + productid + '/' + ProSize + '/' + ProColor;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (html) {
                    $("#loading-image").hide();
                    var qty = html;
                    if (qty >= 1) {
						$("#exchangemessage_"+order_id+"_"+field_no).html("<span style='color:red;'></span>");
                        $('#exchangeQtyId_' + order_id+"_"+field_no).find('option').remove();
                        for (var v = 1; v <= qty; v++) {
                            $('#exchangeQtyId_' + order_id+"_"+field_no).append('<option value="' + v + '">' + v + '</option>');
							$("#exchangeupdate_"+order_id+"_"+field_no).show();
                        }
                    } else {
                        $('#exchangeQtyId_' + order_id+"_"+field_no).find('option').remove();
                        $('#exchangeQtyId_' + order_id+"_"+field_no).append('<option value="' + 0 + '">' + 0 + '</option>');
						$("#exchangeupdate_"+order_id+"_"+field_no).hide();
						$("#exchangemessage_"+order_id+"_"+field_no).html("<span style='color:red;'>Stock not available.</span>");
                    }
                }
            });
        }
		
	function Exchange(order_id,field_no){
		event.preventDefault();
		$("#loading-image").show();
		var datastring = $("#product_exchange_form_"+ order_id + "_"+ field_no).serialize();
		//alert(datastring);
		var url = base_url + "/user/order/exchange/save";
		$.ajax({
				url:url,
				type:'POST',
				data:datastring,
				success:function(result){
					//alert(result);
					 //document.getElementById("loading-image").outerHTML = '<div id="loading-image-removed"></div>';
					 //$("#content_edit_"+order_id+"_"+field_no).hide();
		             $("#content_"+order_id+"_"+field_no).show();
					 $("#message_"+order_id+"_"+field_no).show();
					 $("#buttons_"+order_id+"_"+field_no).show();
					 $("#message_"+order_id+"_"+field_no).html("");
					 $("#request_"+order_id+"_"+field_no).show();
					 $("#exchange_"+order_id+"_"+field_no).hide();
					 $("#cancel_"+order_id+"_"+field_no).hide();
					 $("#loading-image").hide();
					if(result =='success'){
					    $("#content_edit_"+order_id+"_"+field_no).hide();
					    $("#message_"+order_id+"_"+field_no).html("<span style='color:green;'>Exchange request success.</span>");
					    $("#request_"+order_id+"_"+field_no).html("<span class='blink_msg'>Already exchange requested.</span>");
					}else if(result =='exit'){
					    $("#content_edit_"+order_id+"_"+field_no).hide();
					  $("#message_"+order_id+"_"+field_no).html("<span style='color:red;'>Already requested.</span>");
					}else{
					    
					  $("#message_"+order_id+"_"+field_no).html("<span style='color:red;'>Failed.</span>");
					}
				}
            });
		
	}
	
	function ExchangeDiff(order_id,field_no){
		event.preventDefault();
		$("#loading-image").show();
		var datastring = $("#different_product_exchange_form_"+ order_id + "_"+ field_no).serialize();
		//alert(datastring);
		var url = base_url + "/user/order/exchange/save";
		$.ajax({
				url:url,
				type:'POST',
				data:datastring,
				success:function(result){
					//alert(result);
					 //document.getElementById("loading-image").outerHTML = '<div id="loading-image-removed"></div>';
		             $("#content_img_"+order_id+"_"+field_no).show();
					 $("#content_edit_"+order_id+"_"+field_no).show();
					 $("#content_"+order_id+"_"+field_no).show();
					 //$("#exchangecontent_"+order_id+"_"+field_no).hide();
					 $("#content_edit_"+order_id+"_"+field_no).hide();
					 $("#request_"+order_id+"_"+field_no).show();
					 $("#message_"+order_id+"_"+field_no).html("");
					 $("#buttons_"+order_id+"_"+field_no).show();
					 $("#exchange_"+order_id+"_"+field_no).hide();
					 $("#cancel_"+order_id+"_"+field_no).hide();
					 $("#loading-image").hide();
					if(result =='exit'){
					    console.log('exit');
					    $("#exchangecontent_"+order_id+"_"+field_no).show();
						$("#exchangemessage_"+order_id+"_"+field_no).html("<span style='color:red;'>Already requested.</span>");
					}else if(result !=''){
					     console.log('ghgfhfgh');
					   // $("#product_details_show").hide();
						$("#product-details").show();
						$("#content_edit_"+order_id+"_"+field_no).show();
						$("#exchangecontent_"+order_id+"_"+field_no).show();
						$("#searchproduct_"+order_id+"_"+field_no).hide();
					    $("#message_"+order_id+"_"+field_no).html("<span style='color:green;'>Exchange request success.</span>");
					    $("#request_"+order_id+"_"+field_no).html("<span class='blink_msg'>Already exchange requested.</span>");
					    $("#exchange-print").html(result).fadeIn(100);
						$('.close-modal').on('click', function (e) {
						   $("#exchange-print").hide();
					    });
					    window.location.href = base_url+"/user/order/exchange?id="+order_id+"&success=success";
					    //window.location.replace("https://urban-truth.com/exchnage/order-details-v2?order_no="+order_no+"&mobile_no="+mobile_no);
					}else{
					   console.log('43535');
					  $("#exchangemessage_"+order_id+"_"+field_no).html("<span style='color:red;'>Failed.</span>");
					}
				}
            });
		
	}
	
	function Exchangeproduct(order_id,shoppingproduct_id,field_no){
	    event.preventDefault();
		$("#content_edit_"+order_id+"_"+field_no).hide();
		$("#content_"+order_id+"_"+field_no).show();
		//$("#content_img_"+order_id+"_"+field_no).hide();
		$("#searchproduct_"+order_id+"_"+field_no).show();
	//	alert(shoppingproduct_id);
	}
	
	function Searchproduct(order_id,shoppingproduct_id,field_no){
		var key = $("#stylecode_" + order_id + "_"+ field_no).val();
		//alert(key);
		$("#product_details_show").show();
		var search_url = base_url + "/search/exchange/product";
		$.ajax({
			url: search_url,
			type: 'GET',
			data: {key: key, orderid: order_id, shoppingproduct_id: shoppingproduct_id},
			success: function (result)
			{
				$(".ng-scope").css("display", "none");
				$("#product_details_show").html(result);
				var product_id = $("#exchangeproduct_id_" + field_no).val();
				var pre_color = $("#exchangeColorId_" + order_id + "_"+ field_no).val();
				var pre_size = $("#exchangeSizeId_"+ order_id + "_"+ field_no).val();
				ExchangegetSizeByColor(product_id, pre_color, order_id, field_no);
				ExchangegetQtyByColorSize(product_id, pre_color, pre_size, order_id, field_no);
								
			}
		});
	}
	
	function CancelProduct(shoppingproduct_id,order_id,field_no){
		event.preventDefault();
		var x = confirm("Are you sure you want to cancel this order?");
		 if (x){
		   //alert(shoppingproduct_id);
			var url_op = base_url + "/cancelproduct/" + shoppingproduct_id;
			$.ajax({
				url: url_op,
				type: 'GET',
				dataType: 'json',
				data: '',
				success: function (result)
				{
					if(result==1){
						$("#content_"+ order_id +"_"+ field_no).hide();
						$("#content_img_"+ order_id +"_"+ field_no).hide();
						$("#message_"+order_id+"_"+field_no).html("<span style='color:green;'>Cancel request success.</span>");
					}else{
						$("#message_"+order_id+"_"+field_no).html("<span style='color:red;'>Failed.</span>");
					}
				}
			});
		 }else{
			 return false;
		 }
	}
	function CancelOrder(conforder_id){
		event.preventDefault();
		var x = confirm("Are you sure you want to cancel this order?");
		 if (x){
			var url_op = base_url + "/cancel-order/" + conforder_id;
			$.ajax({
				url: url_op,
				type: 'GET',
				success: function (html) {
				   location.reload();
				}
			});
		 }else{
			 return false;
		 }
	}