var accessToken='';
    $(document).ready(function(){
		$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $.ajax({
            url: base_url + "/bKash-token-new",
            type: 'POST',
            contentType: 'application/json',
            success: function (data) {
                console.log('got data from token  ..');
				console.log(JSON.stringify(data));
                accessToken=JSON.stringify(data);
            },
			error: function(){
				console.log('error');
                        
            }
        });

        var paymentConfig={
            createCheckoutURL:base_url + "/bkash-create",
            executeCheckoutURL:base_url + "/bKash-execute-new",
        };

        var paymentRequest;
		var amount=$("#amount").val();
		var invoice=$("#invoice").val();
        paymentRequest = { 
			amount:amount,
			invoice_id:invoice,
			intent:'sale'
		  };
		console.log(JSON.stringify(paymentRequest));
        bKash.init({
            paymentMode: 'checkout',
            paymentRequest: paymentRequest,
            createRequest: function(request){
                console.log('=> createRequest (request) :: ');
                console.log(request);
                
                $.ajax({
                    url: paymentConfig.createCheckoutURL+"?invoice_id="+paymentRequest.invoice_id,
                    type:'GET',
                    contentType: 'application/json',
                    success: function(data) {
                        console.log('got data from create  ..');
                        console.log('data ::=>');
                        console.log(JSON.stringify(data));
                        
                        var obj = JSON.parse(data);
                        
                        if(data && obj.paymentID != null){
                            paymentID = obj.paymentID;
                            bKash.create().onSuccess(obj);
                        }
                        else {
							console.log('error');
                            bKash.create().onError();
                        }
                    },
                    error: function(){
						console.log('error');
                        bKash.create().onError();
                    }
                });
            },
            
            executeRequestOnAuthorization: function(){
                console.log('=> executeRequestOnAuthorization');
                $.ajax({
                    url: paymentConfig.executeCheckoutURL+"?paymentID="+paymentID,
                    type: 'GET',
                    contentType:'application/json',
                    success: function(data){
                        //console.log(JSON.stringify(data));
                        data = JSON.parse(data);
                        if(data && data.paymentID != null){
                            //alert('[SUCCESS] data : ' + JSON.stringify(data));
                            window.location.href  = base_url + "/payment-success?paymentID=" + data.paymentID; 							
                        }else if (data && data.errorCode == 2001) {
							bKash.execute().onError();
							alert('Invalid App Key');
						} else if (data && data.errorCode == 2002) {
							bKash.execute().onError();
							alert('Invalid Payment ID');
						} else if (data && data.errorCode == 2003) {
							bKash.execute().onError();
							alert('Process Failed.');
						} else if (data && data.errorCode == 2004) {
							bKash.execute().onError();
							alert('Invalid firstPaymentDate.');
						} else if (data && data.errorCode == 2005) {
							bKash.execute().onError();
							alert('Invalid frequency.');
						} else if (data && data.errorCode == 2006) {
							bKash.execute().onError();
							alert('Invalid amount.');
						} else if (data && data.errorCode == 2007) {
							bKash.execute().onError();
							alert('Invalid currency.');
						} else if (data && data.errorCode == 2008) {
							bKash.execute().onError();
							alert('Invalid intent.');
						} else if (data && data.errorCode == 2009) {
							bKash.execute().onError();
							alert('Invalid Wallet.');
						} else if (data && data.errorCode == 2010) {
							bKash.execute().onError();
							alert('Invalid OTP.');
						} else if (data && data.errorCode == 2011) {
							bKash.execute().onError();
							alert('Invalid PIN');
						} else if (data && data.errorCode == 2012) {
							bKash.execute().onError();
							alert('Invalid Receiver MSISDN');
						} else if (data && data.errorCode == 2013) {
							bKash.execute().onError();
							alert('Resend Limit Exceeded.');
						} else if (data && data.errorCode == 2014) {
							bKash.execute().onError();
							alert('Wrong PIN');
						} else if (data && data.errorCode == 2015) {
							bKash.execute().onError();
							alert('Wrong PIN count exceeded.');
						} else if (data && data.errorCode == 2016) {
							bKash.execute().onError();
							alert('Wrong verification code.');
						} else if (data && data.errorCode == 2017) {
							bKash.execute().onError();
							alert('Wrong verification limit exceeded.');
						} else if (data && data.errorCode == 2018) {
							bKash.execute().onError();
							alert('	OTP verification time expired.');
						} else if (data && data.errorCode == 2019) {
							bKash.execute().onError();
							alert('PIN verification time expired.');
						} else if (data && data.errorCode == 2020) {
							bKash.execute().onError();
							alert('Exception Occurred.');
						} else if (data && data.errorCode == 2021) {
							bKash.execute().onError();
							alert('Invalid Mandate ID.');
						} else if (data && data.errorCode == 2022) {
							bKash.execute().onError();
							alert('The mandate does not exist.');
						} else if (data && data.errorCode == 2023) {
							bKash.execute().onError();
							alert('Insufficient Balance.');
						} else if (data && data.errorCode == 2024) {
							bKash.execute().onError();
							alert('Exception occurred.');
						} else if (data && data.errorCode == 2025) {
							bKash.execute().onError();
							alert('Invalid request body.');
						} else if (data && data.errorCode == 2026) {
							bKash.execute().onError();
							alert('The reversal amount cannot be greater than the original transaction amount.');
						}else if (data && data.errorCode == 2027) {
							bKash.execute().onError();
							alert('The mandate corresponding to the payer reference number already exists and cannot be created again.');
						}else if (data && data.errorCode == 2028) {
							bKash.execute().onError();
							alert('Reverse failed because the transaction serial number does not exist.');
						} else if (data && data.errorCode == 2029) {
							bKash.execute().onError();
							alert('Duplicate for all transactions.');
						} else if (data && data.errorCode == 2030) {
							bKash.execute().onError();
							alert('Invalid mandate request type.');
						} else if (data && data.errorCode == 2031) {
							bKash.execute().onError();
							alert('Invalid merchant invoice number.');
						} else if (data && data.errorCode == 2032) {
							bKash.execute().onError();
							alert('Invalid transfer type.');
						} else if (data && data.errorCode == 2033) {
							bKash.execute().onError();
							alert('Transaction not found.');
						}else if (data && data.errorCode == 2034) {
							bKash.execute().onError();
							alert('The transaction cannot be reversed because the original transaction has been reversed.');
						}else if (data && data.errorCode == 2035) {
							bKash.execute().onError();
							alert('Reverse failed because the initiator has no permission to reverse the transaction.');
						}else if (data && data.errorCode == 2036) {
							bKash.execute().onError();
							alert('The direct debit mandate is not in Active state.');
						}else if (data && data.errorCode == 2037) {
							bKash.execute().onError();
							alert('The account of the debit party is in a state which prohibits execution of this transaction.');
						}else if (data && data.errorCode == 2038) {
							bKash.execute().onError();
							alert('Debit party identity tag prohibits execution of this transaction.');
						}else if (data && data.errorCode == 2039) {
							bKash.execute().onError();
							alert('The account of the credit party is in a state which prohibits execution of this transaction.');
						}else if (data && data.errorCode == 2040) {
							bKash.execute().onError();
							alert('Credit party identity tag prohibits execution of this transaction.');
						}else if (data && data.errorCode == 2041) {
							bKash.execute().onError();
							alert('Credit party identity is in a state which does not support the current service.');
						}else if (data && data.errorCode == 2042) {
							bKash.execute().onError();
							alert('Reverse failed because the initiator has no permission to reverse the transaction.');
						}else if (data && data.errorCode == 2043) {
							bKash.execute().onError();
							alert('The security credential of the subscriber is incorrect.');
						} else if (data && data.errorCode == 2044) {
							bKash.execute().onError();
							alert('Invalid Payer Reference.');
						} else if (data && data.errorCode == 2045) {
							bKash.execute().onError();
							alert('Invalid Merchant Callback URL.');
						} else if (data && data.errorCode == 2046) {
							bKash.execute().onError();
							alert('Agreement already exists between payer and merchant.');
						} else if (data && data.errorCode == 2047) {
							bKash.execute().onError();
							alert('Invalid Agreement ID.');
						}else if (data && data.errorCode == 2048) {
							bKash.execute().onError();
							alert('Invalid Payer Reference.');
						} else if (data && data.errorCode == 2049) {
							bKash.execute().onError();
							alert('Agreement is in incomplete state.');
						} else if (data && data.errorCode == 2050) {
							bKash.execute().onError();
							alert('Agreement has already been cancelled.');
						} else if (data && data.errorCode == 2051) {
							bKash.execute().onError();
							alert("Agreement execution pre-requisite hasn't been met.");
						} else if (data && data.errorCode == 2052) {
							bKash.execute().onError();
							alert('Invalid Agreement State.');
						} else if (data && data.errorCode == 2053) {
							bKash.execute().onError();
							alert('Invalid Payment State.');
						} else if (data && data.errorCode == 2054) {
							bKash.execute().onError();
							alert("Payment execution pre-requisite hasn't been met.");
						} else if (data && data.errorCode == 2055) {
							bKash.execute().onError();
							alert('This action can only be performed by the agreement or payment initiator party.');
						} else if (data && data.errorCode == 2056) {
							bKash.execute().onError();
							alert('The payment has already been completed.');
						} else if (data && data.errorCode == 2057) {
							bKash.execute().onError();
							alert('Not a bKash Wallet.');
						} else if (data && data.errorCode == 2058) {
							bKash.execute().onError();
							alert('Not a Customer Wallet.');
						} else if (data && data.errorCode == 2059) {
							bKash.execute().onError();
							alert('Multiple OTP request for a single session denied.');
						}else if (data && data.errorCode == 2060) {
							bKash.execute().onError();
							alert("Payment execution pre-requisite hasn't been met.");
						}else if (data && data.errorCode == 2061) {
							bKash.execute().onError();
							alert('This action can only be performed by the agreement or payment initiator party.');
						}else if (data && data.errorCode == 2062) {
							bKash.execute().onError();
							alert('The payment has already been completed.');
						} else if (data && data.errorCode == 2063) {
							bKash.execute().onError();
							alert('Mode is not valid as per request data.');
						} else if (data && data.errorCode == 2064) {
							bKash.execute().onError();
							alert('This product mode currently unavailable.');
						} else if (data && data.errorCode == 2065) {
							bKash.execute().onError();
							alert('Mendatory field missing.');
						} else if (data && data.errorCode == 2066) {
							bKash.execute().onError();
							alert('Agreement is not shared with other merchant.');
						} else if (data && data.errorCode == 2067) {
							bKash.execute().onError();
							alert('Invalid permission.');
						} else if (data && data.errorCode == 2068) {
							bKash.execute().onError();
							alert('Transaction has already been completed.');
						} else if (data && data.errorCode == 2069) {
							bKash.execute().onError();
							alert('Transaction has already been cancelled.');
						} else if (data && data.errorCode == 503) {
							bKash.execute().onError();
							alert('System is undergoing maintenance. Please try again later.');
						}
                        else {
                            bKash.execute().onError();
                        }
                    },
                    error: function(){
                        bKash.execute().onError();
                    }
                });
            },
			onClose : function () {
              //alert('Sure to cancel.');
			}
        });
        
		console.log("Right after init ");
    
        
    });
	
	function callReconfigure(val){
        bKash.reconfigure(val);
    }

    function clickPayButton(){
        $("#bKash_button").trigger('click');
    }
