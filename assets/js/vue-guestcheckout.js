var vuejs = new Vue({
    el: '#order-preview-page',
    data: {
        cart_items: cart_items,
        offer: offer,
        hour: hour_now,
        item_number: item_number,
        city_list: 0,
        city_id_outside_dhaka: [7, 24, 26, 28, 32, 42, 45, 50, 51, 58, 59, 60, 65, 69, 73, 78, 82, 85, 3, 4, 5, 6, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158],
        delivery_method: "cDelivery",
        outside_dhaka: false,
        delivery_type: 0,
        // shipping_charges: [70, 130, 150, 85],
        //covid19
        shipping_charges: [100, 130, 150, 85],
        tax_percent: 0,
        error_city: '',
        bKash_selected: false,
        iPay_selected: false,
        ssl_selected: false,
        promo_code: null,
        promo_discount: 0,
        loading_image: false,
        error_privacy: '',
        error_exchange: '',
		error_firstname: '',
		error_lastname: '',
		error_mobile: '',
		error_address: '',
		error_g_city: '',
        bKash_paymentConfig: {createCheckoutURL: base_url + "/guest-order", executeCheckoutURL: base_url + "/bKash-execute", },
        bKash_paymentRequest: {intent: 'sale'},
    },
    methods: {
        changeCity: function (event) {
            this.city_list = 0;
            var RegionId = event.target.value;
            this.outside_dhaka = event.target.value == 1 ? false : true;
            this.delivery_type = event.target.value == 1 ? 0 : 2;
            if (this.outside_dhaka && this.delivery_method == "cDelivery") {
                document.getElementById('bKash').click();
            }

            jQuery('#regionId').val(RegionId);
            var url_op = base_url + "/citylist/" + RegionId;
            jQuery.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    jQuery('#CityList').empty();
                    jQuery('#CityList').append('<option value="">--- Select Area ---</option>');
                    jQuery.each(data, function (index, cityobj) {
                        jQuery('#CityList').append('<option value="' + cityobj.CityId + '">' + cityobj.CityName + '</option>');
                    });
                }
            });
        },
        changeArea: function (event) {
            this.outside_dhaka = this.city_id_outside_dhaka.includes(parseInt(event.target.value));
            this.delivery_type = this.city_id_outside_dhaka.includes(parseInt(event.target.value)) ? 2 : 0;
            if (this.outside_dhaka && this.delivery_method == "cDelivery") {
                document.getElementById('bKash').click();
            }
            // var AreaId = event.target.value;
            //this.outside_dhaka = event.target.value == 24 ? true : false;
            //this.delivery_type = event.target.value == 24 ? 0 : 2;
            // alert(AreaId);
        },
        setPopupIndex: {
            set: function (index) {
                this.popup_index = index;
            }
        },
        setSelectedIndex: {
            set: function (index) {
                this.selected_index = index;
            }
        },
        storeToTemp: function () {
            this.temp = Object.assign({}, this.addresses[this.popup_index]);
            /*Object.assign(this.temp, this.addresses[this.popup_index]);*/
        },
        getFromTemp: function () {
            Object.assign(this.addresses[this.popup_index], this.temp);

        },
        checkForm: function (e) {
            var exchange = document.getElementById("terms_exchange").checked;
            var privacy = document.getElementById("terms_privacy").checked;
            if (!document.getElementById("CityList").value) {
                document.getElementById("CityList").focus();
                this.error_city = "Please select City and Region";
                e.preventDefault();
            } else if (exchange == false) {
                this.error_exchange = "Please accept Exchange Policy";
                e.preventDefault();
            } else if (privacy == false) {
                this.error_privacy = "Please agree Privacy Policy";
                e.preventDefault();
            } else {
                document.body.style.width = "100vw";
                document.body.style.height = "100vh";
                document.body.style.overflow = "hidden";
                this.loading_image = true;
            }
        },
        setForPayment: function (e) {
            if (!document.getElementById("CityList").value) {
                document.getElementById("CityList").focus();
                this.error_city = "Please select City and Area";
            } else if (!document.getElementsByName("terms_exchange")[0].checked) {
                document.getElementById("terms_exchange").innerHTML = 'You have to accept our Exchange Policy';
            } else if (!document.getElementsByName("terms_privacy")[0].checked) {
                document.getElementById("terms_exchange").innerHTML = '';
                document.getElementById("terms_privacy").innerHTML = 'You have to agree to our Privacy Policy';
            }else if(!document.getElementById("firstname").value){
				document.getElementById("firstname").focus();
				this.error_firstname='Please enter your first name.';
				//alert('required');
			}else if(!document.getElementById("lastname").value){
				document.getElementById("lastname").focus();
				this.error_lastname='Please enter your last name.';
			}else if(!document.getElementById("mobile").value){
				document.getElementById("mobile").focus();
				this.error_mobile='Please enter your mobile number.';
			}else if(!document.getElementById("address").value){
				document.getElementById("address").focus();
				this.error_address='Please enter your address.';
			}else if(!document.getElementById("city").value){
				document.getElementById("city").focus();
				this.error_g_city='Enter your city.';
			} else {
                var form = document.querySelector('#order-form');
                var formData = new FormData(form);
                var object = {};
                object.product_id = [];
                object.product_name = [];
                object.product_barcode = [];
                object.image_link = [];
                object.productalbum_name = [];
                object.product_size = [];
                object.shoppinproduct_quantity = [];
                object.product_price = [];
                formData.forEach(function (value, key) {
                    if (key.includes('product_id'))
                        object.product_id.push(value);
                    else if (key.includes('product_name'))
                        object.product_name.push(value);
                    else if (key.includes('product_barcode'))
                        object.product_barcode.push(value);
                    else if (key.includes('image_link'))
                        object.image_link.push(value);
                    else if (key.includes('productalbum_name'))
                        object.productalbum_name.push(value);
                    else if (key.includes('product_size'))
                        object.product_size.push(value);
                    else if (key.includes('shoppinproduct_quantity'))
                        object.shoppinproduct_quantity.push(value);
                    else if (key.includes('product_price'))
                        object.product_price.push(value);
                    else
                        object[key] = value;
                });
                if (this.delivery_method == 'bKash') {
                    element = document.getElementById('bKash_button');
                    this.bKash_paymentRequest.amount = object.shoppingcart_total;
                    this.bKash_paymentRequest = object;
                    this.bKash_paymentRequest.amount = object.shoppingcart_total;
                } else if (this.delivery_method == 'ssl') {
                    element = document.getElementById('sslczPayBtn');
                    element.postdata = object;
                }
                element.click();
            }
        },
        promoCodeCheck: function () {
            if (!this.promo_code) {
                document.getElementById("promo-code").focus();
                e.preventDefault();
            }
            var url_op = base_url + "/promo-code-check/" + this.promo_code;
            jQuery.ajax({
                url: url_op,
                type: 'GET',
                success: function (data = false) {
                    if (data == 0) {
                        percentage = 10;
                        vuejs.promo_discount = vuejs.subTotal * percentage / 100;
                        jQuery(".messages").html("Congratulations ! You have 10% off your first order.");
                        alert("Congratulations ! You have 10% off your first order.");
                    } else if (data == 1) {
                        alert("This promocode only for first purchase cutomer: " + vuejs.promo_code);
                        jQuery("#promo_check").html("This promocode only for first purchase cutomer: " + vuejs.promo_code);
                    } else if (data == 2) {
                        alert("Invalid Promo Code: " + vuejs.promo_code);
                        jQuery("#promo_check").html("Invalid Promo Code: " + vuejs.promo_code);
                    } else if (data == 3) {
                        alert("This Promo Code has already been used: " + vuejs.promo_code);
                        jQuery("#promo_check").html("This Promo Code already used : " + vuejs.promo_code);
                }
                }
            });
        },
        bKashToken: function () {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: base_url + "/bKash-token",
                type: 'POST',
                contentType: 'application/json',
                success: function (data) {
                },
                error: function () {
                }
            });
        },
        bKashCreate: function (request) {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: vuejs.bKash_paymentConfig.createCheckoutURL,
                type: 'POST',
                data: JSON.stringify(this.bKash_paymentRequest),
                contentType: 'application/json',
                success: function (data) {
                    var obj = JSON.parse(data);
                    console.log(data);
                    if (data && obj.paymentID != null) {
                        paymentID = obj.paymentID;
                        bKash.create().onSuccess(obj);
                    } else if (data && data.errorCode == 2001) {
                        alert('Invalid App Key');
                    }else if(data == 2073){
						bKash.execute().onError();
						alert('Amount is not validated.');
					}else if(data == 2074){
						bKash.execute().onError();
						alert('Shipping charge is not validated.');
					} else {
                        bKash.create().onError();
                    }
                },
                error: function () {
                    bKash.create().onError();
                }
            });
        },
        bKashExecute: function () {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: vuejs.bKash_paymentConfig.executeCheckoutURL + '?paymentID=' + paymentID,
                type: 'GET',
                contentType: 'application/json',
                success: function (data) {
                    var data = JSON.parse(data);
                    if (data && data.paymentID != null) {

                        //save order data to the server
                        window.location = base_url + "/guest-bkash-success?paymentID=" + data.paymentID;
                    } else if (data && data.errorCode == 2001) {
                        bKash.execute().onError();
                        alert('Invalid App Key');
                    } else if (data && data.errorCode == 2002) {
                        bKash.execute().onError();
                        alert('Invalid Payment ID');
                    } else if (data && data.errorCode == 2003) {
                        bKash.execute().onError();
                        alert('Process Failed.');
                    } else if (data && data.errorCode == 2004) {
                        bKash.execute().onError();
                        alert('Invalid firstPaymentDate.');
                    } else if (data && data.errorCode == 2005) {
                        bKash.execute().onError();
                        alert('Invalid frequency.');
                    } else if (data && data.errorCode == 2006) {
                        bKash.execute().onError();
                        alert('Invalid amount.');
                    } else if (data && data.errorCode == 2007) {
                        bKash.execute().onError();
                        alert('Invalid currency.');
                    } else if (data && data.errorCode == 2008) {
                        bKash.execute().onError();
                        alert('Invalid intent.');
                    } else if (data && data.errorCode == 2009) {
                        bKash.execute().onError();
                        alert('Invalid Wallet.');
                    } else if (data && data.errorCode == 2010) {
                        bKash.execute().onError();
                        alert('Invalid OTP.');
                    } else if (data && data.errorCode == 2011) {
                        bKash.execute().onError();
                        alert('Invalid PIN');
                    } else if (data && data.errorCode == 2012) {
                        bKash.execute().onError();
                        alert('Invalid Receiver MSISDN');
                    } else if (data && data.errorCode == 2013) {
                        bKash.execute().onError();
                        alert('Resend Limit Exceeded.');
                    } else if (data && data.errorCode == 2014) {
                        bKash.execute().onError();
                        alert('Wrong PIN');
                    } else if (data && data.errorCode == 2015) {
                        bKash.execute().onError();
                        alert('Wrong PIN count exceeded.');
                    } else if (data && data.errorCode == 2016) {
                        bKash.execute().onError();
                        alert('Wrong verification code.');
                    } else if (data && data.errorCode == 2017) {
                        bKash.execute().onError();
                        alert('Wrong verification limit exceeded.');
                    } else if (data && data.errorCode == 2018) {
                        bKash.execute().onError();
                        alert('	OTP verification time expired.');
                    } else if (data && data.errorCode == 2019) {
                        bKash.execute().onError();
                        alert('PIN verification time expired.');
                    } else if (data && data.errorCode == 2020) {
                        bKash.execute().onError();
                        alert('Exception Occurred.');
                    } else if (data && data.errorCode == 2021) {
                        bKash.execute().onError();
                        alert('Invalid Mandate ID.');
                    } else if (data && data.errorCode == 2022) {
                        bKash.execute().onError();
                        alert('The mandate does not exist.');
                    } else if (data && data.errorCode == 2023) {
                        bKash.execute().onError();
                        alert('Insufficient Balance.');
                    } else if (data && data.errorCode == 2024) {
                        bKash.execute().onError();
                        alert('Exception occurred.');
                    } else if (data && data.errorCode == 2025) {
                        bKash.execute().onError();
                        alert('Invalid request body.');
                    } else if (data && data.errorCode == 2026) {
                        bKash.execute().onError();
                        alert('The reversal amount cannot be greater than the original transaction amount.');
                    } else if (data && data.errorCode == 2029) {
                        bKash.execute().onError();
                        alert('Duplicate for all transactions.');
                    } else if (data && data.errorCode == 2030) {
                        bKash.execute().onError();
                        alert('Invalid mandate request type.');
                    } else if (data && data.errorCode == 2031) {
                        bKash.execute().onError();
                        alert('Invalid merchant invoice number.');
                    } else if (data && data.errorCode == 2032) {
                        bKash.execute().onError();
                        alert('Invalid transfer type.');
                    } else if (data && data.errorCode == 2033) {
                        bKash.execute().onError();
                        alert('Transaction not found.');
                    } else if (data && data.errorCode == 2044) {
                        bKash.execute().onError();
                        alert('Invalid Payer Reference.');
                    } else if (data && data.errorCode == 2045) {
                        bKash.execute().onError();
                        alert('Invalid Merchant Callback URL.');
                    } else if (data && data.errorCode == 2046) {
                        bKash.execute().onError();
                        alert('Agreement already exists between payer and merchant.');
                    } else if (data && data.errorCode == 2047) {
                        bKash.execute().onError();
                        alert('Invalid Agreement ID.');
                    } else if (data && data.errorCode == 2049) {
                        bKash.execute().onError();
                        alert('Agreement is in incomplete state.');
                    } else if (data && data.errorCode == 2050) {
                        bKash.execute().onError();
                        alert('Agreement has already been cancelled.');
                    } else if (data && data.errorCode == 2051) {
                        bKash.execute().onError();
                        alert("Agreement execution pre-requisite hasn't been met.");
                    } else if (data && data.errorCode == 2052) {
                        bKash.execute().onError();
                        alert('Invalid Agreement State.');
                    } else if (data && data.errorCode == 2053) {
                        bKash.execute().onError();
                        alert('Invalid Payment State.');
                    } else if (data && data.errorCode == 2054) {
                        bKash.execute().onError();
                        alert("Payment execution pre-requisite hasn't been met.");
                    } else if (data && data.errorCode == 2055) {
                        bKash.execute().onError();
                        alert('This action can only be performed by the agreement or payment initiator party.');
                    } else if (data && data.errorCode == 2056) {
                        bKash.execute().onError();
                        alert('The payment has already been completed.');
                    } else if (data && data.errorCode == 2057) {
                        bKash.execute().onError();
                        alert('Not a bKash Wallet.');
                    } else if (data && data.errorCode == 2058) {
                        bKash.execute().onError();
                        alert('Not a Customer Wallet.');
                    } else if (data && data.errorCode == 2059) {
                        bKash.execute().onError();
                        alert('Multiple OTP request for a single session denied.');
                    } else if (data && data.errorCode == 2063) {
                        bKash.execute().onError();
                        alert('Mode is not valid as per request data.');
                    } else if (data && data.errorCode == 2064) {
                        bKash.execute().onError();
                        alert('This product mode currently unavailable.');
                    } else if (data && data.errorCode == 2065) {
                        bKash.execute().onError();
                        alert('Mendatory field missing.');
                    } else if (data && data.errorCode == 2066) {
                        bKash.execute().onError();
                        alert('Agreement is not shared with other merchant.');
                    } else if (data && data.errorCode == 2067) {
                        bKash.execute().onError();
                        alert('Invalid permission.');
                    } else if (data && data.errorCode == 2068) {
                        bKash.execute().onError();
                        alert('Transaction has already been completed.');
                    } else if (data && data.errorCode == 2069) {
                        bKash.execute().onError();
                        alert('Transaction has already been cancelled.');
                    } else if (data && data.errorCode == 503) {
                        bKash.execute().onError();
                        alert('System is undergoing maintenance. Please try again later.');
                    } else {
                        bKash.execute().onError();
                    }
                },
                error: function () {
                    bKash.execute().onError();
                }
            });
        },
    },
    computed: {
        getShippingCharge: function () {
            if (this.subTotal >= 3000) {
                var charge = this.shipping_charges[this.delivery_type];
                if (charge == 100 || charge == 150) {
                    return 0;
                } else {
                    return charge;
                }
            } else {
                var charge = this.shipping_charges[this.delivery_type];
                return charge;
            }
            /*  if (this.subTotal >= 3000)
             return 0;
             return this.shipping_charges[this.delivery_type];
             */
        },
        subTotal: {
            get: function () {
                var subTotal = 0;
                for (var key in this.cart_items) {
                    if (!this.cart_items.hasOwnProperty(key))
                        continue;
                    subTotal += cart_items[key].price * cart_items[key].qty;
                }
                return subTotal;
            }
        },
        amountOfOffer: {
            get: function () {
                var total = 0;
                total = this.subTotal * 10 / 100;
                return total;
            }
        },
        amountWithOffer: {
            get: function () {
                if (this.offer == 0) {
                    var total = 0;
                    total = this.subTotal - this.subTotal * 10 / 100;
                } else {
                    total = this.subTotal;
                }
                return total;
            }
        },
        totalBeforeTax: {
            get: function () {
                var total = 0;
                total = this.amountWithOffer;
                return total;
            }
        },
        amountOfTax: {
            get: function () {
                var total = 0;
                total = this.totalBeforeTax * this.tax_percent / 100;
                return total;
            }
        },
        total: {
            get: function () {
                var total = 0;
                total = this.totalBeforeTax + this.amountOfTax + this.getShippingCharge;
                return total;
            }
        },
    },
    beforeMount() {
        this.selectedAddress;
        this.bKashToken();
        this.bKashInit();
    }
});