// Your web app's Firebase configuration
      var firebaseConfig = {
        apiKey: "AIzaSyC8XKnYXEe3AWomcPaJrnz5rrcmXIk4cXE",
        authDomain: "web-push-c50bf.firebaseapp.com",
        databaseURL: "https://web-push-c50bf.firebaseio.com",
        projectId: "web-push-c50bf",
        storageBucket: "web-push-c50bf.appspot.com",
        messagingSenderId: "703044831753",
        appId: "1:703044831753:web:bc861985c3b2df991486d8"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
      // Retrieve Firebase Messaging object.
     const messaging = firebase.messaging();
     // Add the public key generated from the console here.
     messaging.usePublicVapidKey("BGxXeSz6fHMAHnv4fgnCGdFDBzNceOqzq0816K10lvEm0o3pP6A9scKO6RoDDF5M0od8SACJ66XavnAGIND-8YM");
     const tokenDivId = 'token_div';
    const permissionDivId = 'permission_div';
     Notification.requestPermission().then((permission) => {
      if (permission === 'granted') {
        console.log('Notification permission granted.');
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // ...
      } else {
        console.log('Unable to get permission to notify.');
      }
     });
    
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then((currentToken) => {
      if (currentToken) {
        sendTokenToServer(currentToken);
        updateUIForPushEnabled(currentToken);
      } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // Show permission UI.
        updateUIForPushPermissionRequired();
        setTokenSentToServer(false);
      }
    }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
      showToken('Error retrieving Instance ID token. ', err);
      setTokenSentToServer(false);
    });
    // Callback fired if Instance ID token is updated.
    messaging.onTokenRefresh(() => {
      messaging.getToken().then((refreshedToken) => {
        console.log('Token refreshed.');
        // Indicate that the new Instance ID token has not yet been sent to the
        // app server.
        setTokenSentToServer(false);
        // Send Instance ID token to app server.
        sendTokenToServer(refreshedToken);
        // ...
      }).catch((err) => {
        console.log('Unable to retrieve refreshed token ', err);
        showToken('Unable to retrieve refreshed token ', err);
      });
    });
    
    function resetUI() {
    clearMessages();
    showToken('loading...');
    messaging.getToken().then((currentToken) => {
      if (currentToken) {
        sendTokenToServer(currentToken);
        updateUIForPushEnabled(currentToken);
      } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // Show permission UI.
        updateUIForPushPermissionRequired();
        setTokenSentToServer(false);
      }
    }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
      showToken('Error retrieving Instance ID token. ', err);
      setTokenSentToServer(false);
    });
    }
    
    function showToken(currentToken) {
    // // Show token in console and UI.
    // const tokenElement = document.querySelector('#token');
    // tokenElement.textContent = currentToken;
    // document.getElementById("device_token").value = currentToken;
    }
    
    function sendTokenToServer(currentToken) {
    if (!isTokenSentToServer()) {
      console.log('Sending token to server...');
      // TODO(developer): Send the current token to your server.
      setTokenSentToServer(true);
      //save to our server
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          //document.getElementById("demo").innerHTML = this.responseText;
        }
      };
      xhttp.open("GET", "https://pride-limited.com/save-device_token/" + currentToken, true);
      xhttp.send();
      
    } else {
      
      //save to our server
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          //document.getElementById("demo").innerHTML = this.responseText;
        }
      };
      xhttp.open("GET", "https://pride-limited.com/save-device_token/" + currentToken, true);
      xhttp.send();  
      
      console.log('Token already sent to server so won\'t send it again ' +
          'unless it changes');
    }
    
    }
    
    function isTokenSentToServer() {
    return window.localStorage.getItem('sentToServer') === '1';
    }
    
    function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? '1' : '0');
    }
    
    function showHideDiv(divId, show) {
    const div = document.querySelector('#' + divId);
    if (show) {
      div.style = 'display: visible';
    } else {
      div.style = 'display: none';
    }
    }
    
    function requestPermission() {
    console.log('Requesting permission...');
    // [START request_permission]
    Notification.requestPermission().then((permission) => {
      if (permission === 'granted') {
        console.log('Notification permission granted.');
        resetUI();
      } else {
        console.log('Unable to get permission to notify.');
      }
    });
    // [END request_permission]
    }
    
    function deleteToken() {
    messaging.getToken().then((currentToken) => {
      messaging.deleteToken(currentToken).then(() => {
        console.log('Token deleted.');
        setTokenSentToServer(false);
        resetUI();
      }).catch((err) => {
        console.log('Unable to delete token. ', err);
      });
    }).catch((err) => {
      console.log('Error retrieving Instance ID token. ', err);
      showToken('Error retrieving Instance ID token. ', err);
    });
    
    }
    
    // Add a message to the messages element.
    function appendMessage(payload) {
    const messagesElement = document.querySelector('#messages');
    const dataHeaderELement = document.createElement('h5');
    const dataElement = document.createElement('pre');
    dataElement.style = 'overflow-x:hidden;';
    dataHeaderELement.textContent = 'Received message:';
    dataElement.textContent = JSON.stringify(payload, null, 2);
    messagesElement.appendChild(dataHeaderELement);
    messagesElement.appendChild(dataElement);
    }
    
    // Clear the messages element of all children.
    function clearMessages() {
    // const messagesElement = document.querySelector('#messages');
    // while (messagesElement.hasChildNodes()) {
    //   messagesElement.removeChild(messagesElement.lastChild);
    // }
    }
    
    function updateUIForPushEnabled(currentToken) {
    showHideDiv(tokenDivId, true);
    showHideDiv(permissionDivId, false);
    showToken(currentToken);
    }
    
    function updateUIForPushPermissionRequired() {
    showHideDiv(tokenDivId, false);
    showHideDiv(permissionDivId, true);
    }
    
    resetUI();
    
    messaging.onMessage((payload) => {
    var title = payload.data.title;
    var options={
    body: payload.data.body,
    icon: payload.data.icon,
    image: payload.data.image,
    data:{
        time:new Date(Date.now()).toString(),
    	click_action: payload.data.click_action
    }
    };
    var myNotification =new Notification(title,options);
    console.log('Message received. ', payload);
    });
    
    self.addEventListener('notificationclick',function(event){
    var action_click=event.notification.data.click_action;
    event.notification.close();
    event.waitUntil(
     clients.openWindow(action_click)
    );
    });