(function() {
    document.getElementById('edit-address').addEventListener('click', function() {
        showAddressForm();
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#save-address').click(function() {
        var data={
            ordershipping_id:$('#address-ordershipping-id').val(),
            destination_id:$('#address-destination-id').val(),
            firstname:$('#address-fname').val(),
            lastname:$('#address-lname').val(),
            address:$('#address-address').val(),
            /*
            destination:$('#address-destination').val(),
            */
            city:$('#address-city').val(),
            zip:$('#address-zip').val(),
            phone:$('#address-phone').val()
        };
        $.ajax({
            type: 'POST',
            dataType : 'text', /*Default data type; Returned from server*/
            url: base_url+'/admin/order/update/address',
            data: data,
            success: function(d) {
                $('#text-name').text($('#address-fname').val()+' '+$('#address-lname').val());
                $('#text-address').text($('#address-address').val());
                $('#text-city').text($('#address-city').val());
                $('#text-zip').text($('#address-zip').val());
                $('#text-phone').text($('#address-phone').val());
                alert('Update success.After 5s page will auto reload.');
                hideAddressForm();
                setTimeout(function() {
                    location.reload();
                }, 5000);
            }
        });
    });
})();
function showAddressForm() {
    var address_text = document.getElementById('address-text');
    var address_field = document.getElementById('address-field');
    var edit_address = document.getElementById('edit-address');
    var save_address = document.getElementById('save-address');
    address_text.classList.remove('d-block');
    address_text.classList.add('d-none');
    address_field.classList.remove('d-none');
    address_field.classList.add('d-block');
    edit_address.classList.remove('d-block');
    edit_address.classList.add('d-none');
    save_address.classList.remove('d-none');
    save_address.classList.add('d-block');
}
function hideAddressForm() {
    var address_text = document.getElementById('address-text');
    var address_field = document.getElementById('address-field');
    var edit_address = document.getElementById('edit-address');
    var save_address = document.getElementById('save-address');
    address_text.classList.remove('d-none');
    address_text.classList.add('d-block');
    address_field.classList.remove('d-block');
    address_field.classList.add('d-none');
    edit_address.classList.remove('d-none');
    edit_address.classList.add('d-block');
    save_address.classList.remove('d-block');
    save_address.classList.add('d-none');
}