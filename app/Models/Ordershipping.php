<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ordershipping extends Model
{
    use HasFactory;
    
    public function conforder()
    {
        return $this->belongsTo('App\Models\Conforder');
    }
}
