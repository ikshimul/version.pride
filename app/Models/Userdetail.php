<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Userdetail extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_id',
        'address',
        'phone',
        'region', 
        'city',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function store($request,$user_id){
        $info=Userdetail::where('user_id',$user_id)->first();
        $details=Userdetail::find($info->id);
        $details->phone=$request->mobile_no;
        $details->address=$request->address;
        $details->region=$request->region_id;
        $details->city=$request->city;
        $details->zip=$request->zip;
        $details->save();
    }
}
