<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registeruser extends Model
{
    use HasFactory;
    protected $primaryKey = 'registeruser_id';
    protected $table = 'registeruser';
    
    protected $fillable = ['registeruser_login_id', 'registeruser_email', 'registeruser_firstname', 'registeruser_lastname'];
    
    public function store($request,$id){
        
    }
}
