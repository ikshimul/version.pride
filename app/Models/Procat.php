<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Procat extends Model
{
    use HasFactory;
    
    public function subprocats()
    {
        return $this->hasMany('App\Models\Subprocat', 'procat_id', 'id');
        //return $this->hasMany('App\Models\Subprocat');
    }
    
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
    
    public function getinfo($id){
        return Procat::find($id);
    }
}
