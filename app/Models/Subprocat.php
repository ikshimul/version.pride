<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Subprocat extends Model
{
    use HasFactory;
    
    public function procats()
    {
        return $this->belongsTo('App\Models\Procat');
    }
    
    public function getid($subproname){
        return Subprocat::where('subprocat_name', 'like', "%{$subproname}%")->first();
    }
    
    public function getinfo($id){
        return Subprocat::find($id);
    }
    
    public function getall(){
        return DB::table('subprocats')
                ->join('procats', 'procats.id', '=', 'subprocats.procat_id')
                ->select('subprocats.id', 'subprocats.subprocat_name', 'subprocats.subprocat_order', 'subprocats.subprocat_img', 'procats.id as procat_id', 'procats.procat_name')
                ->where('subprocats.status',0)
                ->get();
    }
    
}
