<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Carbon\Carbon;

class Product extends Model implements Buyable{
    
    //use Gloudemans\Shoppingcart\CanBeBought;

    //protected $primaryKey = 'id';
    protected $table = 'products';
    
    public function getBuyableIdentifier($options = null){
        return $this->id;
    }
    public function getBuyableDescription($options = null){
        return $this->name;
    }
    public function getBuyablePrice($options = null){
        return $this->price;
    }
    public function getBuyableWeight($options = null){
        return $this->weight;
    }
    
    public function cats()
    {
        return $this->belongsTo('App\Models\Cat');
    }
    
    public function subcats()
    {
        return $this->belongsTo('App\Models\Subcat');
    }
    
    public function productsize()
    {
        return $this->hasMany('App\Models\Productsize');
    }
    
    public function productalbums()
    {
        return $this->hasMany('App\Models\Productalbum');
    }
    
    public function listnew($category=null){
        $product = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                //->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name','productalbums.productalbum_img');
        if($category=='women'){
                 $single=$product->whereIn('products.procat_id', [5, 9, 10, 11, 12, 13, 20, 7, 16])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($category=='men'){
             $single=$product->whereIn('products.procat_id', [17, 18])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($category=='kids'){
            $single=$product->whereIn('products.procat_id', [6])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($category=='accessories'){
            $single=$product->whereIn('products.subprocat_id', [78,89])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($category=='home'){
            $single=$product->whereIn('products.procat_id', [19])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }else{
             $single=$product->where('product_active_deactive', 0)
             ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->paginate(18);
        }
        
        return $single;
    }
    
    public function listnew_v2($maincat=0){
        if($maincat==0){
            $product = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                //->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name','productalbums.productalbum_img')
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }else{
        $product = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                //->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name','productalbums.productalbum_img')
                ->where('product_active_deactive', 0)
                ->where('products.maincat',$maincat)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }    
        return $product;
    }
    
    public function listnewcatewise($subcat){
        $product = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                ->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name','productalbums.productalbum_img');
        if($subcat=='signature'){
                 $single=$product->whereIn('products.procat_id', [9,16])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($subcat=='classic'){
             $single=$product->whereIn('products.procat_id', [7])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($subcat=='pride-girls'){
            $single=$product->whereIn('products.procat_id', [5])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($subcat=='ethnic'){
            $single=$product->whereIn('products.subprocat_id', [17,18])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($subcat=='boys'){
            $single=$product->whereIn('products.procat_id', [21])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($subcat=='girls'){
            $single=$product->whereIn('products.procat_id', [6])
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }else{
             $single=$product->where('product_active_deactive', 0)
             ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->paginate(18);
        }
        
        return $single;
    }
    
    public function listnewcatewise_v2($id){
        $product = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                //->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name','productalbums.productalbum_img')
                ->where('products.cat',$id)
                ->where('product_active_deactive', 0)
                ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        return $product;
    }
    public function latestproduct($category=null){
        $product = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                //->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_styleref', 'products.product_name', 'productimgs.productimg_img', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name', 'products.product_name', 'products.product_img_thm', 'productalbums.productalbum_name', 'productalbums.productalbum_img');
        if($category=='women'){
                 $single=$product->whereIn('products.procat_id', [5, 9, 10, 11, 12, 13, 20, 7, 16])
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->first();
        }elseif($category=='men'){
             $single=$product->whereIn('products.procat_id', [17, 18])
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->first();
        }elseif($category=='kids'){
            $single=$product->whereIn('products.procat_id', [6])
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->first();
        }elseif($category=='accessories'){
            $single=$product->whereIn('products.subprocat_id', [78,89])
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->first();
        }elseif($category=='home'){
            $single=$product->whereIn('products.procat_id', [19])
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->first();
        }elseif($category=='festive'){
            $single=$product->where('isSpecial',12)
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->first();
        }elseif($category=='office'){
            $single=$product->where('products.id',801)
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->first();
        }elseif($category=='casual'){
            $single=$product->where('products.id',822)
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->first();
        }elseif($category=='ready'){
            $single=$product->where('products.id',722)
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->first();
        }else{
             $single=$product->where('product_active_deactive', 0)
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->first();
        }
        
        return $single;
    }
    
    public function products($maincat){
        $product = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                //->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name','productalbums.productalbum_img');
        if($maincat=='women'){
                 $single=$product->whereIn('products.procat_id', [5, 7, 9, 10, 11, 12, 13, 14, 15, 20, 16])
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($maincat=='men'){
             $single=$product->whereIn('products.procat_id', [17,18])
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($maincat=='kids'){
            $single=$product->whereIn('products.procat_id', [6])
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($maincat=='accessories'){
            $single=$product->whereIn('products.procat_id', [19])
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($maincat=='home'){
            $single=$product->whereIn('products.procat_id', [19])
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($maincat=='new'){
             $single=$product->where('product_active_deactive', 0)
             ->whereDate('products.created_at', '>', Carbon::now()->subDays(60))
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->paginate(18);
        }
        
        return $single;
    }
    
    public function searchbyprice($maincat, $lower_price, $upper_price){
        
        $product = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                //->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name','productalbums.productalbum_img');
        if($maincat=='women'){
                 $product=$product->whereIn('products.procat_id', [5, 7, 9, 10, 11, 12, 13, 14, 15, 20, 16]);
                 if($lower_price<=$upper_price && $lower_price>=0 && $upper_price>=0) {
                     $product = $product->whereBetween('product_price', [$lower_price, $upper_price]);
                 }elseif($lower_price>=0) {
                     $product = $product->where('product_price', '>=', $lower_price);
                 }
                $single=$product->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($maincat=='men'){
             $single=$product->whereIn('products.procat_id', [17,18]);
             if($lower_price<=$upper_price && $lower_price>=0 && $upper_price>=0) {
                     $product = $product->whereBetween('product_price', [$lower_price, $upper_price]);
                 }elseif($lower_price>=0) {
                     $product = $product->where('product_price', '>=', $lower_price);
                 }
                $single=$product
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($maincat=='kids'){
            $single=$product->whereIn('products.procat_id', [6]);
            if($lower_price<=$upper_price && $lower_price>=0 && $upper_price>=0) {
                     $product = $product->whereBetween('product_price', [$lower_price, $upper_price]);
                 }elseif($lower_price>=0) {
                     $product = $product->where('product_price', '>=', $lower_price);
                 }
                $single=$product
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($maincat=='accessories'){
            $single=$product->whereIn('products.procat_id', [19]);
            if($lower_price<=$upper_price && $lower_price>=0 && $upper_price>=0) {
                     $product = $product->whereBetween('product_price', [$lower_price, $upper_price]);
                 }elseif($lower_price>=0) {
                     $product = $product->where('product_price', '>=', $lower_price);
                 }
                $single=$product
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($maincat=='home'){
            $single=$product->whereIn('products.procat_id', [19]);
            if($lower_price<=$upper_price && $lower_price>=0 && $upper_price>=0) {
                     $product = $product->whereBetween('product_price', [$lower_price, $upper_price]);
                 }elseif($lower_price>=0) {
                     $product = $product->where('product_price', '>=', $lower_price);
                 }
                $single=$product
                ->where('product_active_deactive', 0)
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($maincat=='new'){
             $single=$product->where('product_active_deactive', 0)
             ->whereDate('products.created_at', '>', Carbon::now()->subDays(60));
             if($lower_price<=$upper_price && $lower_price>=0 && $upper_price>=0) {
                     $product = $product->whereBetween('product_price', [$lower_price, $upper_price]);
                 }elseif($lower_price>=0) {
                     $product = $product->where('product_price', '>=', $lower_price);
                 }
             $single=$product
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->paginate(18);
        }
        
        return $single;
    }
    
    public function search($key){
       return $search = DB::table('products')
            ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
            ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
            ->select('products.id','products.subcat', 'products.product_price', 'products.product_styleref', 'products.product_name', 'productalbums.productalbum_name', 'productimgs.productimg_img', 'productimgs.productimg_img_medium')
            ->where('products.product_name', 'like', "%{$key}%")
            ->orWhere('products.product_description', 'like', "%{$key}%")
            ->orWhere('products.product_styleref', 'like', "%{$key}%")
            ->where('products.product_active_deactive', 0)
            ->groupBy('products.id')
            ->orderBy('products.id', 'DESC')
            ->limit(10)
            ->get();
    }
    
    public function searchwithpagi($key){
        return $search = DB::table('products')
            ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
            ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
            ->select('products.id','products.subcat', 'products.product_price', 'products.product_styleref', 'products.product_name','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'productalbums.productalbum_img','productalbums.productalbum_name', 'productimgs.productimg_img', 'productimgs.productimg_img_medium')
            ->where('products.product_name', 'like', "%{$key}%")
            ->orWhere('products.product_description', 'like', "%{$key}%")
            ->orWhere('products.product_styleref', 'like', "%{$key}%")
            ->where('products.product_active_deactive', 0)
            ->groupBy('products.id')
            ->orderBy('products.id', 'DESC')
            ->paginate(18);
    }
    
    public function NewArriavl(){
        return $new=DB::table('products')
        //->join('subprocats','subprocats.id','=','products.subprocat_id')
        ->join('productalbums','productalbums.product_id','=','products.id')
        ->select('products.id','products.subcat','product_price','product_styleref','product_description','products.product_img_thm','product_name','products.created_at','productalbum_name')
        ->where('product_active_deactive',0)
        //->whereDate('products.created_at', '>', Carbon::now()->subDays(30))
        ->groupBy('products.id')
        ->orderBy('products.id','DESC')
        ->limit(10)
        ->get();
        return $new;
    }
    
    public function listold($subcat_id=null){
        return $product = DB::table('products')
        ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
        //->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
        ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
        ->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name','productalbums.productalbum_img')
        ->where('products.subprocat_id',$subcat_id)
        ->where('product_active_deactive', 0)
        ->orderBy('products.id', 'desc')
        ->orderBy('productimgs.id','ASC')
        //->groupBy('productalbums.id')
        ->groupBy('products.id')
        ->paginate(18);
    }
    
    public function listAllold($procat){
        //dd($procat);
        return $product = DB::table('products')
        ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
        //->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
        ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
        ->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name','productalbums.productalbum_img')
        ->whereIn('products.procat_id',$procat)
        ->where('product_active_deactive', 0)
        ->orderBy('products.id', 'desc')
        ->orderBy('productimgs.id','ASC')
        //->groupBy('productalbums.id')
        ->groupBy('products.id')
        ->paginate(18);
    }
    
    public function viewincrement($product_id){
        Product::where('id', $product_id)->increment('view');
    }
    
    public function details($product_id){
        return DB::table('products')
        ->join('maincats','maincats.id','=','products.maincat')
        ->join('cats','cats.id','=','products.cat')
        ->join('subcats','subcats.id','=','products.subcat')
        ->join('productalbums', 'productalbums.product_id', '=', 'products.id')
        ->join('productimgs', 'productimgs.productalbum_id', '=', 'productalbums.id')
        ->select('products.*','cats.name as cat_name','maincats.name','subcats.name as subcat_name','productalbums.productalbum_name','productimgs.productimg_img_thm')
        ->where('products.id',$product_id)
        ->groupBy('products.id')
        ->first();
       //   Product::find($product_id);
    }
    
    public function MayLike($cat, $id, $subcat){
        return $productlist=DB::table('products')
        ->join('productalbums','productalbums.product_id','=','products.id')
        ->select('products.id','products.subcat','product_price','product_styleref','product_description','products.product_img_thm','product_name','products.created_at','productalbum_name')
        ->where('product_active_deactive',0)
        ->where('products.cat',$cat)
        ->whereNotIn('products.subcat', [$subcat])
        ->whereNotIn('products.id', [$id])
        ->groupBy('products.id')
        ->orderBy('products.id','DESC')
        ->skip(2)->take(10)
        ->get();
        return $productlist;
    }
    
    public function TopPicks($cat, $id, $subcat){
        return $productlist=DB::table('products')
        ->join('productalbums','productalbums.product_id','=','products.id')
        ->select('products.id','products.subcat','product_price','product_styleref','product_description','products.product_img_thm','product_name','products.created_at','productalbum_name')
        ->where('product_active_deactive',0)
        ->where('products.cat',$cat)
        ->whereNotIn('products.subcat', [$subcat])
        ->whereNotIn('products.id', [$id])
        ->groupBy('products.id')
        ->orderBy('products.view', 'desc')
        ->limit(2)
        ->get();
        return $productlist;
    }
    
    public function getFabrics($categoryid = 0, $subcategory = 0){
        $fabrics = Product::where('products.procat_id', $categoryid)
            ->where('products.subprocat_id', $subcategory)
            ->groupBy('fabric')
            ->orderBy('fabric')
            ->pluck('fabric')
            ->toArray();
        if($categoryid == 0 || $subcategory == 0)
        $fabrics = Product::groupBy('fabric')
            ->orderBy('fabric')
            ->pluck('fabric')
            ->toArray();
        return $fabrics;
    }
    
    public function adminlist($subpro_id = null){
        if($subpro_id==null){
            $list = DB::table('products')
            ->join('subcats', 'products.subcat', '=', 'subcats.id')
            ->join('cats', 'products.cat', '=', 'cats.id')
            ->join('maincats', 'products.maincat', '=', 'maincats.id')
            ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
            ->join('productimgs', 'productimgs.productalbum_id', '=', 'productalbums.id')
            ->select('products.*','productimgs.productimg_img_tiny','productimgs.productimg_img_thm','productimgs.productimg_img_medium','productimgs.productimg_img', 'subcats.name as subcat_name', 'cats.name as subprocat_name', 'maincats.name as procat_name')
            ->where('products.trash', 0)
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->get();
        }else{
        $list = DB::table('products')
            ->join('subcats', 'products.subcat', '=', 'subcats.id')
            ->join('cats', 'products.cat', '=', 'cats.id')
            ->join('maincats', 'products.maincat', '=', 'maincats.id')
            ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
            ->join('productimgs', 'productimgs.productalbum_id', '=', 'productalbums.id')
            ->select('products.*','productimgs.productimg_img_tiny','productimgs.productimg_img_thm','productimgs.productimg_img_medium','productimgs.productimg_img', 'subcats.name as subcat_name', 'cats.name as subprocat_name', 'maincats.name as procat_name')
            ->where('products.subcat', $subpro_id)
            ->where('products.trash', 0)
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->get();
        }
       return $list;
    }
    
    public function adminlistTrash(){
       return $list = DB::table('products')
            ->join('subcats', 'products.subcat', '=', 'subcats.id')
            ->join('cats', 'products.cat', '=', 'cats.id')
            ->join('maincats', 'products.maincat', '=', 'maincats.id')
            ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
            ->join('productimgs', 'productimgs.productalbum_id', '=', 'productalbums.id')
            ->select('products.*','productimgs.productimg_img_tiny','productimgs.productimg_img_thm','productimgs.productimg_img_medium','productimgs.productimg_img', 'subcats.name as subcat_name', 'cats.name as subprocat_name', 'maincats.name as procat_name')
            ->where('products.trash', 1)
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->get();
    }
    
    public function list($subcat_id=null){
       $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('product_active_deactive', 0)
			->where('products.subcat', $subcat_id)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(18);
			// dd($productlist);
			 return $productlist;
    }
    
    public function listPricefilter($subcat_id=null,$minprice,$maxprice){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.subcat', $subcat_id)
			->whereBetween('product_price', [$minprice, $maxprice])
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(18);
    }
    
    public function listSizefilter($subcat_id=null,$size){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->join('productsizes','products.id' ,'=', 'productsizes.product_id')
			->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.subcat', $subcat_id)
			->where('productsizes.productsize_size',$size)
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(18);
    }
    
    public function listColorfilter($subcat_id=null,$color){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->join('productsizes','products.id' ,'=', 'productsizes.product_id')
			->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.subcat', $subcat_id)
			->where('productsizes.color_name', 'like', '%'.$color.'%')
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(18);
    }
	
	public function listAll($cat){
	    return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.cat', $cat)
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listAllPricefilter($cat,$minprice,$maxprice){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.cat', $cat)
			->whereBetween('product_price', [$minprice, $maxprice])
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listAllSizefilter($cat,$size){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->join('productsizes','products.id' ,'=', 'productsizes.product_id')
			->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.cat', $cat)
			->where('productsizes.productsize_size',$size)
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listAllColorfilter($cat,$color){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->join('productsizes','products.id' ,'=', 'productsizes.product_id')
			->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.cat', $cat)
			->where('productsizes.color_name', 'like', '%'.$color.'%')
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listAllMaincatwise($maincat){
       return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.maincat', $maincat)
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listAllMaincatwisePricefilter($maincat,$minprice,$maxprice){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.maincat', $maincat)
			->whereBetween('product_price', [$minprice, $maxprice])
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listAllMaincatwiseSizefilter($maincat,$size){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->join('productsizes','products.id' ,'=', 'productsizes.product_id')
			->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.maincat', $maincat)
			->where('productsizes.productsize_size',$size)
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listAllMaincatwiseColorfilter($maincat,$color){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->join('productsizes','products.id' ,'=', 'productsizes.product_id')
			->select('products.id','products.subcat', 'products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.maincat', $maincat)
			->where('productsizes.color_name', 'like', '%'.$color.'%')
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listOccasions($occassion){
        $product = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                //->join('subprocats', 'products.subprocat_id', '=', 'subprocats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium', 'productalbums.productalbum_name','productalbums.productalbum_img');
        if($occassion=='festive-wear'){
                 $single=$product->where('products.spcollection', 12)
                ->where('product_active_deactive', 0)
                ->orderBy('products.sold', 'DESC')
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($occassion=='office-wear'){
             $single=$product->where('products.isSpecial', 13)
                ->where('product_active_deactive', 0)
                ->orderBy('products.sold', 'DESC')
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($occassion=='ready-to-wear'){
            $single=$product->where('products.cat', 3)
                ->where('product_active_deactive', 0)
                ->orderBy('products.sold', 'DESC')
                ->orderBy('products.id', 'DESC')
                ->groupBy('products.id')
                ->paginate(18);
        }elseif($occassion=='casual-wear'){
             $single=$product->where('products.subcat', 10)
             ->where('product_active_deactive', 0)
             ->orderBy('products.sold', 'DESC')
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->paginate(18);
        }else{
            $single=$product->where('product_active_deactive', 0)
             ->orderBy('products.sold', 'DESC')
            ->orderBy('products.id', 'DESC')
            ->groupBy('products.id')
            ->paginate(18);
        }
        
        return $single;
    }
    
    public function listCollection($isSpecial){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.isSpecial', $isSpecial)
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listCollectionColorfilter($isSpecial,$color){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->join('productsizes','products.id' ,'=', 'productsizes.product_id')
			->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.isSpecial', $isSpecial)
			->where('productsizes.color_name', 'like', '%'.$color.'%')
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listCollectionSizefilter($isSpecial,$size){
        return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->join('productsizes','products.id' ,'=', 'productsizes.product_id')
			->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.isSpecial', $isSpecial)
			->where('productsizes.productsize_size',$size)
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }
    
    public function listCollectionPricefilter($isSpecial,$minprice,$maxprice){
         return $productlist = DB::table('products')
			->join('productalbums', 'products.id', '=', 'productalbums.product_id')
			->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
			->select('products.id', 'products.subcat','products.product_price','products.created_at', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.product_styleref', 'products.product_name', 'productimgs.productimg_img_medium','productimgs.productimg_img', 'productalbums.productalbum_name','productalbums.productalbum_img')
			->where('products.isSpecial', $isSpecial)
			->whereBetween('product_price', [$minprice, $maxprice])
			->where('product_active_deactive', 0)
			->orderBy('products.sold','DESC')
			->orderBy('products.id', 'desc')
			->groupBy('productalbums.id')
			->paginate(36);
    }

}
