<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Registeruserdetails extends Model
{
    use HasFactory;
    
    //registeruserdetails
	protected $primaryKey = 'registeruserdetails_id';
    protected $table = 'registeruserdetails';
    
    public function detailsstore($request){
        $userdetails['registeruser_login_id'] = $request->login_user_id;
        $userdetails['registeruser_id'] = $request->registeruser_id;
        $userdetails['registeruser_address'] = $request->registeruser_address;
        $userdetails['registeruser_country'] = $request->Shipping_ddlcountry;
        $userdetails['registeruser_city'] = $request->registeruser_city;
        $userdetails['registeruser_zipcode'] = $request->registeruser_zipcode;
        $userdetails['registeruser_phone'] = $request->registeruser_phone;
        $insert=DB::table('registeruserdetails')->insertGetId($userdetails);
        return $insert;
    }
    
    public function detailsupdate($request){
        $userdetails['registeruser_login_id'] = $request->login_user_id;
        $userdetails['registeruser_id'] = $request->registeruser_id;
        $userdetails['registeruser_address'] = $request->registeruser_address;
        $userdetails['registeruser_country'] = $request->Shipping_ddlcountry;
        $userdetails['registeruser_city'] = $request->registeruser_city;
        $userdetails['registeruser_zipcode'] = $request->registeruser_zipcode;
        $userdetails['registeruser_phone'] = $request->registeruser_phone;
		$update=DB::table('registeruserdetails')->where('registeruser_id',$request->registeruser_id)->update($userdetails);
		return $update;
    }
    
    public function ShippingUpdate($request,$user_id){
        $update_register_details['registeruser_address']=$request->address;
        $update_register_details['registeruser_phone']=$request->mobile_no;
        $update_register_details['registeruser_city']=$request->registeruser_city;
        $update_register_details['registeruser_zipcode']=$request->registeruser_zipcode;
        $update=DB::table('registeruserdetails')->where('registeruser_id',$user_id)->update($update_register_details);
        return $update;
    }
    
    public function GetUserDetailsByUserId($register_id){
        $check_user_details=DB::table('registeruserdetails')->select('registeruser_id','registeruser_phone','registeruserdetails_id')->where('registeruser_id',$register_id)->first();
        return $check_user_details;
    }
    
}
