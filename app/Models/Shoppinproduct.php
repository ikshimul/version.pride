<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Productsize;
use DB;

class Shoppinproduct extends Model
{
    use HasFactory;
    
    public function shoppingcarts() {
        return $this->belongsTo('App\Models\Shoppingcart') ;
    }
    
    public function totalSale(){
        return DB::table('shoppinproducts')
                ->join('shoppingcarts', 'shoppinproducts.shoppingcart_id', '=', 'shoppingcarts.id')
                ->join('conforders', 'shoppingcarts.id', '=', 'conforders.shoppingcart_id')
                ->where('conforders.conforder_status', '=', 'Closed')
                ->sum('shoppinproducts.shoppinproduct_quantity');
    }
    
    public function savedata($request,$shoppingcart_id){
        $total_pro = $request->product_id;
        $total=0;
        for ($i = 0; $i < count($total_pro); $i++) {
            $shoppinproduct['shoppingcart_id']=$shoppingcart_id;
            $shoppinproduct['product_id']=$request->product_id[$i];
            $shoppinproduct['product_barcode']=$request->product_barcode[$i];
            $shoppinproduct['prosize_name']=$request->product_size[$i];
            $shoppinproduct['productalbum_name']=$request->productalbum_name[$i];
            $shoppinproduct['product_price']=$request->product_price[$i];
            $shoppinproduct['shoppinproduct_quantity']=$request->shoppinproduct_quantity[$i];
            $shoppinproduct['cart_image']=$request->image_link[$i];
            DB::table('shoppinproducts')->insert($shoppinproduct);
           // $shoppinproduct->save();
            $total+=$request->product_price[$i]*$request->shoppinproduct_quantity[$i];
            
            // Reduce product quantity 
            $pro_qty = Productsize::where('product_id', $request->product_id[$i])->where('color_name', $request->productalbum_name[$i])->where('productsize_size', $request->product_size[$i])->first();
            $qty_product_id = $pro_qty->product_id;
            $productsize_size = $pro_qty->productsize_size;
            $color_name = $pro_qty->color_name;
            $SizeWiseQty = $pro_qty->SizeWiseQty;
            $qty = $request->shoppinproduct_quantity[$i];
            $reduce_data['SizeWiseQty'] = $SizeWiseQty - $qty;
            Productsize::where('product_id', $pro_qty->product_id)->where('color_name', $pro_qty->color_name)->where('productsize_size', $pro_qty->productsize_size)->update($reduce_data);
        }
        
        return $total;
    }
    
    public function orderproducts($shoppingcart_id){
        $total_incomplete_order_info = DB::table('products')
                ->join('shoppinproducts', 'products.id', '=', 'shoppinproducts.product_id')
                ->select('products.product_name', 'products.product_img_thm','products.product_price as original_price', 'products.product_styleref', 'shoppinproducts.id as shoppinproduct_id', 'shoppinproducts.shoppinproduct_quantity', 'shoppinproducts.prosize_name', 'shoppinproducts.cart_image', 'shoppinproducts.productalbum_name', 'shoppinproducts.product_price', 'shoppinproducts.product_id','shoppinproducts.exchange')
                ->where('shoppinproducts.shoppingcart_id', '=', $shoppingcart_id)
                ->get();
        return $total_incomplete_order_info;
    }
}
