<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cashexpenses extends Model
{
    use HasFactory;
    
    public function conforder()
    {
        return $this->belongsTo('App\Models\Conforder');
    }
}
