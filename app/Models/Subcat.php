<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Subcat extends Model
{
    use HasFactory;
    
    public function cat() {
        return $this->belongsTo('App\Models\Cat');
    }


    public function products()
    {
        return $this->belongsTo('App\Models\Product');
    }
    
    public function getall(){
        return DB::table('subcats')
                ->join('maincats', 'maincats.id', '=', 'subcats.maincat')
                ->join('cats', 'cats.id', '=', 'subcats.cat')
                ->select('subcats.id', 'subcats.name', 'subcats.slug', 'subcats.subcat_banner', 'cats.id as cat_id', 'cats.name as cat_name','maincats.name as maincat_name')
                ->where('subcats.active',1)
                ->get();
    }
}
