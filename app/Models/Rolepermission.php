<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rolepermission extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'role_id', 'permission',
    ];
    
    public function get_permission($role_id){
		$role_permission=Rolepermission::where('role_id',$role_id)->get();
		$permission=[];
		foreach($role_permission as $p_list){
			$permission[]=$p_list->permission;
		}
		return $permission;
	}
}
