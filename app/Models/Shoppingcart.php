<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shoppingcart extends Model
{
    use HasFactory;
    
    public function conforders()
    {
        return $this->hasOne(Conforder::class);
    }
    
    public function shoppinproducts()
    {
        return $this->hasMany(Shoppinproduct::class);
    }
    
}
