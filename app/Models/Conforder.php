<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;


class Conforder extends Model
{
    use HasFactory;
    
    public function shoppingcart()
    {
        return $this->belongsTo(Shoppingcart::class);
    }
    
    public function cashtransactions()
    {
        return $this->hasOne('App\Models\Cashtransaction');
    }
    
    public function cashexpenses()
    {
        return $this->hasOne('App\Models\Cashexpenses');
    }
    
    public function rider()
    {
        return $this->belongsTo(Rider::class);
    }
    
    public function ordershippings()
    {
        return $this->hasOne('App\Models\Ordershipping');
    }
    
    public function orders(){
            return DB::table('conforders')
            ->join('ordershippings', 'conforders.id', '=', 'ordershippings.conforder_id')
            ->join('shoppingcarts','conforders.shoppingcart_id','=','shoppingcarts.id')
            ->leftjoin('riders','conforders.delivery_by','=','riders.id')
            ->select('conforders.id', 'conforders.conforder_status','conforders.conforder_placed_date','conforders.created_at', 'conforders.conforder_tracknumber', 'conforders.pos_entry_date', 'conforders.conforder_deliverydate', 'shoppingcarts.shipping_charge', 'ordershippings.Shipping_txtfirstname', 'ordershippings.Shipping_txtlastname','shoppingcarts.payment_method','riders.name as delivery_by')
            ->orderBy('conforders.id', 'desc')
            ->get();
    }
    
    public function incompleteorder(){
            return DB::table('conforders')
            ->join('ordershippings', 'conforders.id', '=', 'ordershippings.conforder_id')
            ->join('shoppingcarts','conforders.shoppingcart_id','=','shoppingcarts.id')
            ->leftjoin('riders','conforders.delivery_by','=','riders.id')
            ->select('conforders.id', 'conforders.conforder_status','conforders.conforder_placed_date','conforders.created_at', 'conforders.conforder_tracknumber', 'conforders.pos_entry_date', 'conforders.conforder_deliverydate', 'shoppingcarts.shipping_charge', 'ordershippings.Shipping_txtfirstname', 'ordershippings.Shipping_txtlastname','shoppingcarts.payment_method','riders.name as delivery_by')
            ->where('conforder_status', '!=', 'Closed')
            ->where('conforder_status', '!=', 'Cancelled')
            ->where('conforder_status', '!=', 'Exchanged')
            ->where('conforder_status', '!=', 'Invalidate')
            ->where('conforder_status', '!=', 'Returned')
            ->orderBy('conforders.id', 'desc')
            ->get();
    }
    
    public function recent_sales(){
        $date_end=Carbon::now()->subMonth()->toDateTimeString();
        return DB::table('conforders')
		->join('shoppingcarts', 'conforders.shoppingcart_id', '=', 'shoppingcarts.id')
		->join('shoppinproducts', 'shoppingcarts.id', '=', 'shoppinproducts.shoppingcart_id')
		->join('products', 'shoppinproducts.product_id', '=', 'products.id')
        ->join('procats', 'products.procat_id', '=', 'procats.id')
        ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
        ->join('productimgs', 'productimgs.productalbum_id', '=', 'productalbums.id')
        ->select('conforders.created_at','products.id','products.product_name', 'products.product_description', 'products.product_styleref', 'products.product_price', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.created_at as add_date','productimgs.productimg_img_thm')
        ->orderBy('conforders.id', 'desc')
        ->where('conforder_status','Closed')
		->where('conforders.created_at', '>=', $date_end)
        ->groupBy('products.id')
		->skip(0)->take(7)
        ->get();
    }
    
    public function orderdetails($order_id){
        $shipping_address_details = DB::table('conforders')
                ->select('conforders.id','conforders.user_id', 'conforders.conforder_tracknumber', 'conforders.conforder_status', 'conforders.order_threepldlv', 'conforders.delivery_by', 'conforders.conforder_deliverynotes','conforders.conforder_statusdetails', 'conforders.created_at', 'conforders.conforder_deliverydate', 'conforders.pos_entry_date', 'shoppingcarts.id as shoppingcart_id', 'shoppingcarts.voucher_id', 'shoppingcarts.shipping_charge', 'shoppingcarts.payment_method', 'shoppingcarts.extra_charge', 'shoppingcarts.voucher_id','shoppingcarts.shoppingcart_subtotal','shoppingcarts.shoppingcart_total', 'ordershippings.email','ordershippings.Shipping_txtfirstname', 'ordershippings.Shipping_txtlastname', 'ordershippings.Shipping_txtaddressname', 'ordershippings.Shipping_txtcity', 'ordershippings.Shipping_txtzipcode', 'ordershippings.Shipping_txtphone', 'ordershippings.id as ordershipping_id',  'cities.id as city_id', 'cities.name', 'regions.id as region_id', 'regions.name as region_name', 'userdetails.admin_comment','users.id as user_id','cashexpenses.amount as expense_amount','riders.name as delivery_by')
                ->join('shoppingcarts', 'conforders.shoppingcart_id', '=', 'shoppingcarts.id')
                ->join('ordershippings', 'conforders.id', '=', 'ordershippings.conforder_id')
                ->leftjoin('cities', 'ordershippings.Shipping_txtcity', '=', 'cities.id')
                ->leftjoin('regions', 'regions.id', '=', 'cities.region_id')
                ->leftjoin('riders','conforders.delivery_by','=','riders.id')
                ->leftjoin('cashexpenses', 'conforders.id', '=', 'cashexpenses.conforder_id')
                ->leftjoin('users', 'conforders.user_id', '=', 'users.id')
                ->leftjoin('userdetails', 'userdetails.user_id', '=', 'users.id')
                ->where('conforders.id', '=', $order_id)
                ->first();
        return $shipping_address_details;
    }
}
