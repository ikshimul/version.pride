<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rider extends Model
{
    use HasFactory;
    
    public function conforder()
    {
        return $this->hasMany(Conforder::class,'delivery_by', 'id');
    }
}
