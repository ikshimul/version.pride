<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    use HasFactory;
    
    public function maincat() {
        return $this->belongsTo('App\Models\Maincat');
    }
    public function subcats() {
        return $this->hasMany('App\Models\Subcat') ;
    }
}
