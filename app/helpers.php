<?php
use App\Models\Rolepermission;
use App\Models\Conforder;

function getPermission($role_id){
        $role_permission = Rolepermission::where('role_id', $role_id)->get();
        $permission = [];
        foreach ($role_permission as $p_list) {
            $permission[] = $p_list->permission;
        }
        return $permission;  
}

function newOrder(){
    return Conforder::where('conforder_status', '!=', 'Closed')->where('conforder_status', '!=', 'Cancelled')->where('conforder_status', '!=', 'Exchanged')->where('conforder_status', '!=', 'Invalidate')->where('conforder_status', '!=', 'Returned')->count();
}

function newOrderDiscription(){
    return Conforder::where('conforder_status', '!=', 'Closed')->where('conforder_status', '!=', 'Cancelled')->where('conforder_status', '!=', 'Exchanged')->where('conforder_status', '!=', 'Invalidate')->where('conforder_status', '!=', 'Returned')->orderBy('id','DESC')->limit(10)->get();
}
 
?>