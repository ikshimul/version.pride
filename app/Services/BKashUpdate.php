<?php
namespace App\Services;

use DB;
use App\Models\Conforder;
use App\Models\Shoppingcart;
use App\Models\Bkashpayment;

//Configuration data are available in 'storage/app/config.json'

class BKashUpdate
{

    public function GetToken(){
			$request_token=$this->bkash_Get_Token();
			$idtoken=$request_token['id_token'];
				
			$_SESSION['token']=$idtoken;
			$strJsonFileContents = file_get_contents("storage/app/config.json");
			$array = json_decode($strJsonFileContents, true);

			$array['token']=$idtoken;

			$newJsonString = json_encode($array);
			file_put_contents('storage/app/config.json',$newJsonString);

			echo $idtoken;
	}
	
	public function bkash_Get_Token(){

	$strJsonFileContents = file_get_contents("storage/app/config.json");
	$array = json_decode($strJsonFileContents, true);
	
	$post_token=array(
        'app_key'=>$array["app_key"],                                              
		'app_secret'=>$array["app_secret"]                  
	);	
    
    $url=curl_init($array["tokenURL"]);
	$proxy = $array["proxy"];
	$posttoken=json_encode($post_token);
	$header=array(
		'Content-Type:application/json',
		'password:'.$array["password"],                                                               
        'username:'.$array["username"]                                                           
    );				
    
    curl_setopt($url,CURLOPT_HTTPHEADER, $header);
	curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($url,CURLOPT_POSTFIELDS, $posttoken);
	curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
	//curl_setopt($url, CURLOPT_PROXY, $proxy);
	$resultdata=curl_exec($url);
	curl_close($url);
	return json_decode($resultdata, true);    
}

 public function CreatePayment($invoice){
	$strJsonFileContents = file_get_contents("storage/app/config.json");
	$array = json_decode($strJsonFileContents, true);
	$shopping_cart=Conforder::find($invoice);
	$shopping_amount=Shoppingcart::find($shopping_cart->shoppingcart_id);
	$amount=$shopping_amount->shoppingcart_total;
	$intent = "sale";
	$tracknumber = "PLORDER#100-" . $invoice;
	$proxy = $array["proxy"];
		$createpaybody=array('amount'=>$amount, 'currency'=>'BDT', 'merchantInvoiceNumber'=>$tracknumber,'intent'=>$intent);   
		$url = curl_init($array["createURL"]);
		$createpaybodyx = json_encode($createpaybody);
		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]
		);
		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_POSTFIELDS, $createpaybodyx);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);
		
		$resultdata = curl_exec($url);
		curl_close($url);
		echo $resultdata;
		exit;
   }
   
   public function ExecutePayment($paymentID){
	    $strJsonFileContents = file_get_contents("storage/app/config.json");
		$array = json_decode($strJsonFileContents, true);
		$proxy = $array["proxy"];

		$url = curl_init($array["executeURL"].$paymentID);

		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]              
		);	
			
		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);

		$resultdatax=curl_exec($url);
		curl_close($url);
		echo $resultdatax;
		exit;
   }
   public function success($request){
	    $strJsonFileContents = file_get_contents("storage/app/config.json");
		$array = json_decode($strJsonFileContents, true);
		$proxy = $array["proxy"];
// 		$array["queryURL"] = 'https://checkout.sandbox.bka.sh/v1.2.0-beta/checkout/payment/query/';
		$array["queryURL"] = 'https://checkout.pay.bka.sh/v1.2.0-beta/checkout/payment/query/';

		$url = curl_init($array["queryURL"].$request->paymentID);

		$header=array(
			'Content-Type:application/json',
			'authorization:'.$array["token"],
			'x-app-key:'.$array["app_key"]              
		);	
			
		curl_setopt($url,CURLOPT_HTTPHEADER, $header);
		curl_setopt($url,CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($url, CURLOPT_PROXY, $proxy);

		$resultdatax=curl_exec($url);
		
		curl_close($url);
		$resultdatax = json_decode($resultdatax);
		//return $this->isValid($resultdatax->merchantInvoiceNumber);
		if(isset($resultdatax->merchantInvoiceNumber) && $resultdatax->merchantInvoiceNumber)
		    return $this->isValid($resultdatax);
		else
            abort(404);
   }
   public function isValid($resultdatax) {
       //$row = DB::table('conforder')->where('conforder_tracknumber', $resultdatax->merchantInvoiceNumber)->where('conforder_status', 'Order_Verification_Pending')->first();
	   $row = DB::table('conforders')
	        ->join('shoppingcarts', 'shoppingcarts.id', '=',  'conforders.shoppingcart_id')
	        ->where('conforder_tracknumber', $resultdatax->merchantInvoiceNumber)
	        ->where('conforder_status', 'Order_Verification_Pending')
	        ->select('conforders.id as conforder_id', 'shoppingcarts.shoppingcart_subtotal as shoppingcart_subtotal')
	        ->first();
       //if($row && $resultdatax->amount == $row->shoppingcart_subtotal && $resultdatax->currency == 'BDT') {
	   if($row) {
	      // dd($resultdatax);
	       $baksh=new Bkashpayment();
	       $baksh->conforder_id=$row->conforder_id;
	       $baksh->trxID=$resultdatax->trxID;
	       $baksh->paymentID=$resultdatax->paymentID;
	       $baksh->amount=$resultdatax->amount;
           $baksh->save();
           return $row->conforder_id;
       }
       return false;
   }
}