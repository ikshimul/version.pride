<?php
namespace App\Services;

use Auth;
use DB;
use Session;
use Mail;
use Cart;
use App\Models\Ordershipping;
use App\Models\Conforder;
use App\Models\User;
use App\Models\Shoppingcart;
use App\Models\Sslpayment;
use App\Models\Cities;
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class PaySSL
{
	public function submitpayment($tran_id){
	$orderinfo=Ordershipping::where('conforder_id', $tran_id)->first();
	$order=Conforder::find($tran_id);
    $amount_info=Shoppingcart::find($order->shoppingcart_id);	
	//$post_data = (array) $request;
	$post_data['tran_id'] = $tran_id;
	//live account
	$post_data['store_id'] = "PrideLimitedlive";
	$post_data['store_passwd'] = "5CD294FBBE6D655576";
	//Test account
// 	$post_data['store_id'] = "testbox";
// 	$post_data['store_passwd'] = "qwerty";
	$post_data['total_amount'] = $amount_info->shoppingcart_total;
	$post_data['currency'] = "BDT";
	$post_data['tran_id'] = $tran_id;
	$post_data['product_category'] = 'clothing';
	$server_name=url('/');
	$post_data['success_url'] = $server_name . "/SSL-success";
	$post_data['fail_url'] = $server_name . "/SSL-failure";
	$post_data['cancel_url'] = $server_name . "/SSL-cancel";
	$post_data['ipn_url'] = $server_name . "/SSL-ipn";
	//dd($post_data);
	# $post_data['multi_card_name'] = "mastercard,visacard,amexcard";  # DISABLE TO DISPLAY ALL AVAILABLE

	# EMI INFO
	// $post_data['emi_option'] = "1";
	// $post_data['emi_max_inst_option'] = "9";
	// $post_data['emi_selected_inst'] = "9";
	
    //dd($orderinfo);
    if($orderinfo->email !=null){
         $email=$orderinfo->email;
     }else{
        $email='hello@pride-grp.com'; 
     }
	# CUSTOMER INFORMATION
	$city_info=Cities::find($orderinfo->Shipping_txtcity);
	$post_data['cus_name'] = $orderinfo->Shipping_txtfirstname.' '.$orderinfo->Shipping_txtlastname;
	$post_data['cus_email'] = $email;
	$post_data['cus_add1'] = $orderinfo->Shipping_txtaddressname;
	$post_data['cus_add2'] = "";
	$post_data['cus_city'] = $city_info->name;
	$post_data['cus_state'] = "";
	$post_data['cus_postcode'] = $orderinfo->Shipping_txtzipcode;
	$post_data['cus_country'] = "Bangladesh";
	$post_data['cus_phone'] = $orderinfo->Shipping_txtphone;
	$post_data['cus_fax'] = "";

    $product_info=DB::table('shoppinproducts')
	->join('products','shoppinproducts.product_id','=','products.id')
	->select('shoppinproducts.*','products.product_name')
	->where('shoppingcart_id',$order->shoppingcart_id)->get();
	//dd($product_info);
	$num_of_item=count($product_info);
	foreach($product_info as $request) {
	    $products=$request->product_name;
	  //$products=implode(', ', $request->product_name);
	}
	$post_data['shipping_method'] = 'NO';
	$post_data['num_of_item'] = $num_of_item;
	$post_data['product_name'] = $products;
	$post_data['product_profile'] = 'physical-goods';
    
    $total=0;
	$cart = array();
	foreach($product_info as $request) {
			$cart_add['product'] = $request->productalbum_name;
			$cart_add['amount'] =number_format($request->product_price, 2);
			$total+=$request->product_price*$request->shoppinproduct_quantity;
			array_push($cart, $cart_add);
        }
	$post_data['cart']=json_encode($cart);
	//discount product
	$discount_product=array();
	foreach($orderinfo as $request) {
//	array_push($post_data,"product_amount",$request->product_price + 0);
//	array_push($post_data,"discount_amount",0);
	}
	//dd($post_data);
	$post_data['vat'] = "0";
	$post_data['convenience_fee'] = "0";
    
	# SHIPMENT INFORMATION
	$post_data['ship_name'] = $orderinfo->Shipping_txtfirstname.''.$orderinfo->Shipping_txtlastname;
	$post_data['ship_add1 '] = $orderinfo->Shipping_txtaddressname;
	$post_data['ship_add2'] = "";
	$post_data['ship_city'] = $city_info->name;
	$post_data['ship_state'] = "";
	$post_data['ship_postcode'] = $orderinfo->Shipping_txtzipcode;
	$post_data['ship_country'] = "Bangladesh";

	
	# OPTIONAL PARAMETERS
	$post_data['value_a'] = "";
	$post_data['value_b '] = "";
	$post_data['value_c'] = "";
	$post_data['value_d'] = "";




	//$post_data['allowed_bin'] = "3,4";
	//$post_data['allowed_bin'] = "470661";
	//$post_data['allowed_bin'] = "470661,376947";


	# REQUEST SEND TO SSLCOMMERZ
// 	$direct_api_url = "https://sandbox.sslcommerz.com/gwprocess/v4/api.php";	#For test environment
	$direct_api_url = "https://securepay.sslcommerz.com/gwprocess/v4/api.php";	#For live environment

	$handle = curl_init();
	curl_setopt($handle, CURLOPT_URL, $direct_api_url );
	curl_setopt($handle, CURLOPT_TIMEOUT, 30);
	curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($handle, CURLOPT_POST, 1 );
	curl_setopt($handle, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE); # KEEP IT FALSE IF YOU RUN FROM LOCAL PC


	$content = curl_exec($handle );

	$code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

	if($code == 200 && !( curl_errno($handle))) {
	    curl_close( $handle);
	    $sslcommerzResponse = $content;
	} else {
	    curl_close( $handle);
	    echo "FAILED TO CONNECT WITH SSLCOMMERZ API";
	    exit;
	}

	# PARSE THE JSON RESPONSE
	$sslcz = json_decode($sslcommerzResponse, true );
	//echo "<pre>";
    
	//var_dump($sslcz); exit;

	// $sessionkey = $sslcz['sessionkey'];

	if(isset($sslcz['GatewayPageURL']) && $sslcz['GatewayPageURL']!="") {
		// this is important to show the popup, return or echo to sent json response back
		 echo json_encode(['status' => 'SUCCESS', 'data' => $sslcz['GatewayPageURL'], 'logo' => $sslcz['storeLogo'] ]);	// For Live
	//	echo json_encode(['status' => 'success', 'data' => $sslcz['GatewayPageURL'], 'logo' => $sslcz['storeLogo'] ]);		// For Test
		exit;
	   //return json_encode(['status' => 'SUCCESS', 'data' => $sslcz['GatewayPageURL'], 'logo' => $sslcz['storeLogo'] ]);
	} else {
	   echo json_encode(['status' => 'FAILED', 'data' => null, 'message' => "JSON Data parsing error!"]);
	}
    }
    
    public function ssl_success($request){
        $order=Conforder::find($request->tran_id);
        $amount_info=Shoppingcart::find($order->shoppingcart_id);
		$total_amount = $amount_info->shoppingcart_total;
		$currency='BDT';
		$validation = $this->orderValidate($request,$total_amount,$currency);
		
		if($validation == TRUE) 
		{
		  return $request->tran_id;
		}
		else
		{
			return false;
		}
	}
	
    public function ssl_ipn($request){
		if($request->input('tran_id')){
		    $tran_id = $request->input('tran_id');
		    $order=Conforder::find($tran_id);
            $amount_info=Shoppingcart::find($order->shoppingcart_id);
		    $total_amount = $amount_info->shoppingcart_total;
    		$currency='BDT';
    		$order_details=Conforder::find($tran_id);
    		if($order_details->conforder_status =='Order_Verification_Pending'){
    		    $validation = $this->orderValidate($request,$total_amount,$currency);
    		    if($validation == TRUE) 
                {
                    $conforeder = Conforder::find($tran_id);
                    $conforeder->conforder_status = 'Processing';
                    $conforeder->save();
                }
    		}else if($order_details->conforder_status == 'Processing'){
    		    
    		}else{
    		    
    		}
		}else{
		    echo "Inavalid Data";
		}
	}
	
	public function orderValidate($request,$total_amount,$currency){
		$val_id=urlencode($request->val_id);
		
		//live account
	    $store_id=urlencode("PrideLimitedlive");
		$store_passwd=urlencode("5CD294FBBE6D655576");
		//test account
// 		$store_id=urlencode("testbox");
// 		$store_passwd=urlencode("qwerty");
// 		$requested_url = ("https://sandbox.sslcommerz.com/validator/api/validationserverAPI.php?val_id=".$val_id."&store_id=".$store_id."&store_passwd=".$store_passwd."&v=1&format=json"); // For Test
		$requested_url = ("https://securepay.sslcommerz.com/validator/api/validationserverAPI.php?val_id=".$val_id."&store_id=".$store_id."&store_passwd=".$store_passwd."&v=1&format=json"); // For Live

		$handle = curl_init();
		curl_setopt($handle, CURLOPT_URL, $requested_url);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false); # IF YOU RUN FROM LOCAL PC
		curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); # IF YOU RUN FROM LOCAL PC

		$result = curl_exec($handle);

		$code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

		if($code == 200 && !( curl_errno($handle)))
		{

			# TO CONVERT AS ARRAY
			# $result = json_decode($result, true);
			# $status = $result['status'];

			# TO CONVERT AS OBJECT
			$result = json_decode($result);

			# TRANSACTION INFO
			$status = $result->status;
			$tran_date = $result->tran_date;
			$tran_id = $result->tran_id;
			$val_id = $result->val_id;
			$amount = $result->amount;
			$store_amount = $result->store_amount;
			$bank_tran_id = $result->bank_tran_id;
			$card_type = $result->card_type;
			$currency_type = $result->currency_type;
            $currency_amount = $result->currency_amount;

			# EMI INFO
			$emi_instalment = $result->emi_instalment;
			$emi_amount = $result->emi_amount;
			$emi_description = $result->emi_description;
			$emi_issuer = $result->emi_issuer;

			# ISSUER INFO
			$card_no = $result->card_no;
			$card_issuer = $result->card_issuer;
			$card_brand = $result->card_brand;
			$card_issuer_country = $result->card_issuer_country;
			$card_issuer_country_code = $result->card_issuer_country_code;
            
            # DISCOUNT INFO
			$discount_amount = 0;
			$discount_percentage = 0;
			$discount_remarks = 'NIA';
			
			# API AUTHENTICATION
			$APIConnect = $result->APIConnect;
			$validated_on = $result->validated_on;
			$gw_version = $result->gw_version;
			if ($status == "VALID" || $status == "VALIDATED") {
				if ($currency == "BDT") {
					if (trim($request->tran_id) == trim($tran_id) && (abs($total_amount - $amount) < 1) && trim($currency) == trim('BDT')) {
					        $payment=new Sslpayment();
					        $payment->conforder_id=$tran_id;
					        $payment->bank_tran_id=$bank_tran_id;
					        $payment->total_amount=$amount;
					        $payment->store_amount=$store_amount;
                            $payment->card_type=$card_type;
                            $payment->tran_date=$tran_date;
                            $payment->val_id=$val_id;
                            $payment->card_issuer=$card_issuer;
                            $payment->discount_amount=$discount_amount;
                            $payment->discount_percentage=$discount_percentage;
                            $payment->discount_remarks=$discount_remarks;
                            $payment->save();
							return true;
						} else {
							return false;
						}
				} else {
					if (trim($request->tran_id) == trim($tran_id) && (abs($total_amount - $currency_amount) < 1) && trim($currency) == trim($currency_type)) {
						return true;
					} else {
						return false;
					}
				}
			}else{
				return false;
			}
            
		} else {
			return false;
		}
	}
}