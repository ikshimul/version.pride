<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Auth;

class AdminAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (Auth::guard('admin')->check()) {
            $admin_login_id =Auth::guard('admin')->user()->id;
            return $next($request);
        }else{
            $login='admin.login';
            return redirect()->route($login);
        }
    }

}
