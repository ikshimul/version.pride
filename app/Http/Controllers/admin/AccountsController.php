<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Image;
use Illuminate\Support\Facades\Storage;
use App\Models\Conforder;
use App\Models\Cashexpenses;
use App\Models\Cashtransaction;
use App\Models\Rider;
use DB;
use Carbon\Carbon;
use PDF;
use Auth;

class AccountsController extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public function index(){
        //$data['list']=Cashtransaction::with('conforder.cashexpenses','conforder.rider','conforder.shoppingcart','conforder.ordershippings')->limit(10)->get();
        $data['cash_history']=DB::select(DB::raw("SELECT cashtransactions.transaction_type,cashtransactions.description,cashtransactions.created_at,cashtransactions.transfer_mode,conforders.conforder_deliverydate,conforders.conforder_tracknumber,conforders.id,ordershippings.Shipping_txtfirstname,ordershippings.Shipping_txtlastname,ordershippings.Shipping_txtaddressname,riders.name as delivery_by,cashtransactions.amount as transation_amount,cashexpenses.amount as expense_amount,admins.name as admin_username,riders.id as rider_id,shoppingcarts.payment_method
            FROM cashtransactions
            LEFT JOIN cashexpenses
            ON cashtransactions.conforder_id=cashexpenses.conforder_id
            LEFT JOIN conforders
            ON cashtransactions.conforder_id=conforders.id
            LEFT JOIN ordershippings
            ON conforders.id=ordershippings.conforder_id
            LEFT JOIN shoppingcarts
            ON conforders.shoppingcart_id=shoppingcarts.id
            LEFT JOIN riders
            ON conforders.delivery_by=riders.id
            LEFT JOIN admins
            on cashtransactions.issued_by=admins.id WHERE cashtransactions.created_at >'2021-09-09'
            order BY cashtransactions.id ASC"));
        return view('admin.account.cashbook', $data);
    }
    
    public function reportpdf(){
        $cash_history=DB::select(DB::raw("SELECT cashtransactions.transaction_type,cashtransactions.description,cashtransactions.created_at,cashtransactions.transfer_mode,conforders.conforder_deliverydate,conforders.conforder_tracknumber,conforders.id,ordershippings.Shipping_txtfirstname,ordershippings.Shipping_txtlastname,ordershippings.Shipping_txtaddressname,riders.name as delivery_by,cashtransactions.amount as transation_amount,cashexpenses.amount as expense_amount,admins.name as admin_username,riders.id as rider_id,shoppingcarts.payment_method
            FROM cashtransactions
            LEFT JOIN cashexpenses
            ON cashtransactions.conforder_id=cashexpenses.conforder_id
            LEFT JOIN conforders
            ON cashtransactions.conforder_id=conforders.id
            LEFT JOIN ordershippings
            ON conforders.id=ordershippings.conforder_id
            LEFT JOIN shoppingcarts
            ON conforders.shoppingcart_id=shoppingcarts.id
            LEFT JOIN riders
            ON conforders.delivery_by=riders.id
            LEFT JOIN admins
            on cashtransactions.issued_by=admins.id WHERE cashtransactions.created_at >'2021-09-09'
            order BY cashtransactions.id ASC"));
        $pdf = PDF::loadView('admin.account.cashPDF', compact('cash_history'));
        $current = Carbon::now()->format('YmdHs').'- cashbook-report.pdf';
        return $pdf->download($current);
    }
    
    public function cashTransfer(){
        $data['cash_history']=DB::select(DB::raw("SELECT cashtransactions.transaction_type,cashtransactions.description,cashtransactions.created_at,cashtransactions.transfer_mode,conforders.conforder_deliverydate,conforders.conforder_tracknumber,conforders.id,ordershippings.Shipping_txtfirstname,ordershippings.Shipping_txtlastname,ordershippings.Shipping_txtaddressname,riders.name as delivery_by,cashtransactions.amount as transation_amount,cashexpenses.amount as expense_amount,admins.name as admin_username,riders.id as rider_id,shoppingcarts.payment_method
            FROM cashtransactions
            LEFT JOIN cashexpenses
            ON cashtransactions.conforder_id=cashexpenses.conforder_id
            LEFT JOIN conforders
            ON cashtransactions.conforder_id=conforders.id
            LEFT JOIN ordershippings
            ON conforders.id=ordershippings.conforder_id
            LEFT JOIN shoppingcarts
            ON conforders.shoppingcart_id=shoppingcarts.id
            LEFT JOIN riders
            ON conforders.delivery_by=riders.id
            LEFT JOIN admins
            on cashtransactions.issued_by=admins.id WHERE cashtransactions.created_at >'2021-09-09'
            order BY cashtransactions.id ASC"));
        return view('admin.account.cash-transfer',$data);
    }
    
    public function cashTransfersave(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'amount' => 'required',
                    'transfer_to' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            if ($request->hasFile('voucher_image')) {
                $file = $request->file('voucher_image');
                $imagesavename = time() . '-' . $file->getClientOriginalName();
                $file->move('storage/app/public/accounts/', $imagesavename);
            }else{
               $imagesavename=''; 
            }
            $cash=new Cashtransaction();
            $cash->amount=$request->amount;
            $cash->transaction_type='cash_out';
            $cash->transfer_to=$request->transfer_to;
            $cash->description=$request->description;
            $cash->transfer_mode='cash';
            $cash->issued_by=Auth::guard('admin')->user()->id;
            $cash->save();
            return redirect()->back()->with('save', 'Cash transfer successfully done.');
        }
    }
}
