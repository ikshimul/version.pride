<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Image;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use App\Models\Slider;
use App\Models\Banner;
use App\Models\Popup;

class ContentHandleController extends Controller
{
     public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public function slidercreate(){
        return view('admin.slider.create');
    }
    
    protected function sliderstore(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'slider_caption' => 'required',
                    'slider_occasion' => 'required',
                    'slider_order' => 'required',
                    'slider_image' => 'required',
                    'device' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            if ($request->hasFile('slider_image')) {
                $width = Image::make($request->file('slider_image'))->width();
                $height = Image::make($request->file('slider_image'))->height();
                if (($width == 1366 && $height == 600 && $request->device == 'desktop') || ($width == 550 && $height == 602 && $request->device == 'mobile')) {
                    $file = $request->file('slider_image');
                    $filename = time(). "_" . $file->getClientOriginalName();
                    $image = Image::make($request->file('slider_image'))->save('storage/app/public/slider/' . $filename);
                    $slider=new Slider();
                    $slider->slider_caption=$request->slider_caption;
                    $slider->slider_occasion=$request->slider_occasion;
                    $slider->slider_order=$request->slider_order;
                    $slider->slider_image=$filename;
                    $slider->url_link=$request->url_link;
                    $slider->device=$request->device;
                    $slider->created_ip=$request->ip();
                    $slider->created_by=Auth::guard('admin')->user()->id;
                    $result=$slider->save();
                    if($result) {
                        return redirect()->back()->with('save', 'New slider image added.');
                    } else {
                        return redirect()->back()->with('error', 'Data not added.'); 
                    }
                } else {
                   return redirect()->back()->with('error', "Image size not matched.Image size must be Width: 1366px X Height: 600px.New image size Width: $width px X Height: $height px");  
                }
            } else {
               return redirect()->back()->with('error', 'Please select an image.'); 
            }
        }
    }
    
    public function sliderlist(){
        $data['sliders']=Slider::all();
        return view('admin.slider.list',$data);
    }
    
    protected function slideredit(Request $request){
        $data['slider']=Slider::find($request->id);
        return view('admin.slider.edit',$data);
    }
    
    protected function sliderupdate(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'slider_caption' => 'required',
                    'slider_occasion' => 'required',
                    'slider_order' => 'required',
                    'device' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $slider=Slider::find($request->id);
            if ($request->hasFile('slider_image')) {
                $width = Image::make($request->file('slider_image'))->width();
                $height = Image::make($request->file('slider_image'))->height();
                if (($width == 1366 && $height == 600 && $request->device == 'desktop') || ($width == 550 && $height == 602 && $request->device == 'mobile')) {
                    $file = $request->file('slider_image');
                    $filename = time(). "_" . $file->getClientOriginalName();
                    $image = Image::make($request->file('slider_image'))->save('storage/app/public/slider/' . $filename);
                    Storage::delete("public/slider/$slider->slider_image");
                    $slider->slider_image=$filename;
                }else{
                    return redirect()->back()->with('error', "Image size not matched.Image size must be Width: 1366px X Height: 600px.New image size Width: $width px X Height: $height px");
                }
            }
            $slider->slider_caption=$request->slider_caption;
            $slider->slider_occasion=$request->slider_occasion;
            $slider->slider_order=$request->slider_order;
            $slider->url_link=$request->url_link;
             $slider->device=$request->device;
            $slider->created_ip=$request->ip();
            $slider->created_by=Auth::guard('admin')->user()->id;
            $result=$slider->save();
            return redirect()->back()->with('save', 'Slider updated.');
        }
    }
    
    protected function slideractive(Request $request){
        $slider=Slider::find($request->id);
        $slider->active=1;
        $slider->save();
        return redirect()->back()->with('update', 'Slider actived.');
    }
    
    protected function sliderdeactive(Request $request){
        $slider=Slider::find($request->id);
        $slider->active=0;
        $slider->save();
        return redirect()->back()->with('error', 'Slider deactived.');
    }
    
    protected function sliderdelete(Request $request){
        $slider=Slider::find($request->id);
        Storage::delete("public/slider/$slider->slider_image");
        $slider->delete();
        return redirect()->back()->with('error', 'Slider deleted.');
    }
    
    public function bannercreate(){
        return view('admin.banner.create');
    }
    
    protected function bannerstore(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'banner_title' => 'required',
                    'banner_pos' => 'required',
                    'banner_order' => 'required',
                    'banner_image' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            if ($request->hasFile('banner_image')) {
                $width = Image::make($request->file('banner_image'))->width();
                $height = Image::make($request->file('banner_image'))->height();
                if ($width >= 380 && $height >= 507) {
                $file = $request->file('banner_image');
                $filename = time(). "_" . $file->getClientOriginalName();
                $image = Image::make($request->file('banner_image'))->save('storage/app/public/banner/' . $filename);
                $banner=new Banner();
                $banner->banner_title=$request->banner_title;
                $banner->banner_pos=$request->banner_pos;
                $banner->banner_order=$request->banner_order;
                $banner->banner_image=$filename;
                $banner->banner_link=$request->banner_link;
                $banner->created_by=Auth::guard('admin')->user()->id;
                $banner->created_ip=$request->ip();
                $result=$banner->save();
                if($result){
                     return redirect()->back()->with('save', 'New banner image added.');
                }else{
                    return redirect()->back()->with('error', 'Data not added.'); 
                }
            }else{
                return redirect()->back()->with('error', "Image size not matched.Image size must be Width: 380px X Height: 507px.New image size Width: $width px X Height: $height px");
            }
        }else{
           return redirect()->back()->with('error', 'Please select an image.'); 
        }
        }
    }
    
    public function bannerlist(){
        $data['banners']=Banner::all();
        return view('admin.banner.list',$data);
    }
    
    protected function banneredit(Request $request){
        $data['banner']=Banner::find($request->id);
        return view('admin.banner.edit',$data);
    }
    
    protected function bannerupdate(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'banner_title' => 'required',
                    'banner_pos' => 'required',
                    'banner_order' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $banner=Banner::find($request->id);
            if ($request->hasFile('banner_image')) {
                $width = Image::make($request->file('banner_image'))->width();
                $height = Image::make($request->file('banner_image'))->height();
                if ($width >= 380 && $height >= 507) {
                    $file = $request->file('banner_image');
                    $filename = time(). "_" . $file->getClientOriginalName();
                    $image = Image::make($request->file('banner_image'))->save('storage/app/public/banner/' . $filename);
                    Storage::delete("public/banner/$banner->banner_image");
                    $banner->banner_image=$filename;
                }else{
                   return redirect()->back()->with('error', "Image size not matched.Image size must be Width: 380px X Height: 507px.New image size Width: $width px X Height: $height px"); 
                }
                
            }
            $banner->banner_title=$request->banner_title;
            $banner->banner_pos=$request->banner_pos;
            $banner->banner_order=$request->banner_order;
            $banner->banner_link=$request->banner_link;
            $banner->created_by=Auth::guard('admin')->user()->id;
            $banner->created_ip=$request->ip();
            $result=$banner->save();
            return redirect()->back()->with('save', 'Banner updated.');
        }
    }
    
    protected function banneractive($id){
        $banner=Banner::find($id);
        $banner->active=1;
        $banner->save();
        return redirect()->back()->with('update', 'Banner actived.');
    }
    
    protected function bannerdeactive($id){
        $banner=Banner::find($id);
        $banner->active=0;
        $banner->save();
        return redirect()->back()->with('error', 'Banner deactived.');
    }
    
    protected function bannerdelete(Request $request){
        $banner=Banner::find($request->id);
        Storage::delete("public/banner/$banner->banner_image");
        $banner->delete();
        return redirect()->back()->with('error', 'Banner deleted.');
    }
    
    protected function popupcreate(){
        return view('admin.popup.create');
    }
    
    protected function popupstore(Request $request){
        $vaildation = Validator::make($request->all(), [
            'active' => 'required',
            'image' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
        if ($request->hasFile('image')) {
            $width = Image::make($request->file('image'))->width();
            $height = Image::make($request->file('image'))->height();
            if ($width >= 450 && $height >= 570) {
                $file = $request->file('image');
                $filename = time(). "_" . $file->getClientOriginalName();
                Storage::put('/public/popup/' . $filename, file_get_contents($request->file('image')->getRealPath()));
                if($request->active=='on'){
                    $active=1;
                }else{
                  $active=0;  
                }
                $popup=new Popup();
                $popup->title=$request->title;
                $popup->image=$filename;
                $popup->link=$request->link;
                $popup->created_by=Auth::guard('admin')->user()->id;
                $popup->created_ip=$request->ip();
                $popup->active=$active;
                $popup->save();
                return redirect()->back()->with('save', 'New popup image added.');
            }else{
                return redirect()->back()->with('error', "Image size not matched.Image size must be Width: 450px X Height: 570px.New image size Width: $width px X Height: $height px");
            }
        }else{
        return redirect()->back()->with('error', 'Please select an image.'); 
        }
      }
    }
    
    protected function popuplist(Request $request){
       $data['popups']=Popup::all();
      return view('admin.popup.list',$data);
    }
    
    protected function popupedit(Request $request){
        $data['popup']=Popup::find($request->id);
        return view('admin.popup.edit',$data);
    }
    
    protected function popupupdate(Request $request){
        $vaildation = Validator::make($request->all(), [
           // 'active' => 'required',
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
             $popup=Popup::find($request->id);
            if ($request->hasFile('image')) {
                $width = Image::make($request->file('image'))->width();
                $height = Image::make($request->file('image'))->height();
                if ($width >= 450 && $height >= 570) {
                    $file = $request->file('image');
                    $filename = time(). "_" . $file->getClientOriginalName();
                    Storage::put('/public/popup/' . $filename, file_get_contents($request->file('image')->getRealPath())); 
                    Storage::delete("public/popup/$popup->image");
                    $popup->image=$filename;
                }else{
                    return redirect()->back()->with('error', "Image size not matched.Image size must be Width: 450px X Height: 570px.New image size Width: $width px X Height: $height px");
                }
            }
            if($request->active=='on'){
                    $active=1;
                }else{
                  $active=0;  
                }
            $popup->title=$request->title;
            $popup->link=$request->link;
            $popup->created_by=Auth::guard('admin')->user()->id;
            $popup->created_ip=$request->ip();
            $popup->active=$active;
            $popup->save();
            return redirect()->back()->with('save', 'Update successfully.');
        }
    }
    
    protected function popupactive(Request $request){
        $popup=Popup::find($request->id);
        $popup->active=1;
        $popup->save();
        return redirect()->back()->with('update', 'Popup actived.');
    }
    
    protected function popupdeactive(Request $request){
        $popup=Popup::find($request->id);
        $popup->active=0;
        $popup->save();
        return redirect()->back()->with('update', 'Popup deactived.');
    }
    
    protected function popupdelete(Request $request){
        $popup=Popup::find($request->id);
        Storage::delete("public/popup/$popup->image");
        $popup->delete();
        return redirect()->back()->with('error', 'Popup deleted.');
    }
}
