<?php

namespace App\Http\Controllers\admin\pos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;
use Carbon\Carbon;
use Auth;
use App\Models\Positem;
use App\Models\Product;
use App\Models\Maincat;
use App\Models\Procat;
use App\Models\Subprocat;
use App\Models\Productsize;
use App\Models\Productalbum;
use App\Models\Productimg;
use App\Models\Campaign;
use App\Models\Fabric;

class Manageproduct extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public static function GetSizeBarcode($style_code, $color) {
        return Positem::where('DesignRef', $style_code)->where('color', $color)->where('add_status', 0)->get();
    }
    
    public function AddPosItemList() {
        $item_list = DB::connection('sqlsrv')
                ->table('itemdefination')
                ->join('itemeffectiverate', 'itemdefination.barcode', '=', 'itemeffectiverate.barcode')
                ->join('itemwisestock', 'itemdefination.barcode', '=', 'itemwisestock.barcode')
                ->where('itemwisestock.CurrentStock', '>', 0)
                ->where('itemdefination.Size', '!=', null)
                ->where('itemdefination.Color', '!=', null)
                ->where('itemdefination.Brand', 'Pride Limited')
               // ->where('itemdefination.DesignRef','PKG-ETUWPROP07126KF')
                ->get();
        //dd($item_list);
        foreach ($item_list as $items) {
            $check_duplicate = Positem::where('Barcode', $items->Barcode)->where('DesignRef', $items->DesignRef)->where('Color', $items->Color)->where('Size', $items->Size)->first();
            if ($check_duplicate === null) {
                $item=new Positem();
                $item->Barcode= $items->Barcode;
                $item->ItemMasterID = $items->ItemMasterID;
                $item->ItemDescription = $items->ItemDescription;
                $item->Category = $items->Category;
                $item->subcategory = $items->subcategory;
                $item->itemGroup = $items->itemGroup;
                $item->DesignRef = $items->DesignRef;
                $item->Department = $items->Department;
                $item->Color = $items->Color;
                $item->Size = $items->Size;
                $item->SellRate = $items->SellRate;
                $item->CurrentStock = $items->CurrentStock;
                $item->Brand = $items->Brand;
                $item->save();
               // DB::table('item_define')->insert($data);
            }
            $add_check = Product::where('product_styleref', $items->DesignRef)->first();
            if ($add_check !== null) {
                $check=Productsize::where('product_id', $add_check->id)->first();
				if($check){
                $product_barcodes = Productsize::where('product_id', $add_check->id)->where('color_name', $items->Color)->where('barcode','!=','')->pluck('barcode')->toArray();
                if (count($product_barcodes) > 0 && !in_array($items->Barcode, $product_barcodes)) {
                    $productsize=new Productsize();
                    $productsize->product_id = $add_check->id;
    				$productsize->barcode= $items->Barcode;
    				$productsize->productsize_size= $items->Size;
    				$productsize->color_name = $items->Color;
    				$productsize->SizeWiseQty = $items->CurrentStock;
    				$productsize->save();
                    //Update status
                    $add_status_update['add_status'] = 1;
                    Positem::where('Barcode', $items->Barcode)->update($add_status_update);
                 }
				}else{
				$productsize=new Productsize();
				$productsize->product_id = $add_check->id;
				$productsize->barcode= $items->Barcode;
				$productsize->productsize_size= $items->Size;
				$productsize->color_name = $items->Color;
				$productsize->SizeWiseQty = $items->CurrentStock;
				$productsize->save();
				//DB::table('productsize')->insert($added_stock);
				//Update status
				$add_status_update['add_status'] = 1;
				Positem::where('Barcode', $items->Barcode)->update($add_status_update);
			  } 
                
            }
            $added_update['add_status'] = 1;
         //   DB::table('item_define')->where('Barcode', $item->Barcode)->update($added_update);
            $live_stock['SizeWiseQty'] = $items->CurrentStock;
            $data_stock['CurrentStock'] = $items->CurrentStock;
            Productsize::where('barcode', $items->Barcode)->where('productsize_size',$items->Size)->update($live_stock);
            Positem::where('Barcode', $items->Barcode)->update($data_stock);
            $price_update['product_price'] = $items->SellRate;
         //   DB::table('product')->where('product_styleref', $item->DesignRef)->update($price_update); 
        }
        echo "all data sync";
    }
    
    public function create(Request $request){
        $stylecode =$request->stylecode;
        $data['procat_list'] = Procat::all();
        $data['total_product'] = Product::count();
        $data['maincats'] = Maincat::all();
        $data['campaigns'] = Campaign::where('status',0)->orderBy('id','DESC')->get();
         $data['fabrics'] = Fabric::where('active',1)->orderBy('id','DESC')->get();
        $data['unique_style'] = Positem::where('add_status', 0)->groupBy('DesignRef')->orderBy('id', 'DESC')->get();
        $data['item_define'] = Positem::where('DesignRef', $stylecode)->first();
        $data['item_color'] = Positem::where('DesignRef', $stylecode)->where('add_status', 0)->groupBY('color')->get();
        $data['stylecode'] = $stylecode;
        $stye_check = DB::table('products')
		->join('cats','products.cat','=','cats.id')
		->join('subcats','products.subcat','=','subcats.id')
		->select('cats.name as procat_name','subcats.name as subprocat_name','products.product_name','products.product_price','products.discount_product_price','products.product_description','products.product_img_thm')
		->where('product_styleref', $stylecode)
		->where('product_active_deactive', 0)
		->first();
		if($stye_check){
			$data['old_item']=$stye_check;
		}else{
			$data['old_item']=null;
		}
        return view('admin.product.pos.add', $data);
    }
    
    
    public function store(Request $request){
        //dd($request->txtstyleref);
        $totalnumberofinserteddata = 0;
        $numberofboxedchecked = 0;
        $current_date = date("d-m-Y");
        $created_at = Carbon::now();
        $style_code = $request->txtstyleref;
        $stye_check = Product::where('product_styleref', $style_code)->where('product_active_deactive', 0)->first();
        if ($stye_check) {
            $product_id = $stye_check->id;
        } else {
            $vaildation = Validator::make($request->all(), [
                        'txtproductname' => 'required|min:4',
                        'txtprice' => 'required',
                            // 'txtstyleref' => 'required|unique:product,product_styleref',
            ]);
            if ($vaildation->fails()) {
                return redirect()->back()->withErrors($vaildation)->withInput();
            } else {
                $discount = $request->txtpricediscounted;
                $original_price = $request->txtprice;
                if ($discount >= 1) {
                    $discount_price = ($discount / 100) * $original_price;
                    //  $discount_price = $original_price - $discount;
                    //  $discount_price = ($discount_price * 100) / $original_price;
                    $price_with_discount = $original_price - $discount_price;
                } else {
                    $discount_price = 0;
                    $price_with_discount = 0;
                }
                if ($request->campaign) {
                    $specialcollection = $request->campaign;
                } else {
                    $specialcollection = 0;
                }
                if ($request->has('occassion')) {
                    $occassion = $request->occassion;
                } else {
                    $occassion = 0;
                }
                $product=new Product();
                $product->maincat=$request->maincat;
                $product->cat=$request->cat;
                $product->subcat=$request->subcat;
                $product->product_name = $request->txtproductname;
                $product->fabric = $request->fabric;
                $product->product_price = $request->txtprice;
                $product->discount_product_price = $request->txtpricediscounted;
                $product->product_pricediscounted= $price_with_discount;
                $product->product_pricefilter = $request->ddlfilter;
                $product->product_styleref = $request->txtstyleref;
                $product->product_description = $request->txtproductdetails;
                $product->product_care = $request->txtproductcare;
                $product->created_by= Auth::guard('admin')->user()->id;
                $product->isSpecial = $specialcollection;
                $product->spcollection=$occassion;
                $product->sold=$request->txtorder;
                $product->save();
                $product_id = $product->id;
                //dd($product_id);
                //$sold['sold'] = $product_id;
                //DB::table('product')->where('product_id', $product_id)->update($sold);
            }
        }
        $total_grp = $request->total_grp;
        for ($loopcountermain = 1; $loopcountermain <= $total_grp; $loopcountermain++) {
            $txtcolorvalname = "txtcolorname_" . $loopcountermain;
            $color_name_post = "txtcolorname_" . $loopcountermain;
            $txtcolovalue = $request->$color_name_post;
            $colorfilethm = "file_colorthm_" . $loopcountermain;
            //check click or not
            if ($request->hasFile($colorfilethm)) {
                $totalnumberofavailabesize = session('numberofavailabesize');
                for ($loopcountersize = 1; $loopcountersize <= $totalnumberofavailabesize; $loopcountersize++) {
                    $checkboxvarname = "size" . $loopcountersize . "_" . $loopcountermain;
                    $checkboxvarvalename = "input_size" . $loopcountersize . "_" . $loopcountermain;
                    $checkboxvarvalename_barcode = "barcode_input_size" . $loopcountersize . "_" . $loopcountermain;
                    if ($request->$checkboxvarname == TRUE) {
                        $productsizaname = $request->$checkboxvarname;
                        $productsizquantity = $request->$checkboxvarvalename;
                        $barcode = $request->$checkboxvarvalename_barcode;
                        $numberofboxedchecked++;
                        if ($productsizquantity > 0) {
                            
                        } else {
                            $productsizquantity = 0;
                        }
                        $productsize=new Productsize();
                        $productsize->product_id = $product_id;
                        $productsize->barcode= $barcode;
                        $productsize->productsize_size= $productsizaname;
                        $productsize->SizeWiseQty = $productsizquantity;
                        $productsize->color_name = $txtcolovalue;
                        $productsize->save();
                        $productsize_id = $productsize->id;
                        //$add_status_update['add_status'] = 1;
                        Positem::where('Barcode', $barcode)->update(['add_status'=>1]);
                        if ($productsize_id) {
                            $totalnumberofinserteddata++;
                        } else {
                            
                        }
                    } else {
                        
                    }
                }
                if ($totalnumberofinserteddata == $numberofboxedchecked) {
                    $productalbum=new Productalbum();
                    $productalbum->product_id=$product_id;
                    $productalbum->productalbum_name=$txtcolovalue;
                    $productalbum->productalbum_order=$loopcountermain;
                    $productalbum->save();
                    $productalbum_id =$productalbum->id;
                    if ($productalbum_id) {
                        if ($request->hasFile($colorfilethm)) {
                            $file = $request->file($colorfilethm);
                            $savename = $productalbum_id . '_album_thm_' . $file->getClientOriginalName();
                            $image = Image::make($request->file($colorfilethm))->save('storage/app/public/pgallery/' . $savename);
                            if ($image) {
                                Productalbum::where('id', $productalbum_id)->update(['productalbum_img'=>$savename]);
                                for ($filecounter = 1; $filecounter <= 6; $filecounter++) {
                                    $imageid = NULL;
                                    $imgfieldname = "file_im" . $filecounter . "_" . $loopcountermain;
                                    if ($request->hasFile($imgfieldname)) {
                                        $file = $request->file($imgfieldname);
                                        $filename = $product_id . "_product_image_" . $filecounter . "_" . $file->getClientOriginalName();
                                        $image = Image::make($request->file($imgfieldname))->save('storage/app/public/pgallery/' . $filename);
                                        $productimg=new Productimg();
                                        $productimg->productalbum_id=$productalbum_id;
                                        $productimg->productimg_order='';
                                        $productimg->save();
                                        $productimg_id = $productimg->id;
                                        if ($image) {
                                            $width = Image::make($request->file($imgfieldname))->width();
                                            $height = Image::make($request->file($imgfieldname))->height();
                                            if ($width >= 1590 && $width <= 1610 && $height >= 2390 && $height <= 2410) {
                                                //upload thm file
                                                $filenamethmb = $product_id . "_product_image_" . $filecounter . "_thm_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(200, 300)->save('storage/app/public/pgallery/' . $filenamethmb);
                                                //upload tiny file
                                                $filenametiny = $product_id . "_product_image_" . $filecounter . "_tiny_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(64, 96)->save('storage/app/public/pgallery/' . $filenametiny);
                                                //upload medium file
                                                $filenamemedium = $product_id . "_product_image_" . $filecounter . "_medium_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(370, 555)->save('storage/app/public/pgallery/' . $filenamemedium);
                                                Productimg::where('id', $productimg_id)->update(['productimg_img'=>$filename,'productimg_img_tiny'=>$filenametiny,'productimg_img_medium'=>$filenamemedium,'productimg_img_thm'=>$filenamethmb,'productimg_order'=>$filecounter]);
                                                if ($filecounter == 1 && $loopcountermain == 1) {
                                                    Product::where('id',$product_id)->update(['product_img_thm'=>$filenamemedium]);
                                                } 
                                            } else {
                                                $get_productimg =Productimg::find('id', $productimg_id);
												Storage::delete(public_path()."/pgallery/$get_productimg->productimg_img_tiny");
												Storage::delete(public_path()."/pgallery/$get_productimg->productimg_img_medium");
												Storage::delete(public_path()."/pgallery/$get_productimg->productimg_img_thm");
												Storage::delete(public_path()."/pgallery/$get_productimg->productimg_img");
												$proimg =Productimg::find($productimg_id);
                                                $proimg->delete();
                                                $proalbum=Productalbum($productalbum_id);
                                                $proalbum->delete();
												$get_barcode=Productsize::where('product_id', $product_id)->where('color_name', $txtcolovalue)->get();
												foreach($get_barcode as $barcode){
												   Positem::where('Barcode', $barcode->barcode)->update(['add_status'=>0]);
												}
												Productsize::where('product_id', $product_id)->where('color_name', $txtcolovalue)->delete();
												if ($stye_check) {
													
												}else{
                                                Product::where('product_id', $product_id)->delete();
												}
                                                // DB::table('product')->where('product_id',$product_id)->delete();
                                                toastr()->error('upload size must be greater than or equal 1600*2400');
                                                return redirect()->back()->with('error', 'upload size must be greater than or equal 1600*2400');
                                            }
                                        }
                                    }
                                }
                            }
                        } 
                    } 
                } 
            }
        }
        toastr()->success('Product Upload Successfully!');
        return redirect()->back()->with('save', 'Product Upload Successfully!');
    }
    
}
