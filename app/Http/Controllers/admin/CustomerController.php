<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Userdetail;
use App\Models\Region;
use App\Models\Phnumber;
use DB;
use Validator;
use Response;
use Mail;
use App\Notifications\SendEmail;
use Notification;

class CustomerController extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public function list(){
       // $data['users']=User::with('userdetails')->orderBy('id', 'DESC')->get();
        $data['users']=DB::table('users')->leftjoin('userdetails','users.id','=','userdetails.user_id')->select('users.id','users.name','users.mobile_no','users.email','users.active','userdetails.address','userdetails.phone')->orderBy('users.id', 'DESC')->get();
        return view('admin.customer.list',$data);
    }
    
    public function edit(Request $request){
        $data['regions']=Region::select('id','name')->get();
        //$data['user']=User::with('userdetails')->find($request->id);
        $data['user']=DB::table('users')->leftjoin('userdetails','users.id','=','userdetails.user_id')->select('users.id','users.name','users.email','users.mobile_no','users.active','userdetails.id as details_id','userdetails.address','userdetails.phone','userdetails.region','userdetails.city','userdetails.zip')->where('users.id',$request->id)->first();
        return view('admin.customer.edit',$data);
    }
    
    public function update(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|email|unique:users,email,'.$request->id,
                    'mobile_no' => 'required|unique:users,mobile_no,'.$request->id,
                    'phone' => 'required|unique:userdetails,phone,'.$request->details_id,
                    'address' => 'required',
                    'region' => 'required',
                    'city' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
           // dd($request);
            $user=User::find($request->id);
            $user->name=$request->name;
            $user->email=$request->email;
            $user->mobile_no=$request->mobile_no;
            $user->save();
            if($request->has('details_id')){
                $details=Userdetail::find($request->details_id);
                $details->phone=$request->phone;
                $details->address=$request->address;
                $details->region=$request->region;
                $details->city=$request->city;
                $details->zip=$request->zip;
                $details->save();
            }else{
                $details=new Userdetail();
                $details->user_id=$request->id;
                $details->phone=$request->phone;
                $details->address=$request->address;
                $details->region=$request->region;
                $details->city=$request->city;
                $details->zip=$request->zip;
                $details->save();
            }
            return redirect()->back()->with('save', 'Customer update successfully.');
        }
    }
    
    public function sendSMSForm(){
        return view('admin.customer.notify.sms');
    }
    
    public function SearchMobile(Request $request){
        $queries=User::where('email', 'LIKE', '%'.$request->term.'%')
    		->orWhere('name', 'LIKE', '%'.$request->term.'%')
    		->orWhere('mobile_no', 'LIKE', '%'.$request->term.'%')
    		->groupBy('id')
    		->take(10)
    		->get();
    	if ($queries->isEmpty() ) {
          return response(['error' => 'Record not found'], 404);
        }else{
        foreach ($queries as $query)
        	{
        	    $results[] = array(
                    "label" => 'Customer Mobile : '.$query->mobile_no.'  / Customer Name : '.$query->name,
                    "value" =>$query->mobile_no,
                    "idno" => $query->mobile_no
                );
        	}
        return Response::json($results);
        }
    }
    
    public function sendSMS(Request $request){
        if($request->user=='single'){
            //dd($request);
            //exit;
            // $mobileno = "88" . $request->mobileno;
            // $sms_text = "$request->message";
            // $user = "Pride";
            // $pass = "xA33I127";
            // $sid = "PrideLtdEng";
            // $url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
            // $param = "user=$user&pass=$pass&sms[0][0]=$mobileno&sms[0][1]=" . urlencode($sms_text) . "&sms[0][2]=1234567890&sid=$sid";
            // $crl = curl_init();
            // curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
            // curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
            // curl_setopt($crl, CURLOPT_URL, $url);
            // curl_setopt($crl, CURLOPT_HEADER, 0);
            // curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($crl, CURLOPT_POST, 1);
            // curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
            // $response = curl_exec($crl);
            // curl_close($crl);
            return redirect()->back()->with('success', 'SMS send successfully.'); 
        }else if($request->user=='multi'){
            $queries = User::select('mobile_no')->get();
            $i=0;
            foreach($queries as $phone){
                    $i++;
                    // //$this->SMSSendMulti($phone->registeruser_phone,$request->message);
                    // $mobileno = "88" . $phone->mobile_no;
                    // $sms_text = "$request->message";
                    // $user = "Pride";
                    // $pass = "xA33I127";
                    // $sid = "PrideLtdEng";
                    // $url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
                    // $param = "user=$user&pass=$pass&sms[0][0]=$mobileno&sms[0][1]=" . urlencode($sms_text) . "&sms[0][2]=1234567890&sid=$sid";
                    // $crl = curl_init();
                    // curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
                    // curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
                    // curl_setopt($crl, CURLOPT_URL, $url);
                    // curl_setopt($crl, CURLOPT_HEADER, 0);
                    // curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
                    // curl_setopt($crl, CURLOPT_POST, 1);
                    // curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
                    // //$response = curl_exec($crl);
                    // curl_close($crl);
            }
            return redirect()->back()->with('success', "Total $i SMS send successfully."); 
        }else{
            return redirect()->back()->with('error', 'SMS send error.'); 
        }
    }
    
    public function sendEmailForm(){
        return view('admin.customer.notify.email');
    }
    
    public function SearchEmail(Request $request){
    	$results = array();
    	$queries = User::where('email', 'LIKE', '%'.$request->term.'%')
    		->orWhere('name', 'LIKE', '%'.$request->term.'%')
    		->orWhere('mobile_no', 'LIKE', '%'.$request->term.'%')
    		->groupby('id')
    		->take(10)
    		->get();
        	foreach ($queries as $query)
        	{
        	    $results[] = array(
                    "label" => 'Customer Email : '.$query->email.' / Customer Name : '.$query->name,
                    "value" =>$query->email,
                    "idno" => $query->id
                );
        	}
        return Response::json($results);
    }
    
    public function sendEmail(Request $request){
        if($request->user=='single'){
                $emailto=$request->emailto;
                $email_data['email']=$request->emailto;
                $email_data['subject']=$request->subject;
                $message_body=htmlspecialchars_decode($request->message);
                $email_data['message_details']=$message_body;
                $email_data['file']=$request->file('file');
                Notification::route('mail', $request->emailto)->notify(new SendEmail($email_data));
                return redirect()->back()->with('success', 'Email send successfully.');
        }else if($request->user=='multi'){
            $users=User::all();
            $i=0;
            foreach($users as $user){
                $i++;
                $email_data['email']= $user->email;
                $email_data['subject']=$request->subject;
                $message_body=html_entity_decode($request->message);
                $email_data['message_details']=$message_body;
                $email_data['file']=$request->file('file');
                Notification::route('mail', $user->email)->notify(new SendEmail($email_data));
            }
            return redirect()->back()->with('success', "Total $i emails send successfully.");
        }
    }
    
    public function PhoneRequest(){
        $data['numbers']=Phnumber::select('id','phnumber_name','phnumber_number','comment_box','created_at')->orderBy('id','DESC')->get();
        return view('admin.customer.phone_request',$data);
    }
    
}
