<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Campaign;

class CampaignManage extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public function create(){
        return view('admin.campaign.add');
    }
    
    public function store(Request $request){
        $campaign=new Campaign();
        $campaign->category=$request->category;
        $campaign->name=$request->name;
        $campaign->slug=$request->slug;
        $campaign->starts_at=$request->starts_at;
        $campaign->expires_at=$request->expires_at;
        $campaign->created_by=Auth::guard('admin')->user()->id;
        $campaign->save();
        toastr()->success('Campaign added successfully!');
        return redirect()->back()->with('save', 'Campaign added successfully!.');
    }
    
    public function list(){
        $campaign=new Campaign();
        $data['list']=$campaign->list();
        return view('admin.campaign.list',$data);
    }
    
    public function edit($id){
        $data['campaign']=Campaign::find($id);
        return view('admin.campaign.edit',$data);
    }
    
    public function update(Request $request){
        $campaign=Campaign::find($request->id);
        $campaign->category=$request->category;
        $campaign->name=$request->name;
        $campaign->slug=$request->slug;
        $campaign->starts_at=$request->starts_at;
        $campaign->expires_at=$request->expires_at;
        $campaign->updated_by=Auth::guard('admin')->user()->id;
        $campaign->save();
        toastr()->success('Campaign updated successfully!');
        return redirect()->back()->with('save', 'Campaign updated successfully!.');
    }
    
    public function delete($id){
        Campaign::where('id',$id)->update(['status'=>1]);
        toastr()->success('Campaign delete successfully!');
        return redirect()->back()->with('save', 'Campaign delete successfully!.');
    }
    
}
