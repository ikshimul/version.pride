<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;
use Image;
use Illuminate\Support\Facades\Storage;
use Session;
use Carbon\Carbon;
use Auth;
use App\Models\Maincat;
use App\Models\Cat;
use App\Models\Subcat;
use App\Models\Procat;
use App\Models\Subprocat;
use App\Models\Prosize;
use App\Models\Product;
use App\Models\Productsize;
use App\Models\Productalbum;
use App\Models\Productimg;
use App\Models\Campaign;
use App\Models\Fabric;
use Illuminate\Support\Facades\Input;

class Manageproduct extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public function create() {
        $data['maincats'] = Maincat::all();
        $data['cats'] = Cat::all();
        $data['subcats'] = Subcat::all();
        $value = Auth::guard('admin')->user()->id;
        $data['total_product'] = Product::count();
        $data['procat_list'] = Procat::all();
        $data['avail_size'] = Prosize::all();
        $data['campaigns'] = Campaign::where('status',0)->orderBy('id','DESC')->get();
        $data['fabrics'] = Fabric::where('active',1)->orderBy('id','DESC')->get();
        return view('admin.product.add', $data);
    }
    
    public function store(Request $request){
        //dd($request);
        $created_by=Auth::guard('admin')->user()->id;
        $totalnumberofinserteddata = 0;
        $numberofboxedchecked = 0;
        $current_date = date("d-m-Y");
        $vaildation = Validator::make($request->all(), [
                    'txtproductname' => 'required|min:4',
                    'txtprice' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $discount = $request->txtpricediscounted;
            $original_price = $request->txtprice;
            if ($discount >= 1) {
                $discount_price = $original_price - $discount;
                $discount_price = ($discount_price * 100) / $original_price;
            } else {
                $discount_price = 0;
            }
            if ($request->campaign) {
                $specialcollection = $request->campaign;
            } else {
                $specialcollection = 0;
            }
            if ($request->has('occassion')) {
                $occassion = $request->occassion;
            } else {
                $occassion = 0;
            }
            $product=new Product();
            $product->maincat=$request->maincat;
            $product->cat=$request->cat;
            $product->subcat=$request->subcat;
            $product->product_name=$request->txtproductname;
            $product->fabric=$request->fabric;
            $product->product_price=$request->txtprice;
            $product->discount_product_price=$discount_price;
            $product->product_pricefilter=$request->ddlfilter;
            $product->product_styleref=$request->txtstyleref;
            $product->product_styleref=$request->txtstyleref;
            $product->product_description=$request->txtproductdetails;
            $product->product_care=$request->txtproductcare;
            $product->created_by=$created_by;
            $product->isSpecial=$specialcollection;
            $product->spcollection=$occassion;
            $product->sold=$request->txtorder;
            $product->save();
            $product_id=$product->id;
            //Product::where('id',$product->id)->update(['sold'=>$product->id]);
            
            $total_grp = $request->total_grp;
            for ($loopcountermain = 1; $loopcountermain <= $total_grp; $loopcountermain++) {
                $txtcolorvalname = "txtcolorname_" . $loopcountermain;
                $color_name_post = "txtcolorname_" . $loopcountermain;
                $txtcolovalue = $request->$color_name_post;
                $colorfilethm = "file_colorthm_" . $loopcountermain;
                $totalnumberofavailabesize = session('numberofavailabesize');
                for ($loopcountersize = 1; $loopcountersize <= $totalnumberofavailabesize; $loopcountersize++) {
                    $checkboxvarname = "size" . $loopcountersize . "_" . $loopcountermain;
                    $checkboxvarvalename = "input_size" . $loopcountersize . "_" . $loopcountermain;
                    $checkboxvarvalename_barcode = "barcode_input_size" . $loopcountersize . "_" . $loopcountermain;
                    if ($request->$checkboxvarname == TRUE) {
                        $productsizaname = $request->$checkboxvarname;
                        $productsizquantity = $request->$checkboxvarvalename;
                        $barcode=$request->$checkboxvarvalename_barcode;
                        $numberofboxedchecked++;
                        if ($productsizquantity > 0) {
                            
                        } else {
                            $productsizquantity = 0;
                        }
                        $productsize=new Productsize();
                        $productsize->product_id = $product->id;
                        $productsize->barcode = $barcode;
                        $productsize->productsize_size = $productsizaname;
                        $productsize->SizeWiseQty = $productsizquantity;
                        $productsize->color_name = $txtcolovalue;
                        $productsize->save();
                        $productsize_id=$productsize->id;
                        if ($productsize_id) {
                            $totalnumberofinserteddata++;
                        } else {
                            
                        }
                    } else {
                        
                    }
                }
                if ($totalnumberofinserteddata == $numberofboxedchecked) {
                    //dd($request);
                    $productalbum=new Productalbum();
                    $productalbum->product_id=$product->id;
                    $productalbum->productalbum_name=$txtcolovalue;
                    $productalbum->productalbum_order=$loopcountermain;
                    $productalbum->save();
                    $productalbum_id=$productalbum->id;
                    if ($productalbum_id) {
                        if ($request->hasFile($colorfilethm)) {
                            $file = $request->file($colorfilethm);
                            $savename = $productalbum_id . '_album_thm_' . $file->getClientOriginalName();
                            $image = Image::make($request->file($colorfilethm))->save('storage/app/public/pgallery/' . $savename);
                            if ($image) {
                                Productalbum::where('id', $productalbum_id)->update(['productalbum_img'=>$savename]);
                                for ($filecounter = 1; $filecounter <= 6; $filecounter++) {
                                    $imageid = NULL;
                                    $imgfieldname = "file_im" . $filecounter . "_" . $loopcountermain;
                                    if ($request->hasFile($imgfieldname)) {
                                        $file = $request->file($imgfieldname);
                                        $filename = $product_id . "_product_image_" . $filecounter . "_" . $file->getClientOriginalName();
                                        $image = Image::make($request->file($imgfieldname))->save('storage/app/public/pgallery/' . $filename);
                                        $productimg=new Productimg();
                                        $productimg->productalbum_id=$productalbum_id;
                                        $productimg->productimg_order='';
                                        $productimg->save();
                                        $productimg_id = $productimg->id;
                                        if ($image) {
                                            $width = Image::make($request->file($imgfieldname))->width();
                                            $height = Image::make($request->file($imgfieldname))->height();
                                            if ($width >= 1590 && $width <= 1610 && $height >= 2390 && $height <= 2410) {
                                                //upload thm file
                                                $filenamethmb = $product_id . "_product_image_" . $filecounter . "_thm_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(200, 300)->save('storage/app/public/pgallery/' . $filenamethmb);
                                                //upload tiny file
                                                $filenametiny = $product_id . "_product_image_" . $filecounter . "_tiny_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(64, 96)->save('storage/app/public/pgallery/' . $filenametiny);
                                                //upload medium file
                                                $filenamemedium = $product_id . "_product_image_" . $filecounter . "_medium_" . $file->getClientOriginalName();
                                                $image = Image::make($request->file($imgfieldname))->resize(370, 555)->save('storage/app/public/pgallery/' . $filenamemedium);
                                                Productimg::where('id', $productimg_id)->update(['productimg_img'=>$filename,'productimg_img_tiny'=>$filenametiny,'productimg_img_medium'=>$filenamemedium,'productimg_img_thm'=>$filenamethmb,'productimg_order'=>$filecounter]);
                                                if ($filecounter == 1 && $loopcountermain == 1) {
                                                    Product::where('id',$product->id)->update(['product_img_thm'=>$filenamemedium]);
                                                } else {
                                                    
                                                }
                                            } else {
                                                $productimg = Productimg::find($productimg_id);
                                                $productimg->delete();
                                                
                                                $productalbum = Productalbum::find($productalbum_id);
                                                $productalbum->delete();

                                                Product::where('id',$product_id)->update(['product_active_deactive'=>1,'trash'=>1]);
                                                return redirect()->back()->with('error', 'Image size problem.Upload image size must be equal 1600 X 2400');
                                            }
                                        }
                                    }
                                }
                            } else {
                                
                            }
                        } else {
                            
                        }
                    } else {
                        return redirect()->back()->with('error', 'Product ablum not inserted!');
                    }
                } else {
                    
                }
            }
            return redirect()->back()->with('save', 'Product upload successfully!');
        }
    }
    
    public function list(Request $request){
        $product=new Product();
        $subpro_id =$request->scid;
        $data['sub_pro_selected'] = $subpro_id;
        $subpro = new Subcat();
        $data['product_list'] = $product->adminlist($subpro_id);
        $data['sub_category'] = $subpro->getall();
        $data['subpro_id'] = $subpro_id;
        return view('admin.product.list', $data);
    }
    
    protected function edit(Request $request){
        $data['total_product'] = Product::count();
        $data['maincats'] = Maincat::all();
        $data['cats'] = Cat::all();
        $data['subcats'] = Subcat::all();
        $data['subprocats'] = Subprocat::all();
        $data['procat_list'] = Procat::all();
        $data['avail_size'] = Prosize::all();
        $data['campaigns'] = Campaign::where('status',0)->orderBy('id','DESC')->get();
        $data['product']=Product::find($request->id);
        return view('admin.product.edit', $data);
    }
    
    protected function update(Request $request){
        if ($request->has('campaign')) {
            $isSpecial = $request->campaign;
        } else {
            $isSpecial = 0;
        }
        
        if ($request->has('occassion')) {
            $occassion = $request->occassion;
        } else {
            $occassion = 0;
        }
        $discount = $request->txtpricediscounted;
        $original_price = $request->txtprice;
        if ($discount >= 1) {
            $discount_price = ($discount / 100) * $original_price;
            $price_with_discount=$original_price - $discount_price;
        } else {
            $price_with_discount = 0;
        }
        $product=Product::find($request->id);
        $product->maincat=$request->maincat;
        $product->cat=$request->cat;
        $product->subcat=$request->subcat;
        $product->product_styleref=$request->txtstyleref;
        $product->product_name=$request->txtproductname;
        $product->product_price=$request->txtprice;
        $product->discount_product_price=$request->txtpricediscounted;
        $product->product_pricediscounted=$price_with_discount;
        $product->product_description=$request->txtproductdetails;
        $product->fabric= $request->fabric;
        $product->product_care= $request->txtproductcare;
        $product->isSpecial=$isSpecial;
        $product->spcollection=$occassion;
        $product->save();
        return redirect()->back()->with('update', 'Updated successfully !');
    }
    
    protected function active(Request $request){
        $product=Product::find($request->id);
        $product->product_active_deactive=0;
        $product->trash=0;
        $product->save();
        return redirect()->back()->with('update', 'Active Successfully.');
    }
    
    protected function deactive(Request $request){
        $product=Product::find($request->id);
        $product->product_active_deactive=1;
        $product->save();
        return redirect()->back()->with('deactive', 'Deactive Successfully.');
    }
    
    protected function delete(Request $request){
        $product=Product::find($request->id);
        $product->product_active_deactive=1;
        $product->trash=1;
        $product->save();
        return redirect()->back()->with('deactive', 'Delete Successfully.');
    }
    
    public function trashProducts(){
        $product=new Product();
        $data['product_list'] = $product->adminlistTrash();
        return view('admin.product.trash', $data);
    }
    
    public function trashProductreactive(Request $request){
         $product=Product::find($request->id);
        $product->product_active_deactive=0;
        $product->trash=0;
        $product->save();
        return redirect()->back()->with('update', 'Reactive Successfully.');
    }
    
    public function trashProductdelete(Request $request){
        $albums=Productalbum::where('product_id',$request->id)->get();
        foreach($albums as $album){
            $imgs=Productimg::where('productalbum_id',$album->id)->get();
            if($imgs){
                 foreach($imgs as $info){
                    $get_productimg = Productimg::find($info->id);
                    $product_tiny_image = $get_productimg->productimg_img_tiny;
                    $product_medium_image = $get_productimg->productimg_img_medium;
                    $productimg_img_thm = $get_productimg->productimg_img_thm;
                    $productimg_img = $get_productimg->productimg_img;
                    Storage::delete("public/pgallery/$product_tiny_image");
                    Storage::delete("public/pgallery/$product_medium_image");
                    Storage::delete("public/pgallery/$productimg_img_thm");
                    Storage::delete("public/pgallery/$productimg_img");
                    Productimg::where('id', $info->id)->delete();
                 }
            }
            $album_info=Productalbum::find($album->id);
            Productsize::where('product_id',$request->id)->where('color_name',$album_info->productalbum_name)->delete();
            Storage::delete("public/pgallery/$album->productalbum_img");
            Productalbum::where('id',$album->id)->delete();
        }
        Product::where('id',$request->id)->delete();
        return redirect()->back()->with('deactive', 'Product deleted successfully!');
    }
}
