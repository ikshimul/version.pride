<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Productsize;
use App\Models\Product;
use App\Models\Maincat;
use App\Models\Cat;
use App\Models\Subcat;
use App\Models\Prosize;
use DB;

class Managestock extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public static function GetSizeQty($id){
        $productsize=new Productsize();
        return  $productsize->productsizes($id);
    }
    
    public static function GetExpectSize($product_id,$color_name){
        $prosizes = DB::select("SELECT *  FROM prosizes where prosize_name not in(select productsize_size  FROM productsizes where product_id='$product_id' and color_name='$color_name')");
        return $prosizes;
    }
    
     public static function GetColorProSize($product_id){
        $color_wise_size = DB::table('productsizes')->where('product_id', $product_id)->groupBy('color_name')->get();
        return $color_wise_size;
    }
    
    public function view(Request $request){
        $data['product']=Product::find($request->id);
        $data['prosizes']=Prosize::where('active',1)->get();
        //dd($data['prosize']);
        $data['product_maincat']=Maincat::find($data['product']->maincat);
        $data['product_procat']=Cat::find($data['product']->cat);
        $data['product_subprocat']=Subcat::find($data['product']->subcat);
        return view('admin.product.stock.view',$data);
    }
    
    public function add(Request $request){
         $stcok=new Productsize();
         $stcok->product_id=$request->product_id;
         $stcok->SizeWiseQty=$request->SizeWiseQty;
         $stcok->barcode=$request->barcode;
         $stcok->color_name=$request->color_name;
         $stcok->productsize_size=$request->productsize_size;
         $stcok->save();
         return redirect()->back()->with('save', 'New size added successfully!');
    }
    
    public function update(Request $request){
        $total_pro = $request->id;
        for ($i = 0; $i < count($total_pro); $i++) {
          $stcok=Productsize::find($request->id[$i]);
          $stcok->SizeWiseQty=$request->qty[$i];
          $stcok->barcode=$request->barcode[$i];
          $stcok->save();
        }
        return redirect()->back()->with('update', 'Updated successfully!');
    }
    
    public function delete($id){
        $stcok=Productsize::find($id);
        $stcok->delete();
        return redirect()->back()->with('deactive', 'delete successfully!');
    }
}
