<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Productsize;
use App\Models\Product;
use App\Models\Productalbum;
use App\Models\Productimg;
use Image;
use Storage;
use Validator;

class Manageimage extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public function view(Request $request){
        $data['product'] = Product::find($request->product_id);
        $data['images'] = Productimg::where('productalbum_id', $request->album_id)->orderBy('productimg_order', 'ASC')->get();
        $data['album_id']=$request->album_id;
        return view('admin.product.image.view', $data);
    }
    
    public function add(Request $request){
        $data['product'] = Product::find($request->product_id);
        $data['album_id'] = $request->album_id;
        return view('admin.product.image.add', $data);
    }
    
    public function edit(Request $request){
        $data['image']=Productimg::find($request->id);
        $data['product'] = Product::find($request->product_id);
        $data['serial']=$request->serial;
        $data['product_id']=$request->product_id;
        return view('admin.product.image.edit', $data);
    }
    
    public function save(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'filename' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
        if ($request->hasFile('filename')) {
            $product_id=$request->product_id;
            $albumid=$request->album_id;
            $file = $request->file('filename');
            $filename = $albumid . "_product_image_" . $file->getClientOriginalName();
            $image = Image::make($request->file('filename'))->resize(1300, 1667)->save('storage/app/public/pgallery/' . $filename);
            $width = Image::make($request->file('filename'))->width();
            $height = Image::make($request->file('filename'))->height();
            if ($width >= 1590 && $width <= 1610 && $height >= 2390 && $height <= 2410) {
                //upload thm file
                $filenamethmb = $product_id . "_product_image_thm_" . $file->getClientOriginalName();
                $image = Image::make($request->file('filename'))->resize(200, 300)->save('storage/app/public/pgallery/' . $filenamethmb);
                //upload tiny file
                $filenametiny = $product_id . "_product_image_tiny_" . $file->getClientOriginalName();
                $image = Image::make($request->file('filename'))->resize(64, 96)->save('storage/app/public/pgallery/' . $filenametiny);
                //upload medium file
                $filenamemedium = $product_id . "_product_image_medium_" . $file->getClientOriginalName();
                $image = Image::make($request->file('filename'))->resize(370, 555)->save('storage/app/public/pgallery/' . $filenamemedium);
                $productimg =new Productimg();
                $productimg->productimg_order=$request->productimg_order;
                $productimg->productalbum_id=$albumid;
                $productimg->productimg_img=$filename;
                $productimg->productimg_img_tiny=$filenametiny;
                $productimg->productimg_img_medium=$filenamemedium;
                $productimg->productimg_img_thm=$filenamethmb;
                $productimg->save();
            } else {
                return redirect()->back()->with('error', 'upload size must be greater than or equal  1600 X 2400');
            }
            return redirect("admin/product/image/view?album_id=$albumid&product_id=$product_id")->with('update', 'Image added successfully!');
        }else{
            return redirect()->back()->with('error', 'Please select an image.');
        }
      }
    }
    
    public function update(Request $request){
        $image_id = $request->id;
        $product_id=$request->product_id;
        if ($request->hasFile('filename')) {
            $get_productimg = Productimg::find($request->id);
            $product_tiny_image = $get_productimg->productimg_img_tiny;
            $product_medium_image = $get_productimg->productimg_img_medium;
            $productimg_img_thm = $get_productimg->productimg_img_thm;
            $productimg_img = $get_productimg->productimg_img;
            Storage::delete("public/pgallery/$product_tiny_image");
            Storage::delete("public/pgallery/$product_medium_image");
            Storage::delete("public/pgallery/$productimg_img_thm");
            Storage::delete("public/pgallery/$productimg_img");
            $file = $request->file('filename');
            $filename = $image_id . "_product_image_" . $file->getClientOriginalName();
            $image = Image::make($request->file('filename'))->resize(1600, 2400)->save('storage/app/public/pgallery/' . $filename);
            $width = Image::make($request->file('filename'))->width();
            $height = Image::make($request->file('filename'))->height();
            if ($width >= 1590 && $width <= 1610 && $height >= 2390 && $height <= 2410) {
                //upload thm file
                $filenamethmb = $product_id . "_product_image_thm_" . $file->getClientOriginalName();
                $image = Image::make($request->file('filename'))->resize(200, 300)->save('storage/app/public/pgallery/' . $filenamethmb);
                //upload tiny file
                $filenametiny = $product_id . "_product_image_tiny_" . $file->getClientOriginalName();
                $image = Image::make($request->file('filename'))->resize(64, 96)->save('storage/app/public/pgallery/' . $filenametiny);
                //upload medium file
                $filenamemedium = $product_id . "_product_image_medium_" . $file->getClientOriginalName();
                $image = Image::make($request->file('filename'))->resize(370, 555)->save('storage/app/public/pgallery/' . $filenamemedium);
                $productimg = Productimg::find($request->id);
                $productimg->productimg_order=$request->productimg_order;
                $productimg->productimg_img=$filename;
                $productimg->productimg_img_tiny=$filenametiny;
                $productimg->productimg_img_medium=$filenamemedium;
                $productimg->productimg_img_thm=$filenamethmb;
                $productimg->save();
                if ($request->serial == 1) {
                    Product::where('id', $request->product_id)->update(['product_img_thm'=>$filenamemedium]);
                } elseif ($request->serial == 3) {
                   
                }
            } else {
                return redirect()->back()->with('error', 'Image size problem.Upload image size must be equal 1600 X 2400');
            }
            return redirect()->back()->with('save', 'Image edit successfully!');
        }
        return redirect()->back()->with('error', 'please select an image which size must be equal 1600 X 2400');
    }
    
    public function delete(Request $request){
        if ($request->serial == 1) {
            return redirect()->back()->with('error', 'You can not delete first image.Please update first image.');
        }else{
                $get_productimg = Productimg::find($request->id);
                $product_tiny_image = $get_productimg->productimg_img_tiny;
                $product_medium_image = $get_productimg->productimg_img_medium;
                $productimg_img_thm = $get_productimg->productimg_img_thm;
                $productimg_img = $get_productimg->productimg_img;
                Storage::delete("public/pgallery/$product_tiny_image");
                Storage::delete("public/pgallery/$product_medium_image");
                Storage::delete("public/pgallery/$productimg_img_thm");
                Storage::delete("public/pgallery/$productimg_img");
                Productimg::where('id', $request->id)->delete();
                return redirect()->back()->with('update', 'Image deleted successfully!');
                
        }
    }
    
}
