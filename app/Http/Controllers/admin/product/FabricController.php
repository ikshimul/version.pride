<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Fabric;

class FabricController extends Controller
{
     public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public function create(){
        $data['fabrics']=Fabric::all();
        return view('admin.product.fabric.add',$data);
    }
    
    protected function store(Request $request){
       $vaildation = Validator::make($request->all(), [
                    'name' => 'required',
                    'care' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $fabric=new Fabric();
            $fabric->name=$request->name;
            $fabric->care=$request->care;
            $fabric->save();
            return redirect()->back()->with('save', 'Save successfully!');
            //return 1;
        }
    }
    
    public function edit(Request $request){
        $data['fabric']=Fabric::find($request->id);
        return view('admin.product.fabric.edit',$data);
    }
    
    protected function update(Request $request){
        $fabric=Fabric::find($request->id);
        $fabric->name=$request->name;
        $fabric->care=$request->care;
        $fabric->save();
        return redirect()->back()->with('save', 'Update successfully!');
       // return 1;
    }
    
    protected function active(Request $request){
        $fabric=Fabric::find($request->id);
        $fabric->active=1;
        $fabric->save();
        return redirect()->back()->with('update', 'Active successfully!');
    }
    
    protected function deactive(Request $request){
        $fabric=Fabric::find($request->id);
        $fabric->active=0;
        $fabric->save();
        return redirect()->back()->with('error', 'Deactive successfully!');
    }
    
    protected function delete(Request $request){
        $fabric=Fabric::find($request->id);
        $fabric->delete();
        return redirect()->back()->with('error', 'Delete successfully!');
    }
    
    public function view(Request $request){
        $febric=Fabric::where('id',$request->id)->first();
        return json_encode($febric);
    }
}
