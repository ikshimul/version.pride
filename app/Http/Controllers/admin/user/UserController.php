<?php

namespace App\Http\Controllers\admin\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use App\Models\Role;
use App\Models\Rolepermission;
use Image;
use Storage;
use Auth;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public function create(){
        $data['roles']=Role::select('id','role_name')->where('active',1)->get();
        return view('admin.user.create',$data);
    }
    
    protected function store(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'full_name' => 'required',
                    'email' => 'required|email|unique:admins,email',
                    'username' => 'required|max:50|unique:admins,username',
                    'password' => 'required|min:6',
                    'c_password' => 'required|same:password',
                    'role_id' => 'required'
        ]);
        if ($vaildation->fails()) {
            //dd($vaildation);
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $file = $request->file('photo');
            $savename = time() . '_' . $file->getClientOriginalName();
            Storage::put('public/admin-user/' . $savename, file_get_contents($request->file('photo')->getRealPath()));
            $role_info=Role::where('id',$request->role_id)->first();
            $user= Admin::create([
                'username' => $request->username,
                'name' => $request->full_name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'image' => $savename,
                'mobile' => $request->mobile,
                'address' => $request->address,
                'role_id' => $request->role_id,
                'active' => 0,
                'admin_type' => $role_info->role_name
            ]);
            if($user){
            return redirect()->back()->with('save', 'New admin user create successfully.');
            }else{
                return redirect()->back()->with('error', 'admin user create failed.');
            }
        }
    }
    
    public function list() {
        $data['users'] = Admin::get();
        return view('admin.user.list', $data);
    }
    
    protected function edit(Request $request){
        $data['roles']=Role::select('id','role_name')->where('active',1)->get();
        $data['user'] = Admin::find($request->id);
        return view('admin.user.edit', $data);
    }
    
    protected function update(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'full_name' => 'required',
                    'email' => 'required|email|unique:admins,email,'.$request->id,
                    'username' => 'required|unique:admins,username,'.$request->id,
                    'mobile' => 'required|unique:admins,mobile,'.$request->id,
                    'role_id' => 'required'
        ]);
        if ($vaildation->fails()) {
            //dd($vaildation);
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $role_info=Role::where('id',$request->role_id)->first();
            $user=Admin::find($request->id);
            if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $savename = time() . '_' . $file->getClientOriginalName();
                Storage::put('public/admin-user/' . $savename, file_get_contents($request->file('photo')->getRealPath()));
                $user->image=$savename;
            }
            $user->username=$request->username;
            $user->name=$request->full_name;
            $user->mobile=$request->mobile;
            $user->address=$request->address;
            $user->role_id=$request->role_id;
            $user->admin_type=$role_info->role_name;
            $user->save();
            return redirect()->back()->with('save', 'Admin update successfully.');
        }
    }
    
    public function active(Request $request) {
        $user=Admin::find($request->id);
        $user->active=1;
        $user->updated_by=Auth::guard('admin')->user()->id;
        $user->save();
        return redirect()->back()->with('update', 'Admin unblock successfully');
    }
    
    public function deactive(Request $request){
       $user=Admin::find($request->id);
        $user->active=0;
        $user->updated_by=Auth::guard('admin')->user()->id;
        $user->save();
        return redirect()->back()->with('error', 'Admin block successfully');
    }
    
    public function Profile(){
        $data['admin']=Auth::guard('admin')->user();
        return view('admin.user.profile', $data);
    }
    
    public function ProfileUpdate(Request $request){
        $rules = [
            'name' => 'required',
            'username' => Rule::unique('admins','username')->ignore($request->id, 'id'),
            'email' => Rule::unique('admins','email')->ignore($request->id, 'id'),
        ];
        $vaildation = Validator::make($request->all(), $rules);
        //dd($vaildation);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $admin=Admin::find($request->id);
            if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $savename = time(). '_' . $file->getClientOriginalName();
                Storage::put('public/admin-user/' . $savename, file_get_contents($request->file('photo')->getRealPath()));
                $admin->image=$savename;
            }
            $admin->name=$request->name;
            $admin->email=$request->email;
            $admin->username=$request->username;
            $admin->mobile=$request->mobile;
            $admin->address=$request->address;
            $admin->updated_by=Auth::guard('admin')->user()->id;
            $admin->save();
            return redirect()->back()->with('info', 'Information update successfully.');
        }
    }
    
    public function changePassword(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::guard('admin')->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("pass_error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("pass_error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $rules = [
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
            'password_confirmation' => 'required|same:new-password'
        ];
        $vaildation = Validator::make($request->all(), $rules);
        //dd($vaildation);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        }else{
        //Change Password
        $user = Auth::guard('admin')->user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("pass","Password changed successfully !");
        }
    }
}
