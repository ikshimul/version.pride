<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Conforder;
use App\Models\Shoppinproduct;
use PDF;
use Carbon\Carbon;

class Invoice extends Controller
{
    public function index(Request $request){
        $order_id=$request->id;
        $order=new Conforder();
        $order_product=new Shoppinproduct();
        $data['shipping_address_details'] = $order->orderdetails($order_id);
        $data['total_incomplete_order_info'] = $order_product->orderproducts($data['shipping_address_details']->shoppingcart_id);
        $data['order_id'] = $order_id;
        return view('admin.invoice.print', $data);
    }
    
    public function generatePDF(Request $request)
    {
        $order_id=$request->id;
        $order=new Conforder();
        $order_product=new Shoppinproduct();
        $data['shipping_address_details'] = $order->orderdetails($order_id);
        $data['total_incomplete_order_info'] = $order_product->orderproducts($data['shipping_address_details']->shoppingcart_id);
        $data['order_id'] = $order_id;
        //return view('admin.invoice.pdf', $data);
        $order=$data['shipping_address_details']->conforder_tracknumber;
        $current = Carbon::now()->format('YmdHs').'-'.$order.'.pdf';
        $pdf = PDF::loadView('admin.invoice.pdf',$data);    
        return $pdf->download($current);
    }
}
