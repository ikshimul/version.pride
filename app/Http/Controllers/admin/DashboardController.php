<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Charts\MonthlyUser;
use Auth;
use DB;
use App\Models\User;
use App\Models\Rolepermission;
use App\Models\Conforder;
use App\Models\Productsize;
use App\Models\Shoppinproduct;
use App\Models\Phnumber;
use App\Models\Product;
use App\Models\Browser;


class DashboardController extends Controller {

    public function __construct() {
       // $this->middleware('auth:admin');
        $this->middleware('AdminAuth');
    }
    
   public static function getRolePermission() {
        $role_id = Auth::guard('admin')->user()->role_id;
        $role_permission = Rolepermission::where('role_id', $role_id)->get();
        $permission = [];
        foreach ($role_permission as $p_list) {
            $permission[] = $p_list->permission;
        }
        return $permission;
    }
    
    public static function userReport(){
       $totals = DB::select("SELECT SUM(shoppingcarts.shoppingcart_total) as monthlytotal FROM conforders
            INNER JOIN shoppingcarts
            ON conforders.shoppingcart_id=shoppingcarts.id
            WHERE YEAR(conforders.created_at)=2021 and conforders.conforder_status='Closed' GROUP BY MONTH(conforders.created_at)
            ORDER BY MONTH(conforders.created_at) ASC");
            $sale = [];
                foreach ($totals as $sales) {
                    $sale[] = $sales->monthlytotal;
                }
             return json_encode($sale,JSON_NUMERIC_CHECK);   
    }

    public function index() {
        $stock=new Productsize();
        $sale=new Shoppinproduct();
        $order=new Conforder();
        $product=new Product();
        $data['totalorder']=Conforder::where('conforder_status', '!=', 'Invalidate')->count();
        $data['total_incomplete_order_info']=$order->incompleteorder();
        if(Auth::guard('admin')->user()->role_id == 1 || Auth::guard('admin')->user()->role_id == 2){
            $data['totalregisteruser']=User::count();
            $data['totalbouncedorderrate']=Conforder::where('conforder_status','Cancelled')->count();
            $data['total_qty']=$stock->totalstock();
            $data['total_sale_product']=$sale->totalSale();
            $data['guest_order']=Conforder::where('user_id',0)->where('conforder_status', '!=', 'Invalidate')->count();
            $data['register_order']=Conforder::whereNotIn('user_id',[0])->where('conforder_status', '!=', 'Invalidate')->count();
            $data['cancel_order']=Conforder::where('conforder_status','Cancelled')->count();
            $data['phone_request']=Phnumber::count();
            //$data['last_ten_products']=$product->NewArriavl();
            $data['recent_sale']=$order->recent_sales();
            $data['site_user']=User::orderBy('id', 'desc')->limit(8)->get();
            $this->SoldoutUpdate();
            $months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
            $user = [];
                foreach ($months as $key => $value) {
                    $user[] = User::where(\DB::raw("DATE_FORMAT(created_at, '%M')"),$value)->where(\DB::raw("DATE_FORMAT(created_at, '%Y')"),2021)->count();
                }
            
            $data['months']=json_encode($months,JSON_NUMERIC_CHECK);
            $data['users']=json_encode($user,JSON_NUMERIC_CHECK);
            
            $totals = DB::select("SELECT SUM(shoppingcarts.shoppingcart_total) as monthlytotal FROM conforders
            INNER JOIN shoppingcarts
            ON conforders.shoppingcart_id=shoppingcarts.id
            WHERE YEAR(conforders.created_at)=2021 and conforders.conforder_status='Closed' GROUP BY MONTH(conforders.created_at)
            ORDER BY MONTH(conforders.created_at) ASC");
            $sale = [];
                foreach ($totals as $sales) {
                    $sale[] = $sales->monthlytotal;
                }
             $data['saleReport']=json_encode($sale,JSON_NUMERIC_CHECK); 
             $monthlyorder = [];
                foreach ($months as $key => $value) {
                    $monthlyorder[] = Conforder::where(DB::raw("(DATE_FORMAT(created_at,'%M'))"),$value)->where(\DB::raw("DATE_FORMAT(created_at, '%Y')"),2021)->where('conforder_status', '!=', 'Invalidate')->count();
                }
             $data['orderReport']=json_encode($monthlyorder,JSON_NUMERIC_CHECK); 
             $data['paymentreport']=$this->PaymentReport();
             $data['last_ten_products']=Product::where('product_active_deactive', 0)->whereDate('products.created_at', '>', Carbon::now()->subDays(30))->orderBy('id', 'DESC')->limit(8)->get();
             $date_end=Carbon::now()->subMonth()->toDateTimeString();
             $data['recent_sale']=DB::table('conforders')
    		->join('shoppingcarts', 'conforders.shoppingcart_id', '=', 'shoppingcarts.id')
    		->join('shoppinproducts', 'shoppingcarts.id', '=', 'shoppinproducts.shoppingcart_id')
    		->join('products', 'shoppinproducts.product_id', '=', 'products.id')
            ->select('conforders.created_at','products.id','products.product_name', 'products.product_description', 'products.product_styleref', 'products.product_price', 'products.product_pricediscounted', 'products.discount_product_price', 'products.product_img_thm','products.created_at as add_date','shoppinproducts.cart_image')
            ->orderBy('conforders.id', 'desc')
            ->where('conforders.conforder_status','Closed')
            ->where('conforders.created_at', '>=', $date_end)
            ->groupBy('products.id')
    		->limit(8)
            ->get();;
             $data['site_user']=User::with('userdetails')->orderBy('id', 'DESC')->limit(8)->get();
             $data['comparison']=$this->getComprisonChartData();
             return view('admin.dashboard-v2',$data);
        }elseif(Auth::guard('admin')->user()->role_id == 3){
            return view('admin.dashboard.delivery',$data);
        }
        
    }
    
    public function indexv2(){
        $order=new Conforder();
        $product=new Product();
        if(Auth::guard('admin')->user()->role_id == 1 || Auth::guard('admin')->user()->role_id == 2){
            $data['totalorder']=Conforder::count();
            $data['totalregisteruser']=User::count();
            $data['totalbouncedorderrate']=Conforder::where('conforder_status','Cancelled')->count();
            $data['total_qty']=Productsize::sum('SizeWiseQty');
            $data['total_sale_product']=Shoppinproduct::sum('shoppinproduct_quantity');
            $data['guest_order']=Conforder::where('user_id',0)->where('conforder_status', '!=', 'Invalidate')->count();
            $data['register_order']=Conforder::whereNotIn('user_id',[0])->where('conforder_status', '!=', 'Invalidate')->count();
            $data['cancel_order']=Conforder::where('conforder_status','Cancelled')->count();
            $data['phone_request']=Phnumber::count();
            $data['total_incomplete_order_info']=$order->incompleteorder();
            $data['last_ten_products']=$product->NewArriavl();
            $data['recent_sale']=$order->recent_sales();
            $data['site_user']=User::orderBy('id', 'desc')->limit(8)->get();
            $data['paymentreport']=$this->PaymentReport();
            //dd($data);
            $this->SoldoutUpdate();
            return view('admin.dashboard-v2',$data);
        }elseif(Auth::guard('admin')->user()->role_id == 3){
            echo 'Delivery Dashboard';
        }
    }
    
    public function SoldoutUpdate(){
        $sql = "SELECT SUM(SizeWiseQty) FROM productsizes GROUP BY product_id";
        $result = DB::select($sql);
		$stocks = DB::table('productsizes')
                 ->select('product_id', DB::raw('sum(SizeWiseQty) as stock'))
                 ->groupBy('product_id')
                 ->get();
		foreach($stocks as $stock){
			if($stock->stock <=0){
				$stock->product_id.'<br>';
				Product::where('id', $stock->product_id)->update(['sold' => -$stock->product_id]);
			}else{
			   Product::where('id', $stock->product_id)->update(['sold' => $stock->product_id]); 
			}
		}
	//	echo 'sold updated';
    }
    
    public function PaymentReport(){
        $bkash_payment=Conforder::join('shoppingcarts','conforders.shoppingcart_id','=','shoppingcarts.id')
		->where('payment_method','bKash')
        ->where('conforder_status','Closed')
		->where(DB::raw("(DATE_FORMAT(conforders.created_at,'%m'))"),date('m'))
        ->where(\DB::raw("DATE_FORMAT(conforders.created_at, '%Y')"),date('Y'))
		->sum('shoppingcarts.shoppingcart_total');
		$bank_payment=Conforder::join('shoppingcarts','conforders.shoppingcart_id','=','shoppingcarts.id')
		->where('payment_method','ssl')
        ->where('conforder_status','Closed')
		->where(DB::raw("(DATE_FORMAT(conforders.created_at,'%m'))"),date('m'))
        ->where(\DB::raw("DATE_FORMAT(conforders.created_at, '%Y')"),date('Y'))
		->sum('shoppingcarts.shoppingcart_total');
		$cash_payment=Conforder::join('shoppingcarts','conforders.shoppingcart_id','=','shoppingcarts.id')
		->where('payment_method','cDelivery')
        ->where('conforder_status','Closed')
        ->where(DB::raw("(DATE_FORMAT(conforders.created_at,'%m'))"),date('m'))
        ->where(\DB::raw("DATE_FORMAT(conforders.created_at, '%Y')"),date('Y'))
		->sum('shoppingcarts.shoppingcart_total');
		//dd($cash_payment);
		$channel = [$bkash_payment, $bank_payment, $cash_payment];
		return json_encode($channel,JSON_NUMERIC_CHECK);
		
    }
    
    public function getComprisonChartData(){
            $output = array();
            $syear = 2019;
               $totals = DB::select("SELECT sum(shoppingcarts.shoppingcart_total) as monthlytotal,DATE_FORMAT(conforders.created_at, '%Y-%m') AS new_date,DATE_FORMAT(conforders.created_at, '%Y') AS year,DATE_FORMAT(conforders.created_at, '%m') AS month
			from conforders
			inner join shoppingcarts on conforders.shoppingcart_id=shoppingcarts.id
			where conforders.conforder_status='Closed' AND
			DATE_FORMAT(conforders.created_at, '%Y')=2019 group by new_date order by month  asc");
        
             $count = count($totals);
             $i = 0;
             $output4['label'] = 2019;
             $output4['fill'] = false;
             $output4['borderWidth'] = 2;
             $output4['lineTension'] = 0;
             $output4['spanGaps'] = true;
             $output4['borderColor'] = '#efefef';
             $output4['pointRadius'] = 3;
             $output4['pointHoverRadius'] = 7;
             $output4['pointColor'] = '#efefef';
             $output4['pointBackgroundColor'] = 'black';
             foreach($totals as $total){
                    $int = (int)$total->monthlytotal;
                    $output4['data'][$i] =  $int;
                    $i++;
                }

            for($k=$count;$k<=11;$k++){
                $output4['data'][$k] = 0;
            }   
            
            $syear = 2020;
        
               $totals = DB::select("SELECT sum(shoppingcarts.shoppingcart_total) as monthlytotal,DATE_FORMAT(conforders.created_at, '%Y-%m') AS new_date,DATE_FORMAT(conforders.created_at, '%Y') AS year,DATE_FORMAT(conforders.created_at, '%m') AS month
			from conforders
			inner join shoppingcarts on conforders.shoppingcart_id=shoppingcarts.id
			where conforders.conforder_status='Closed' AND
			DATE_FORMAT(conforders.created_at, '%Y')=2020 group by new_date order by month  asc");
        
             $count = count($totals);
             $i = 0;
             $output5['label'] = 2020;
             $output5['fill'] = false;
             $output5['borderWidth'] = 2;
             $output5['lineTension'] = 0;
             $output5['spanGaps'] = true;
             $output5['borderColor'] = '#efefef';
             $output5['pointRadius'] = 3;
             $output5['pointHoverRadius'] = 7;
             $output5['pointColor'] = '#efefef';
             $output5['pointBackgroundColor'] = 'black';
             foreach($totals as $total){
                    $int = (int)$total->monthlytotal;
                    $output5['data'][$i] =  $int;
                    $i++;
                }

            for($k=$count;$k<=11;$k++){
                $output5['data'][$k] = 0;
            }   
            
            $syear = 2021;
        
            $totals = DB::select("SELECT sum(shoppingcarts.shoppingcart_total) as monthlytotal,DATE_FORMAT(conforders.created_at, '%Y-%m') AS new_date,DATE_FORMAT(conforders.created_at, '%Y') AS year,DATE_FORMAT(conforders.created_at, '%m') AS month
			from conforders
			inner join shoppingcarts on conforders.shoppingcart_id=shoppingcarts.id
			where conforders.conforder_status='Closed' AND
			DATE_FORMAT(conforders.created_at, '%Y')=2021 group by new_date order by month  asc");
        
             $count = count($totals);
             $i = 0;
             $output6['label'] = 2021;
             $output6['fill'] = false;
             $output6['borderWidth'] = 2;
             $output6['lineTension'] = 0;
             $output6['spanGaps'] = true;
             $output6['borderColor'] = '#efefef';
             $output6['pointRadius'] = 3;
             $output6['pointHoverRadius'] = 7;
             $output6['pointColor'] = '#efefef';
             $output6['pointBackgroundColor'] = 'black';
             foreach($totals as $total){
                    $int = (int)$total->monthlytotal;
                    $output6['data'][$i] =  $int;
                    $i++;
                }

            for($k=$count;$k<=11;$k++){
                $output6['data'][$k] = 0;
            }   
           $output[] = $output4;
           $output[] = $output5;
           $output[] = $output6;
   //dd(json_encode($output4));
          return json_encode($output,JSON_NUMERIC_CHECK);
          //return json_encode($output);
        
    }
    
}
