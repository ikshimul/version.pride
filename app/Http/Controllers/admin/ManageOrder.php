<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use DB;
use Auth;
use App\Models\Phnumber;
use App\Models\Conforder;
use App\Models\Shoppingcart;
use App\Models\Shoppinproduct;
use App\Models\Rider;
use App\Models\Cashexpenses;
use App\Models\Product;
use App\Models\Productalbum;
use App\Models\Productimg;
use App\Models\Productsize;
use App\Models\Ordershipping;
use App\Models\Cashtransaction;
use App\Models\Deliveryhistories;
use App\Models\Cities;
use App\Models\Bkashpayment;
use App\Models\Sslpayment;
use App\Models\Exchange;
use Carbon\Carbon;
use App\Notifications\bKashpaid;

class ManageOrder extends Controller
{
    public function __construct() {
        //$this->middleware('auth:admin', ['except' => 'Invoice']);
        //$this->middleware('AdminAuth', ['except' => 'Invoice']);
        //$this->middleware('permission', ['only' => ['manageIncompleteOrder','manageAllOrder','OrderFilteringView','UpdateOrderStatus','OrderDetails']]);
        $this->middleware('AdminAuth');
    }
    
    public static function getNewPhoneRequest(){
       $request_numbers= Phnumber::where('view',0)->where('phone_status',1)->count();
       return $request_numbers;
     }
     
      public static function getNewPhoneRequestList(){
       $request_numbers_list= Phnumber::where('view',0)->where('phone_status',1)->get();
       return $request_numbers_list;
     }
     
     public static function GetProductImageBig($product_id,$color){
         $img=new Productimg();
         return $img->getimagesinglebyproductid($product_id,$color);
     }
     
     public static function GetColorListByProductId($product_id){
         $color=new Productalbum();
         return $color->getproductwise($product_id);
     }
     
     public static function GetSizeListByProductId($product_id,$color_name){
         $size=new Productsize();
         return $size->getsizescolorwise($product_id,$color_name);
     }
     
     public static function GetQtyProductId($product_id,$color_name,$size){
         $size=new Productsize();
         return $size->getsizewiseqtysum($product_id,$color_name,$size);
     }
     
    public static function getTotlaIncomplte_order() {
        return Conforder::where('conforder_status', '!=', 'Closed')->where('conforder_status', '!=', 'Cancelled')->where('conforder_status', '!=', 'Exchanged')->where('conforder_status', '!=', 'Invalidate')->where('conforder_status', '!=', 'Returned')->count();
    }
    
    public function exchangeOrderNotify(){
        return Exchange::where('exchnage_complete',0)->where('active',1)->count();
    }
    
    public function newOrderNotify(){
        return Conforder::where('conforder_status', '!=', 'Closed')->where('conforder_status', '!=', 'Cancelled')->where('conforder_status', '!=', 'Exchanged')->where('conforder_status', '!=', 'Invalidate')->where('conforder_status', '!=', 'Returned')->count();
    }
    
    public function details(Request $request){
        $order_id=$request->id;
        $order=new Conforder();
        $order_product=new Shoppinproduct();
        $data['shipping_address_details'] = $order->orderdetails($order_id);
        $data['order_products'] = $order_product->orderproducts($data['shipping_address_details']->shoppingcart_id);
        $data['exchange_requests']=Exchange::where('shoppingcart_id',$data['shipping_address_details']->shoppingcart_id)->where('exchnage_complete',0)->where('active',1)->get();
        $data['riders'] = Rider::all()->where('status',0);
        $data['conforder'] = Cashexpenses::where('conforder_id',$order_id)->first();
        $data['bkash'] = Bkashpayment::where('conforder_id',$order_id)->first();
        $data['ssl'] = Sslpayment::where('conforder_id',$order_id)->first();
        $data['order_id'] = $order_id;
        $data['package_list']=$this->eCourierPackages();
        $data['ecourier_city']=$this->eCourierCity();
       // dd($data['ecourier_area']);
        //$data['package_list']=DB::table('ecourier_packages')->where('status',0)->get();
        $data['three_pl']=Deliveryhistories::where('conforder_id',$order_id)->first();
        $data['card_number'] = 'NIA';
        return view('admin.order.details', $data);
    }
    
    public function eCourierCity(){
        $curl = curl_init();
        $packagelist = [
                'parcel' => 'packagelist',
            ];
		curl_setopt_array($curl, array(
                CURLOPT_URL => "https://backoffice.ecourier.com.bd/api/city-list",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                //CURLOPT_POSTFIELDS => $packagelist,
                CURLOPT_HTTPHEADER => array(
                    "API-SECRET:zReJG",
                    "API-KEY: 66eY",
                    "USER-ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			
			$package_list = curl_exec($curl); 
			//dd($package_list);
			return json_decode($package_list);
    }
    

    public function eCourierPackages(){
        //https://staging.ecourier.com.bd/api/packages
        //https://backoffice.ecourier.com.bd/api/packages
        $curl = curl_init();
        $packagelist = [
                'parcel' => 'packagelist',
            ];
		curl_setopt_array($curl, array(
                CURLOPT_URL => "https://staging.ecourier.com.bd/api/packages",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $packagelist,
                CURLOPT_HTTPHEADER => array(
                    "API-SECRET:zReJG",
                    "API-KEY: 66eY",
                    "USER-ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			
			$package_list = curl_exec($curl); 
			//dd($package_list);
			return json_decode($package_list);
    }
    
    public function addressupdate(Request $request){
        Ordershipping::where('id', $request->ordershipping_id)->update([
            'Shipping_txtfirstname' => $request->firstname,
            'Shipping_txtlastname' => $request->lastname,
            'Shipping_txtaddressname' => $request->address,
            'Shipping_txtcity' => $request->city,
            'Shipping_txtzipcode' => $request->zip,
            'Shipping_txtphone' => $request->phone
        ]);
        $ordershipping=Ordershipping::find($request->ordershipping_id);
        $conforder=Conforder::find($ordershipping->conforder_id);
        $shoppingcarts=Shoppingcart::find($conforder->shoppingcart_id);
        if($shoppingcarts->shoppingcart_subtotal <= 2999){
            $cities=Cities::find($request->city);
            if($cities){
                if($cities->maping == 1){
                    $shipping_charge=150;
                }elseif($cities->maping == 0){
                    $shipping_charge=100;
                }
                Shoppingcart::where('id', $conforder->shoppingcart_id)->update(['shipping_charge' => $shipping_charge]);
            }
        }elseif($shoppingcarts->shoppingcart_subtotal >= 3000){
           Shoppingcart::where('id', $conforder->shoppingcart_id)->update(['shipping_charge' => 0]); 
        }
    }
    
    public function shipupdate(Request $request){
        Ordershipping::where('id', $request->ordershipping_id)->update(['Shipping_txtcity' => $request->route_city]);
        Shoppingcart::where('id', $request->shoppingcart_id)->update(['shipping_area' => $request->route_city]);
        $shoppingcarts=Shoppingcart::find($request->shoppingcart_id);
        if($shoppingcarts->shoppingcart_subtotal <= 2999){
            $cities=Cities::find($request->route_city);
            if($cities){
                if($cities->maping == 1){
                    $shipping_charge=150;
                }elseif($cities->maping == 0){
                    $shipping_charge=100;
                }
                Shoppingcart::where('id', $request->shoppingcart_id)->update(['shipping_charge' => $shipping_charge]);
            }
        }elseif($shoppingcarts->shoppingcart_subtotal >= 3000){
           Shoppingcart::where('id', $request->shoppingcart_id)->update(['shipping_charge' => 0]); 
        }
    }
    
    public function statusupdate(Request $request){
        //dd($request);
        $order=new Conforder();
        $order_product=new Shoppinproduct();
        $delivery=new Deliveryhistories();
        $ordershipping=Ordershipping::where('conforder_id',$request->conforder_id)->first();
        if($request->order_status =='Pending_Dispatch'){
            $this->SendSMS($request);
        }
        if($request->order_status =='Dispatched'){
			$this->SenddispatchedSMS($request);
		 }
        if($request->order_status =='Cancelled'){
			$this->CancelSendSMS($request);
		 }
       $this->StockUpdate($request->conforder_id,$request->order_status);
        if($request->delivered_by !=''){
           $order_status_result = Conforder::where('id', $request->conforder_id)->update(['delivery_by' => $request->delivered_by]);
        }
         $order_status_result = Conforder::where('id', $request->conforder_id)->update(['conforder_status' => $request->order_status,'conforder_statusdetails' => $request->orderstatusnote,'conforder_deliverydate' =>$request->delivery_date, 'pos_entry_date' => $request->pos_entry_date,'order_threepldlv' =>$request->ddlthreepldlv]);
         if ($request->order_status == 'Bkash_Payment_Receive'){
                $email_data['firstname']=$ordershipping->Shipping_txtfirstname;
                $email_data['lastname']=$ordershipping->Shipping_txtlastname;
                $email_data['conforder_tracknumber']="PLORDER#-".$request->conforder_id;
                Notification::route('mail' , $ordershipping->email)->notify(new bKashpaid($email_data));
         }elseif($request->order_status == 'Closed'){
             $shipping_address_details =$order->orderdetails($request->conforder_id);
             $order_products = $order_product->orderproducts($shipping_address_details->shoppingcart_id);
              foreach ($order_products as $order_info) {
                Product::where('id', $order_info->product_id)->increment('sale');
              }
             $dupli_cash_check=Cashtransaction::where('conforder_id',$request->conforder_id)->first();
             if(!$dupli_cash_check){
                 $cash=new Cashtransaction();
                 $cash->transaction_type='cash_in';
                 $cash->transfer_to='online';
                 $cash->conforder_id=$request->conforder_id;
                 $cash->amount=$request->total_amount;
                 $cash->issued_by=Auth::guard('admin')->user()->id;
                 $cash->save();
             }else{
                  Cashtransaction::where('conforder_id',$request->conforder_id)->update(['amount'=> $request->total_amount]);
             }
             $expense=new Cashexpenses();
             $expense->conforder_id=$request->conforder_id;
             if($request->expenses_amount ==''){
                 $expense->amount=0;
             }else{
                   $expense->amount=$request->expenses_amount;
             }
             $expense->expense_type='delivery_charge';
             $expense->created_by= Auth::guard('admin')->user()->id;
             $dupli_check=Cashexpenses::where('conforder_id',$request->conforder_id)->first();
             if(!$dupli_check){
                 $expense->save();
             }else{
                 Cashexpenses::where('conforder_id',$request->conforder_id)->update(['amount'=> $request->expenses_amount]);
             }
         }
         
          if ($request->ddlthreepldlv == 'Pathao') {
              //Pathao delivery
              $pathao_check=Deliveryhistories::where('conforder_id', $request->conforder_id)->first();
              if(!$pathao_check){
                $this->CreatePathao($request);
                return redirect("admin/order/details?id=$request->conforder_id")->with('update', 'Pathao status active.');
              }else{
                 return redirect("admin/order/details?id=$request->conforder_id")->with('update', 'Pathao already active.'); 
              }
          }elseif ($request->ddlthreepldlv == 'Sundorbon') {
              //sundorbon delivery
                $sundorbon_check=Deliveryhistories::where('conforder_id',$request->conforder_id)->first();
                if(!$sundorbon_check){
                    $delivery->delivey_by=$request->ddlthreepldlv;
                    $delivery->conforder_id=$request->conforder_id;
                    $delivery->assign_date=Carbon::now();
                    $delivery->save();
                }
                $datasundorbon['delivery_by']=4;
                Conforder::where('id',$request->conforder_id)->update(['delivery_by'=>4]);
                toastr()->success('Sundorbon status updated.');
                return redirect("admin/order/details/$request->conforder_id")->with('update', 'Sundorbon status updated.');
          }elseif($request->ddlthreepldlv == 'eCourier'){
              //eCourier delivery
              $eCourier_check=Deliveryhistories::where('conforder_id', $request->conforder_id)->first();
              if(!$eCourier_check){
                    $this->CreateeCourier($request);
                    toastr()->success('eCourier status updated.');
				    return redirect("admin/order/details/$request->conforder_id")->with('update', 'eCourier status updated');
              }else{
                  return redirect("admin/order/details?id=$request->conforder_id")->with('update', 'eCourier already active.'); 
              }
          }else{
             toastr()->success('status updated.');
             return redirect("admin/order/details?id=$request->conforder_id")->with('update', 'status updated.');
          }
         
    }
    
    public function exchange(Request $request){
       //dd($request);
        $exchange=Shoppinproduct::where('id',$request->shoppinproduct_id)->update(['productalbum_name'=> $request->new_productalbum_name,'prosize_name'=>$request->new_prosize_name,'shoppinproduct_quantity'=> $request->new_shoppinproduct_quantity]);
        if ($exchange) {
            $pre_change =Productsize::where('product_id', $request->product_id)->where('color_name', $request->productalbum_name)->where('productsize_size', $request->prosize_name)->first();
            $update_qty = $pre_change->SizeWiseQty + $request->shoppinproduct_quantity;
            $pre_change_update = Productsize::where('product_id', $pre_change->product_id)->where('color_name', $pre_change->color_name)->where('productsize_size', $pre_change->productsize_size)->update(['SizeWiseQty'=>$update_qty]);
            if ($pre_change_update) {
                $new_productalbum_name= $request->new_productalbum_name;
                $new_productsize_size = $request->new_prosize_name;
                $new_change =Productsize::where('product_id', $pre_change->product_id)->where('color_name', $request->new_productalbum_name)->where('productsize_size', $request->new_prosize_name)->first();
                $new_update_qty = $new_change->SizeWiseQty -  $request->new_shoppinproduct_quantity;
                $new_change_update = Productsize::where('product_id', $new_change->product_id)->where('color_name', $new_change->color_name)->where('productsize_size', $new_change->productsize_size)->update(['SizeWiseQty'=>$new_update_qty]);
                if ($new_change_update) {
                    $get_shoppingcart_id=Shoppinproduct::find($request->shoppinproduct_id);
                    $subtotal_sql = DB::select(DB::raw("select SUM(product_price*shoppinproduct_quantity)as total from shoppinproducts where shoppingcart_id='$get_shoppingcart_id->shoppingcart_id' and shoppinproduct_quantity!=0"));
                      foreach ($subtotal_sql as $cart_update_total) {
                                 $subtotal = $cart_update_total->total;
                     }
                    $cartinfo=Shoppingcart::find($get_shoppingcart_id->shoppingcart_id);
                    $cities=Cities::find($cartinfo->shipping_area);
                     if($subtotal < 3000){
                         if($cities->maping == 1){
                        $shipping_charge=150;
                        }elseif($cities->maping == 0){
                            $shipping_charge=100;
                        }
                        Shoppingcart::where('id', $get_shoppingcart_id->shoppingcart_id)->update(['shipping_charge' => $shipping_charge, 'shoppingcart_subtotal'=>$subtotal, 'shoppingcart_total' => $subtotal+$shipping_charge+$cartinfo->extra_charge]);
                     }else{
                         $shipping_charge=0;
                         Shoppingcart::where('id', $get_shoppingcart_id->shoppingcart_id)->update(['shipping_charge' => $shipping_charge, 'shoppingcart_subtotal'=>$subtotal, 'shoppingcart_total' => $subtotal+$shipping_charge+$cartinfo->extra_charge]);
                     }
                    toastr()->success('Exchange success.');
                    return redirect()->back()->with('update', 'Exchange success');
                } else {
                    toastr()->error('Exchange not success.');
                    return redirect()->back()->with('update', 'Exchange not success');
                }
            }
            
        }
         return redirect()->back()->with('update', 'Exchange not success');
       
   }
   
   public function UserExchangeApproved(Request $request){
        $shoppingproduct_id=$request->shoppinproduct_id;
		$old_product_info=Shoppinproduct::where('id',$shoppingproduct_id)->first();
		$old_product_id=$old_product_info->product_id;
		$old_product_color=$old_product_info->productalbum_name;
		$old_product_size=$old_product_info->prosize_name;
		$old_product_qty=$old_product_info->shoppinproduct_quantity;
		$stock_return=Productsize::where('product_id',$old_product_id)->where('color_name',$old_product_color)->where('productsize_size',$old_product_size)->update(['SizeWiseQty' => DB::raw("SizeWiseQty+1")]);
		if($stock_return){
			Shoppinproduct::where('id',$shoppingproduct_id)->update(['shoppinproduct_quantity' => 0]);
			$exchange_product=Exchange::where('shoppinproduct_id',$shoppingproduct_id)->first();
			$shopingp=new Shoppinproduct();
			$shopingp->shoppingcart_id=$exchange_product->shoppingcart_id;
			$shopingp->product_id=$exchange_product->product_id;
			$shopingp->product_barcode=$exchange_product->product_barcode;
			$shopingp->product_price=$exchange_product->product_price;
			$shopingp->prosize_name=$exchange_product->prosize_name;
			$shopingp->productalbum_name=$exchange_product->productalbum_name;
			$shopingp->shoppinproduct_quantity=$exchange_product->shoppinproduct_quantity;
			$shopingp->cart_image=$exchange_product->cart_image;
			$shopingp->exchange=1;
			$shopingp->save();
			Exchange::where('shoppinproduct_id',$shoppingproduct_id)->update(['exchnage_complete'=>1]);
			$stock=Productsize::where('product_id',$exchange_product->product_id)->where('color_name',$exchange_product->productalbum_name)->where('productsize_size',$exchange_product->prosize_name)->decrement('SizeWiseQty',$exchange_product->shoppinproduct_quantity);
			$get_shoppingcart_id=Shoppinproduct::find($request->shoppinproduct_id);
            $subtotal_sql = DB::select(DB::raw("select SUM(product_price*shoppinproduct_quantity)as total from shoppinproducts where shoppingcart_id='$get_shoppingcart_id->shoppingcart_id' and shoppinproduct_quantity!=0"));
              foreach ($subtotal_sql as $cart_update_total) {
                         $subtotal = $cart_update_total->total;
             }
            $cartinfo=Shoppingcart::find($get_shoppingcart_id->shoppingcart_id);
            if($cartinfo->voucher_id==1){
               $discount=$subtotal*10/100;
            }else{
                $discount=$cartinfo->shoppingcart_discount;
            }
            
            $cities=Cities::find($cartinfo->shipping_area);
             if(($subtotal-$discount) < 3000){
                 if($cities->maping == 1){
                $shipping_charge=150;
                }elseif($cities->maping == 0){
                    $shipping_charge=100;
                }
                Shoppingcart::where('id', $get_shoppingcart_id->shoppingcart_id)->update(['shipping_charge' => $shipping_charge, 'shoppingcart_subtotal'=>$subtotal, 'shoppingcart_total' => $subtotal+$shipping_charge+$cartinfo->extra_charge-$discount]);
             }else{
                 $shipping_charge=0;
                 Shoppingcart::where('id', $get_shoppingcart_id->shoppingcart_id)->update(['shipping_charge' => $shipping_charge, 'shoppingcart_subtotal'=>$subtotal, 'shoppingcart_total' => $subtotal+$shipping_charge+$cartinfo->extra_charge-$discount]);
             }
			return redirect()->back()->with('update', 'Exchange approved');
		}
		return redirect()->back()->with('error', 'Exchange not approved');
	}
	
	public function UserExchangeCancel(Request $request){
	    Exchange::where('shoppinproduct_id',$request->shoppinproduct_id)->update(['active'=>0]);
	    return redirect()->back()->with('update', 'Exchange cancel');
	}
   
   
   public function CanceIndivisuallQty($product_id, $ProColor, $ProSize, $ProQty, $shoppinproduct_id){
        $get_shoppingcart_id=Shoppinproduct::find($shoppinproduct_id);
        $update_result=Shoppinproduct::where('id', $shoppinproduct_id)->update(['shoppinproduct_quantity'=>0]);
        if ($update_result) {
             $subtotal_sql = DB::select(DB::raw("select SUM(product_price*shoppinproduct_quantity)as total from shoppinproducts where shoppingcart_id='$get_shoppingcart_id->shoppingcart_id' and shoppinproduct_quantity!=0"));
              foreach ($subtotal_sql as $cart_update_total) {
                         $subtotal = $cart_update_total->total;
             }
            $product_info = Productsize::where('product_id', '=', $product_id)->where('productsize_size', '=', $ProSize)->where('color_name', '=', $ProColor)->first();
            $SizeWiseQty= $ProQty + $product_info->SizeWiseQty;
            $update_Prosize_result =Productsize::where('product_id', $product_id)->where('color_name', $ProColor)->where('productsize_size', $ProSize)->update(['SizeWiseQty'=>$SizeWiseQty]);
            
            $cartinfo=Shoppingcart::find($get_shoppingcart_id->shoppingcart_id);
            $cities=Cities::find($cartinfo->shipping_area);
            if($subtotal < 3000){
                if($cities){
                    if($cities->maping == 1){
                        $shipping_charge=150;
                    }elseif($cities->maping == 0){
                        $shipping_charge=100;
                    }
                
                Shoppingcart::where('id', $get_shoppingcart_id->shoppingcart_id)->update(['shipping_charge' => $shipping_charge, 'shoppingcart_subtotal'=>$subtotal, 'shoppingcart_total' => $subtotal+$shipping_charge+$cartinfo->extra_charge]);
                echo 1;
                }
            }else{
               echo 1; 
            }
          } else {
            echo 0;
       }
   }
   
   public function AddNewProduct(Request $request){
       $data['shoppingcart_id']=$request->shoppingcart_id;
       $data['order']=Conforder::where('shoppingcart_id',$request->shoppingcart_id)->first();
       return view('admin.order.add-new-product',$data);
   }
   
   public function ExchangeSearchProduct($key){
		$data['search'] = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                ->join('cats', 'products.cat', '=', 'cats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id', 'products.cat', 'products.product_price', 'products.product_styleref', 'products.product_name','cats.name', 'productalbums.productalbum_name', 'productimgs.productimg_img', 'productimgs.productimg_img_medium')
                ->where('products.product_active_deactive', 0)
                ->where('products.product_name', 'like', "%{$key}%")
                ->orWhere('products.product_description', 'like', "%{$key}%")
                ->orWhere('products.product_styleref', 'like', "%{$key}%")
                ->groupBy('products.id')
                ->orderBy('products.id', 'DESC')
                ->take(10)
                ->get();
        $data['key']=$key;
        return view('admin.order.search-product', $data);
	}
	
	public function SearchProductinfo($key,$shoppingcart_id){
		$orderinfo=Conforder::where('shoppingcart_id',$shoppingcart_id)->first();
		$info=Ordershipping::where('conforder_id',$orderinfo->id)->first();
        $data['mobile_no'] = $info->Shipping_txtphone;
		$data['order_id_en'] = $orderinfo->id;
        $data['order_id']=$orderinfo->id;
        $data['shoppingcart_id']=$shoppingcart_id;
        $data['product_info'] = $this->Productinfo($key);
    	return view('admin.order.search-product-details', $data);
		
	}
	
	public function Productinfo($key){
		return $product_info = DB::table('products')
                ->select('products.product_name', 'products.product_img_thm','products.id', 'products.product_price as original_price', 'products.product_styleref')
                ->where('products.product_styleref', '=', $key)
                ->first();
	}
	
	public function AddNewProductSave(Request $request){
	    $stock_check=Productsize::where('product_id',$request->product_id)->where('color_name',$request->new_productalbum_name)->where('productsize_size',$request->new_prosize_name)->first();
	    if($stock_check->SizeWiseQty > 0){
    	    $ablum_info=Productalbum::where('product_id',$request->product_id)->where('productalbum_name',$request->new_productalbum_name)->first();
    		$album_id=$ablum_info->id;
    		$productimg=Productimg::where('productalbum_id',$album_id)->orderBy('productimg_order','ASC')->first();
    		$cart_image=$productimg->productimg_img_medium;
    	    $shoppinproduct=new Shoppinproduct();
    	    $shoppinproduct->shoppingcart_id=$request->shoppingcart_id;
    	    $shoppinproduct->product_id=$request->product_id;
    	    $shoppinproduct->product_barcode=$stock_check->barcode;
    	    $shoppinproduct->prosize_name=$request->new_prosize_name;
    	    $shoppinproduct->productalbum_name=$request->new_productalbum_name;
    	    $shoppinproduct->product_price=$request->product_price;
    	    $shoppinproduct->shoppinproduct_quantity=$request->new_shoppinproduct_quantity;
    	    $shoppinproduct->cart_image=$cart_image;
    	    $shoppinproduct->save();
    	    Productsize::where('product_id',$request->product_id)->where('color_name',$request->new_productalbum_name)->where('productsize_size',$request->new_prosize_name)->decrement('SizeWiseQty',$request->new_shoppinproduct_quantity);
    	    $subtotal_sql = DB::select(DB::raw("select SUM(product_price*shoppinproduct_quantity)as total from shoppinproducts where shoppingcart_id='$request->shoppingcart_id' and shoppinproduct_quantity!=0"));
              foreach ($subtotal_sql as $cart_update_total) {
                         $subtotal = $cart_update_total->total;
             }
            $cartinfo=Shoppingcart::find($request->shoppingcart_id);
            if($cartinfo->voucher_id==1){
               $discount=$subtotal*10/100;
            }else{
                $discount=$cartinfo->shoppingcart_discount;
            }
            
            $cities=Cities::find($cartinfo->shipping_area);
             if(($subtotal-$discount) < 3000){
                 if($cities->maping == 1){
                $shipping_charge=150;
                }elseif($cities->maping == 0){
                    if($cartinfo->shipping_charge==120){
                        $shipping_charge=$cartinfo->shipping_charge;
                    }else{
                    $shipping_charge=100;
                    }
                }
                Shoppingcart::where('id', $request->shoppingcart_id)->update(['shipping_charge' => $shipping_charge, 'shoppingcart_subtotal'=>$subtotal, 'shoppingcart_total' => $subtotal+$shipping_charge+$cartinfo->extra_charge-$discount]);
             }else{
                 if($cartinfo->shipping_charge==120){
                     $shipping_charge=$cartinfo->shipping_charge;
                 }else{
                     $shipping_charge=0;
                 }
                 Shoppingcart::where('id', $request->shoppingcart_id)->update(['shipping_charge' => $shipping_charge, 'shoppingcart_subtotal'=>$subtotal, 'shoppingcart_total' => $subtotal+$shipping_charge+$cartinfo->extra_charge-$discount]);
             }
    	    return 1;
	    }else{
	        return 0;
	    }
	}
   
   public function changepayment(Request $request){
       Shoppingcart::where('id',$request->shoppingcart_id)->update(['payment_method'=> $request->payment_mode]);
   }
   
   public function extracharge(Request $request){
       $info=Shoppingcart::find($request->shoppingcart_id);
       $total=$info->shoppingcart_subtotal + $request->charge + $info->shipping_charge;
       Shoppingcart::where('id',$request->shoppingcart_id)->update(['extra_charge'=> $request->charge,'shoppingcart_total'=>$total]);
       return $total;
   }
    
    public function SendSMS($request){
	$order_status=$request->order_status;
	$confoorderid=$request->conforder_id;
	$get_delivery_date=$request->delivery_date;
	$today=date("Y-m-d");
	$bitly_link = app('bitly')->getUrl('https://pride-limited.com/track-order'); 
	//$bitly_link = 'https://pride-limited.com/track-order';
	if($order_status =='Pending_Dispatch'){
		//conforder details
		$order_detials = Conforder::find($confoorderid);
		//get guest customer details
		$user_details = Ordershipping::where('conforder_id', $confoorderid)->first();
		if($get_delivery_date !=''){
			$delivery_date=$order_detials->conforder_deliverydate;
			if($delivery_date == NULL){
			   //send sms for delivery date set || new date set
			   $date=strtotime($request->delivery_date);
			   $order_date=date('d M',$date);
				$user_mobile = "88" . $user_details->Shipping_txtphone;
				if($request->ddlthreepldlv == 'Pathao'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with PATHAO.
				Please be available on your phone to receive any calls from our logistics.";
				}else if($request->ddlthreepldlv == 'eCourier'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with E-COURIER.
				Track your order here: $bitly_link";
				}else if($request->ddlthreepldlv == 'Sundorbon'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with Sundorbon.
				Please be available on your phone to receive any calls from our logistics.";
				}else{
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber is verified and will be delivered on the $order_date, at any time before 8pm BST.
				Please be available on your phone to receive any calls from our logistics.";
				}
				$user = "Pride";
				$pass = "xA33I127";
				$sid = "PrideLtdEng";
				$url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
				$param = "user=$user&pass=$pass&sms[0][0]=$user_mobile&sms[0][1]=" . urlencode($sms_text) . "&sms[0][2]=1234567890&sid=$sid";
				$crl = curl_init();
				curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($crl, CURLOPT_URL, $url);
				curl_setopt($crl, CURLOPT_HEADER, 0);
				curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($crl, CURLOPT_POST, 1);
				curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
				$response = curl_exec($crl);
				curl_close($crl);
			  //end send sms 
				
			}else if($get_delivery_date == $delivery_date){
				//sms not send || today equal delivery date
				
			}else if($get_delivery_date != $delivery_date && $get_delivery_date > $delivery_date){
				//send sms update delivery time || update date
			   $date=strtotime($request->delivery_date);
			   $order_date=date('d M',$date);
			   //dd($order_date);
				$user_mobile = "88" . $user_details->Shipping_txtphone;
				if($request->ddlthreepldlv == 'Pathao'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with PATHAO.
				Please be available on your phone to receive any calls from our logistics.";
				}else if($request->ddlthreepldlv == 'eCourier'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with E-COURIER.
				Please be available on your phone to receive any calls from our logistics.";
				}else if($request->ddlthreepldlv == 'Sundorbon'){
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber has been dispatched with Sundorbon.
				Please be available on your phone to receive any calls from our logistics.";
				}else{
				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
				Your order $order_detials->conforder_tracknumber is verified and will be delivered on the $order_date, at any time before 8pm BST.
				Please be available on your phone to receive any calls from our logistics.";
				}
				$user = "Pride";
				$pass = "xA33I127";
				$sid = "PrideLtdEng";
				$url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
				$param = "user=$user&pass=$pass&sms[0][0]=$user_mobile&sms[0][1]=" . urlencode($sms_text) . "&sms[0][2]=1234567890&sid=$sid";
				$crl = curl_init();
				curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
				curl_setopt($crl, CURLOPT_URL, $url);
				curl_setopt($crl, CURLOPT_HEADER, 0);
				curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($crl, CURLOPT_POST, 1);
				curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
				$response = curl_exec($crl);
				curl_close($crl);
			  //end send sms 
			 }
	   }
	 }
   }
   
   public function SenddispatchedSMS($request){
        $order_status=$request->ddlstatus;
    	$confoorderid=$request->conforder_id;
    	$get_delivery_date=$request->delivery_date;
    	$today=date("Y-m-d");
    	$bitly_link = 'https://pride-limited.com/track/order';
    	if($order_status =='Dispatched'){
    	    $order_detials = Conforder::find($confoorderid);
    	    $user_details = Ordershipping::where('conforder_id', $confoorderid)->first();
    	    if($get_delivery_date !=''){
    	        $delivery_date=$order_detials->conforder_deliverydate;
    	        if($delivery_date == NULL){
    	            //send sms for delivery date set || new date set
    			   $date=strtotime($request->delivery_date);
    			   $order_date=date('d M,',$date);
    				$user_mobile = "88" . $user_details->Shipping_txtphone;
    				if($request->ddlthreepldlv == 'Pathao'){
    				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
Your order $order_detials->conforder_tracknumber has been dispatched with PATHAO.
Please be available on your phone to receive any calls from our logistics.";
    				}else if($request->ddlthreepldlv == 'eCourier'){
    				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
Your order $order_detials->conforder_tracknumber has been dispatched with E-COURIER.
Track your order here: $bitly_link";
    				}else if($request->ddlthreepldlv == 'Sundorbon'){
    				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
Your order $order_detials->conforder_tracknumber has been dispatched with Sundorbon.
Please be available on your phone to receive any calls from our logistics.";
    				}else{ 
    				$sms_text = null;
    				} 
    	        }else if($get_delivery_date == $delivery_date){
    	            //sms not send || today equal delivery date
    	            $sms_text = null;
    	        }else if($get_delivery_date != $delivery_date && $get_delivery_date > $delivery_date){
    	            //send sms update delivery time || update date
    	            $date=strtotime($request->delivery_date);
    			   $order_date=date('d M',$date);
    			   //dd($order_date);
    				$user_mobile = "88" . $user_details->Shipping_txtphone;
    				if($request->ddlthreepldlv == 'Pathao'){
    				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
Your order $order_detials->conforder_tracknumber has been dispatched with PATHAO.
Please be available on your phone to receive any calls from our logistics.";
    				}else if($request->ddlthreepldlv == 'eCourier'){
    				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
Your order $order_detials->conforder_tracknumber has been dispatched with E-COURIER.
Please be available on your phone to receive any calls from our logistics.";
    				}else if($request->ddlthreepldlv == 'Sundorbon'){
    				$sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
Your order $order_detials->conforder_tracknumber has been dispatched with Sundorbon.
Please be available on your phone to receive any calls from our logistics.";
    				}else{ 
    				$sms_text = null;
    				} 
    	        }else{
    	           $sms_text = null; 
    	        }
    	        
    	        if($sms_text !=null){
    	            $user = "Pride";
    				$pass = "xA33I127";
    				$sid = "PrideLtdEng";
    				$url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
    				$param = "user=$user&pass=$pass&sms[0][0]=$user_mobile&sms[0][1]=" . urlencode($sms_text) . "&sms[0][2]=1234567890&sid=$sid";
    				$crl = curl_init();
    				curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
    				curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
    				curl_setopt($crl, CURLOPT_URL, $url);
    				curl_setopt($crl, CURLOPT_HEADER, 0);
    				curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
    				curl_setopt($crl, CURLOPT_POST, 1);
    				curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
    				$response = curl_exec($crl);
    				curl_close($crl);
    	        }
    	    }
    	}
   }
   
   public function CancelSendSMS($request){
       $confoorderid=$request->conforder_id;
       $order_detials = Conforder::find($confoorderid);
       $user_details = Ordershipping::where('conforder_id', $confoorderid)->first();
       $user_mobile = "88" . $user_details->Shipping_txtphone;
       if($order_detials->conforder_status !='Cancelled'){
       $sms_text = "Dear $user_details->Shipping_txtfirstname $user_details->Shipping_txtlastname,
Your order $order_detials->conforder_tracknumber is now cancelled.

We hope you will shop with us again soon!

-Team Pride";
        $user = "Pride";
		$pass = "xA33I127";
		$sid = "PrideLtdEng";
		$url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
		$param = "user=$user&pass=$pass&sms[0][0]=$user_mobile&sms[0][1]=" . urlencode($sms_text) . "&sms[0][2]=1234567890&sid=$sid";
		$crl = curl_init();
		curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($crl, CURLOPT_URL, $url);
		curl_setopt($crl, CURLOPT_HEADER, 0);
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($crl, CURLOPT_POST, 1);
		curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
		$response = curl_exec($crl);
		curl_close($crl);
       }
   }
   
   public function CreatePathao($request){
            $confoorderid=$request->conforder_id;
            $curl = curl_init();
            $token_postdata = [
                'client_id' => '1113',
                'client_secret' => 'r9PGPS4PzVWCXpbiTBM2u5iGzqnr7vyLpYT3XS2d',
                'username' => 'sumbalmomen@gmail.com',
                'password' => 'delivery@pride2020',
                'grant_type' => 'password'
            ];
           curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-hermes.pathaointernal.com/aladdin/api/v1/issue-token", //api url call here
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($token_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json"
                ),
            ));
            $token_response = curl_exec($curl);
            $result = json_decode($token_response);
            $access_token = $result->access_token;
			$payment_method=$request->payment_method;
            if($payment_method =='CCRD'){
                $totalamount=0;
            }elseif($payment_method =='MPAY'){
                $totalamount=0;
            }else{
               $totalamount=$request->total_amount; ; 
            }
            $order_data=Conforder::find($request->conforder_id);
            $order=new Conforder();
            $shipping_address_details = $order->orderdetails($request->conforder_id);
            $delivery_postdata = [
                'store_id' =>23421,
				'merchant_order_id'=>$order_data->conforder_tracknumber,
                'recipient_name' => $shipping_address_details->Shipping_txtfirstname . ' ' . $shipping_address_details->Shipping_txtlastname,
                'recipient_phone' => $shipping_address_details->Shipping_txtphone,
				'recipient_address' => $shipping_address_details->Shipping_txtaddressname,
                'recipient_city' => $request->pathao_city_id,
                'recipient_zone' => $request->pathao_zone_id,
                'recipient_area' => $request->pathao_area_id,
				'delivery_type' => 48,
				'item_type' => 2,
				'special_instruction' => $order_data->conforder_deliverynotes,
				'item_quantity' =>$request->total_loop,
				'item_weight' =>0.5,
				'amount_to_collect' =>$totalamount,
				'item_description' =>''
            ];
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-hermes.pathaointernal.com/aladdin/api/v1/orders",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($delivery_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "Authorization: Bearer " . $access_token
                ),
            ));
 
            $delivery_response = curl_exec($curl);
            //dd($delivery_response);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                dd($err);
                echo "cURL Error #:" . $err;
            } else {
                $json = json_decode($delivery_response);
                //dd($json->data->consignment_id);
                $delivery=new Deliveryhistories();
    			$delivery->delivey_by=$request->ddlthreepldlv;
    			$delivery->ecr_number=$json->data->consignment_id;
                $delivery->conforder_id=$request->conforder_id;
                $delivery->assign_date=Carbon::now();
                $delivery->save();
                Conforder::where('id',$request->conforder_id)->update(['delivery_by'=>3,'order_threepldlv'=>$request->ddlthreepldlv]);
            }

   }
   public function CreateeCourier($request){
      // dd($request);
       Conforder::where('id',$request->conforder_id)->update(['delivery_by'=>5,'order_threepldlv'=>$request->ddlthreepldlv]);
        $order_data=Conforder::find($request->conforder_id);
        if($request->payment_method =='CCRD'){
            $totalamount=0;
        }elseif($request->payment_method =='MPAY'){
            $totalamount=0;
        }else{
           $totalamount=$request->total_amount; 
        }
       $order=new Conforder();
       $curl = curl_init();
	   $shipping_address_details = $order->orderdetails($request->conforder_id);
	   $track_number=$order_data->conforder_tracknumber;
	   $package_code=$request->package_code;
	   $payment_method=$request->payment_method;
	   $number_of_product=$request->total_loop;
		$eCdelivery_postdata = [
            'parcel' => 'insert',
            'recipient_name' => $shipping_address_details->Shipping_txtfirstname . ' ' . $shipping_address_details->Shipping_txtlastname,
            'recipient_mobile' => $shipping_address_details->Shipping_txtphone,
            'recipient_city' => $request->ecourier_city,
			'recipient_thana' => $request->ecourier_thana,
			'recipient_area' => $request->ecourier_area,
            'recipient_address' => $shipping_address_details->Shipping_txtaddressname,
            'package_code' => $package_code,
			'payment_method' => $payment_method,
			'recipient_zip' =>$request->ecourier_postcode,
            'product_price' => $totalamount,
			'product_id' => $track_number,
			'number_of_item' => $number_of_product
        ];
		//dd($eCdelivery_postdata);
		//https://staging.ecourier.com.bd/api/order-place
		//https://backoffice.ecourier.com.bd/api/order-place
		
		curl_setopt_array($curl, array(
            CURLOPT_URL => "https://staging.ecourier.com.bd/api/order-place",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 3000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($eCdelivery_postdata),
            CURLOPT_HTTPHEADER => array(
                "API-SECRET:zReJG",
                "API-KEY: 66eY",
                "USER-ID:I7155",
				"Content-Type:application/json"
            ),
        ));
		$delivery_response = curl_exec($curl);
		//dd($delivery_response);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
			//  dd($delivery_response);
			//  exit;
			
			$json = json_decode($delivery_response);
			$delivery=new Deliveryhistories();
			$delivery->delivey_by=$request->ddlthreepldlv;
			$delivery->ecr_number=$json->ID;
            $delivery->conforder_id=$request->conforder_id;
            $delivery->package_code=$package_code;
            $delivery->assign_date=Carbon::now();
            $delivery->save();
		}
   }
   
   public function eCourierCancelOrder($ecr){
        $curl = curl_init();
		$cancel_postdata = [
            'tracking' => $ecr,
            'comment' => 'test order'
        ];
		curl_setopt_array($curl, array(
            CURLOPT_URL => "https://backoffice.ecourier.com.bd/api/cancel-order",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 3000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($cancel_postdata),
            CURLOPT_HTTPHEADER => array(
                "API-SECRET:zReJG",
                "API-KEY: 66eY",
                "USER-ID:I7155",
				"Content-Type:application/json"
            ),
        ));
		$cancel_response = curl_exec($curl);
		//dd($delivery_response);
        $err = curl_error($curl);
		curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $info=Deliveryhistories::where('ecr_number',$ecr)->first();
            if($info){
               Conforder::where('id',$info->conforder_id)->update(['delivery_by'=>'','order_threepldlv'=>'']);
               $delivery_del = Deliveryhistories::find($info->id);
               $delivery_del->delete();
               
               toastr()->error('eCourier cancel.');
               return redirect("admin/order/details?id=$info->conforder_id")->with('update', 'eCourier cancel.');
               //return redirect("admin/order/details/$info->conforder_id")->with('update', 'eCourier cancel.');
			   //dd($cancel_response);
            }
		}
   }
   
   
   public function StockUpdate($confoorderid,$order_status){
           $order=new Conforder();
           $order_product=new Shoppinproduct();
           $shipping_address_details =$order->orderdetails($confoorderid);
           $order_products = $order_product->orderproducts($shipping_address_details->shoppingcart_id);
           $check=Conforder::find($confoorderid);
           if($check->conforder_status =='Invalidate' || $check->conforder_status =='Returned' || $check->conforder_status =='Cancelled'){
               if($order_status=='Pending_Dispatch'){
                   foreach ($order_products as $order_info) {
                        $result = Productsize::where('product_id',$order_info->product_id)->where('productsize_size',$order_info->prosize_name)->where('color_name',$order_info->productalbum_name)->first();
                         if($result->SizeWiseQty > 0){
                           $pro_SizeWiseQty = $result->SizeWiseQty - $order_info->shoppinproduct_quantity;
                           //dd($pro_SizeWiseQty);
                           Productsize::where('product_id', $order_info->product_id)->where('color_name', $result->color_name)->where('productsize_size', $result->productsize_size)->update(['SizeWiseQty' => $pro_SizeWiseQty]);
                         }
                    }
               }
           }else{
                if ($order_status == 'Invalidate' || $order_status == 'Returned' || $order_status == 'Cancelled') {
                    foreach ($order_products as $order_info) {
                        $result = Productsize::where('product_id',$order_info->product_id)->where('productsize_size',$order_info->prosize_name)->where('color_name',$order_info->productalbum_name)->first();
                        $pro_SizeWiseQty = $result->SizeWiseQty + $order_info->shoppinproduct_quantity;
                        Productsize::where('product_id', $order_info->product_id)->where('color_name', $result->color_name)->where('productsize_size', $result->productsize_size)->update(['SizeWiseQty' => $pro_SizeWiseQty]);
                    }
                }
           }
    }
    
    public function incompleteorder(){
        $order=new Conforder();
        $data['orders']=$order->incompleteorder();
        return view('admin.order.incomplete', $data);
    }
    
    protected function orderlist(){
        $order=new Conforder();
        $data['orders']=$order->orders();
        return view('admin.order.list', $data);
    }
}
