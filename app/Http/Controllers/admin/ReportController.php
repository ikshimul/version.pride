<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;
use DB;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function __construct() {
       // $this->middleware('auth:admin');
        $this->middleware('AdminAuth');
    }
    
    public function sale(Request $request){
        $query_scope = DB::table('conforders')
                ->join('ordershippings', 'conforders.id', '=', 'ordershippings.conforder_id')
                ->join('shoppingcarts', 'conforders.shoppingcart_id', '=', 'shoppingcarts.id')
                ->join('shoppinproducts', 'shoppingcarts.id','=', 'shoppinproducts.shoppingcart_id')
                ->leftJoin('cashexpenses', 'conforders.id', '=', 'cashexpenses.conforder_id')
                ->leftjoin('users', 'conforders.user_id', '=', 'users.id')
                ->select('conforders.id','conforders.conforder_tracknumber', 'conforders.conforder_deliverydate', 'conforders.conforder_status', 'conforders.conforder_placed_date','conforders.created_at','conforders.pos_entry_date','conforders.order_threepldlv', 'users.name', 'shoppingcarts.shipping_Charge', 'shoppingcarts.extra_charge', 'shoppingcarts.shoppingcart_subtotal', 'shoppingcarts.shoppingcart_total', 'shoppingcarts.payment_method','cashexpenses.amount','ordershippings.Shipping_txtfirstname','ordershippings.Shipping_txtlastname');
        $order_status = 'Order_Verification_Pending';
        if(isset($request->order_status)) {
            $order_status = $request->order_status == ''?'%%':$request->order_status;
        }
        if(isset($request->from) && isset($request->to) && !empty($request->from) && !empty($request->to) && empty($request->ddlthreepldlv)) {
            $from = $request->from;
            $to = $request->to;
            $order_threepldlv='';
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->whereBetween('conforders.created_at', array($from, $to))
                ->orderBy('conforders.id', 'desc')
                ->groupBy('conforders.id')
                ->get();
            return view('admin.report.sale', compact('sale_reports', 'from', 'to','order_threepldlv', 'order_status'));
        }elseif(isset($request->from) && isset($request->to) && isset($request->ddlthreepldlv) && !empty($request->from) && !empty($request->to) && !empty($request->ddlthreepldlv)){
            $from = $request->from;
            $to = $request->to;
            $order_threepldlv=$request->ddlthreepldlv;
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->where('shoppingcarts.payment_method', 'like', $order_threepldlv)
                ->whereBetween('conforders.created_at', array($from, $to))
                ->orderBy('conforders.id', 'desc')
                ->groupBy('conforders.id')
                ->get();
            return view('admin.report.sale', compact('sale_reports', 'from', 'to','order_threepldlv', 'order_status'));
        } elseif(isset($request->from) && !empty($request->from)) {
            $from = $request->from;
            $order_threepldlv='';
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->where('conforders.created_at', '>=', $from)
                ->orderBy('conforders.id', 'desc')
                ->groupBy('conforders.id')
                ->get();
            return view('admin.report.sale', compact('sale_reports', 'from', 'to','order_threepldlv', 'order_status'));
        }elseif(isset($request->ddlthreepldlv) && !empty($request->ddlthreepldlv)){
            $from=date('Y-m-d');
            $to=date('Y-m-d');
             $order_threepldlv=$request->ddlthreepldlv;
             $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->where('shoppingcarts.payment_method', 'like', $order_threepldlv)
                ->orderBy('conforders.id', 'desc')
                ->groupBy('conforders.id')
                ->get();
            return view('admin.report.sale', compact('sale_reports','from', 'to','order_threepldlv', 'order_status'));
        } else {
            $from=date('Y-m-d');
            $to=date('Y-m-d');
            $order_threepldlv='';
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->orderBy('conforders.id', 'desc')
                ->groupBy('conforders.id')
                ->get();
          //  dd($sale_reports);
           return view('admin.report.sale', compact('sale_reports','from', 'to','order_threepldlv', 'order_status'));
        }
    }
    
    public function salePdf(Request $request){
        $query_scope = DB::table('conforders')
                ->join('ordershippings', 'conforders.id', '=', 'ordershippings.conforder_id')
                ->join('shoppingcarts', 'conforders.shoppingcart_id', '=', 'shoppingcarts.id')
                ->join('shoppinproducts', 'shoppingcarts.id','=', 'shoppinproducts.shoppingcart_id')
                ->leftJoin('cashexpenses', 'conforders.id', '=', 'cashexpenses.conforder_id')
                ->leftjoin('users', 'conforders.user_id', '=', 'users.id')
                ->select('conforders.id','conforders.conforder_tracknumber', 'conforders.conforder_deliverydate', 'conforders.conforder_status', 'conforders.conforder_placed_date','conforders.created_at','conforders.pos_entry_date','conforders.order_threepldlv', 'users.name', 'shoppingcarts.shipping_Charge', 'shoppingcarts.extra_charge', 'shoppingcarts.shoppingcart_subtotal', 'shoppingcarts.shoppingcart_total', 'shoppingcarts.payment_method','cashexpenses.amount','ordershippings.Shipping_txtfirstname','ordershippings.Shipping_txtlastname');
        $order_status = 'Order_Verification_Pending';
        if(isset($request->order_status)) {
            $order_status = $request->order_status == ''?'%%':$request->order_status;
        }
        if(isset($request->from) && isset($request->to) && !empty($request->from) && !empty($request->to) && empty($request->ddlthreepldlv)) {
            $from = $request->from;
            $to = $request->to;
            $order_threepldlv='';
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->whereBetween('conforders.created_at', array($from, $to))
                ->orderBy('conforders.id', 'desc')
                ->groupBy('conforders.id')
                ->get();
            $pdf = PDF::loadView('admin.report.salePDF', compact('sale_reports'));
            $current = Carbon::now()->format('YmdHs').'- sale report.pdf';
            return $pdf->download($current);
        }elseif(isset($request->from) && isset($request->to) && isset($request->ddlthreepldlv) && !empty($request->from) && !empty($request->to) && !empty($request->ddlthreepldlv)){
            $from = $request->from;
            $to = $request->to;
            $order_threepldlv=$request->ddlthreepldlv;
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->where('shoppingcarts.payment_method', 'like', $order_threepldlv)
                ->whereBetween('conforders.created_at', array($from, $to))
                ->orderBy('conforders.id', 'desc')
                ->groupBy('conforders.id')
                ->get();
            $pdf = PDF::loadView('admin.report.salePDF', compact('sale_reports'));
            $current = Carbon::now()->format('YmdHs').'- sale report.pdf';
            return $pdf->download($current);
        } elseif(isset($request->from) && !empty($request->from)) {
            $from = $request->from;
            $order_threepldlv='';
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->where('conforders.created_at', '>=', $from)
                ->orderBy('conforders.id', 'desc')
                ->groupBy('conforders.id')
                ->get();
            $pdf = PDF::loadView('admin.report.salePDF', compact('sale_reports'));
            $current = Carbon::now()->format('YmdHs').'- sale report.pdf';
            return $pdf->download($current);
        }elseif(isset($request->ddlthreepldlv) && !empty($request->ddlthreepldlv)){
            $from=date('Y-m-d');
            $to=date('Y-m-d');
             $order_threepldlv=$request->ddlthreepldlv;
             $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->where('shoppingcarts.payment_method', 'like', $order_threepldlv)
                ->orderBy('conforders.id', 'desc')
                ->groupBy('conforders.id')
                ->get();
            $pdf = PDF::loadView('admin.report.salePDF', compact('sale_reports'));
            $current = Carbon::now()->format('YmdHs').'- sale report.pdf';
            return $pdf->download($current);
        } else {
            $from=date('Y-m-d');
            $to=date('Y-m-d');
            $order_threepldlv='';
            $sale_reports = $query_scope
                ->where('conforder_status', 'like', $order_status)
                ->orderBy('conforders.id', 'desc')
                ->groupBy('conforders.id')
                ->get();
          //  dd($sale_reports);
          $pdf = PDF::loadView('admin.report.salePDF', compact('sale_reports'));
          $current = Carbon::now()->format('YmdHs').'- sale report.pdf';
          return $pdf->download($current);
        }
    }
}
