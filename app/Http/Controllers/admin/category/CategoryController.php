<?php

namespace App\Http\Controllers\admin\category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Image;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use App\Models\Maincat;
use App\Models\Procat;
use App\Models\Cat;
use App\Models\Subprocat;
use App\Models\Subcat;

class CategoryController extends Controller
{
    public function __construct() {
        $this->middleware('AdminAuth');
    }
    
    public function Maincat(){
        $data['maincats']=Maincat::all();
        return view('admin.category.maincat',$data);
    }
    
    public function MaincatSave(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'name' => 'required|unique:maincats,name',
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $maincat=new Maincat();
            $maincat->name=$request->name;
            $maincat->slug=$request->slug;
            $result =$maincat->save();
            return redirect()->back()->with('save', 'Create successfully');
            }
    }
    
    public function MaincatEdit($id){
        $data['maincat']=Maincat::find($id);
        return view('admin.category.maincat_edit',$data);
    }
    
    public function Maincatupdate(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'name' => 'required',
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $maincat=Maincat::find($request->id);
            $maincat->name=$request->name;
            $maincat->slug=$request->slug;
            $result =$maincat->save();
            return redirect()->back()->with('save', 'Update successfully');
            }
    }
    
    protected function Maincatactive($id){
        $maincat=Maincat::find($id);
        $maincat->active=1;
        $maincat->save();
        return redirect()->back()->with('update', 'Active successfully');
    }
    
    protected function Maincatdeactive($id){
        $maincat=Maincat::find($id);
        $maincat->active=0;
        $maincat->save();
        return redirect()->back()->with('deactive', 'Deactive successfully');
    }
    
    public function Procat(){
        $data['maincats']=Maincat::where('active',1)->get();
        $data['procats']=DB::table('cats')
            ->join('maincats', 'maincats.id', '=', 'cats.maincat')
            ->select('cats.*', 'maincats.name as maincat_name')
            ->get();
        return view('admin.category.procat',$data);
    }
    
    public function ProcatSave(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'name' => 'required|unique:cats,name',
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $procat=new Cat();
            $procat->maincat=$request->maincat_id;
            $procat->name=$request->name;
            $procat->slug=$request->slug;
            $result =$procat->save();
            return redirect()->back()->with('save', 'Create successfully');
            }
    }
    
    public function Procatedit($id){
        $data['maincats']=Maincat::where('active',1)->get();
        $data['procat']=Cat::find($id);
        return view('admin.category.procat_edit',$data);
    }
    
    public function Procatupdate(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'name' => 'required',
        ]);
        if ($vaildation->fails()) {
            // dd($vaildation);
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $procat=Cat::find($request->id);
            $procat->maincat=$request->maincat_id;
            $procat->name=$request->name;
            $procat->slug=$request->slug;
            $result =$procat->save();
            return redirect()->back()->with('save', 'Update successfully');
            }
    }
    
    protected function Procatactive($id){
        $procat=Cat::find($id);
        $procat->active=1;
        $procat->save();
        return redirect()->back()->with('update', 'Active successfully');
    }
    
    protected function Procatdeactive($id){
        $procat=Cat::find($id);
        $procat->active=0;
        $procat->save();
        return redirect()->back()->with('deactive', 'Deactive successfully');
    }
    
    public function Subprocat(){
         $data['maincats']=Maincat::where('active',1)->get();
         $data['procats']=DB::table('cats')
            ->join('maincats', 'maincats.id', '=', 'cats.maincat')
            ->select('cats.*', 'maincats.name as maincat_name','maincats.id as maincat_id')
            ->get();
        $data['list']=DB::table('subcats')
            ->join('maincats', 'maincats.id', '=', 'subcats.maincat')
            ->join('cats', 'cats.id', '=', 'subcats.cat')
            ->select('subcats.*', 'maincats.name as maincat_name','maincats.id as maincat_id','cats.name as procat_name')
            ->get();
        return view('admin.category.subprocat',$data);
    }
    
    public function SubprocatSave(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'name' => 'required|unique:subcats,name',
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $imagesavename = time() . '-' . $file->getClientOriginalName();
                $file->move('storage/app/public/category/', $imagesavename);
            }else{
               $imagesavename=''; 
            }
            $subprocat=new Subcat();
            $subprocat->maincat=$request->maincat_id;
            $subprocat->cat=$request->procat_id;
            $subprocat->name=$request->name;
            $subprocat->slug=$request->slug;
            $subprocat->subcat_banner=$imagesavename;
            $result =$subprocat->save();
            return redirect()->back()->with('save', 'Create successfully');
        }
    }
    
    public function Subprocatedit($id){
        $data['maincats']=Maincat::where('active',1)->get();
        $data['procats']=Cat::where('active',1)->get();
        $data['subprocat']=Subcat::find($id);
        return view('admin.category.subprocat_edit',$data);
    }
    
    public function Subprocatupdate(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'name' => 'required',
        ]);
        if ($vaildation->fails()) {
            // dd($vaildation);
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $subprocat=Subcat::find($request->id);
            $subprocat->maincat=$request->maincat_id;
            $subprocat->cat=$request->procat_id;
            $subprocat->name=$request->name;
            $subprocat->slug=$request->slug;
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $imagesavename = time() . '-' . $file->getClientOriginalName();
                $file->move('storage/app/public/category/', $imagesavename);
                $subprocat->subcat_banner=$imagesavename;
            }
            $result =$subprocat->save();
            return redirect()->back()->with('save', 'Create successfully');
        }
    }
    
    protected function Subprocatactive($id){
        $subpro=Subcat::find($id);
        $subpro->active=1;
        $subpro->save();
        return redirect()->back()->with('update', 'Active Successfully.');
    }
    
    protected function Subprocatdeactive($id){
        $subpro=Subcat::find($id);
        $subpro->active=0;
        $subpro->save();
        return redirect()->back()->with('deactive', 'Deactive Successfully.');
    }
}
