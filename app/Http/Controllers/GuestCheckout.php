<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cookie;
use Cart;
use DB;
use App\Models\User;
use App\Models\Region;
use App\Models\Shoppingcart;
use App\Models\Shoppinproduct;
use App\Models\Conforder;
use App\Models\Ordershipping;
use App\Models\Productsize;
use App\Models\Cities;
use App\Models\Voucher;
use App\Models\Uservoucher;
use App\Mail\Invoicedetails;

class GuestCheckout extends Controller
{
    public function index(){
        $data['regions']=Region::all();
        return view('guest_checkout',$data);
    }
    
    public function saveorder(Request $request){
        $discount=0;
        //shopping cart save data
        $shoppingcart=new Shoppingcart();
        $shoppingcart->shipping_charge=$request->shipping_charge;
        $shoppingcart->shoppingcart_subtotal=$request->shoppingcart_subtotal;
        $shoppingcart->shoppingcart_discount=$discount;
        $shoppingcart->shoppingcart_total=$request->shoppingcart_total;
        $shoppingcart->payment_method=$request->dmselect;
        $shoppingcart->delivery_method=$request->delivery_type;
        $shoppingcart->shipping_area=$request->CityList;
        $shoppingcart->save();
        $shoppingcart_id=$shoppingcart->id;
        
        //shoppinproduct save data
        $total=0;
        foreach (Cart::instance('products')->content() as $row){
            $shoppinproduct = new Shoppinproduct();
            $shoppinproduct->shoppingcart_id=$shoppingcart_id;
            $shoppinproduct->product_id=$row->id;
            $shoppinproduct->product_barcode=$row->options->barcode;
            $shoppinproduct->prosize_name=$row->options->size;
            $shoppinproduct->productalbum_name=$row->options->color;
            $shoppinproduct->product_price=$row->price;
            $shoppinproduct->shoppinproduct_quantity=$row->qty;
            $shoppinproduct->cart_image=$row->options->product_image;
            $shoppinproduct->save();
            $total+=$row->price*$row->qty;
            
            // Reduce product quantity 
            $pro_qty = Productsize::where('product_id', $row->id)->where('color_name', $row->options->color)->where('productsize_size', $row->options->size)->first();
            $reduce_data['SizeWiseQty'] = $pro_qty->SizeWiseQty - $row->qty;
            Productsize::where('product_id', $pro_qty->product_id)->where('color_name', $pro_qty->color_name)->where('productsize_size', $pro_qty->productsize_size)->update($reduce_data);
        }
        
        //Conforder data save
        $conforder=new Conforder();
        $conforder->user_id=0;
        $conforder->shoppingcart_id=$shoppingcart_id;
        $conforder->conforder_deliverynotes=$request->conforder_deliverynotes;
        $conforder->conforder_status='Order_Verification_Pending';
        $conforder->save();
        $conforder_id=$conforder->id;
        $conforder_update = Conforder::find($conforder_id);
        $conforder_update->conforder_tracknumber = "PLORDER#100-" . $conforder_id;
        $conforder_update->save();
        
        //Ordershipping data save
        $rdershipping = new Ordershipping();
        $rdershipping->conforder_id=$conforder_id;
        $rdershipping->email=$request->email;
        $rdershipping->Shipping_txtaddressname=$request->address;
        $rdershipping->Shipping_txtfirstname=$request->firstname;
        $rdershipping->Shipping_txtlastname=$request->lastname;
        $rdershipping->Shipping_txtcity=$request->CityList;
        $rdershipping->Shipping_txtzipcode=$request->zip;
        $rdershipping->Shipping_txtphone=$request->mobile_no;
        $rdershipping->save();
        
        $total=$total-$discount+$request->shipping_charge;
        if($total != $request->shoppingcart_total){
            return redirect()->back()->with('payment-msg', 'amount is not validated.');
			//echo '2073';
		}else{ 
		   $email_data = $this->SendInvoice($request,$conforder_id,$shoppingcart_id);
		   return redirect("digital-payment?invoice_id=$conforder_id")->with('email_data',$email_data);
		}
    }
    
     public function SendInvoice($request,$conforder_id,$shoppingcart_id) {
        //conforder details
        $user_details = Ordershipping::find($conforder_id);
        //get city name
        $city_info=Cities::find($request->CityList);
        $track_number="PLORDER#100-" . $conforder_id;
        
    	//order confirmation mail send
    	$email_data['email'] = $user_details->email;
    	$email_data['track_number'] = $track_number;
    	$email_data['first_name'] = $user_details->Shipping_txtfirstname;
    	$email_data['last_name'] = $user_details->Shipping_txtlastname;
    	$email_data['address'] = $user_details->Shipping_txtaddressname;
    	$email_data['city'] = $city_info->name;
    	$email_data['zip'] = $user_details->Shipping_txtzipcode;
    	$email_data['country'] = 'Bangladesh';
    	$email_data['phone'] = $user_details->Shipping_txtphone;
    	$email_data['order_date'] = date('Y-m-d');
    	
    	//shopping cart info
    	$shoppingcart_info = Shoppingcart::find($shoppingcart_id);
    	$email_data['shipping_Charge'] = $shoppingcart_info->shipping_charge;
    	$email_data['shoppingcart_subtotal'] = $shoppingcart_info->shoppingcart_subtotal;
    	$email_data['shoppingcart_total'] = $shoppingcart_info->shoppingcart_total;
    	$email_data['get_offer']=0;
    	
    	//shopping product info
    	$email_data['order_product'] = DB::table('shoppinproducts')
    			->join('products', 'products.id','=','.shoppinproducts.product_id')
    			->select('shoppinproducts.cart_image','shoppinproducts.productalbum_name','shoppinproducts.prosize_name', 'shoppinproducts.shoppinproduct_quantity', 'shoppinproducts.product_price', 'products.product_name', 'products.product_styleref')
    			->where('shoppingcart_id', $shoppingcart_id)
    			->get();
     if($user_details->email !=null){
         $email=$user_details->email;
     }else{
        $email='ikshimuluits@gmail.com'; 
     }
     $to=array(
          $user_details->Shipping_txtfirstname =>$email
          );
      Mail::to($to)->send(new Invoicedetails($email_data));
      Cart::instance('products')->destroy();
      if(Cookie::get('cart_user_id') !== null){
			$userId=request()->cookie('cart_user_id');
			Cart::instance('products')->erase($userId);
		}
      //Cart::instance('products')->erase();
      return $email_data;
    }
    
}
