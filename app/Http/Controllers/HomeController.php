<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Banner;
use App\Models\Browser;
use App\Models\Popups;
use App\Models\Procat;
use App\Models\Subprocat;
use App\Models\Productsize;
use App\Models\Productalbum;
use App\Models\Showroom;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
     
    public function index()
    {
        $data['slides']  = Slider::where('active', 1)->orderBy('slider_order')->get();
        $data['banner_list']=Banner::where('active',1)->orderBy('banner_order','ASC')->limit(3)->get();
        //browser history
        $ip=$_SERVER['REMOTE_ADDR'];
        if($ip != "162.158.7.100"){
            $this->get_browser_name($_SERVER['HTTP_USER_AGENT']);
        }
        $product=new Product();
        //$data['product_list'] =$product->MostView();
        $data['product_list'] =$product->NewArriavl();
        $data['popup']  = Popups::where('active', 1)->orderBy('id','DESC')->first();
        $data['category_name'] = 'New Arrival';
        return view('home', $data);
    }
    
    public function get_browser_name($user_agent)
        {
            if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')){
                Browser::where('id',3)->increment('total_view');
            }
            elseif (strpos($user_agent, 'Edge') || strpos($user_agent, 'MSIE')){
                 Browser::where('id',4)->increment('total_view');
            }
            elseif (strpos($user_agent, 'Chrome')){  
                 Browser::where('id',1)->increment('total_view');
            }
            elseif (strpos($user_agent, 'Safari')) {
                 Browser::where('id',5)->increment('total_view');
            }
            elseif (strpos($user_agent, 'Firefox')){
                  Browser::where('id',2)->increment('total_view');
            }
            elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')){
                Browser::where('id',6)->increment('total_view');
            }
            elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'YourApp/') !== false){
                 Browser::where('id',7)->increment('total_view');
            }
            elseif(stripos(strtolower($_SERVER['HTTP_USER_AGENT']),'android') !== false) { 
                 Browser::where('id',8)->increment('total_view');
            }
            else{
                 Browser::where('id',9)->increment('total_view');
            }
        }
    
    public function InsideRewards(){
        return view('footer_page.loyalty.faq');
    }
    
    public function InsideRewardsFaq(){
        return view('footer_page.loyalty.faq_more');
    }
    
    public function ComingSoonPage(){
        return view('coming_soon');
    }
    
    public function AboutUs(){
        return view('footer_page.about_us');
    }
    
    public function WorkWithUs(){
       return view('footer_page.work_with_us');
    }
    
    public function ContactUs(){
       return view('footer_page.contact_us');
    }
    
    public function ExchangePolicy(){
        return view('footer_page.exchange_policy');
    }
    
    public function Faq(){
        return view('footer_page.faq');
    }
    
    public function StoreLocator(){
        $data['store'] = Showroom::where('status',1)->orderBy('id', 'ASC')->get();
        return view('footer_page.storelocator', $data);
    }
    
    public function PrivacyCookies(){
        return view('footer_page.privacy_cookies');
    }
    
    public function HowToOrder($link='product'){
        $data['active']=$link;
        return view('footer_page.how_to_order',$data);
    }
    
    public function LoyaltyTerms(){
        return view('loyalty.terms');
    }
    
    public function SiteMap(){
		return view('footer_page.sitemap');
	}
	
	public function SizeGuide(){
        return view('footer_page.size-guide');
    }
	
	public function userlogout(){
		Auth::guard('web')->logout();
		return redirect('/');
	}
        
}
