<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Region;
use App\Models\Userdetail;
use App\Models\Uservoucher;
use App\Models\Product;
use App\Models\Productalbum;
use App\Models\Productimg;
use App\Models\Ordershipping;
use App\Models\Conforder;
use App\Models\Shoppinproduct;
use App\Models\Shoppingcart;
use App\Models\Exchange;
use Auth;
use DB;

class ExchangeController extends Controller
{
    public static function GetColorListByProductId($product_id) {
        $color_list = DB::table('productalbums')
                ->select('productalbums.productalbum_name')
                ->where('productalbums.product_id', '=', $product_id)
                ->get();
        return $color_list;
    }
    
    public static function GetSizeListByProductId($product_id, $productalbum_name) {
        $Size_list = DB::table('productsizes')
                ->select('productsizes.productsize_size')
                ->where('productsizes.product_id', '=', $product_id)
                ->where('productsizes.color_name', '=', $productalbum_name)
                ->orderBy('productsize_size','ASC')
                ->get();
        return $Size_list;
    }
    
    public static function GetQtyProductId($product_id, $productalbum_name, $prosize_name) {
        $qtys = DB::table('productsizes')
                ->where('productsizes.product_id', '=', $product_id)
                ->where('productsizes.color_name', '=', $productalbum_name)
                ->where('productsizes.productsize_size', '=', $prosize_name)
                ->sum('productsizes.SizeWiseQty');
        return $qtys;
    }
    
    protected function index(Request $request){
        $order_id=$request->id;
	    $order=new Conforder();
	    $order_product=new Shoppinproduct();
	    $data['shipping_address_details'] = $order->orderdetails($order_id);
        $data['total_incomplete_order_info'] = $order_product->orderproducts($data['shipping_address_details']->shoppingcart_id);
        $data['total_exchange_request_info'] = $this->getExchangeRequestInfo($order->orderdetails($order_id)->shoppingcart_id);
        $data['order_no'] = $order_id;
        if(Auth::user()->id ==$data['shipping_address_details']->user_id){
            if ($request->has('success')) {
			    $data['message']='Request success';
			}else{
			   $data['message']=''; 
			}
            return view('user.order.exchange.index',$data);
        }else{
            abort(401);
        }
    }
    
    protected function different(Request $request){
        $shoppinproduct_id=$request->shoppinproduct_id;
        $order_no=$request->order_no;
        $data['i']=1;
		$data['shoppinproduct_info']=Shoppinproduct::where('id',$shoppinproduct_id)->first();
		$data['shoppingcart_cart']=Shoppingcart::where('id',$data['shoppinproduct_info']->shoppingcart_id)->first();
		$data['orderinfo']=Conforder::where('shoppingcart_id',$data['shoppinproduct_info']->shoppingcart_id)->first();
		$data['order_id']=$data['orderinfo']->id;
		$data['product_info']=Product::where('id',$data['shoppinproduct_info']->product_id)->first();
		$data['userinfo']= Ordershipping::where('conforder_id',$data['orderinfo']->id)->first();  
		return view('user.order.exchange.different',$data);
    }
    
    public function ExchangeSearchProduct($key){
		$data['search'] = DB::table('products')
                ->join('productalbums', 'products.id', '=', 'productalbums.product_id')
                ->join('cats', 'products.cat', '=', 'cats.id')
                ->join('productimgs', 'productalbums.id', '=', 'productimgs.productalbum_id')
                ->select('products.id', 'products.cat', 'products.product_price', 'products.product_styleref', 'products.product_name','cats.name', 'productalbums.productalbum_name', 'productimgs.productimg_img', 'productimgs.productimg_img_medium')
                ->where('products.product_active_deactive', 0)
                ->where('products.product_name', 'like', "%{$key}%")
                ->orWhere('products.product_description', 'like', "%{$key}%")
                ->orWhere('products.product_styleref', 'like', "%{$key}%")
                ->groupBy('products.id')
                ->orderBy('products.id', 'DESC')
                ->take(10)
                ->get();
        $data['key']=$key;
        //dd($data);
        return view('user.order.exchange.search', $data);
	}
	
	public function SearchProductinfo($key,$shoppinproduct_id){
		$shoppinproduct_info=Shoppinproduct::where('id',$shoppinproduct_id)->first();
		$orderinfo=Conforder::where('shoppingcart_id',$shoppinproduct_info->shoppingcart_id)->first();
		$price_check=Product::where('product_styleref',$key)->where('product_price', '>=', $shoppinproduct_info->product_price)->first();
		$info=Ordershipping::where('conforder_id',$orderinfo->id)->first();
		if($price_check){
		        $data['mobile_no'] = $info->Shipping_txtphone;
				$data['order_id_en'] = $orderinfo->id;
		        $data['order_id']=$orderinfo->id;
		        $data['shoppinproduct_id']=$shoppinproduct_id;
		        $data['shoppingcart_id']=$shoppinproduct_info->shoppingcart_id;
		        $data['product_info'] = $this->Productinfo($key);
		    	return view('user.order.exchange.search_product_details', $data);
		}else{
				echo '<p style="padding:10px;color:red;">Price less than previous product. Please select a product which has a price to greater than or equal previous product value.</p>';
			}
	
	}
	
	public function Productinfo($key){
		return $product_info = DB::table('products')
                ->select('products.product_name', 'products.product_img_thm','products.id', 'products.product_price as original_price', 'products.product_styleref')
                ->where('products.product_styleref', '=', $key)
                ->first();
	}
	
	public function ExchangeRequestSave(Request $request){
		//dd($request);
		$product_id=$request->product_id;
		$color=$request->new_productalbum_name;
		$ablum_info=Productalbum::where('product_id',$product_id)->where('productalbum_name',$color)->first();
		$album_id=$ablum_info->id;
		$productimg=Productimg::where('productalbum_id',$album_id)->orderBy('productimg_order','ASC')->first();
		$cart_image=$productimg->productimg_img_medium;
		$check_duplicate=Exchange::where('shoppinproduct_id',$request->shoppinproduct_id)->where('exchnage_complete',0)->first();
		if($check_duplicate){
			echo 'exit';
		}else{
			$exchange=new Exchange();
			$exchange->shoppinproduct_id=$request->shoppinproduct_id;
			$exchange->shoppingcart_id=$request->shoppingcart_id;
			$exchange->product_id=$request->product_id;
			$exchange->product_price=$request->product_price;
			$exchange->prosize_name=$request->new_prosize_name;
			$exchange->productalbum_name=$request->new_productalbum_name;
			$exchange->shoppinproduct_quantity=$request->new_shoppinproduct_quantity;
			$exchange->cart_image=$cart_image;
			$exchange->save();
			$data['id']=$exchange->id;
			//return redirect()->back()->with('success', 'Exchange requested success');
			$data['image']=$cart_image;
			$data['product_name']=$request->product_name;
			$data['color']=$request->new_productalbum_name;
			$data['size']=$request->new_prosize_name;
			//return view('user.exchnage.exchange-popup',$data);
			//return redirect()->back()->with('success', 'Exchange requested success');
			echo 'success';
		}
	}
    
    private function getExchangeRequestInfo($shoppingcartid){
		$total_incomplete_order_info = DB::table('products')
                ->join('exchanges', 'products.id', '=', 'exchanges.product_id')
                ->select('products.product_name', 'products.product_img_thm','products.id', 'products.product_price as original_price', 'products.product_styleref','exchanges.shoppingcart_id','exchanges.shoppinproduct_id', 'exchanges.shoppinproduct_quantity', 'exchanges.prosize_name', 'exchanges.cart_image', 'exchanges.productalbum_name', 'exchanges.product_barcode', 'exchanges.product_price', 'exchanges.product_id', 'exchanges.productalbum_name')
                ->where('exchanges.shoppingcart_id', '=', $shoppingcartid)
				->where('exchanges.shoppinproduct_quantity', '>=', 1)
                ->get();
        return $total_incomplete_order_info;
	}
	
	public static function GetPriceVariation($shoppingproduct_id){
		return Shoppinproduct::where('id',$shoppingproduct_id)->first();
	}
	
}
