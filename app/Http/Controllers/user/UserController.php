<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Validator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Region;
use App\Models\Userdetail;
use App\Models\Uservoucher;
use Auth;
use App\Notifications\Signup;

class UserController extends Controller
{
    use Notifiable;
    
    public function showRegisterForm(Request $request) {
        return view('auth.register_form', ['regi_email' => $request->regi_email]);
    }
    
    public function showRegisterFormBadRequest(Request $request) {
       return view('auth.register_form');
    }
    
    public function create(Request $request) {
        $vaildation = Validator::make($request->all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'regi_email' => 'required|email|unique:users,email',
                    'regi_password' => 'required|min:4',
                    'password_confirmation' => 'required|same:regi_password'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $hashedPassword = Hash::make($request->regi_password);
            $time=Carbon::now()->toDateTimeString();
            $user=new User();
            $user->name=$request->first_name.' '.$request->last_name;
            $user->email=$request->regi_email;
            $user->password=$hashedPassword;
            $user->save();
            $user_id=$user->id;
           // return Response::json(array('success' => true, 'last_insert_id' => $regi->id), 200);
            Auth::loginUsingId($user_id);
            $user->notify(new Signup($request));
            return redirect()->route('user-details', ['id' => $user_id]);
        }
    }
    
    protected function Register(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'f_name' => 'required',
                    'l_name' => 'required',
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'mobile' => 'required|numeric|min:11|unique:users,mobile_no',
                    'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $user=User::create([
            'name' => $request->f_name.' '.$request->l_name,
            'email' => $request->email,
            'mobile_no' => $request->mobile,
            'password' => Hash::make($request->password),
          ]);
          $user_id=$user->id;
          Auth::loginUsingId($user_id);
          // return Response::json(array('success' => true, 'last_insert_id' => $regi->id), 200);
          $details=new Userdetail();
          $details->user_id=$user_id;
          $details->phone=$request->mobile;
          $details->offer=0;
          $details->save();
          $voucher=new Uservoucher();
          $voucher->user_id=$user_id;
          $voucher->voucher_id=1;
          $voucher->save();
          $user->notify(new Signup($request));
          //return redirect()->route('user-details', ['id' => $user_id]);
          Auth::loginUsingId($user_id);
          return redirect()->route('checkout');
        }
    }
    
    public function UserDetails($user_id) {
        $check_user_id = Auth::user()->id;
        if ($check_user_id == $user_id) {
            $words = explode(' ', Auth::user()->name);
            $data['last_name'] = array_pop($words);
            $data['first_name'] = implode(' ', $words);
            $data['regions']=Region::all();
            return view('user.user_details_form',$data);
        } else {
           return redirect()->intended('/');
        }
    }
    
    public function SaveUserDetails(Request $request) {
        $vaildation = Validator::make($request->all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'address' => 'required',
                    'region' => 'required',
                    'city' => 'required',
                    'zipcode' => 'numeric|min:4',
                   // 'phone' => 'required|numeric|min:11|unique:userdetails,phone'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            //dd($request);
            $details=new Userdetail();
            $details->user_id=$request->user_id;
            $details->address=$request->address;
            $details->region=$request->region;
            $details->city=$request->city;
            $details->zip=$request->zipcode;
            $details->phone=$request->phone;
            $details->user_ip=request()->ip();
            $details->save();
            $user_ip = request()->ip();
            $offer=Userdetail::where('user_ip',$user_ip)->where('user_id','!=',Auth::user()->id)->first();
            if($offer == null){
                $duplicate=Userdetail::where('phone',$request->phone)->where('user_id','!=',Auth::user()->id)->first();
                if($duplicate == null){
                   $voucher=new Uservoucher();
                   $voucher->user_id=Auth::user()->id;
                   $voucher->voucher_id=1;
                   $voucher->save();
                   $u['offer']=0; 
                }else{
                  $u['offer']=1;  
                  Userdetail::where('user_id', Auth::user()->id)->update($u);
                }
            }else{
               $u['offer']=1;
               Userdetail::where('user_id', Auth::user()->id)->update($u);
            }
            return redirect()->intended();
        }
        //  dd($request);
    }
    
}
