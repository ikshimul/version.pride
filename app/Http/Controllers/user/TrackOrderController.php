<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Session;
use Auth;
use DB;
use App\Models\User;
use App\Models\Registeruserdetails;
use App\Models\Registeruser;

class TrackOrderController extends Controller
{
    
    public function orderPreview($confo_id, $shopping_cart_id) {
        $data['confo_info'] = DB::table('conforder')
                ->where('conforder_id', $confo_id)
                ->first();
        $data['order_info'] = DB::table('shoppingcart')
                ->where('shoppingcart_id', $shopping_cart_id)
                ->first();
        $data['order_product'] = DB::table('shoppinproduct')
                ->join('product', '.shoppinproduct.product_id', '=', 'product.product_id')
                ->select('shoppinproduct.cart_image', 'shoppinproduct.shoppinproduct_quantity', 'shoppinproduct.product_price', 'product.product_name', 'product.product_code')
                ->where('shoppingcart_id', $shopping_cart_id)
                ->get();
        return view('user.order_view', $data);
    }
    
    public function index(){
        return view('user.order.track_order');
    }
    
    public function OrderTrackByECR(Request $request){
        $ecr=$request->ecr;
		$curl = curl_init();
		$ecr = [
                'parcel' => 'track',
                'product_id' => $ecr
            ];
			
			curl_setopt_array($curl, array(
                CURLOPT_URL => "https://ecourier.com.bd/apiv2/",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 3000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $ecr,
                CURLOPT_HTTPHEADER => array(
                    "API_SECRET:zReJG",
                    "API_KEY: 66eY",
                    "USER_ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			$response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $json = json_decode($response);
               // $status = $json->status;
				 return $json->query_data;
			}
	}
	
	public function TrackOrderStatus(Request $request){
	    $tracknumber=$request->ecr;
	    $status=DB::table('conforder')->select('conforder_status')->where('conforder_tracknumber',$tracknumber)->first();
	    if($status){
	    return $status->conforder_status;
	    }else{
	        return 0;
	    }
	}
	
	public function MyOrders(){
		$login_id = Auth::user()->id;
        $get_user_id = DB::table('registeruser')
                ->select('registeruser.registeruser_id')
                ->where('registeruser.registeruser_login_id', $login_id)
                ->first();
        $register_user_id = $get_user_id->registeruser_id;
        $data['user_details'] = DB::table('registeruser')
                        ->join('registeruserdetails', 'registeruser.registeruser_id', '=', 'registeruserdetails.registeruser_id')
                        ->select('registeruser.registeruser_id', 'registeruser.registeruser_email', 'registeruser.registeruser_firstname', 'registeruser.registeruser_email', 'registeruser.registeruser_lastname', 'registeruserdetails.registeruser_address', 'registeruserdetails.registeruser_country', 'registeruserdetails.registeruser_city', 'registeruserdetails.registeruser_zipcode', 'registeruserdetails.registeruser_phone')
                        ->where('registeruser.registeruser_id', $register_user_id)->first();
        if($data['user_details']==null){
            	return redirect()->route('user-details', ['id' => $login_id]);
        }else{
        $user_id = $data['user_details']->registeruser_id;
        $data['orderlist'] = DB::table('conforder')
                ->join('shoppingcart', 'conforder.shoppingcart_id', '=', 'shoppingcart.shoppingcart_id')
                ->select('conforder.conforder_id', 'conforder.shoppingcart_id', 'conforder.conforder_tracknumber', 'conforder.conforder_placed_date', 'conforder.conforder_status', 'shoppingcart.shoppingcart_subtotal', 'shoppingcart.Shipping_Charge', 'shoppingcart.shoppingcart_total')
                ->where('conforder.registeruser_id', $user_id)
                ->orderBy('conforder.conforder_id', 'desc')
                ->get();
		}
		return view('user.my_orders',$data);
	}
	
	public function UserOrderDetails($order_id){
        $data['shipping_address_details'] = $this->getShippingAddressDetails($order_id);
        $data['total_incomplete_order_info'] = $this->getTotalOrderInfo($this->getShippingAddressDetails($order_id)->shoppingcart_id);
        return view('user.order-details', $data);
    }
    
    public function CancelOrder($confoorderid){
		$shipping_address_details = $this->getShippingAddressDetails($confoorderid);
        $total_incomplete_order_info = $this->getTotalOrderInfo($shipping_address_details->shoppingcart_id);
		foreach ($total_incomplete_order_info as $order_info) {
			$result = $this->getProductInfoByProductSize($order_info->product_id, $order_info->prosize_name, $order_info->productalbum_name);
			$pro_SizeWiseQty = $result->SizeWiseQty + $order_info->shoppinproduct_quantity;
			$update_qty = $this->update_qty($order_info->product_id, $result->color_name, $result->productsize_size, $pro_SizeWiseQty);
			if ($update_qty) {
			   $data['conforder_status'] = 'Cancelled';
			   $data['conforder_statusdetails']='Order cancel by user';
			   DB::table("conforder")->where('conforder_id', $confoorderid)->update($data);
			} else {
				return 0;
			}
		}
	}
	
	public function getProductInfoByProductSize($product_id, $prosize, $color) {
        $product_info = DB::table('productsize')
                ->select('productsize.color_name', 'productsize.SizeWiseQty', 'productsize.productsize_size')
                ->where('productsize.product_id', '=', $product_id)
                ->where('productsize.productsize_size', '=', $prosize)
                ->where('productsize.color_name', '=', $color)
                ->first();
        return $product_info;
    }
    
    public function update_qty($product_id, $pro_color, $prosize, $pro_SizeWiseQty) {
        $data['SizeWiseQty'] = $pro_SizeWiseQty;
        $update_result = DB::table('productsize')->where('product_id', $product_id)->where('color_name', $pro_color)->where('productsize_size', $prosize)->update($data);
        if ($update_result) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function getTotalOrderInfo($shoppingcartid) {
        $total_incomplete_order_info = DB::table('product')
                ->join('shoppinproduct', 'product.product_id', '=', 'shoppinproduct.product_id')
                ->select('product.product_name', 'product.product_img_thm','product.product_id', 'product.product_price as original_price', 'product.product_styleref','shoppinproduct.shoppingcart_id','shoppinproduct.shoppinproduct_id', 'shoppinproduct.shoppinproduct_quantity', 'shoppinproduct.prosize_name', 'shoppinproduct.cart_image', 'shoppinproduct.productalbum_name', 'shoppinproduct.Shipping_Charge','shoppinproduct.shipping_area','shoppinproduct.deliveryMethod','shoppinproduct.product_barcode', 'shoppinproduct.product_price', 'shoppinproduct.product_id', 'shoppinproduct.productalbum_name')
                ->where('shoppinproduct.shoppingcart_id', '=', $shoppingcartid)
                ->get();
        return $total_incomplete_order_info;
    }
    
    public function getShippingAddressDetails($order_id) {
        $shipping_address_details = DB::table('conforder')
                ->select('conforder.conforder_id','conforder.employee_status', 'conforder.conforder_tracknumber', 'conforder.conforder_completed', 'conforder.conforder_status','conforder.order_threepldlv', 'conforder.conforder_deliverynotes','conforder.conforder_statusdetails', 'conforder.conforder_placed_date','conforder.created_at', 'conforder.conforder_deliverydate', 'conforder.pos_entry_date', 'shoppingcart.shoppingcart_id','shoppingcart.shoppingcart_total', 'ordershipping.Shipping_txtfirstname', 'ordershipping.Shipping_txtlastname', 'ordershipping.Shipping_txtaddressname', 'ordershipping.Shipping_txtcity', 'ordershipping.Shipping_txtzipcode', 'ordershipping.Shipping_txtphone', 'shoppingcart.shoppingcart_id', 'ordershipping.ordershipping_id', 'ordershipping.registeruser_id',  't_city.CityId', 't_city.CityName', 'shoppinproduct.shoppinproduct_id','registeruser.admin_comment')
                ->join('shoppingcart', 'conforder.shoppingcart_id', '=', 'shoppingcart.shoppingcart_id')
                ->join('ordershipping', 'conforder.conforder_id', '=', 'ordershipping.conforder_id')
                ->join('shoppinproduct', 'shoppingcart.shoppingcart_id', '=', 'shoppinproduct.shoppingcart_id')
                ->join('t_city', 'shoppinproduct.shipping_area', '=', 't_city.CityId')
                ->leftjoin('registeruser', 'ordershipping.registeruser_id', '=', 'registeruser.registeruser_id')
                ->where('conforder.conforder_id', '=', $order_id)
                ->first();
        return $shipping_address_details;
    }
}
