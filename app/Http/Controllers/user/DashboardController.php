<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Region;
use App\Models\Cities;
use App\Models\Userdetail;
use App\Models\Uservoucher;
use App\Models\Conforder;
use App\Models\Shoppinproduct;
use Auth;
use DB;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    protected function Dashboard(){
        $details=Userdetail::where('user_id',Auth::user()->id)->first();
        if($details){
            return view('user.user-dashboard');
        }else{
            session()->flash('save', 'Please enter your address first.');
			return redirect()->route('user-details', ['id' => Auth::user()->id]);
        }
    }
    
    protected function Profile(){
        $data['details']=Userdetail::where('user_id',Auth::user()->id)->first();
        if( is_numeric($data['details']->region)){
            $region=Region::find($data['details']->region);
            $data['region']=$region->name;
        }else{
            $data['region']=$data['details']->region;
        }
        
        if( is_numeric($data['details']->city)){
            $city=Cities::find($data['details']->city);
            $data['city']=$city->name;
        }else{
           $data['city']=$data['details']->city; 
        }
        return view('user.profile',$data);
    }
    
    protected function ProfileEdit(Request $request){
        $data['details']=Userdetail::where('user_id',Auth::user()->id)->first();
        return view('user.profile_edit',$data);
    }
    
    protected function ProfileUpdate(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|unique:users,email,'.$request->id,
                    'mobile_no' => 'required|unique:users,mobile_no,'.$request->id
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
           // dd($request);
            $user=User::find($request->id);
            $user->name=$request->name;
            $user->email=$request->email;
            $user->mobile_no=$request->mobile_no;
            $user->save();
            return redirect()->back()->with('save', 'Update Successfully');
        }
    }
    
    protected function AddressEdit(){
        $data['details']=Userdetail::where('user_id',Auth::user()->id)->first();
        $data['regions']=Region::select('id','name')->get();
        $data['cities']=Cities::select('id','name')->get();
        return view('user.address_edit',$data);
    }
    
    protected function AddressUpdate(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'address' => 'required',
                    'region' => 'required',
                    'city' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            //dd($request);
            $user=Userdetail::find($request->id);
            $user->address=$request->address;
            $user->region=$request->region;
            $user->city=$request->city;
            $user->save();
            return redirect()->back()->with('save', 'Update Successfully');
        }
    }
    
    protected function ChangePassword(){
        return view('user.change-password');
    }
    
    protected function ChangePasswordUpdate(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'mobile_no' => 'required|unique:users,mobile_no,'.$request->id,
                    'old_password' => 'required',
                    'new_password' => 'required|max:8|different:old_password',
                    'confirm_password' => 'required|same:new_password'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
        $user = User::find($request->id);
         if (Hash::check($request->old_password, $user->password)) { 
                $user->mobile_no=$request->mobile_no;
                $user->password=Hash::make($request->new_password);
                $user->remember_token=Str::random(60);
                $user->save();
            //   $user->fill([
            //     'password' => Hash::make($request->new_password)
            //     ])->save();
               return redirect()->back()->with('save', 'Password changed.Please login now.');
            } else {
                return redirect()->back()->with('error', 'Old password does not match');
            }
        }
    }
    
    protected function MyOrders(){
        $data['orderlist'] = DB::table('conforders')
                ->join('shoppingcarts', 'conforders.shoppingcart_id', '=', 'shoppingcarts.id')
                ->select('conforders.id', 'conforders.shoppingcart_id', 'conforders.conforder_tracknumber', 'conforders.created_at', 'conforders.conforder_placed_date', 'conforders.conforder_deliverydate','conforders.conforder_status', 'shoppingcarts.shoppingcart_subtotal', 'shoppingcarts.Shipping_Charge', 'shoppingcarts.shoppingcart_total')
                ->where('conforders.user_id', Auth::user()->id)
                ->orderBy('conforders.id', 'desc')
                ->paginate(10);
		return view('user.order.list',$data);
	}
	
	protected function OrderDetails($order_id){
	    $order=new Conforder();
	    $order_product=new Shoppinproduct();
	    $data['shipping_address_details'] = $order->orderdetails($order_id);
        $data['total_incomplete_order_info'] = $order_product->orderproducts($data['shipping_address_details']->shoppingcart_id);
        $data['order_id'] = $order_id;
        return view('user.order.details', $data);
	}
	
	protected function Orderdetailsv2(Request $request){
	    $order_id=$request->id;
	    $order=new Conforder();
	    $order_product=new Shoppinproduct();
	    $data['shipping_address_details'] = $order->orderdetails($order_id);
        $data['total_incomplete_order_info'] = $order_product->orderproducts($data['shipping_address_details']->shoppingcart_id);
        $data['order_id'] = $order_id;
        if(Auth::user()->id ==$data['shipping_address_details']->user_id){
        return view('user.order.details-v2', $data);
        }else{
            abort(401);
        }
	}
	
	public static function shoppingProduct($shopping_cart_id){
		$order_product = DB::table('shoppinproducts')
                ->join('products', '.shoppinproducts.product_id', '=', 'products.id')
                ->select('shoppinproducts.cart_image', 'shoppinproducts.shoppinproduct_quantity', 'shoppinproducts.product_price', 'products.product_name', 'products.product_styleref')
                ->where('shoppingcart_id', $shopping_cart_id)
                ->get();
	    return $order_product;
	}
}
