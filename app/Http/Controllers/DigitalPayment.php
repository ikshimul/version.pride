<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Ordershipping;
use App\Models\Shoppingcart;
use App\Models\Conforder;
use App\Services\BKashUpdate;
use App\Services\PaySSL;

class DigitalPayment extends Controller
{
    protected $bkash;
	protected $sslc;
	
	public function __construct() {
        date_default_timezone_set("Asia/Dhaka");
        $this->bkash = new BKashUpdate();
		$this->sslc = new PaySSL();
    }
    
    public function index(Request $request){
		$data['invoice_id']=$request->invoice_id;
		$data['conforderinfo']=Conforder::find($request->invoice_id);
        $data['orderinfo']=Ordershipping::where('conforder_id', $request->invoice_id)->first();
		$data['amount_info']=Shoppingcart::find($data['conforderinfo']->shoppingcart_id);	
		$data['order_status']=Conforder::find($request->invoice_id)->first();
// 		$data['shoppingcart_total']=session("email_data")['shoppingcart_total'];
// 		$data['shipping_Charge']=session("email_data")['shipping_Charge'];
// 		$data['track_number']=session("email_data")['track_number'];
// 		$data['conforder_tracknumber']=session("email_data")['track_number'];
		$shoppingcart_id=$data['conforderinfo']->shoppingcart_id;
		$data['order_product'] = DB::table('shoppinproducts')
                    ->join('products','products.id','=', 'shoppinproducts.product_id')
					->join('subprocats','subprocats.id','=','products.subprocat_id')
                    ->select('products.product_styleref as sku', 'products.product_name as name', 'subprocats.subprocat_name as category',  'shoppinproducts.product_price as price', 'shoppinproducts.shoppinproduct_quantity as quantity')
                    ->where('shoppingcart_id', $shoppingcart_id)
                    ->get();
		return view('digital_payment',$data);
	}
	
	public function BkashCreate(Request $request){
		$invoice_id=$request->invoice_id;
		$this->bkash->CreatePayment($invoice_id);
	}
	
	public function updateToken(Request $request) {
	    $this->bkash->GetToken();
	}
	
	public function ExecutePayment(Request $request) {
	    $this->bkash->ExecutePayment($request->paymentID);
	}
	
	public function bKashPaymentSuccess(Request $request){
		$conforder_id = $this->bkash->success($request);
		if($conforder_id) {
		    $conforder=new Conforder();
		    $conforeder = Conforder::find($conforder_id);
            $conforeder->conforder_status = 'Processing';
            $conforeder->save();
            
		    $order_info=Conforder::find($conforder_id);
		    $shoppingcart=new Shoppingcart();
		    $shoppingcart=Shoppingcart::find($order_info->shoppingcart_id);
		    $shoppingcart->payment_method='bKash';
		    $shoppingcart->save();
		    return redirect("digital-payment?invoice_id=$conforder_id")->with('status', 'Payment success.');
		    //echo 'Payment Success';
		}
	}
	
	public function SSLCreate(Request $request){
		$this->sslc->submitpayment($request->order);
	}
	
	public function SSLSuccess(Request $request){
		$conforder_id = $this->sslc->ssl_success($request);
		if($conforder_id){
		    $conforder=new Conforder();
		    $conforeder = Conforder::find($conforder_id);
            $conforeder->conforder_status = 'Processing';
            $conforeder->save();
            
		    $order_info=Conforder::find($conforder_id);
		    $shoppingcart=new Shoppingcart();
		    $shoppingcart=Shoppingcart::find($order_info->shoppingcart_id);
		    $shoppingcart->payment_method='ssl';
		    $shoppingcart->save();
          return redirect("digital-payment?invoice_id=$conforder_id")->with('status', 'Payment success.');
		}else{
		  abort(404);
		}
	}
	
	public function SSLFailure(Request $request){
	   $invoice_id=$request->tran_id;
	   return redirect("digital-payment?invoice_id=$invoice_id")->with('failed', 'Payment failed.');
	}
	
	public function SSLCancel(Request $request){
	    $invoice_id=$request->tran_id;
	    $order_info=Conforder::find($invoice_id);
		$shoppingcart=new Shoppingcart();
	    $shoppingcart=Shoppingcart::find($order_info->shoppingcart_id);
	    $shoppingcart->payment_method='bKash';
	    $shoppingcart->save();
	    return redirect("digital-payment?invoice_id=$invoice_id")->with('failed', 'Payment Cancelled.');
	}
}
