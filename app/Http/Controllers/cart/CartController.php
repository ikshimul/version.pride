<?php

namespace App\Http\Controllers\cart;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Cart;
use DB;
use Illuminate\Support\Facades\Cookie;
use Session;
use App\Models\Tempcart;
use Auth;
use Carbon\Carbon;
use App\Models\Productsize;

class CartController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Dhaka');
        if(Cookie::get('cart_user_id') !== null){
			$userId=request()->cookie('cart_user_id');
			$content =Cart::instance('products')->restore($userId);
		}elseif (Auth::check()){
		    $userId = Auth::id();
		    $content =Cart::instance('products')->restore($userId);
		}
    
    }
    
    
    public static function GetProductsizelist($pro_id, $color_name) {
        $size=new Productsize();
        return $size->getsizescolorwise($pro_id,$color_name); 
    }
    
    public static function GetProductqty($product_id, $product_size, $color_name) {
        $size=new Productsize();
        return $size->getsizewiseqtysum($product_id,$product_size,$color_name);
    }
    
    public function index() {
        if(Cookie::get('cart_user_id') !== null){
			$userId=request()->cookie('cart_user_id');
			 Cart::instance('products')->restore($userId);
		}else{
		    $userId = Auth::id();
		    Cart::instance('products')->restore($userId);
		}
        return view('cart.index');
    }

    public function cartDestroy() {
        Cart::instance('products')->destroy();
        if(Cookie::get('cart_user_id') !== null){
			$userId=request()->cookie('cart_user_id');
			Cart::instance('products')->erase($userId);
		}
        
        return view('cart.index');
    }
    
    public function addProduct(Request $request) {
        $CartItems = Cart::instance('products')->content();
		$request_added_qty=0;
		$added_qty=0;
        $size=new Productsize();
        $get_barcode=$size->getbarcode($request->productid,$request->productsize,$request->productcolor);
		$get_productqty=$get_barcode->SizeWiseQty;
		if($get_productqty > 0){
			$item_count=count($CartItems);
			if(count($CartItems)>0){
			foreach($CartItems as $row) {
				   if($row->id == $request->productid && $row->options->size == $request->productsize && $row->options->color == $request->productcolor){
					  $added_qty=$row->qty; 					 
				   }else{
					  
				   }
				}
			  $request_added_qty=$added_qty+$request->productqty;
			}
			if($get_productqty >= $request_added_qty){
				$product_barcode=$get_barcode->barcode;
				$product = Product::find($request->productid);
				$product_name = $product->product_name;
				$discount_amount=0;
				if ($product->product_pricediscounted < 1) {
				   $product_price=$product->product_price;
				} else {
					$product_price=$product->product_pricediscounted;
					$discount_amount=$product->product_price-$product->product_pricediscounted;
				}
				Cart::instance('products')->add(['id' => $product->id, 'name' => $product->product_name, 'qty' => $request->productqty, 'price' => $product_price, 'weight' => 0, 'options' => ['color' => $request->productcolor, 'size' => $request->productsize, 'discount_amount' => $discount_amount, 'barcode' => $product_barcode, 'product_image' => $request->productimage]])->associate('Product');
				if (Auth::check())
            		{
            		  $userId = Auth::id();
            		  Cookie::queue(cookie('cart_user_id', $userId, $minute = 43800));
            		}else{
            			if(Cookie::get('cart_user_id') !== null){
            				$userId=request()->cookie('cart_user_id');
            			}else{
            			$userId=$this->Coupongenerate();
            			Cookie::queue(cookie('cart_user_id', $userId, $minute = 43800));
            			}
            		}
            	 $content =Cart::instance('products')->restore($userId);
            	 if (  DB::table('cart')->where('identifier', $userId)->exists() ) {
            	     DB::table('cart')->update([
                    'instance' => $this->currentInstance(),
                    'content' => serialize($content)
                    ], [
                        'identifier' => $userId,
                    ]);
            	 }else{
            	    Cart::instance('products')->store($userId);	
            	 }
				$data['product_name']=$product->product_name;
				$data['product_image']=$request->productimage;
				return view('cart.cartpopup',$data);
			}else{
				return 0;
			}
		}else{
			return 1;
		}

    }
    
    public function PopupaddProduct(Request $request) {
        $CartItems = Cart::instance('products')->content();
		$request_added_qty=0;
		$added_qty=0;
        $product_color = $request->productcolor;
        $product_size = $request->productsize;
        $productqty = $request->quantity;
        $productid = $request->productid;
        $productimage = $request->productimage;
        $get_barcode=DB::table('productsizes')->where('product_id',$productid)->where('productsize_size',$product_size)->where('color_name',$product_color)->first();
		$get_productqty=$get_barcode->SizeWiseQty;
		if($get_productqty > 0){
			$item_count=count($CartItems);
			if(count($CartItems)>0){
			foreach($CartItems as $row) {
				   if($row->id == $productid && $row->options->size == $product_size && $row->options->color == $product_color){
					  $added_qty=$row->qty; 					 
				   }else{
					  
				   }
				}
			  $request_added_qty=$added_qty+$productqty;
			}else{
			    $request_added_qty=$productqty;
			}
			if($get_productqty >= $request_added_qty){
				$product_barcode=$get_barcode->barcode;
				$product = Product::find($productid);
				$product_name = $product->product_name;
				$discount_amount=0;
				if ($product->product_pricediscounted < 1) {
				   $product_price=$product->product_price;
				} else {
					$product_price=$product->product_pricediscounted;
					$discount_amount=$product->product_price-$product->product_pricediscounted;
				}
				Cart::instance('products')->add(['id' => $product->id, 'name' => $product->product_name, 'qty' => $productqty, 'price' => $product_price, 'weight' => 0, 'options' => ['color' => $product_color, 'size' => $product_size, 'discount_amount' => $discount_amount, 'barcode' => $product_barcode, 'product_image' => $productimage]]);
				if (Auth::check())
            		{
            		  $userId = Auth::id();
            		  Cookie::queue(cookie('cart_user_id', $userId, $minute = 43800));
            		}else{
            			if(Cookie::get('cart_user_id') !== null){
            				$userId=request()->cookie('cart_user_id');
            			}else{
            			$userId=$this->Coupongenerate();
            			Cookie::queue(cookie('cart_user_id', $userId, $minute = 43800));
            			}
            		}
            	 $content =Cart::instance('products')->restore($userId);
            	 if (  DB::table('cart')->where('identifier', $userId)->exists() ) {
            	     DB::table('cart')->update([
                    'instance' => $this->currentInstance(),
                    'content' => serialize($content)
                    ], [
                        'identifier' => $userId,
                    ]);
            	 }else{
            	    Cart::instance('products')->store($userId);	
            	 }
				$data['product_name']=$product->product_name;
				$data['product_image']=$productimage;
				return view('cart.cartpopup',$data);
			}else{
				return 0;
			}
		}else{
			return 1;
		}

    }
    
    public function CartReload(){
        return view('cart.cart-home');
    }
    
    public function Coupongenerate(){
		$coupon = '';
		$prefix='pride-';
		$uppercase    = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'];
        $lowercase    = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
        $numbers      = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $symbols      = ['`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '\\', '|', '/', '[', ']', '{', '}', '"', "'", ';', ':', '<', '>', ',', '.', '?'];
        $characters   = [];
		$characters = array_merge($characters, $lowercase, $uppercase);
		$characters = array_merge($characters, $numbers);
		//$characters = array_merge($characters, $symbols);
		for ($i = 0; $i < 8; $i++) {
				$coupon .= $characters[mt_rand(0, count($characters) - 1)];
			}
			return $prefix . $coupon;
				
	}
	
	public function cartProductUpdate(Request $request) {
        $rowId = $request->rowid;
        $color = $request->colorname;
        $size = $request->sizename;
        $qty = $request->quantity;
        if($qty > 0){
        $image = $request->image_link;
        Cart::instance('products')->update($rowId, ['qty' => $qty, 'options' => ['color' => $color, 'size' => $size, 'product_image' => $image]]);
        }else{
           Cart::instance('products')->remove($rowId); 
        }
        toastr()->success('Item update successfully.');
        return redirect('/shop-cart')->with('update', 'Item update successfully.');
        //  return redirect('cart.index');
    }
    
    public function cartProductDelete($rowId) {
        Cart::instance('products')->remove($rowId);
        if(Cookie::get('cart_user_id') !== null){
			$userId=request()->cookie('cart_user_id');
			Cart::instance('products')->erase($userId);
			Cart::instance('products')->store($userId);	
		}elseif (Auth::check()){
		    $userId = Auth::id();
		    $content =Cart::instance('products')->erase($userId);
		    Cart::instance('products')->store($userId);	
		}
	//	Cart::instance('products')->store($userId);	
        toastr()->warning('Item removed successfully.');
        return redirect('/shop-cart')->with('delete', 'Item removed successfully.');
    }
    
    public function CartItemCount(){
        $CartItems = Cart::instance('products')->content();
        $item_count=count($CartItems);
        return $item_count;
    }
    
    public function Cartrestore(){
//         if(Cookie::get('cart_user_id') !== null){
// 			$userId=request()->cookie('cart_user_id');
// 			$content =Cart::instance('products')->restore($userId);
// 		}elseif (Auth::check()){
// 		    $userId = Auth::id();
// 		    $content =Cart::instance('products')->restore($userId);
// 		}
    }
    
    public function __destruct()
    {
        if(Cookie::get('cart_user_id') !== null){
			$userId=request()->cookie('cart_user_id');
			$content =Cart::instance('products')->store($userId);
		}elseif (Auth::check()){
		    $userId = Auth::id();
		    $content =Cart::instance('products')->store($userId);
		}
        
    }
    
}
