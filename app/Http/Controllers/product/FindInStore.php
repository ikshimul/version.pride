<?php

namespace App\Http\Controllers\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class FindInStore extends Controller
{
    public function getcity($product_code){
        $product_code=htmlentities($product_code);
        $city_list=DB::connection('sqlsrv')->select("SELECT DISTINCT ShowroomDetails.district FROM  ShowroomDetails inner join Showroomwiseitemstock on ShowroomDetails.SalesPointID=Showroomwiseitemstock.salespointid where Showroomwiseitemstock.designref='$product_code'");
        return $city_list;
    }
    
    public function ShowroomStock($district, $barcode){
    $showroom_stock=DB::connection('sqlsrv')
	->table('ShowroomDetails')
	->join('Showroomwiseitemstock', 'ShowroomDetails.SalesPointID', '=', 'Showroomwiseitemstock.salespointid')
	->select('ShowroomDetails.SalesPointName','ShowroomDetails.SPAddress','ShowroomDetails.district','ShowroomDetails.city','ShowroomDetails.OpeningTime','ShowroomDetails.ClosingTime','Showroomwiseitemstock.Barcode','Showroomwiseitemstock.CurrentStock')
	->where('ShowroomDetails.district', $district)
	->where('Showroomwiseitemstock.Barcode',$barcode)
	->where('ShowroomDetails.SalesPointName','!=','UT Online')
	->get();
    return view('product.showroom_stock', ['showroom_stock'=>$showroom_stock]);
	}
	
	public function ShowroomStockByDesignRef($district,$designref){
	   $product_style=str_replace('_','/',$designref);
		 $showroom_stock=DB::connection('sqlsrv')
		->table('ShowroomDetails')
		->join('Showroomwiseitemstock', 'ShowroomDetails.SalesPointID', '=', 'Showroomwiseitemstock.salespointid')
		->select('ShowroomDetails.SalesPointName','ShowroomDetails.SPAddress','ShowroomDetails.district','ShowroomDetails.city','ShowroomDetails.OpeningTime','ShowroomDetails.ClosingTime','Showroomwiseitemstock.Barcode','Showroomwiseitemstock.CurrentStock')
		->where('ShowroomDetails.district',$district)
		->where('Showroomwiseitemstock.designref',$product_style)
		->where('ShowroomDetails.SalesPointName','!=','UT Online')
		->get();
		return view('product.showroom_stock', ['showroom_stock'=>$showroom_stock]);
	}
}
