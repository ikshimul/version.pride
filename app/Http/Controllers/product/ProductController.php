<?php

namespace App\Http\Controllers\product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\Models\Product;
use App\Models\Productalbum;
use App\Models\Productimg;
use App\Models\Productsize;
use App\Models\Procat;
use App\Models\Subprocat;
use App\Models\Maincat;
use App\Models\Cat;
use App\Models\Subcat;

class ProductController extends Controller {
    
    public function __construct(){
         $this->product=new Product();
    }

    public static function GetProductColorAlbum($pro_id) {
        $color_albums= Productalbum::getproductwise($pro_id);
        return $color_albums;
    }
    
    public static function getlastablbum($product_id){
        $color_album = Productalbum::getlastablbum($product_id);
        return $color_album;
    }
    
    public static function GetProductImageByColorAlbum($pro_album_id) {
        $images=Productimg::getimagemulti($pro_album_id);
        return $images;
    }
    
    public static function GetProductImageByProductID($product_id){
        $color_album = Productalbum::getlastablbum($product_id);
		$images=Productimg::getimagesingle($color_album->id);
		return $images;
	}
	
	public static function GetProductImageSingleByColorAlbum($color_album_id){
	    $images=Productimg::getimagesingle($color_album_id);
		return $images;
	}
    
    public static function ProductWiseQty($product_id){
       $qty=Productsize::getqtysum($product_id);
       return $qty;
    }
    
    public static function GetLatestProduct($category=null) {
       $product=new Product();
       return $product->latestproduct($category);
    }
    
    
    public function ProductTotalQty($product_id){
        $qty=Productsize::getqtysum($product_id);
        echo $qty;
    }
    
    public function SearchNew($key){
        $data['search']=$this->product->search($key);
        $data['key']=$key;
        return view('search.search', $data);
    }
    
    public function SearchResultView($key){
        $data['search']=$this->product->searchwithpagi($key);
        $data['key']=$key;
        return view('search.search_view', $data);
    }
    
    public function ProductsNew($cat=''){
       $maincatinfo=Maincat::where('name',$cat)->first();
       $product=new Product();
       if($cat==''){
           $data['main_cate'] = 'New';
           $data['main_link'] = "new";
           $data['product_list']=$product->listnew_v2();
       }else{
       //$data['product_list']=$product->listnew($cat);
       $data['product_list']=$product->listnew_v2($maincatinfo->id);
       $data['main_cate'] = $cat;
       $data['main_link'] = "$maincatinfo->name";
       }
       $data['title'] = '';
       $data['last'] = '';
       $data['subcatid'] = '';
       $data['subcategory'] = '';
       $data['procat_id'] = 0;
       $data['subcategory_id'] = 0;
       $data['sub_link'] ="";
       $data['color_names'] = $this->getProductColors();
       $data['fabrics'] = $this->getFabrics();
       return view('product.list', $data);
    }
    
    public function ProductsNewCatewise($maincat,$catname){
       $catname=str_replace('-', ' ', $catname);
       $catname=str_replace('_', '-', $catname);
       $product=new Product();
	   $catinfo=Cat::where('name',$catname)->first();
	   $maincatinfo=Maincat::where('id',$catinfo->maincat)->first();
       //$data['product_list']=$product->listnewcatewise($catinfo);
       $data['product_list']=$product->listnewcatewise_v2($catinfo->id);
       $data['title'] = $catname;
       $data['last'] = '';
       $data['main_cate'] = $maincat;
       $data['subcatid'] = '';
       $data['procat_id'] = 0;
       $data['subcategory'] = $catname;
       $data['subcategory_id'] = 0;
       $data['main_link'] = "$maincatinfo->name";
	   $data['sub_link'] ="$maincatinfo->name/$catname/";
       $data['color_names'] = $this->getProductColors();
       $data['fabrics'] = $this->getFabrics();
       return view('product.list', $data);
    }
    
    public function Products($maincat){
       $product=new Product();
       $data['product_list']=$product->products($maincat);
       $data['title'] = '';
       $data['last'] = '';
       $data['main_cate'] = $maincat;
       $data['subcatid'] = 'all';
       $data['subcategory'] = $maincat;
       $data['color_names'] = $this->getProductColors();
       $data['fabrics'] = $this->getFabrics();
       return view('product.list', $data);
    }
    
    public function ProductAll($catname){
        //dd($catname);
        $product=new Product();
        if($catname=='signature'){
            $main_cat='women';
            $procat = array("9","16");
            $data['product_list']=$product->listAll($procat);
        }elseif($catname=='classic'){
            $main_cat='women';
            $procat = array("7");
            $data['product_list']=$product->listAll($procat);
        }else if($catname=='pride-girls'){
            $main_cat='women';
            $procat = array("5");
            $data['product_list']=$product->listAll($procat);
        }else if($catname=='panjabi'){
            $main_cat='men';
            $procat = array("17","18");
            $data['product_list']=$product->listAll($procat);
        }elseif($catname=='girls'){
            $main_cat='women';
            $procat = array("6");
            $data['product_list']=$product->listAll($procat);
        }elseif($catname=='boys'){
            $main_cat='kids';
            $procat = array("21");
            $data['product_list']=$product->listAll($procat);
        }else{
           $main_cat=''; 
        }
       $data['title'] = $catname;
       $data['last'] = '';
       $data['main_cate'] = $main_cat;
       $data['subcatid'] = $main_cat;
       $data['subcategory'] = $catname;
       $data['color_names'] = $this->getProductColors();
       $data['fabrics'] = $this->getFabrics();
       return view('product.list', $data);
        //dd($data);
    }
    
    public function ProductList($subcategory, $subcatname) {
       $subcatname=str_replace('-', ' ', $subcatname);
       $subcatname=str_replace('_', '-', $subcatname);
       $subcat=new Subprocat();
       $subinfo=$subcat->getid($subcatname);
       $product=new Product();
       if($subinfo){
        $data['product_list']=$product->list($subinfo->id);
        $subcat_id= $subinfo->id;
        $catinfo=Procat::where('id',$subinfo->procat_id)->first();
        $maincatinfo=Maincat::where('id',$catinfo->maincat_id)->first();
        $maincat=$maincatinfo->name;
       }else{
           $data['product_list']=$product->list();
           $subcat_id = '';
           $maincat='';
       }
        
       $data['title'] = $subcatname;
       $data['last'] = '';
       $data['main_cate'] = $maincat;
       $data['subcatid'] = $subcat_id;
       $data['subcategory'] = $subcategory;
       $data['color_names'] = $this->getProductColors();
       $data['fabrics'] = $this->getFabrics();
       return view('product.list', $data);
       //dd($data);
    }
    
    public function details($product_name, $product_color, $product_id){
        $product=new Product();
        $product->viewincrement($product_id);
        $data['singleproduct'] = $product->details($product_id);
        
        $album=new Productalbum();
        $productalbum =$album->getproductcolorwise($product_id,$product_color);
        $data['product_selected_color'] = $productalbum;
        $data['product_color_image'] = Productalbum::where('product_id', $product_id)->get();
        
        $img=new Productimg();
        $data['singleproductmultiplepic'] = $img->getsingleproductimgs($productalbum->id);
        
        $size=new Productsize();
        $data['product_qty'] = Productsize::getqtysum($product_id);
        $data['product_sizes'] = $size->getsizescolorwise($product_id,$product_color);
        $data['product_color']=str_replace('-','/',$product_color);
        
        $data['may_like']=$product->MayLike($data['singleproduct']->cat, $data['singleproduct']->id, $data['singleproduct']->subcat);
        $data['top_pick']=$product->TopPicks($data['singleproduct']->cat, $data['singleproduct']->id, $data['singleproduct']->subcat);

        $data['product_id'] = $product_id;
        $data['main_cate'] = $data['singleproduct']->name;
        
        $main=str_replace('-', '_', $data['singleproduct']->name);
		$main=str_replace(' ', '-', $main);
		$mainname=strtolower($main);
		
		$cat=str_replace('-', '_', $data['singleproduct']->cat_name);
		$cat=str_replace(' ', '-', $cat);
		$catname=strtolower($cat);
		
		$subcat=str_replace('-', '_', $data['singleproduct']->subcat_name);
		$subcat=str_replace(' ', '-', $subcat);
		$subcatname=strtolower($subcat);
		
        $data['main_link'] = "$mainname";
	    $data['sub_link'] ="$mainname/$catname";
	    $data['last_link'] ="$mainname/$catname/$subcatname";
	    
        $data['title']=$data['singleproduct']->product_name;
        $data['alt']=$data['singleproduct']->product_name;
        $data['category_name'] = $data['singleproduct']->cat_name;
        $data['subcategory_name'] = $data['singleproduct']->subcat_name;
        $data['title'] = $data['singleproduct']->product_name;
        $data['image']=$data['singleproduct']->product_img_thm;
        $data['description']=$data['singleproduct']->product_description;
        $data['product_price']=$data['singleproduct']->product_price;
        $data['retailer_item_id']=$data['singleproduct']->product_styleref;
        $data['color_names'] = $this->getProductColors();
        $show_fb = true;
        return view('product.details', $data)->with('show_fb', $show_fb);
    }
    
    public function getProductColors($categoryid = 0, $subcategory = 0) {
        $prosize=new Productsize();
        return $prosize->getcolors($categoryid, $subcategory);
    }
    
    public function getFabrics($categoryid = 0, $subcategory = 0) {
        $product=new Product();
        return $product->getFabrics($categoryid, $subcategory);
    }
    
    public function productByPrice($categoryid, $subcategory, $lower_price=0, $upper_price=0) {
        $product=new Product();
        if($categoryid=='all'){
            $data['product_list']=$product->searchbyprice($subcategory, $lower_price, $upper_price);
        }elseif($categoryid=='women' || $categoryid=='men' || $categoryid=='kids' || $categoryid=='home'){
            //$data['product_list']=$product->searchbypricesub($subcategory, $lower_price, $upper_price);
            dd($subcategory);
        }
        
    }

}
