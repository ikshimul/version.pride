<?php

namespace App\Http\Controllers\product;

use Illuminate\Http\Request as GetRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon\Carbon;
use App\Models\Maincat;
use App\Models\Cat;
use App\Models\Subcat;
use App\Models\Product;
use App\Models\Productalbum;
use App\Models\Productimg;
use App\Models\Productsize;
use App\Models\Campaign;
use Request;

class UpdateProductController extends Controller
{
    public function ProductAllMaincatwise(GetRequest $request){
        $maincat=Request::segment(1);
        $product=new Product();
		$maincatinfo=Maincat::where('name',$maincat)->first();
		if($request->price !=''){
		    $prices = $request->price;
            $price = explode('-', $prices);
            $minprice=$price[0];
            $maxprice=$price[1];
            $data['lower_price'] = $minprice;
            $data['upper_price'] = $maxprice;
		    $data['product_list']=$product->listAllMaincatwisePricefilter($maincatinfo->id,$minprice,$maxprice);
		}elseif($request->size !=''){
		    $data['size'] = $request->size;
		    $data['product_list']=$product->listAllMaincatwiseSizefilter($maincatinfo->id,$request->size);
		}elseif($request->color !=''){
		    $data['color_name'] = $request->color;
		    $data['product_list']=$product->listAllMaincatwiseColorfilter($maincatinfo->id,$request->color);
		}else{
		$data['product_list']=$product->listAllMaincatwise($maincatinfo->id);
		}
        $data['title'] = '';
        $data['last'] ='';
        $data['main_cate'] = $maincatinfo->name;
	    $data['procat_id'] = 0;
        $data['subcategory'] = 0;
        $data['subcategory'] = '';
        $data['subcategory_id'] = 0;
	    $data['main_link'] = "$maincatinfo->name";
	    $data['sub_link'] ="";
        $data['color_names'] = $this->getProductColorsbyMaincat($maincatinfo->id);
        $data['fabrics'] = $this->getFabrics();
        return view('product.list', $data);
    }
    
    protected function ProductAllMaincatwiseDemo(GetRequest $request){
        $maincat='women';
        $product=new Product();
		$maincatinfo=Maincat::where('name',$maincat)->first();
		if($request->price !=''){
		    $prices = $request->price;
            $price = explode('-', $prices);
            $minprice=$price[0];
            $maxprice=$price[1];
            $data['lower_price'] = $minprice;
            $data['upper_price'] = $maxprice;
		    $data['product_list']=$product->listAllMaincatwisePricefilter($maincatinfo->id,$minprice,$maxprice);
		}elseif($request->size !=''){
		    $data['size'] = $request->size;
		    $data['product_list']=$product->listAllMaincatwiseSizefilter($maincatinfo->id,$request->size);
		}elseif($request->color !=''){
		    $data['color_name'] = $request->color;
		    $data['product_list']=$product->listAllMaincatwiseColorfilter($maincatinfo->id,$request->color);
		}else{
		$data['product_list']=$product->listAllMaincatwise($maincatinfo->id);
		}
        $data['title'] = '';
        $data['last'] ='';
        $data['main_cate'] = $maincatinfo->name;
	    $data['procat_id'] = 0;
        $data['subcategory'] = 0;
        $data['subcategory'] = '';
        $data['subcategory_id'] = 0;
	    $data['main_link'] = "$maincatinfo->name";
	    $data['sub_link'] ="";
        $data['color_names'] = $this->getProductColors();
        $data['fabrics'] = $this->getFabrics();
        return view('product.list_demo', $data);
    }
    
	public function ProductAll(GetRequest $request,$catname){
        $catname=str_replace('-', ' ', $catname);
        $catname=str_replace('_', '-', $catname);
        //dd($catname);
        $product=new Product();
		$catinfo=Cat::where('name',$catname)->first();
		$maincatinfo=Maincat::where('id',$catinfo->maincat)->first();
		if($request->price !=''){
		    $prices = $request->price;
            $price = explode('-', $prices);
            $minprice=$price[0];
            $maxprice=$price[1];
            $data['lower_price'] = $minprice;
            $data['upper_price'] = $maxprice;
		    $data['product_list']=$product->listAllPricefilter($catinfo->id,$minprice,$maxprice);
		}elseif($request->size !=''){
		    $data['size'] = $request->size;
		    $data['product_list']=$product->listAllSizefilter($catinfo->id,$request->size);
		}elseif($request->color !=''){
		    $data['color_name'] = $request->color;
		    $data['product_list']=$product->listAllColorfilter($catinfo->id,$request->color);
		}else{
		   $data['product_list']=$product->listAll($catinfo->id);
	   }
       $data['title'] = '';
       $data['last'] =$catname;
       $data['main_cate'] = $maincatinfo->name;
	   $data['procat_id'] = $catinfo->id;
       $data['subcategory'] = 0;
       $data['subcategory'] = $catname;
       $data['subcategory_id'] = 0;
	   $data['main_link'] = "$maincatinfo->name";
	   $data['sub_link'] ="";
       $data['color_names'] = $this->getProductColorsbyCat($catinfo->id);
       $data['fabrics'] = $this->getFabrics();
       return view('product.list', $data);
    }
	
    public function ProductList(GetRequest $request,$cat, $subcatname) {
       $subcatname=str_replace('-', ' ', $subcatname);
       $subcatname=str_replace('_', '-', $subcatname);
       if($cat=='boys'){
           $subinfo=Subcat::where('name',$subcatname)->where('cat',9)->first();
       }else{
          $subinfo=Subcat::where('name',$subcatname)->first();
       }
       $product=new Product();
       if($subinfo){
        $subcat_id= $subinfo->id;
        $catinfo=Cat::where('id',$subinfo->cat)->first();
        $maincatinfo=Maincat::where('id',$subinfo->maincat)->first();
        $maincat=$maincatinfo->name;
        if($request->price !=''){
		    $prices = $request->price;
            $price = explode('-', $prices);
            $minprice=$price[0];
            $maxprice=$price[1];
            $data['lower_price'] = $minprice;
            $data['upper_price'] = $maxprice;
		    $data['product_list']=$product->listPricefilter($subinfo->id,$minprice,$maxprice);
		}elseif($request->size !=''){
		    $data['size'] = $request->size;
		    $data['product_list']=$product->listSizefilter($subinfo->id,$request->size);
		}elseif($request->color !=''){
		    $data['color_name'] = $request->color;
		    $data['product_list']=$product->listColorfilter($subinfo->id,$request->color);
		}else{
		//dd($subinfo->id);
        $data['product_list']=$product->list($subinfo->id);
        
		}
       }else{
           $data['product_list']=$product->list();
           $subcat_id = '';
           $maincat='';
       }
       $data['title'] = $cat;
       $data['last'] =$subcatname;
       $data['main_cate'] = $maincat;
	   $data['procat_id'] = $catinfo->id;
       $data['subcategory'] = $subinfo->name;
       $data['subcategory_id'] = $subinfo->id;
	   $data['main_link'] = "$maincatinfo->name";
	   $data['sub_link'] ="$maincatinfo->name/$cat/";
       $data['color_names'] = $this->getProductColors();
       $data['fabrics'] = $this->getFabrics();
       return view('product.list', $data);
       
    }
    
    public function Occasions(GetRequest $request,$name){
       $product=new Product();
       if($request->price !=''){
		    $prices = $request->price;
            $price = explode('-', $prices);
            $minprice=$price[0];
            $maxprice=$price[1];
            $data['lower_price'] = $minprice;
            $data['upper_price'] = $maxprice;
		    $data['product_list']=$product->listOccasionsPricefilter($name,$minprice,$maxprice);
		}elseif($request->size !=''){
		    $data['size'] = $request->size;
		    $data['product_list']=$product->listOccasionsSizefilter($name,$request->size);
		}elseif($request->color !=''){
		    $data['color_name'] = $request->color;
		    $data['product_list']=$product->listOccasionsColorfilter($name,$request->color);
		}else{
        $data['product_list']=$product->listOccasions($name);
		}
       $data['title'] = '';
       $data['last'] =$name;
       $data['main_cate'] = 'women';
	   $data['procat_id'] = '';
       $data['subcategory'] = $name;
       $data['subcategory_id'] = '';
	   // dd($data);
	   $data['main_link'] = "women";
	   $data['sub_link'] ="women/$name/";
       $data['color_names'] = $this->getProductColors();
       $data['fabrics'] = $this->getFabrics();
      // dd($data);
       return view('product.list', $data);
    }
    
    public function Collection(GetRequest $request,$slug=0){
        $product=new Product();
        $campaign=Campaign::select('id','name')->where('slug',$slug)->first();
        $isSpecial=$campaign->id;
        if($request->price !=''){
		    $prices = $request->price;
            $price = explode('-', $prices);
            $minprice=$price[0];
            $maxprice=$price[1];
            $data['lower_price'] = $minprice;
            $data['upper_price'] = $maxprice;
		    $data['product_list']=$product->listCollectionPricefilter($isSpecial,$minprice,$maxprice);
		}elseif($request->size !=''){
		    $data['size'] = $request->size;
		    $data['product_list']=$product->listCollectionSizefilter($isSpecial,$request->size);
		}elseif($request->color !=''){
		    $data['color_name'] = $request->color;
		    $data['product_list']=$product->listCollectionColorfilter($isSpecial,$request->color);
		}else{
        $data['product_list']=$product->listCollection($isSpecial);
		}
	   $data['title'] = '';
       $data['last'] =$campaign->name;
       $data['main_cate'] = 'collection';
	   $data['procat_id'] = '';
       $data['subcategory'] = $campaign->name;
       $data['subcategory_id'] = $campaign->id;
	   // dd($data);
	   $data['main_link'] = "collection";
	   $data['sub_link'] ="collection/$campaign->name";
       $data['color_names'] = $this->getProductColors();
       $data['fabrics'] = $this->getFabrics();
       return view('product.list', $data);
    }
    
    public function details($product_name, $product_color, $product_id){
        $product_color=str_replace('-','/',$product_color);
        $product=new Product();
        $product->viewincrement($product_id);
        $data['singleproduct'] = $product->details($product_id);
        
        $album=new Productalbum();
        $productalbum =$album->getproductcolorwise($product_id,$product_color);
        $data['product_selected_color'] = $productalbum;
        $data['product_color_image'] = Productalbum::where('product_id', $product_id)->get();
        
        $img=new Productimg();
        $data['singleproductmultiplepic'] = $img->getsingleproductimgs($productalbum->id);
        $data['cart_image'] = Productimg::where('productalbum_id', $productalbum->id)->orderBy('id', 'ASC')->first();
        
        $size=new Productsize();
        $data['product_qty'] = Productsize::getqtysum($product_id);
        $data['product_sizes'] = $size->getsizescolorwise($product_id,$product_color);
        $data['product_color']=str_replace('-','/',$product_color);
        
        $data['may_like']=$product->MayLike($data['singleproduct']->cat, $data['singleproduct']->id, $data['singleproduct']->subcat);
        $data['top_pick']=$product->TopPicks($data['singleproduct']->cat, $data['singleproduct']->id, $data['singleproduct']->subcat);

        $data['product_id'] = $product_id;
        $data['main_cate'] = $data['singleproduct']->name;
        
        $main=str_replace('-', '_', $data['singleproduct']->name);
		$main=str_replace(' ', '-', $main);
		$mainname=strtolower($main);
		
		$cat=str_replace('-', '_', $data['singleproduct']->cat_name);
		$cat=str_replace(' ', '-', $cat);
		$catname=strtolower($cat);
		
		$subcat=str_replace('-', '_', $data['singleproduct']->subcat_name);
		$subcat=str_replace(' ', '-', $subcat);
		$subcatname=strtolower($subcat);
		
        $data['main_link'] = "$mainname";
	    $data['sub_link'] ="$mainname/$catname";
	    $data['last_link'] ="$mainname/$catname/$subcatname";
	    
        $data['title']=$data['singleproduct']->product_name;
        $data['alt']=$data['singleproduct']->product_name;
        $data['category_name'] = $data['singleproduct']->cat_name;
        $data['subcategory_name'] = $data['singleproduct']->subcat_name;
        $data['title'] = $data['singleproduct']->product_name;
        $data['image']=$data['singleproduct']->product_img_thm;
        $data['description']=$data['singleproduct']->product_description;
        $data['product_price']=$data['singleproduct']->product_price;
        $data['retailer_item_id']=$data['singleproduct']->product_styleref;
        $product_name = str_replace(' ', '-', $data['singleproduct']->product_name);
        $product_url = strtolower($product_name);
        $data['details_link']="shop/".$product_url."/color-".$product_color."/".$product_id;
        $data['color_names'] = $this->getProductColors();
        $show_fb = true;
        return view('product.details', $data)->with('show_fb', $show_fb);
    }
	
	public function getProductColors($maincat = 0, $subcategory = 0) {
        $color_names = DB::table('products')
            ->join('productsizes','products.id' ,'=', 'productsizes.product_id')
            ->where('products.maincat', $maincat)
            ->where('products.subprocat_id', $subcategory)
            ->groupBy('color_name')
            ->orderBy('color_name')
            ->pluck('color_name')
            ->toArray();
        if($maincat == 0 || $subcategory == 0)
        $color_names = DB::table('productsizes')
            ->where('status', 1)
            ->groupBy('color_name')
            ->orderBy('color_name')
            ->pluck('color_name')
            ->toArray();
        return $color_names;
    }
    
    public function detailsv2($product_name, $product_code, $product_color){
        $product_color=str_replace('-','/',$product_color);
        $info=Product::where('product_styleref',$product_code)->where('product_active_deactive',0)->first();
        if($info){
        $product_id=$info->id;
        $product=new Product();
        $product->viewincrement($product_id);
        $data['singleproduct'] = $product->details($product_id);
        //dd($data['singleproduct']);
        $album=new Productalbum();
        $productalbum =$album->getproductcolorwise($product_id,$product_color);
        $data['product_selected_color'] = $productalbum;
        $data['product_color_image'] = Productalbum::where('product_id', $product_id)->get();
        
        $img=new Productimg();
        $data['singleproductmultiplepic'] = $img->getsingleproductimgs($productalbum->id);
        $data['cart_image'] = Productimg::where('productalbum_id', $productalbum->id)->orderBy('id', 'ASC')->first();
        
        $size=new Productsize();
        $data['product_qty'] = Productsize::getqtysum($product_id);
        $data['product_sizes'] = $size->getsizescolorwise($product_id,$product_color);
        $data['product_color']=str_replace('-','/',$product_color);
        
        $data['may_like']=$product->MayLike($data['singleproduct']->cat, $data['singleproduct']->id, $data['singleproduct']->subcat);
        $data['top_pick']=$product->TopPicks($data['singleproduct']->cat, $data['singleproduct']->id, $data['singleproduct']->subcat);

        $data['product_id'] = $product_id;
        $data['main_cate'] = $data['singleproduct']->name;
        
        $main=str_replace('-', '_', $data['singleproduct']->name);
		$main=str_replace(' ', '-', $main);
		$mainname=strtolower($main);
		
		$cat=str_replace('-', '_', $data['singleproduct']->cat_name);
		$cat=str_replace(' ', '-', $cat);
		$catname=strtolower($cat);
		
		$subcat=str_replace('-', '_', $data['singleproduct']->subcat_name);
		$subcat=str_replace(' ', '-', $subcat);
		$subcatname=strtolower($subcat);
		
        $data['main_link'] = "$mainname";
	    $data['sub_link'] ="$mainname/$catname";
	    $data['last_link'] ="$mainname/$catname/$subcatname";
	    
        $data['title']=$data['singleproduct']->product_name;
        $data['alt']=$data['singleproduct']->product_name;
        $data['category_name'] = $data['singleproduct']->cat_name;
        $data['subcategory_name'] = $data['singleproduct']->subcat_name;
        $data['title'] = $data['singleproduct']->product_name;
        $data['image']=$data['singleproduct']->product_img_thm;
        $data['description']=$data['singleproduct']->product_description;
        $data['product_price']=$data['singleproduct']->product_price;
        $data['retailer_item_id']=$data['singleproduct']->product_styleref;
        $product_name = str_replace(' ', '-', $data['singleproduct']->product_name);
        $product_url = strtolower($product_name);
        $data['details_link']="shop/".$product_url."/color-".$product_color."/".$product_id;
        $data['color_names'] = $this->getProductColors();
        $show_fb = true;
        return view('product.details', $data)->with('show_fb', $show_fb);
        }else{
            abort(404);
        }
    }
    
    public function getProductColorsbyMaincat($maincat){
         $color_names = DB::table('products')
            ->join('productsizes','products.id' ,'=', 'productsizes.product_id')
            ->where('products.maincat', $maincat)
            ->groupBy('color_name')
            ->orderBy('color_name')
            ->pluck('color_name')
            ->toArray();
            return $color_names;
    }
    
    public function getProductColorsbyCat($cat){
         $color_names = DB::table('products')
            ->join('productsizes','products.id' ,'=', 'productsizes.product_id')
            ->where('products.cat', $cat)
            ->groupBy('color_name')
            ->orderBy('color_name')
            ->pluck('color_name')
            ->toArray();
            return $color_names;
    }
	
	public function getFabrics($categoryid = 0, $subcategory = 0) {
        $fabrics = DB::table('products')
            ->where('products.procat_id', $categoryid)
            ->where('products.subprocat_id', $subcategory)
            ->groupBy('fabric')
            ->orderBy('fabric')
            ->pluck('fabric')
            ->toArray();
        if($categoryid == 0 || $subcategory == 0)
        $fabrics = DB::table('products')
            ->groupBy('fabric')
            ->orderBy('fabric')
            ->pluck('fabric')
            ->toArray();
        return $fabrics;
    }
}
