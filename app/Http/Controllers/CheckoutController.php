<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Cart;
use DB;
use App\Models\User;
use App\Models\Userdetail;
use App\Models\Region;
use App\Models\Shoppingcart;
use App\Models\Shoppinproduct;
use App\Models\Conforder;
use App\Models\Ordershipping;
use App\Models\Productsize;
use App\Models\Cities;
use App\Models\Voucher;
use App\Models\Uservoucher;
use App\Mail\Invoicedetails;


class CheckoutController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        date_default_timezone_set("Asia/Dhaka");
    }
    
    public function index(){
            $data['userdetails'] = User::find(Auth::user()->id)->userdetails;
            $data['regions']=Region::all();
        return view('checkout',$data);
    }
    
    public function saveorder(Request $request){
        if($request->has('save')){
           $details=new Userdetail();
           $details->store($request,Auth::user()->id);
        }
        //$user_info=User::find(Auth::user()->id)->userdetails;
        $voucher=Uservoucher::where('user_id',Auth::user()->id)->where('active',1)->first();
        if($voucher){
          $voucher_id=$voucher->voucher_id;
          $dicount_info=Voucher::find($voucher->voucher_id);
          $type=$dicount_info->is_fixed;
          $dis_perc=$dicount_info->discount_amount;
          if($type==0){
              $discount=$request->shoppingcart_subtotal*$dis_perc/100;
          }else{
              $discount=$dis_perc;
          }
        }else{
            $voucher_id=null;
            $discount=0;
        }
        //shopping cart save data
        $subtotal=0;
        $stotal=0;
        foreach (Cart::instance('products')->content() as $row){
            $subtotal+=$row->price*$row->qty;
        }
        $stotal=$subtotal+$request->shipping_charge-$discount;
        $shoppingcart=new Shoppingcart();
        $shoppingcart->voucher_id=$voucher_id;
        $shoppingcart->shipping_charge=$request->shipping_charge;
        $shoppingcart->shoppingcart_subtotal=$subtotal;
        $shoppingcart->shoppingcart_discount=$discount;
        $shoppingcart->shoppingcart_total=$stotal;
        $shoppingcart->payment_method=$request->dmselect;
        $shoppingcart->delivery_method=$request->delivery_type;
        $shoppingcart->shipping_area=$request->CityList;
        $shoppingcart->save();
        $shoppingcart_id=$shoppingcart->id;
        
        //shoppinproduct save data
        //$total=$shoppinproduct->savedata($request,$shoppingcart_id);
        $total=0;
        foreach (Cart::instance('products')->content() as $row){
            $shoppinproduct = new Shoppinproduct();
            $shoppinproduct->shoppingcart_id=$shoppingcart_id;
            $shoppinproduct->product_id=$row->id;
            $shoppinproduct->product_barcode=$row->options->barcode;
            $shoppinproduct->prosize_name=$row->options->size;
            $shoppinproduct->productalbum_name=$row->options->color;
            $shoppinproduct->product_price=$row->price;
            $shoppinproduct->shoppinproduct_quantity=$row->qty;
            $shoppinproduct->cart_image=$row->options->product_image;
            $shoppinproduct->save();
            $total+=$row->price*$row->qty;
            
            // Reduce product quantity 
            $pro_qty = Productsize::where('product_id', $row->id)->where('color_name', $row->options->color)->where('productsize_size', $row->options->size)->first();
            $reduce_data['SizeWiseQty'] = $pro_qty->SizeWiseQty - $row->qty;
            Productsize::where('product_id', $pro_qty->product_id)->where('color_name', $pro_qty->color_name)->where('productsize_size', $pro_qty->productsize_size)->update($reduce_data);
        }
        
        //Conforder data save
        $conforder=new Conforder();
        $conforder->user_id=Auth::user()->id;
        $conforder->shoppingcart_id=$shoppingcart_id;
        $conforder->conforder_deliverynotes=$request->conforder_deliverynotes;
        $conforder->conforder_status='Order_Verification_Pending';
        $conforder->save();
        $conforder_id=$conforder->id;
        $conforder_update = Conforder::find($conforder_id);
        $conforder_update->conforder_tracknumber = "PLORDER#100-" . $conforder_id;
        $conforder_update->save();
        
        //Ordershipping data save
        $rdershipping = new Ordershipping();
        $rdershipping->conforder_id=$conforder_id;
        $rdershipping->email=$request->email;
        $rdershipping->Shipping_txtaddressname=$request->address;
        $rdershipping->Shipping_txtfirstname=$request->firstname;
        $rdershipping->Shipping_txtlastname=$request->lastname;
        $rdershipping->Shipping_txtcity=$request->CityList;
        $rdershipping->Shipping_txtzipcode=$request->zip;
        $rdershipping->Shipping_txtphone=$request->mobile_no;
        $rdershipping->save();
        
        $total=$total-$discount+$request->shipping_charge;
        if($total != $request->shoppingcart_total){
            return redirect()->back()->with('payment-msg', 'amount is not validated.');
			//echo '2073';
		}else{ 
		   $voucher=Uservoucher::where('user_id',Auth::user()->id)->update(['active'=>0]);
		   $voucher=Userdetail::where('user_id',Auth::user()->id)->update(['offer'=>1]);
		   $email_data = $this->SendInvoice($request,$conforder_id,$shoppingcart_id);
		   return redirect("digital-payment?invoice_id=$conforder_id")->with('email_data',$email_data);
		}
        
        
    }
    
    public function SendInvoice($request,$conforder_id,$shoppingcart_id) {
        //conforder details
        $user_details = Ordershipping::where('conforder_id',$conforder_id)->first();
        //get city name
        $city_info=Cities::find($request->CityList);
        $track_number="PLORDER#100-" . $conforder_id;
        
    	//order confirmation mail send
    	$email_data['email'] = $user_details->email;
    	$email_data['track_number'] = $track_number;
    	$email_data['first_name'] = $user_details->Shipping_txtfirstname;
    	$email_data['last_name'] = $user_details->Shipping_txtlastname;
    	$email_data['address'] = $user_details->Shipping_txtaddressname;
    	$email_data['city'] = $city_info->name;
    	$email_data['zip'] = $user_details->Shipping_txtzipcode;
    	$email_data['country'] = 'Bangladesh';
    	$email_data['phone'] = $user_details->Shipping_txtphone;
    	$email_data['order_date'] = date('Y-m-d');
    	
    	//shopping cart info
    	$shoppingcart_info = Shoppingcart::find($shoppingcart_id);
    	$email_data['shipping_Charge'] = $shoppingcart_info->shipping_charge;
    	$email_data['shoppingcart_subtotal'] = $shoppingcart_info->shoppingcart_subtotal;
    	$email_data['shoppingcart_total'] = $shoppingcart_info->shoppingcart_total;
    	$email_data['voucher_id'] = $shoppingcart_info->voucher_id;
    	$email_data['get_offer']=0;
    	
    	//shopping product info
    	$email_data['order_product'] = DB::table('shoppinproducts')
    			->join('products', 'products.id','=','.shoppinproducts.product_id')
    			->select('shoppinproducts.cart_image','shoppinproducts.productalbum_name','shoppinproducts.prosize_name', 'shoppinproducts.shoppinproduct_quantity', 'shoppinproducts.product_price', 'products.product_name', 'products.product_styleref')
    			->where('shoppingcart_id', $shoppingcart_id)
    			->get();
      //dd($email_data);
      Mail::to($request->user())->send(new Invoicedetails($email_data));
      $this->SendSMS($email_data);
      Cart::instance('products')->destroy();
      Cart::instance('products')->erase(Auth::user()->id);
      return $email_data;
    }
    
    public function SendSMS($order_detials){
        //dd($order_detials['shipping_Charge']);
        date_default_timezone_set("Asia/Dhaka");
		$order_date=date("d M, Y h:i:sa");
		if($order_detials['shipping_Charge'] == 130){
			$contact="Our customer support will contact you within the business day for same-day delivery.";
		}else if($order_detials['shipping_Charge'] == 120){
			$contact="Our customer support will contact you within the next business day for next-day delivery.";
		}else{
			$contact="Our customer support will contact you within 48 hrs to confirm.";
		}
        $user_mobile = "88" . $order_detials['phone'];
        $name= $order_detials['first_name'].' '.$order_detials['last_name'];
        $conforder_tracknumber=$order_detials['track_number'];
        
        $sms_text = "Dear $name,
		We received your order $conforder_tracknumber on $order_date BST.
		$contact";
		
		$sms_text_bangla="প্রিয় গ্রাহক, আমরা  $order_date তে আপনার অর্ডার,$conforder_tracknumber পেয়েছি। আমাদের একজন প্রতিনিধি অর্ডারটি নিশ্চিত করতে আপনার সাথে ৪৮ ঘন্টার মধ্যে যোগাযোগ করবেন। ধন্যবাদ!
- টীম PRIDE";
        $text=$this->convertBanglatoUnicode($sms_text_bangla);
        $user = "Pride";
        $pass = "xA33I127";
        $sid = "PrideLtdEng";
        //$sid="PrideLtdBng";
        $url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
        $param = "user=$user&pass=$pass&sms[0][0]=$user_mobile&sms[0][1]=" . urlencode($sms_text) . "&sms[0][2]=1234567890&sid=$sid";
        $crl = curl_init();
        curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($crl, CURLOPT_URL, $url);
        curl_setopt($crl, CURLOPT_HEADER, 0);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
        $response = curl_exec($crl);
        curl_close($crl);
       //end send sms
    }
    
    public function convertBanglatoUnicode($BanglaText)
    {
       $unicodeBanglaTextForSms = strtoupper(bin2hex(iconv('UTF-8', 'UCS-2BE', $BanglaText)));
       return $unicodeBanglaTextForSms;
    }
    
}
