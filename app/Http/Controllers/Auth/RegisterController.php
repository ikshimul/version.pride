<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Otpverify;
use App\Models\Userdetail;
use App\Models\Uservoucher;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Notifications\Signup;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function VerifyMobile(){
        return view('auth.verify-mobile');
    }
    
    protected function SendOTP(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'mobile_no' => 'required|numeric|min:11|unique:users,mobile_no'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
           $pin = $this->generatePIN();	
		   $verify=new Otpverify();
		   $verify->mobile_no=$request->mobile_no;
		   $verify->pin_code=$pin;
		   $verify->save();
		   $this->SendOTPSMS($request->mobile_no,$pin);
		   return redirect()->route('otp.verify', ['mobile' => $request->mobile_no]); 
        }
    }
    
    protected function OTPverify(Request $request){
	    $data['mobile_no'] = $request->mobile;
		return view('auth.verify-otp',$data);
	}
	
	public function OTPConfirm(Request $request){
		$pin=$request->verify_code;
		$result=Otpverify::where('mobile_no',$request->mobile_no)->where('pin_code',$pin)->orderBy('id','DESC')->first();
		if($result){
		   Otpverify::where('mobile_no',$request->mobile_no)->delete();
		   return redirect()->route('signup', ['mobile_no' => $request->mobile_no]);
		}else{
		  return redirect()->back()->with('error', 'OTP number wrong.Please enter correct OTP number.');
		}
	}
    
    protected function Signup(Request $request){
        $data['mobile_no'] = $request->mobile_no;
		return view('auth.signup',$data);
    }
    
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    
    protected function Register(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'f_name' => 'required',
                    'l_name' => 'required',
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'mobile' => 'required|numeric|min:11|unique:users,mobile_no',
                    'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $user=User::create([
            'name' => $request->f_name.' '.$request->l_name,
            'email' => $request->email,
            'mobile_no' => $request->mobile,
            'password' => Hash::make($request->password),
          ]);
          $user_id=$user->id;
          Auth::loginUsingId($user_id);
          // return Response::json(array('success' => true, 'last_insert_id' => $regi->id), 200);
          $details=new Userdetail();
          $details->user_id=$user_id;
          $details->phone=$request->mobile;
          $details->offer=0;
          $details->save();
          $voucher=new Uservoucher();
          $voucher->user_id=$user_id;
          $voucher->voucher_id=1;
          $voucher->save();
          $user->notify(new Signup($request));
          //return redirect()->route('user-details', ['id' => $user_id]);
          Auth::loginUsingId($user_id);
          return redirect()->route('checkout');
        }
    }
    
    protected function generatePIN($digits = 4){
    $i = 0; 
    $pin = ""; 
		while($i < $digits){
			$pin .= mt_rand(0, 9);
			$i++;
		}
       return $pin;
    }
    
    protected function SendOTPSMS($mobile,$pin){
            $user_mobile = "88" . $mobile;
            $sms_text = "Your One-Time Password (OTP) for Account Registration is $pin.Validity for OTP is 5 Minutes. 
            
-Team Pride";
            $user = "Pride";
            $pass = "xA33I127";
            $sid = "PrideLtdEng";
            $url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
            $param = "user=$user&pass=$pass&sms[0][0]=$user_mobile&sms[0][1]=" . urlencode($sms_text) . "&sms[0][2]=1234567890&sid=$sid";
            $crl = curl_init();
            curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($crl, CURLOPT_URL, $url);
            curl_setopt($crl, CURLOPT_HEADER, 0);
            curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($crl, CURLOPT_POST, 1);
            curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
            $response = curl_exec($crl);
            curl_close($crl);
        // end send sms
    }
}
