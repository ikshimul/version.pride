<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Socialite;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class SocialLoginController extends Controller {
  
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    public function handleProviderCallback(Request $request, $service)
    {
        
         if (!$request->has('code') || $request->has('denied')) {
            return redirect('/');
        } 
        try {
            if($service == 'google') {
                $user = Socialite::driver($service)->stateless()->user();
                dd($user);
            }else{
                $user = Socialite::driver($service)->user();
            }
        } catch (Exception $e) {
            return redirect("auth/$service");
        }
        if($this->isUserExistsAnother($user, $service)) {
            \Session::flash('message', 'This email already exists in our database. Please try another way to login.');
            \Session::flash('class', 'warning');
            return redirect('/login');
           // return view('auth.login');
        }
      // $authUser = $this->findOrCreateUser($user, $service);
            $authUser = User::where('social_id', $user->id)->first();
            if ($authUser){
                Auth::login($authUser, true);
                //  return redirect()->route('home');
                \Session::flash('message', 'Your account has been created.');
                \Session::flash('class', 'success');
                return view('auth.social_return');
            }else{
            $user_check = User::where('email', $user->email)->first();
            if($user_check){
                \Session::flash('message', 'This email already exists in our database. Please try another way to login.');
                \Session::flash('class', 'warning');
                return redirect('/login');
            }else{
               // dd($user);
            $user_info= User::create([
                'name' => $user->name,
                'email' => $user->email,
                'social_id' => $user->id,
               // 'avatar' => $user->avatar,
                'service' => $service
            ]);  
         //   return $user;
            $words = explode(' ', $user->name);
            $last_name = array_pop($words);
            $first_name = implode(' ', $words);
            
            $user_id= $user->id;
            $registerdata['registeruser_firstname'] = $first_name;
            $registerdata['registeruser_lastname'] = $last_name;
            $registerdata['registeruser_email'] = $user->email;
           $registerdata['registeruser_login_id'] = $user_info->id;
           $registerdata['registeruser_ip'] = request()->ip();
           $registerdata['created_at'] = Carbon::now()->toDateTimeString();
            $registeruser_id = DB::table('registeruser')->insertGetId($registerdata);
           Auth::login($user_info, true);
        //  return redirect()->route('home');
        \Session::flash('message', 'Your account has been created.');
        \Session::flash('class', 'success');
        return view('auth.social_return');
        //return redirect()->intended('/');
            }
        }
    }

    private function findOrCreateUser($socialUser, $service)
    {
        $authUser = User::where('social_id', $socialUser->id)->first();

        if ($authUser){
            return $authUser;
        }
        $user= User::create([
            'name' => $socialUser->name,
            'email' => $socialUser->email,
            'social_id' => $socialUser->id,
            'avatar' => $socialUser->avatar,
            'service' => $service
        ]);
        return $user;
    }
    private function isUserExistsAnother($socialUser, $service) {
        $user = User::where('email', $socialUser->email)->where('service','!=',$service)->first();
        return $user;
    }
    public function googleNew(Request $request)
    {
        $profile = json_decode($request->profile);
        $loginRequest = User::where('email', $profile->email)->whereNotNull('social_id')->where('social_id', $request->social_id)->first();
        $exist = User::where('email', $profile->email)->first();
        if ($loginRequest && Auth::loginUsingId($loginRequest->id)) {
            return redirect()->intended('/home');
        } elseif($exist) {
            \Session::flash('message', 'This email already exists in our database. Please try another way to login.');
            \Session::flash('class', 'warning');
            return redirect('/login');
        } else {
            $user= User::create([
                'name' => $profile->name,
                'email' => $profile->email,
                'social_id' => $request->social_id,
                //'avatar' => $profile->image,
                'service' => 'google'
            ]);
            //Auth::loginUsingId($user->id);
            
            $words = explode(' ', $profile->name);
            $last_name = array_pop($words);
            $first_name = implode(' ', $words);
            
            $user_id= $user->id;
            $registerdata['registeruser_firstname'] = $first_name;
            $registerdata['registeruser_lastname'] = $last_name;
            $registerdata['registeruser_email'] = $user->email;
            $registerdata['registeruser_login_id'] = $user->id;
            $registerdata['registeruser_ip'] = request()->ip();
            $registerdata['created_at'] = Carbon::now()->toDateTimeString();
            $registeruser_id = DB::table('registeruser')->insertGetId($registerdata);
            Auth::login($user, true);
            //  return redirect()->route('home');
            \Session::flash('message', 'Your account has been created.');
            \Session::flash('class', 'success');
            return redirect()->intended('/home');
        }
    }
}
