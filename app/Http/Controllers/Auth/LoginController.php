<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Validator;
use Carbon\Carbon;
use DB;
use App\Models\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }
    
    public function handleProviderCallback(Request $request, $service)
    {
        //dd($request);
         if (!$request->has('code') || $request->has('denied')) {
            return redirect('/');
        } 
        try {
            if($service == 'google') {
                $user = Socialite::driver($service)->stateless()->user();
                //dd($user);
            }else{
                $user = Socialite::driver($service)->user();
                //dd($user);
            }
        } catch (Exception $e) {
            return redirect("auth/$service");
        }
        
        if($this->isUserExistsAnother($user, $service)) {
            \Session::flash('message', 'This email already exists in our database. Please try another way to login.');
            \Session::flash('class', 'warning');
            return redirect('/login');
        }
        
        $authUser = User::where('social_id', $user->id)->first();
        if ($authUser){
            Auth::login($authUser, true);
            \Session::flash('message', 'Your account has been created.');
            \Session::flash('class', 'success');
            return view('auth.social_return');
        }else{
           $user_check = User::where('email', $user->email)->first();
            if($user_check){
                \Session::flash('message', 'This email already exists in our database. Please try another way to login.');
                \Session::flash('class', 'warning');
                return redirect('/login');
            }else{
                $user_info= User::create([
                'name' => $user->name,
                'email' => $user->email,
                'social_id' => $user->id,
                'service' => $service
            ]);  
         //   return $user;
            $words = explode(' ', $user->name);
            $last_name = array_pop($words);
            $first_name = implode(' ', $words);
            $user_id= $user->id;
            $registerdata['registeruser_firstname'] = $first_name;
            $registerdata['registeruser_lastname'] = $last_name;
            $registerdata['registeruser_email'] = $user->email;
            $registerdata['registeruser_login_id'] = $user_info->id;
            $registerdata['registeruser_ip'] = request()->ip();
            $registerdata['created_at'] = Carbon::now()->toDateTimeString();
            $registeruser_id = DB::table('registeruser')->insertGetId($registerdata);
            Auth::login($user_info, true);
            \Session::flash('message', 'Your account has been created.');
            \Session::flash('class', 'success');
            return view('auth.social_return');
            }
        }

    }
    
    private function isUserExistsAnother($socialUser, $service) {
        $user = User::where('email', $socialUser->email)->where('service','!=',$service)->first();
        return $user;
    }
    
    protected function customLogin(Request $request){
        $vaildation = Validator::make($request->all(), [
                    'mobile' => 'required',
                    'password' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $user_login = ['mobile_no' => $request->mobile, 'password' => $request->password];
            if (Auth::attempt($user_login, true)) {
                //echo 'success';
                return redirect()->intended('/checkout');
            }else{
                return redirect()->back()->with('failed', 'Your mobile no or password is not correct or is not registered at pride-limited.com')->withInput();
            }
            
        }
    }
    
}
