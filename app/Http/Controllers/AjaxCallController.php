<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productsize;
use App\Models\Cities;
use App\Models\Cat;
use App\Models\Subcat;
use App\Models\Product;
use App\Models\Productalbum;
use App\Models\Productimg;
use App\Models\Maincat;
use App\Models\Showroom;
use App\Models\Conforder;
use App\Models\Shoppinproduct;
use DB;

class AjaxCallController extends Controller
{
    public function getregionwisecity($region_id){
        $cities=Cities::where('region_id',$region_id)->get();
        return json_encode($cities);
    }
    
    public function ProductDetails(Request $request){
		$product_id=$request->id;
		$product_color=$request->color;
		$data['singleproduct'] = Product::where('id', $product_id)->first();
		$productalbum = Productalbum::where('product_id', $product_id)->where('productalbum_name',$product_color)->first();
		$data['singleproductmultiplepic'] = Productimg::where('productalbum_id', $productalbum->id)->orderBy('id', 'ASC')->take(3)->get();
        $data['cart_image'] = Productimg::where('productalbum_id', $productalbum->id)->orderBy('id', 'ASC')->first();
        $data['product_color_image'] = Productalbum::where('product_id', $product_id)->where('productalbum_name',$product_color)->get();
        $data['product_sizes'] = Productsize::where('product_id', $product_id)->where('color_name',$product_color)->get();
        $data['product_qty'] = Productsize::where('product_id', $product_id)->sum('SizeWiseQty');
        $data['selected_color']=$product_color;
        $data['product_color']=$product_color;
        $data['product_selected_color'] = Productalbum::where('product_id', $product_id)->where('productalbum_name', $product_color)->first();
        $product_name = str_replace(' ', '-', $data['singleproduct']->product_name);
        $product_url = strtolower($product_name);
        $data['details_link']="shop/".$product_url."/color-".$product_color."/".$product_id;
		return view('product.quick_view_new',$data);
	}
	
	public function UserTrackOrderDetails(Request $request){
	    $order=new Conforder();
	    $order_product=new Shoppinproduct();
		$tracknumber = $request->tracknumber;
		$order_info=Conforder::where('conforder_tracknumber', $tracknumber)->first();
        if($order_info){
		$data['shipping_address_details'] = $order->orderdetails($order_info->id);
        $data['total_incomplete_order_info'] = $order_product->orderproducts($data['shipping_address_details']->shoppingcart_id);
        $data['order_id'] = $order_info->id;
        return view('user.order.details', $data);
		}else{
            echo 'null';
        }
        
    }
    
    public function getQuantityByColor($product_id, $product_size, $color_name) {
        $size=new Productsize();
        $product_id=htmlentities($product_id);
		$product_size=htmlentities($product_size);
		$color_name=htmlentities($color_name);
        $color_name=str_replace("-","/",$color_name);
        $qty = $size->getsizewiseqtysum($product_id,$product_size,$color_name);
        return json_encode($qty);
    }
    
    public function GetProductBarcodeBySize($product_id, $product_size, $color_name){
        $color_name=str_replace("-","/",$color_name);
        $size=new Productsize();
        $get_barcode = $size->getbarcode($product_id,$product_size,$color_name);
        echo $get_barcode->barcode;
    }
    
    public function getProcat($id){
        $procats=Cat::where('maincat',$id)->get();
        return json_encode($procats);
    }
    
    protected function GetProWiseSubcats($id){
        $subprocats=Subcat::where('cat',$id)->get();
        return json_encode($subprocats);
    }
    
    public function GetProWiseSubpro($id) {
        $subpro = DB::table('subprocats')
                ->join('procats', 'procats.id', '=', 'subprocats.procat_id')
                ->select('subprocats.id', 'subprocats.subprocat_name', 'subprocats.subprocat_order', 'subprocats.subprocat_img','subprocats.procat_id', 'procats.procat_name')
                ->where('subprocats.procat_id', $id)
                ->get();
        return json_encode($subpro);
    }
    
    
    public function eCourierThana($city){
        $curl = curl_init();
        $input = [
                'city' => $city,
            ];
		curl_setopt_array($curl, array(
                CURLOPT_URL => "https://backoffice.ecourier.com.bd/api/thana-list",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $input,
                CURLOPT_HTTPHEADER => array(
                    "API-SECRET:zReJG",
                    "API-KEY: 66eY",
                    "USER-ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			
			$thanalist = curl_exec($curl); 
			$json = json_decode($thanalist);
			return json_encode($json->message);
    }
    
    public function eCourierPostcode($city,$thana){
         $curl = curl_init();
        $input = [
                'city' => $city,
                'thana' => $thana,
            ];
		curl_setopt_array($curl, array(
                CURLOPT_URL => "https://backoffice.ecourier.com.bd/api/postcode-list",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $input,
                CURLOPT_HTTPHEADER => array(
                    "API-SECRET:zReJG",
                    "API-KEY: 66eY",
                    "USER-ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			
			$arealist = curl_exec($curl); 
			$json = json_decode($arealist);
			return json_encode($json->message);
    }
    
    public function eCourierArea($postcode){
         $curl = curl_init();
        $input = [
                'postcode' => $postcode,
            ];
		curl_setopt_array($curl, array(
                CURLOPT_URL => "https://backoffice.ecourier.com.bd/api/area-list",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $input,
                CURLOPT_HTTPHEADER => array(
                    "API-SECRET:zReJG",
                    "API-KEY: 66eY",
                    "USER-ID:I7155",
					"Content-Type:multipart/form-data"
                ),
            ));
			
			$arealist = curl_exec($curl); 
			$json = json_decode($arealist);
			return json_encode($json->message);
    }
    
    public function PathaogetCity(){
		$access_token=$this->getaccesstoken();
		$curl = curl_init();
			curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-hermes.pathaointernal.com/aladdin/api/v1/countries/1/city-list",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "Authorization: Bearer " . $access_token
                ),
            ));
            $city_list = curl_exec($curl); 
			$data=json_decode($city_list);
			return $data->data->data;
	}
    
    public function PathaogetZone($city_id){
		$access_token=$this->getaccesstoken();
		$curl = curl_init();
			curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-hermes.pathaointernal.com/aladdin/api/v1/cities/$city_id/zone-list",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "Authorization: Bearer " . $access_token
                ),
            ));
            $zone_list = curl_exec($curl); 
			$data=json_decode($zone_list);
			return $data->data->data;
	}
	
	public function PathaogetArea($zone_id){
		$access_token=$this->getaccesstoken();
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api-hermes.pathaointernal.com/aladdin/api/v1/zones/$zone_id/area-list",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json",
				"Authorization: Bearer " . $access_token
			),
		));
		$area_list = curl_exec($curl); 
		$data=json_decode($area_list);
		return $data->data->data;
	}
	
	public function getaccesstoken(){
		$curl = curl_init();
            $token_postdata = [
                'client_id' => '1113',
                'client_secret' => 'r9PGPS4PzVWCXpbiTBM2u5iGzqnr7vyLpYT3XS2d',
                'username' => 'sumbalmomen@gmail.com',
                'password' => 'delivery@pride2020',
                'grant_type' => 'password'
            ];
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-hermes.pathaointernal.com/aladdin/api/v1/issue-token", //api url call here
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($token_postdata),
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/json"
                ),
            ));
            $token_response = curl_exec($curl);
            $result = json_decode($token_response);
            $access_token = $result->access_token;
			
		return $access_token;
	}
    
    public function Searchstore(Request $request){
	    $key=$request->location;
	    $data['store'] = Showroom::where('district', 'like', "%{$key}%")
            ->where('status', 1)
            ->orderBy('id', 'ASC')
            ->get();
        return view('footer_page.search-store',$data);
	}
	
	public function getmonthlysalesdata($syear){
            $syear=htmlentities($syear);
            $totals = DB::select("SELECT SUM(shoppingcarts.shoppingcart_total) as monthlytotal FROM conforders
            INNER JOIN shoppingcarts
            ON conforders.shoppingcart_id=shoppingcarts.id
            WHERE YEAR(conforders.created_at)=$syear and conforders.conforder_status='Closed' GROUP BY MONTH(conforders.created_at)
            ORDER BY MONTH(conforders.created_at) ASC");
            $user = [];
                foreach ($totals as $sale) {
                    $user[] = $sale->monthlytotal;
                }
             $count = count($totals);
               $ext_mon = 12-$count;
               $i = 0;
               $j = 0;
               $comma = ',';
               $back = '';
               $data='';
                foreach($totals as $total){
                       $data.=$total->monthlytotal.$comma;
                }
                
                for($j=0;$j<=$ext_mon;$j++){
                    $zero = 0;
                    if($j==$ext_mon){
                        $data.=$zero.$back;
                    }else{
                        $data.=$zero.$comma;
                    }
                    
                }
                 //dd($data);
                 echo json_encode($user,JSON_NUMERIC_CHECK);
                //echo json_encode($data);

    }
    
}
