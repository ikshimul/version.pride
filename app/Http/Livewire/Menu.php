<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Product;
use App\Models\Campaign;

class Menu extends Component
{
    public function render()
    {
        $product=new Product();
        $data['all']=$product->latestproduct();
        $data['festive']=$product->latestproduct('festive');
        $data['ready']=$product->latestproduct('ready');
        $data['casual']=$product->latestproduct('casual');
        $data['office']=$product->latestproduct('office');
        $data['woman']=$product->latestproduct('women');
        $data['men']=$product->latestproduct('men');
        $data['kids']=$product->latestproduct('kids');
        $data['accessories']=$product->latestproduct('accessories');
        $data['home']=$product->latestproduct('home');
        $data['campaigns']=Campaign::select('category','name','slug','starts_at','expires_at','status')->where('status',0)->orderBy('id','DESC')->get();
        return view('livewire.menu',$data);
    }
}
