<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

class ProductList extends Component
{
    public $list;
    
    use WithPagination;
    
    public function render()
    {
        //return view('livewire.product-list');
        //dd($this->list);
        return view('livewire.product-list', ['product_list' => $this->list]);
    }
}
