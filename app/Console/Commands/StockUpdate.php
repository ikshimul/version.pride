<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Product;

class StockUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'follow:stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sold out product alert';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $stocks = DB::table('productsizes')
                 ->select('product_id', DB::raw('sum(SizeWiseQty) as stock'))
                 ->groupBy('product_id')
                 ->get();
		foreach($stocks as $stock){
			if($stock->stock <=0){
				Product::where('id', $stock->product_id)->update(['sold' => -$stock->product_id]);
			}
		}
		$this->info('Sold out updated.');
        //return 0;
    }
}
