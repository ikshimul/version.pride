<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class bKashpaid extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data=$data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $name=$this->data['firstname'].' '.$this->data['lastname'];
        $order=$this->data['conforder_tracknumber'];
        //dd($order);
        return (new MailMessage)
                    ->subject('bKash Payment Comfirmation')
                    ->greeting("Dear $name")
                    ->line("Thank you for sending us the payment for your order $order via bKash mobile payment service.We will now process your order for delivery.Please expect your order within 2-5 business days.To track your order please")
                    ->action('Click Here', url('/track-order'))
                    ->line('Thanks for shopping with us!.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
