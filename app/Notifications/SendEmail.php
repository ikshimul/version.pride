<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendEmail extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data=$data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message=$this->data['message_details'];
        $subject=$this->data['subject'];
        $file=$this->data['file'];
        if($file){
            return (new MailMessage)
                    ->subject($subject)
                    ->line("$message")
                    ->line('Thank you for using our application!')
                    ->attach($this->data['file']->getRealPath(), array(
                        'as'=>'file.' . $this->data['file']->getClientOriginalName(),
                        'mime' => $this->data['file']->getMimeType())
                     );
        }else{
           return (new MailMessage)
                    ->subject($subject)
                    ->line("$message")
                    ->line('Thank you for using our application!');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
