<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Exceptions\RenderException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
      $this->renderable(function (RenderException $e, $request) {
            $class = get_class($e);
        	    switch($class) {
        		case 'Illuminate\Auth\AuthenticationException':
        		$guards=array_get($e->guards(),0);
        		switch($guards){
        			case 'admin':
        			    $login = 'admin.login';
        				break;
        		      default:
        			    $login = 'login';
        			    break;
        	    	}
        		 return redirect()->route($login);
        	   }
        		
               // return parent::render($request, $exception);
      });
    }
    
}
