<?php

namespace App\Exceptions;

use Exception;

class RenderException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $class = get_class($exception);
	    switch($class) {
		case 'Illuminate\Auth\AuthenticationException':
		$guards=array_get($exception->guards(),0);
		switch($guards){
			case 'admin':
			    $login = 'admin.login';
				break;
		      default:
			    $login = 'login';
			    break;
	    	}
		 return redirect()->route($login);
	   }
		
       // return parent::render($request, $exception);
    }
}