@extends('layouts.app')
@section('title', 'Manage Profile')
@section('content')
<style>
  .dashboard-profile {
    /*width: 288px;*/
    height: 182px;
    background-color: #eee;
    padding: 16px;
    margin-right: 12px;
    float: left;
}
.dashboard-mod-title {
    color: black;
    font-size: 25px;
    font-weight: 600;
    margin-bottom: 16px;
    border-bottom: 1px solid #00000038;
    height: 32px;
    line-height: 20px;
}
.dashboard-mod-title span {
    color: #dadada;
    font-size: 12px;
}
.dashboard-mod-title a {
    font-size: 12px;
    color: #1a9cb7;
}
.dashboard-info {
    overflow-wrap: break-word;
    word-wrap: break-word;
    -webkit-hyphens: auto;
    -ms-hyphens: auto;
    hyphens: auto;
    overflow-y: auto;
    height: 154px;
}
.dashboard-info-item {
    font-size: 14px;
    color: #424242;
    margin-bottom: 10px;
}
.dashboard-info-item.last {
    margin-top: 20px;
}
.dashboard a {
    color: #1a9cb7;
}
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/user/dashboard')}}" title="Go to Home Page">My Account</a>
                            </li>
                            <li class="item cms_page">
                                 <a href="{{url('/user/profile')}}" title="Go to Home Page"><strong>Manage Profile</strong></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
		    <div class="col-md-12">
			  <div class="row" style="margin-bottom: 20px;">
			     <div class="col-md-4 dashboard-profile">
			         <div class="dashboard-mod-title">Personal Profile <span>|</span> 
			           <a data-spm="dprofile_edit" href='{{url("/user/profile/edit")}}'>EDIT</a>
			         </div>
			         <div class="dashboard-info">
			             <div class="dashboard-info-item">{{Auth::user()->name}}</div>
			             <div class="dashboard-address-phone">{{$details->phone}}</div>
			             <div class="dashboard-info-item">{{Auth::user()->email}}</div>
			             <!--<div class="dashboard-info-item last"><a href="javascript:;">Subscribe to our Newsletter</a></div>-->
			         </div>
			     </div>
			     <div class="col-md-4 dashboard-profile">
			         <div class="dashboard-mod-title">Address Book <span>|</span> 
			           <a data-spm="dprofile_edit" href='{{url("/user/address/edit")}}'>EDIT</a>
			         </div>
			         <div class="dashboard-info">
			             <div class="dashboard-info-item">{{Auth::user()->name}}</div>
			             <div class="dashboard-address-detail">{{$details->address}}</div>
			             <div class="dashboard-address-detail">{{$region}} - {{$city}} - {{$details->zip}}</div>
			             
			         </div>
			     </div>
			  </div>
			</div>
        </div>
    </div>
</main>
@endsection