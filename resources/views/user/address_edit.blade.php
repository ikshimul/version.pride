@extends('layouts.app')
@section('title', 'Edit Address')
@section('content')
<style>
  .dashboard-profile {
    /*width: 288px;*/
    height: auto;
    background-color: #eee;
    padding: 16px;
    margin-right: 12px;
    float: left;
}
.dashboard-mod-title {
    color: black;
    font-size: 25px;
    font-weight: 600;
    margin-bottom: 16px;
    border-bottom: 1px solid #00000038;
    height: 32px;
    line-height: 20px;
}
.dashboard-mod-title span {
    color: #dadada;
    font-size: 12px;
}
.dashboard-mod-title a {
    font-size: 12px;
    color: #1a9cb7;
}
.dashboard-info {
    overflow-wrap: break-word;
    word-wrap: break-word;
    -webkit-hyphens: auto;
    -ms-hyphens: auto;
    hyphens: auto;
    overflow-y: auto;
    height: 154px;
}
.dashboard-info-item {
    font-size: 14px;
    color: #424242;
    margin-bottom: 10px;
}
.dashboard-info-item.last {
    margin-top: 20px;
}
.dashboard a {
    color: #1a9cb7;
}
.fsm{
    color:red;
}
.required{
    color:red;
}
.error{
    color:red;
}
.-success{
    color:green;
    font-weight:600;
    font-size:14px;
    border: 1px solid;
    padding: 4px;
    margin-bottom: 15px;
}
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/user/dashboard')}}" title="Go to Home Page">My Account</a>
                            </li>
                            <li class="item cms_page">
                                 <a href="{{url('/user/profile')}}" title="Go to Home Page"><strong>Manage Profile</strong></a>
                            </li>
                            <li class="item cms_page">
                                 <a href='{{url("/user/profile/edit?id=")}}{{Auth::user()->id}}' title="Go to Home Page"><strong>Edit Address</strong></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
		    <div class="col-md-12">
			  <div class="row" style="margin-bottom: 20px;">
			     <div class="col-md-6 col-xs-12 dashboard-profile">
			         <div class="box-bd">
			             @if (session('save'))
                        <center>
                            <div class=" osh-msg-box -success">{{session('save')}}
                            </div>
                        </center>
                        @endif
						<p class="mbs fsm pull-right">* Required fields</p>
                            <form id="form-validate" action="{{url('/user/address/update')}}" method="post">
                                {{ csrf_field() }}
                                <input name="id" id="id" type="hidden"  value="{{$details->id}}">
                                <fieldset class="ui-fieldset">
                                    <div class="unit size1of2">
                                        <div class="ui-formRow mtm">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_first_name">Address <span class="required">*</span></label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('address') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <textarea class="form-control" name="address" id="address" type="text" required>{{$details->address}}</textarea> 
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_email">Region <span class="required">*</span></label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('region') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <select class="form-control input-text" name="region" id="region" required>
                                                        <option value=""> -- Select Region -- </option>
                                                        <?php foreach($regions as $region){ ?>
                                                        <option value="{{$region->id}}">{{$region->name}}</option>
                                                        <?php } ?>
                                                    </select> 
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('region') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_email">City <span class="required">*</span></label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('city') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <select class="form-control input-text" name="city" id="city" required>
                                                        <option value="">Select City</option>
                                                    </select> 
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_email">Zip Code</label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('zip') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <input class="form-control" name="zip" id="zip" type="text"  value="{{$details->zip}}">
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('zip') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow">
                                            <div class="col1">
                                                &nbsp;
                                            </div>
                                            <div class="col2">
                                                
                                                <button style="width:48%;" class="action primary checkout pull-right" type="submit" id="send"><span>Update</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
			     </div>
			  </div>
			</div>
        </div>
    </div>
</main>
<script src="{{ asset('assets/js/validation/jquery.validate.min.js') }}"></script>
<script src="{{asset('assets/js/validation/additional-methods.min.js')}}"></script>
<script>
document.getElementById('region').value = "<?php echo $details->region; ?>"
jQuery( "#form-validate" ).validate({
	messages: {
		address :{
		    required: "Please enter your address"
		},
		region:{
		    required: "Please select your region name"
		},
		city:{
		    required: "Please enter your city name"
		}
	},
// 	submitHandler: function(form) {
//         jQuery.ajax({
//             url: form.action,
//             type: form.method,
//             data: jQuery(form).serialize(),
//             success: function(response) {
//                 jQuery('#answers').html(response);
//             }            
//         });
//     }
});
    jQuery('#dob_cal').on('click', function() {
        $('#datepicker').datepicker('show');
    });
jQuery(document).ready(function($){
       var region_id =$("#region").val();
		var url_op = base_url + "/get-citylist/" + region_id;
		$.ajax({
			url: url_op,
			type: 'GET',
			dataType: 'json',
			data: '',
			success: function (data) {
				//alert(data);
				$('#city').empty();
				$('#city').append('<option value=""> select city </option>');
				$.each(data, function (index, cityobj) {
					$('#city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
				});
				// $('#city').focus().select();
				 document.getElementById('city').value = "<?php echo $details->city; ?>"
				 
			}
		});
	   $("#region").on('change',function(){
	    var region_id =$("#region").val();
		var url_op = base_url + "/get-citylist/" + region_id;
		$.ajax({
			url: url_op,
			type: 'GET',
			dataType: 'json',
			data: '',
			success: function (data) {
				//alert(data);
				$('#city').empty();
				$('#city').append('<option value=""> select city </option>');
				$.each(data, function (index, cityobj) {
					$('#city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
				});
				 $('#city').focus().select();
				 document.getElementById('city').value = "<?php echo $details->city; ?>"
				 
			}
		});
    });
});
</script>
@endsection