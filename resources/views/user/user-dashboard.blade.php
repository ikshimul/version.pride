@extends('layouts.app')
@section('title', 'Your Account')
@section('content')
<style>
    .sidebar {
        padding: 0 20px 9999px 0;
        font-size: 13px;
        margin: 0 0 -9999px;
        border-right: 1px solid #eee;
    }
	.block-dashboard-info .block-content .box-actions a {
    padding: 0 0 1px;
    margin: 0 5px 0 0;
    border-bottom: 1px solid black;
}
.a-box {
    display: block;
    border-radius: 4px;
    border: 1px #ddd solid;
    background-color: #fff;
}
.ya-card--rich {
    height: 100%;
}
.a-box .a-box-inner {
    border-radius: 4px;
    position: relative;
    padding: 14px 18px;
}
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/user/dashboard')}}" title="Go to Home Page">My Account</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Account Dashboard</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
		    <div class="col-md-12">
			  <div class="row" style="margin-bottom: 20px;">
			     <div class="ya-card-cell">
			         <a href="{{url('/user/track/order')}}">
						<div class="col-md-4 col-sm-4 col-lg-4 col-xs-6" style="margin-bottom: 20px;">
							<div class="a-box ya-card--rich">
								<div class="a-box-inner">
									<div class="row">
										<div class="a-column col-md-3">
											<img alt="Your Orders" src="{{url('/')}}/storage/app/public/user-menu/icon-5.png">
										</div>
										<div class="a-column col-md-9 a-span-last">
											<h2 class="a-spacing-none ya-card__heading--rich a-text-normal">
												Your Orders
											</h2>
											<div><span class="a-color-secondary">Track order</span></div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					  </a>
					</div>
					<div class="ya-card-cell">
					    <a href="{{url('/user/profile')}}">
    					<div class="col-md-4 col-sm-4 col-lg-4 col-xs-6" style="margin-bottom: 20px;">
    						<div class="a-box ya-card--rich">
    							  <div class="a-box-inner">
    								<div class="row">
    									<div class="a-column col-md-3">
    										<img alt="Your Profiles" src="{{url('/')}}/storage/app/public/user-menu/icon-4.png">
    									</div>
    									<div class="a-column col-md-9 a-span-last">
    										<h2 class="a-spacing-none ya-card__heading--rich a-text-normal">
    											Your Profiles
    										</h2>
    										<div><span class="a-color-secondary">Manage user profiles for personalized experiences</span></div>
    										
    									</div>
    								</div>
    							  </div>
    						</div>
    					</div>
    					</a>
					</div>
					<div class="ya-card-cell">
					    <a href="{{url('/user/orders')}}">
    					<div class="col-md-4 col-sm-4 col-lg-4 col-xs-6" style="margin-bottom: 20px;">
    						<div class="a-box ya-card--rich">
    						  <div class="a-box-inner">
    								<div class="row">
    									<div class="a-column col-md-3">
    										<img alt="Archived orders" src="{{url('/')}}/storage/app/public/user-menu/icon.png">
    									</div>
    									<div class="a-column col-md-9 a-span-last">
    										<h2 class="a-spacing-none ya-card__heading--rich a-text-normal">
    											Archived orders
    										</h2>
    										<div><span class="a-color-secondary">View and manage your archived orders</span></div>
    										
    									</div>
    								</div>
    							</div>
    						</div>
    					</div>
    					</a>
					</div>
					<div class="ya-card-cell">
    			    	<a href="#">
    					<div class="col-md-4 col-sm-4 col-lg-4 col-xs-6" style="margin-bottom: 20px;">
    						<div data-card-identifier="GiftCards" class="a-box ya-card--rich">
    							<div class="a-box-inner">
    								<div class="row">
    									<div class="a-column col-md-3">
    										<img alt="Gift cards" src="{{url('/')}}/storage/app/public/user-menu/icon.png">
    									</div>
    									<div class="a-column col-md-9 a-span-last">
    										<h2 class="a-spacing-none ya-card__heading--rich a-text-normal">
    											Gift cards
    										</h2>
    										<div><span class="a-color-secondary">View balance, redeem, or reload cards</span></div>
    										
    									</div>
    								</div>
    							</div>
    						</div>
    					</div>
    					</a>
					</div>
					<div class="ya-card-cell">
					    <a href='{{url("/user/change-password")}}'>
    				    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-6" style="margin-bottom: 20px;">
    					    <div data-card-identifier="SignInAndSecurity" class="a-box ya-card--rich">
    							<div class="a-box-inner">
    								<div class="row">
    									<div class="a-column col-md-3">
    										<img alt="Login &amp; security" src="{{url('/')}}/storage/app/public/user-menu/icon-2.png">
    									</div>
    									<div class="a-column col-md-9 a-span-last">
    										<h2 class="a-spacing-none ya-card__heading--rich a-text-normal">
    											Login &amp; security
    										</h2>
    										<div><span class="a-color-secondary">Edit password and mobile number</span></div>
    										
    									</div>
    								</div>
    							</div>
    						</div>
    					</div>
    					</a>
    				</div>
			  </div>
			</div>
        </div>
    </div>
</main>
@endsection