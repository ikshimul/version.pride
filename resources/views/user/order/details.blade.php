<link rel="stylesheet"  href="{{asset('assets/css/order.css')}}?3"/>
<link rel="stylesheet"  href="{{asset('assets/css/order_details_v2.css')}}"/>
<div class="package" id="">
	<div class="dummy-wrapper">
		<div class="package-header">
			<div class="package-header-left">
				<div class="package-info"><span class="lazada lazada-ic-package header-icon lazada-icon text header"></span><span class="text package-info-text">{{ $shipping_address_details->conforder_tracknumber }}</span></div>
				<div class="seller-info">
					<a href="">
					<span class="text">Order on&nbsp;</span>
					<span class="text link"> <?php $place_date=strtotime($shipping_address_details->created_at); echo date('d M, Y  h:i A',$place_date); ?></span>&nbsp;</a>
					<?php if (Auth::check()) { if(Auth::user()->id ==$shipping_address_details->user_id){ ?>
					<div class="im-chat">
					  <span class="lazada lazada-edit1 lazada-icon"></span>
					  <span><a href="#">Exchange</a></span>
					</div>
					<?php } } ?>
				</div>
			</div>
			<div class="package-header-right"></div>
		</div>
		<div class="package-body">
		<div class="shipping-info">
			<div class="shipping-delivery-info">
			   <span class="pull-left text delivery-success"><?php if($shipping_address_details->conforder_deliverydate !=''){ 
				$delivery_date=strtotime($shipping_address_details->conforder_deliverydate);
				echo 'Delivered on '.date('d M, Y',$delivery_date);
				}?></span>
			   <div class="pull-right absolute-top-right">
			       <?php if($shipping_address_details->shipping_charge==120){
			           $delivery="Next Day";
			           $icon="lazada-ic-Express";
			       }elseif($shipping_address_details->shipping_charge==130){
			           $delivery="Same Day";
			           $icon="lazada-ic-Express-new";
			       }else{
			           $delivery="Standard";
			           $icon="lazada-ic-Standard";
			       } ?>
			       <span class="lazada {{$icon}} lazada-icon delivery-method"></span>
			       <span class="text desc">&nbsp;{{$delivery}}</span></div>
			</div>
			<?php  
			     $left="83";
                 $varify="next-step-item-disabled";
                 $processing="next-step-item-disabled";
                 $dispatched="next-step-item-disabled";
                 $delivered="next-step-item-disabled";
             if($shipping_address_details->conforder_status=='Order_Verification_Pending'){
                 $left="83";
                 $varify="next-step-item-process";
                 $processing="next-step-item-disabled";
                 $dispatched="next-step-item-disabled";
                 $delivered="next-step-item-disabled";
             }elseif($shipping_address_details->conforder_status=='Processing' || $shipping_address_details->conforder_status=='Pending_Dispatch'){
                 $left="268";
                 $varify="next-step-item-finish";
                 $processing="next-step-item-process";
                 $dispatched="next-step-item-disabled";
                 $delivered="next-step-item-disabled";
             }elseif($shipping_address_details->conforder_status=='Dispatched'){
                 $left="451";
                 $varify="next-step-item-finish";
                 $processing="next-step-item-finish";
                 $dispatched="next-step-item-process";
                 $delivered="next-step-item-disabled";
             }elseif($shipping_address_details->conforder_status=='Closed'){
                 $left="636";
                 $varify="next-step-item-finish";
                 $processing="next-step-item-finish";
                 $dispatched="next-step-item-finish";
                 $delivered="next-step-item-process";
             }?>
			<div class="progress-info-division">
				<div class="next-step next-step-dot next-step-horizontal progress-bar">
				    <div class="next-step-item {{$varify}} next-step-item-first" style="width: auto;">
						<div class="next-step-item-tail">
							<div class="next-step-item-tail-underlay">
							   <div class="next-step-item-tail-overlay" style="width: 100%;"></div>
							</div>
						</div>
						<div class="next-step-item-container">
							<div class="next-step-item-node">
							   <span class="next-step-item-node-dot"></span>
							</div>
							<div class="next-step-item-title">Varification</div>
						</div>
					</div>
					<div class="next-step-item {{$processing}}" style="width: auto;">
						<div class="next-step-item-tail">
							<div class="next-step-item-tail-underlay">
							   <div class="next-step-item-tail-overlay" style="width: 100%;"></div>
							</div>
						</div>
						<div class="next-step-item-container">
							<div class="next-step-item-node">
							   <span class="next-step-item-node-dot"></span>
							</div>
							<div class="next-step-item-title">Processing</div>
						</div>
					</div>
					<div class="next-step-item {{$dispatched}}" style="width: auto;">
						<div class="next-step-item-tail">
							<div class="next-step-item-tail-underlay">
							   <div class="next-step-item-tail-overlay" style="width: 100%;"></div>
							</div>
						</div>
						<div class="next-step-item-container">
							<div class="next-step-item-node">
							  <span class="next-step-item-node-dot"></span>
							</div>
							<div class="next-step-item-title">Dispatched</div>
						</div>
					</div>
					<div class="next-step-item {{$delivered}} next-step-item-last" style="width: auto;">
						<div class="next-step-item-tail">
						<div class="next-step-item-tail-underlay">
						  <div class="next-step-item-tail-overlay"></div></div>
						</div>
						<div class="next-step-item-container">
							<div class="next-step-item-node">
							   <span class="next-step-item-node-dot"></span>
							</div>
							<div class="next-step-item-title">Delivered</div>
						</div>
					</div>
				</div>
				<!--<div class="tracking-list">-->
				<!--	<div class="translate" style="left: {{$left}}px;"></div>-->
				<!--	<div class="tracking-list-info-table">-->
				<!--		<div class="tracking-item ">-->
				<!--		   <div class="text desc info tracking-item-time light-gray">08 Jun 2021 - 14:30</div>-->
				<!--			<div class="tracking-item-content"><span class="text desc">Thank you for shopping at Pride Limited! Your order is being verified.</span></div>-->
				<!--		</div>-->
				<!--	</div>-->
				<!--	<div class="view-more">-->
				<!--	   <a class="text link">-->
				<!--	  <span class="tracking-view-btn">VIEW MORE</span></a>-->
				<!--	</div>-->
				<!--</div>-->
			</div>
		</div>
		<?php foreach($total_incomplete_order_info as $orderinfo){ ?>
    		<div class="order-item">
    			<div class="item-pic" data-spm="list_image"><a href="#"><img src="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}"></a></div>
    			<div class="item-main ">
    				<div>
    					<div class="text title item-title" data-spm="details_title"><a href="#">{{$orderinfo->product_name}}</a></div>
    					<p class="text info desc">{{$orderinfo->productalbum_name}}</p>
    					<p class="text desc">{{$orderinfo->prosize_name}}</p>
    					<p class="text desc bold"></p>
    				</div>
    			</div>
    			<div class="item-status"><div class="item-price text bold">Tk. {{$orderinfo->product_price*$orderinfo->shoppinproduct_quantity}}</div></div>
    			<div class="item-quantity">
    			  <span><span class="text desc info multiply">Qty:</span><span class="text">&nbsp;{{$orderinfo->shoppinproduct_quantity}}</span></span>
    			</div>
    			<div class="item-info">
    			   <!--<p class="text link bold item-info-link">exchange</p>-->
    			</div>
    			<div class="clear"></div>
    		</div>
		<?php } ?>
		</div>
	</div>
</div>
<div class="">
	<div class="col-md-7" style="padding-left: 0px;">
	    
	   <?php if (Auth::check()) { if(Auth::user()->id ==$shipping_address_details->user_id){ ?>
		<div class="delivery-summary">
			<div class="delivery-wrapper">
				<h3 class="title">Shipping Address</h3><span class="username">{{$shipping_address_details->Shipping_txtfirstname}}  {{$shipping_address_details->Shipping_txtlastname}}</span>
				<span class="address">
				  <span class="in-line"> {{$shipping_address_details->name}}, {{$shipping_address_details->region_name}}, {{$shipping_address_details->Shipping_txtaddressname}} </span>
				</span>
				<span>{{$shipping_address_details->Shipping_txtphone}}</span>
			</div>
		</div>
		<?php } } ?>
	</div>
	<div class="col-md-5">
		<div class="row total-summary">
			<div>
				<div class="text header">Total Summary</div>
				<div style="padding-bottom: 10px;">
					<div class="row">
					  <span class="text pull-left">Subtotal</span>
					  <span class="text price pull-right">Tk {{$shipping_address_details->shoppingcart_subtotal}}</span>
					 </div>
					<div class="row">
					    <span class="text pull-left">Shipping Fee</span>
					    <span class="text price pull-right">Tk {{$shipping_address_details->shipping_charge}}</span>
					 </div>
					<!--<div class="row">-->
					<!--   <div class="fee-main">-->
					<!--       <span class="text pull-left">Shipping Fee Promotion</span>-->
					<!--      <span class="text pull-right">-Tk 0</span>-->
					<!--   </div>-->
					<!--</div>-->
				</div>
				<hr><div class="row second-header"><span class="text bold pull-left">Total</span><span class="text bold total-price pull-right">Tk {{$shipping_address_details->shoppingcart_total}}</span></div>
				<div class="row second-header">
				<span class="text bold pull-left"></span><span class="text bold total-price pull-right"></span></div>
				<div>
				   <div class="row"><span class="text">Paid by&nbsp;</span>
				      <span class="text bold">
				          <?php
                                if ($shipping_address_details->payment_method == 'cDelivery') {
                                    echo 'Cash on Delivery';
                                } else if ($shipping_address_details->payment_method == 'ssl') {
                                    echo 'Credit/ Debit Card';
                                } elseif ($shipping_address_details->payment_method == 'bKash') {
                                    echo 'bKash';
                                } else {
                                    echo $shipping_address_details->payment_method;
                                }
                            ?>
				       </span>
				     </div>
				</div>
			</div>
		</div>
	</div>
</div>