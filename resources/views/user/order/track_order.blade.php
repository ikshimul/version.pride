@extends('layouts.app')
@section('title', 'Track Order')
@section('content')
<link rel="stylesheet"  href="{{asset('assets/css/track_order.css')}}"/>
<main id="maincontent" class="page-main"><a id="contentarea" tabindex="-1"></a>
        <div class="beadcumarea">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12"><div class="breadcrumbs">
                            <ul class="items">
                                <li class="item home">
                                    <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                                </li>
                                <li class="item">
                                    <a href="{{url('/user/dashboard')}}" title="Go to Home Page">Account Dashboard</a>
                                </li>
                                <li class="item active">
                                    <strong>Track Order</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="block block-dashboard-info">
                   <div class="block-title"><strong>Track Your Order</strong></div>
                    <hr>
                    <div class="row">
					    <div class="col-md-12">
					        <p style="font-size:14px;font-weight: 600;color: #210fac;padding-bottom: 6px;">A text message and email with the order number was sent to you</p>
						<label class="pure-material-textfield-outlined">
						  <input class="form-control" placeholder=" " value="PLORDER#100-" id="ecr_number">
						  <span>Enter the Order Number</span>
						</label>
						</div>
						<div class="col-md-12" style="text-align:right;">
							<button class="custom-search-btn"  type="submit" id="track_order">
								Search
							</button>
						</div>
					</div>
    				<div id="store-list-loading" style="text-align:center; padding-bottom:10px; display:none"><img style="width:auto; margin:auto" src="{{url('/')}}/storage/app/public/pride-loading-gif_new.gif" /></div>
    				<div id="order-status"></div>
    				<div id="order-status-date" class='orderdetails'></div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    jQuery(function ($) {
        document.getElementById("ecr_number").focus();
        $("#track_order").on('click', function(e){
			//alert('ok');
			var tracknumber=$("#ecr_number").val();
			if (tracknumber.length < 13){
			    alert('Please enter your correct order number');
			    document.getElementById("ecr_number").focus();
			}else{
	        document.getElementById('store-list-loading').style.display = "block";
			var url_op = base_url + "/ajax/user-order-details-by-tracknumber";
			$.ajax({
				url: url_op,
				type: 'GET',
				data: {tracknumber: tracknumber},
				success: function (html) {
				   //alert(html);
				   document.getElementById('store-list-loading').style.display = "none";
				   if(html=='null'){
					   $(".orderdetails").html("<p style='text-align:center;color:red;padding-top:20px;font-weight:600;'font-size:16px;>Sorry, Tracking number not matched.</p>");
				   }else{
					 $(".orderdetails").html(html);
				   }
				}
			});
          }
		 e.preventDefault();
		});
    });
</script>
@endsection

