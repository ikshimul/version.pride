@extends('layouts.app')
@section('title', 'My Orders')
@section('content')
<?php use App\Http\Controllers\user\DashboardController; ?>
<link rel="stylesheet"  href="{{asset('assets/css/user_order_list.css')}}?1"/>
<main id="maincontent" class="page-main"><a id="contentarea" tabindex="-1"></a>
        <div class="beadcumarea">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12"><div class="breadcrumbs">
                            <ul class="items">
                                <li class="item home">
                                    <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                                </li>
                                <li class="item">
                                    <a href="{{url('/user/dashboard')}}" title="Go to Home Page">Account Dashboard</a>
                                </li>
                                <li class="item active">
                                    <strong>Archived orders</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="my-account">
                    @if (session('save'))
                    <center>
                        <div class=" osh-msg-box -success">{{session('save')}}
                        </div>
                    </center>
                    @endif    
                    <div class="page-title">
                        <div class="mi-title">
									<h5>My Orders</h5>
								</div>
                    </div>
                    <div class="dashboard">
                        <div class="row">
                            <div class="col-md-12">
							
								<div class="orders">
								  @foreach($orderlist as $order) 
									<div class="order">
										<div class="order-info">
											<div class="pull-left">
											   <div class="info-order-left-text">Order <a class="link"> {{$order->conforder_tracknumber}}</a></div>
											   <p class="text info desc">Placed on <?php $place_date=strtotime($order->created_at);echo date('d M, Y  h:i A',$place_date)?></p>
											</div>
											<div class="pull-cont"></div>
											<a class="pull-right link manage" href='{{url("user/order?id=$order->id")}}' style="color: #3b3092;">MANAGE</a>
											<div class="clear"></div>
										</div>
										<?php $products=DashboardController::shoppingProduct($order->shoppingcart_id);
										   //dd($products);
										   foreach($products as $product){ ?>
											<div class="order-item">
												<div class="item-pic" data-spm="detail_image">
												   <a href="#"><img src="{{url('/')}}/storage/app/public/pgallery/{{$product->cart_image}}"></a>
												</div>
												<div class="item-main item-main-mini">
													<div>
														<div class="text title item-title" data-spm="details_title">{{$product->product_name}}</div>
														<p class="text desc"></p><p class="text desc bold"></p>
													</div>
												</div>
												<div class="item-quantity"><span><span class="text desc info multiply">Qty:</span><span class="text">&nbsp;{{$product->shoppinproduct_quantity}}</span></span></div>
												<div class="item-status item-capsule"><p class="capsule"><?php echo str_replace("_"," ",$order->conforder_status); ?></p></div>
												<div class="item-info"><p class="text delivery-success"> 
												<?php if($order->conforder_deliverydate !=''){ 
												$delivery_date=strtotime($order->conforder_deliverydate);
												echo 'Delivered on '.date('d M, Y',$delivery_date);
												}?> </p></div>
												<div class="clear"></div>
											</div>
										   <?php } ?>
									</div>
								  @endforeach
								</div>
							
							<div class="center-block"><center>{{ $orderlist->links() }}</center></div>
							
                            </div>
                        </div>
                        
       <!--                 <div class="row">-->
       <!--                     <div class="col-md-12">-->
       <!--                         <div class="mi-account-section">-->
								
							<!--	<div class="mi-account-orders">-->
							<!--		@foreach($orderlist as $order) -->
							<!--		<div class="mi-order">-->
							<!--			<p class="mi-order-status"><b style="font-size:16px;font-weight:500;">Status :</b>-->
							<!--			<?php if ($order->conforder_status == "Order_Verification_Pending") { ?>-->
       <!--                                     <span class="text-primary" style="font-size:16px;"><?php echo 'Verification Pending'; ?></span>-->
       <!--                                 <?php } else if ($order->conforder_status == "Pending_Dispatch") { ?>-->
       <!--                                     <span class="text-info" style="font-size:16px;"><?php echo str_replace("_"," ",$order->conforder_status); ?></span>-->
       <!--                                 <?php } else if ($order->conforder_status == "Dispatch") { ?>-->
       <!--                                     <span class="text-info" style="font-size:16px;"><?php echo str_replace("_"," ",$order->conforder_status); ?></span>-->
       <!--                                 <?php } else if($order->conforder_status == "Bkash_Payment_Receive") { ?>-->
       <!--                                      <span class="text-info" style="font-size:16px;"><?php echo str_replace("_"," ",$order->conforder_status); ?></span>-->
       <!--                                 <?php }else if($order->conforder_status == "Cancelled" || $order->conforder_status == "Invalidate"){ ?>-->
							<!--			   <span class="text-danger" style="font-size:16px;"><?php echo str_replace("_"," ",$order->conforder_status); ?></span>-->
							<!--		    <?php }else if($order->conforder_status == "Processing" || $order->conforder_status == "Closed"){ ?>-->
							<!--			   <span class="text-success" style="font-size:16px;"><?php echo str_replace("_"," ",$order->conforder_status); ?></span>-->
							<!--		    <?php }else{ ?>-->
       <!--                                    <span class="" style="font-size:16px;color:black;"><?php echo str_replace("_"," ",$order->conforder_status); ?></span>-->
       <!--                                 <?php } ?>-->
							<!--			</p>-->
							<!--			<p class="mi-order-id"><b style="font-size:16px;font-weight:500;">Order ID :</b> {{$order->conforder_tracknumber}}</p>-->
							<!--			<p class="mi-order-price"><b style="font-size:16px;font-weight:500;">Total Amount :</b> Tk. <?php echo number_format($order->shoppingcart_total, 2); ?></p>-->
							<!--			<p class="mi-order-button" id="show_button_{{$order->id}}">-->
							<!--				<button class="mi-order-moredetails"  onclick="ShowDetails(<?php echo $order->id;?>)"><i class="fa fa-fw fa-eye"></i> Details</button>-->
							<!--			 <?php if ($order->conforder_status == "Order_Verification_Pending" || $order->conforder_status == "Pending_Dispatch") { ?>-->
											<!--<a class="mi-order-moredetails" href='{{url("/exchange-request/{$order->id}")}}' target="__blank">Exchange Request</a>-->
							<!--				<button class="mi-order-moredetails" onClick="window.location='https://version.pride-limited.com/digital-payment?invoice_id=<?php echo $order->id;?>';" target="__blank">Pay Now</button>-->
							<!--				<button class="mi-order-moredetails"  onclick="CancelOrder(<?php echo $order->id;?>)" style="color:red;">Cancel Order</button>-->
							<!--			 <?php } ?>-->
							<!--			</p>-->
							<!--			<p class="mi-order-button" id="hide_button_{{$order->id}}" style="display:none;">-->
							<!--				<button class="mi-order-moredetails"  onclick="HideDetails(<?php echo $order->id;?>)">Hide Details</button>-->
							<!--			</p>-->
							<!--		</div>-->
							<!--		<div class="orderdetails_{{$order->id}}">-->
									
							<!--	    </div>-->
							<!--		@endforeach-->
							<!--		<div class="center-block"><center>{{ $orderlist->links() }}</center></div>-->
							<!--	</div>-->
							<!--</div>-->
       <!--                     </div>-->
       <!--                 </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    function ShowDetails(conforder_id){
        //alert(conforder_id);
        var url_op = base_url + "/user/order/details/" + conforder_id;
            jQuery.ajax({
                url: url_op,
                type: 'GET',
                success: function (html) {
                   //alert(html);
                   jQuery(".orderdetails_"+conforder_id).html(html);
				   jQuery("#show_button_"+conforder_id).hide();
				   jQuery("#hide_button_"+conforder_id).show();
				   jQuery(".orderdetails_"+conforder_id).fadeIn();
                }
            });
    }
	
	function HideDetails(conforder_id){
		jQuery(".orderdetails_"+conforder_id).fadeOut();
		jQuery("#show_button_"+conforder_id).show();
	    jQuery("#hide_button_"+conforder_id).hide();
	}
	
	function CancelOrder(conforder_id){
		var x = confirm("Are you sure you want to cancel this order?");
		 if (x){
			var url_op = base_url + "/cancel-order/" + conforder_id;
			jQuery.ajax({
				url: url_op,
				type: 'GET',
				success: function (html) {
				   location.reload();
				}
			});
		 }else{
			 return false;
		 }
	}
</script>
@endsection

