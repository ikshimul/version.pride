<?php

use App\Http\Controllers\user\ExchangeController;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="referrer" content="no-referrer-when-downgrade">
        <meta name="description" content="Urban Truth Return Portal">
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=0" name="viewport">
        <title>Exchange portal</title>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:wght@300&display=swap" rel="stylesheet">
        <link rel="icon" type="image/png" href="{{asset('assets_admin/images/logo.png')}}">
        <noscript>
        <style>
            body .default-loader {
                cursor: default;
            }
            .noscript-warning {
                padding: 10px;
                color: #8a6d3b;
                background-color: #fcf8e3;
                font-size: 14px;
                padding: 15px;
                border: 1px solid transparent;
                text-align: center;
            }
            .noscript-warning a {
                color: #337ab7;
                text-decoration: none;
                background-color: transparent;
            }
        </style>
    <div class="noscript-warning">
        <span>Please <a href="https://www.enable-javascript.com/" title="https://www.enable-javascript.com/" target="_blank">enable JavaScript support </a> at your browser to use our website.</span>
    </div>
    </noscript>
    <style>
        .default-loader {
            display: table;
            height: 100vh;
            width: 100vw;
            background: white;
            cursor: wait;
        }
        .logo-img-re{
            width: 150px;
            height: auto;
        }
        .load-content{
            height:100%;
        }
        body {
            font-family: open sans condensed,sans-serif !important;
        }
        input.error{
            border-bottom: 1px solid red !important;
        }
        .error{
            color:red;
        }
    </style>
    
	<link  href="{{ asset('assets/exchange/bootstrap-4.5.3-dist/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link  href="{{ asset('assets/css/order_details.css') }}?9" rel="stylesheet">
	<link id="color-scheme" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.css') }}" rel="stylesheet" />
	<link  href="{{ asset('assets/exchange/style.css') }}?1" rel="stylesheet" />
    <script src="{{asset('assets/booking/js/jquery.min.js')}}"></script>
	
    <script>
       var base_url = "{{ URL::to('') }}";
    </script>
	<style>
		.form-search .iconic {
			display: block;
			position: absolute;
			top: 41px;
            left: 26px;
			font-family: 'Icon';
		}
		.form-search .iconic * {
			fill: #b0b0b0;
			stroke: #b0b0b0;
		}
		.iconic-property-stroke {
			fill: none !important;
		}
		.form-search input[type="search"] {
			-webkit-appearance: none;
		}
		.form-search .input-lg, .form-search .input-group-lg > .form-control, .form-search .input-group-lg > .input-group-addon, .form-search .input-group-lg > .input-group-btn > .btn {
				padding-left: 40px;
			}
		input.form-control {
			-webkit-appearance: none;
			font-size: 13px;
			border-radius: 1px;
			border: 0;
			box-shadow: 0 0 0 1px #ededed, 0 0 0 2px transparent;
			-webkit-transition: background-color 50ms linear;
			transition: background-color 50ms linear;
		}
		
		.searchsuite-autocomplete {
			background-color: #fff;
			left: 12px;
			margin-top: 2px;
			width: 230px;
			position: absolute;
			z-index: 9999999;
			-webkit-box-shadow: 0 2px 0 0 rgb(127 127 127 / 10%);
			box-shadow: 0 2px 0 0 rgb(127 127 127 / 10%);
		}
		.searchsuite-autocomplete .products ul {
			background: #f1f1f1;
			padding: 10px;
			height: 333px;
			overflow: auto;
		}
		.searchsuite-autocomplete ul li {
			color: #000;
			cursor: unset;
			padding: 5px 0;
			font-weight: 600;
			font-size: 12px;
		}
		.searchsuite-autocomplete .products ul li {
			border-bottom: 1px solid #d4d2d2;
		}
		.searchsuite-autocomplete ul li .qs-option-image {
			max-width: 82px;
			width: 30%;
			margin: 0 auto 10px;
		}
		.searchsuite-autocomplete ul li .qs-option-info {
			color: #000;
			padding: 0 0 5px;
			text-align: center;
		}
		.font-26{
		font-size:26px;
    	}
    	.font-16{
    		font-size:16px;
    	}
    	  .btn:before {
    		font-family: "FontAwesome";
    		speak: none;
    		font-style: normal;
    		font-weight: normal;
    		font-variant: normal;
    		text-transform: none;
    		line-height: 1;
    		position: relative;
    		-webkit-font-smoothing: antialiased;
    	}
    	.btn-sep:before {
        background: rgba(0, 0, 0, 0.15);
    	}
    	.btn-2:before {
    		position: absolute;
    		height: 100%;
    		left: 0;
    		top: 0;
    		line-height: 2;
    		font-size: 122%;
    		width: 47px;
    	}
    	.btn-sep:before {
    		background: rgba(0, 0, 0, 0.15);
    	}
    	.icon-card:before {
    		content: "\f09d";
    	}
    	
    	.icon-download:before {
    		content: "\f1c1";
    	}
    	.btn{
    		position: relative;
    		border-radius: 0px;
            font-size: 14px
    	}
    	.btn:after {
    		content: "";
    		position: absolute;
    		z-index: -1;
    		-webkit-transition: all 0.3s;
    		-moz-transition: all 0.3s;
    		transition: all 0.3s;
    	}
    	.btn-sep {
        padding: 6px 30px 6px 73px;
       }
    	.badge {
    		display: inline-block;
    		padding: 6px 12px;
    		font-size: 125%;
    		font-weight: 500;
    		line-height: 1;
    		text-align: center;
    		white-space: nowrap;
    		vertical-align: baseline;
    		border-radius: 0px;
    		transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    	}
    	.badge-warning {
            color: #fff;
            background-color: #fbbc00;
        }
    	.invoice-heading{
    		font-size: 17px;
    		font-weight: 500;
    		padding-right: 12px;
    	}
    	
        .btn {
            padding: 6px 30px 6px 73px !important;
            border-radius: 0px !important;
        }
        .btn-info {
            color: #fff;
            background-color: #17a2b8 !important;;
            border-color: #17a2b8 !important;;
        }
        .btn-primary {
            color: #fff;
            background-color: #007bff !important;
            border-color: #007bff !important;
        }
        .product-search{
            display:none;
        }
	.font-26{
		font-size:26px;
	}
	.font-16{
		font-size:16px;
	}
	  .btn:before {
		font-family: "FontAwesome";
		speak: none;
		font-style: normal;
		font-weight: normal;
		font-variant: normal;
		text-transform: none;
		line-height: 1;
		position: relative;
		-webkit-font-smoothing: antialiased;
	}
	.btn-sep:before {
    background: rgba(0, 0, 0, 0.15);
	}
	.btn-2:before {
		position: absolute;
		height: 100%;
		left: 0;
		top: 0;
		line-height: 2;
		font-size: 122%;
		width: 47px;
	}
	.btn-sep:before {
		background: rgba(0, 0, 0, 0.15);
	}
	.icon-card:before {
		content: "\f09d";
	}
	
	.icon-download:before {
		content: "\f1c1";
	}
	.btn{
		position: relative;
		border-radius: 0px;
        font-size: 14px
	}
	.btn:after {
		content: "";
		position: absolute;
		z-index: -1;
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}
	.btn-sep {
    padding: 6px 30px 6px 73px;
   }
	.badge {
		display: inline-block;
		padding: 6px 12px;
		font-size: 125%;
		font-weight: 500;
		line-height: 1;
		text-align: center;
		white-space: nowrap;
		vertical-align: baseline;
		border-radius: 0px;
		transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
	.badge-warning {
        color: #fff;
        background-color: #fbbc00;
    }
	.invoice-heading{
		font-size: 17px;
		font-weight: 500;
		padding-right: 12px;
	}
	.table {
		width: 100%;
		margin-bottom: 1rem;
		color: #212529;
	}
	table {
		border-collapse: collapse;
	}
	.table-bordered {
		border: 1px solid #dee2e6;
	}
	.table-bordered td, .table-bordered th {
		border: 1px solid #dee2e6;
	}
	.table td, .table th {
		padding: .75rem;
		vertical-align: top;
		border-top: 1px solid #dee2e6;
	}
	.table-responsive {
		display: block;
		width: 100%;
		overflow-x: auto;
		-webkit-overflow-scrolling: touch;
	}
	.orderdetails-product-image {
          -webkit-box-flex: 0;
          -webkit-flex: 0 0 80px;
              -ms-flex: 0 0 80px;
                  flex: 0 0 80px;
          max-width: 80px;
          margin-right: 15px;
          border: 1px solid #e7e7e7;
        }
    .product-search{
            display:none;
        }
        #loading-image {
            position:fixed;
            left:0;
            top:0;
            width:100vw;
            height:100vh;
            background:#333;
            opacity:0.8;
            margin:0;
            z-index:999;
        }
        #loading-image img {
            position:absolute;
            left:50%;
            top:50%;
            transform:translate(-50%, -50%);
            opacity:1;
        }
	</style>
</head>
<body>
    <div id="root" class="start active">
        <div class="loader d-none">
            <div class="">
                <img src="{{ url('/')}}/storage/app/public/loader.gif" class="loading" alt="...">
                <div class="message">Please be patient while we process your request. It may take up to 1 minute. Please do not close this window.</div>
            </div>
        </div>
        <div class="main-layout">
            <header style="box-shadow: 0 0 6px 0px rgb(0 0 0 / 24%);">
                <div class="header-body">
                    <div class="main-container container">
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-center header-image col-md-4 order-md-2">
                                <button tabindex="0" aria-label="logo" type="button" class="link retailer-logo btn btn-link">
                                    <img class="logo-img-re" src="{{url('/')}}/storage/app/public/logo_new.png" alt="Pride Limited">
                                </button>
                            </div>
                            <div class="d-flex align-items-center header-language col-md-4 order-md-1">
                                <div class="lang-menu">
                                </div>
                            </div>
                            <div class="d-flex align-items-center header-help col-md-4 order-md-3">
                                <div class="need-help" tabindex="0">
                                    <button type="button" class="link btn btn-link">
                                        <img src="{{ url('/')}}/storage/app/public/question.svg" alt="Need help?">Need help?
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div class="content" style="padding-top:4px;">
                <div class="content-body p-4 bg-light">
                    <div class="container-fluid">
					  <div class="row">
					  <div class="col-md-12 col-sm-12 col-xl-12">
                        <p class="font-26 text-center">Pride Limited Invoice</p>
						<p class="font-16 text-center">Your invoice for your order {{ $orderinfo->conforder_tracknumber }}</p>
						</div>
					  </div>
                    </div>
                </div>
				<div class="container-fluid p-4">
				   <div class="row">
					  <div class="col-auto mr-auto">
					      <p>
					          <span class="invoice-heading">Invoice {{ $orderinfo->conforder_tracknumber }}</span>
					          <span class="badge badge-warning">Unpaid</span>
					     </p>
					  </div>
					  <div class="col-auto">
					    <!--<a href="https://pride-limited.com/digital-payment?invoice_id={{$order_id}}" target="_blank" class="btn btn-info btn-sm btn-2 btn-sep icon-card" role="button" aria-pressed="true"> Pay</a> --->
						<!--<a href="https://pride-limited.com/exchange-print" target="_blank" class="btn btn-primary btn-sm btn-2 btn-sep icon-download" role="button" aria-pressed="true"> Download</a>-->
						<a href="#"  class="btn btn-primary btn-sm btn-2 btn-sep icon-download" role="button" aria-pressed="true"> Download</a>
					  </div>
					</div>
					<hr>
					<div class="row">
					  <div class="col-auto mr-auto">
					    <p class="font-16 font-weight-bold">Bill To: </p>
						<address>
						  {{$userinfo->Shipping_txtfirstname .' '.$userinfo->Shipping_txtlastname}}<br>
						  {{$userinfo->Shipping_txtaddressname}}<br>
						  {{$userinfo->Shipping_txtphone}}
						</address>
					  </div>
					  <div class="col-auto">
					    <p class="font-16 font-weight-bold">Shipped To: </p>
					    <address>
						  {{$userinfo->Shipping_txtfirstname .' '.$userinfo->Shipping_txtlastname}}<br>
						  {{$userinfo->Shipping_txtaddressname}}<br>
						  {{$userinfo->Shipping_txtphone}}
						</address>
					  </div>
					</div>
					<div class="row" style="padding-bottom:355px;">
					    <div id="message_{{$order_id}}_{{$i}}"></div>
					    <div style="display:none;" id="loading-image">
					        <img src="{{ url('/')}}/storage/app/public/loader.gif" class="loading" alt="...">
					       </div>
						<div class="col-sm-12 col-md-6 col-lg-4 col-xs-12">
						   <div class="row">
						     <div class="col-sm-12 col-xs-12">
						        <div class="mi-orderdetails">
                                   <div class="mi-orderdetails-body">
        							    <div class="mi-orderdetails-product">
        									<div class="mi-orderdetails-product-image" id="content_img_{{$orderinfo->id}}_{{$i}}">
        										<img src="{{url('/')}}/storage/app/public/pgallery/{{$shoppinproduct_info->cart_image}}" width="100%" alt="product image">
        									</div>
        									<input id="order_no_en" type="hidden" value=""/>
											<input id="mobile_no_en" type="hidden" value=""/>
        									<div class="mi-orderdetails-product-content" id="content_edit_{{$orderinfo->id}}_{{$i}}" style="display:block;">
        									  <form action="{{url('/cart/add-to-cart')}}" method="post" id="product_exchange_form_{{$orderinfo->id}}_{{$i}}">
        										<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        										<?php
        										$shipping_charge = $shoppinproduct_info->Shipping_Charge;
        										$selectdestination = $shoppinproduct_info->selectdestination;
        										$dmselect = $shoppinproduct_info->dmselect;
        										?>
        										<h5 class="mi-orderdetails-product-title"><a href="#">{{$product_info->product_name}}</a></h5>
        										<span class="mi-orderdetails-product-edit">Color: 
        										   <?php $Colorlist = ExchangeController::GetColorListByProductId($product_info->id);
        										   ?>
        											<select class="form-control" name="new_productalbum_name" id="ColorId_{{$orderinfo->id}}_{{$i}}" onchange="Productcolor(<?php echo $shoppinproduct_info->product_id;?>,<?php echo $order_id;?>,<?php echo $i;?>)">
        												<?php
        												foreach ($Colorlist as $color) {
        													?>
        													<option value="<?php echo $color->productalbum_name; ?>"<?php
        													if ($color->productalbum_name == $shoppinproduct_info->productalbum_name) {
        														echo ' selected';
        													}
        													?>><?php echo $color->productalbum_name; ?></option>
        															<?php
        														}
        														?>										 
        											</select>
        										</span>
        										<span class="mi-orderdetails-product-edit">Size : 
        										   <select class="form-control" name="new_prosize_name" id="SizeId_{{$order_id}}_{{$i}}" onchange="Productsize(<?php echo $shoppinproduct_info->product_id;?>,<?php echo $order_id;?>,<?php echo $i;?>)">
        												<?php
        												$Sizelist = ExchangeController::GetSizeListByProductId($shoppinproduct_info->product_id, $shoppinproduct_info->productalbum_name);
        												foreach ($Sizelist as $Size) {
        													?>
        													<option value="<?php echo $Size->productsize_size; ?>"<?php
        													if ($Size->productsize_size == $shoppinproduct_info->prosize_name) {
        														echo ' selected';
        													}
        													?>><?php echo $Size->productsize_size; ?></option>
        															<?php
        														}
        														?>
        											</select>
        										</span>
        										<span class="mi-orderdetails-product-edit">Quantity : 
        											<select class="form-control" name="new_shoppinproduct_quantity" id="QtyId_{{$order_id}}_{{$i}}">
        												<?php
        												$Qtys = ExchangeController::GetQtyProductId($shoppinproduct_info->product_id, $shoppinproduct_info->productalbum_name, $shoppinproduct_info->prosize_name);
        												for ($q = 1; $q <= $Qtys + 1; $q++) {
        													?>
        													<option value="<?php echo $q; ?>" <?php
        													if ($q == $shoppinproduct_info->shoppinproduct_quantity) {
        														echo ' selected';
        													}
        													?>><?php echo $q; ?></option>
        														<?php }
        														?>
        											</select>
        										</span>
												<input type="hidden" name="product_id"  id="order_id_<?php echo $i; ?>" value="{{$order_id}}">
        										<input type="hidden" name="product_id"  id="product_id_<?php echo $i; ?>" value="{{$shoppinproduct_info->product_id}}">
        										<input type="hidden" name="product_price"  id="product_price_<?php echo $i; ?>" value="{{$shoppinproduct_info->product_price}}">
        										<input type="hidden" name="shoppinproduct_id"  id="shoppinproduct_id_<?php echo $i; ?>" value="{{$shoppinproduct_info->id}}">
        										<input type="hidden" name="shoppingcart_id"  id="shoppingcart_id_<?php echo $i; ?>" value="{{$shoppinproduct_info->shoppingcart_id}}">
        										<input type="hidden" name="productalbum_name"  id="Color_<?php echo $i; ?>" value="{{$shoppinproduct_info->productalbum_name}}">
        										<input type="hidden" name="prosize_name"  id="ProSize_<?php echo $i; ?>" value="{{$shoppinproduct_info->prosize_name}}">
        										<input type="hidden" name="shoppinproduct_quantity"  id="ProQty_<?php echo $i; ?>" value="{{$shoppinproduct_info->shoppinproduct_quantity}}">
        										<input type="hidden" name="Shipping_Charge"  id="Shipping_Charge_<?php echo $i; ?>" value="{{$shipping_charge}}">
        										<input type="hidden" name="selectdestination"  id="selectdestination_<?php echo $i; ?>" value="{{$selectdestination}}">
        										<input type="hidden" name="dmselect"  id="dmselect_<?php echo $i; ?>" value="{{$dmselect}}">
        										<a type="submit" class="mi-order-moredetails custom-btn-edit" href="#" id="update_{{$order_id}}_{{$i}}" onclick="Exchange(<?php echo $order_id;?>,<?php echo $i;?>)">Exchange</a> 
												<div id="stockmessage_{{$order_id}}_{{$i}}"></div>
        										<a class="mi-order-moredetails custom-btn-edit" href="#" id="exchangeproduct_{{$order_id}}_{{$i}}" onclick="Exchangeproduct(<?php echo $order_id;?>,<?php echo $shoppinproduct_info->id;?>,<?php echo $i;?>)">Exchange with different Product</a>
        										<!--<a class="mi-order-moredetails custom-btn-edit" href="#" id="exit_{{$order_id}}_{{$i}}" onclick="exit(<?php echo $order_id;?>,<?php echo $i;?>)">Exit</a>-->
        									 </form>
        									</div>
        									<div class="mi-orderdetails-product-content" id="content_{{$order_id}}_{{$i}}" style="display:none;">
												<h5 class="mi-orderdetails-product-title"><a href="#">{{$product_info->product_name}}</a></h5>
												<span class="mi-orderdetails-product-color">Color: {{$shoppinproduct_info->productalbum_name}}</span>
												<span class="mi-orderdetails-product-size">Size : {{$shoppinproduct_info->prosize_name}}</span>
												<span class="mi-orderdetails-product-price">
													Tk. {{$shoppinproduct_info->product_price*$shoppinproduct_info->shoppinproduct_quantity}} </span>
											</div>
        								</div>
        							</div>
        						 </div>
							 </div>
						   </div>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-4 col-xs-12 product-search" id="searchproduct_{{$order_id}}_{{$i}}" style="padding-top: 31px;">
						  <div class="form-search">
						    <div>
							  <input type="search" value="" name="question" id="question" autocomplete="off" placeholder="Search product here" class="form-control input-lg">
								  <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="14" height="14" data-icon="magnifying-glass" class="iconic-magnifying-glass iconic-size-sm injected-svg iconic" viewBox="0 0 16 16" data-src="/iconic/svg/magnifying-glass-sm.svg">
								  <g class="iconic-metadata">
									<title>Magnifying Glass</title>
								  </g>
								  <g class="iconic-container" data-width="16" data-height="16">
									<path stroke="#000" stroke-width="3" stroke-linecap="round" class="iconic-magnifying-glass-handle iconic-property-stroke" fill="none" d="M11 11l3.5 3.5"></path>
									<circle stroke="#000" stroke-width="2" cx="6.5" cy="6.5" r="5.5" class="iconic-magnifying-glass-rim iconic-property-stroke" fill="none"></circle>
								  </g>
								</svg>
							</div>
						  </div>
						  <div id="searchsuite_autocomplete" class="searchsuite-autocomplete" data-bind="visible: showPopup()" style="display: block;"></div>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-4 col-xs-12">
						  <div class="product-details"></div>
						</div>
					  </div>
				</div>
            </div>
            <footer class="footer">
                <div class="footer-body">
                    <div class="main-container container">
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-center justify-content-lg-start footer-retailer-logo-wrapper col-lg-6">
                                <div class="footer-retailer-logo mb-2 mb-lg-0">
                                    <button aria-label="logo" type="button" class="retailer-logo-footer btn btn-link">
                                        <img class="logo-img-re" src="{{url('/')}}/storage/app/public/logo_new.png" alt="Pride Limited">
                                    </button>
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center justify-content-lg-end footer-retailer-info-wrapper col-lg-6">
                                <div class="footer-information">
                                    <div align="center">By using this service you agree to Pride Limited:&nbsp;<a href="#">Terms</a>&nbsp;&amp;&nbsp;
                                        <a href="#">Privacy Policy.</a>&nbsp;<br>Pride Limited © 2021 All Rights Reserved.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <i id="isMobile"></i>
    </div>
    <!--<script type = "text/javascript" src = "{{ asset('assets/js/exchange-new.js') }}?4"></script>-->
    <script src="{{ asset('assets/js/validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/exchange.js') }}?5"></script>
    <script>
		$(document).ready(function () {
			$("#question").keyup(function () {
				var question = $("#question").val();
				//  alert(question);
				if (question == '') {
					$("#searchsuite_autocomplete").css("display", "none");
					$('.product-details').css("display", "none");
				} else {
					question = $("#question").val();
					var url = base_url + "/user/order/exchange/search/" + question;
					$.ajax({
						url: url,
						type: 'GET',
						success: function (data) {
							//  alert(data);
							$("#searchsuite_autocomplete").css("display", "block");
							$('.searchsuite-autocomplete').html(data);
							$(".get_name").click(function(){
							    event.preventDefault();
							    $("#loading-image").show();
								stylecode = $(this).attr('data-value');
								var question = $("#question").val('');
								var question = $("#question").val(stylecode);
								var shoppinproduct_id = $("#shoppinproduct_id_1").val();
								//alert(stylecode);
								$("#searchsuite_autocomplete").css("display", "none");
								var product = base_url + "/user/order/exchange/search-product-details/" + stylecode+ "/"+shoppinproduct_id;
								$.ajax({
									url: product,
									type: 'GET',
									success: function (details) {
									    $("#searchsuite_autocomplete").css("display", "none");
									    $("#loading-image").hide();
										$(".product-details").css("display", "block");
										$('.product-details').html(details);
									}
								});
							});
						}
					});
				}
			});
			
			$('.retailer-logo').on('click', function (e) {
                window.open('https://pride-limited.com', '_blank');
            });
            $('.need-help').on('click', function (e) {
               // alert('ok');
                window.open('https://pride-limited.com/contact-us', '_blank');
            });
			
		});
		function getstylecode(stylecode){
		   //alert(stylecode);
		   document.getElementById("question").value =stylecode;
		}
</script>
</body>
</html>
