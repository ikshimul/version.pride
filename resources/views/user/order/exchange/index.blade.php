<?php
  use App\Http\Controllers\user\ExchangeController;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="referrer" content="no-referrer-when-downgrade">
        <meta name="description" content="Pride Limited Return Portal">
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=0" name="viewport">
        <title>Exchange portal</title>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:wght@300&display=swap" rel="stylesheet">
        <link rel="icon" type="image/png" href="{{asset('assets_admin/images/logo.png')}}">
        <noscript>
        <style>
            body .default-loader {
                cursor: default;
            }
            .noscript-warning {
                padding: 10px;
                color: #8a6d3b;
                background-color: #fcf8e3;
                font-size: 14px;
                padding: 15px;
                border: 1px solid transparent;
                text-align: center;
            }
            .noscript-warning a {
                color: #337ab7;
                text-decoration: none;
                background-color: transparent;
            }
        </style>
    <div class="noscript-warning">
        <span>Please <a href="https://www.enable-javascript.com/" title="https://www.enable-javascript.com/" target="_blank">enable JavaScript support </a> at your browser to use our website.</span>
    </div>
    </noscript>
    <style>
        .default-loader {
            display: table;
            height: 100vh;
            width: 100vw;
            background: white;
            cursor: wait;
        }
        .logo-img-re{
            width: 100px;
            height: auto;
        }
        .load-content{
            height:100%;
        }
        body {
            font-family: open sans condensed,sans-serif !important;
        }
        input.error{
            border-bottom: 1px solid red !important;
        }
        .error{
            color:red;
        }
    </style>
    
	<link  href="{{ asset('assets/exchange/bootstrap-4.5.3-dist/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link id="color-scheme" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.css') }}" rel="stylesheet" />
	<link  href="{{ asset('assets/exchange/style.css') }}?1" rel="stylesheet" />
    <script src="{{asset('assets/booking/js/jquery.min.js')}}"></script>
	
    <script>
var base_url = "{{ URL::to('') }}";
    </script>
	<style>
	.font-26{
		font-size:26px;
	}
	.font-16{
		font-size:16px;
	}
	  .btn:before {
		font-family: "FontAwesome";
		speak: none;
		font-style: normal;
		font-weight: normal;
		font-variant: normal;
		text-transform: none;
		line-height: 1;
		position: relative;
		-webkit-font-smoothing: antialiased;
	}
	.btn-sep:before {
    background: rgba(0, 0, 0, 0.15);
	}
	.btn-2:before {
		position: absolute;
		height: 100%;
		left: 0;
		top: 0;
		line-height: 2;
		font-size: 122%;
		width: 47px;
	}
	.btn-sep:before {
		background: rgba(0, 0, 0, 0.15);
	}
	.icon-card:before {
		content: "\f09d";
	}
	
	.icon-download:before {
		content: "\f1c1";
	}
	.btn{
		position: relative;
		border-radius: 0px;
        font-size: 14px
	}
	.btn:after {
		content: "";
		position: absolute;
		z-index: -1;
		-webkit-transition: all 0.3s;
		-moz-transition: all 0.3s;
		transition: all 0.3s;
	}
	.btn-sep {
    padding: 6px 30px 6px 73px;
   }
	.badge {
		display: inline-block;
		padding: 6px 12px;
		font-size: 125%;
		font-weight: 500;
		line-height: 1;
		text-align: center;
		white-space: nowrap;
		vertical-align: baseline;
		border-radius: 0px;
		transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
	.badge-warning {
        color: #fff;
        background-color: #fbbc00;
    }
	.invoice-heading{
		font-size: 17px;
		font-weight: 500;
		padding-right: 12px;
	}
	.table {
		width: 100%;
		margin-bottom: 1rem;
		color: #212529;
	}
	table {
		border-collapse: collapse;
	}
	.table-bordered {
		border: 1px solid #dee2e6;
	}
	.table-bordered td, .table-bordered th {
		border: 1px solid #dee2e6;
	}
	.table td, .table th {
		padding: .75rem;
		vertical-align: top;
		border-top: 1px solid #dee2e6;
	}
	.table-responsive {
		display: block;
		width: 100%;
		overflow-x: auto;
		-webkit-overflow-scrolling: touch;
	}
	.orderdetails-product-image {
          -webkit-box-flex: 0;
          -webkit-flex: 0 0 80px;
              -ms-flex: 0 0 80px;
                  flex: 0 0 80px;
          max-width: 80px;
          margin-right: 15px;
          border: 1px solid #e7e7e7;
        }
	</style>
</head>
<body>
    <div id="root" class="start active">
        <div class="loader d-none">
            <div class="">
                <img src="{{ url('/')}}/storage/app/public/loader.gif" class="loading" alt="...">
                <div class="message">Please be patient while we process your request. It may take up to 1 minute. Please do not close this window.</div>
            </div>
        </div>
        <div class="main-layout">
            <header style="box-shadow: 0 0 6px 0px rgb(0 0 0 / 24%);">
                <div class="header-body">
                    <div class="main-container container">
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-center header-image col-md-4 order-md-2">
                                <button tabindex="0" aria-label="logo" type="button" class="link retailer-logo btn btn-link">
                                    <img class="logo-img-re" src="{{url('/')}}/storage/app/public/logo_new.png" alt="Pride Limited">
                                </button>
                            </div>
                            <div class="d-flex align-items-center header-language col-md-4 order-md-1">
                                <div class="lang-menu">
                                </div>
                            </div>
                            <div class="d-flex align-items-center header-help col-md-4 order-md-3">
                                <div class="need-help" tabindex="0">
                                    <button type="button" class="link btn btn-link">
                                        <img src="{{ url('/')}}/storage/app/public/question.svg" alt="Need help?">Need help?
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div class="content" style="padding-top:4px;">
                <div class="content-body p-4 bg-light">
                    <div class="container-fluid">
					  <div class="row">
					  <div class="col-md-12 col-sm-12 col-xl-12">
                        <p class="font-26 text-center">Pride Limited Invoice</p>
						<p class="font-16 text-center">Your invoice for your order {{ $shipping_address_details->conforder_tracknumber }}</p>
						</div>
					  </div>
                    </div>
                </div>
				<div class="container-fluid p-4">
				   <div class="row">
					  <div class="col-auto mr-auto"><p><span class="invoice-heading">Invoice {{ $shipping_address_details->conforder_tracknumber }}</span><span class="badge badge-warning">Unpaid</span></p></div>
					  <div class="col-auto">
					    <a href='{{url("digital-payment?invoice_id={$order_no}")}}' target="_blank" class="btn btn-info btn-sm btn-2 btn-sep icon-card" role="button" aria-pressed="true"> Pay</a>
						<a href='{{url("exchange-print")}}' target="_blank" class="btn btn-primary btn-sm btn-2 btn-sep icon-download" role="button" aria-pressed="true"> Download</a>
					  </div>
					</div>
					<hr>
					<div class="row">
					  <div class="col-auto mr-auto">
					    <p class="font-16 font-weight-bold">Bill To: </p>
						<address>
						  <?php echo $shipping_address_details->Shipping_txtfirstname.' '.$shipping_address_details->Shipping_txtlastname;?><br>
						  <?php if($shipping_address_details->Shipping_txtaddressname == ''){ echo $shipping_address_details->Shipping_txtaddress1;}else{ echo $shipping_address_details->Shipping_txtaddressname;} ?><br>
						  <?php echo $shipping_address_details->Shipping_txtphone;?>
						</address>
					  </div>
					  <div class="col-auto">
					    <p class="font-16 font-weight-bold">Shipped To: </p>
					    <address>
						  <?php echo $shipping_address_details->Shipping_txtfirstname.' '.$shipping_address_details->Shipping_txtlastname;?><br>
						  <?php if($shipping_address_details->Shipping_txtaddressname == ''){ echo $shipping_address_details->Shipping_txtaddress1;}else{ echo $shipping_address_details->Shipping_txtaddressname;} ?><br>
						  <?php echo $shipping_address_details->Shipping_txtphone;?>
						</address>
					  </div>
					</div>
					<div class="row">
					  <div class="col-md-12">
					     <?php if($message !=''){ ?>
					      <div class="alert alert-success" role="alert">
                              {{$message}}
                            </div>
                        <?php } ?>
						<div class="table-responsive">
							<table  class="table table-bordered">
							   <thead class="light-border">
								<tr>
								  <th>SL</th>
								  <th>Title</th>
								  <th>Discription</th>
								  <th>Size</th>
								  <th>Qty</th>
								  <th>Amount</th>
								  <th>Action</th>
								</tr>
							   </thead>
							   <tbody>
							       <?php $update_subtotal=0; $i=0; foreach($total_incomplete_order_info as $orderinfo){ $i++; ?>
    								 <tr>
    									<td>1</td>
    									<td>
    									    <div>
                    							<img class="orderdetails-product-image" src="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}" alt="product image">
                    							{{$orderinfo->product_name}}
                    						</div>
    									 </td>
    									<td>Color: {{$orderinfo->productalbum_name}}</td>
    									<td>{{$orderinfo->prosize_name}}</td>
    									<td>{{$orderinfo->shoppinproduct_quantity}}</td>
    									<td>Tk {{$orderinfo->product_price*$orderinfo->shoppinproduct_quantity}}</td>
    									<td>
    									  <a href='{{url("/user/order/exchange/different?shoppinproduct_id={$orderinfo->shoppinproduct_id}&order_no={$order_no}")}}'>Exchange</a> 
    									  <!--| <!--<a href="#">Cancel</a>-->
    									</td>
    								 </tr>
    								 <?php } ?>
									 <?php 
									    $extra_amount=0; $update_subtotal=0; $i=0; foreach($total_exchange_request_info as $orderinfo){ $i++;
                                        $shopinfo = ExchangeController::GetPriceVariation($orderinfo->shoppinproduct_id);
										$previous_price=+$shopinfo->product_price;
										$new_price=+$orderinfo->product_price;
										$extra_amount=+$new_price-$previous_price;
									 ?>
    								 <tr>
    									<td>1</td>
    									<td>
    									    <div>
                    							<img class="orderdetails-product-image" src="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}" alt="product image">
                    							{{$orderinfo->product_name}}
                    						</div>
    									 </td>
    									<td>Color: {{$orderinfo->productalbum_name}}</td>
    									<td>{{$orderinfo->prosize_name}}</td>
    									<td>{{$orderinfo->shoppinproduct_quantity}}</td>
    									<td>Tk {{$orderinfo->product_price*$orderinfo->shoppinproduct_quantity}}</td>
    									<td>
    									  Exchange Product
    									</td>
    								 </tr>
    								 <?php } ?>
									 <tr>
									    <td colspan="5" style="text-align: right;">Extra Payment Amount</td>
										<td>Tk {{$extra_amount}}</td>
										<td></td>
									 </tr>
							   </tbody>
							</table>
						  </div>
						</div>
					</div>
				</div>
            </div>
            <footer class="footer">
                <div class="footer-body">
                    <div class="main-container container">
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-center justify-content-lg-start footer-retailer-logo-wrapper col-lg-6">
                                <div class="footer-retailer-logo mb-2 mb-lg-0">
                                    <button aria-label="logo" type="button" class="retailer-logo-footer btn btn-link">
                                        <img class="logo-img-re" src="{{url('/')}}/storage/app/public/logo_new.png" alt="Pride Limited">
                                    </button>
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center justify-content-lg-end footer-retailer-info-wrapper col-lg-6">
                                <div class="footer-information">
                                    <div align="center">By using this service you agree to Pride Limited:&nbsp;<a href="#">Terms</a>&nbsp;&amp;&nbsp;
                                        <a href="#">Privacy Policy.</a>&nbsp;<br>Pride Limited © 2021 All Rights Reserved.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <i id="isMobile"></i>
    </div>
    <!--<script type = "text/javascript" src = "{{ asset('assets/js/exchange-new.js') }}?4"></script>-->
    <script src="{{ asset('assets/js/validation/jquery.validate.min.js') }}"></script>
</body>
</html>