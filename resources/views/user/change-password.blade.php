@extends('layouts.app')
@section('title', 'Change Password')
@section('content')
<style>
  .dashboard-profile {
    /*width: 288px;*/
    height: auto;
    background-color: #eee;
    padding: 16px;
    margin-right: 12px;
    float: left;
}
.dashboard-mod-title {
    color: black;
    font-size: 25px;
    font-weight: 600;
    margin-bottom: 16px;
    border-bottom: 1px solid #00000038;
    height: 32px;
    line-height: 20px;
}
.dashboard-mod-title span {
    color: #dadada;
    font-size: 12px;
}
.dashboard-mod-title a {
    font-size: 12px;
    color: #1a9cb7;
}
.dashboard-info {
    overflow-wrap: break-word;
    word-wrap: break-word;
    -webkit-hyphens: auto;
    -ms-hyphens: auto;
    hyphens: auto;
    overflow-y: auto;
    height: 154px;
}
.dashboard-info-item {
    font-size: 14px;
    color: #424242;
    margin-bottom: 10px;
}
.dashboard-info-item.last {
    margin-top: 20px;
}
.dashboard a {
    color: #1a9cb7;
}
.fsm{
    color:red;
}
.required{
    color:red;
}
.error{
    color:red;
}
.osh-msg-box {
    border-width: 1px;
    border-style: solid;
    padding: 2px 5px;
    margin: 10px 0;
}
.osh-msg-box.-error {
    border-color: #D10B23;
    color: #D10B23;
}
.osh-msg-box.-success {
    border-color: #28a745;
    color: #28a745;
}
.osh-msg-box {
    border-width: 1px;
    border-style: solid;
    padding: 2px 5px;
    margin: 10px 0;
}
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/user/dashboard')}}" title="Go to Home Page">My Account</a>
                            </li>
                            <li class="item cms_page">
                                 <a href='{{url("/user/change-password")}}' title="Go to Home Page"><strong>Login & Security</strong></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
		    <div class="col-md-12">
			  <div class="row" style="margin-bottom: 20px;">
			     <div class="col-md-6 col-xs-12 dashboard-profile">
			         <div class="box-bd">
			             @if (session('save'))
                        <center>
                            <div class=" osh-msg-box -success">{{session('save')}}</div>
                        </center>
                        @endif
                         @if (session('error'))
                        <center>
                            <div class=" osh-msg-box -error">{{session('error')}}</div>
                        </center>
                        @endif
						<p class="mbs fsm pull-right">* Required fields</p>
                            <form id="form-validate" action="{{url('/user/change-password')}}" method="post">
                                {{ csrf_field() }}
                                <input name="id" id="id" type="hidden"  value="{{Auth::user()->id}}">
                                <fieldset class="ui-fieldset">
                                    <div class="unit size1of2">
                                        <div class="ui-formRow">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_email">Mobile <span class="required">*</span></label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('mobile_no') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <input class="form-control" name="mobile_no" id="mobile_no" type="text" value="{{Auth::user()->mobile_no}}" required/>                                                                    
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('mobile_no') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow mtm">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_first_name">Old Password<span class="required">*</span></label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('old_password') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <input class="form-control" name="old_password" id="old_password" type="password" required/>   
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('old_password') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_email">New Password <span class="required">*</span></label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('new_password') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <input class="form-control" name="new_password" id="new_password" type="password" required/>                                                                    
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('new_password') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_email">Confirm New Password <span class="required">*</span></label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('confirm_password') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <input class="form-control" name="confirm_password" id="confirm_password" type="password" required/>                                                                    
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('confirm_password') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow">
                                            <div class="col1">
                                                &nbsp;
                                            </div>
                                            <div class="col2">
                                                <button style="width:48%;" class="action primary checkout pull-right" type="submit" id="send"><span>Update</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
			     </div>
			  </div>
			</div>
        </div>
    </div>
</main>
<script src="{{ asset('assets/js/validation/jquery.validate.min.js') }}"></script>
<script src="{{asset('assets/js/validation/additional-methods.min.js')}}"></script>
<script>
jQuery( "#form-validate" ).validate({
     rules : {
        new_password : {
            minlength : 6
        },
        confirm_password : {
            minlength : 6,
            equalTo : "#new_password"
        }
    },
	messages: {
		mobile_no :{
		    required: "Please enter your address"
		},
		old_password:{
		    required: "Please select your old password"
		},
		new_password:{
		    required: "Please enter your new password"
		},
		confirm_password:{
		    required: "Please enter your confirm password"
		}
	},
});
</script>
@endsection