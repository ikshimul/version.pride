@extends('layouts.app')
@section('title', 'Edit Profile')
@section('content')
<style>
  .dashboard-profile {
    /*width: 288px;*/
    height: auto;
    background-color: #eee;
    padding: 16px;
    margin-right: 12px;
    float: left;
}
.dashboard-mod-title {
    color: black;
    font-size: 25px;
    font-weight: 600;
    margin-bottom: 16px;
    border-bottom: 1px solid #00000038;
    height: 32px;
    line-height: 20px;
}
.dashboard-mod-title span {
    color: #dadada;
    font-size: 12px;
}
.dashboard-mod-title a {
    font-size: 12px;
    color: #1a9cb7;
}
.dashboard-info {
    overflow-wrap: break-word;
    word-wrap: break-word;
    -webkit-hyphens: auto;
    -ms-hyphens: auto;
    hyphens: auto;
    overflow-y: auto;
    height: 154px;
}
.dashboard-info-item {
    font-size: 14px;
    color: #424242;
    margin-bottom: 10px;
}
.dashboard-info-item.last {
    margin-top: 20px;
}
.dashboard a {
    color: #1a9cb7;
}
.fsm{
    color:red;
}
.required{
    color:red;
}
.error{
    color:red;
}
.osh-msg-box {
    border-width: 1px;
    border-style: solid;
    padding: 2px 5px;
    margin: 10px 0;
}
.osh-msg-box.-error {
    border-color: #D10B23;
    color: #D10B23;
}
.osh-msg-box.-success {
    border-color: #28a745;
    color: #28a745;
}
.osh-msg-box {
    border-width: 1px;
    border-style: solid;
    padding: 2px 5px;
    margin: 10px 0;
}
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/user/dashboard')}}" title="Go to Home Page">My Account</a>
                            </li>
                            <li class="item cms_page">
                                 <a href="{{url('/user/profile')}}" title="Go to Home Page"><strong>Manage Profile</strong></a>
                            </li>
                            <li class="item cms_page">
                                 <a href='{{url("/user/profile/edit?id=")}}{{Auth::user()->id}}' title="Go to Home Page"><strong>Edit Profile</strong></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
		    <div class="col-md-12">
			  <div class="row" style="margin-bottom: 20px;">
			     <div class="col-md-6 col-xs-12 dashboard-profile">
			         <div class="box-bd">
			             @if (session('save'))
                        <center>
                            <div class=" osh-msg-box -success">{{session('save')}}</div>
                        </center>
                        @endif
						<p class="mbs fsm pull-right">* Required fields</p>
                            <form id="form-account-edit" action="{{url('/user/profile/update')}}" method="post">
                                {{ csrf_field() }}
                                <input name="id" id="id" type="hidden"  value="{{Auth::user()->id}}">
                                <fieldset class="ui-fieldset">
                                    <div class="unit size1of2">
                                        <div class="ui-formRow mtm">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_first_name">Full Name <span class="required">*</span></label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('name') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <input class="form-control" name="name" id="name" type="text"  value="{{Auth::user()->name}}">   
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_email">E-mail <span class="required">*</span></label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('email') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <input class="form-control" name="email" id="EditForm_email" type="email" value="{{Auth::user()->email}}" />                                                                    
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow">
                                            <div class="col1 txtRight">
                                                <label class="mts" for="EditForm_email">Mobile <span class="required">*</span></label>                            
                                            </div>
                                            <div class="col2">
                                                <div class="collection {{ $errors->has('mobile_no') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
                                                    <input class="form-control" name="mobile_no" id="EditForm_mobile_no" type="text" value="{{Auth::user()->mobile_no}}" />                                                                    
                                                </div>
                                                <span class="help-block error">
                                                    <strong>{{ $errors->first('mobile_no') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="ui-formRow">
                                            <div class="col1">
                                                &nbsp;
                                            </div>
                                            <div class="col2">
                                                <button style="width:48%;" class="action primary checkout pull-right" type="submit" id="send"><span>Update</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
			     </div>
			  </div>
			</div>
        </div>
    </div>
</main>
@endsection