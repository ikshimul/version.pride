@extends('layouts.app')
@section('title','User Details')
@section('content')
<style>
    button {
        width: 100%;
        height: 30px;
        font-size: 10px;
        line-height: 28px;
        color: #fff;
        text-transform: uppercase;
        letter-spacing: 2px;
        position: relative;
        border: none;
        background: rgb(41, 30, 136);
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -ms-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
    .omniauth-container {
        box-shadow: 0 0 0 1px #e5e5e5;
        border-bottom-right-radius: 2px;
        border-bottom-left-radius: 2px;
        padding: 15px;
    }
    .prepend-top-15 {
        margin-top: 25px;
    }
    .d-block {
        display: block !important;
    }
    label.label-bold {
        font-weight: 600;
    }
    .omniauth-btn {
        margin-bottom: 16px;
        width: 48%;
        padding: 8px;
    }
    .omniauth-container .omniauth-btn img {
        width: 18px;
        height: 18px;
        margin-right: 16px;
    }
    .btn, .project-buttons .stat-text {
        border-radius: 3px;
        font-size: 14px;
        height: 38px;
        font-weight: 400;
        padding: 6px 10px;
        background-color: #fff;
        border-color: #e5e5e5;
        color: #2e2e2e;
        color: #2e2e2e;
    }
    .text-left {
        text-align: left !important;
    }
    .align-items-center, .page-title-holder {
        align-items: center !important;
    }
    .btn, .project-buttons .stat-text {
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        user-select: none;

        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 20px;
        border-radius: 0.25rem;
        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }
    img {
        vertical-align: middle;
        border-style: none;
    }
    .justify-content-between {
        justify-content: space-between !important;
    }
    .flex-wrap {
        flex-wrap: wrap !important;
    }
    .d-flex, .page-title-holder, .right-sidebar.build-sidebar .trigger-variables-btn-container {
        display: flex !important;
    }
	.login-container:after {
    position: absolute;
    left: 50%;
    top: 0;
    bottom: 0;
    background: none;
    width: 1px;
    content: '';
}
    .sidebar {
        padding: 0 20px 9999px 0;
        font-size: 13px;
        margin: 0 0 -9999px;
        border-right: 1px solid #eee;
    }
  label.error {
    color: red;
    font-size: 12px;
    padding-top: 10px;
    }
    input.error {
        border: 1px solid #c70000;
    }
    .input-description{
        font-size:10px;
    }
    .form-control{
        font-size:12px;
    }
    </style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>User Details</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
                </div>
                <div class="login-container">
                    <div class="block">
                        <div class="block-title">
                            <strong id="block-new-customer-heading" role="heading" aria-level="2">Please complete your address infomatrion</strong>
                            <center style="color:black;">
                            @if (session('save'))
                            <div class="alert alert-danger">
                                {{ session('save') }}
                            </div>
                            @endif
                            </center>
                        </div>
                        <form class="form create account form-create-account" action="{{route('UserDetailsSave')}}" method="post" id="form-validate" enctype="multipart/form-data" autocomplete="off">
                            {{ csrf_field() }}
							<input type="hidden" name="user_id" value="{{Auth::user()->id}}"/>
                            <fieldset class="fieldset create info">
                                <div class="field">
                                    <label class="label" for="first_name"><span>First Name</span></label>
                                    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                       <input class="form-control" type="text" name="first_name" placeholder="First name*" value="{{ $first_name }}" required />
                                        <div class="help-block with-errors">{{ $errors->first('first_name') }}</div>
                                    </div>
                                </div>
                                <div class="field field-name-lastname required">
                                    <label class="label" for="last_name"><span>Last Name</span></label>
                                    <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <input type="text" id="last_name" class="form-control"
                                               name="last_name"
                                               value="{{ $last_name }}"
                                               title="Last&#x20;Name"
                                               class="input-text required-entry"  required>
                                        <div class="help-block with-errors">{{ $errors->first('last_name') }}</div>
                                    </div>
                                </div>
                                <div class="field gender">
                                    <label class="label" for="gender"><span>Address</span></label>
                                    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                        <textarea type="text" id="lastname" name="address"  title="Last&#x20;Name" class="form-control input-text required-entry"  required>{{ old('address') }}</textarea>
										<div class="help-block with-errors">{{ $errors->first('address') }}</div>
										<span class="input-description">e.g House No 3, Road 5, Block j, Baridhara, Dhaka - 1212</span>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="fieldset create account" data-hasrequired="* Required Fields">
                                <div class="password-area">
                                    <div class="field password required" data-mage-init='{"passwordStrengthIndicator": {}}'>
                                        <label for="password" class="label"><span>Region</span></label>
                                        <div class="control {{ $errors->has('region') ? ' has-error' : '' }}">
										<select class="form-control js-select js-order-country" name="region" id="region" required>
										    <option value="">Select Region</option>
										    @foreach($regions as $region)
											  <option value="{{$region->id}}">{{$region->name}}</option>
											@endforeach
										</select>
                                        </div>
                                    </div>
                                    <div class="field confirmation required">
                                        <label for="password-confirmation" class="label"><span>City</span></label>
                                        <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                            <select class="form-control js-select js-order-country" name="city" id="city" required>
										    
										    </select>
											<div class="help-block with-errors">{{ $errors->first('city') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
							<fieldset class="fieldset create account" data-hasrequired="* Required Fields">
                                <div class="password-area">
                                    <div class="field password required" data-mage-init='{"passwordStrengthIndicator": {}}'>
                                        <label for="password" class="label"><span>Post Code / Zipe</span></label>
                                        <div class="form-group {{ $errors->has('zipcode') ? ' has-error' : '' }}">
										<input class="form-control" id="zipcode" type="text" name="zipcode" data-minlength="4" data-error="Minimum of 4 characters" value="{{ old('zipcode') }}" />
										  <div class="help-block with-errors"> {{ $errors->first('zipcode') }}</div>
                                        </div>
                                    </div>
                                    <div class="field confirmation required">
                                        <label for="password-confirmation" class="label"><span>Mobile Number</span></label>
                                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                             <input class="form-control" id="phone" type="text" name="phone"  data-minlength="11" data-error="Minimum of 11 digits" value="{{Auth::user()->mobile_no}}" required  pattern="^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$" readonly/>
											 <div class="help-block with-errors"> {{ $errors->first('phone') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="actions-toolbar">
                                <div class="primary">
                                    <button type="submit" class="action submit primary" title="Create an Account"><span>Complete Account</span></button>
                                </div>
                            </div>
                        </form>
<!--                        <script>
                            jQuery(function ($) {
                                var dataForm = $('#form-validate');
                                var ignore = 'input[id$="full"]';
                                dataForm.mage('validation', {
                                    errorPlacement: function (error, element) {
                                        if (element.prop('id').search('full') !== -1) {
                                            var dobElement = $(element).parents('.customer-dob'),
                                                    errorClass = error.prop('class');
                                            error.insertAfter(element.parent());
                                            dobElement.find('.validate-custom').addClass(errorClass)
                                                    .after('<div class="' + errorClass + '"></div>');
                                        } else {
                                            error.insertAfter(element);
                                        }
                                    },
                                    ignore: ':hidden:not(' + ignore + ')'
                                }).find('input:text').attr('autocomplete', 'off');
                            });
                        </script>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="{{ asset('assets/js/validation/jquery.validate.min.js') }}"></script>
<script src="{{asset('assets/js/validation/additional-methods.min.js')}}"></script>
<script>
jQuery( "#form-validate" ).validate({
	messages: {
		first_name: {
			required: "Please enter your first name"
		},
		last_name: {
			required: "Please enter your last name"
		},
		address :{
		    required: "Please enter your address"
		},
		region:{
		    required: "Please select your region name"
		},
		city:{
		    required: "Please enter your city name"
		},
		phone:{
		    required: "Please provide your 11 digit mobile number"
		}
	},
// 	submitHandler: function(form) {
//         jQuery.ajax({
//             url: form.action,
//             type: form.method,
//             data: jQuery(form).serialize(),
//             success: function(response) {
//                 jQuery('#answers').html(response);
//             }            
//         });
//     }
});
    jQuery('#dob_cal').on('click', function() {
        $('#datepicker').datepicker('show');
    });
jQuery(document).ready(function($){
//      var region_id =$("#region").val();
// 		var url_op = base_url + "/get-citylist/" + region_id;
// 		$.ajax({
// 			url: url_op,
// 			type: 'GET',
// 			dataType: 'json',
// 			data: '',
// 			success: function (data) {
// 				//alert(data);
// 				$('#city').empty();
// 				$('#city').append('<option value=""> select city </option>');
// 				$.each(data, function (index, cityobj) {
// 					$('#city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
// 				});
// 			   $('#city').focus();
// 			}
// 		});
	   $("#region").on('change',function(){
	    var region_id =$("#region").val();
		var url_op = base_url + "/get-citylist/" + region_id;
		$.ajax({
			url: url_op,
			type: 'GET',
			dataType: 'json',
			data: '',
			success: function (data) {
				//alert(data);
				$('#city').empty();
				$('#city').append('<option value=""> select city </option>');
				$.each(data, function (index, cityobj) {
					$('#city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
				});
				 $('#city').focus().select();
			}
		});
    });
});
</script>
@endsection