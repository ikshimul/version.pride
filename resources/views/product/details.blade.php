@extends('layouts.app')
@section('title',$title)
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/product_details.css')}}?10">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/add_to_cart.css')}}?10">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/product_details_new.css')}}?12">
<style>
 .page-main{
     padding-top: 0px;
 }
.dropdown-menu{
    display:none;
}
a.simple.button {
    margin-bottom:15px;
}
.product-shop {
    margin: 0 0 15px;
    padding: 0 19px;
    padding-top: 0px; 
}
.dropdown-menu>li:first-child {
    border: 1px solid #ddd!important;
    padding: 11px;
}

.selected-color {
    border: 3px solid #291e88!important;
    padding: 2px;
}
@media (max-width: 1000px){
.dropdown-menu > li {
    border: 1px solid #ddd !important;
    padding: 11px !important;
 }
.dropdown-menu>li {
    /* border: none; */
    /* padding: 18px 8px 1px 12px; */
 }
}
#loading-image {
    position:fixed;
    left:0;
    top:0;
    width:100vw;
    height:100vh;
    background:#333;
    opacity:0.8;
    margin:0;
    z-index:999;
}
#loading-image img {
    position:absolute;
    left:50%;
    top:50%;
    width:auto;
    height:auto;
    transform:translate(-50%, -50%);
    opacity:1;
}
.dropdown{
  display: block;
}
.text-14{
    font-size:14px;
}
.match-with-sec .product-items li .product-item-photo {
    max-width: 98%;
}
.match-with-sec .block-title{
        font-family: 'Open Sans Condensed', sans-serif !important;
}
.match-with-sec .product-items li {
    margin: 5px 0 0;
    padding: 10px 0 0;
}
.product-info-stock-sku {
    color: black;
    font-size: 11px;
    font-weight: 600;
}
button[disabled], html input[disabled] {
    cursor: default;
    opacity: 0.65;
}
 .loading-modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url("{{url('/')}}/storage/app/public/pride-loading-gif_new.gif") 
                50% 50% 
                no-repeat;
}
</style>
<!-- Magic Zoom Plus Magento 2 module version v1.5.20 [v1.6.64:v5.2.4] -->
<link type="text/css" href="{{asset('assets/MagicZoomPlus/magiczoomplus.css')}}" rel="stylesheet" media="screen" />
<link type="text/css" href="{{asset('assets/MagicZoomPlus/magiczoomplus.module.css')}}" rel="stylesheet" media="screen" />
<script type="text/javascript" src="{{asset('assets/MagicZoomPlus/magiczoomplus.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/MagicZoomPlus/magictoolbox.utils.js')}}"></script>
<script type="text/javascript">
var mzOptions = {
    'zoomWidth': 'auto',
    'zoomHeight': 'auto',
    'zoomPosition': 'right',
    'zoomDistance': 15,
    'selectorTrigger': 'click',
    'transitionEffect': true,
    'lazyZoom': false,
    'rightClick': true,
    'zoomMode': 'zoom',
    'zoomOn': 'hover',
    'upscale': true,
    'smoothing': true,
    'variableZoom': false,
    'zoomCaption': 'off',
    'expand': 'window',
    'expandZoomMode': 'zoom',
    'expandZoomOn': 'click',
    'expandCaption': true,
    'closeOnClickOutside': true,
    'cssClass': '',
    'hint': 'once',
    'textHoverZoomHint': 'Hover to zoom',
    'textClickZoomHint': 'Click to zoom',
    'textExpandHint': 'Click to expand',
    'textBtnClose': 'Close',
    'textBtnNext': 'Next',
    'textBtnPrev': 'Previous'
}
</script>
<script type="text/javascript">
    var mzMobileOptions = {
        'zoomMode': 'zoom',
        'textHoverZoomHint': 'Touch to zoom',
        'textClickZoomHint': 'Double tap to zoom',
        'textExpandHint': 'Tap to expand'
    }
</script>

<main id="maincontent" class="page-main">
    <div class="loading-modal"></div>
    <div id="cart-modal-content"></div>
    <div v-if="loading_image" id="loading-image">
        <img src="{{ url('/') }}/storage/app/public/pride-loading-gif_new.gif" Alt="Loading..." />
    </div>
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item">
                                <a href="{{url($main_link)}}" title="Main category"><?php echo $main_cate; ?></a>
                            </li>
                            <li class="item">
                                <a href="{{url($sub_link)}}" title="Sub Category"><?php echo $category_name; ?></a>
                            </li>
                            <li class="item">
                                <a href="{{url($last_link)}}" title="Child sub category"><?php echo $subcategory_name; ?></a>
                            </li>
                            <li class="item">
                                <a href="<?php echo url()->current();?>" title=""><?php echo $title; ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-detail">
                    <div class="row quickview-tpl">
                        <div class="col-sm-7  col-lg-5">
                            <div class="gallery-detail">
                                <a id="gallery-prev-area" tabindex="-1"></a>
                                <div class="action-skip-wrapper"><a class="action skip gallery-next-area" href="#gallery-next-area"><span>Skip to the end of the images gallery</span></a>
                                </div><div class="action-skip-wrapper"><a class="action skip gallery-prev-area" href="#gallery-prev-area"><span>Skip to the beginning of the images gallery</span></a>
                                </div><a id="gallery-next-area" tabindex="-1"></a>
                                <div class="MagicToolboxContainer selectorsRight minWidth" data-mage-init='{"magicToolboxThumbSwitcher": {"playIfBase":0,"showRelated":0,"videoAutoRestart":0,"tool":"magiczoomplus","switchMethod":"click","productId":"90363"}}'>
                                    <div class="MagicToolboxMainContainer">
                                        <div id="mtImageContainer" style="display: block;">
                                            <div>
                                                <?php
                                                $i = 0;
                                                foreach ($singleproductmultiplepic as $smplist) {
                                                    $i++;
                                                    if ($i == 1) {
                                                        ?>
                                                        <a id="MagicZoomPlusImage-product-90363"  class="MagicZoom" href="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}" title="{{$singleproduct->product_name}}" data-options="lazyZoom:true;">
                                                            <img itemprop="image" src="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}"   alt="{{$singleproduct->product_name}}" />
                                                        </a>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div id="mt360Container" style="display: none;"></div>
                                        <div id="mtVideoContainer" style="display: none;"></div>    
                                    </div>
                                    <div class="MagicToolboxSelectorsContainer" style="flex-basis: 88px; width: 88px;">
                                        <div id="MagicToolboxSelectors90363" class="">
                                            <!--                                            active-selector-->
                                            @foreach($singleproductmultiplepic as $smplist)	
                                            <a class="mt-thumb-switcher  " data-zoom-id="MagicZoomPlusImage-product-90363" href="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img}}"  data-image="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img}}" title="{{$singleproduct->product_name}}">
                                                <img src="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img_tiny}}"  alt="{{$singleproduct->product_name}}" />
                                            </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    if (window.matchMedia("(max-width: 767px)").matches) {
                                        $scroll = document.getElementById('MagicToolboxSelectors90363');
                                        if ($scroll && typeof $scroll != 'undefined') {
                                            $attr = $scroll.getAttribute('data-options');
                                            if ($attr !== null) {
                                                $scroll.setAttribute('data-options', $attr.replace(/orientation *\: *[a-zA-Z]{1,}/gm, 'orientation:horizontal'));
                                            }
                                        }
                                    }
                                </script>
                            </div>
                        </div>
                        <div class="col-sm-5 col-lg-4 product-shop">
                            <div class="product-info-main">
                                <div class="page-title-wrapper product">
                                    <div class="page-title-wrapper">
                                        <div>
                                            <h1 class="product-title">
                                    			<span>
                                    			  {{$singleproduct->product_name}}
                                    			</span>
                                            </h1>
                                        </div>
                                        <?php if ($category_name == 'Ready To Wear') { ?>
                                            <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_girl.png" class="brand-img" style="width:175px;height:auto;"/>
                                            </div>
                                        <?php } elseif ($subcategory_name == 'Classic Sari' || $subcategory_name == 'Classic Three Piece') { ?>
                                            <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_classic.png" class="brand-img" style="width:175px;height:auto;"/>
                                            </div>
                                        <?php } elseif ($category_name == 'Ethnic Meanswear') { ?>
                                            <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_menswear.png" class="brand-img" style="width:175px;height:auto;"/>
                                            </div>
                                        <?php }elseif($category_name =='Girls' || $category_name =='Boys'){ ?>
                                         <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_kids.png" class="brand-img" style="width:175px;height:auto;"/>
                                          </div>
                                         <?php }elseif($subcategory_name == 'Signature Three Piece'){ ?>
                                         <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                 <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_signature.png" class="brand-img" style="width:175px;height:auto;"/>
                                          </div>
                                         <?php } ?>
                                    </div>
                                </div>
                                <div class="product-info-price">
                                    <div class="product-info-stock-sku">
                                        <div class="sku-product">                
                                            <label>
                                                Product Code:
                                            </label>
                                            <span>{{$singleproduct->product_styleref}}</span>
                                        </div>
                                        <div class="product attribute sku">
                                             <div class="font-alt product_barcode" style="display:none;font-size:8pt;text-transform: none;padding-top:5px;"><span> Barcode : </span> <span id="barcode"></span></div>
                                        </div>
                                    </div>
                                     <div class="prices">
                            			<span class="price" itemprop="price">Tk {{$singleproduct->product_price}}</span>
                                        <input type="hidden" id="product_regular_price" name="product_regular_price" value="{{$singleproduct->product_price}}">
                                    </div>
                                    <div class="short-description">
                                        {{$singleproduct->product_description}}
                                    </div>
                                </div>
                                <div class="cartForm">
                                    <style>
                                        .product-info-main .sizechart { display : none; }
                                    </style>
                                    <div class="product-add-form">
                                        <form action="{{url('/cart/popup-add-to-cart')}}" method="POST" id="add-to-cart-quickview-form">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="productid" value="{{$singleproduct->id}}" />
                                            <input type="hidden" name="productcolor" id="selectcolor" value="{{$product_color}}">
                                            <input type="hidden" name="productimage" id="productImage" value="{{$cart_image->productimg_img_thm}}">
                                            <div class="swatch color  color" data-option-index="1" style="display: block">          
                                				<div class="header">
                                					Color <em>*</em>        
                                				</div>
                                					<?php
                                					      $product_name = str_replace(' ', '-', $singleproduct->product_name);
                                                          $product_url = strtolower($product_name);
                                                          $product_code = strtolower($singleproduct->product_styleref);
                                						  $i = 0;
                                						  foreach ($product_color_image as $color) {
                                						  $swatch_color_album=str_replace('/','-',$color->productalbum_name);
                                						  $swatch_color_album = strtolower($swatch_color_album);
                                						  $i++;
                                					?>
                                						<div data-value="$color->productalbum_name" class="swatch-element  color {{$color->productalbum_name}} available">
                                							<div class="tooltip">
                                								{{$color->productalbum_name}}
                                							</div>
                                							<input id="quickview-swatch-1-{{$color->productalbum_name}}" type="radio" name="option-{{$i}}" value="{{$color->productalbum_name}}" <?php if(strtolower($color->productalbum_name) == $product_color){ echo 'checked';}?>>
                                							<label class="swatch_variant_img" for="quickview-swatch-1-{{$color->productalbum_name}}">
                                								<a href='{{url("shop/{$product_url}/{$product_code}/{$swatch_color_album}")}}'>
                                								    <span class="bgImg" style="background-color: {{$color->productalbum_name}}; background-image: url({{ URL::to('') }}/storage/app/public/pgallery/{{$color->productalbum_img}});"></span>
                                								</a>
                                								<span class="crossed-out"> </span>
                                							</label>
                                						</div>
                                					<?php } ?>
                                			</div>     
                                            <div class="swatch size  size" data-option-index="0" style="display: block">          
                                                <div class="header">
                                                    Size <em>*</em>        
                                                </div>
                                            	@php($i = 0)
                                                    @foreach($product_sizes as $size)
                                                    <div data-value="{{$size->productsize_size}}" class="swatch-element swatch-element-size  size {{$size->productsize_size}} <?php
                                            			if ($size->SizeWiseQty < 1)
                                            				echo 'soldout';
                                            			elseif ($i == 0)
                                            				echo 'available';
                                            			?>">
                                                        <input id="quickview-swatch-0-{{$size->productsize_size}}" type="radio" name="option-0" value="{{$size->productsize_size}}" <?php if($i == 0){ echo 'checked';}?>>            
                                                        <label for="quickview-swatch-0-{{$size->productsize_size}}">
                                                            {{$size->productsize_size}}
                                                            <span class="crossed-out"></span>
                                                        </label>
                                                    </div>
                                            		@php($i++)
                                                    @endforeach 
                                                    <input id="product-size-input" class="swatch-input super-attribute-select" name="productsize" type="hidden" value="{{$product_sizes[0]->productsize_size}}" data-selector="productsize" data-validate="{required: true}" aria-required="true" aria-invalid="false" data-attr-name="size">
                                            </div>
                                            <div class="box-tocart">
                                                <div class="fieldset">
                                                    <div class="quantity">
                                                        <label for="quantity">
                                                            Quantity:
                                                        </label>
                                                        <div class="qty-group">
                                                            <a href="#" data-qv-minus-qtt="" class="minus button"></a>
                                                            <input type="text" id="qty" data-qv-qtt-id="quantity" name="quantity" value="1">
                                                            <a href="#" data-qv-plus-qtt="" class="plus button"></a>
                                                        </div>
                                                    </div>
                                                    <div class="total-price">
                                                        <label>
                                                            Subtotal:
                                                        </label>
                                                        <span class="total-money">Tk {{$singleproduct->product_price}}</span>
                                                    </div> 
                                                    <div style="color:red;font-weight:700;padding-bottom:10px;" id="total_sold_out_msg"></div>
                                                     <div style="color:red;font-weight:700;padding-bottom:10px;" id="sold_out_msg"></div>
                                                    <div class="actions">
                                                        <button onClick="startLoading()" id="product-add-to-cart" type="submit"
                                                                title="Add to Cart"
                                                                class="action tocart">
                                                            <span class="pride.add_to_cart" id="add_to_cart_text">Add to Cart</span>
                                                        </button>
                                                        <div id="instant-purchase" data-bind="scope:'instant-purchase'">
                                                        </div>
                                                        <div class="size-popup">
                                                            <div class="popup">
                                                                <div class="video-area">
                                                                    <div class="video-holder">
                                                                        <div class="video-frame">
                                                                            <div class="size-chart-tabel"><h1>CLOTHING SIZE CHART GIRLS</h1> 
                                                                                <table width="100%" border="1"> 
                                                                                    <tr class="even"> 
                                                                                        <th scope="col">GIRLS</th> 
                                                                                        <th scope="col">MEASURE YOUR</th> 
                                                                                        <th scope="col">XS</th> 
                                                                                        <th scope="col">S</th> 
                                                                                        <th scope="col">M</th> 
                                                                                        <th scope="col">L</th> 
                                                                                        <th scope="col">XL</th> 
                                                                                        <th scope="col">XXL</th> 
                                                                                        <th scope="col">XXXL</th> 
                                                                                    </tr> 
                                                                                    <tr > 
                                                                                        <th scope="row">REGULAR FIT TUNIC</th> 
                                                                                        <th scope="row">CHEST</th> 
                                                                                        <td></td> 
                                                                                        <td>38"</td> 
                                                                                        <td>39.5"</td> 
                                                                                        <td>41"</td> 
                                                                                        <td>42.5"</td> 
                                                                                        <td>44"</td> 
                                                                                        <td>45.5"</td>  
                                                                                    </tr> 
                                                                                    <tr class="even"> 
                                                                                        <th scope="row">TAILORED FIT TUNIC</th> 
                                                                                        <th scope="row">CHEST</th> 
                                                                                        <td></td> 
                                                                                        <td>36"</td> 
                                                                                        <td>37.5"</td> 
                                                                                        <td>39"</td> 
                                                                                        <td>40.5"</td> 
                                                                                        <td>42"</td> 
                                                                                        <td>43.5"</td> 
                                                                                    </tr> 
                                                                                    <tr > 
                                                                                        <th scope="row">PANT</th>
                                                                                        <th scope="row">WAIST</th> 
                                                                                        <td>26"</td> 
                                                                                        <td>28"</td> 
                                                                                        <td>30"</td> 
                                                                                        <td>32"</td> 
                                                                                        <td>34"</td> 
                                                                                        <td>36"</td> 
                                                                                        <td>38"</td> 
                                                                                    </tr> 
                                                                                    <tr class="even"> 
                                                                                        <th scope="row">LEGGINGS</th> 
                                                                                        <th scope="row">WAIST</th> 
                                                                                        <td></td> 
                                                                                        <td>26"</td> 
                                                                                        <td>27"</td> 
                                                                                        <td>28"</td> 
                                                                                        <td>29"</td> 
                                                                                        <td>33"</td> 
                                                                                        <td>31"</td> 
                                                                                    </tr> 
                                                                                </table> 
                                                                                <span class="note">*Measurements in inches</span> 
                                                                            </div>                                        
                                                                            <a class="close icon-delete-outline" href="#"><span>Close</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <a class="sizechart open" href="#popup1">size chart</a>
                                                        </div>

                                                    </div>
                                                    <div id="customers_view_qv">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                        <label id="customers_view">5</label> 
                                            			<span>
                                            			  customers are viewing this product
                                            			</span>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                               <div id="find-store-section" style="position:relative; min-height:50px;">
									<a class="simple button" href="#" title="{{$singleproduct->product_name}}">
										<span class="sprite-left store-locater sprite-left"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
										Find In Store
									</a>
									<span class="find_store_close">X</span>
								</div> 
                                <div class="searchstore">
                                    <div id="preferred-store-panel">
										<div class="dropdown">
											<button id="select-district" class="btn btn-primary dropdown-toggle"  style="width:100%; background-color:#291e88; border-color:#291e88" type="button" data-toggle="dropdown">Select District <span class="caret"></span></button>
											<ul class="dropdown-menu" style="width:100%" id="find_in_city">
											<!--	<li style="cursor:pointer" onClick="getStore(this.textContent || this.innerText)" class="list-group-item"></li> -->
											</ul>
										</div>
                                        <span class="stockmsg">Please note availability may change</span>
                                    </div>
                                    <div  class="searchresults">
                                        <div id="store-list-loading" style="text-align:center; padding-bottom:10px; display:none"><img style="width:auto; margin:auto" src="{{url('/')}}/storage/app/public/pride-loading-gif_new.gif" /></div>
                                        <div class="store-list-container" id="store-list-container">
                                            
                                        </div>
                                        <div class="store-list-pagination">
                                        </div></div>
                                </div>
                                <div class="product-shop">    
                                    <div class="product info detailed">
                                        <div class="content">
                                            <div class="product data items" data-mage-init='{"tabs":{"openedState":"active"}}'>
                                                <div class="data item title"
                                                     aria-labeledby="tab-label-additional-title"
                                                     data-role="collapsible" id="tab-label-additional">
                                                    <a class="data switch"
                                                       tabindex="-1"
                                                       data-toggle="switch"
                                                       href="#additional"
                                                       id="tab-label-additional-title">
                                                        Fabric Composition            
                                                    </a>
                                                </div> 
                                                <div class="data item content" id="product.info.overview" data-role="content">
                                                    <div class="product attribute overview">
                                                        <div style="font-size:14px" class="value" itemprop="description">{{ $singleproduct->fabric }}</div>
                                                    </div>
                                                </div>
                                                <div class="data item content" id="additional" data-role="content">
                                                    <div class="additional-attributes-wrapper table-wrapper">
                                                        <table class="data table additional-attributes" id="product-attribute-specs-table">
                                                            <!--<caption class="table-caption"></caption>-->
                                                            <tbody>
                                                                <tr>
                                                                    <th class="col label" style="font-size:16px" scope="row">Care</th>
                                                                    <td class="col data" data-th="Material">{{ $singleproduct->product_care }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social-networks">
                                    <strong class="label">share this product</strong>
                                    <ul class="social-icons">
                                        <?php $current_url=URL::current();?>
                                        <li><a class="tw icon-social-twitter" href="javascript:window.open('https://twitter.com/share?url={{$current_url}}&text={{$singleproduct->product_name}}', 'twitter', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Tweet"><span>Twitter</span></a></li> 
                                        <li><a class="fb icon-facebook"  href="javascript:window.open('https://www.facebook.com/dialog/share?app_id=258246005086458&display=popup&href={{$current_url}}&redirect_uri={{$current_url}}', 'facebook', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Share on Facebook"><span>facebook</span></a></li>
                                        <!-- li><a class="pn icon-pinterest-p" href="javascript:window.open('https://pinterest.com/pin/create/button/?url=&media=&description=MA18406', 'pinterest', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Pin it"><span>Pinterest</span></a></li -->
                                        <!--<li> <a class="wa fa fa-whatsapp" href="whatsapp://send?text=https://www.pride-limited.com/ma18406-beige-3pc.html" data-action="share/whatsapp/share"><span>Share via Whatsapp</span></a></li> 
                                        https://www.facebook.com/dialog/share?app_id=258246005086458&display=popup&href={{$singleproduct->id}}&redirect_uri=https://pride-limited.com/
                                        --->
                                    </ul>
                                </div>
                            </div>
                              <br>
                              <div class="row mt-70 hidden-sm hidden-md hidden-lg">
                                  <div class="col-sm-12">
                                    <ul class="nav nav-tabs font-change" role="tablist">
                                      <li class="active"><a href="#shipping" data-toggle="tab"><i class="fa fa-bicycle" aria-hidden="true"></i> Shipping</a></li>
                                      <li><a href="#exchange" data-toggle="tab"><i class="fa fa-exchange" aria-hidden="true"></i> Exchange Policy</a></li>
                                    </ul>
                                    <div class="tab-content">
                                      <div class="tab-pane active" id="shipping">
                                        <div class="panel-group" id="accordion">
                						<br>
                    					    <h4>* Order delivery is subject to availability of stock.</h4>
                    						<p class="text-14">We can ship to virtually any address in our country. Note that Currently, we do not ship outside Bangladesh.</p>
                    						<p  class="text-14">When you place an order, we will estimate shipping and delivery dates for you based on the availability of your items and the shipping options you choose. Depending on the shipping provider you choose, shipping date estimates may appear on the shipping quotes page.</p>
                    						<p  class="text-14">If any question Please <a href="{{url('/contact-us')}}">Click here</a> to contact our Customer Service Team.</p>
                                            <!--<div class="panel panel-default">
                                              <div class="panel-heading">
                                                <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#support1">What Kinds Of Shipping Options Do You Offer?</a></h4>
                                              </div>
                                              <div class="panel-collapse collapse in" id="support1">
                                                <div class="panel-body">
                    							    We provide Card and bKash payment to Dhaka customers. For customers outside Dhaka, prepayment must be paid via bKash or card and then we ship the product via our trusted partners.
                                                    Free shipping is available for purchases worth TK 3000 and above.
                    							</div>
                                              </div>
                                            </div> --->
                                          </div>
                                      </div>
                                      <div class="tab-pane" id="exchange">
                					  <br>
                                        <p  class="text-14">Customers have to pay the bill amount in advance through Credit Card/Bkash/Bank Deposit for any orders.<br>Pride Limited does not refund any products. However, we do exchange products if:</p>
                                        <p  class="text-14">The exchange must be made within 15 days of purchase and the product, product package and the hangtag must be intact. The original invoice must be presented during exchange. Call our customer service to exchange the product. You can return your items through courier service to our e-commerce warehouse at the following address and then we will ship the right sized/colored product to you.</p>
                    					<p  class="text-14"><br>Please <a href="{{url('/contact-us')}}">Click here </a> to contact our Customer Service Team.</p>
                    				  </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-3">
                                <div class="match-with-sec">
                                <div class="block related" data-mage-init='{"relatedProducts":{"relatedCheckbox":".related.checkbox"}}' data-limit="3" data-shuffle="1">
                                    <div class="block-title title">
                                        <strong id="block-related-heading" role="heading" aria-level="2">Top Picks</strong>
                                    </div>
                                    <div class="block-content content" aria-labelledby="block-related-heading">
                                        <div class="products wrapper grid products-grid products-related">
                                            <ol class="products list items product-items">
                                                <?php
                                                foreach ($top_pick as $top) {
                                                $product_name = str_replace(' ', '-', $top->product_name);
                                                $product_url = strtolower($product_name);
                                                $product_code = strtolower($top->product_styleref);
                                                $top_color_album=str_replace('/','-',$top->productalbum_name);
                                                $top_color_album = strtolower($top_color_album);
                                                    ?>
                                                    <li class="item product product-item">                                
                                                        <div class="product-item-info related-available">
                                                            <!-- related_products_list-->   
                                                            <?php if($top->subcat==22){?>
                                                            <a href='{{url("shop/{$product_url}/color-{$top_color_album}/{$top->id}")}}' class="product photo product-item-photo">
                                                            <?php }else{ ?>
                                                            <a href='{{url("shop/{$product_url}/{$product_code}/{$top_color_album}")}}' class="product photo product-item-photo">
                                                            <?php } ?>
                                                                <span class="product-image-container">
                                                                    <span class="product-image-wrapper">
                                                                        <img class="product-image-photo"
                                                                             src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $top->product_img_thm; ?>" alt=" {{$top->product_name}}"/></span>
                                                                </span>
                                                            </a>
                                                            <div class="product details product-item-details">
                                                                <strong class="product name product-item-name">
                                                                    <?php if($top->subcat==22){?>
                                                                    <a href='{{url("shop/{$product_url}/color-{$top_color_album}/{$top->id}")}}' class="product-item-link"><strong>{{$top->product_name}}</strong></a>
                                                                    <?php }else{ ?>
                                                                    <a href='{{url("shop/{$product_url}/{$product_code}/{$top_color_album}")}}' class="product-item-link"><strong>{{$top->product_name}}</strong></a>
                                                                    <?php } ?>
                                                                </strong>
                                                                <div class="price-box price-final_price" data-role="priceBox" data-product-id="{{$top->id}}" data-price-box="product-id-{{$top->id}}">
                                                                    <span class="price-container price-final_price tax weee">
                                                                        <span  id="product-price-{{$top->product_price}}" data-price-amount="{{$top->product_price}}" data-price-type="finalPrice" class="price-wrapper ">
                                                                            <span class="price">Tk {{$top->product_price}} </span>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                <div class="field choice related">
                                                                    <input type="checkbox" class="checkbox related" id="related-checkbox75234" name="related_products[]" value="{{$top->id}}" />
                                                                    <label class="label" for="related-checkbox75234"><span>Shop Now</span></label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ol>
                                        </div>
                                       <!-- <a class="btn-next" href="javascript:void(0);">next</a>
                                        <a class="btn-prev" href="javascript:void(0);">prev</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-70 hidden-xs">
                  <div class="col-sm-12">
                    <ul class="nav nav-tabs font-change" role="tablist">
                      <li class="active"><a href="#shipping_down" data-toggle="tab"><i class="fa fa-bicycle" aria-hidden="true"></i> Shipping</a></li>
                      <li><a href="#exchange_down" data-toggle="tab"><i class="fa fa-exchange" aria-hidden="true"></i> Exchange Policy</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="shipping_down">
                        <div class="panel-group" id="accordion">
						<br>
    					    <h4>* Order delivery is subject to availability of stock.</h4>
    						<p class="text-14">We can ship to virtually any address in our country. Note that Currently, we do not ship outside Bangladesh.</p>
    						<p  class="text-14">When you place an order, we will estimate shipping and delivery dates for you based on the availability of your items and the shipping options you choose. Depending on the shipping provider you choose, shipping date estimates may appear on the shipping quotes page.</p>
    						<p  class="text-14">If any question Please <a href="{{url('/contact-us')}}">Click here</a> to contact our Customer Service Team.</p>
                            <!--<div class="panel panel-default">
                              <div class="panel-heading">
                                <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#support1">What Kinds Of Shipping Options Do You Offer?</a></h4>
                              </div>
                              <div class="panel-collapse collapse in" id="support1">
                                <div class="panel-body">
    							    We provide Card and bKash payment to Dhaka customers. For customers outside Dhaka, prepayment must be paid via bKash or card and then we ship the product via our trusted partners.
                                    Free shipping is available for purchases worth TK 3000 and above.
    							</div>
                              </div>
                            </div> --->
                          </div>
                      </div>
                      <div class="tab-pane" id="exchange_down">
					  <br>
                        <p  class="text-14">Customers have to pay the bill amount in advance through Credit Card/Bkash/Bank Deposit for any orders.<br>Pride Limited does not refund any products. However, we do exchange products if:</p>
                        <p  class="text-14">The exchange must be made within 15 days of purchase and the product, product package and the hangtag must be intact. The original invoice must be presented during exchange. Call our customer service to exchange the product. You can return your items through courier service to our e-commerce warehouse at the following address and then we will ship the right sized/colored product to you.</p>
    					<p  class="text-14"><br>Please <a href="{{url('/contact-us')}}">Click here </a> to contact our Customer Service Team.</p>
    				  </div>
                    </div>
                  </div>
                </div>
                <div class="block upsell" data-mage-init='{"upsellProducts":{}}' data-limit="4" data-shuffle="1">
                    <div class="block-title title" style="margin: 0 0 20px !important;">
                        <strong id="block-upsell-heading" role="heading" aria-level="2">You may also like</strong>
                    </div>
                    <div class="block-content content" aria-labelledby="block-upsell-heading">
                        <div class="products wrapper grid products-grid products-upsell">
                            <ol class="products list items product-items">
                                <?php
                                foreach ($may_like as $product) {
                                $product_name = str_replace(' ', '-', $product->product_name);
                                $product_url = strtolower($product_name);
                                $product_code = strtolower($product->product_styleref);
                                $color_album = str_replace('/', '-', $product->productalbum_name);
                                $color_album = strtolower($color_album);
                                    ?>
                                    <li class="item product product-item">                                
                                        <div class="product-item-info ">
                                            <!-- upsell_products_list--> 
                                            <?php if($product->subcat==22){?>
                                            <a href='{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}' class="product photo product-item-photo">
                                            <?php }else{ ?>
                                            <a href='{{url("shop/{$product_url}/{$product_code}/{$color_album}")}}' class="product photo product-item-photo">
                                            <?php } ?>
                                                 <img src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->product_img_thm; ?>"  alt="{{$product->product_name}}" class="img-responsive product-image-photo img-thumbnail  lazy" data-src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->product_img_thm; ?>"> 
                                            </a>
                                            <div class="product details product-item-details">
                                                <strong class="product name product-item-name">
                                                    <?php if($product->subcat==22){?>
                                                    <a href='{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}' class="product-item-link">{{$product->product_name}}</a>
                                                    <?php }else{ ?>
                                                    <a href='{{url("shop/{$product_url}/{$product_code}/{$color_album}")}}' class="product-item-link">{{$product->product_name}}</a>
                                                    <?php } ?>
                                                </strong>
                                                <div class="price-box price-final_price" data-role="priceBox" data-product-id="{{$product->id}}" data-price-box="product-id-{{$product->id}}">
                                                    <span class="price-container price-final_price tax weee">
                                                        <span  id="product-price-{{$product->product_price}}" data-price-amount="{{$product->product_price}}" data-price-type="finalPrice" class="price-wrapper ">
                                                            <span class="price">Tk {{$product->product_price}} </span>    
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ol>
                        </div>
                        <a class="btn-next" href="javascript:void(0);"><img style="width:34px" src="{{url('/')}}/storage/app/public/icon_next.svg"/>Next</a>
                        <a class="btn-prev" href="javascript:void(0);"> <img style="width:34px" src="{{url('/')}}/storage/app/public/icon_prev.svg"/>prev</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- Modal -->
<script>
    
</script>
<script>
    jQuery(document).ready(function ($) {
        $('.swatch-option').click(function () {
            $('#product-size-text').text($(this).text());
            $('#product-size-input').val($(this).text());
            $('.swatch-option').removeClass("active");
            $(this).addClass("active");
        });

        // Hide the Modal
        @if (Session::has('returned'))
        $("#proceed-modal").modal("show");
        @endif
        var base_url = "{{ URL::to('') }}";
        var product_name = '<?php echo $product_url; ?>';
        var product_id = '<?php echo $singleproduct->id; ?>';
        var color_name = '<?php echo $product_color; ?>';
    });
</script>
<script>
    jQuery(document).ready(function ($) {
       // FindStore();
        $('.find_store_close').hide();
        var product_id = "{{$singleproduct->id}}";
        var get_color_name = "{{$product_color}}";
        var product_styleref = "{{$singleproduct->product_styleref}}";
        var color_name = get_color_name.replace("/", "-");
        //find in city list
        var url_op = base_url + "/find-get-city/" + product_styleref;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data)
                {
                 if(data ==''){
                     $('#find_in_city').append("<li style='cursor:pointer;color:red;'  class='list-group-item'>No data Found</li>");
                 }else{
                    $.each(data, function (index, citobj) {
                      $('#find_in_city').append("<li style='cursor:pointer' onClick='getStore(this.textContent || this.innerText)' class='list-group-item'>" + citobj.district + "</li>");
                    });
                   }
                }
            });
     //   alert(color_name);
		var productSize = $('#product-size-input').val();
		getProductBarcode(product_id, color_name, productSize);
		$('.swatch-element-size').click(function () {
		     //alert($(this).data('value'));
            $('#product-size-input').val($(this).data('value'));
            
        });
        getQtyByColorSize(product_id, color_name, productSize);
        $('#pqty').on('change', function (e) {
            var pqty = e.target.value;
            $('#productQty').val(pqty);
        });
        $('.swatch-element-size').on('click', function (e) {
			var productSize = $('#product-size-input').val();
			$(".loading-modal").show();
			getProductBarcode(product_id, color_name, productSize);
            getQtyByColorSize(product_id, color_name, productSize);
        });
        $('#colorName').on('change', function (e) {
            var productColor = e.target.value;
            $('#productColor').val(productColor);
        });
        function  getQtyByColorSize(product_id, color_name, productSize) {
            var url_op = base_url + "/ajaxcall-getQuantityByColor/" + product_id + '/' + productSize + '/' + color_name;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (stock) {
                   //  alert(html);
                   $(".loading-modal").hide();
                    var qty = stock;
                  //  alert(qty);
                    if (qty > 0) {
                        $('#qty').attr({"max": qty});
						$("#sold_out_msg").html(" ");
						$(".product-inventory").show();
						$("#stock-msg").html("<span style='color:green;'>In stock</span>");
						$('#qty').val(1);
						$('#product-add-to-cart').html("Add to Cart");
                        document.getElementById("product-add-to-cart").disabled = false;
                    } else {
						$('#qty').val(0);
						$('#qty').attr({"max": 0});
					   if(productSize =='Free Size'){
						      $("#sold_out_msg").html("<span style='color:red;font-weight:700;padding-bottom:10px;'>This color has sold out. Please select another color.</span>");
						}else if(productSize =='NA'){
						     $("#sold_out_msg").html("<span style='color:red;font-weight:700;padding-bottom:10px;'>This color has sold out. Please select another color.</span>");
						}else{
						    $("#sold_out_msg").html("<span style='color:red;font-weight:700;padding-bottom:10px;'>" + productSize + " size has sold out. Please select another size.</span>");
						}
						$(".product-inventory").show();
						$("#stock-msg").html("<span style='color:red;'>Out of stock</span>");
						$('#product-add-to-cart').html("Out of stock");
						document.getElementById("product-add-to-cart").disabled = true;
					}
                }
            });
        }
		
		function getProductBarcode(product_id, color_name, productSize)
        {
            var url_op = base_url + "/ajaxcall-getBarcode/" + product_id + '/' + productSize + '/' + color_name;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (html) {
                if(html !=''){
                       $(".product_barcode").show();
                       $("#barcode").html(html);
                    }
                }
            });
        }
        
		$('.simple').on('click', function (e) {
            //alert('ok');
            $('.searchstore').css('display', 'inline-block');
            $('.find_store_close').show();
			return false;
        });
        $(".store-details").mouseenter(function () {
                $(this).children(".stockmessage").removeClass('hide');
            });
            $(".store-details").mouseleave(function () {
               $(this).children(".stockmessage").addClass('hide');
        });
        $(".find_store_close").on('click',function(){
            $('.searchstore').css('display', 'none');
            $('.find_store_close').hide();
        });
        
        function getStore(text) {
		var barcode = document.getElementById('barcode').textContent;
		document.getElementById('store-list-loading').style.display = "block";
		
		$.ajax({url: base_url+"/store-list/"+text+"/"+barcode, success: function(html){
			if(text==null || text=="") {
				alert('Error');
			} else {
			    document.getElementById('store-list-loading').style.display = "none";
				document.getElementById('store-list-container').innerHTML = html;
				
				$('.simple').on('click', function (e) {
            
            $('.searchstore').css('display', 'inline-block');
            $('.find_store_close').show();
            e.preventDefault();
            });
            $(".store-details").mouseenter(function () {
                    $(this).children(".stockmessage").removeClass('hide');
                });
                $(".store-details").mouseleave(function () {
                   $(this).children(".stockmessage").addClass('hide');
            });
            $(".find_store_close").on('click',function(){
                $('.searchstore').css('display', 'none');
                $('.find_store_close').hide();
            });
			}
		}});
		document.getElementById('select-district').innerHTML = text + ' <span class="caret"></span>';
		
	   }
	   $("#product-add-to-cart").click(function(e){
	     e.preventDefault();
	     $(".loading-modal").show();
	     var datastring = $("#add-to-cart-quickview-form").serialize();
	     //alert(datastring);
	     var cart_url = base_url + "/cart/popup-add-to-cart";
	     $.ajaxSetup({
          headers: {
            'X-CSSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
	     $.ajax({
	         url:cart_url,
	         type:'POST',
	         data:datastring,
	         success:function(result){
	        // alert(result);
	           $(".loading-modal").hide();
	           $("#cart-modal-content").html(result).fadeIn(800).delay(3000).fadeOut(1500);  
				  $('.close-modal').on('click', function (e) {
					  $("#cart-modal-content").hide();
				  });
				  $('.continue-shopping').on('click', function (e) {
					  $("#cart-modal-content").hide();
				  });
				  $("#btn_checkout_pop").on('click',function(){
                		window.location.href = base_url+"/checkout";
                });
	           var cart_count = base_url + "/cart/item-count";
			   $.ajax({
    				url:cart_count,
    				type:'GET',
    				success:function(result){
    				    if(result==0){
    					   $(".counter").hide();  
    					   //$(".g-stickycart-count").hide(); 
    					}else{
    					   $(".counter").show();  
    					   $(".counter").html(result); 
    					   //$(".g-stickycart-count").html(result); 
    					}
    				}
			   });
			  var cart_reload = base_url + "/cart/reload";
			   $.ajax({
    				url:cart_reload,
    				type:'GET',
    				success:function(result){
    				    $('.block-minicart').empty();
				        $('.block-minicart').append(result);
                        $("#btn-minicart-close").on("click", function () {
                            $(".block-minicart").removeClass("block-minicart-open");
                        });
                        $("#top-cart-btn-checkout").click(function(){
                             window.location=base_url+ "/checkout";
                        });
                        $("#top-cart-btn-cart").click(function(){
                             window.location=base_url+ "/shop-cart";
                        });
    				}
			   });
	             
	         }
	     });
	 });
	   var output = document.getElementById('customers_view');
        setInterval(function ()
        {
    		var x = Math.floor((Math.random() * 50) + 1);
            output.innerHTML = x;
        }, 3500);
        
        function updatePricingQuickview() {
          var quantity = parseInt($('[data-qv-qtt-id]').val());
          var p = $('.quickview-tpl #product_regular_price').val();
          var totalPrice1 = p * quantity;
          $('.quickview-tpl .total-price span').html('Tk '+totalPrice1);
    
        };
                                                            
        $('[data-qv-qtt-id]').on('change', updatePricingQuickview);
        
        var buttonSlt = '[data-qv-minus-qtt], [data-qv-plus-qtt]',
            buttonElm = $(buttonSlt);
    
        $(document).off('click.changeQttQv', buttonSlt).on('click.changeQttQv', buttonSlt, function(e) {
            e.preventDefault();
            e.stopPropagation();
    
            var self = $(this),
                input = self.siblings('input[name="quantity"]'),
                oldVal = parseInt(input.val()),
                newVal = 1;
    
            switch (true) {
                case (self.hasClass('plus')): {
                    newVal = oldVal + 1;
                    break;
                }
                case (self.hasClass('minus') && oldVal > 1): {
                    newVal = oldVal - 1;
                    break;
                }            
            }
    
            input.val(newVal);
            updatePricingQuickview();
        });          
    });
</script>
<script>
var timer1;
var timer2;
var counter=1;
var running = false;
(function() {
	
})();
function updateActive(items, i) {
	i = i%(items.length);
	items[i>0?i-1:items.length-1].classList.remove('active');
	items[i].classList.add('active');
	counter++;
	if(!running) {
    	timer2 = setInterval(function() {
    		updateActive(items, counter);
    	}, 1500);
    	running = true;
    	clearInterval(timer1);
	}
}
function fadeImages(element) {
	var items = element.getElementsByClassName('item');
	for (var i = 0; i < items.length; i++) {
		items[i].classList.remove('active');
	}
	items[1].classList.add('active');
	timer1 = setInterval(function() {
		updateActive(items, counter);
	}, 100);
}
function removeTimer(element) {
    clearInterval(timer1);
	clearInterval(timer2);
	counter=1;
	var items = element.getElementsByClassName('item');
	for (var i = 0; i < items.length; i++) {
		items[i].classList.remove('active');
	}
	items[0].classList.add('active');
	running = false;
}

/* Open filter category */
(function(){
 //   document.getElementById('filter-category').classList.add('active');
})();
</script>
<script>
	function getStore(text) {
		var barcode = document.getElementById('barcode').textContent;
		var get_style_name = "{{$singleproduct->product_styleref}}";
        var stylecode = get_style_name.replace("/", "_");
        
		document.getElementById('store-list-loading').style.display = "block";
		var no_data = document.getElementById('no-data');
		if(no_data) {
		    no_data.style.display = "none";
		}
		
		var address = '';
		
		if(barcode) {
		    address = base_url+"/store-list/"+text+"/"+barcode;
		} else {
		    address = base_url+"/showroom-wise-stock-by-designref/"+text+"/"+stylecode;
		}
		
		jQuery.ajax({url: address, success: function(html){
			if(text==null || text=="") {
				alert('Error');
			} else {
			    document.getElementById('store-list-loading').style.display = "none";
				document.getElementById('store-list-container').innerHTML = html;
				
				jQuery('.simple').on('click', function (e) {
            
            jQuery('.searchstore').css('display', 'inline-block');
            jQuery('.find_store_close').show();
            e.preventDefault();
            });
            jQuery(".store-details").mouseenter(function () {
                    jQuery(this).children(".stockmessage").removeClass('hide');
                });
                jQuery(".store-details").mouseleave(function () {
                   jQuery(this).children(".stockmessage").addClass('hide');
            });
            jQuery(".find_store_close").on('click',function(){
                jQuery('.searchstore').css('display', 'none');
                jQuery('.find_store_close').hide();
            });
			}
		}});
		document.getElementById('select-district').innerHTML = text + ' <span class="caret"></span>';
		
	   }
</script>
<script>
    (function(){
        document.getElementById("loading-image").outerHTML='<div id="loading-image-removed"></div>';
    })();
    
    function startLoading(){
      //  var amount=document.getElementById("qty").getAttribute("max");
      //  if(amount - document.getElementById("qty").value >= 0)
        //    document.getElementById("loading-image-removed").outerHTML='<div id="loading-image"><img src="https://pride-limited.com/storage/app/public/loader.gif" Alt="Loading..." /></div>';
    }
</script>
@endsection