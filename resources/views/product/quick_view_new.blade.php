<link rel="stylesheet" type="text/css" href="{{asset('assets/css/quick_view.css')}}?3">

<div class="ajax-success-modal halo-modal ajax-quickview" data-quickview-modal="" style="display: block;">
    <div class="modal-overlay">
        <div class="halo-modal-content">
            <a class="close close-modal" title="Close">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24px" height="24px" class="icon-close">
					<path style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;isolation:auto;mix-blend-mode:normal" d="M 4.7070312 3.2929688 L 3.2929688 4.7070312 L 10.585938 12 L 3.2929688 19.292969 L 4.7070312 20.707031 L 12 13.414062 L 19.292969 20.707031 L 20.707031 19.292969 L 13.414062 12 L 20.707031 4.7070312 L 19.292969 3.2929688 L 12 10.585938 L 4.7070312 3.2929688 z" font-weight="400" font-family="sans-serif" white-space="normal" overflow="visible"></path>
				</svg>
							
		 </a>
            
<div class="halo-modal-body">
<div class="loading-modal"></div>
<div id="cart-modal-content"></div>
<div class="row quickview-tpl" data-collections-related="/collections/?view=related">
    <div class="col-md-6 product-photos" data-more-view-product-qv="">
        <div class="product-img-box">
            <div style="position: relative;" class="wrapper-images">
                <div class="product-photo-container slider-for quickview-featured-image slick-initialized slick-slider">
                        <div aria-live="polite" class="slick-list draggable">
							<div class="slick-track" role="listbox" style="opacity: 1; width: 1684px;">
							<?php foreach($singleproductmultiplepic as $smplist){ ?>
								<div class="thumb filter-power-wave-molle-blue-sneaker-for-men slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide90" style="width: 421px; position: relative; left: 0px; top: 0px; z-index: 999; opacity: 1;">
									<a data-zoom="" class="fancybox" rel="gallery1" href="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}" data-fancybox="images" tabindex="0" style="position: relative; overflow: hidden;">
										<img id="qv-product-featured-image-" src="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}" alt="{{$singleproduct->product_name}}" class="lazyautosizes ls-is-cached lazyloaded" data-src="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}" data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" data-aspectratio="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}" data-sizes="auto" sizes="421px">
									<img role="presentation" alt="" src="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}" class="zoomImg" style="position: absolute; top: 0px; left: 0px; opacity: 0; width: 1024px; height: 1024px; border: none; max-width: none; max-height: none;"></a>
								</div>
							<?php } ?>
							</div>
						</div>
                </div>
                
            </div>

            <div class="click-to-zoom">
                <span class="text">Click image to zoom in</span>
            </div>
            
      <!--      <div class="slider-nav slick-initialized slick-slider" data-rows="4" data-vertical="false">-->
      <!--            <div aria-live="polite" class="slick-list draggable">-->
					 <!-- <div class="slick-track" role="listbox" style="opacity: 1; width: 352px; transform: translate3d(0px, 0px, 0px);">-->
					 <!-- <?php foreach($singleproductmultiplepic as $smplist){ ?>-->
						<!--  <div class="item filter-power-wave-molle-blue-sneaker-for-men slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide100" style="width: 78px;">-->
						<!--	<div class="product-single__media" data-media-id="7616384532563">-->
						<!--	  <a href="javascript:void(0)" data-image="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}" data-zoom-image="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}" tabindex="0">-->
						<!--		<img src="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img_tiny}}" alt="{{$singleproduct->product_name}}">-->
						<!--	  </a>-->
						<!--	</div>-->
						<!--  </div>-->
					 <!-- <?php } ?>-->
						 
					 <!-- </div>-->
				  <!--</div>-->
      <!--      </div>-->
        </div>    
    </div>
    <div class="col-md-6 product-shop">
        <h1 class="product-title">
			<span>
			  {{$singleproduct->product_name}}
			</span>
        </h1>
        
        <div class="product-infor">
            <div class="sku-product">                
                <label>
                    Product Code:
                </label>
                <span>{{$singleproduct->product_styleref}}</span>
            </div>
            <div class="product-inventory" style="display:none;">                
                <label>
                    Availability:
                </label>
                <span id="stock-msg"></span>
            </div>
                            
        </div>
        <div class="countdown-item">
        </div>
        <div class="prices">
            <!--<span class="compare-price">Tk 2,799.00</span> -->
            <!--<span class="price on-sale" itemprop="price">Tk 1,959.30</span> -->
			<span class="price" itemprop="price">Tk {{$singleproduct->product_price}}</span>
            <input type="hidden" id="product_regular_price" name="product_regular_price" value="{{$singleproduct->product_price}}">
        </div>
        <!--<div class="label-sale-bg"><p>-30%</p></div> --->
        <div class="short-description">
            {{$singleproduct->product_description}}
        </div>
        <form action="{{url('/cart/popup-add-to-cart')}}" method="POST" class="shopify-product-form" id="add-to-cart-quickview-form">
            {{ csrf_field() }}
            <input type="hidden" name="productid" id="productid" value="{{$singleproduct->id}}" />
            <input type="hidden" name="productcolor" id="selectcolor" value="{{$product_color}}">
            <input type="hidden" name="productimage" id="productImage" value="{{$cart_image->productimg_img_thm}}">
            <div id="product-variants"> 
    <style>
        .swatch.size{
            display:block!important;
        }
        #add-to-cart-quickview-form .selector-wrapper.size{
            display: none !important;
        }
    </style>
    
<div class="swatch size  size" data-option-index="0" style="display: none">          
    <div class="header">
        Size <em>*</em>        
    </div>
	@php($i = 0)
        @foreach($product_sizes as $size)
        <div data-value="{{$size->productsize_size}}" class="swatch-element swatch-element-size  size {{$size->productsize_size}} <?php
			if ($size->SizeWiseQty < 1)
				echo 'soldout';
			elseif ($i == 0)
				echo 'available';
			?>">
            <input id="quickview-swatch-0-{{$size->productsize_size}}" type="radio" name="option-0" value="{{$size->productsize_size}}" <?php if($i == 0){ echo 'checked';}?>>            
            <label for="quickview-swatch-0-{{$size->productsize_size}}">
                {{$size->productsize_size}}
                <span class="crossed-out"></span>
            </label>
        </div>
		@php($i++)
        @endforeach 
        <input id="product-size-input" class="swatch-input super-attribute-select" name="productsize" type="hidden" value="{{$product_sizes[0]->productsize_size}}" data-selector="productsize" data-validate="{required: true}" aria-required="true" aria-invalid="false" data-attr-name="size">
</div>
    <style>
        .swatch.color{
            display:block!important;
        }
        #add-to-cart-quickview-form .selector-wrapper.color{
            display: none !important;
        }
    </style>
			<div class="swatch color  color" data-option-index="1" style="display: none">          
				<div class="header">
					Color <em>*</em>        
				</div>
					<?php
						  $i = 0;
						  foreach ($product_color_image as $color) {
						  $swatch_color_album=str_replace('/','-',$color->productalbum_name);
						  $i++;
					?>
						<div data-value="$color->productalbum_name" class="swatch-element  color {{$color->productalbum_name}} available">
							<div class="tooltip">
								{{$color->productalbum_name}}
							</div>
							<input id="quickview-swatch-1-{{$color->productalbum_name}}" type="radio" name="option-{{$i}}" value="{{$color->productalbum_name}}" <?php if($color->productalbum_name == $product_color){ echo 'checked';}?>>
							
							<label class="swatch_variant_img" for="quickview-swatch-1-{{$color->productalbum_name}}">
								<span class="bgImg" style="background-color: {{$color->productalbum_name}}; background-image: url({{ URL::to('') }}/storage/app/public/pgallery/{{$color->productalbum_img}});">            	
								</span>
								<span class="crossed-out"> </span>
							</label>
						</div>
					<?php } ?>
			</div>     
                <div class="selector-wrapper size">
					<label for="product-select-qv-option-0">Size<em>*</em></label>
					<select class="single-option-selector" data-option="option1" id="product-select-qv-option-0">
					@php($i = 0)
                    @foreach($product_sizes as $size)
						<option value="{{$size->productsize_size}}">{{$size->productsize_size}}</option>
					</select>
					@php($i++)
                   @endforeach 
					<span class="icon-dropdown">
					<i class="fa fa-angle-down"></i></span>
				</div>
				<div class="selector-wrapper option-non color">
					<label for="product-select-qv-option-1">Color<em>*</em></label>
					<select class="single-option-selector" data-option="option2" id="product-select-qv-option-1">
					   <option value="{{$product_color}}">{{$product_color}}</option>
					</select>
					<span class="icon-dropdown">
					<i class="fa fa-angle-down"></i></span>
				</div>
				<select id="product-select-qv" name="id" style="display:none">
                    @php($i = 0)
                    @foreach($product_sizes as $size)
                    <option selected="<?php if ($i == 0){ echo 'selected';}?>" <?php if ($size->SizeWiseQty < 1){echo 'disabled="disabled"';}?> value="{{$size->productsize_size}}">
                        {{$size->productsize_size}} / {{$product_color}}
                    </option>
                    @php($i++)
                    @endforeach                     
                </select>
            </div>
            
            <div class="quantity">
                <label for="quantity">
                    Quantity:
                </label>
                <div class="qty-group">
                    <a href="#" data-qv-minus-qtt="" class="minus button"></a>
                    <input type="text" id="qty" data-qv-qtt-id="quantity" name="quantity" value="1">
                    <a href="#" data-qv-plus-qtt="" class="plus button"></a>
                </div>
            </div>
            
            <div class="total-price">
                <label>
                    Subtotal:
                </label>
                <span class="total-money">Tk {{$singleproduct->product_price}}</span>
            </div> 
            

            <div class="groups-btn">
                <div class="bottom-button">
                    <input data-btn-addtocart="" type="submit" name="add" class="btn" id="product-add-to-cart" value="Add to Cart" data-form-id="#add-to-cart-quickview-form">
<!--<a class="wishlist" data-icon-wishlist="" href="#" data-product-handle="power-wave-molle-blue-sneaker-for-men-1" data-id="4718269857875">
    <svg id="lnr-heart" viewBox="0 0 1024 1024" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path class="path1" d="M486.4 972.8c-4.283 0-8.566-1.074-12.434-3.222-4.808-2.67-119.088-66.624-235.122-171.376-68.643-61.97-123.467-125.363-162.944-188.418-50.365-80.443-75.901-160.715-75.901-238.584 0-148.218 120.582-268.8 268.8-268.8 50.173 0 103.462 18.805 150.051 52.952 27.251 19.973 50.442 44.043 67.549 69.606 17.107-25.565 40.299-49.634 67.55-69.606 46.589-34.147 99.878-52.952 150.050-52.952 148.218 0 268.8 120.582 268.8 268.8 0 77.869-25.538 158.141-75.901 238.584-39.478 63.054-94.301 126.446-162.944 188.418-116.034 104.754-230.314 168.706-235.122 171.376-3.867 2.149-8.15 3.222-12.434 3.222zM268.8 153.6c-119.986 0-217.6 97.614-217.6 217.6 0 155.624 120.302 297.077 221.224 388.338 90.131 81.504 181.44 138.658 213.976 158.042 32.536-19.384 123.845-76.538 213.976-158.042 100.922-91.261 221.224-232.714 221.224-388.338 0-119.986-97.616-217.6-217.6-217.6-87.187 0-171.856 71.725-193.314 136.096-3.485 10.453-13.267 17.504-24.286 17.504s-20.802-7.051-24.286-17.504c-21.456-64.371-106.125-136.096-193.314-136.096z"></path></svg>
    <span class="wishlist-text text-hover">Add to Wish List</span>
</a> -->
 
                </div>
            </div>
        </form>    
        
        <?php $singleproduct->isSpecial==14; ?>
        <div id="customers_view_qv" data-customer-view="100, 59, 11, 14,  46, 10, 18">
            <i class="fa fa-eye" aria-hidden="true"></i>
            <label id="customers_view">5</label> 
			<span>
			  customers are viewing this product
			</span>
        </div>
        <div class="share_toolbox">  
            <span>Share:</span>
            <?php $current_url="https://pride-limited.com/".$details_link;?>
            <a class="at-share-btn" href="javascript:window.open('https://www.facebook.com/dialog/share?app_id=258246005086458&display=popup&href={{$current_url}}&redirect_uri={{$current_url}}', 'facebook', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Share on Facebook"><span class="at-icon-wrapper" style="line-height: 16px; height: 16px; width: 16px;">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-1" class="at-icon at-icon-facebook" style="fill: rgb(34, 34, 34); width: 16px; height: 16px;">
			<title id="at-svg-facebook-1">Facebook</title><g><path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path></g></svg></span></a>
			
			<a class="at-share-btn" href="javascript:window.open('https://twitter.com/share?url={{$current_url}}&text={{$singleproduct->product_name}}', 'twitter', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Tweet"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-2" class="at-icon at-icon-twitter" style="fill: rgb(34, 34, 34); width: 16px; height: 16px;">
			<title id="at-svg-twitter-2">Twitter</title><g><path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path></g></svg></a>
        </div>    
        
    </div>  
</div>
</div>
        </div>     
    </div>
</div>
<script>
jQuery(window).ready(function($){
    $("#product-add-to-cart").click(function(e){
	     e.preventDefault();
	     $(".loading-modal").show();
	     var datastring = $("#add-to-cart-quickview-form").serialize();
	     //alert(datastring);
	     var cart_url = base_url + "/cart/popup-add-to-cart";
	     $.ajax({
	         url:cart_url,
	         type:'POST',
	         data:datastring,
	         success:function(result){
	        // alert(result);
	           $(".loading-modal").hide();
	           $("#cart-modal-content").html(result).fadeIn(800).delay(3000).fadeOut(1500);  
				  $('.close-modal').on('click', function (e) {
					  $("#cart-modal-content").hide();
				  });
				  $('.continue-shopping').on('click', function (e) {
					  $("#cart-modal-content").hide();
				  });
				  $("#btn_checkout_pop").on('click',function(){
                		window.location.href = base_url+"/checkout";
                });
	           var cart_count = base_url + "/cart/item-count";
			   $.ajax({
    				url:cart_count,
    				type:'GET',
    				success:function(result){
    				    if(result==0){
    					   $(".counter").hide();  
    					   //$(".g-stickycart-count").hide(); 
    					}else{
    					   $(".counter").show();  
    					   $(".counter").html(result); 
    					   //$(".g-stickycart-count").html(result); 
    					}
    				}
			   });
			  var cart_reload = base_url + "/cart/reload";
			   $.ajax({
    				url:cart_reload,
    				type:'GET',
    				success:function(result){
    				    $('.block-minicart').empty();
				        $('.block-minicart').append(result);
                        $("#btn-minicart-close").on("click", function () {
                            $(".block-minicart").removeClass("block-minicart-open");
                        });
                        $("#top-cart-btn-checkout").click(function(){
                             window.location=base_url+ "/checkout";
                        });
                        $("#top-cart-btn-cart").click(function(){
                             window.location=base_url+ "/shop-cart";
                        });
    				}
			   });
	             
	         }
	     });
	 });
	 
    var output = document.getElementById('customers_view');
    setInterval(function ()
    {
		var x = Math.floor((Math.random() * 50) + 1);
        output.innerHTML = x;
    }, 3500);
    
    function updatePricingQuickview() {
      var quantity = parseInt($('[data-qv-qtt-id]').val());
      var p = $('.quickview-tpl #product_regular_price').val();
      var totalPrice1 = p * quantity;
      $('.quickview-tpl .total-price span').html('Tk '+totalPrice1);

    };
                                                        
    $('[data-qv-qtt-id]').on('change', updatePricingQuickview);
    
    var buttonSlt = '[data-qv-minus-qtt], [data-qv-plus-qtt]',
        buttonElm = $(buttonSlt);

    $(document).off('click.changeQttQv', buttonSlt).on('click.changeQttQv', buttonSlt, function(e) {
        e.preventDefault();
        e.stopPropagation();

        var self = $(this),
            input = self.siblings('input[name="quantity"]'),
            oldVal = parseInt(input.val()),
            newVal = 1;

        switch (true) {
            case (self.hasClass('plus')): {
                newVal = oldVal + 1;
                break;
            }
            case (self.hasClass('minus') && oldVal > 1): {
                newVal = oldVal - 1;
                break;
            }            
        }

        input.val(newVal);

        updatePricingQuickview();
    });          
});
</script>
