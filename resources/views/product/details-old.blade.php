<?php

use App\Http\Controllers\cart\LastseenController;
use App\Http\Controllers\product\ProductController;
?>
@extends('layouts.app')
@section('title',$title)
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/product_details.css')}}?10">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/add_to_cart.css')}}?10">
<style>
 .page-main{
     padding-top: 0px;
 }
    .dropdown-menu{
        display:none;
    }
    a.simple.button {
        margin-bottom:15px;
    }
    .product-shop {
    margin: 0 0 15px;
    padding: 0 19px;
    padding-top: 16px; 
}
.dropdown-menu>li:first-child {
    border: 1px solid #ddd!important;
    padding: 11px;
}
.swatch {
    border: 2px solid #eee;
    padding: 1px;
}
.selected-color {
    border: 3px solid #291e88!important;
    padding: 2px;
}
@media (max-width: 1000px){
.dropdown-menu > li {
    border: 1px solid #ddd !important;
    padding: 11px !important;
}
.dropdown-menu>li {
    /* border: none; */
    /* padding: 18px 8px 1px 12px; */
}
}
#loading-image {
    position:fixed;
    left:0;
    top:0;
    width:100vw;
    height:100vh;
    background:#333;
    opacity:0.8;
    margin:0;
    z-index:999;
}
#loading-image img {
    position:absolute;
    left:50%;
    top:50%;
    width:auto;
    height:auto;
    transform:translate(-50%, -50%);
    opacity:1;
}
.dropdown{
  display: block;
}
.text-14{
    font-size:14px;
}
</style>
<!-- Magic Zoom Plus Magento 2 module version v1.5.20 [v1.6.64:v5.2.4] -->
<link type="text/css" href="{{asset('assets/MagicZoomPlus/magiczoomplus.css')}}" rel="stylesheet" media="screen" />
<link type="text/css" href="{{asset('assets/MagicZoomPlus/magiczoomplus.module.css')}}" rel="stylesheet" media="screen" />
<script type="text/javascript" src="{{asset('assets/MagicZoomPlus/magiczoomplus.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/MagicZoomPlus/magictoolbox.utils.js')}}"></script>
<script type="text/javascript">
var mzOptions = {
    'zoomWidth': 'auto',
    'zoomHeight': 'auto',
    'zoomPosition': 'right',
    'zoomDistance': 15,
    'selectorTrigger': 'click',
    'transitionEffect': true,
    'lazyZoom': false,
    'rightClick': true,
    'zoomMode': 'zoom',
    'zoomOn': 'hover',
    'upscale': true,
    'smoothing': true,
    'variableZoom': false,
    'zoomCaption': 'off',
    'expand': 'window',
    'expandZoomMode': 'zoom',
    'expandZoomOn': 'click',
    'expandCaption': true,
    'closeOnClickOutside': true,
    'cssClass': '',
    'hint': 'once',
    'textHoverZoomHint': 'Hover to zoom',
    'textClickZoomHint': 'Click to zoom',
    'textExpandHint': 'Click to expand',
    'textBtnClose': 'Close',
    'textBtnNext': 'Next',
    'textBtnPrev': 'Previous'
}
</script>
<script type="text/javascript">
    var mzMobileOptions = {
        'zoomMode': 'zoom',
        'textHoverZoomHint': 'Touch to zoom',
        'textClickZoomHint': 'Double tap to zoom',
        'textExpandHint': 'Tap to expand'
    }
</script>

<main id="maincontent" class="page-main">
    <div v-if="loading_image" id="loading-image">
        <img src="{{ url('/') }}/storage/app/public/loader.gif" Alt="Loading..." />
    </div>
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item">
                                <a href="{{url($main_link)}}" title="Main category"><?php echo $main_cate; ?></a>
                            </li>
                            <li class="item">
                                <a href="{{url($sub_link)}}" title="Sub Category"><?php echo $category_name; ?></a>
                            </li>
                            <li class="item">
                                <a href="{{url($last_link)}}" title="Child sub category"><?php echo $subcategory_name; ?></a>
                            </li>
                            <li class="item">
                                <a href="<?php echo url()->current();?>" title=""><?php echo $title; ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-detail">
                    <div class="row">
                        <div class="col-sm-7  col-lg-5">
                            <div class="gallery-detail">
                                <a id="gallery-prev-area" tabindex="-1"></a>
                                <div class="action-skip-wrapper"><a class="action skip gallery-next-area" href="#gallery-next-area"><span>Skip to the end of the images gallery</span></a>
                                </div><div class="action-skip-wrapper"><a class="action skip gallery-prev-area" href="#gallery-prev-area"><span>Skip to the beginning of the images gallery</span></a>
                                </div><a id="gallery-next-area" tabindex="-1"></a>
                                <div class="MagicToolboxContainer selectorsRight minWidth" data-mage-init='{"magicToolboxThumbSwitcher": {"playIfBase":0,"showRelated":0,"videoAutoRestart":0,"tool":"magiczoomplus","switchMethod":"click","productId":"90363"}}'>
                                    <div class="MagicToolboxMainContainer">
                                        <div id="mtImageContainer" style="display: block;">
                                            <div>
                                                <?php
                                                $i = 0;
                                                foreach ($singleproductmultiplepic as $smplist) {
                                                    $i++;
                                                    if ($i == 1) {
                                                        ?>
                                                        <a id="MagicZoomPlusImage-product-90363"  class="MagicZoom" href="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}" title="{{$singleproduct->product_name}}" data-options="lazyZoom:true;">
                                                            <img itemprop="image" src="{{url('/')}}/storage/app/public/pgallery/{{$smplist->productimg_img}}"   alt="{{$singleproduct->product_name}}" />
                                                        </a>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div id="mt360Container" style="display: none;"></div>
                                        <div id="mtVideoContainer" style="display: none;"></div>    
                                    </div>
                                    <div class="MagicToolboxSelectorsContainer" style="flex-basis: 88px; width: 88px;">
                                        <div id="MagicToolboxSelectors90363" class="">
                                            <!--                                            active-selector-->
                                            @foreach($singleproductmultiplepic as $smplist)	
                                            <a class="mt-thumb-switcher  " data-zoom-id="MagicZoomPlusImage-product-90363" href="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img}}"  data-image="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img}}" title="{{$singleproduct->product_name}}">
                                                <img src="{{ URL::to('') }}/storage/app/public/pgallery/{{$smplist->productimg_img_tiny}}"  alt="{{$singleproduct->product_name}}" />
                                            </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    if (window.matchMedia("(max-width: 767px)").matches) {
                                        $scroll = document.getElementById('MagicToolboxSelectors90363');
                                        if ($scroll && typeof $scroll != 'undefined') {
                                            $attr = $scroll.getAttribute('data-options');
                                            if ($attr !== null) {
                                                $scroll.setAttribute('data-options', $attr.replace(/orientation *\: *[a-zA-Z]{1,}/gm, 'orientation:horizontal'));
                                            }
                                        }
                                    }
                                </script>
                            </div>
                        </div>
                        <div class="col-sm-5 col-lg-4">
                            <div class="product-info-main">
                                <div class="page-title-wrapper product">
                                    <div class="page-title-wrapper">
                                        <div>
                                            <h1 class="page-title">
                                                <span class="base" data-ui-id="page-title-wrapper" >{{$singleproduct->product_name}}</span>
                                            </h1>
                                        </div>
                                        <?php if ($category_name == 'Ready To Wear') { ?>
                                            <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_girl.png" class="brand-img" style="width:175px;height:auto;"/>
                                            </div>
                                        <?php } elseif ($subcategory_name == 'Classic Sari' || $subcategory_name == 'Classic Three Piece') { ?>
                                            <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_classic.png" class="brand-img" style="width:175px;height:auto;"/>
                                            </div>
                                        <?php } elseif ($category_name == 'Ethnic Meanswear') { ?>
                                            <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_menswear.png" class="brand-img" style="width:175px;height:auto;"/>
                                            </div>
                                        <?php }elseif($category_name =='Girls' || $category_name =='Boys'){ ?>
                                         <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_kids.png" class="brand-img" style="width:175px;height:auto;"/>
                                          </div>
                                         <?php }elseif($subcategory_name == 'Signature Three Piece'){ ?>
                                         <div class="pull-right hidden-xs" style="position: absolute;right: 29px;top: 0px;">
                                                 <img src="{{ URL::to('') }}/storage/app/public/brand_logo/pride_signature.png" class="brand-img" style="width:175px;height:auto;"/>
                                          </div>
                                         <?php } ?>
                                    </div>
                                </div>
                                <div class="product-info-price">
                                    <div class="product-info-stock-sku">
                                        <div class="product attribute sku">
                                            <strong class="type">Code</strong>    
                                            <div class="value" itemprop="code">{{$singleproduct->product_styleref}}</div>
                                             <div class="font-alt product_barcode" style="display:none;font-size:8pt;text-transform: none;padding-top:5px;"><span> Barcode : </span> <span id="barcode"></span></div>
                                        </div>
                                    </div>
                                    <div class="price-box price-final_price" data-role="priceBox" data-product-id="{{$singleproduct->id}}" data-price-box="product-id-{{$singleproduct->id}}">
                                        <span class="price-container price-final_price tax weee"
                                              itemprop="offers" itemscope itemtype="#">
                                            <span  id="product-price-{{$singleproduct->product_price}}" data-price-amount="{{$singleproduct->product_price}}"
                                                   data-price-type="finalPrice"
                                                   class="price-wrapper "
                                                   itemprop="price">
                                                <span class="price"> 
                                                <?php
                                            if ($singleproduct->product_pricediscounted < 1) {
                                                echo 'Tk' . ' ' . $singleproduct->product_price . '<br>';
                                            } else {
                                                echo '<div class="FColor">' . 'Tk' . ' ' . $singleproduct->discount_product_price . '</div>';
                                                ?>
                                                                                <!--<div class="sale-flash"><?php $singleproduct->discount_product_price . '%'; ?>  Off*</div>-->
                                                <?php
                                                echo '<div class="offer-rate"><del> Tk' . $singleproduct->product_price . '</del>'; echo '&nbsp;&nbsp;'.$singleproduct->product_pricediscounted . '% OFF</div>'; 
                                            }
                                            ?>
                                                
                                                </span>   
                                                   
                                            </span>
                                            <meta itemprop="priceCurrency" content="TK" />
                                        </span>
                                    </div>
                                </div>
                                <div class="cartForm">
                                    <style>
                                        .product-info-main .sizechart { display : none; }
                                    </style>
                                    <div class="product-add-form">
                                        <form action="{{url('/cart/add-to-cart')}}" method="post" id="product_addtocart_form">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="productid" value="{{$singleproduct->id}}" />
                                            <input type="hidden" name="productcolor" id="selectcolor" value="{{$product_color}}">
                                            <input type="hidden" name="productimage" id="productImage" value="{{$singleproduct->productimg_img_thm}}">
                                            <div class="product-options-wrapper" id="product-options-wrapper" data-hasrequired="* Required Fields">
                                                <div class="fieldset" tabindex="0">
                                                    <div class="swatch-opt" data-role="swatch-options">
                                                        <div class="swatch-attribute size" attribute-code="size" attribute-id="142" option-selected="273">
                                                            <span id="option-label-size-142" class="swatch-attribute-label">Color</span>
                                                            <span class="swatch-attribute-selected-option">{{ $product_color }} </span>
                                                            <?php
                                                            $product_name = str_replace(' ', '-', $singleproduct->product_name);
                                                            $product_url = strtolower($product_name);
                                                            ?>
                                                            <?php
                                                            $i = 0;
                                                            foreach ($product_color_image as $color) {
                                                                $swatch_color_album=str_replace('/','-',$color->productalbum_name);
                                                                $i++;
                                                                ?>
                                                                <?php
                                                                if ($i == 5) {
                                                                    echo "</br></br>";
                                                                } else {
                                                                    echo "";
                                                                }
                                                                ?>
                                                                <a class="pull-right"  style="overflow:hidden; width:30px; height:30px; border-radius:50%; display:inline-block;margin: 0 5px 6px 0px;" href='{{url("shop/{$product_url}/color-{$swatch_color_album}/{$singleproduct->id}")}}'>
                                                                    <img style="width:30px; height:30px; display:inline-block; border-radius:50%" src="{{ URL::to('') }}/storage/app/public/pgallery/{{$color->productalbum_img}}" class="img-responsive swatch <?php if ($product_selected_color->productalbum_img == $color->productalbum_img) echo 'selected-color'; ?>"/>
                                                                </a>
                                                               &nbsp;&nbsp;
                                                            <?php } ?>
                                                            <input class="swatch-input super-attribute-select" type="text" value="{{$product_color}}" data-selector="productsize" data-validate="{required: true}" aria-required="true" aria-invalid="false" data-attr-name="size">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-options-wrapper" id="product-options-wrapper" data-hasrequired="* Required Fields">
                                                <div class="fieldset" tabindex="0">
                                                    <div class="swatch-opt" data-role="swatch-options">
                                                        <div class="swatch-attribute size" attribute-code="size" attribute-id="142" option-selected="273">
                                                            <span id="option-label-size-142" class="swatch-attribute-label">Size</span>
                                                            <span id="product-size-text" class="swatch-attribute-selected-option">{{$product_sizes[0]->productsize_size}}</span>
                                                            <div aria-activedescendant="option-label-size-142-item-273" tabindex="0" aria-invalid="false" aria-required="true" role="listbox" aria-labelledby="option-label-size-142" class="swatch-attribute-options clearfix">
                                                                <!--selected-->
                                                                @php($i = 0)
                                                                @foreach($product_sizes as $size)
                                                                <div class="swatch-option text <?php
                                                                if ($size->SizeWiseQty < 1)
                                                                    echo 'disabled-size';
                                                                elseif ($i == 0)
                                                                    echo 'active';
                                                                ?>" aria-checked="false" aria-describedby="option-label-size-142" tabindex="0" option-type="0" option-id="271" option-label="{{$size->productsize_size}}" aria-label="{{$size->productsize_size}}" option-tooltip-thumb="" option-tooltip-value="{{ $size->productsize_size }}" role="option">{{ $size->productsize_size }}</div>
                                                                @php($i++)
                                                                @endforeach
                                                            </div>
                                                            <input id="product-size-input" class="swatch-input super-attribute-select" name="productsize" type="text" value="{{$product_sizes[0]->productsize_size}}" data-selector="productsize" data-validate="{required: true}" aria-required="true" aria-invalid="false" data-attr-name="size">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-tocart">
                                                <div class="fieldset">
                                                    <div class="field qty">
                                                        <label class="label" for="qty"><span>Quantity</span></label>
                                                        <div class="control ">
                                                            <div class="inputnumber">
                                                                <a href="#" class="plusminus" id="plus"><span>+</span></a>
                                                                <input type="number" name="productqty" id="qty" min="1" max="{{$product_qty}}" value="1" title="Qty" class="input-text qty"/>
                                                                <a href="#" class="plusminus" id="minus"><span>-</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="color:red;font-weight:700;padding-bottom:10px;" id="total_sold_out_msg"></div>
                                                     <div style="color:red;font-weight:700;padding-bottom:10px;" id="sold_out_msg"></div>
                                                    <div class="actions">
                                                        <button onClick="startLoading()" id="add-cart" type="submit"
                                                                title="Add to Cart"
                                                                class="action tocart"
                                                                id="product-addtocart-button">
                                                            <span class="pride.add_to_cart">Add to Cart</span>
                                                        </button>
                                                        <div id="instant-purchase" data-bind="scope:'instant-purchase'">
                                                        </div>
                                                        <div class="size-popup">
                                                            <div class="popup">
                                                                <div class="video-area">
                                                                    <div class="video-holder">
                                                                        <div class="video-frame">
                                                                            <div class="size-chart-tabel"><h1>CLOTHING SIZE CHART GIRLS</h1> 
                                                                                <table width="100%" border="1"> 
                                                                                    <tr class="even"> 
                                                                                        <th scope="col">GIRLS</th> 
                                                                                        <th scope="col">MEASURE YOUR</th> 
                                                                                        <th scope="col">XS</th> 
                                                                                        <th scope="col">S</th> 
                                                                                        <th scope="col">M</th> 
                                                                                        <th scope="col">L</th> 
                                                                                        <th scope="col">XL</th> 
                                                                                        <th scope="col">XXL</th> 
                                                                                        <th scope="col">XXXL</th> 
                                                                                    </tr> 
                                                                                    <tr > 
                                                                                        <th scope="row">REGULAR FIT TUNIC</th> 
                                                                                        <th scope="row">CHEST</th> 
                                                                                        <td></td> 
                                                                                        <td>38"</td> 
                                                                                        <td>39.5"</td> 
                                                                                        <td>41"</td> 
                                                                                        <td>42.5"</td> 
                                                                                        <td>44"</td> 
                                                                                        <td>45.5"</td>  
                                                                                    </tr> 
                                                                                    <tr class="even"> 
                                                                                        <th scope="row">TAILORED FIT TUNIC</th> 
                                                                                        <th scope="row">CHEST</th> 
                                                                                        <td></td> 
                                                                                        <td>36"</td> 
                                                                                        <td>37.5"</td> 
                                                                                        <td>39"</td> 
                                                                                        <td>40.5"</td> 
                                                                                        <td>42"</td> 
                                                                                        <td>43.5"</td> 
                                                                                    </tr> 
                                                                                    <tr > 
                                                                                        <th scope="row">PANT</th>
                                                                                        <th scope="row">WAIST</th> 
                                                                                        <td>26"</td> 
                                                                                        <td>28"</td> 
                                                                                        <td>30"</td> 
                                                                                        <td>32"</td> 
                                                                                        <td>34"</td> 
                                                                                        <td>36"</td> 
                                                                                        <td>38"</td> 
                                                                                    </tr> 
                                                                                    <tr class="even"> 
                                                                                        <th scope="row">LEGGINGS</th> 
                                                                                        <th scope="row">WAIST</th> 
                                                                                        <td></td> 
                                                                                        <td>26"</td> 
                                                                                        <td>27"</td> 
                                                                                        <td>28"</td> 
                                                                                        <td>29"</td> 
                                                                                        <td>33"</td> 
                                                                                        <td>31"</td> 
                                                                                    </tr> 
                                                                                </table> 
                                                                                <span class="note">*Measurements in inches</span> 
                                                                            </div>                                        
                                                                            <a class="close icon-delete-outline" href="#"><span>Close</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <a class="sizechart open" href="#popup1">size chart</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                jQuery('#plus').click(function (e) {
                                                    var currentval = jQuery('#qty').val();
                                                    var newValue = parseInt(currentval) + 1;
                                                    jQuery('#qty').val(newValue);
                                                    e.preventDefault();
                                                });
                                                jQuery('#minus').click(function (e) {
                                                    var currentval1 = jQuery('#qty').val();
                                                    var newValue1 = currentval1 - 1;
                                                    if (newValue1 > 0) {
                                                        jQuery('#qty').val(newValue1);
                                                    }
                                                    e.preventDefault();
                                                });
                                            </script>

                                        </form>
                                    </div>
                                </div>
                               <div id="find-store-section" style="position:relative; min-height:50px;">
									<a class="simple button" href="#" title="{{$singleproduct->product_name}}">
										<span class="sprite-left store-locater sprite-left"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
										Find In Store
									</a>
									<span class="find_store_close">X</span>
								</div> 
                                <div class="searchstore">
                                    <div id="preferred-store-panel">
										<div class="dropdown">
											<button id="select-district" class="btn btn-primary dropdown-toggle"  style="width:100%; background-color:#291e88; border-color:#291e88" type="button" data-toggle="dropdown">Select District <span class="caret"></span></button>
											<ul class="dropdown-menu" style="width:100%" id="find_in_city">
											<!--	<li style="cursor:pointer" onClick="getStore(this.textContent || this.innerText)" class="list-group-item"></li> -->
											</ul>
										</div>
                                        <span class="stockmsg">Please note availability may change</span>
                                    </div>
                                    <div  class="searchresults">
                                        <div id="store-list-loading" style="text-align:center; padding-bottom:10px; display:none"><img style="width:auto; margin:auto" src="{{url('/')}}/storage/app/public/loader.gif" /></div>
                                        <div class="store-list-container" id="store-list-container">
                                            
                                        </div>
                                        <div class="store-list-pagination">
                                        </div></div>
                                </div>
                                <div class="product-shop">    
                                    <div class="product info detailed">
                                        <div class="content">
                                            <div class="product data items" data-mage-init='{"tabs":{"openedState":"active"}}'>
                                                <div class="data item title"
                                                     aria-labeledby="tab-label-product.info.overview-title"
                                                     data-role="collapsible" id="tab-label-product.info.overview">
                                                    <a class="data switch"
                                                       tabindex="-1"
                                                       data-toggle="switch"
                                                       href="#product.info.overview"
                                                       id="tab-label-product.info.overview-title">
                                                        Description                    
                                                    </a>
                                                </div>
                                                <div class="data item content" id="product.info.overview" data-role="content">
                                                    <div class="product attribute overview">
                                                        <div style="font-size:14px" class="value" itemprop="description">{{ $singleproduct->product_description }}</div>
                                                    </div>
                                                </div>
                                                
                                                <div class="data item title"
                                                     aria-labeledby="tab-label-additional-title"
                                                     data-role="collapsible" id="tab-label-additional">
                                                    <a class="data switch"
                                                       tabindex="-1"
                                                       data-toggle="switch"
                                                       href="#additional"
                                                       id="tab-label-additional-title">
                                                        Fabric Composition            
                                                    </a>
                                                </div> 
                                                <div class="data item content" id="product.info.overview" data-role="content">
                                                    <div class="product attribute overview">
                                                        <div style="font-size:14px" class="value" itemprop="description">{{ $singleproduct->fabric }}</div>
                                                    </div>
                                                </div>
                                                <div class="data item content" id="additional" data-role="content">
                                                    <div class="additional-attributes-wrapper table-wrapper">
                                                        <table class="data table additional-attributes" id="product-attribute-specs-table">
                                                            <!--<caption class="table-caption"></caption>-->
                                                            <tbody>
                                                                <tr>
                                                                    <th class="col label" style="font-size:16px" scope="row">Care</th>
                                                                    <td class="col data" data-th="Material">{{ $singleproduct->product_care }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="social-networks">
                                    <strong class="label">share this product</strong>
                                    <ul class="social-icons">
                                        <?php $current_url=URL::current();?>
                                        <li><a class="tw icon-social-twitter" href="javascript:window.open('https://twitter.com/share?url={{$current_url}}&text={{$singleproduct->product_name}}', 'twitter', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Tweet"><span>Twitter</span></a></li> 
                                        <li><a class="fb icon-facebook"  href="javascript:window.open('https://www.facebook.com/dialog/share?app_id=258246005086458&display=popup&href={{$current_url}}&redirect_uri={{$current_url}}', 'facebook', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Share on Facebook"><span>facebook</span></a></li>
                                        <!-- li><a class="pn icon-pinterest-p" href="javascript:window.open('https://pinterest.com/pin/create/button/?url=&media=&description=MA18406', 'pinterest', 'width=640,height=480,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');" title="Pin it"><span>Pinterest</span></a></li -->
                                        <!--<li> <a class="wa fa fa-whatsapp" href="whatsapp://send?text=https://www.pride-limited.com/ma18406-beige-3pc.html" data-action="share/whatsapp/share"><span>Share via Whatsapp</span></a></li> 
                                        https://www.facebook.com/dialog/share?app_id=258246005086458&display=popup&href={{$singleproduct->id}}&redirect_uri=https://pride-limited.com/
                                        --->
                                    </ul>
                                </div>
                              </div>
                              <br>
                              <div class="row mt-70 hidden-sm hidden-md hidden-lg">
                                  <div class="col-sm-12">
                                    <ul class="nav nav-tabs font-change" role="tablist">
                                      <li class="active"><a href="#shipping" data-toggle="tab"><i class="fa fa-bicycle" aria-hidden="true"></i> Shipping</a></li>
                                      <li><a href="#exchange" data-toggle="tab"><i class="fa fa-exchange" aria-hidden="true"></i> Exchange Policy</a></li>
                                    </ul>
                                    <div class="tab-content">
                                      <div class="tab-pane active" id="shipping">
                                        <div class="panel-group" id="accordion">
                						<br>
                    					    <h4>* Order delivery is subject to availability of stock.</h4>
                    						<p class="text-14">We can ship to virtually any address in our country. Note that Currently, we do not ship outside Bangladesh.</p>
                    						<p  class="text-14">When you place an order, we will estimate shipping and delivery dates for you based on the availability of your items and the shipping options you choose. Depending on the shipping provider you choose, shipping date estimates may appear on the shipping quotes page.</p>
                    						<p  class="text-14">If any question Please <a href="{{url('/contact-us')}}">Click here</a> to contact our Customer Service Team.</p>
                                            <!--<div class="panel panel-default">
                                              <div class="panel-heading">
                                                <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#support1">What Kinds Of Shipping Options Do You Offer?</a></h4>
                                              </div>
                                              <div class="panel-collapse collapse in" id="support1">
                                                <div class="panel-body">
                    							    We provide Card and bKash payment to Dhaka customers. For customers outside Dhaka, prepayment must be paid via bKash or card and then we ship the product via our trusted partners.
                                                    Free shipping is available for purchases worth TK 3000 and above.
                    							</div>
                                              </div>
                                            </div> --->
                                          </div>
                                      </div>
                                      <div class="tab-pane" id="exchange">
                					  <br>
                                        <p  class="text-14">Customers have to pay the bill amount in advance through Credit Card/Bkash/Bank Deposit for any orders.<br>Pride Limited does not refund any products. However, we do exchange products if:</p>
                                        <p  class="text-14">The exchange must be made within 15 days of purchase and the product, product package and the hangtag must be intact. The original invoice must be presented during exchange. Call our customer service to exchange the product. You can return your items through courier service to our e-commerce warehouse at the following address and then we will ship the right sized/colored product to you.</p>
                    					<p  class="text-14"><br>Please <a href="{{url('/contact-us')}}">Click here </a> to contact our Customer Service Team.</p>
                    				  </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-3">
                                <div class="match-with-sec">
                                <div class="block related" data-mage-init='{"relatedProducts":{"relatedCheckbox":".related.checkbox"}}' data-limit="3" data-shuffle="1">
                                    <div class="block-title title">
                                        <strong id="block-related-heading" role="heading" aria-level="2">Top Picks</strong>
                                    </div>
                                    <div class="block-content content" aria-labelledby="block-related-heading">
                                        <div class="products wrapper grid products-grid products-related">
                                            <ol class="products list items product-items">
                                                <?php
                                                foreach ($top_pick as $top) {
                                                $product_name = str_replace(' ', '-', $top->product_name);
                                                $product_url = strtolower($product_name);
                                                $top_color_album=str_replace('/','-',$top->productalbum_name);
                                                    ?>
                                                    <li class="item product product-item">                                
                                                        <div class="product-item-info related-available">
                                                            <!-- related_products_list-->                    
                                                            <a href="{{url("shop/{$product_url}/color-{$top_color_album}/{$top->id}")}}" class="product photo product-item-photo">
                                                                <span class="product-image-container">
                                                                    <span class="product-image-wrapper">
                                                                        <img class="product-image-photo"
                                                                             src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $top->productimg_img_medium; ?>" alt=" {{$top->product_name}}"/></span>
                                                                </span>
                                                            </a>
                                                            <div class="product details product-item-details">
                                                                <strong class="product name product-item-name">
                                                                    <a class="product-item-link" title="{{$top->product_name}}" href="{{url("shop/{$product_url}/color-{$top_color_album}/{$top->id}")}}">
                                                                        {{$top->product_name}}
                                                                    </a>
                                                                </strong>
                                                                <div class="price-box price-final_price" data-role="priceBox" data-product-id="{{$top->id}}" data-price-box="product-id-{{$top->id}}">
                                                                    <span class="price-container price-final_price tax weee">
                                                                        <span  id="product-price-{{$top->product_price}}" data-price-amount="{{$top->product_price}}" data-price-type="finalPrice" class="price-wrapper ">
                                                                            <span class="price">Tk {{$top->product_price}} </span>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                <div class="field choice related">
                                                                    <input type="checkbox" class="checkbox related" id="related-checkbox75234" name="related_products[]" value="{{$top->id}}" />
                                                                    <label class="label" for="related-checkbox75234"><span>Shop Now</span></label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ol>
                                        </div>
                                       <!-- <a class="btn-next" href="javascript:void(0);">next</a>
                                        <a class="btn-prev" href="javascript:void(0);">prev</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-70 hidden-xs">
                  <div class="col-sm-12">
                    <ul class="nav nav-tabs font-change" role="tablist">
                      <li class="active"><a href="#shipping_down" data-toggle="tab"><i class="fa fa-bicycle" aria-hidden="true"></i> Shipping</a></li>
                      <li><a href="#exchange_down" data-toggle="tab"><i class="fa fa-exchange" aria-hidden="true"></i> Exchange Policy</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="shipping_down">
                        <div class="panel-group" id="accordion">
						<br>
    					    <h4>* Order delivery is subject to availability of stock.</h4>
    						<p class="text-14">We can ship to virtually any address in our country. Note that Currently, we do not ship outside Bangladesh.</p>
    						<p  class="text-14">When you place an order, we will estimate shipping and delivery dates for you based on the availability of your items and the shipping options you choose. Depending on the shipping provider you choose, shipping date estimates may appear on the shipping quotes page.</p>
    						<p  class="text-14">If any question Please <a href="{{url('/contact-us')}}">Click here</a> to contact our Customer Service Team.</p>
                            <!--<div class="panel panel-default">
                              <div class="panel-heading">
                                <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#support1">What Kinds Of Shipping Options Do You Offer?</a></h4>
                              </div>
                              <div class="panel-collapse collapse in" id="support1">
                                <div class="panel-body">
    							    We provide Card and bKash payment to Dhaka customers. For customers outside Dhaka, prepayment must be paid via bKash or card and then we ship the product via our trusted partners.
                                    Free shipping is available for purchases worth TK 3000 and above.
    							</div>
                              </div>
                            </div> --->
                          </div>
                      </div>
                      <div class="tab-pane" id="exchange_down">
					  <br>
                        <p  class="text-14">Customers have to pay the bill amount in advance through Credit Card/Bkash/Bank Deposit for any orders.<br>Pride Limited does not refund any products. However, we do exchange products if:</p>
                        <p  class="text-14">The exchange must be made within 15 days of purchase and the product, product package and the hangtag must be intact. The original invoice must be presented during exchange. Call our customer service to exchange the product. You can return your items through courier service to our e-commerce warehouse at the following address and then we will ship the right sized/colored product to you.</p>
    					<p  class="text-14"><br>Please <a href="{{url('/contact-us')}}">Click here </a> to contact our Customer Service Team.</p>
    				  </div>
                    </div>
                  </div>
                </div>
                <div class="block upsell" data-mage-init='{"upsellProducts":{}}' data-limit="4" data-shuffle="1">
                    <div class="block-title title">
                        <strong id="block-upsell-heading" role="heading" aria-level="2">You may also like</strong>
                    </div>
                    <div class="block-content content" aria-labelledby="block-upsell-heading">
                        <div class="products wrapper grid products-grid products-upsell">
                            <ol class="products list items product-items">
                                <?php
                                foreach ($may_like as $product) {
                                    $product_name = str_replace(' ', '-', $product->product_name);
                                    $product_url = strtolower($product_name);
                                    $data = ProductController::GetProductColorAlbum($product->id);
                                    $color_album=str_replace('/','-',$product->productalbum_name);
                                    // dd($data);
                                    $sold_out = ProductController::ProductWiseQty($product->id);
                                    foreach ($data as $pro_album) {
                                        $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->id);
                                    }
                                     $images_last = ProductController::GetProductImageByColorAlbum($pro_album->id); 
                                     
                                  //   dd($images);
                                    ?>
                                    <li class="item product product-item">                                
                                        <div class="product-item-info ">
                                            <!-- upsell_products_list-->                    
                                            <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}" class="product photo product-item-photo">
                                                <span class="product-image-container">
                                                    <span class="product-image-wrapper">
                                                        <span class="custom-carousel" onmouseenter="fadeImages(this)" onmouseleave="removeTimer(this)">
                                                            @php($i = 0)
                                                            @foreach($images_last as $image_l)
                                                            <img class="item large_img<?php if($i==0) echo ' active';?>" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $image_l->productimg_img_medium }}" alt="{{$product->product_name}}"/>
                                                            @php($i++)
                                                            @endforeach
                                                        </span>
                                                        </span>
                                                </span>
                                            </a>
                                            <div class="product details product-item-details">
                                                <strong class="product name product-item-name"><a class="product-item-link" title="{{$product->product_name}}" href="{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}">
                                                        {{$product->product_name}}</a>
                                                </strong>
                                                <div class="price-box price-final_price" data-role="priceBox" data-product-id="{{$product->id}}" data-price-box="product-id-{{$product->id}}">
                                                    <span class="price-container price-final_price tax weee">
                                                        <span  id="product-price-{{$product->product_price}}" data-price-amount="{{$product->product_price}}" data-price-type="finalPrice" class="price-wrapper ">
                                                            <span class="price">Tk {{$product->product_price}} </span>    
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ol>
                        </div>
                        <a class="btn-next" href="javascript:void(0);"><img style="width:34px" src="{{url('/')}}/storage/app/public/icon_next.svg"/>Next</a>
                        <a class="btn-prev" href="javascript:void(0);"> <img style="width:34px" src="{{url('/')}}/storage/app/public/icon_prev.svg"/>prev</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- Modal -->
<div class="modal fade" id="proceed-modal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="alertCartModal"><?php $check_stock=session('stock_out_message'); if($check_stock==1){ echo 'Your product has been added to the cart !';}else{ ?>
                       <span style="color:red;">{{session('stock_out_message')}}</span>
                    <?php } ?> </div>
                    <div class="col-sm-6 col-md-4 col-xs-5">
                        <img style="border: 1px solid #eee;" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $singleproductmultiplepic[0]->productimg_img }}" alt="" />
                    </div>
                    <div class="col-sm-6 col-md-8 col-xs-7">
                        <div class="page-title-wrapper product">
                            <div class="page-title-wrapper">
                                <div>
                                    <h1 class="page-title">
                                        <span class="base title_mobile" data-ui-id="page-title-wrapper" >{{$singleproduct->product_name}}</span>
                                    </h1>
                                </div>
                            </div>
                        </div>
                         <div class="product-info-price">
                            <div class="product-info-stock-sku">
                                <div class="product attribute sku" style="color: black;font-size:11px;">
                                    <strong class="type">Style Code</strong>    
                                    <div class="value" itemprop="sku">{{$singleproduct->product_styleref}}</div>
                                    <!--<br><strong class="type">Barcode</strong>    
                                    <div class="value" itemprop="sku">{{session('cart_product_barcode')}}</div> -->
                                </div>
                            </div>
                         </div>
                        <p><span></span><span style="font-weight: 600;">Tk {{session('cart_product_price')}}</span></p>
                        <p><span>Color - </span><span style="font-weight: 600;">{{session('cart_product_color')}}</span></p>
                        <p><span>Size - </span><span style="font-weight: 600;">{{session('cart_product_size')}}</span></p>
                    </div>
                    <!--<div class="col-xs-6" style="position:absolute;bottom:20px;right:0">
                        <a style="color:#291e88" href="{{ url('shop-cart') }}">Checkout</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="text-danger" data-dismiss="modal">Close</a>
                    </div> -->
                </div>
                <!--<table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Image</th>
                            <th>Color</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$singleproduct->product_name}}</td>
                            <td><img style="width:50px" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $singleproductmultiplepic[0]->productimg_img }}" alt="" /></td>
                            <td><img src="{{ URL::to('') }}/storage/app/public/pgallery/{{$product_selected_color->productalbum_img}}" style="width:30px; height:30px; display:inline-block; border-radius:50%;"/></td>
                        </tr>
                    </tbody>
                </table>-->
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="{{ url('shop-cart') }}" style="color:#1d0d9e;border-bottom:1px solid #1d0d9e;">Checkout</a> &nbsp;&nbsp; | &nbsp;&nbsp;
                <a href="#" data-dismiss="modal" style="color:red;border-bottom:1px solid #1d0d9e;">Close</a>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    
</script>
<script>
    jQuery(document).ready(function ($) {
        $('.swatch-option').click(function () {
            $('#product-size-text').text($(this).text());
            $('#product-size-input').val($(this).text());
            $('.swatch-option').removeClass("active");
            $(this).addClass("active");
        });

        // Hide the Modal
        @if (Session::has('returned'))
        $("#proceed-modal").modal("show");
        @endif
        var base_url = "{{ URL::to('') }}";
        var product_name = '<?php echo $product_url; ?>';
        var product_id = '<?php echo $singleproduct->id; ?>';
        var color_name = '<?php echo $product_color; ?>';
       /* var url_op = base_url + "/GetTotalForAlert/" + product_id;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (html) {
                    if (html > 0) {
                } else {
                    $("#sold_out_msg").html(" ");
                    $("#total_sold_out_msg").html("<span>This product has sold out.");
                    $("#ProductQty").find('option').remove();
                    $("#ProductQty").append('<option value="' + 0 + '">' + 0 + '</option>');
                 //   document.getElementById("add_to_cart").disabled = true;
                 }
                 
                }
            }); */
        $("#add-cart").click(function(e){
            e.preventDefault();
            document.getElementById("loading-image-removed").outerHTML = '<div id="loading-image"><img src="{{url('/')}}/storage/app/public/loader.gif" Alt="Loading..." /></div>';
			var datastring = $("#product_addtocart_form").serialize();
			var cart_url = base_url + "/cart/add-to-cart";
            $.ajax({
				url:cart_url,
				type:'POST',
				data:datastring,
				success:function(result){
					//alert(result);
					 document.getElementById("loading-image").outerHTML = '<div id="loading-image-removed"></div>';
					if(result ==0){
					    alert('This size has minimum quantity which you already added into your cart.');
					}else if(result ==1){
					    alert('This product has sold out.');
					}else{
					  var cart_count = base_url + "/cart/item-count";
					   $.ajax({
            				url:cart_count,
            				type:'GET',
            				success:function(result){
            				    $(".cart_number").show();  
			                    $(".g-stickycart-count").show();
            				   $(".cart_number").html(result); 
            				   $(".g-stickycart-count").html(result); 
            				}
					   });
					  $("#CartPopupArea").html(result).fadeIn(800).delay(3000).fadeOut(1500);  
					  $('.close-modal').on('click', function (e) {
						  $("#CartPopupArea").hide();
					  });
					  $('.continue-shopping').on('click', function (e) {
						  $("#CartPopupArea").hide();
					  });
					  $("#btn_checkout_pop").on('click',function(){
                    		window.location.href = base_url+"/checkout";
                    	});
                    	
        //                 var temp_cart = base_url + "/cart/save-to-database";
    				// 	 $.ajax({
    				// 		url:temp_cart,
    				// 		type:'POST',
    				// 		data:datastring, 
    				// 		success:function(result){
    							
    				// 		}
    				// 	 });
					}
				}
            });
        //     // The GTM code.
        //     dataLayer.push({
        //       "event": "addToCart",
        //       "ecommerce": {
        //         "currencyCode": "TK",
        //         "add": {
        //           "products": [{
        //             "id": "{{$singleproduct->product_styleref}}",
        //             "name": "{{$singleproduct->product_name}}",
        //             "price": "{{session('cart_product_price')}}",
        //             "brand": "Pride",
        //             "category": "{{$category_name}}",
        //             "variant": "{{session('cart_product_color')}}",
        //             "dimension1": "{{session('cart_product_size')}}",
        //             "quantity": 1
        //           }]
        //         }
        //       }
        //     });
        //  //   console.log(dataLayer);
        // });
        // dataLayer.push({
        //   'ecommerce': {
        //     'detail': {
        //       'actionField': {'list': 'Top View Gallery'},    // 'detail' actions have an optional list property.
        //       'products': [{
        //         'name': '{{$singleproduct->product_name}}',         // Name or ID is required.
        //         'id': '{{$singleproduct->product_styleref}}',
        //         'price': "{{$singleproduct->product_price}}",
        //         'brand': 'Pride',
        //         'category': "{{$category_name}}",
        //         'variant': "{{$product_color}}"
        //       }]
        //      }
        //   }
        // });
        //   console.log(dataLayer);
      });
    });
</script>
<script>
    jQuery(document).ready(function ($) {
        
        
       // FindStore();
        $('.find_store_close').hide();
        var product_id = "{{$singleproduct->id}}";
        var get_color_name = "{{$product_color}}";
        var product_styleref = "{{$singleproduct->product_styleref}}";
        var color_name = get_color_name.replace("/", "-");
        //find in city list
        var url_op = base_url + "/find-get-city/" + product_styleref;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data)
                {
                 if(data ==''){
                     $('#find_in_city').append("<li style='cursor:pointer;color:red;'  class='list-group-item'>No data Found</li>");
                 }else{
                    $.each(data, function (index, citobj) {
                      $('#find_in_city').append("<li style='cursor:pointer' onClick='getStore(this.textContent || this.innerText)' class='list-group-item'>" + citobj.district + "</li>");
                    });
                   }
                }
            });
            
     //   alert(color_name);
       // var productSize = $("#ProductSizeList option:selected").text();
		var productSize = $('#product-size-input').val();
		$('.swatch-option').click(function () {
            $('#product-size-text').text($(this).text());
            $('#product-size-input').val($(this).text());
            $('.swatch-option').removeClass("active");
            $(this).addClass("active");
        });
        getQtyByColorSize(product_id, color_name, productSize);
		getProductBarcode(product_id, color_name, productSize);
        // getSizeByColor(color_name);
        $('#colorName').on('change', function (e) {
            var color_name = e.target.value;
            //  alert(color_name);
            getSizeByColor(color_name);
        });

        $('#pqty').on('change', function (e) {
            var pqty = e.target.value;
            $('#productQty').val(pqty);
        });
        $('.swatch-option').on('click', function (e) {
			var productSize = $('#product-size-input').val();
		//	alert(productSize);
            getQtyByColorSize(product_id, color_name, productSize);
			getProductBarcode(product_id, color_name, productSize);
			var location = $('.list-group-item').val();	
			//alert(location);
        });
        $('#colorName').on('change', function (e) {
            var productColor = e.target.value;
            $('#productColor').val(productColor);
        });

        function getSizeByColor(color_name) {
            var url_op = base_url + "/getSizeByColor/" + product_id + '/' + color_name;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data)
                {
                    //   alert(data);
                    $('#ProductSizeList').empty();
                    $('#ProductSizeList').append('<option value="">Select Size</option>');
                    $.each(data, function (index, subcatobj) {
                        //   $('#ProductSizeList').append('<option value="' + subcatobj.productsize_id + '">' + subcatobj.productsize_size + '</option>');

                        $('#ProductSizeList').append('<option value="' + subcatobj.productsize_size + '">' + subcatobj.productsize_size + '</option>');
                    });
                }
            });
        }

        function  getQtyByColorSize(product_id, color_name, productSize) {
            var url_op = base_url + "/ajaxcall-getQuantityByColor/" + product_id + '/' + productSize + '/' + color_name;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (qty) {
                    //alert(html);
                    //var qty = html[0].SizeWiseQty;
                   // alert(qty);
                    if (qty > 0) {
                        $('#qty').attr({"max": qty-amountToCart()});
						$("#sold_out_msg").html(" ");
						$('#qty').val(1);
                        document.getElementById("add-cart").disabled = false;
                    } else {
						$('#qty').val(0);
						$('#qty').attr({"max": 0});
						$("#sold_out_msg").html("<span style='color:red;font-weight:700;padding-bottom:10px;'>" + productSize + " size has sold out. Please select another size.</span>");
						document.getElementById("add-cart").disabled = true;
					}
                }
            });
        }
		var url_op_total_qty = base_url + "/GetTotalForAlert/" + product_id;
        $.ajax({
            url: url_op_total_qty,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (html) {
               //  alert(html);
                if (html > 0) {

                } else {
                    alert('This product has sold out');
                    $("#sold_out_msg").html('');
                    $("#ProductQty").find('option').remove();
                    $("#ProductQty").append('<option value="' + 0 + '">' + 0 + '</option>');
                    $("#total_sold_out_msg").html("<span style='color:red;font-weight:700;padding-bottom:10px;'>This product has sold out.</span>");
                }
            }
        });
		
		function getProductBarcode(product_id, color_name, productSize)
        {
            var url_op = base_url + "/ajaxcall-getBarcode/" + product_id + '/' + productSize + '/' + color_name;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (html) {
                if(html !=''){
                       $(".product_barcode").show();
                       $("#barcode").html(html);
                    }
                }
            });
        }
        
		$('.simple').on('click', function (e) {
            //alert('ok');
            $('.searchstore').css('display', 'inline-block');
            $('.find_store_close').show();
			return false;
        });
        $(".store-details").mouseenter(function () {
                $(this).children(".stockmessage").removeClass('hide');
            });
            $(".store-details").mouseleave(function () {
               $(this).children(".stockmessage").addClass('hide');
        });
        $(".find_store_close").on('click',function(){
            $('.searchstore').css('display', 'none');
            $('.find_store_close').hide();
        });
        
        function getStore(text) {
		var barcode = document.getElementById('barcode').textContent;
		document.getElementById('store-list-loading').style.display = "block";
		
		$.ajax({url: base_url+"/store-list/"+text+"/"+barcode, success: function(html){
			if(text==null || text=="") {
				alert('Error');
			} else {
			    document.getElementById('store-list-loading').style.display = "none";
				document.getElementById('store-list-container').innerHTML = html;
				
				$('.simple').on('click', function (e) {
            
            $('.searchstore').css('display', 'inline-block');
            $('.find_store_close').show();
            e.preventDefault();
            });
            $(".store-details").mouseenter(function () {
                    $(this).children(".stockmessage").removeClass('hide');
                });
                $(".store-details").mouseleave(function () {
                   $(this).children(".stockmessage").addClass('hide');
            });
            $(".find_store_close").on('click',function(){
                $('.searchstore').css('display', 'none');
                $('.find_store_close').hide();
            });
			}
		}});
		document.getElementById('select-district').innerHTML = text + ' <span class="caret"></span>';
		
	   }
    });
</script>
<script>
var timer1;
var timer2;
var counter=1;
var running = false;
(function() {
	
})();
function updateActive(items, i) {
	i = i%(items.length);
	items[i>0?i-1:items.length-1].classList.remove('active');
	items[i].classList.add('active');
	counter++;
	if(!running) {
    	timer2 = setInterval(function() {
    		updateActive(items, counter);
    	}, 1500);
    	running = true;
    	clearInterval(timer1);
	}
}
function fadeImages(element) {
	var items = element.getElementsByClassName('item');
	for (var i = 0; i < items.length; i++) {
		items[i].classList.remove('active');
	}
	items[1].classList.add('active');
	timer1 = setInterval(function() {
		updateActive(items, counter);
	}, 100);
}
function removeTimer(element) {
    clearInterval(timer1);
	clearInterval(timer2);
	counter=1;
	var items = element.getElementsByClassName('item');
	for (var i = 0; i < items.length; i++) {
		items[i].classList.remove('active');
	}
	items[0].classList.add('active');
	running = false;
}

/* Open filter category */
(function(){
 //   document.getElementById('filter-category').classList.add('active');
})();
</script>
<script>
	function getStore(text) {
		var barcode = document.getElementById('barcode').textContent;
		var get_style_name = "{{$singleproduct->product_styleref}}";
        var stylecode = get_style_name.replace("/", "_");
        
		document.getElementById('store-list-loading').style.display = "block";
		var no_data = document.getElementById('no-data');
		if(no_data) {
		    no_data.style.display = "none";
		}
		
		var address = '';
		
		if(barcode) {
		    address = base_url+"/store-list/"+text+"/"+barcode;
		} else {
		    address = base_url+"/showroom-wise-stock-by-designref/"+text+"/"+stylecode;
		}
		
		jQuery.ajax({url: address, success: function(html){
			if(text==null || text=="") {
				alert('Error');
			} else {
			    document.getElementById('store-list-loading').style.display = "none";
				document.getElementById('store-list-container').innerHTML = html;
				
				jQuery('.simple').on('click', function (e) {
            
            jQuery('.searchstore').css('display', 'inline-block');
            jQuery('.find_store_close').show();
            e.preventDefault();
            });
            jQuery(".store-details").mouseenter(function () {
                    jQuery(this).children(".stockmessage").removeClass('hide');
                });
                jQuery(".store-details").mouseleave(function () {
                   jQuery(this).children(".stockmessage").addClass('hide');
            });
            jQuery(".find_store_close").on('click',function(){
                jQuery('.searchstore').css('display', 'none');
                jQuery('.find_store_close').hide();
            });
			}
		}});
		document.getElementById('select-district').innerHTML = text + ' <span class="caret"></span>';
		
	   }
	   function amountToCart() {
        var size = jQuery(".swatch-option.text.active").text();
	    @php($reduction_amount = 0)
        @php($CartItems = Cart::instance('products')->content())
        @foreach($CartItems as $item)
        <?php $color=($item->options->has('color') ? $item->options->color : '');?>
        <?php $size=($item->options->has('size') ? $item->options->size : ''); ?>
        @if($item->id == $singleproduct->id && $color == $product_color)
        if(size == "{{ $size }}")
        @php($reduction_amount = $item->qty)
        return {{ $reduction_amount }};
        @endif
        @endforeach
        return 0;
	}
</script>
<script>
    (function(){
        document.getElementById("loading-image").outerHTML='<div id="loading-image-removed"></div>';
    })();
    function startLoading(){
      //  var amount=document.getElementById("qty").getAttribute("max");
      //  if(amount - document.getElementById("qty").value >= 0)
        //    document.getElementById("loading-image-removed").outerHTML='<div id="loading-image"><img src="https://pride-limited.com/storage/app/public/loader.gif" Alt="Loading..." /></div>';
    }
</script>
@endsection