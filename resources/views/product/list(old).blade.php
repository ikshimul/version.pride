@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<?php
use App\Http\Controllers\product\ProductController;
use App\Models\Productalbum;
use App\Models\Subprocat;
?>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/list.css')}}">
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="itemhome">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item">
                                <a href="#" title="Main category"><?php echo $main_cate; ?></a>
                            </li>
                            <li class="item">
                                <a href="#" title="Sub category"><?php echo $title; ?></a>
                            </li>
                            <!--<li class="item">-->
                            <!--    <a href="#" title="Child sub category"><?php echo str_replace('-', '/', $last); ?></a>-->
                            <!--</li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-banner">
                        <div class="block-content">
                            <a href="#" class="opener-filter">filters</a>
                            <div class="filter-holder">
                                <div class="filter-content">
                                    <ul class="list-inline">
                                        <li id='filter-category'>
                                            <a class="opener-cate" href="#"> Category <span class="icon-down"></span></a> 
                                            <div class="filter-block">
                                                @if(trim($main_cate) == 'women')
                                                <strong><a href="{{url("/signature/signature-sari/9/signature")}}" ><span>Pride Signature</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <?php foreach(Subprocat::wherein('procat_id',[9,16])->orderBy('id','DESC')->get() as $menu){
                                                        $subproname=str_replace('-', '_', $menu->subprocat_name);
                                                        $subproname=str_replace(' ', '-', $subproname);
                                                        $subproname=strtolower($subproname);
                                                    ?>
                                                       <li  class="item"><a class="<?php if($menu->id ==$subcatid) echo 'active'; ?>" href="{{url("/women/signature/{$subproname}")}}" ><span>{{$menu->subprocat_name}}</span></a></li>
                                                    <?php } ?>
                                                </ol>
                                                <strong><a href="#" ><span>Pride Classic</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <?php foreach(Subprocat::where('procat_id',7)->get() as $menu){
                                                        $subproname=str_replace('-', '_', $menu->subprocat_name);
                                                        $subproname=str_replace(' ', '-', $subproname);
                                                        $subproname=strtolower($subproname);
                                                        ?>
                                                       <li  class="item"><a class="<?php if($menu->id ==$subcatid) echo 'active'; ?>" href="{{url("/women/classic/{$subproname}")}}" ><span>{{$menu->subprocat_name}}</span></a></li>
                                                    <?php } ?>
                                                </ol>
                                                <strong><a href="{{url("/pride-girls/all/5/pride-girls")}}" ><span>Pride Girls</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <?php foreach(Subprocat::where('procat_id',5)->get() as $menu){
                                                          $subproname=str_replace('-', '_', $menu->subprocat_name);
                                                          $subproname=str_replace(' ', '-', $subproname);
                                                          $subproname=strtolower($subproname);
                                                        ?>
                                                        <li  class="item"><a class="<?php if($menu->id ==$subcatid) echo 'active'; ?>" href="{{url("/women/pride-girls/{$subproname}")}}" ><span>{{$menu->subprocat_name}}</span></a></li>
                                                    <?php } ?>
                                                </ol>
                                                <strong><a href="#"><span>Collection</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <!--<li  class="item"><a class="#" href="{{url('/eid-collection-19/woman/9')}}" ><span>Eid Collection 19</span></a></li>-->
                                                   <!-- <li  class="item"><a href="{{url('/capsule-collection-19/woman/9')}}" ><span>Capsule Collection 2019</span></a></li> -->
                                                    <li  class="item"><a href="{{url('/summer-collection-19/woman/9')}}" ><span>Summer Collection 2020</span></a></li>
                                                    <!--<li  class="item"><a class="#" href="{{url('/falgun-collection-2019/woman/9')}}" ><span>Falgun Collection 18</span></a></li>
                                                    <!--<li  class="item"><a class="#" href="#" ><span>Boishakh 1425</span></a></li>
                                                    <li  class="item"><a class="#" href="#" ><span>Puja 18</span></a></li>-->
                                                </ol>
                                                @endif
                                                @if(trim($main_cate) == 'men')
                                                <strong><a href="{{url("/athenic-men/panjabi/17/all-panjabi")}}"><span>Panjabi</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <?php foreach(Subprocat::wherein('procat_id',[17,18])->where('deleted_status',0)->get() as $menu){
                                                          $subproname=str_replace('-', '_', $menu->subprocat_name);
                                                          $subproname=str_replace(' ', '-', $subproname);
                                                          $subproname=strtolower($subproname);
                                                    ?>
                                                      <li  class="item"><a class="<?php if($menu->id ==$subcatid) echo 'active'; ?>" href="{{url("/men/panjabi/{$subproname}")}}" ><span>{{ ucwords($menu->subprocat_name) }}</span></a></li>
                                                    <?php } ?>
                                                </ol>
                                                <strong><a href="#"><span>Collection</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <!--<li  class="item"><a class="<?php if($subcatid == 'man' && $subcategory == 17 ) echo 'active'; ?>" href="{{url('/eid-collection-19/man/17')}}" ><span>Eid Collection' 19</span></a></li>
                                                   <!-- <li  class="item"><a class="<?php if($subcatid == 'ethnic-menswear' && $subcategory == 17 ) echo 'active'; ?>" href="{{url('/eid-collection-201/ethnic-menswear/17')}}" ><span>Eid Collection 18</span></a></li>
                                                    <li  class="item"><a class="<?php if($subcatid == 'ethnic-menswear' && $subcategory == 17 ) echo 'active'; ?>" href="{{url('/puja-collection-2018/ethnic-menswear/17')}}" ><span>Puja Collection</span></a></li>-->
                                                </ol>
                                                @endif
                                                @if(trim($main_cate) == 'kids')
                                                <strong><a href="{{url("/kids/girls/6/all-girls")}}" ><span>Girls</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <?php foreach(Subprocat::wherein('procat_id',[6])->where('deleted_status',0)->get() as $menu){
                                                          $subproname=str_replace('-', '_', $menu->subprocat_name);
                                                          $subproname=str_replace(' ', '-', $subproname);
                                                          $subproname=strtolower($subproname);
                                                    ?>
                                                    <li  class="item"><a class="<?php if($menu->id ==$subcatid) echo 'active'; ?>" href="{{url("/kids/girl/{$subproname}")}}" ><span>{{ ucwords($menu->subprocat_name) }}</span></a></li>
                                                    <?php } ?>
                                                </ol>
                                                <strong><a href="{{url('/kids/boys/6/39')}}"><span>Boys</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <?php foreach(Subprocat::wherein('procat_id',[21])->get() as $menu){
                                                          $subproname=str_replace('-', '_', $menu->subprocat_name);
                                                          $subproname=str_replace(' ', '-', $subproname);
                                                          $subproname=strtolower($subproname);
                                                    ?>
                                                      <li class="item"><a class="<?php if($menu->id ==$subcatid) echo 'active'; ?>" href="{{url("/kids/boys/{$subproname}")}}" ><span>{{ ucwords($menu->subprocat_name) }}</span></a></li>
                                                    <?php } ?>
                                                </ol>
                                                <strong><a href="#"><span>Collection</span></a></strong>
                                                <ol class="m-filter-item-list">
                                                    <!--<li  class="item"><a class="<?php if($subcatid == 'kids' && $subcategory == 6 ) echo 'active'; ?>" href="{{url('/eid-collection-19/kids/6')}}" ><span>Eid Collection' 19</span></a></li>
                                                    <!--<li class="item"><a class="" href="#" ><span>Boishakh 1425</span></a></li>
                                                    <li class="item"><a class="" href="{{url('/eid-collection-201/pride-kids/6')}}" ><span>Eid Collection</span></a></li>
                                                    <li class="item"><a class="" href="{{url('/boishakh-1425/pride-kids/6')}}" ><span>Boishakh 1425</span></a></li>-->
                                                </ol>
                                                @endif
                                            </div>
                                        </li>
                                       
                                    </ul>
                                </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <input name="form_key" type="hidden" value="" />
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
                </div>



                <style>
                    .product-items .product-item-photo .product-image-wrapper {
                        display: block;
                        overflow: hidden;
                        position: relative;
                        border: 0px solid #00000012;
                    }
                </style>
                <div class="products wrapper grid products-grid">
                    <ol class="products list items product-items">
                        @foreach($product_list as $product)
                        <?php
                        $product_name = str_replace(' ', '-', $product->product_name);
                        $product_url = strtolower($product_name);
                       // $data = ProductController::GetProductColorAlbum($product->product_id);
                       $pro_album = Productalbum::where('product_id',$product->id)->orderBy('id','DESC')->first();
                        // dd($data);
                        $sold_out = ProductController::ProductWiseQty($product->id);
                      //  foreach ($data as $pro_album) {
                        //    $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
                       // }
                        ?>
                        <li class="item product product-item">    
                           <?php
                            date_default_timezone_set('Asia/Dhaka');
    						$today=date('d-m-Y');
    						$insert_date=$product->created_at;
    						$datetime1 = new DateTime($today);
    						$datetime2 = new DateTime($insert_date);
    						$interval = $datetime1->diff($datetime2);
    						$date_difference=$interval->format('%a');
    						$color_album=str_replace('/','-',$pro_album->productalbum_name);
                           if($sold_out <= 0){ ?>
                             <!--<span class="sprice-tag">Sold Out</span> --->
                             <span class="sold-out">Sold Out</span>
                           <?php }else if($product->product_pricediscounted > 1){ ?>
							   <span class="sprice-tag"><?php echo $product->product_pricediscounted;?>% Off</span>
						   <?php  } if($date_difference < 31){ ?>
                             <div class="tag_container round_tag_lt bg_red t_white"><span class="ttl_header">New</span></div> 
                           <?php } ?>
                            <div class="product-item-info" data-container="product-grid">
                                <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}">
                                    <span class="product-image-container">
                                        <span class="product-image-wrapper" >
                                            <span class="custom-carousel" onmouseenter="fadeImages(this)" onmouseleave="removeTimer(this)">
                                                <?php $images = ProductController::GetProductImageByColorAlbum($pro_album->id); ?>
                                                @php($i = 0)
                                                @foreach($images as $image)
                                                <img class="item large_img<?php if($i==0) echo ' active';?> swatch_img_<?php echo $product->id;?>" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $image->productimg_img_medium }}" alt="No Image Found"/>
                                                @php($i++)
                                                @endforeach
                                            </span>
                                        </span>
                                    </span>
                                </a>
                                <div class="product details product-item-details">
                                    <div class="info-holder">
                                        <strong class="product name product-item-name">
                                            <a class="product-item-link"
                                               href="{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}">
                                                {{$product->product_name}}
                                            </a>
                                        </strong>
                                        <!-- swatch --->
                                        <?php
										$data = ProductController::GetProductColorAlbum($product->id);
										?>
										<div class="ws_100 swatch round show_desktop show_tablet">
											<ul class="color">
												<?php
												$i = 0;
												$data = ProductController::GetProductColorAlbum($product->id);
												foreach ($data as $pro_album) {
													$i++;
													//echo '=='.$pro_album->productalbum_id;
													$colorwiseimg = ProductController::GetProductImageSingleByColorAlbum($pro_album->id);
													?>
													<li>
														<span class="<?php
														if ($i == 1) {
															echo 'selected';
														}
														?>">
															<input type="hidden" id="color_album" value="{{$product->productalbum_name}}"/>
															<input type="hidden" id="product_id" value="{{$product->id}}"/>
															<img  id="{{$product->id}}" rel="{{ URL::to('') }}/storage/app/public/pgallery/{{$colorwiseimg->productimg_img}}" src="{{ URL::to('') }}/storage/app/public/pgallery/{{$pro_album->productalbum_img}}" data-color="01" data-modal-target="#quickview_pop">
														</span>
													</li>
												<?php } ?>
											</ul>
										</div>
										 <!-- swatch --->
                                    </div>
                                    <div class="info-holder">
                                       <?php if ($product->product_pricediscounted < 1) { ?>
										<div class="price-box price-final_price" data-role="priceBox">
											<span class="price-container price-final_price tax weee">
												<span class="price-label">Regular Price</span>
												<span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
													<span class="price">Tk &nbsp;{{$product->product_price}}</span>
												</span>
											</span>
										</div>
										<?php }else{?>
										<div class="price-box price-final_price" data-role="priceBox"  data-price-box="product-id-84726">
											<span class="normal-price">
												<span class="price-container price-final_price tax weee">
														<span  data-price-type="finalPrice" class="price-wrapper ">
															<span class="price">Tk &nbsp;{{$product->discount_product_price}}</span>
														</span>
												</span>
											</span>
										</div>                                                            
										<div class="price-box price-final_price" data-role="priceBox">
												<span class="old-price">
													<span class="price-container price-final_price tax weee">
														<span class="price-label">Regular Price</span>
														<span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
															<span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
													</span>
												</span>
										</div>
									<?php } ?>                                                      
                                        <div class="product-item-inner">
                                            <div class="product actions product-item-actions">
                                                <div class="actions-primary">
                                                    <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}" class="action tocart primary"><span>Shop Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                </div>
				<div class="pages">
					<center>  {{{ $product_list->links() }}} </center>
				</div>
            </div>
        </div>
    </div>
</main>
<hr>
<div class="col-md-1">&nbsp;</div>
@if(trim($main_cate) == '0000')
<div class="col-md-10">
    <div class="category-view">
        <div class="container">
            <div class="row">
                <div class="col-md-12">    
                    <div class="category-cms">
                        <section class="gallery-page">
                            <header class="head"></header>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="gallery-holder">
                                        <div class="img"><a href="{{url('/popart-collection')}}">
                                                <img src="{{url('/')}}/storage/app/public/popart/pop_p_1.jpg" alt=""></a>
                                            <div class="info">
                                                <h2>Catalogues</h2>
                                                <a class="btn" href="{{url('/popart-collection')}}">Pop Art Collection</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-1">&nbsp;</div>
@endif

@endsection
@section('appended.script')
<script>
var timer1;
var timer2;
var counter=1;
var running = false;
(function() {
	
})();
function updateActive(items, i) {
	i = i%(items.length);
	items[i>0?i-1:items.length-1].classList.remove('active');
	items[i].classList.add('active');
	counter++;
	if(!running) {
    	timer2 = setInterval(function() {
    		updateActive(items, counter);
    	}, 1500);
    	running = true;
    	clearInterval(timer1);
	}
}
function fadeImages(element) {
	var items = element.getElementsByClassName('item');
	for (var i = 0; i < items.length; i++) {
		items[i].classList.remove('active');
	}
	items[1].classList.add('active');
	timer1 = setInterval(function() {
		updateActive(items, counter);
	}, 100);
}
function removeTimer(element) {
    clearInterval(timer1);
	clearInterval(timer2);
	counter=1;
	var items = element.getElementsByClassName('item');
	for (var i = 0; i < items.length; i++) {
		items[i].classList.remove('active');
	}
	items[0].classList.add('active');
	running = false;
}
jQuery('.show_tablet img').click(function () {
	var product_id = jQuery(this).attr('id');
	 //alert(product_id);
	// console.log(jQuery(this));
	jQuery('.swatch_img_'+ product_id).attr('src', jQuery(this).attr('rel'));
	//jQuery(this).parents('div:eq(0)').parents('div:eq(0)').find('.fancybox').attr('href', jQuery(this).attr('rel'));
	return false;
});
/* Open filter category */
(function(){
    <?php if(!((isset($lower_price) && $lower_price !=0) || (isset($upper_price) && $upper_price !=0) || (isset($size) && $size != null) || (isset($color_name) && $color_name != null)))  {?>
        document.getElementById('filter-category').classList.add('active');
    <?php } elseif((isset($lower_price) || isset($upper_price)) && ($lower_price != 0) && ($upper_price != 0)) { echo 'console.log("'.$lower_price.' and '.$upper_price.'");';?>
        document.getElementById('filter-price').classList.add('active');
    <?php } elseif(isset($size) && $size != null) { ?>
        document.getElementById('filter-size').classList.add('active');
    <?php } elseif(isset($color_name) && $color_name != null) { ?>
        document.getElementById('filter-color').classList.add('active');
    <?php } elseif(isset($fabric) && $fabric != null) { ?>
        document.getElementById('filter-fabric').classList.add('active');
    <?php }?>
})();
</script>
@endsection