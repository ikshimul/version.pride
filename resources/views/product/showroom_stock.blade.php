<ul class="store-list">
	<?php
	    
		$item_number = 5;
		for($i = 0; $i<$item_number && $i<count($showroom_stock); $i++) {
			$stock = $showroom_stock[$i];
		?>
		<li class="store-tile">
			<div class="store-address">
				<a href="#"><span class="heading-name"><?php echo $stock->SalesPointName;?></span></a><br>
				<?php echo $stock->SPAddress;?><br>
				<?php echo $stock->city;?>
			</div>
			<div class="store-details">
			  <?php if($stock->CurrentStock <= 0){ ?>
			  <span class="outofstock" data-status="1">not available</span><br>
			  <span class="stockmessage outofstock-msg hide">Ship to FREE for when you <br> select 'in Store Pick Up' at checkout (selected stores only)</span>
    		  <?php }elseif($stock->CurrentStock <= 2){ ?>
    		    <span class="lowstock findstore" data-status="2">low stock</span><br>
    			<span class="hide stockmessage instock-msg">Availability at store is not guaranteed. Inventory status refreshed hourly.</span><br>
    		  <?php }else{ ?>
    			<span class="instock" data-status="3">available</span><br>
    			<span class="hide stockmessage instock-msg">Availability at store is not guaranteed. Inventory status refreshed hourly.</span><br>
    		  <?php } ?>
    			
    			<span class="opening"><?php echo $stock->OpeningTime;?> - <?php echo $stock->ClosingTime;?></span>
			</div>
		</li>
	<?php } ?>
	@if($item_number<count($showroom_stock))
	<span class="morestore more" data-toggle="collapse" data-target="#collapse">More</span>
	<div  id="collapse" class="collapse" class="more-store-details hide">
		<?php
		for(;$i<count($showroom_stock); $i++) {
			$stock = $showroom_stock[$i];
		?>
		<li class="store-tile">
			<div class="store-address">
				<a href="#"><span class="heading-name"><?php echo $stock->SalesPointName;?></span></a><br>
				<?php echo $stock->SPAddress;?><br>
				<?php echo $stock->city;?>
			</div>
			<div class="store-details">
				<span class="instock" data-status="3">available</span><br>
				<span class="hide stockmessage instock-msg">Availability at store is not guaranteed. Inventory status refreshed hourly.</span><br>
				<span class="opening"><?php echo $stock->OpeningTime;?> - <?php echo $stock->ClosingTime;?></span>
			</div>
		</li>
	<?php } ?>
	@if($item_number<count($showroom_stock))
	<a href="#find-store-section"><span class="morestore more" data-toggle="collapse" data-target="#collapse">Less</span></a>
	@endif
	</div>
	@endif
	
	@if(count($showroom_stock)<1)
	<div id="no-data" style="text-align:center; color:red;">No data available</div>
	@endif
</ul>