@extends('layouts.app')
@section('title','Login | Register')
@section('content')
<style>
 label {
    display: inline-block;
    margin-bottom: .5rem;
    color: black;
    font-weight: 600;
    font-size:11pt;
}
.shop-page-title .h1 {
    /* margin: 13px; */
    padding-left: 17px;
    font-size: 15pt;
    color: #333;
    text-align: center;
}
/*.form-control {
            padding: .5em 8px !important;
            border-radius: 0;
            border: none;
            border: 1px solid;
            color: #999;
            background-color: transparent;
            outline: none!important;
            box-shadow: none!important;
        } */
.modal {
  text-align: center;
  background:transparent;
  background: transparent;
    box-shadow: none;
    border: none;
}

@media screen and (min-width: 768px) { 
  .modal:before {
    display: inline-block;
    vertical-align: middle;
    content: " ";
    height: 100%;
  }
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
  text-align:center;
}
.order-form {
	margin-bottom:0;
	color:#333;
}
hr {
	margin-top:20px;
	margin-bottom:20px;
}
.mb-1 {
	margin-bottom:0.25rem;
}
.mb-2 {
	margin-bottom:0.5rem;
}
.mb-3 {
	margin-bottom:1rem;
}
.form-group {
    height:68px;
}
.btn-sm {
    height:33px;
    font-size:12px;
}
.page-main {
    min-height:67vh;
}
.page-main {
    padding-top:64px;
}
@media screen and (max-width: 992px) {
    .page-main {
        padding-top:0;
    }
}
.btn-social {
    font-size:16px;
}
.form-control{
    font-size: 11pt;
}
.with-errors{
    color:red;
}
.abcRioButtonBlue {
    background-color: #291e88;
    border: none;
    color: #fff;
}
.btn-facebook {
    color: #fff;
    background-color: #291e88;
    border-color: rgba(0,0,0,0.2);
}
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item">
                                <a href="" title="">Login | Register</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="shop-page-title">
            @if (\Session::has('message'))
                <div class="">
                    <ul class="list-group">
                        <li class="text-center list-group-item list-group-item-{!! \Session::get('class') !!}">{!! \Session::get('message') !!}</li>
                    </ul>
                </div>
            @endif
             <center>
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
             <center>
            @if (session('error'))
            <div class="alert alert-success">
                {{ session('error') }}
            </div>
            @endif
        </center>
        </div>
        <div class="row" style="padding-bottom:13px;">
            <div class="col-xs-12 col-md-6 col-lg-offset-1 col-md-offset-3 col-lg-4">
                <form class="order-form js-login-form" action="{{ route('login') }}" method="POST" data-toggle="validator" autocomplete="off">
                    {{ csrf_field() }}
                    <input type="hidden" name="url" value="{{URL::previous()}}"/>
					<div class="row">
						<div class="row" style="height:40px"><hr style="border-color:#aaa">
						<div style="position:absolute; left:50%;top:0; transform:translate(-50%, 0); height:40px; line-height:40px; background:#FFF; font-size:24px; padding:0 20px; white-space:nowrap">LOGIN HERE</div></div>
					</div>
					<!--<div id="token_div" style="display: none;">-->
     <!--                  <input type="hidden" id="device_token" name="device_token"/>-->
     <!--               </div>-->
     <!--                 <div id="permission_div" style="display: none;">-->
     <!--                   <h4>Needs Permission</h4>-->
     <!--                   <p id="token"></p>-->
     <!--                   <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored"-->
     <!--                       onclick="requestPermission()">Request Permission</button>-->
     <!--                 </div>-->
     <!--                <div id="messages"></div>-->
                    <div class="row" style="margin-top:30px;">
                        @if (session('failed'))
                        <center>
                            <div class="help-block with-errors">{{session('failed')}}
                            </div>
                        </center>
                        @endif 
                      <div class="col-sm-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                        	@if ($errors->has('email'))
							<div class="help-block with-errors">
								{{ $errors->first('email') }}
							</div>
							@endif
						</div>
						<div class="form-group col-sm-12 {{ $errors->has('email') ? ' has-error' : '' }}{{session('failed') ? ' has-error' :''}}">
							<label>E-mail<sup style="color:red;">*</sup></label>
							<input class="form-control" type="email" name="email" value="{{ old('email') }}" data-error="Enter your password" autocomplete="email" style="font-family: myriad-pro;" placeholder="Email Address" required autofocus>
						
						</div>
                    </div>
					<div class="row">
						<div class="form-group col-sm-12 {{ $errors->has('password') ? ' has-error' : '' }}">
							<label>Password<sup style="color:red;">*</sup></label>
							<input class="form-control" id="password" type="password" name="password"  data-minlength="4" data-error="Minimum of 4 characters" style="font-family: myriad-pro;" placeholder="Password" required>
							@if ($errors->has('password'))
							<div class="help-block with-errors">
								{{ $errors->first('password') }}
							</div>
							@endif
						</div>
                    </div>
                    <button class="order-form__submit btn btn-shop d-block mx-auto my-4 btn-sm" style="max-width:100%; background-image:linear-gradient(to bottom, #291e88, #291e88, #291e88); color:#FFF" type="submit">login</button>
					<div class="row mb-3" style="margin-top:16px">
                        <div class="col-xs-6 mb-2">
                            <label class="d-inline-flex align-items-center pointer m-0 custom-control custom-checkbox">
                                <input style="margin-right:10px" class="custom-control-input" type="checkbox"  name="remember" {{ old('remember') ? 'checked' : '' }}><span class="d-inline-block mr-2 custom-control-indicator"></span><span class="custom-control-description">Remember me</span>
                            </label>
                        </div>
                        <div class="col-xs-6 mb-2">
                            <p class="mb-1"><a class="order-form__forgot d-inline-block" style="line-height:26px; float:right" href="{{url('password/reset')}}" data-toggle="modal"><span>Forgot your password?</span></a></p>
                        </div>
                    </div>
                </form>
            </div>
			<div class="col-xs-12 col-md-6 col-lg-offset-2 col-md-offset-3 col-lg-4 clearfix">
                <form class="order-form js-login-form" action="{{ route('user.create.form') }}" method="POST" onsubmit="showLoadingImage()" autocomplete="off">
                    {{ csrf_field() }}
					<div class="row">
						<div class="row" style="height:40px"><hr style="border-color:#aaa"><div style="position:absolute; left:50%;top:0; transform:translate(-50%, 0); height:40px; line-height:40px; background:#FFF; font-size:24px; padding:0 20px; white-space:nowrap;">NEW ACCOUNT</div>
						</div>
					</div>
					<div class="row" style="margin-top:30px;height:83px">
						<div class="form-group col-sm-12" style="height:59px; padding-top:15px; font-size:16px">
							Create an account to take advantage of faster and easier shopping.
						</div>
					</div>
                    <div class="row">
                        <div class="form-group col-xs-12 col-lg-12 {{ $errors->has('regi_email') ? ' has-error' : '' }}">
                            <label>Email<sup style="color:red;">*</sup></label>
                            <input class="form-control" type="email" name="regi_email"  value="{{ old('regi_email') }}" required  autocomplete="email" style="font-family: myriad-pro;" placeholder="Email Address"/>
                            <div class="help-block with-errors">{{ $errors->first('regi_email') }}</div>
                        </div>
                    </div>
                    <button class="order-form__submit btn btn-shop mx-auto btn-block btn-sm" style="max-width:100%; background-image:linear-gradient(to bottom, #291e88, #291e88, #291e88); color:#FFF" type="submit">Register</button>
                </form>
                <!--
                 <br>
                <div style="height:40px"><hr style="border-color:#aaa"><div style="position:absolute; left:50%;top:285px; transform:translate(-50%, 0); height:40px; line-height:40px; background:#FFF; font-size:24px; padding:0 20px; white-space:nowrap">OR</div></div>
                
                --->
            </div>
        </div>
        
      
       <div class="row">
            <div class="col-xs-12">
                <div style="height:24px"><hr style="border-color:#aaa"><div style="position:absolute; left:50%;top:0; transform:translate(-50%, 0); height:40px; line-height:40px; background:#FFF; font-size:24px; padding:0 20px; white-space:nowrap">OR</div></div>
				<center>
				     <br>
				<div class="col-offset-4 col-md-4">
				</div>
				<div class="col-md-4">
                <a class="order-form__submit btn btn-shop mx-auto btn-block btn-sm" style="max-width:100%; background-image:linear-gradient(to bottom, #291e88, #291e88, #291e88); color:#FFF;text-transform: uppercase;letter-spacing: 2px;font-size: 13px;" href="{{url('/guest-checkout')}}">Checkout as a Guest</a>
				</div>
				</center>
            </div>
        </div>
      
        <div class="row">
            <br>
            <div class="col-xs-12">
                <div style="height:40px"><hr style="border-color:#aaa"><div style="position:absolute; left:50%;top:0; transform:translate(-50%, 0); height:40px; line-height:40px; background:#FFF; font-size:24px; padding:0 20px; white-space:nowrap">OR</div></div>
            </div>
        </div>
         <div class="row">
		  <div class="col-sm-12 text-center">
            <div class="col-sm-12">
                 <a href="javascript:void(0);" onclick="window.open('https://version.pride-limited.com/auth/facebook', '_blank', 'width=800, height=700')" class="btn btn-social btn-facebook">
                    <span class="fa fa-facebook"></span> Sign in with facebook
                </a>
            </div>
            <div class="col-sm-12">
                 <div style="display:inline-block; margin-top:10px" id="my-signin2" data-onsuccess="onSignIn" data-theme="dark" onClick="if(form) form.submit(); submitted = true;"></div>
            </div>
		<!--	<div class="col-sm-12" style="padding-top: 12px;">
				<div id="my-signin2"></div>
            </div> --->
			</div>
            <!--<div class="col-sm-6">
                <a href="{{url('/guest-checkout')}}" class="order-form__submit btn btn-shop mx-auto btn-block btn-sm" style="max-width:100%; background-image:linear-gradient(to bottom, #666, #111, #000); color:#FFF;font-weight: 400;line-height: 1.42857143;font-size: 12px;">
                    Checkout as a Guest
                </a>
            </div> --->
        </div>
    </div>
</main>

@guest

<script>
        var form;
        var submitted = false;
      function onSignIn(googleUser) {
		var profile = googleUser.getBasicProfile();
		var user = {
			'name': profile.getName(),
			'given_name': profile.getGivenName(),
			'family_name': profile.getFamilyName(),
			'image': profile.getImageUrl(),
			'email': profile.getEmail()
		}
		var f = document.createElement('form');
		f.action='https://pride-limited.com/google-login';
		f.method='POST';

		var i=document.createElement('input');
		i.type='hidden';
		i.name='profile';
		i.value=JSON.stringify(user);
		f.appendChild(i);
		
		i=document.createElement('input');
		i.type='hidden';
		i.name='_token';
		i.value='{{ csrf_token() }}';
		f.appendChild(i);
		
		i=document.createElement('input');
		i.type='hidden';
		i.name='social_id';
		i.value=profile.getId();
		f.appendChild(i);
		
		b = document.body;
		document.body.appendChild(f);
		form = f;
		if(submitted)
		    f.submit();
      }
        function renderButton() {
			gapi.signin2.render('my-signin2', {
				'scope': 'profile email',
				'width': 198,
				'height': 35,
				'longtitle': true,
				'theme': 'dark',
				'onsuccess': onSignIn,
			});
		}
</script>
<script src="https://apis.google.com/js/platform.js?onload=renderButton"></script>
@endguest

@endsection
