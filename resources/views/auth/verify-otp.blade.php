@extends('layouts.app')
@section('title','Otp Verify')
@section('content')
<link  rel="stylesheet"  href="{{asset('assets/css/mobile-signup.css')}}" />
<main id="maincontent" class="page-main">
    <!--<a id="contentarea" tabindex="-1"></a>-->
    <!--<div class="beadcumarea">-->
    <!--    <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">-->
    <!--                    <ul class="items">-->
    <!--                        <li class="item home">-->
    <!--                            <a href="{{url('/')}}" title="Go to Home Page">Home</a>-->
    <!--                        </li>-->
    <!--                        <li class="item">-->
    <!--                            <a href="" title="">OTP Verify</a>-->
    <!--                        </li>-->
    <!--                    </ul>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
    <div class="main">
    	<section class="module">
    	    <div class="container">
    	      <div class="section-signup">
                  <div class="signin-page">
                    <div class="container">
                        <div class="blocks-wrap">
                            <div class="block">
                                <div class="sign-info">
                                     <h1 class="page-title">Just seconds to create your Pride<sup>&#174;</sup> account!</h1>
                                    <p></p>
                                    <!--<img class="sign-image" src="{{url('/')}}/storage/app/public/signup.svg" alt="signup">-->
                                </div>
                            </div>
                            <div class="block">
                                <div class="signup-form">
                                        <center><h5 class="pb-17">An OTP has been texted to your phone number</h5></center>
                                        <center>
                                            @if (session('error'))
                                            <div class="alert alert-danger">
                                                {{ session('error') }}
                                            </div>
                                            @endif
                                        </center>
                                        <form class="order-form js-login-form" id="otp-verify" action="{{ url('mobile/otp/confirm') }}" method="POST" onsubmit="showLoadingImage()" autocomplete="off">
                                                {{ csrf_field() }}
                                            <input type="hidden" name="mobile_no" id="mobile_no" value="{{$mobile_no}}"> 
                            			  <div class="form-group">
                            				<label>OTP Number<sup style="color:red;">*</sup></label>
                            				<input class="form-control" type="text" name="verify_code" id="verify_code" required placeholder="Please enter the OTP number we texted to you via SMS"/>
                            				<div class="help-block with-errors">{{ $errors->first('otp') }}</div>
                            			  </div>
                            			  <div class="form-group">
                            				<button class="btn btn-d custom-btn btn-design" style="max-width:46%; background-image:linear-gradient(to bottom, #291e88, #291e88, #291e88); color:#FFF">Confirm</button>
                            			  </div>
                            		   </form>
                            		  <div class="row mb-3" style="margin-top:16px">
                            			<div class="col-xs-12 mb-2">
                            			   <div class="message"></div>
                            			   If you have not received OTP. you can click re-send button.
                            			   <a style="border: 1px solid #eee;padding: 5px;cursor: pointer;" id="resend" class="action primary checkout" onclick="OTPResend('<?php echo $mobile_no;?>')"><span>Resend</span></a>
                            			</div>
                        			</div>
                                </div>
                            </div>
                        </div>
                    </div><!-- pnf -->
                  </div>
                </div>
    	     </div>
    	</section>
     </div>
</main>
 <script src="{{ asset('assets/js/validation/jquery.validate.min.js') }}"></script>
<script src="{{asset('assets/js/validation/additional-methods.min.js')}}"></script>
<script>
jQuery( "#otp-verify" ).validate({
	rules: {
        verify_code:{
			required: true,
            number: true	
		}			
	},
	messages: {
		verify_code: {
			required: " Please submit 4 digit verification code to verify phone"
		},
	}
});

function OTPResend(mobile_no){
	console.log(mobile_no);
	var resend_otp = base_url + "/signup/resend-otp/";
       jQuery.ajax({
        url: resend_otp,
        type: 'GET',
        data: { mobile_number: mobile_no},
        beforeSend: function () {
         jQuery('#resend').attr("disabled", "disabled");
        },
        success: function (msg) {
			if (msg == 'ok') {
			jQuery('.message').val('');
			jQuery('.message').html('<p style="color:green;">OTP Resend successfully.Please check your sms.</p>');
			}else if(msg == 'spam'){
					jQuery('.message').html('<p style="color:red;">Already send more than three SMS. Please check your sms.</p>');
			} else {
			jQuery('.message').html('<span style="color:red;">Some problem occurred, please try again.</span>');
			}
			
		  }
       });
}
</script>
@endsection
