@extends('layouts.app')
@section('title','Mobile Verify')
@section('content')
<link  rel="stylesheet"  href="{{asset('assets/css/mobile-signup.css')}}" />
<main id="maincontent" class="page-main">
    <!--<a id="contentarea" tabindex="-1"></a>-->
    <!--<div class="beadcumarea">-->
    <!--    <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">-->
    <!--                    <ul class="items">-->
    <!--                        <li class="item home">-->
    <!--                            <a href="{{url('/')}}" title="Go to Home Page">Home</a>-->
    <!--                        </li>-->
    <!--                        <li class="item">-->
    <!--                            <a href="" title="">Signup</a>-->
    <!--                        </li>-->
    <!--                    </ul>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
    <div class="main">
    	<section class="module">
    	  <div class="container">
    	      <div class="section-signup">
                  <div class="signin-page">
                    <div class="container">
                        <div class="blocks-wrap">
                            <div class="block">
                                <div class="sign-info">
                                     <h1 class="page-title">Just seconds to create your Pride<sup>&#174;</sup> account!</h1>
                                    <p></p>
                                    <!--<img class="sign-image" src="{{url('/')}}/storage/app/public/signup.svg" alt="signup">-->
                                </div>
                            </div>
                            <div class="block">
                                <div class="signup-form">
                                        <center><h5 class="pb-17">Simply enter your mobile number to verify</h5></center>
                                        <form class="order-form js-login-form" id="mobile-signup" action="{{ url('mobile/otp/send') }}" method="POST" autocomplete="off">
                            			     {{ csrf_field() }}
                            			     <input type="hidden" name="login_user_id"/>
                            			  <div class="form-group">
                            				<label>Mobile Number<sup style="color:red;">*</sup></label>
                            				<input class="form-control" type="text" name="mobile_no"  required placeholder="Mobile Number" value="{{ old('mobile_no') }}"/>
                            				<label class="help-block error">{{ $errors->first('mobile_no') }}</label>
                            			  </div>
                            			 
                            			  <div class="form-group">
                            				<button class="btn btn-d custom-btn btn-design" style="max-width:100%; background-image:linear-gradient(to bottom, #291e88, #291e88, #291e88); color:#FFF" type="submit">Confirm</button>
                            			  </div>
                    		    	</form>
                                </div>
                            </div>
                        </div>
                    </div><!-- pnf -->
                  </div>
                </div>
    	     </div>
    	</section>
     </div>
</main>
 <script src="{{ asset('assets/js/validation/jquery.validate.min.js') }}"></script>
<script src="{{asset('assets/js/validation/additional-methods.min.js')}}"></script>
<script>
jQuery( "#mobile-signup" ).validate({
	rules: {
        mobile_number:{
			required: true,
            number: true	
		}			
	},
	messages: {
		mobile_number: {
			required: "Please enter your mobile number to register"
		},
	}
});
</script>
@endsection
