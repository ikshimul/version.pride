@extends('layouts.app')
@section('title','Create Account')
@section('content')
<link  rel="stylesheet"  href="{{asset('assets/css/mobile-signup.css')}}" />
<style>
    .page-title{
        top:311px;
    }
    @media only screen and (min-width: 601px) and (max-width: 1024px){
        .page-title{
            top:1px;
        }
    }
    @media only screen and (max-width: 600px) {
        .page-title{
            top:1px;
        }
    }
</style>
<main id="maincontent" class="page-main">
    <!--<a id="contentarea" tabindex="-1"></a>-->
    <!--<div class="beadcumarea">-->
    <!--    <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">-->
    <!--                    <ul class="items">-->
    <!--                        <li class="item home">-->
    <!--                            <a href="{{url('/')}}" title="Go to Home Page">Home</a>-->
    <!--                        </li>-->
    <!--                        <li class="item">-->
    <!--                            <a href="#" title="">Create Account</a>-->
    <!--                        </li>-->
    <!--                    </ul>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
    <div class="container">
    	<div class="section-signup">
          <div class="signin-page">
            <div class="container">
                <div class="blocks-wrap">
                    <div class="block">
                        <div class="sign-info">
                            <h1 class="page-title">Create your Pride<sup>&#174;</sup> account</h1>
                            <p></p>
                            <!--<img class="sign-image" src="{{url('/')}}/storage/app/public/signup.svg" alt="signup">-->
                        </div>
                    </div>
                    <div class="block">
                        <div class="signup-form">
                                <center><h5>Lets create your account</h5></center>
                                <center><p>Enter your details to sign up</p></center>
                                <form class="form" action="{{ url('register') }}" method="POST" id="login-form">
                    			     {{ csrf_field() }}
                    			  <div class="form-group">
                    				<label for="f_name">First Name<sup style="color:red;">*</sup></label>
                    				<input class="form-control" id="f_name" type="text" name="f_name" value="{{ old('f_name') }}"/>
                    			  </div>
                    			  <div class="form-group">
                    				<label for="l_name">Last Name<sup style="color:red;">*</sup></label>
                    				<input class="form-control" id="l_name" type="text" name="l_name" value="{{ old('l_name') }}"/>
                    			  </div>
                    			  <div class="form-group">
                    				<label for="mobile">Mobile No.<sup style="color:red;">*</sup></label>
                    				<input class="form-control" id="mobile" type="text" name="mobile" value="{{$mobile_no}}" readonly />
                    				<label class="help-block error">{{ $errors->first('mobile') }}</label>
                    			  </div>
                    			   <div class="form-group">
                    				<label for="email">Email<sup style="color:red;">*</sup></label>
                    				<input class="form-control" id="email" type="text" name="email" value="{{ old('email') }}"/>
                    				<label class="help-block error">{{ $errors->first('email') }}</label>
                    			  </div>
                    			  <div class="form-group">
                    				<label for="password">Create Password<sup style="color:red;">*</sup></label>
                    				<input class="form-control" id="password" type="password" name="password"/>
                    				<label class="help-block error">{{ $errors->first('password') }}</label>
                    			  </div>
                    			  <div class="form-group">
                    				<label for="password">Confirm Password<sup style="color:red;">*</sup></label>
                    				<input class="form-control" id="password_confirmation" type="password" name="password_confirmation"/>
                    				<label class="help-block error">{{ $errors->first('password_confirmation') }}</label>
                    			  </div>
                    			  <div class="form-group">
                    				<button class="btn btn-d custom-btn btn-design" style="max-width:100%; background-image:linear-gradient(to bottom, #291e88, #291e88, #291e88); color:#FFF" type="submit">Register To Checkout</button>
                    			  </div>
                    			</form>
                            
                               <center><p class="have-account">Already have an account? <a href="{{url('/mobile-login')}}" class="link">Sign in</a></p></center>

                        </div>
                    </div>
                </div>
            </div><!-- pnf -->
          </div>
        </div>
     </div>
</main>
 <script src="{{ asset('assets/js/validation/jquery.validate.min.js') }}"></script>
<script src="{{asset('assets/js/validation/additional-methods.min.js')}}"></script>
<script>
jQuery( "#login-form" ).validate({
	rules: {
		f_name: "required",
		l_name: "required",
		email: "required",
		password: "required",
        mobile:{
			required: true,
            number: true	
		},
		password_confirmation: {
			equalTo: "#password"
		}
	},
	messages: {
	    f_name: {
			required: "Please enter your first name"
		},
		l_name: {
			required: "Please enter your last name"
		},
		email: {
			required: "Please enter your email address"
		},
		password: {
			required: "Please enter your password"
		},
		mobile: {
			required: "Please enter your mobile no"
		},
	}
});
</script>
@endsection
