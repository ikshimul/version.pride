@extends('layouts.app')
@section('title','Login | Register')
@section('content')
<link  rel="stylesheet"  href="{{asset('assets/css/mobile-signup.css')}}?4" />
<style>
    .socials-form {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            background: #F2C94C;
            display: flex;
            height: 100%;
            flex-direction: column;
            margin-top: auto;
            padding: 40px 40px 40px;
            width: 100%;
        }
        .socials-form h5{
            font-size: 18px;
            margin-bottom: 5px;
            font-weight: 600;
            color: #291d88;
        }
</style>
<main id="maincontent" class="page-main">
    <!--<a id="contentarea" tabindex="-1"></a>-->
    <!--<div class="beadcumarea">-->
    <!--    <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">-->
    <!--                    <ul class="items">-->
    <!--                        <li class="item home">-->
    <!--                            <a href="{{url('/')}}" title="Go to Home Page">Home</a>-->
    <!--                        </li>-->
    <!--                        <li class="item">-->
    <!--                            <a href="" title="">Login | Register</a>-->
    <!--                        </li>-->
    <!--                    </ul>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->

    	<section class="module">
    	    <div class="container">
    	      <div class="section-signup">
                  <div class="signin-page">
                    <div class="container">
                        <span class="heading f-16 font-weight-600 text-uppercase">Please sign in, or register!</span>
                        <center>
                            @if (session('save'))
                            <div class="alert alert-danger">
                                {{ session('save') }}
                            </div>
                            @endif
                        </center>
                        <div class="blocks-wrap">
                            <div class="block">
                                <div class="signup-form">
                                        <center><h5 class="pb-17">I am already registered at Pride<sup>&#174;</sup></h5></center>
                                        <center>
                                            @if (session('error'))
                                            <div class="alert alert-danger">
                                                {{ session('error') }}
                                            </div>
                                            @endif
                                        </center>
                                        @if (session('failed'))
                                        <center>
                                            <div class="help-block with-errors">{{session('failed')}}
                                            </div>
                                        </center>
                                        @endif 
                            			<form class="form" action="{{url('/custom-login')}}" method="POST" id="login-form">
                            			    {{ csrf_field() }}
                            			  <div class="form-group">
                            				<label for="mobile">Mobile No.<sup style="color:red;">*</sup></label>
                            				<input class="form-control" id="mobile" type="text" name="mobile" value="{{ old('mobile') }}"/>
                            			  </div>
                            			  <div class="form-group">
                            				<label for="password">Password<sup style="color:red;">*</sup></label>
                            				<input class="form-control" id="password" type="password" name="password"/>
                            			  </div>
                            			  <div class="form-group" style="text-align: center;">
                            				<button class="btn btn-d custom-btn btn-design" type="submit">Login</button>
                            			  </div>
                            			 
                            			</form>
                            			<div class="row mb-3" style="margin-top:16px">
                                			<div class="col-xs-6 mb-2">
                                			<p class="mb-1"><a class="order-form__forgot d-inline-block" style="line-height:26px; float:left" href="https://pride-limited.com/password/reset" data-toggle="modal"><span>Forgot your password?</span></a></p>
                                			</div>
                            			</div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="socials-form">
                                    <center><h5 class="pb-17">I Want a Pride<sup>&#174;</sup> User Account.</h5></center>
                        			<p class="f-16 pt-17">Use this option to provide your details to create an account and check out. Simple and easy, don't worry!</p>
                        			<div class="pt-17" style="text-align: center;">
                        			    <a class="btn btn-d custom-btn btn-design"  href="{{url('mobile/verify')}}">Create an Account</a>
                        			</div>
                                </div>
                            </div>
                            
                        </div>
                    </div><!-- pnf -->
                  </div>
                </div>
    	     </div>
    	</section>
</main>
 <script src="{{ asset('assets/js/validation/jquery.validate.min.js') }}"></script>
<script src="{{asset('assets/js/validation/additional-methods.min.js')}}"></script>
<script>
jQuery( "#login-form" ).validate({
	rules: {
		password: "required",
        mobile:{
			required: true,
            number: true	
		}			
	},
	messages: {
		password: {
			required: "Please enter your password"
		},
		mobile: {
			required: "Please enter your mobile no"
		},
	}
});
</script>
@endsection
