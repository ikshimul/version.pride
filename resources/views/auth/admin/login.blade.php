<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Admin Log in</title>
  <link rel="icon" type="image/png" href="{{asset('assets_admin/images/logo.png')}}"/>
  <!-- Google Font: Source Sans Pro -->
 <link rel="preconnect" href="https://fonts.gstatic.com">
 <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
  <!-- Font Awesome -->
  <link  href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- icheck bootstrap -->
  <link  href="{{ asset('assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}" rel="stylesheet">
  <!-- Theme style -->
  <link  href="{{ asset('assets/admin/css/adminlte.min.css') }}" rel="stylesheet">
  <style>
    body {
        /*font-family: open sans condensed,sans-serif!important;*/
        font-family: 'PT Sans Narrow', sans-serif; 
    }
    .invalid-feedback{
        display:block;
    }
</style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="{{url('/admin/login')}}" class="h1"><b>Admin </b>Login</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="{{ route('admin.login.submit') }}" method="post">
          {{ csrf_field() }}
        <div class="input-group mb-3 {{ $errors->has('username') ? ' has-error' : '' }}">
         <input id="email" type="text" class="form-control" name="username" placeholder="Enter user name" value="{{ old('username') }}" required autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
          @if ($errors->has('username'))
				<div class="error invalid-feedback">
					{{ $errors->first('username') }}
				</div>
			@endif
        </div>
        <div class="input-group mb-3 {{ $errors->has('password') ? ' has-error' : '' }}">
          <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @if ($errors->has('password'))
			<span class="help-block">
				<strong>{{ $errors->first('password') }}</strong>
			</span>
		@endif
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
                <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center mt-2 mb-3">
        <!--<a href="#" class="btn btn-block btn-primary">-->
        <!--  <i class="fab fa-facebook mr-2"></i> Sign in using Facebook-->
        <!--</a>-->
        <!--<a href="#" class="btn btn-block btn-danger">-->
        <!--  <i class="fab fa-google-plus mr-2"></i> Sign in using Google+-->
        <!--</a>-->
      </div>
      <!-- /.social-auth-links -->
      <p class="mb-1">
        <a href="{{ route('admin.password.request') }}">I forgot my password</a>
      </p>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/admin/js/adminlte.min.js')}}"></script>
</body>
</html>