@extends('layouts.app')
@section('title', 'Pride Limited | Store Locator')
@section('content')
<style>
.panel-default {
    border-color: #eee;
}
.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid #eee;
    border-radius: 0px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.panel-body {
    min-height: 240px;
}
h4{
    margin-bottom: 0;
    padding: 10px 0 6px 0;
    font-weight: bold;
    /*text-transform: lowercase;*/
    font-size: 125%;
}
.storemapper-storelink{
    background-color: transparent!important;
    border-color: transparent!important;
    color: #69b1bf!important;
    text-transform: uppercase!important;
    font-size: 14px!important;
    border-radius: 0!important;
    padding: 0!important;
    font-weight: 700;
    line-height: 16px;
    display: block;
}
.address{
    font-size: 14px;
    line-height: 16px;
    margin-top: 0;
    margin-bottom: 5px;
}
.email{
   font-size: 14px;
    line-height: 16px;
    margin-top: 0;
    margin-bottom: 5px; 
}
.mobile{
    font-size: 14px;
    line-height: 16px;
    margin-top: 0;
    margin-bottom: 5px; 
    display: block;
}
.time{
    font-size: 14px;
    line-height: 16px;
    margin-top: 3px;
    margin-bottom: 5px; 
    display: block;
}
.text-address{
    position: relative;
    top: -2px;
}
.text-email{
    position: relative;
    top: -3px;
}
.text-mobile{
    position: relative;
    top: -1px;
}
.view-map{
    position: relative;
    top: 15px;
}
.text-time{
    position: relative;
    top: -2px;
    font-weight:600;
}
.Absolute-Center {
  margin: auto;
  top: 0; left: 0; bottom: 0; right: 0;
}
.Absolute-Center.is-Responsive {
  width: 50%; 
  height: 50%;
  padding: 13px;
}
.form-control {
    height: 34px;
    font-size:14px;
}
.not-found{
    text-align:center;
    color:red;
}
#search-store-result{
   padding-top:20px; 
}
.page-main{
    padding-top:60px;
}
@media (min-width: 1024px){
  .header-store{
    font-size: 200%;
    text-align:center;
  }
}
@media only screen and (max-width: 600px) {
  .Absolute-Center.is-Responsive {
      width: 100%; 
      height: 100%;
      padding: 0px;
      padding-top: 34px;
    }
    
    .header-store{
        font-size: 16px;
        text-align:center;
      }
    .Absolute-Center.is-Responsive {
      padding: 0px;
    }
    
    .page-main{
        padding-top:6px;
    }
}
</style>
<main id="maincontent" class="page-main"><a id="contentarea" tabindex="-1"></a>
<div class="row">
    <div class="col-md-12">
         <div class="Absolute-Center is-Responsive">
          <div class="col-sm-12 col-md-10 col-md-offset-1">
              <center><span class="header-store">Store Locator</span></center>
            <form id="loginForm">
                <div class="form-group">
                  <label for="usr">Select your location:</label>
                  <select class="form-control" name="location" id="search-loc">
                     <option value=""> -- select location -- </option>
                    <option value="dhaka">Dhaka</option>
                    <option value="chittagong">Chittagong</option>
                    <option value="rajshahi">Rajshahi</option>
                    <option value="sylhet">Sylhet</option>
                    <option value="khulna">Khulna</option>
                    <option value="mymensingh">Mymensingh</option>
                    <option value="brahmanbaria">Brahmanbaria</option>
                    <option value="rongpur">Rongpur</option>
                    <option value="barisal">Barisal</option>
                  </select>
                </div>
              <!--<div class="form-group input-group">-->
              <!--  <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>-->
              <!--  <input class="form-control" type="text" id="search-loc" name='location' placeholder="search your location"/>    -->
              <!--  <div class="input-group-btn">-->
              <!--    <button class="btn btn-default" type="submit" id="btn-search-location">-->
              <!--      <i class="glyphicon glyphicon-search"></i>-->
              <!--    </button>-->
              <!--  </div>-->
              <!--</div>-->
            </form>        
          </div>  
        </div> 
    </div>
</div>
<div class="row">
    <!--<hr>-->
    <div id='loader' style='text-align: center;display: none;'>
          <img src="{{url('/')}}/storage/app/public/loader.gif" style="width:auto; margin:auto" Alt="Loading...">
    </div>
    <div class="col-md-12 col-xs-12 col-log-12" id="search-store-result">
         <?php foreach ($store as $storelist) { ?>
             <div class="col-sm-4 col-md-4 col-xs-12 col-lg-3">
                 <div class="panel panel-default">
                      <div class="panel-body">
                          <h4 class="storemapper-title"><?php echo $storelist->SalesPointName; ?></h4>
                          <p class="address">
                              <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 1792 1792">
                              <path d="M1152 640q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm256 0q0 109-33 179l-364 774q-16 33-47.5 52t-67.5 19-67.5-19-46.5-52L417 819q-33-70-33-179 0-212 150-362t362-150 362 150 150 362z"></path>
                              </svg><span class="text-address"><?php echo $storelist->SPAddress; ?></span>
                          </p>
                          <span class="email"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width="13" height="13" viewBox="0 0 125 100">
                              <path d="M60,0 l50,0 a10,10 0 0,1 7,17 l-50,50 a10,10 0 0,1 -13,0 l-50,-50 a10,10 0 0,1 7,-17z" stroke="#000" stroke-width="0" fill="#000" />
                              <path d="M60,90 l54,0 a10,10 0 0,0 7,-7 l0,-60 -50,50 a15,15 0 0,1 -21,0 l-50,-50 0,60 a10,10 0 0,0 7,7z" stroke="#000" stroke-width="0" fill="#000" />  
                            </svg> <span class="text-email"><?php echo $storelist->email; ?></span></span>
                           <!--<span class="mobile">-->
                           <!--    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.5 17.311l-1.76-3.397-1.032.505c-1.12.543-3.4-3.91-2.305-4.497l1.042-.513-1.747-3.409-1.053.52c-3.601 1.877 2.117 12.991 5.8 11.308l1.055-.517z"/></svg>-->
                           <!--    <span class="text-mobile"><?php echo $storelist->Phone; ?></span>-->
                           <!--</span>-->
                           <span class="time">
                               <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm5.848 12.459c.202.038.202.333.001.372-1.907.361-6.045 1.111-6.547 1.111-.719 0-1.301-.582-1.301-1.301 0-.512.77-5.447 1.125-7.445.034-.192.312-.181.343.014l.985 6.238 5.394 1.011z"/></svg>
                               <span class="text-time" <?php if($storelist->active=='closed'){ echo 'style="color:red"';}else{ echo 'style="color:green"';}?>> <?php echo $storelist->OpeningTime; ?> - <?php echo $storelist->ClosingTime; ?> • <?php echo ucfirst($storelist->active);?></span>
                                <!--<span class="text-time"> 10:00am - 5:00pm</span>-->
                                <!--<span class="text-time"> Closed till 21<sup>1st</sup> April</span>-->
                           </span>
                          <div>
                              <p class="view-map">
                                 <a href="http://maps.google.com/maps?saddr=&amp;daddr=<?php echo $storelist->SPAddress; ?>" class="storemapper-storelink" target="_blank"  aria-label="View on the map: <?php echo $storelist->SPAddress; ?>">View On Map</a>
                               </p>
                          </div>
                        </div>
                  </div>
             </div>
         <?php } ?>
    </div>
</div>
</main>
<script>
jQuery(document).ready(function () {
jQuery("#search-loc").change(function () {
    var loc = jQuery("#search-loc").val();
    if (loc == '') {
      loc = '';
    } else {
      loc = jQuery("#search-loc").val();
    }
    var str_length = jQuery("#search-loc").val().replace(/ /g, '').length;
    var url = base_url + "/ajax/search-store-loc?location=" + loc;
    jQuery.ajax({
        url: url,
        type: 'GET',
        beforeSend: function(){
            jQuery("#search-store-result").hide();
            jQuery("#loader").show();
           },
        success: function (data) {
          jQuery('#search-store-result').html(data);
        },
        complete:function(data){
        jQuery("#loader").hide();
        jQuery("#search-store-result").show();
       }
    });
});
});
</script>
@endsection