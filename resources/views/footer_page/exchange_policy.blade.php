@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style type="text/css">
    div.aboutus_mother_container2{
        width: 100%;
        height: auto;
        background-color: #FFFFFF;
        /*opacity:0.3;
        filter:alpha(opacity=30); /* For IE8 and earlier */
        min-height: 450px;
        text-align:center;
        color:#000000;
        font-size:16px;
    }
    .h1 {
        font-size: 24px;
    }
    .h2 {
        text-align:justify;
        font-size: 18px;
    }
    .top-heading{
        font-weight: 600;
        padding-top: 17px;
        padding-bottom: 30px;
        font-size: 19px;
        text-align: justify;
    }
    .heading{
        display:block;
        font-weight:600;
    }
    .body-letter{
        padding-left:10px;
        text-align: justify;
        /*line-height: 2.26; */
    }
    .module, .module-small {
        position: relative;
        padding: 10px 0;
        background-repeat: no-repeat;
        background-position: 50% 50%;
        background-size: cover;
    }
    p, ol, ul, blockquote {
        margin: 0 0 13px;
    }
    .padding-top{
        padding-top: 10px;
    }
    ul li.exchange {
        list-style-type: circle;
        margin-left:30px;
        padding:6px;
    }
    ul li a.exchange {
        color:#00f;
    }
</style>
<div class="container bg-grey" style="padding-top:70px;color:black;" id="search_result">
    <main class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="layout">
                        <div class="module section">
                            <div class="three modules">
                                <h2>HOW TO MAKE AN EXCHANGE FOR ONLINE PURCHASES</h2>
                                <div class="col-md-12" style="color:black;font-size:15px;">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse"  data-parent="#accordion" href="#collapseOne">
                                                        1.	INSIDE DHAKA
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne"  class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul>
                                                        <li class="exchange">Our corporate exchange policy allows you to only exchange any garment within 15 days of purchase, provided the garment and hangtag is intact and the customer provides the receipt. The product condition must be same as when you received it, for all cases.</li>
                                                        <li class="exchange">We only facilitate exchanges for the same product but different sizes, or a different product. The price difference will be collected or reimbursed to you, depending on the invoice value.</li>
                                                        <li class="exchange">To exchange your previous order with a different product, please place a new order for the different product first.</li>
                                                        <li class="exchange">You have 15 days from the shipping date to exchange your pride-limited.com purchase for which the standard delivery will be required, payable or due in cash or bKash.</li>

                                                        <li class="exchange">If a mistake is made on the part of our Shipping & Logistics, such as a wrong size, color or item was sent than the one you have ordered, the exchange is free of delivery cost.</li>

                                                        <li class="exchange">If you request an exchange for which the order has no issue, you will be charged for delivery of exchange.</li>

                                                        <li class="exchange">If you choose to request an exchange, please contact us on our Facebook or Instagram Messenger with your Name and Order No.</li>
                                                    </ul>

                                                    <h3>REMEMBER</h3>

                                                    <ul style="font-weight:700;">
                                                        <li class="exchange">ONLY ONE EXCHANGE REQUEST CAN BE DONE AT A TIME FOR THE SAME ORDER.</li>

                                                        <li class="exchange">YOU MAY ADD UP TO 5 ITEMS PER EXCHANGE REQUEST.</li>

                                                        <li class="exchange">JEWELRY CANNOT BE EXCHANGED.</li>

                                                        <li class="exchange">THE EXCHANGE OPTION WILL NOT BE VISIBLE IF THE PURCHASED ITEM IS NO LONGER AVAILABLE.</li>

                                                        <li class="exchange">ONLINE EXCHANGE WILL NOT BE AVAILABLE FOR ANY ITEM PURCHASED WITH A GIFT CARD.</li>

                                                        <li class="exchange">IF YOU ARE REGISTERED UNDER THE PRIDE INSIDER REWARDS LOYALTY PROGRAM, YOUR POINTS WILL GET ADJUSTED WITH YOUR EXCHANGE OF PRODUCTS.</li>

                                                        <li class="exchange">ANY PURCHASE FOR WHICH YOU HAVE REDEEMED YOUR POINTS AND/OR RECEIVED DISCOUNT WILL NOT BE ELIGIBLE FOR AN EXCHANGE.</li>

                                                        <li class="exchange" style="font-weight:500;">Please see our PRIDE INSIDER LOYALTY PROGRAM Terms and Conditions for more information. <a class="exchange" href="https://pride-limited.com/loyalty-terms-&-conditions">CLick Here</a>.</li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse"  data-parent="#accordion" href="#outofdhaka">
                                                        2.	OUT OF DHAKA
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="outofdhaka"  class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>We currently do not offer any online exchanges for out of Dhaka city deliveries.</p>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse"  data-parent="#accordion" href="#forstore">
                                                        3.	FOR PURCHASES MADE WITH CARD PAYMENTS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="forstore"  class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul>
                                                        <li class="exchange">In the event of a Customer effecting a transaction by using a Payment Card, the Merchant may request SSL for a refund on any grounds whatsoever within a period of seven (07) days from the actual delivery of the product, then SSL shall be entitled to cancel authorization and refuse to make any payments to the Merchant and/or enforce a refund from the Merchant. </li>
                                                        <li class="exchange">
                                                            SSL shall forthwith inform the Merchant of the same and shall debit or collect the payment from the Merchant's Account and make an intermediate credit to SSL's 2. Account in favor of the said Customer. 
                                                        </li>
                                                        <li class="exchange">
                                                            In the event the Merchant accepts a customer order/agrees to provide the service to the Customer but however subsequently notifies to SSI about the Merchant's inability to comply with a customer order/service- Merchant shall forthwith make a proper cancellation for giving effect to the same as well as provide the funds in their account to facilitate a refund of the amount due to the customer.
                                                        </li>
                                                        <li class="exchange">
                                                            In the event of a refund to a cardholder In respect of any transaction of any goods,/ services that are not received as ordered by the Cardholder or are lawfully rejected or services are not performed or partly performed or cancelled or price is lawfully disputed by the Cardholder or price adjustment is not allowed or for any other reason whatsoever, a credit slip shall be issued by the Merchant to SSL.
                                                        </li>
                                                        <li class="exchange">
                                                            Within seven (07) to nine (09) working days after the refund has been agreed between the Merchant and the cardholder and SSL shall: i. Debit the Merchant's account forthwith; and/or ii. Deduct the outstanding amount from the Merchant's account; and/or iii. If there is insufficient funds available in Merchant's account, SSL shall claim from the Merchant the amount to be refunded to the Customer. 
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse"  data-parent="#accordion" href="#cardpayment">
                                                        4.	IN THE CASE OF VOID TRANSACTIONS MADE WITH CREDIT/DEBIT CARD PAYMENTS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="cardpayment"  class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul>
                                                        <li class="exchange">
                                                            If a Transaction is an Invalid or Fraud Transaction, Bank may, with prior discussion with SSL (and without a request or demand from a Cardholder), Refuse to accept the Transaction; or
                                                        </li>
                                                        <li class="exchange">
                                                            If the Transaction has been processed, at any time within 180 days of the date of the Transaction, charge that Transaction back to the Customer by debiting the Merchant Account or otherwise exercising its rights under the MEF. 
                                                        </li>
                                                        <li class="exchange">
                                                            Merchant is liable to verify the shipping address before delivering any product to customer's shipping address. 
                                                        </li>
                                                        <li class="exchange">
                                                            Merchant shall not deliver a product in an address other than the address mentioned as delivery address by the Customer. If card holder claims for charge back, merchant must submit proper delivery receipts & invoices, otherwise Merchant will bear the liability of charge back.
                                                        </li>
                                                        <li class="exchange">
                                                            Here, Invalid or Fraud Transaction shall mean i. The transaction and its records are illegal; ii. The particulars inserted in the order form are not identical with the actual particulars identifying the Cardholder; iii. The Nominated Card is invalid at the time of transaction; iv. The Nominated Card is listed on the Warning Bulletin issued to SSL; v. The Merchant has failed to observe the MEF in relation to the transaction; vi. The Nominated Card was used without the consent of the Cardholder.
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>


                            <h2>HOW TO MAKE AN EXCHANGE FOR STORE PURCHASES</h2>
                            <ul>
                                <li class="exchange">
                                    Our corporate exchange policy allows you to only exchange any garment within 15 days of purchase, provided the garment and hangtag is intact and the customer provides the receipt. We only facilitate exchanges for the same product but different sizes, or a different product. The price difference will be collected or reimbursed to you, depending on the invoice value.
                                </li>
                                <li class="exchange">
                                    Please go to the counter/ showroom the product was purchased in, and ask the store manager or sales staff to assist you.
                                </li>
                            </ul>

                            <h3>REMEMBER:</h3>
                            <ul style="font-weight:700;">
                                <li class="exchange">ONLY ONE EXCHANGE REQUEST CAN BE DONE AT A TIME FOR THE SAME INVOICE.</li>

                                <li class="exchange">JEWELRY CANNOT BE EXCHANGED.</li>

                                <li class="exchange">THE EXCHANGE FOR A DIFFERENT SIZE OR COLOR WILL NOT BE AVAILABLE IF THE ITEM NO LONGER AVAILABLE.</li>

                                <li class="exchange">STORE EXCHANGE WILL NOT BE AVAILABLE FOR ANY ITEM PURCHASED WITH A GIFT CARD.</li>

                                <li class="exchange">IF YOU ARE REGISTERED UNDER THE PRIDE INSIDER REWARDS LOYALTY PROGRAM, YOUR POINTS WILL GET ADJUSTED WITH YOUR EXCHANGE OF PRODUCTS.</li>

                                <li class="exchange">ANY PURCHASE FOR WHICH YOU HAVE REDEEMED YOUR POINTS AND/OR RECEIVED DISCOUNT WILL NOT BE ELIGIBLE FOR AN EXCHANGE.</li>

                                <li class="exchange" style="font-weight:500;">Please see our PRIDE INSIDER LOYALTY PROGRAM Terms and Conditions for more information. <a class="exchange" href="https://pride-limited.com/loyalty-terms-&-conditions">CLick here</a>.</li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection