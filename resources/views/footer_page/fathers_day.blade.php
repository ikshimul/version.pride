@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style type="text/css">
    .page-main{
        padding-top: 62px;
      }
    .h1 {
        font-size: 24px;
    }

    .title {
        text-align:justify;
        font-size: 14px;
    }
    p{
        font-size:16px;
    }
    @media only screen and (max-width: 600px) {
      .page-main{
        padding-top: 2px;
      }
      p{
        font-size:14px;
    }
    .mobile-footer-logo{
        width:70%;
    }
    }
</style>
<main id="maincontent" class="page-main"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Pride Father’s Day Giveaway</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="search_result">
        <div class="container"><!-- Container start -->
            <section id="content">
                <div class="content-wrap">
                    <div class="container clearfix">
                        <center> 
                             <img src="{{ url('/') }}/storage/app/public/web-banner.jpg" style="width:100%">
                            <div class="h1" style="font-weight:600;"> Pride Father’s Day Giveaway</div><br>
                            <div class="title">
                                <strong><h3 style="font-weight:600;">Contest Guidelines:</h3></strong>
                                <br>
                                    <p>1. Take a picture with your dad, either one of you in Pride apparel.</p>
                                    <p>2. Share it on your Facebook or Instagram, and write in one line: 'Why your father means the world to you!'. Make sure the post is public!</p>
                                    <p>3. Also make sure to add the hashtags #MadeWithPride #WearWithPride, and tag @PrideLimitedBangladesh in the caption. Finally, tag three of your FnF in the comments.</p>
                                    <br>
                                    <p>What you stand to win: </p>
                                    <p>A full wardrobe of two attires for   your dad from Pride Limited.</p>
                                    <p>A Gift Hamper from luxury men's groomer,Truefitt + Hill, worth Tk10,000! (Hamper includes products and salon service voucher).</p>
                                    <p>*Giveaway ends on 19th June 2021, winners will be announced on Father’s Day, 20th June 2021.</p>
                                <br>
                                <strong><h3 style="font-weight:600;">Terms & Conditions </h3></strong><br>
                                <p>
                                    1. This giveaway contest will run from 14/06/2021 till 19/06/2021, 11:59 pm. Any entries received after the contest period has ended will not be counted.
                                </p>
                                <p>2. Different members from the same family are not eligible to enter separately.</p>
                                <p>3. Employees of Pride Limited (including its affiliated and related companies) and their immediate family members (children, parents, siblings, spouses) are not eligible to enter.</p>
                                <p>4. Pride Limited reserves the right to revise the contest period and any other details, at any time without prior notification. </p>
                                <p>5. Truefitt & Hill service vouchers included in the prize are valid for a three month period within which they can be redeemed at the salon. </p>
                            </div>
                        </center>
                    </div>
                </div>
        </div><!-- end container -->
    </div>
</main>
@endsection