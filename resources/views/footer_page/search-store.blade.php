<?php foreach ($store as $storelist) { ?>
     <div class="col-sm-4 col-md-4 col-xs-12 col-lg-3">
         <div class="panel panel-default">
              <div class="panel-body">
                  <h4 class="storemapper-title"><?php echo $storelist->SalesPointName; ?></h4>
                  <p class="address">
                      <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 1792 1792">
                      <path d="M1152 640q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm256 0q0 109-33 179l-364 774q-16 33-47.5 52t-67.5 19-67.5-19-46.5-52L417 819q-33-70-33-179 0-212 150-362t362-150 362 150 150 362z"></path>
                      </svg><span class="text-address"><?php echo $storelist->SPAddress; ?></span>
                  </p>
                  <span class="email"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width="13" height="13" viewBox="0 0 125 100">
                      <path d="M60,0 l50,0 a10,10 0 0,1 7,17 l-50,50 a10,10 0 0,1 -13,0 l-50,-50 a10,10 0 0,1 7,-17z" stroke="#000" stroke-width="0" fill="#000" />
                      <path d="M60,90 l54,0 a10,10 0 0,0 7,-7 l0,-60 -50,50 a15,15 0 0,1 -21,0 l-50,-50 0,60 a10,10 0 0,0 7,7z" stroke="#000" stroke-width="0" fill="#000" />  
                    </svg> <span class="text-email"><?php echo $storelist->email; ?></span></span>
                   <!--<span class="mobile">-->
                   <!--    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4.5 17.311l-1.76-3.397-1.032.505c-1.12.543-3.4-3.91-2.305-4.497l1.042-.513-1.747-3.409-1.053.52c-3.601 1.877 2.117 12.991 5.8 11.308l1.055-.517z"/></svg>-->
                   <!--    <span class="text-mobile"><?php echo $storelist->Phone; ?></span>-->
                   <!--</span>-->
                   <span class="time">
                       <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm5.848 12.459c.202.038.202.333.001.372-1.907.361-6.045 1.111-6.547 1.111-.719 0-1.301-.582-1.301-1.301 0-.512.77-5.447 1.125-7.445.034-.192.312-.181.343.014l.985 6.238 5.394 1.011z"/></svg>
                       <!--<span class="text-time"> 10:00am - 5:00pm</span>-->
                       <span class="text-time" <?php if($storelist->active=='closed'){ echo 'style="color:red"';}else{ echo 'style="color:green"';}?>> <?php echo $storelist->OpeningTime; ?> - <?php echo $storelist->ClosingTime; ?>  • <?php echo ucfirst($storelist->active);?></span>
                       <!--<span class="text-time"> Closed till 21<sup>1st</sup> April</span>-->
                   </span>
                  <div>
                      <p class="view-map">
                         <a href="http://maps.google.com/maps?saddr=&amp;daddr=<?php echo $storelist->SPAddress; ?>" class="storemapper-storelink" target="_blank"  aria-label="View on the map: <?php echo $storelist->SPAddress; ?>">View On Map</a>
                       </p>
                  </div>
                </div>
          </div>
     </div>
 <?php } ?>