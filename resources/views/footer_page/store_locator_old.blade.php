@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style>
    .panel-default>.panel-heading {
        color: #333;
        background-color: #f5f5f5;
        border-color: #ddd;
    }
    .panel-heading {
        padding: 10px 15px;
        border: 1px solid transparent;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }
    .panel-group .panel+.panel {
        margin-top: 5px;
    }
    .panel-group {
        margin-bottom: 20px;
    }
    .panel-group .panel {
        margin-bottom: 0;
        border-radius: 4px;
    }
    .well, .label, .alert, .progress, .form-control, .modal-content, .panel-heading, .panel-group .panel, .nav-tabs > li > a, .nav-pills > li > a {
        border-radius: 2px;
    }
    .panel-title {
        margin-top: 0;
        margin-bottom: 0;
        font-size: 16px;
        color: inherit;
    }
    :after, :before {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .panel-default>.panel-heading+.panel-collapse>.panel-body {
        border-top-color: #ddd;
    }
    .panel-group .panel-heading+.panel-collapse>.list-group, .panel-group .panel-heading+.panel-collapse>.panel-body {
        border: 1px solid #ddd;
    }
    .panel-body {
        padding: 15px;
    }
    .h1, h1 {
        font-size: 25px;
    }
    h1.page-header {
        color: #291e88;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Store Locator</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="search_result">
        <div class="container"><!-- Container start -->
            <section id="content">
                <div class="content-wrap">
                    <div class="container clearfix">
                        <center> <h1 class="page-header">STORE LOCATOR </h1></center>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d233880.1830883405!2d90.22284429041237!3d23.662441503089187!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sPride+Limited+!5e0!3m2!1sen!2sbd!4v1525078450251" width="100%" height="400px"></iframe>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                <h1 class="page-header">STORE LOCATOR 
                                    <small>Pride Showroom List</small>
                                </h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">  
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                <h4 class="panel-title">
                                                    Dhaka
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <b>Pride- Banani, Dhaka.</b></p>
                                                67/D, Block # E, Road # 11,</p> Green Delux (Ground Floor),</p> Banani, Dhaka – 1213</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>Pride – Dhanmondi, Dhaka</p></b>
                                                1st Floor, House 8/1, Road 4,</p> Dhanmondi R/A, Dhaka - 1205</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>PRIDE - Dhaka New Market</p></b>
                                                41, Govt. New Market, Dhaka.</p>

                                                <b>PRIDE - Mirpur 10</p></b>
                                                Promij Tower, House # 23, Section-6, Block- KA,</p> Mirpur, Dhaka-1216 </p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>Pride – Baily Road, Dhaka.</p></b>
                                                ABC Mehjabeen Square,</p> 1/1 BailyRoad, Dhaka </p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>PRIDE - N.Gonj.</p></b>
                                                Santana Market ( 1st Floor ),</p> 233/1 B.B Road, Chashara, Narayangonj.</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>PRIDE - Gausia 1.</p></b>
                                                1, Noor Menshion, ( 1st. Floor ) Gausia Market, Dhaka.</p>

                                                <b>PRIDE - Twin Tower.</p></b>
                                                Concord Twin Tower, Chamelibag, Shantinagar, Dhaka.</p>

                                                <b>PRIDE - Anarkoli.</p></b>
                                                Anarkoli Super Market, ( Ground Floor ). Siddeshwari, Dhaka.</p>

                                                <b>PRIDE - Mouchak 1.</p></b>
                                                J - 107, Mouchak Market ( 1st Floor ), Siddeshwari, Dhaka.</p>

                                                <b>PRIDE - Capital Tower.</p></b>
                                                Capital Tower Market (2nd floor), Mirpr-1, Dhaka</p>

                                                <b>PRIDE - Navana Baily Star</p></b>
                                                Navana Baily Star (2nd Floor),Baily Road, Santinagor. Dhaka</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>PRIDE - Estarnplaza</p></b>
                                                2/30 Estarnplaza (1st floor),Hatirpool,Dhaka</p>

                                                <b>PRIDE - Mascot Plaza.</p></b>
                                                228-229 Mascot Plaza, 107/A Sonargoan Janopath, Sector-7, Uttara, Dhaka.</p>

                                                <b>PRIDE - Rapa Plaza</p></b>
                                                30-33, Rapa Plaza ( 2 nd Floor ), Dhanmondi - 27, Dhaka.</p>


                                                <b>PRIDE - Bashundora City</p></b>
                                                Level -4A (41-42),Bashundora City Complex, Panthapath, Dhaka</p>
                                                <b>PRIDE - Savar.</p></b>
                                                Afroza Plaza ( 1st Floor ), Savar Bazar Bus Stand, Savar.</p>
                                                Time : 10:00am - 8:00pm</p>
                                                <b>PRIDE - Savar.</p></b>
                                                Afroza Plaza ( 1st Floor ), Savar Bazar Bus Stand, Savar.</p>
                                                <b>PRIDE - Mouchak 2.</p></b>
                                                Shop # 63 (3rd Floor) Mouchak Market,Dhaka</p>

                                                <b>PRIDE - Center point Mouchak</p></b>
                                                31/32 Center point, Siddshowri, Mouchak, Dhaka</p>
                                                <b>PRIDE - TMC Showroom</p></b>
                                                TMC Shoping Complex (1st Floor),   52 New Eskaton Road,Bangla Motor ,Dhaka-1000</p>
                                                Time : 10:00am - 8:00pm</p>
                                                
                                                <b>PRIDE - Islampur Showroom</p></b>
                                                Shop No.28-30, Holding No.43-44,Islam Plaza,Islampur,Dhaka</p>
                                                Time : 10:00am - 8:00pm</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                <h4 class="panel-title">
                                                    Chittagong
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">

                                                <b>PRIDE – Nasirabad</b></p>
                                                <p>998, CDA Avenue ( 1st Floor ) East Nasirabad, Chittagong</p> 

                                                <b>PRIDE - Younsco City.</b></p>
                                                <p>Younsco City Complex (1st Floor), GSC More, CTG</p> 

                                                <b>PRIDE - Afmi Plaza</b></p>
                                                20/21 AfmiPlaza, Probortok More, Nasirabad, Ctg</p>

                                                <b>PRIDE - CTG Solosohor</p></b>
                                                1103 CTG shopping Complex,Solosohor, CTG</p>

                                                <b>PRIDE - CTG Bay Shopping</p></b>
                                                74 Bay Shopping Center (1st Floor), Bay Shopping EPZ,Ctg</p>

                                                <b>PRIDE - CTG New Market</p></b>
                                                Azmir Store, 25 Biponi Bitan, New Market, Chittagong. Phone - 031 - 636765 </p>

                                                <b>PRIDE - Feni</p></b>
                                                129/2, Trank Road ( 1st Floor) Feni. </p>

                                                <b>PRIDE - Lakshmipur</p></b>
                                                Sky Khan Tower, Tomiz Market,Lakshmipur.</p>

                                                <b>PRIDE - Maijdi Nowakhali</p></b>
                                                AmirComplex (Ground Floor), Cort main Road, Nowakhali</p>

                                                <b>PRIDE - Chadpur</p></b>
                                                Hossain Plaza, Zor Pukur Par, J. M. Sen Gupta Road, Chandpur.</p>

                                                <b>PRIDE - Cox's Bazar.</p></b>
                                                Hazi Barmiz Market (2nd Floor),Main Road, Cox's Bazar.</p>

                                                <b>PRIDE - Comilla</p></b>
                                                Gani Bhuiyan Mansion, Monohorpur, Comilla.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                <h4 class="panel-title">
                                                    Khulna
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body">

                                                <b>PRIDE - Khulna 1.</b></p>
                                                <p>Mazib Sharani, Shahib bari More, Khulna.</p> 
                                                Time : 10:00am - 8:00pm</p>

                                                <b>PRIDE - Khulna 2</b></p>
                                                10 KDA, New Market, Khulna.</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>PRIDE - Khulna - 3</p></b>
                                                Akther Chember (1st Floor),81 Sir Iqbal Road,Khulna.</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>PRIDE - Satkhira</p></b>
                                                Mehedi Super Market, Boro Bazar Sorok, Satkhira (Under One Bank).</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>PRIDE - Magura</p></b>
                                                JamJam Market (1st Floor), Sahid Atar Ali Road,Magura </p>

                                                <b>PRIDE – Jessor</p></b>
                                                47, Jess Tower(Ground Floor), M.K. Road, Jessore.</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>PRIDE – Gopalgonj</p></b>
                                                Mousumi Sumer Market, 27 Katpotti Road, Gopalgonj</p>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                                <h4 class="panel-title">
                                                    Sylhet
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseFour" class="panel-collapse collapse">
                                            <div class="panel-body">

                                                <b>PRIDE - Sylhet 1</b></p>
                                                Al - Amin Complex(1st Floor), Zinda Bazar, Sylhet.</p> 

                                                <b>PRIDE - Moulovi Bazar</b></p>
                                                Central Road (Chourasta), Moulovi Bazar,</p>

                                                <b>PRIDE - B.Baria</p></b>
                                                City Center (1st Floor), Shop # 6+7, Court Road, B. Baria.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                                <h4 class="panel-title">
                                                    Rajshahi
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseFive" class="panel-collapse collapse">
                                            <div class="panel-body">

                                                <b>PRIDE - Rajshahi</b></p>
                                                Khaza Plaza(1st Floor), Saheb Bazar, Rajshahi.</p> 

                                                <b>PRIDE - DinajPur</b></p>
                                                Pinky Supper Market, Bashunia Potti, Dinajpur</p> 

                                                <b>PRIDE - Nawgoan</b></p>
                                                Chistia Tower Sonali Bank Road (1st Floor),Nawgan</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <!--<b>PRIDE - Bogra 3</p></b>-->
                                                <!--Jolessoritola, kali Bari More,Bogra.</p>-->
                                                <!--Time : 10:00am - 8:00pm</p>-->

                                                <b>PRIDE - Shajadpur</p></b>
                                                Dariarpur Bazar,Shahajadpur,Sherajgonj</p>

                                                <b>PRIDE - Serpur</p></b>
                                                Goal Potty, Sherpur.</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <!--<b>PRIDE - Bogra2</p></b>-->
                                                <!--Anwara Super Market (1st Floor), Marina Road, New Market, Bogra</p>-->
                                                <!--Time : 10:00am - 8:00pm</p>-->

                                                <b>PRIDE - Sirajgonj</p></b>
                                                Nobab Market (Ground Floor),SS Road, Sherajgonj</p>
                                                Time : 10:00am - 8:00pm</p>

                                                <b>PRIDE - Pabna</p></b>
                                                19/19 New Market, Pabna</p>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#Rangpur">
                                                <h4 class="panel-title">
                                                    Rangpur
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="Rangpur" class="panel-collapse collapse">
                                            <div class="panel-body">

                                                <b>PRIDE - Rongpur</p></b>
                                                Hashi Shopping Center(1st floor),Jahaj Company More,StationRoad, Rangpur</p>
                                                Time : 10:00am - 8:00pm</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                                <h4 class="panel-title">
                                                    Mymensingh
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseSix" class="panel-collapse collapse">
                                            <div class="panel-body">

                                                <b>PRIDE - Jamalpur Showroom</p></b>
                                                Soydagor Mension, Medical Road, Tomal Tola, Jamalpur.</p>
                                                Time : 10:00am - 8:00pm</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                                                <h4 class="panel-title">
                                                    Barishal
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseSeven" class="panel-collapse collapse">
                                            <div class="panel-body">

                                                <b>PRIDE - Barishal</p></b>
                                                Younus Plaza, 1st Floor, K. B. Hemayet Uddin Road, Barisal.
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
        </div>
    </div>
</main>
@endsection