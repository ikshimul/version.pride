@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style type="text/css">
    .h1 {
        font-size: 24px;
    }
    .h2 {
        font-size: 18px;
    }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Work With Us</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="search_result">
        <div class="container"><!-- Container start -->
            <section id="content">
                <div class="content-wrap">
                    <div class="container clearfix">
                         <div class="h1"><center><h3 style="color:#000">Press & Talent</h3></center> </div>
                        <div class="h2"> Press</div>
                        <p>For press related inquiries please contact <a href="mailto:press@pride-grp.com">press@pride-grp.com</a></p>
                        <div class="h2"> Talent</div>
                        <p>We're always looking to collaborate with fresh new talent! If you think you have got what it takes to be part of our innovative and dynamic team, please email your resume to <a href="mailto:talent@pride-grp.com">talent@pride-grp.com</a></p> 
                    </div>
                </div>
        </div><!-- end container -->
    </div>
</main>
@endsection