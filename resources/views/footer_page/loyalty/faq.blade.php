@extends('layouts.app')
@section('title', 'Pride Limited | PRIDE INSIDER REWARDS')
@section('content')
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/sephora.css')}}/?20" />
<style>
    .title {
        text-transform: uppercase;
        font-weight: 600;
    }
    .verticle-center {
        height:260px;
    }
    .verticle-center>div:first-child {
        position:absolute;
        left:50%;
        top:50%;
        margin:0;
        transform:translate(-50%, -50%);
        width:100%;
    }
    @media only screen and (max-width: 768px) {
        .verticle-center {
            height:100px;
        }
        .gift {
            margin-top:20px;
            margin-bottom:20px;
        }
    }
    @media only screen and (max-width: 992px) {
        .gift {
            margin-top:20px;
            margin-bottom:20px;
        }
    }
    
</style>
<main data-v-7cf67cfb="" id="page" class="page-container container">
    <div data-v-72bbedf8="" data-v-7cf67cfb="" class="bp-container">
        <div data-v-72bbedf8="" class="bp-section">
            <div data-v-72bbedf8="" class="header-section" style="">
                <img src="{{ url('/') }}/storage/app/public/loyalty/banner.jpg" style="width:100%">
            </div>
            <div data-v-72bbedf8="" class="content-section">

                <!--                <div data-v-506590fb="" data-v-72bbedf8="" class="rewards-container">
                                    <div data-v-61fb2e0f="" data-v-506590fb="" class="heading-container">
                                        <div data-v-61fb2e0f="" class="title">Beauty Pass Rewards</div>
                                        <div data-v-61fb2e0f="" class="description">Browse an exciting collection of must-have deluxe samples and curated sample sets from top brands.</div>
                                    </div>
                
                                    <a data-v-506590fb="" href="/rewards-boutique" class="view-rewards">
                                        <div data-v-506590fb="" class="text">View All Rewards</div>
                                        <div data-v-506590fb="" class="arrow">
                
                                        </div>
                                    </a>
                
                                </div>-->

                <hr data-v-72bbedf8="" class="hr">
                <div data-v-20494ef0="" data-v-72bbedf8="" class="gifts-container" style="height:auto">

                    <div data-v-20494ef0="" class=" row">
                        <div data-v-61fb2e0f="" data-v-20494ef0="" class="heading-container col-xs-12 col-lg-4">
                            <div data-v-61fb2e0f="" class="title">Complimentary Gifts &amp; Services</div>
                            <div data-v-61fb2e0f="" class="description">Enjoy more amazing rewards and perks when you upgrade to the next tier.</div>

                        </div>
                        <div data-v-20494ef0="" class="gift col-xs-12 col-md-6 col-lg-4">
                            <div data-v-20494ef0="" class="img img-bday" style="background-image: url({{ url('/') }}/storage/app/public/loyalty/Icon-1.jpg);"></div>
                            <div data-v-20494ef0="" class="title">birthday</div>
                            <div data-v-20494ef0="" class="description">Enjoy birthday gifts during your birthday month.</div>
                        </div>
                        <div data-v-20494ef0="" class="gift col-xs-12 col-md-6 col-lg-4">
                            <div data-v-20494ef0="" class="img img-upgrade" style="background-image: url({{ url('/') }}/storage/app/public/loyalty/Icon-2.jpg);"></div>
                            <div data-v-20494ef0="" class="title">tier upgrade</div>
                            <div data-v-20494ef0="" class="description">Receive an exclusive perks with each tier upgrade</div>

                        </div>
                    </div>

                </div>
                <hr data-v-72bbedf8="" class="hr">
                
                <div data-v-72bbedf8="" class="points-container row">
                    <div class="col-xs-12 col-sm-5 col-md-6 verticle-center">
                        <div data-v-04702ec3="" class="heading-container">
                                <div data-v-61fb2e0f="" class="title">Accumulate Points</div>
                                <div data-v-61fb2e0f="" class="description">Earn points every time you shop, connect and share with us.</div>
                        </div>
                    </div>
                    <div class="points col-xs-12  col-sm-7 col-md-6">
                        <div data-v-04702ec3="" class="point col-xs-12">
                            <div data-v-04702ec3="" class="img icon-1"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Shop with Pride</div>
                                <div data-v-04702ec3="" class="description">1 Point for every Tk 100 spent</div>

                            </div>

                        </div>
                        <div data-v-04702ec3="" class="point col-xs-12">
                            <div data-v-04702ec3="" class="img icon-3"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Sign up to program</div>
                                <div data-v-04702ec3="" class="description">+100 points</div>

                            </div>

                        </div>
                        <div data-v-04702ec3="" class="point col-xs-12">
                            <div data-v-04702ec3="" class="img icon-5"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Register your account online</div>
                                <div data-v-04702ec3="" class="description">+50 points</div>

                            </div>

                        </div>
                        <div data-v-04702ec3="" class="point col-xs-12">
                            <div data-v-04702ec3="" class="img icon-2"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Celebrate your Birthday</div>
                                <div data-v-04702ec3="" class="description">Redeem gifts in your birthday month</div>

                            </div>

                        </div>
                        <div data-v-04702ec3="" class="point col-xs-12">
                            

                        </div>
                        <!--<div data-v-04702ec3="" class="point">
                            <div data-v-04702ec3="" class="img icon-6"></div>
                            <div data-v-04702ec3="" class="detail">
                                <div data-v-04702ec3="" class="title">Download the mobile app</div>
                                <div data-v-04702ec3="" class="description">+40 points</div>

                            </div>

                        </div> --->

                    </div>

                </div>
                
                <hr data-v-72bbedf8="" class="hr">
                <div data-v-be2d245e="" data-v-72bbedf8="" class="membership-container">
                    <div data-v-be2d245e="" class="title">membership levels</div>
                    <table data-v-be2d245e="" class="levels-table">
                        <tr data-v-be2d245e="" class="tr">
                            <td data-v-be2d245e="" class="td"></td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="svg-icon white-card card-icon" style="background:url({{ url('/')}}/storage/app/public/card3.jpg); background-size: 100% 100%;"></div>
                            </td><td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="svg-icon black-card card-icon" style="background:url({{ url('/')}}/storage/app/public/card2.jpg); background-size: 100% 100%;"></div>
                            </td><td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="svg-icon gold-card card-icon" style="background:url({{ url('/')}}/storage/app/public/card1.jpg); background-size: 100% 100%;"></div>
                            </td>
                        </tr>
                        <tr data-v-be2d245e="" class="tr">
                            <td data-v-be2d245e="" class="td">Spend per calendar year</td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="span">Tk 3000 or more</div>
                            </td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="span">Tk 50,000</div>
                            </td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="span">Tk 100,000</div>
                            </td>
                        </tr>
                        <tr data-v-be2d245e="" class="tr">
                            <td data-v-be2d245e="" class="td">Insider Rewards</td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="span">1 point = Tk 1</div>
                            </td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="span">1 point = Tk 1.50</div>
                            </td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="span">1 point = Tk 2</div>
                            </td>
                        </tr>
						<tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Birthday Rewards</td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="dot red-dot"></div>
                            </td><td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="dot black-dot"></div>
                            </td>
                            <td data-v-be2d245e="" class="td">
                                <div data-v-be2d245e="" class="dot gold-dot"></div>
                            </td>
						</tr>
						<tr data-v-be2d245e="" class="tr">
                            <td data-v-be2d245e="" class="td">Special Occasion Promotions</td>
                            <td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot red-dot"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot black-dot"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td>
						</tr>
						<tr data-v-be2d245e="" class="tr">
							<td data-v-be2d245e="" class="td">Members-only Promotions</td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot red-dot"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot black-dot"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td>
						</tr>
						<tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Welcome gift</td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot black-dot"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td>
						</tr>
						<tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Birthday gift</td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot black-dot"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td>
						</tr>
						<tr data-v-be2d245e="" class="tr"><td data-v-be2d245e="" class="td">Private sale early access</td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td>
						</tr>
						<tr data-v-be2d245e="" class="tr">
							<td data-v-be2d245e="" class="td">Free Delivery on all online purchases</td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td>
						</tr>
						<tr data-v-be2d245e="" class="tr">
							<td data-v-be2d245e="" class="td">Personalised Membership Card</td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td>
						</tr>
						<tr data-v-be2d245e="" class="tr">
							<td data-v-be2d245e="" class="td">Personal Shopping with Fashion Influencer</td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="span"></div></td>
							<td data-v-be2d245e="" class="td"><div data-v-be2d245e="" class="dot gold-dot"></div></td>
						</tr>
					</table>
				</div>
				<hr data-v-72bbedf8="" class="hr">
				<div data-v-0834b57d="" data-v-72bbedf8="" class="how-container">
					<div data-v-0834b57d="" class="title">how it works</div>
					<div data-v-0834b57d="" class="hows">
						<div data-v-0834b57d="" class="how"><div data-v-0834b57d="" class="img-section"><div data-v-0834b57d="" class="img img-create"></div></div>
							<div data-v-0834b57d="" class="description">
								<div data-v-0834b57d="" class="title">Become a member</div>
								<div data-v-0834b57d="" class="detail">Shop Tk 2000 or more and join for free!</div>
							</div>
						</div>
						<div data-v-0834b57d="" class="how">
							<div data-v-0834b57d="" class="img-section">
								<div data-v-0834b57d="" class="img img-earn"></div>
							</div>
							<div data-v-0834b57d="" class="description">
								<div data-v-0834b57d="" class="title">Earn Points</div>
								<div data-v-0834b57d="" class="detail">Every Tk 100 spent earns you one point.</div>
							</div>
						</div>
						<div data-v-0834b57d="" class="how">
							<div data-v-0834b57d="" class="img-section">
								<div data-v-0834b57d="" class="img img-rewarded"></div>
							</div>
							<div data-v-0834b57d="" class="description">
								<div data-v-0834b57d="" class="title">get rewarded</div>
								<div data-v-0834b57d="" class="detail">Use points to redeem exclusive rewards.</div>
							</div>
						</div>
						<div data-v-0834b57d="" class="how">
							<div data-v-0834b57d="" class="img-section">
								<div data-v-0834b57d="" class="img img-upgrade"></div>
							</div>
							<div data-v-0834b57d="" class="description"><div data-v-0834b57d="" class="title">upgrade</div>
								<div data-v-0834b57d="" class="detail">Advance to next tier for even more benefits.</div>
							</div>
						</div>
					</div>
				</div>
			<hr data-v-72bbedf8="" class="hr">
			<div data-v-6524658e="" data-v-72bbedf8="" class="join-container">
				<div data-v-6524658e="" class="title">Join Pride Insider Rewards</div>
				<div data-v-6524658e="" class="description">When you sign up for Insider Rewards, you'll instantly receive 100 bonus points that you can use to redeem rewards. Get started and sign up now.</div>
				<div data-v-6524658e="" class="sessions"><a data-v-6524658e="" href="{{url('/login')}}" class="btn register">Register</a><a data-v-6524658e="" href="{{url('/login')}}" class="btn sign-in">Sign In</a></div>
				<div data-v-6524658e="" class="title" style="padding-top:13px;">Sign Up in Stores</div>
				<div class="row" style="text-align: left;padding-top:10px;">
				   <div class="col-md-3">1. Pride- Banani, Dhaka.</div>
				   <div class="col-md-3">2. Pride – Dhanmondi, Dhaka</div>
				   <div class="col-md-3">3. Pride – Baily Road, Dhaka.</div>
				   <div class="col-md-3">4. PRIDE - Rapa Plaza</div>
				   <div class="col-md-3">5. PRIDE - Dhaka New Market</div>
				   <div class="col-md-3">6. PRIDE - Mirpur 10</div>
				</div>
			</div>
			<hr data-v-72bbedf8="" class="hr">
			<div data-v-0f0c09c8="" data-v-72bbedf8="" class="faqs-container">
				<div data-v-0f0c09c8="" class="h3 title">FAQs</div>
				<div data-v-0f0c09c8="" class="faqs">
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-0" data-parent="#faq-0-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">How do I redeem Rewards?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-0" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">You can avail rewards at any Pride store and Urban Truth shop-in-shop, simply by giving your card to the sales clerk, or your membership number at the point of purchase. You can choose your desired rewards, based on your point balance, upto 20% at a time. All Rewards can be redeemed only upon purchase of an item. 
<br><br>We currently allow you to only become an Insider Member by registering online. Redeeming points online is currently not available.

							</div>
						</div>
					</div>
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-1" data-parent="#faq-1-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">How can I redeem my Birthday Gift?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-1" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">Gold and Platinum Members are eligible to redeem one birthday gift per year during their birthday month. To redeem in-store, no purchase is necessary when you are redeeming your birthday gift. All you have to do is select one item only from our stores to redeem as your Birthday Gift. This offer is subject to change, alteration, or termination by Pride Limited at its sole discretion at any time.</div>
						</div>
					</div>
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-2" data-parent="#faq-2-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">How do I become a Gold or Platinum member and what is the validity of my tier status?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-2" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">To become a Gold Member, you will need to spend an accumulated value of Tk 50,000 within 12 months of signing up for Pride Insider Rewards. Your status is valid for a 12 month period. To become a Platinum Member, you will need to spend an accumulated value of Tk 100,000 within 12 months of getting qualified for Platinum Member Status. Your status is valid for a 12 month period.</div>
						</div>
					</div>
					<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-3" data-parent="#faq-3-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">How do I maintain my Gold or Platinum member tier status?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-3" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							    To maintain any tier status, make sure your account is active by shopping any Taka amount and earning points at least once a month, for a period of 12 months since your registration or tier upgrade. Points do not carry over to the next active year so you must redeem your points within the current 12 months.
<br><br>
To maintain your Gold tier status, and to move to the next tier, you need to shop an accumulated amount of Tk 100,000 within 12 months. To maintain your Platinum tier status, and to move to the next tier, keep shopping any amount every month and redeem your points.
<br><br>
If your account is inactive for more than 12 months, you still hold your current tier status, but your redemption status is demoted to previous tier benefit. Example, if you get demoted, you get to stay Gold Member status, however you can only redeem 1 point = Tk 1.  

							</div>
						</div>
						
						
					</div>
				<div data-v-0f0c09c8="" aria-multiselectable="true" role="tablist" class="faq accordion">
						<div data-v-0f0c09c8="" onclick="" data-target="#faq-4" data-parent="#faq-4-accordion" role="tab" data-toggle="collapse" class="collapsed"><i data-v-0f0c09c8="" class="collapse-arrow"></i>
							<div data-v-0f0c09c8="" class="question">What is the Pride Insider Rewards Program?</div>
						</div>
						<div data-v-0f0c09c8="" id="faq-4" role="tabpanel" class="collapse">
							<div data-v-0f0c09c8="" class="answer">
							    Pride Insider Rewards Program is a rewards program (“Program”) that allows you to earn points, redeem rewards (“Rewards”), receive member exclusive promotions, welcome and birthday surprises and the latest fashion news. The Program is available in-stores and online for all showrooms in Dhaka and online for all stores in Bangladesh,  and requires a single purchase of Tk 2000 or more to join.
<br><br>
How do I earn Pride Insider Rewards points?
<br>
<br>
You can earn points through any of the methods given below:
<br>
<br>
<ul>
    <li>Every Tk 100 spent = 1 point</li>
    <li>Program sign-up = 100 points (one-time only)</li>
    <li>Register online = 50 points</li>
</ul>
<br>
<br>
Are there any limitations to the number of Rewards I can redeem in a single purchase?
<br><br>
Unfortunately, due to limited quantities, you can only redeem one unit of any particular Reward in a single purchase in a single day, from a single showroom. 
							</div>
						</div>
					</div> 
				</div>
				<a data-v-0f0c09c8="" href="{{url('/insider-rewards-faq')}}" class="view-faqs">
					<div data-v-0f0c09c8="" class="text">VIEW ALL FAQS</div>
					<div data-v-0f0c09c8="" class="arrow"></div>
				</a>
			</div>
		</div>
	</div>
</main>
@endsection