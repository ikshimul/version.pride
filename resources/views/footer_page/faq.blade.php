@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<style>
   
    .panel-default>.panel-heading {
        color: #333;
        background-color: #f5f5f5;
        border-color: #ddd;
    }
    .panel-heading {
        padding: 10px 15px;
        border: 1px solid transparent;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }
    .panel-group .panel+.panel {
        margin-top: 5px;
    }
    .panel-group {
        margin-bottom: 20px;
    }
    .panel-group .panel {
        margin-bottom: 0;
        border-radius: 4px;
    }
    .well, .label, .alert, .progress, .form-control, .modal-content, .panel-heading, .panel-group .panel, .nav-tabs > li > a, .nav-pills > li > a {
        border-radius: 2px;
    }
    .panel-title {
        margin-top: 0;
        margin-bottom: 0;
        font-size: 16px;
        color: inherit;
    }
    :after, :before {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .panel-default>.panel-heading+.panel-collapse>.panel-body {
        border-top-color: #ddd;
    }
    .panel-group .panel-heading+.panel-collapse>.list-group, .panel-group .panel-heading+.panel-collapse>.panel-body {
        border: 1px solid #ddd;
    }
    .panel-body {
        padding: 15px;
    }
    .h1, h1 {
        font-size: 25px;
    }
    h1.page-header {
        color: #291e88;
    }
    ul li.exchange {
        list-style-type: circle;
        margin-left:30px;
        padding:6px;
    }
    ul li a.exchange {
        color:#00f;
    }
    h4.panel-title {
    text-transform: uppercase;
   }
   .hyperlink{
    text-decoration: underline;
    color: blue;
   }
</style>
<main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>FAQ</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container"><!-- Container start -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <h1 class="page-header">FAQ
                                    <small>Frequently Asked Questions</small>
                                </h1>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">  
                            <div class="col-md-1"></div>                     
                            <div class="col-md-10" style="color:black;font-size:15px;">
                                <div class="panel-group" id="accordion">

                                    <div class="panel panel-default active">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                <h4 class="panel-title">
                                                    1.	Registration
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in collapse">
                                            <div class="panel-body">

                                                <b>1.	Do I Need To Register Before Placing An Order?</b>
                                                <p>You need to register before placing an order. You register as soon as you have finished shopping, right before checkout. Our registration process is fast, free, and will save you time for future purchases. <a href="{{url('/login')}}">Click here</a> to register.</p>
                                                <b><p>2.	How Do I Register?</b></p>
                                                <p>You can <a class="hyperlink" href="{{url('/login')}}">Click here</a> to register.</p>
                                                <b><p>3.	Why Should I Become A Registered User?</b></p>
                                                <p>You will not be required to re-enter your shipping  addresses every time you order online. Whenever you place an order, it will be delivered to the registered address on file, unless you direct us otherwise. Additionally, you will be missing out on several benefits for registered users such as such as, such as modifying, cancelling or paying later for your current order. </p>
                                                <b><p>4.	How Do I Reset My Password?</b></p>
                                                <p>Please <a class="hyperlink" href="{{url('/login')}}">Click here</a>  to reset your password.</p>
                                                <b><p>5.	How Do I Change My Account Information?</b></p>
                                                <p>As soon as you <a class="hyperlink" href="{{url('/login')}}">sign in</a>  to your account with your email address and password, it will direct you to your account overview to update/edit your account information.</p>
                                                <b><p>6.	I Cannot Find The Answers To My Questions, How Do I Reach Customer Service?</b></p>
                                                <p>Please <a class="hyperlink" href="{{url('/contact-us')}}">Click here</a> to contact our Customer Service Team.</p>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                <h4 class="panel-title">
                                                    2. Ordering
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <b> 1.	Can I order by phone?</b>
                                                <p><p>Unfortunately, we do not take orders over the phone at this time. However, you may order directly from our website on a desktop or a mobile device. <br><a class="hyperlink" href="{{url('/')}}">Click here</a> to shop.</p>
                                                <br>
                                                <b><p>2.	What kinds of payment methods do you accept?</b></p>
                                                <p>We accept the following forms of payment:</p>
                                                <ul style="padding-left: 10px;padding-bottom: 15px;">
                                                    <li><strong>1. Cash on Delivery (for Dhaka customers only)</strong></li>
                                                    <li><strong>2. bKash</strong></li>
                                                    <!--<li><strong>3. iPay</strong></li>-->
                                                    <li><strong>4. Credit/ Debit Card for online payments</strong></li>
                                                </ul>


                                                <b>3.	Is My Privacy And Personal Information Secure On Your Site?</b></p></b>
                                                <p>Shopping on our site is safe and secure. Please <a class="hyperlink" href="{{url('/privacy-cookies')}}">Click here</a> to view the full details on how we protect your privacy and personal information.</p>


                                                <b><p>4.	Do you charge VAT on your items?</b></p>
                                                <p>By law, we are required to charge VAT. Our product prices are inclusive of VAT and thus, you will see no difference in our offline and online store prices.</p>


                                                <b><p>5.	Do You Restock Items That Are Sold Out?</b></p>
                                                <p>Unfortunately we do not restock most of our items. We do however, bring back some of our popular items. You may also used the find in store fecture on your desire product page in our website to see if a sold out item  or size or color is available in our physcial store.</p>
                                                <b><p>6.	Can I Modify or Cancel My Order? </b></p>
                                                <p>To ensure a super-fast delivery, we process your orders within 24 hours. Someone from our customer service will call you to confirm your order before shipping it. If you remain unavailable on phone for the next 24 hours, we will cancel it from our end. You cannot cancel your order after we have shipped it. Our delivery man will wait for 10 minutes for you to check the product. If there is any issue with the size, you can exchange the product after speaking with the customer service over phone. However, the delivery charge must be paid to the delivery man.</p>
                                                <b><p>7.	How Do I Know What Size To Choose?</b></p>
                                                <p><a href="{{url('/size-guide')}}">Click here</a> to view our Size Charts.</p>
                                                <b><p>8.	I Cannot Find The Answers To My Questions, How Do I Reach Customer Service? </b></p>
                                                <p>Please <a class="hyperlink" href="{{url('/contact-us')}}">Click here</a> to contact our Customer Service Team.</p>

                                                <p><p><strorng style="font-size:18px;font-weight:600;">Order Status</strorng></p>
                                                <p><p>1.	How Do I Check On The Status Of My Order?</p>
                                                <p><p>You may view the status of your order by logging into your Pride Limited account and clicking on My Orders. <a class="hyperlink" href="{{url('/track-order')}}">Click here</a> to check on that status of your order.</p>
                                                <p><p>Confirmations are sent via email within 30 minutes when 
                                                    Your order has been received or
                                                    Your shipment has been processed, based on the valid email address provided to us.</p>
                                                <p><p>If you did not receive any confirmation emails, please check your email spam filter. Select <a class="hyperlink" href="{{url('/')}}">www.pride-limited.com</a> a trusted website domain in your spam filter. To avoid any duplicate orders, please go to <a href="{{url('/my-account')}}">MY ACCOUNT</a> to verify your order has been placed before resubmitting your order.	Please allow 24 to 48 hours for your order status to change.</p>

                                                <p><p>	Can you explain the order statuses?</p>

                                                <p><p>In the process of delivering your item to you, your package goes through a series of stages. Here's a quick guide to easily decode the delivery statuses you see when tracking your order.</p>
                                                <table>
                                                    <tr>
                                                        <th>STATUS</th>
                                                        <th>MEANING</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Order Verification</td>
                                                        <td>Order is getting verified at our customer service. You will receive a call from our customer service for verification.</td>

                                                    </tr>

                                                    <tr>
                                                        <td>Pending Prepayment</td>
                                                        <td>The team is waiting for customers outside Dhaka to prepay first.</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Pending Dispatch</td>
                                                        <td>Order is being packed and processed.</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Dispatched</td>
                                                        <td>Order has been given to the courier company for delivery.</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Closed</td>
                                                        <td>Order has been delivered and payment has been received.</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Returned/Exchanged</td>
                                                        <td>Order has been exchanged.</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Cancelled</td>
                                                        <td>Order has been cancelled.</td>
                                                    </tr>
                                                </table>



                                                <p><p><b>Why Was My Order Cancelled?</b></p>
                                                <p><p>There may be few reasons for cancellation. If Pride Limited customer service is unable to reach the customer within the next 24 hours, the order is cancelled. Other reasons like product being out of stock, customer unavailable during time of delivery, non-receipt of prepayment, duplicate orders are the basis of cancellations.</p></p>
                                                <p><p>If your order has been cancelled, please place a new order. You may also contact our customer service to resolve any issues with order cancellation.</p>

                                                <p><p>I Cannot Find The Answers To My Questions, How Do I Reach Customer Service? 
                                                    Please <a class="hyperlink" href="{{url('/contact-us')}}">Click here</a>to contact our Customer Service Team.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                <h4 class="panel-title">
                                                    3. Shipping
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p><p><b>1.	What Kinds Of Shipping Options Do You Offer?</b></br>
                                                    <p>For customers outside Dhaka, prepayment must be paid via bKash or card and then we ship the order via our logistic partners.</p>
                                                <p><p> Free shipping is available for purchases worth TK 3000 and above.</p>
                                                <p><p>Our rates are as follows:</p>

                                                <table>
                                                    <tr>
                                                        <td>Express delivery (Inside Dhaka)</td>
                                                        <td>TK 120</td>
                                                        <td>1 to 2 business days (Except Friday & Govt. holiday)</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Standard delivery (Inside Dhaka)</td>
                                                        <td>TK 100</td>
                                                        <td>3 to 5 business days</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Outside Dhaka</td>
                                                        <td>TK 150</td>
                                                        <td>3 to 7 Business Days</td>
                                                    </tr>
                                                </table>
                                                <p><p><b>2.Which International Countries Do You Ship To?</b></p>
                                                <p><p>Currently, we do not ship outside Bangladesh.</p>

                                                <p><p><b>3.	I Cannot Find The Answers To My Questions, How Do I Reach Customer Service?</b></p> 
                                                <p><p>Please<a href="{{url('/contact-us')}}"> Click here </a> to contact our Customer Service Team.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsebKash">
                                                <h4 class="panel-title">
                                                    4. bKash Payment
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapsebKash" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <b><p>bKash Payment Instruction : </b></p>
                                                <ol>
                                                    <li>1. Go to bKash Menu by dialing <strong>*247#</strong>.</li>
                                                    <li>2. Choose <strong>payment</strong>.</li>
                                                    <li>3. Enter Merchant bKash Wallet No <strong>01939050505</strong>.</li>
                                                    <li>4. Enter the amount of your order value.</li>
                                                    <li>5. Enter 1 as a reference No: or you can mention the purpose of the transaction in one word. e.g. Bill.</li>
                                                    <li>6. Enter 1 as Counter No: .</li>
                                                    <li>7. Enter your Menu PIN to confirm.</li>
                                                    <li>8. Done! You will receive a confirmation SMS.</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                                <h4 class="panel-title">
                                                    5. Returns
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseFour" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p><p><b>1.	What is your return policy?</b> </p>
                                                <p><p>Pride Limited does not refund any products. However, we do exchange products at no extra delivery charge if:</p>
                                                <p><p>•	We have sent you the wrong product, size or color;</p>
                                                <p><p>•	IF it is a faulty product</p>
                                                The exchange must be made within 15 days of purchase and the product, product package and the hangtag must be intact. The original invoice must be presented during exchange. Call our customer service to exchange the product. 
                                                You can return your items through courier service to our e-commerce warehouse at the following address and then we will ship the right sized/colored product to you.</p>
                                                <b><p>
                                                    <p>E-Commerce Warehouse</p>
                                                    <p>Pride Group
                                                    <p>Pride Hamza, House 54, Road 1, Sector 6, Uttara, Dhaka
                                                    <p>Dhaka, Bangladesh</p>
                                                </b>
                                                <!--<p><p>To find a store location near you, go to Store locator.</p>-->
                                                <br>
                                                <p><p><b>2. I Cannot Find The Answers To My Questions, How Do I Reach Customer Service?</b> </p>
                                                <p><p>Please<a href="{{url('/contact-us')}}"> Click here </a> to contact our Customer Service Team.</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse"  data-parent="#accordion" href="#forstore">
                                                6.	FOR PURCHASES MADE WITH CARD PAYMENTS
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="forstore"  class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul>
                                                <li class="exchange">In the event of a Customer effecting a transaction by using a Payment Card, the Merchant may request SSL for a refund on any grounds whatsoever within a period of seven (07) days from the actual delivery of the product, then SSL shall be entitled to cancel authorization and refuse to make any payments to the Merchant and/or enforce a refund from the Merchant. </li>
                                                <li class="exchange">
                                                    SSL shall forthwith inform the Merchant of the same and shall debit or collect the payment from the Merchant's Account and make an intermediate credit to SSL's 2. Account in favor of the said Customer. 
                                                </li>
                                                <li class="exchange">
                                                    In the event the Merchant accepts a customer order/agrees to provide the service to the Customer but however subsequently notifies to SSI about the Merchant's inability to comply with a customer order/service- Merchant shall forthwith make a proper cancellation for giving effect to the same as well as provide the funds in their account to facilitate a refund of the amount due to the customer.
                                                </li>
                                                <li class="exchange">
                                                    In the event of a refund to a cardholder In respect of any transaction of any goods,/ services that are not received as ordered by the Cardholder or are lawfully rejected or services are not performed or partly performed or cancelled or price is lawfully disputed by the Cardholder or price adjustment is not allowed or for any other reason whatsoever, a credit slip shall be issued by the Merchant to SSL.
                                                </li>
                                                <li class="exchange">
                                                    Within seven (07) to nine (09) working days after the refund has been agreed between the Merchant and the cardholder and SSL shall: i. Debit the Merchant's account forthwith; and/or ii. Deduct the outstanding amount from the Merchant's account; and/or iii. If there is insufficient funds available in Merchant's account, SSL shall claim from the Merchant the amount to be refunded to the Customer. 
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse"  data-parent="#accordion" href="#cardpayment">
                                            7.	IN THE CASE OF VOID TRANSACTIONS MADE WITH CREDIT/DEBIT CARD PAYMENTS
                                        </a>
                                    </h4>
                                </div>
                                <div id="cardpayment"  class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li class="exchange">
                                                If a Transaction is an Invalid or Fraud Transaction, Bank may, with prior discussion with SSL (and without a request or demand from a Cardholder), Refuse to accept the Transaction; or
                                            </li>
                                            <li class="exchange">
                                                If the Transaction has been processed, at any time within 180 days of the date of the Transaction, charge that Transaction back to the Customer by debiting the Merchant Account or otherwise exercising its rights under the MEF. 
                                            </li>
                                            <li class="exchange">
                                                Merchant is liable to verify the shipping address before delivering any product to customer's shipping address. 
                                            </li>
                                            <li class="exchange">
                                                Merchant shall not deliver a product in an address other than the address mentioned as delivery address by the Customer. If card holder claims for charge back, merchant must submit proper delivery receipts & invoices, otherwise Merchant will bear the liability of charge back.
                                            </li>
                                            <li class="exchange">
                                                Here, Invalid or Fraud Transaction shall mean i. The transaction and its records are illegal; ii. The particulars inserted in the order form are not identical with the actual particulars identifying the Cardholder; iii. The Nominated Card is invalid at the time of transaction; iv. The Nominated Card is listed on the Warning Bulletin issued to SSL; v. The Merchant has failed to observe the MEF in relation to the transaction; vi. The Nominated Card was used without the consent of the Cardholder.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                                    
                                    <!--
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                                <h4 class="panel-title">
                                                    6. Eid 2018 Order Status
                                                </h4>
                                            </a>
                                        </div>
                                        <div id="collapseFive" class="panel-collapse collapse collapse">
                                            <div class="panel-body">
                                                <p> <b>Q. When is the last date for placing Eid orders?</b></p>
                                                <p>A. Customers inside Dhaka can order before 14<sup>th</sup> June 2018, Thursday to get delivery before Eid. Customers outside Dhaka city must order before the 11<sup>th</sup> of June to receive before Eid. All orders placed   after the deadline will be processed after Eid holidays.</p>                                                                                                            
                                            </div>
                                        </div>
                                    </div>
                                    --->
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
    </div>
</main>
@endsection