@extends('layouts.app')
@section('title','Pride Limited | Size Guide')
@section('content')
<style>
   
    .panel-default>.panel-heading {
        color: #333;
        background-color: #f5f5f5;
        border-color: #ddd;
    }
    .panel-heading {
        padding: 10px 15px;
        border: 1px solid transparent;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }
    .panel-group .panel+.panel {
        margin-top: 5px;
    }
    .panel-group {
        margin-bottom: 20px;
    }
    .panel-group .panel {
        margin-bottom: 0;
        border-radius: 4px;
    }
    .well, .label, .alert, .progress, .form-control, .modal-content, .panel-heading, .panel-group .panel, .nav-tabs > li > a, .nav-pills > li > a {
        border-radius: 2px;
    }
    .panel-title {
        margin-top: 0;
        margin-bottom: 0;
        font-size: 16px;
        color: inherit;
    }
    :after, :before {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .panel-default>.panel-heading+.panel-collapse>.panel-body {
        border-top-color: #ddd;
    }
    .panel-group .panel-heading+.panel-collapse>.list-group, .panel-group .panel-heading+.panel-collapse>.panel-body {
        border: 1px solid #ddd;
    }
    .panel-body {
        padding: 15px;
    }
    .h1, h1 {
        font-size: 25px;
    }
    h1.page-header {
        color: #291e88;
    }
    ul li.exchange {
        list-style-type: circle;
        margin-left:30px;
        padding:6px;
    }
    ul li a.exchange {
        color:#00f;
    }
    h4.panel-title {
    text-transform: uppercase;
   }
   .hyperlink{
    text-decoration: underline;
    color: blue;
   }
   .size-chart-tabel table{
       font-size:14px;
   }
   .page-main{
    	    padding-top:64px;
    	}
   @media only screen and (max-width: 600px) {
       .page-main{
    	    padding-top:0px;
    	}
   }
</style>
<main id="maincontent" class="page-main" >
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Size Guide</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container"><!-- Container start -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="size-chart-tabel"><h1>CLOTHING SIZE CHART GIRLS</h1> 
                        <table width="100%" border="1"> 
                            <tr class="even"> 
                                <th scope="col">GIRLS</th> 
                                <th scope="col">MEASURE YOUR</th> 
                                <th scope="col">XS</th> 
                                <th scope="col">S</th> 
                                <th scope="col">M</th> 
                                <th scope="col">L</th> 
                                <th scope="col">XL</th> 
                                <th scope="col">XXL</th> 
                                <th scope="col">XXXL</th> 
                            </tr> 
                            <tr > 
                                <th scope="row">REGULAR FIT TUNIC</th> 
                                <th scope="row">CHEST</th> 
                                <td></td> 
                                <td>38"</td> 
                                <td>39.5"</td> 
                                <td>41"</td> 
                                <td>42.5"</td> 
                                <td>44"</td> 
                                <td>45.5"</td>  
                            </tr> 
                            <tr class="even"> 
                                <th scope="row">TAILORED FIT TUNIC</th> 
                                <th scope="row">CHEST</th> 
                                <td></td> 
                                <td>36"</td> 
                                <td>37.5"</td> 
                                <td>39"</td> 
                                <td>40.5"</td> 
                                <td>42"</td> 
                                <td>43.5"</td> 
                            </tr> 
                            <tr > 
                                <th scope="row">PANT</th>
                                <th scope="row">WAIST</th> 
                                <td>26"</td> 
                                <td>28"</td> 
                                <td>30"</td> 
                                <td>32"</td> 
                                <td>34"</td> 
                                <td>36"</td> 
                                <td>38"</td> 
                            </tr> 
                            <tr class="even"> 
                                <th scope="row">LEGGINGS</th> 
                                <th scope="row">WAIST</th> 
                                <td></td> 
                                <td>26"</td> 
                                <td>27"</td> 
                                <td>28"</td> 
                                <td>29"</td> 
                                <td>33"</td> 
                                <td>31"</td> 
                            </tr> 
                        </table> 
                        <span class="note">*Measurements in inches</span> 
                    </div>
                    <div class="size-chart-tabel"><h1>PANJABI MEASUREMENT SHEET</h1> 
                        <table width="100%" border="1"> 
                            <tr class="even"> 
                                <th scope="col">Side Chart in cm</th> 
                                <th scope="col">&nbsp;</th> 
                                <th scope="col">38</th> 
                                <th scope="col">40</th> 
                                <th scope="col">42</th> 
                                <th scope="col">44</th> 
                                <th scope="col">46</th> 
                            </tr> 
                            <tr > 
                                <th scope="row"></th> 
                                <th scope="row"></th> 
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                <td></td> 
                            </tr> 
                            <tr class="even"> 
                                <th scope="row">Chest width</th> 
                                <th scope="row">A</th> 
                                <td>51</td> 
                                <td>54</td> 
                                <td>57</td> 
                                <td>60</td> 
                                <td>63</td> 
                            </tr> 
                            <tr > 
                                <th scope="row">Waist width 40 cm down H.P.S</th>
                                <th scope="row">B</th> 
                                <td>51</td> 
                                <td>54</td> 
                                <td>57</td> 
                                <td>60</td> 
                                <td>63</td>
                            </tr> 
                            <tr class="even"> 
                                <th scope="row">Hip 60 cm down H.P.S</th> 
                                <th scope="row">C</th> 
                                <td>52</td> 
                                <td>55</td> 
                                <td>58</td> 
                                <td>61</td> 
                                <td>64</td>
                            </tr> 
							<tr> 
                                <th scope="row">Bottom width</th> 
                                <th scope="row">D</th> 
                                <td>54</td> 
                                <td>57</td> 
                                <td>60</td> 
                                <td>63</td> 
                                <td>66</td>
                            </tr> 
							<tr class="even"> 
                                <th scope="row">Front Length H.P.S</th> 
                                <th scope="row">E</th> 
                                <td>102.5</td> 
                                <td>105</td> 
                                <td>107.5</td> 
                                <td>110</td> 
                                <td>112.5</td>
                            </tr> 
							<tr> 
                                <th scope="row">Back Lernrth H.P.S</th> 
                                <th scope="row">F</th> 
                                <td>102.5</td> 
                                <td>105</td> 
                                <td>107.5</td> 
                                <td>110</td> 
                                <td>112.5</td>
                            </tr> 
							<tr class="even"> 
                                <th scope="row">Shoulder to shoulder</th> 
                                <th scope="row">G</th> 
                                <td>102.5</td> 
                                <td>105</td> 
                                <td>107.5</td> 
                                <td>110</td> 
                                <td>112.5</td>
                            </tr> 
							<tr> 
                                <th scope="row">Neck width IN TO IN</th> 
                                <th scope="row">H</th> 
                                <td>15.5</td> 
                                <td>16</td> 
                                <td>16.5</td> 
                                <td>17</td> 
                                <td>17.5</td>
                            </tr> 
							<tr class="even"> 
                                <th scope="row">Front neck drop</th> 
                                <th scope="row">I</th> 
                                <td>8</td> 
                                <td>8.5</td> 
                                <td>9</td> 
                                <td>9.5</td> 
                                <td>10</td>
                            </tr> 
							<tr> 
                                <th scope="row">Back neck drop</th> 
                                <th scope="row">J</th> 
                                <td>1.5</td> 
                                <td>1.5</td> 
                                <td>1.5</td> 
                                <td>1.5</td> 
                                <td>1.5</td>
                            </tr>
							<tr class="even"> 
                                <th scope="row">Sleeve length</th> 
                                <th scope="row">K</th> 
                                <td>62</td> 
                                <td>63</td> 
                                <td>64</td> 
                                <td>65</td> 
                                <td>66</td>
                            </tr>
							<tr> 
                                <th scope="row">Sleeve opening</th> 
                                <th scope="row">L</th> 
                                <td>15.5</td> 
                                <td>16</td> 
                                <td>16.5</td> 
                                <td>17</td> 
                                <td>17.5</td>
                            </tr>
							<tr class="even"> 
                                <th scope="row">Armhole straight</th> 
                                <th scope="row">M</th> 
                                <td>22</td> 
                                <td>23</td> 
                                <td>24</td> 
                                <td>25</td> 
                                <td>26</td>
                            </tr>
							<tr> 
                                <th scope="row">Upper arm</th> 
                                <th scope="row">N</th> 
                                <td>21</td> 
                                <td>22</td> 
                                <td>23</td> 
                                <td>24</td> 
                                <td>25</td>
                            </tr>
							<tr class="even"> 
                                <th scope="row">PLACKET  LENGTH</th> 
                                <th scope="row">O</th> 
                                <td>25</td> 
                                <td>25</td> 
                                <td>25</td> 
                                <td>25</td> 
                                <td>25</td>
                            </tr>
                        </table> 
                        <span class="note">*Measurements in cm</span> 
                    </div>  
                </div>
            </div>
            <div class="col-md-1"></div>
    </div>
</main>
@endsection