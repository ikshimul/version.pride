@extends('layouts.app')
@section('title','Order Confirm')
@section('content')
<link rel="stylesheet" media="all" href="{{asset('assets/css/order_confirm.css')}}?6" />
<style>
  .paymentMethodItem:hover {
    background: #c5e6f7;
    border: 1px solid #b4b9bb;
}
</style>
<main id="maincontent" class="page-main" style="padding-top: 64px;">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container">
            <div class="row">
                <div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                            </li>
                            <li class="item cms_page">
                                <strong>Order Confirm</strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap">
        <div class="main">
            @if (session('failed'))
			   <div class="alert alert-warning"><strong>Warning!</strong>
			      {{session('failed')}}
			  </div>
            @endif 
           @if (session('status'))
			   <div class="alert alert-success"><strong>Success!</strong>
			       {{session('status')}}
			   </div>
            @endif 	
            <div class="step__sections">
                <div class="">
                    <div class="section section--contact-information">
                        <div class="section__header">
                            <div class="layout-flex layout-flex--tight-vertical layout-flex--loose-horizontal layout-flex--wrap">
                                <h2 class="section__title layout-flex__item layout-flex__item--stretch" id="main-header">
                                    Your Order Has Been Confirmed!
                                </h2>
                            </div>
                        </div>									 
                    </div> 
                </div>
                <div class="section section--shipping-address">
                    <div class="section__header">
                        <h2 class="section__title">
                            Here is your tracking information
                        </h2>
                    </div>
                    <div class="section__content">
                        <div class="fieldset">
                            <div data-address-fields>
                                <div class="field--half field field--optional" data-address-field="first_name">
                                    <p>Order Tracking Number:&nbsp;<strong>PLORDER#100-{{$invoice_id}}</strong></p>
                                    <p> Please note, it may take 24 hours for your tracking number to return any information.</p>
                                    <p>  To track your order <a href="{{url('/track-order')}}" target="__blank" title="Track Your Order" style="text-decoration: underline;">click here</a>.</p>
                                    <p class="bodytxt" style="margin-top: 5px;color:#000000;"><a class="tracking" target="__blank" style="text-decoration: underline;" href="{{url('/contact-us')}}">Contact us for more information</a>.</p>   
                                </div>
                            </div>  
                        </div> 
                    </div> 
                </div>
            </div>
            <?php  if($conforderinfo->conforder_status !='Processing' && $conforderinfo->conforder_status !='Dispatched' && $conforderinfo->conforder_status !='Closed'){ ?>
            Please pay with <strong><?php
                if ($amount_info->payment_method == 'ssl') {
                    echo 'Credit / Debit Card';
                } else {
                    echo 'bKash';
                }
                ?></strong>
            <br>
            <br>
            <div class="main_payment">
                <input type="hidden" id="payment_method" value="<?php echo $amount_info->payment_method;?>" readonly />
                <input type="hidden" id="amount" value="<?php echo $amount_info->shoppingcart_total; ?>" readonly />
                <input type="hidden" id="invoice" value="<?php echo $invoice_id; ?>" readonly />
                <h4>Do you want to pay now with a different payment method?</h4>
                <div class="paymentMethodItem" id="sslczPayBtn"
						token="<?php echo uniqid().uniqid().uniqid(); ?>"
						postdata=""
						order="<?php echo $invoice_id; ?>" 					
						endpoint="{{url('/SSL-create')}}">
                    <div class="Portwallet paymentMethodItemContent">
                        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="background-image:url({{url('/')}}/storage/app/public/payment/payment_icons_portwallet.png?q=low&amp;webp=1&amp;alpha=1);"/>
                        <p>Credit / Debit Card</p>
                    </div>
                </div>
                <div class="paymentMethodItem" id="bKash_button">
                    <div class="Bkash paymentMethodItemContent">
                        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="background-image:url({{url('/')}}/storage/app/public/payment/bkash_pay.png?q=low&amp;webp=1&amp;alpha=1);"/>
                        <p>bKash</p>
                    </div>
                </div>
            </div>
            <?php
             } 
            ?>
        </div>
        <div class="sidebar checkout-block">
            <div id="order-summary" class="order-summary order-summary--is-collapsed" data-order-summary>
                <h2 class="">Delivery Address</h2>
                <div class="order-summary__section">
                    <div class="order-summary__section__content">
                        <span class="delivery-address">{{$orderinfo->Shipping_txtaddressname}}</span>
                    </div>
                </div>
                <h2 class="">Order summary</h2>
                <div class="order-summary__sections">
                    <div class="order-summary__section hidden-xs">
                    </div>
                    <div class="order-summary__section order-summary__section--total-lines" data-order-summary-section="payment-lines">
                        <table class="total-line-table">
                            <tbody class="total-line-table__tbody">
                                <tr class="total-line total-line--subtotal">
                                    <th class="total-line__name" scope="row">Subtotal</th>
                                    <td class="total-line__price">
                                        <span class="order-summary__emphasis" data-checkout-subtotal-price-target="220900">
                                            Tk <?php echo number_format($amount_info->shoppingcart_subtotal, 2); ?>
                                        </span>
                                    </td>
                                </tr>
                                <tr class="total-line total-line--shipping">
                                    <th class="total-line__name" scope="row">
                                        <span>
                                            Shipping
                                        </span>
                                    </th>
                                    <td class="total-line__price">
                                        <span class="order-summary__small-text" data-checkout-total-shipping-target="0">
                                            Tk <?php echo number_format($amount_info->shipping_charge, 2); ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot class="total-line-table__footer">
                                <tr class="total-line">
                                    <th class="total-line__name payment-due-label" scope="row">
                                        <span class="payment-due-label__total">Total</span>
                                        <span class="payment-due-label__taxes order-summary__small-text " data-checkout-taxes>
                                            Including <span><span>taxes</span>
                                    </th>
									<td class="total-line__price payment-due">
										<span class="payment-due__price">
											Tk <?php echo number_format($amount_info->shoppingcart_total, 2); ?>
										</span>
									</td>
                                </tr>
                            </tfoot>
                        </table>
                     </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    window.dataLayer = window.dataLayer || [];
    dataLayer.push({
    'event': 'purchase',
            'transactionId':"{{$conforderinfo->track_number}}",
            'transactionAffiliation': 'Pride Limited',
            'transactionTotal': "{{$amount_info->shoppingcart_total}}",
            'transactionTax': 0,
            'transactionShipping': "{{$amount_info->shipping_charge}}",
            'transactionProducts': {!! $order_product !!}
    });
    console.log(dataLayer);
</script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script type="text/javascript">
  (function (window, document) {
	var loader = function () {
		var script = document.createElement("script"), tag = document.getElementsByTagName("script")[0];
		script.src = "https://seamless-epay.sslcommerz.com/embed.min.js?" + Math.random().toString(36).substring(7);
		tag.parentNode.insertBefore(script, tag);
	};

	window.addEventListener ? window.addEventListener("load", loader, false) : window.attachEvent("onload", loader);
})(window, document);

// (function (window, document) {
//     var loader = function () {
//     var script = document.createElement("script"), tag = document.getElementsByTagName("script")[0];
//     script.src = "https://sandbox.sslcommerz.com/embed.min.js?" + Math.random().toString(36).substring(7);
//     tag.parentNode.insertBefore(script, tag);
//     };
//     window.addEventListener ? window.addEventListener("load", loader, false) : window.attachEvent("onload", loader);
//     })(window, document);
</script>
<!--<script src="https://scripts.sandbox.bka.sh/versions/1.2.0-beta/checkout/bKash-checkout-sandbox.js"></script>-->
<script src="https://scripts.pay.bka.sh/versions/1.2.0-beta/checkout/bKash-checkout.js"></script>
<script src="{{asset('assets/js/bkash.js')}}"></script>
<script>
    $(function () {
    var payment_method = $("#payment_method").val();
    console.log(payment_method);
    setTimeout(() => {
    if (payment_method == 'bKash'){
    $("#bKash_button").click();
    } else{
    $("#sslczPayBtn").click();
    $("#sslczPayBtn").show();
    }
    }, 2000);
    });
</script>
@endsection

