<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head ><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="theme-color" content="#291d88" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="Pride, Pride Limited as a brand of Pride Group,Shop from Pride Limited Online Store,Superbrand of bangladesh,Pop Art,Eid Collection,Sari" />
        <meta name="keywords" content="Pride, pride limited, pride limited Bangladesh, pride limited bd, pride limited company, pride limited dhaka Bangladesh ,pride limited jfp dhaka Bangladesh,  
              Pride Classic Collection, Pride Signature Collection, Pride Actinic  Menswear,  Pride Kids Collection, Pride Girls Collection, Pride Crafted, Pride Homes, Pride Classic sari, Pride Signature Sari, 
              Pride classic share, pride signature share, pride classic dupatta, pride classic unstitched three piece,  pride signature unstitched three piece, pride signature stitched three piece,urbantruthbd, 
              pride signature unstitched two piece, pride signature stitched two piece, pride signature unstitched one piece, pride signature stitched one piece,   
              pridegirls, pride girls formal, pride girls semi formal, pride girls casual, pride girls bottom, pride girls dupatta, pride kids boys, pride kids girls, new arrival, 
              pride women collection, pride group, pride online shopping, online ladies wear, online pride, online order, pride Panjabi, pride mans wear, pride long Panjabi, pride short Panjabi,
              pride shari, pride showroom, pride digital print collection, Pride banani 11, pride newmarket, pride Dhanmondi, pride Chittagong,pride bd,pride, bd pride,pride" />
        <meta name="robots" content="INDEX,FOLLOW"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="author" content="Pride Limited">
        <link rel="manifest" href="{{asset('assets/manifest.json')}}"/>
        <title>@yield('title')</title>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:wght@300&display=swap" rel="stylesheet">
        <link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/pride.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/bag-new.css')}}?11">
        <link rel="stylesheet" href="{{asset('assets/css/v3.css')}}?11">
        <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}?2">
        <link rel="stylesheet" href="{{asset('assets/fancybox/dist/jquery.fancybox.min.css')}}" />
        <script src="{{asset('assets/js/firebase/firebase-app.js')}}"></script>
        <script src="{{asset('assets/js/firebase/firebase-messaging.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/web-push.js')}}?3"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/jquery.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/bootstrap.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/jquery.main.js')}}"></script>
        <script  type="text/javascript"  src="{{asset('assets/js/pace.js')}}"></script>
        <script async  src="{{asset('assets/js/facebook.js')}}"></script>
        <meta property="fb:app_id" content="258246005086458" />
        <?php if (isset($show_fb)) { ?>
            <meta property="og:title" content="{{ $alt }}" />
            <meta property="og:description" content="{{ $description}}" />
            <meta property="og:url" content="{{url()->current()}}" />
            <meta property="og:image" content="{{ url('/')}}/storage/app/public/pgallery/{{ $image }}" />
            <meta property="og:image:secure_url" content="{{ url('/')}}/storage/app/public/pgallery/{{ $image }}" />
            <meta property="og:image:type" content="image/jpeg" />
            <meta property="og:image:width" content="400" />
            <meta property="og:image:height" content="300" />
            <meta property="og:image:alt" content="{{ $alt}}" />
            <meta property="og:type" content="product" />
            <meta property="product:brand" content="Pride Limited">
			<meta property="product:availability" content="in stock">
			<meta property="product:condition" content="new">
			<meta property="product:price:amount" content="{{$product_price}}">
			<meta property="product:price:currency" content="BDT">	
		    <meta property="product:retailer_item_id" content="{{$retailer_item_id}}">
			<meta property="product:item_group_id" content="{{$category_name}}">
        <?php } else { ?>
            <meta property="og:image" content="https://pride-limited.com/storage/app/public/slider/1569494824_web-banner2.jpg" />
            <meta property="og:image:secure_url" content="https://pride-limited.com/storage/app/public/slider/1569494824_web-banner2.jpg" />
            <meta property="og:image:type" content="image/jpeg" />
            <meta property="og:image:width" content="400" />
            <meta property="og:image:height" content="300" />
            <meta property="og:image:alt" content="Pop Art collection" />
            <meta property="og:type" content="product" />
            <meta property="og:url" content="{{url()->current()}}" />
            <meta property="og:title" content="Pride Limited | A Pride Group Venture" />
            <meta property="og:description" content="Shop from Pride Limited Online Store" />
        <?php }
        ?>
        <link  rel="icon" type="image/x-icon" href="{{ URL::to('') }}/storage/app/public/img/logo.png" />
        <link  rel="shortcut icon" type="image/x-icon" href="{{ URL::to('') }}/storage/app/public/img/logo.png" />
        <!--<meta name="google-site-verification" content="soQvn3sDBt1wfXIsdtZGdXQygBJtZ-JwgnknduoanMI" />-->
        <meta name="google-site-verification" content="fyb3DuF2aXKewOMD9iUP_rF0FUaVQQ5pv4hRWCpazCo" />
        <!--<meta name="google-signin-client_id" content="368187051828-20gl2gm34m24t4k23l78t7m8j7uecjt2.apps.googleusercontent.com">-->
         <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script> 
         <link rel="stylesheet" type="text/css" media="all" href="{{asset('assets/font-awesome-4.7.0/css/font-awesome.min.css')}}"/>
         @livewireStyles
        @yield('appended.style')
        <script>
          function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
            console.log('statusChangeCallback');
            console.log(response);                   // The current login status of the person.
            if (response.status === 'connected') {   // Logged into your webpage and Facebook.
              testAPI();  
            } else {                                 // Not logged into your webpage or we are unable to tell.
              document.getElementById('status').innerHTML = 'Please log ' +
                'into this webpage.';
            }
          }
        
          function checkLoginState() {               // Called when a person is finished with the Login Button.
            FB.getLoginStatus(function(response) {   // See the onlogin handler
              statusChangeCallback(response);
            });
          }
        
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '2420447694694195',
              cookie     : true,                     // Enable cookies to allow the server to access the session.
              xfbml      : true,                     // Parse social plugins on this webpage.
              version    : 'v8.0'           // Use this Graph API version for this call.
            });

            FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
              statusChangeCallback(response);        // Returns the login status.
            });
          };
         
          function testAPI() {                      // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
            console.log('Welcome!  Fetching your information.... ');
            FB.api('/me', function(response) {
              console.log('Successful login for: ' + response.name);
              document.getElementById('status').innerHTML =
                'Thanks for logging in, ' + response.name + '!';
            });
          }
        
        </script>
        <style>
            <?php
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) { ?>
              .nav-block .logo {
                width: 17%;
                left: 44%;
                top:7px;
            }
            .menu-image{
              width:35%;    
            }
            
            .free-shipping{
                padding: 0 20px !important;
            }
            .contact-us{
                padding: 0 20px !important;
            }
            .store-locator{
                padding: 0 20px !important;
            }
            .topnote-p{
                padding: 11px 0px !important;
            }
             .products-grid .product-item-info .product-item-details .product-item-name a{
                font-size:13px;
                overflow:hidden;
                line-height:1;
                }
                .price-box{
                    margin:9px 0 22px;
                }
                .panel-block-row.trending .items.row .slick-arrow{
                    width: 30px;
                    height: 30px;
                }
              
            <?php } ?>
            .view-cart{
               text-align: center;
               padding-top: 15px;
            }
           .fancybox-wrap {
    			position: absolute;
    			top: 0;
    			left: 0;
    			z-index: 8020;
    			max-width:100% !important; 
    		}
    		.fancybox-outer, .fancybox-inner {
    		    position: relative;
    		    width: 100% !important;
    		    height: 100% !important; 
    		}
        </style>
        <script type="text/javascript">
          var base_url = "{{ URL::to('') }}";
          var csrf_token = "{{ csrf_token() }}";
        </script>
       
    </head>
<body data-container="body" data-mage-init='{"loaderAjax": {}, "loader": { "icon": "#"}}' class="cms-home cms-index-index page-layout-1column" ontouchstart="">
    <div class="wrapper">
        <noscript>
        <div class="message global noscript">
            <div class="content">
                <p>
                    <strong>JavaScript seems to be disabled in your browser.</strong>
                    <span>For the best experience on our site, be sure to turn on Javascript in your browser.</span>
                </p>
            </div>
        </div>
        </noscript>
        <div class="page-wrapper">
            <header id="header" class="page-header">
                <div class="topnote">
                    <p style="margin: 0 auto; max-width: 1250px; padding: 11px 15px; text-align: center; font-size:12px">
					<a style="color:white;padding: 0 45px;border-right: 1px solid #afafaf;" href="{{url('/new')}}" class="free-shipping">Free standard shipping on all orders above tk.3000/-</a>
					<a style="color:white;padding: 0 45px; border-right: 1px solid #afafaf !important;" class="contact-us" href="{{url('/contact-us')}}">Customer Service</a> 
					<a style="color:white;padding: 0 45px;"  href="{{url('/store-locator')}}" class="store-locator">Find A Pride Store Near You</a></p>
                </div>
                <div class="header-block hidden-xs hidden-sm">
                    
                </div>
                <div class="nav-block">
                    <div class="nav-wrap">
                        <div class="nav-holder"> 
                            <a href="#" class="nav-opener"><span>menu</span></a>	
                            <div class="block block-search">
                                <div class="block block-title"><strong>Search</strong></div>
                                <div class="block block-content">
                                    <form class="form minisearch" onsubmit="updateSearchLink()" id="search_mini_form" action="{{ url('search-view') }}" method="get">
                                        <div class="field search">
                                            <label class="label" for="search" data-role="minisearch-label">
                                                <span>Search </span>
                                            </label>
                                            <div class="control">
                                                <input id="question" type="text" name="question" value="{{ $key ?? '' }}" placeholder="Search" class="input-text" maxlength="128" role="combobox"  aria-haspopup="false" aria-autocomplete="both" autocomplete="off"/>
                                                <div id="search_autocomplete" class="search-autocomplete"></div>
                                                <div class="nested">
                                                    <a class="action advanced" href="#" data-action="advanced-search">
                                                        Advanced Search    
                                                    </a>
                                                </div>
                                                <div data-bind="scope: 'searchsuiteautocomplete_form'">
                                                    <div id="searchsuite_autocomplete" class="searchsuite-autocomplete" data-bind="visible: showPopup()" style="">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="actions">
                                            <button type="submit"
                                                    title="Search"
                                                    class="action search">
                                                <span>Search </span>
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            
                            @livewire('menu')
                            
                        </div>
                        <!--<div id="top-opener-hover"><img src="#" /></div>-->
                        <a href="#" class="nav-opener top-opener"><span>menu</span></a>	
                        <strong class="logo"><a id="logo-tab" href="{{ url('/') }}"><img class="logo-tab" src="{{url('/')}}/storage/app/public/logo_new.png"/></a></strong>
                        <div class="nav-frame">
                            <div data-block="minicart" class="minicart-wrapper">
                                <h6 class="minicart-wrapper-open"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <polygon style="fill:#FF9E24;" points="475.975,144.854 420.981,144.854 374.646,322.056 420.981,499.258 475.975,499.258 "></polygon>
                                    <polygon style="fill:#FF8C00;" points="365.987,144.854 319.652,322.056 365.987,499.258 420.981,499.258 420.981,144.854 "></polygon>
                                    <rect x="36.025" y="144.854" style="fill:#FFB14D;" width="329.962" height="354.404"></rect>
                                    <path style="fill:#11113F;" d="M475.975,134.429h-67.852v-37.3C408.123,43.572,364.551,0,310.994,0
                                          c-7.79,0-15.595,0.945-23.198,2.81c-6.214,1.522-10.015,7.795-8.492,14.008c1.522,6.214,7.8,10.015,14.008,8.492
                                          c5.801-1.422,11.75-2.143,17.683-2.143c40.783,0,73.963,33.179,73.963,73.962v37.3h-18.971h-67.852v-37.3
                                          C298.136,43.573,254.564,0,201.006,0c-53.557,0-97.129,43.572-97.129,97.129v37.3H36.025c-6.397,0-11.584,5.187-11.584,11.584
                                          v354.404c0,6.397,5.187,11.584,11.584,11.584h329.962h109.987c6.397,0,11.584-5.187,11.584-11.584V146.013
                                          C487.558,139.615,482.372,134.429,475.975,134.429z M127.044,97.129c0-40.783,33.18-73.962,73.963-73.962
                                          s73.962,33.179,73.962,73.962v37.3h-37.937v-37.3c0-10.989,2.363-21.579,7.024-31.474c2.727-5.787,0.246-12.689-5.542-15.416
                                          c-5.787-2.729-12.69-0.246-15.416,5.542c-6.128,13.007-9.236,26.917-9.236,41.347v37.3h-86.82V97.129z M47.609,157.596h56.268
                                          v12.859c0,6.397,5.187,11.584,11.584,11.584c6.397,0,11.584-5.187,11.584-11.584v-12.859h147.924v12.859
                                          c0,6.397,5.187,11.584,11.584,11.584c6.397,0,11.584-5.187,11.584-11.584v-12.859h56.268v331.236H47.609V157.596z M464.391,488.833
                                          h-31.827V261.024c0-6.397-5.187-11.584-11.584-11.584c-6.397,0-11.584,5.187-11.584,11.584v227.809h-31.825V157.596h31.826v61.741
                                          c0,6.397,5.187,11.584,11.584,11.584c6.397,0,11.584-5.187,11.584-11.584v-61.741h31.826V488.833z"></path>
                                    </svg>
                                    <span class="counter qty" style="display:none;">
                                        <span class="counter-number">
                                            <?php echo Cart::instance('products')->count(); ?>
                                        </span>
                                    </span>
                                </h6>
                                
                                <div class="block block-minicart ui-dialog-content ui-widget-content" data-role="dropdownDialog" id="ui-id-1" style="display: none;">
                                    <div id="minicart-content-wrapper">
                                        <div class="block-title">
                                            <strong>
                                                <span class="text">My Cart</span>
                                                <span class="qty"><?php echo Cart::instance('products')->count(); ?></span>
                                            </strong>
                                        </div>
                                        <div class="block-content">
                                            <button type="button" id="btn-minicart-close" class="action close" title="Close">
                                                <img class="cart-actions-close" src="{{url('/')}}/storage/app/public/close.png" />
                                                <span>Close</span>
                                            </button>
                                            <?php if (Cart::count() == 0) { ?>
                                                <strong class="empty">Sorry! your shopping cart is empty!</strong>
                                            <?php } else { ?>
                                                <a class="action viewcart"  href="{{url('/shop-cart')}}">
                                                    <span class="minicart-label">Cart </span>
                                                </a>
                                                <div class="items-total">
                                                    <span class="count"><?php echo Cart::instance('products')->count(); ?></span>
                                                    <span>Item:</span>
                                                </div>
                                                <strong class="subtitle">Recently added item(s)</strong>
                                                <div data-action="scroll" class="minicart-items-wrapper" style="height: 95px;">
                                                    <?php
                                                    $i = 0;
                                                    foreach (Cart::instance('products')->content() as $row) :
                                                        $i++;
                                                        $name = $row->name;
                                                        $pro_name = str_replace(' ', '-', $name);
                                                        $product_url = strtolower($pro_name);
                                                        $color = $row->options->color;
                                                        $color_album = str_replace('/', '-', $color);
                                                        ?>
                                                        <ol id="mini-cart" class="minicart-items" role="tablist">
                                                            <li class="item product product-item odd last" data-role="product-item" data-collapsible="true" role="tab" aria-selected="false" aria-expanded="false" tabindex="0">
                                                                <div class="product">
                                                                    <a  tabindex="-1" target="_blank" class="product-item-photo" href="{{url("shop/{$product_url}/color-{$color_album}/{$row->id}")}}" title="<?php echo $row->name; ?>">
                                                                        <img class="photo image" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo ($row->options->has('product_image') ? $row->options->product_image : ''); ?>" alt="<?php echo $row->name; ?>" style="width: 85px; height: 120px;">
                                                                    </a>
                                                                    <div class="product-item-details">
                                                                        <strong class="product-item-name">
                                                                            <a  href="#"><?php echo $row->name; ?></a>
                                                                        </strong>
                                                                        <div data-role="content" class="content">
                                                                            <strong class="subtitle"><span>Options Details</span></strong>
                                                                            <dl class="product options list">
                                                                                <dt class="label" style="font-weight: 700;">Size</dt>
                                                                                <dd class="values">
                                                                                    <span><?php echo ($row->options->has('size') ? $row->options->size : ''); ?></span>
                                                                                </dd>
                                                                            </dl>
                                                                        </div>
                                                                        <div class="product-item-pricing">

                                                                            <div class="price-container">
                                                                                <span class="price-wrapper">
                                                                                    <span class="price-excluding-tax">
                                                                                        <span class="minicart-price">
                                                                                            <span class="price">Tk <?php echo $row->price * $row->qty; ?></span>        
                                                                                        </span>
                                                                                    </span>
                                                                                </span>
                                                                            </div>
                                                                            <!--<div class="details-qty qty">-->
                                                                            <!--    <label class="label">Qty</label>-->
                                                                            <!--    <span class="mini-qty-minus">-</span>-->
                                                                            <!--    <input type="number" size="4" class="item-qty cart-item-qty" id="cart-item-7845891-qty" value="<?php echo $row->qty; ?>"  readonly="readonly">-->
                                                                            <!--    <span class="mini-qty-plus" >+</span>-->
                                                                            <!--    <button class="update-cart-item" style="display: none" id="update-cart-item-7845891" data-cart-item="7845891" title="Update">-->
                                                                            <!--        <span data-bind="i18n: 'Update'">Update</span>-->
                                                                            <!--    </button>-->
                                                                            <!--</div>-->
                                                                        </div>
                                                                        <!--<div class="product actions">-->
                                                                        <!--    <div class="secondary">-->
                                                                        <!--        <a href="{{url('/cart/delete')}}/<?php echo $row->rowId; ?>"  class="action delete ik-shopcart-product-close" data-cart-item="<?php echo $row->rowId; ?>" title="Remove item">-->
                                                                        <!--            <img class="product-actions-delete" src="{{url('/')}}/storage/app/public/delete-icon.svg" />-->
                                                                        <!--            <span>Remove</span>-->
                                                                        <!--        </a>-->
                                                                        <!--    </div>-->
                                                                        <!--</div>-->
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ol>
                                                    <?php endforeach; ?>
                                                </div>
                                                <div class="subtotal mincartpoup">
                                                    <span class="label">Total </span>
                                                    <span class="total-price grand-total" data-bind="html: getCartParam('grand_total')"><span class="price">Tk <?php echo Cart::subtotal(); ?></span></span>
                                                </div>
                                            <?php } ?>
                                        </div>
                                         <?php if (Cart::count() != 0) { ?>
                                        <div class="actions">
                                            <div class="primary">
                                                <button id="top-cart-btn-cart" type="button" class="action primary checkout" data-action="close" title="Shopping Cart">View Cart</button>
                                            </div>
                                            
                                            <div class="primary">
                                                <button id="top-cart-btn-checkout" type="button" class="action primary checkout" data-action="close" title="Checkout">Checkout</button>
                                            </div>
                                        </div>
                                         <?php } ?>
                                    </div>
                                </div>
                                
                            </div>
                            <div data-block="minicart" class="myaccount-wrapper">
                                <div class="dropdown">
								  <a class="dropbtn"><img class="dropbtn user-icon" src="{{url('/')}}/storage/app/public/user.png" /></a>
								  <div class="dropdown-content">
								  @guest
								   <a href="{{ route('login') }}">Log In/Register</a>
								   <a href="{{ url('/user/track/order') }}">Track Order</a>
                                  @else
									<a href="{{url('/user/dashboard')}}">My Account</a>
									<a href="{{ url('/user/orders') }}">My Orders</a>
									<a href="{{ url('/user/track/order') }}">Track Order</a>
									<a href="{{ url('/user/logout') }}">Logout</a>
								   @endguest
								  </div>
								</div>
							    <!--<a href="{{url('/my-account')}}"><img class="user-icon" src="{{url('/')}}/storage/app/public/user.png" /></a>-->
							</div>
       <!--                     <div data-block="minicart" class="wishlist-wrapper">-->
							<!--    <a href="{{url('/my-wishlist')}}"><img class="wish-list" src="{{url('/')}}/storage/app/public/wishlist.svg" /></a>-->
							<!--</div>-->
							
                            <div id="CartPopupArea"></div>
                        </div>
                    </div>
                </div>
            </header>
			<div class="pagecontent">
                <!-- main content --->
                    @yield('content')
                <!-- end main content ---->
            </div>
            <footer class="page-footer">
                <div class="footer-aside"><div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong class="logo">
                                    <a  href="{{url('/')}}"><img class="img-responsive mobile-footer-logo" src="{{url('/')}}/storage/app/public/logo.png"></a>
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Load Facebook SDK for JavaScript -->
                <div id="fb-root"></div>
                <!-- Your Chat Plugin code -->
                <div class="fb-customerchat"
                  attribution=install_email
                  page_id="37089740265"
                  theme_color="#291d88"
                    logged_in_greeting="Have a quick chat if you need any help!"
                    logged_out_greeting="Have a quick chat if you need any help!">
                </div>
                <div class="footer content">
                    <div class="container">
                        <div class="row">
						   <div class="col-xs-12">
                                <div class="footer-links">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong class="title">Company</strong>
                                            <ul>
                                                <li><a href="{{url('/about-us')}}">About Us</a></li>
                                                <li><a href="{{url('/work-with-us')}}">Work with us</a></li>
                                                <li><a href="{{url('/contact-us')}}">Contact us</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><strong class="title">Customer Care</strong>
                                            <ul>
                                                <li><a href="{{url('/how-to-order')}}">How To Order</a></li>
                                                <li><a href="{{url('/exchange-policy')}}">Exchange Policy</a></li>
                                                <li><a href="{{url('/size-guide')}}">Size Guide</a></li>
                                                <li><a href="{{url('/insider-rewards')}}">Pride Insider Rewards</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3" style="z-index:1"><strong class="title">QUICK LINK</strong>
                                            <ul>
                                                <li><a href="{{url('/user/track/order')}}" target="_blank">Track Your Order</a></li>
                                                <li><a href="{{url('/store-locator')}}">Store Locator</a></li>
                                                <li><a href="{{url('/privacy-cookies')}}">Privacy & Cookies</a></li>
                                                <li><a href="{{url('/faq')}}">FAQ's</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="footer-newsletter">
                                                <?php if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) { ?>
                                                    <img src="{{url('/')}}/storage/app/public/superbrandlogo-20_21.png" class="img-responsive" style="width:33%;margin: 0 auto;">
                                                <?php } else { ?>
                                                    <img src="{{url('/')}}/storage/app/public/superbrandlogo-20_21.png" class="img-responsive" style="width:50%;margin: 0 auto;">
                                                <?php } ?>
                                                <!--                                                    <div class="block newsletter">
                                                                                                        <form class="form subscribe"
                                                                                                              novalidate
                                                                                                              action="#"
                                                                                                              method="post"
                                                                                                              data-mage-init='{"validation": {"errorClass": "mage-error"}}'
                                                                                                              id="newsletter-validate-detail">
                                                                                                            <div class="field newsletter">
                                                                                                                <label class="label" for="newsletter"><span>Sign Up for Our Newsletter:</span></label>
                                                                                                                <div class="control">
                                                                                                                    <input name="email" type="email" id="newsletter"
                                                                                                                           placeholder="Enter your email address"
                                                                                                                           data-validate="{required:true, 'validate-email':true}"/>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="actions">
                                                                                                                <button class="action subscribe primary" title="Subscribe" type="submit">
                                                                                                                    <span>Sign Up</span>
                                                                                                                </button>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                    </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
							<div class="social-networks">
								<div class="col-xs-12 col-md-4 col-md-offset-0 col-lg-offset-1">
									<h3>PAYMENT OPTIONS</h3>
									<ul class="payment-methods">
										<li class="cash-on-delivery"></li>
										<li class="master-card"></li>
										<li class="visa"></li>
										<li class="american-express"></li><br/>
										<li class="bkash"></li>
										<!--<li class="ipay"></li>-->
									</ul>
								</div>
								<div class="col-xs-12 col-md-4 col-lg-2 text-center">
									<ul class="social-icons row">
										<li><a class="icon-instagram text-center" target="_blank" href="https://www.instagram.com/pridelimitedpr/"><span>instagram</span></a></li>
										<!--<li><a class="icon-social-twitter text-center" target="_blank" href="#"><span>twitter</span></a></li> -->
										<li><a class="icon-facebook text-center" target="_blank" href="https://www.facebook.com/PrideLimitedBangladesh/"><span>facebook</span></a></li>
										<!-- li><a class="icon-pinterest-p" href="https://www.pinterest.com/Pride/"><span>pinterest</span></a></li -->
									</ul>
								</div>
							</div>
							</div>
							<div class="row">
							<div class="col-md-12" >
								<small class="copyright">
									<span>Copyright © <?php echo date('Y');?> Pride Limited. All Rights Reserved. </span>
								</small>
							</div>
							</div>
                        </div>
                    </div>					
                </div>
            </footer>
            <!--- phone modal   ----> 
			<div class="modal fade" id="modalForm" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Modal Header -->
						<!--<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button> -->

						<!-- Modal Body -->
						<div class="modal-body">
							<!--<img src="{{url('/')}}/assets/image/callimage.jpg" class="img-responsive"> -->
							 <h2 style="color:#2516a7;"><i class="fa fa-phone" aria-hidden="true" style="color:#2516a7;font-size:30px;"></i> Need Assistance? Let Us Call You!</h2>
							 <br>
							<div class="form-group">
								<label for= "inputName">Name</label>
								<input type="text" class="form-control" id="inputName" placeholder="Write your name"/>
							</div>
							<div class="form-group">
								<label for= "inputName">Contact Number</label>
								<input type="text" class="form-control" id="inputNumber" placeholder="Enter your contact number"/>
							</div>
							<div class="form-group">
								<label for= "inputName">Leave Message</label>
								<textarea type="text" class="form-control" id="inputmessage" placeholder="Leave message" rows="3"></textarea>
							</div>
							<p class="statusMsg"></p>

						</div>
						<div class="modal-footer">
							<button type="button" style="width:48%;background-color:#2516a7;" class="pull-left" data-dismiss="modal">Cancel</button>
							<button type="button" style="width:48%;background-color:#2516a7;" class="pull-right submitBtn" onClick="submitContactForm()">Send</button>
						</div>
					</div>
				</div>
			</div>
			<!--- End phone modal --->
            <script src="{{asset('assets/js/ajax_modal.js')}}"></script>
            <script type="text/javascript" src="{{asset('assets/slick/slick.min.js')}}"></script>
        </div>
    </div>
<input type="hidden" value="{{url('/')}}" id="url" name="url">
<script  type="text/javascript"  src="{{asset('assets/js/custom.js?20')}}"></script>
<script  type="text/javascript"  src="{{asset('assets/fancybox/dist/jquery.fancybox.min.js')}}"></script>
@livewireScripts
<script>
    jQuery(document).ready(function($){
        $('.main-category').on('click', function (e) {
    //         e.preventDefault();
    // 		var url = $(this).attr('href');
    // 		history.pushState({}, null, url);
    // 		window.onpopstate = function() {
    // 		  window.location.href = window.location.origin;
    // 		};
    // 		$(".loading-modal").show();
    // 		$.ajax({
    // 			type: "GET",
    // 			url: url,
    // 			success: function(response){
    // 				$('.pagecontent').empty();
    // 				$("html, body").scrollTop($("body").offset().top);
    // 				$(response).find('.pagecontent > *').appendTo('.pagecontent');
    // 				 $(".loading-modal").hide();
    // 			 }
	   // 	});
        });
       
       $(".minicart-wrapper-open").on("click", function () {
            $(".block-minicart").toggleClass("block-minicart-open");
        });
        $("#btn-minicart-close").on("click", function () {
            $(".block-minicart").removeClass("block-minicart-open");
        });
        $(".logo").click(function(){
            window.location=base_url;
        });
        $("#top-cart-btn-checkout").click(function(){
             window.location=base_url+ "/checkout";
        });
        $("#top-cart-btn-cart").click(function(){
             window.location=base_url+ "/shop-cart";
        });
        var base_url = "{{ URL::to('') }}";
        $(".logo").click(function(){
            window.location=base_url;
        });
		 var cart_count = base_url + "/cart/item-count";
		   $.ajax({
				url:cart_count,
				type:'GET',
				success:function(result){
				if(result==0){
					   $(".counter").hide();  
					   //$(".g-stickycart-count").hide(); 
					}else{
					   $(".counter").show();  
					   $(".counter").html(result); 
					   //$(".g-stickycart-count").html(result); 
					}
				}
		   });
		   $.ajax({
				url:base_url + "/cart-restore",
				type:'GET',
				success:function(result){
					
				}
		   });
		   jQuery(".fancybox").fancybox({
    		'transitionIn'	:	'elastic',
    		'transitionOut'	:	'elastic',
    		'speedIn'		:	600, 
    		'speedOut'		:	200, 
    		'overlayShow'	:	false
    	   });
	});
</script>
<script type="text/javascript">
// var message="Right-click has been disabled";
// function clickIE() {
//     if (document.all) {
//         (message);
//         return false;
//     }
// }
// function clickNS(e) {
//     if (document.layers || (document.getElementById && !document.all)) {
//         if (e.which == 2||e.which == 3) {
//             (message);
//             return false;
//         }
//     }
// }
// if (document.layers) {
//     document.captureEvents(Event.MOUSEDOWN);
//     document.onmousedown = clickNS;
// } else {
//     document.onmouseup = clickNS;
//     document.oncontextmenu = clickIE;
// }
// document.oncontextmenu = new Function("return false");
// document.getElementsByClassName('my-img').ondragstart = function() { return false; };
</script>
@yield('appended.script')
</body>
</html>
