<?php
use App\Http\Controllers\product\ProductController;
use App\Models\Productalbum;
use App\Models\Subprocat;
?>
<div>
   @foreach($product_list as $product)
    <?php
    $product_name = str_replace(' ', '-', $product->product_name);
    $product_url = strtolower($product_name);
   // $data = ProductController::GetProductColorAlbum($product->product_id);
   $pro_album = Productalbum::where('product_id',$product->id)->orderBy('id','DESC')->first();
    // dd($data);
    $sold_out = ProductController::ProductWiseQty($product->id);
  //  foreach ($data as $pro_album) {
    //    $colorwiseimg = ProductController::GetProductImageByColorAlbum($pro_album->productalbum_id);
   // }
    ?>
    <li class="item product product-item">    
       <?php
        date_default_timezone_set('Asia/Dhaka');
		$today=date('d-m-Y');
		$insert_date=$product->created_at;
		$datetime1 = new DateTime($today);
		$datetime2 = new DateTime($insert_date);
		$interval = $datetime1->diff($datetime2);
		$date_difference=$interval->format('%a');
		$color_album=str_replace('/','-',$pro_album->productalbum_name);
       if($sold_out <= 0){ ?>
         <!--<span class="sprice-tag">Sold Out</span> --->
         <span class="sold-out">Sold Out</span>
       <?php }else if($product->product_pricediscounted > 1){ ?>
		   <span class="sprice-tag"><?php echo $product->product_pricediscounted;?>% Off</span>
	   <?php  } if($date_difference < 31){ ?>
         <div class="tag_container round_tag_lt bg_red t_white"><span class="ttl_header">New</span></div> 
       <?php } ?>
        <div class="product-item-info" data-container="product-grid">
            <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}">
                <span class="product-image-container">
                    <span class="product-image-wrapper" >
                        <span class="custom-carousel" onmouseenter="fadeImages(this)" onmouseleave="removeTimer(this)">
                            <?php $images = ProductController::GetProductImageByColorAlbum($pro_album->id); ?>
                            @php($i = 0)
                            @foreach($images as $image)
                            <img class="item large_img<?php if($i==0) echo ' active';?> swatch_img_<?php echo $product->id;?>" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $image->productimg_img_medium }}" alt="No Image Found"/>
                            @php($i++)
                            @endforeach
                        </span>
                    </span>
                </span>
            </a>
            <div class="product details product-item-details">
                <div class="info-holder">
                    <strong class="product name product-item-name">
                        <a class="product-item-link"
                           href="{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}">
                            {{$product->product_name}}
                        </a>
                    </strong>
                    <!-- swatch --->
                    <?php
					$data = ProductController::GetProductColorAlbum($product->id);
					?>
					<div class="ws_100 swatch round show_desktop show_tablet">
						<ul class="color">
							<?php
							$i = 0;
							$data = ProductController::GetProductColorAlbum($product->id);
							foreach ($data as $pro_album) {
								$i++;
								//echo '=='.$pro_album->productalbum_id;
								$colorwiseimg = ProductController::GetProductImageSingleByColorAlbum($pro_album->id);
								?>
								<li>
									<span class="<?php
									if ($i == 1) {
										echo 'selected';
									}
									?>">
										<input type="hidden" id="color_album" value="{{$product->productalbum_name}}"/>
										<input type="hidden" id="product_id" value="{{$product->id}}"/>
										<img  id="{{$product->id}}" rel="{{ URL::to('') }}/storage/app/public/pgallery/{{$colorwiseimg->productimg_img}}" src="{{ URL::to('') }}/storage/app/public/pgallery/{{$pro_album->productalbum_img}}" data-color="01" data-modal-target="#quickview_pop">
									</span>
								</li>
							<?php } ?>
						</ul>
					</div>
					 <!-- swatch --->
                </div>
                <div class="info-holder">
                   <?php if ($product->product_pricediscounted < 1) { ?>
					<div class="price-box price-final_price" data-role="priceBox">
						<span class="price-container price-final_price tax weee">
							<span class="price-label">Regular Price</span>
							<span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
								<span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
						</span>
					</div>
					<?php }else{?>
					<div class="price-box price-final_price" data-role="priceBox"  data-price-box="product-id-84726">
						<span class="normal-price">
							<span class="price-container price-final_price tax weee">
									<span  data-price-type="finalPrice" class="price-wrapper ">
										<span class="price">Tk &nbsp;{{$product->discount_product_price}}</span>
									</span>
							</span>
						</span>
					</div>                                                            
					<div class="price-box price-final_price" data-role="priceBox">
							<span class="old-price">
								<span class="price-container price-final_price tax weee">
									<span class="price-label">Regular Price</span>
									<span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
										<span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
								</span>
							</span>
					</div>
				<?php } ?>                                                      
                    <div class="product-item-inner">
                        <div class="product actions product-item-actions">
                            <div class="actions-primary">
                                <a href="{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}" class="action tocart primary"><span>Shop Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
    @endforeach
</div>
