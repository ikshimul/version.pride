<?php
    use App\Models\Subprocat;
    use App\Models\Subcat;
?>

    <nav id="nav" role="navigation" >
        <div class="menu" id="om">
            <ul>
                <li><a href="{{url('/new')}}"  class="level-top" ><span>New In</span></a>
                    <ul>
                        <li style="width: 25%;"><a href="{{url('new/women')}}" ><span>Women</span></a>
                            <ul>
                                @foreach($campaigns as $campaign)
                                   @if($campaign->category == 1 || $campaign->category==6)
                                   <li  class="level2 nav-1-1-1 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                   @endif
                                @endforeach
                                <li  class="level2 nav-1-1-1"><a href="{{url('/new/women/unstitched')}}" ><span>Unstitched</span></a></li>
                                <li  class="level2 nav-1-1-2"><a href="{{url('/new/women/sari')}}" ><span>Sari</span></a></li>
                                <li  class="level2 nav-1-1-3"><a href="{{url('/new/women/ready-to-wear')}}" ><span>Ready To Wear</span></a></li>
                                <?php
                                    $product_name = str_replace(' ', '-', $woman->product_name);
                                    $product_url = strtolower($product_name);
                                    ?>
                                <div>
                                   <a href="{{url('new/women')}}" ><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $woman->productimg_img; ?>" class="img-responsive menu-image"/></a>
                                </div>
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <li style="width: 25%;"><a href="{{url('new/men')}}" ><span>Men</span></a>
                            <ul>
                                @foreach($campaigns as $campaign)
                                   @if($campaign->category == 2 || $campaign->category==6)
                                   <li  class="level2 nav-1-1-1 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                   @endif
                                @endforeach
                                <li  class="level2 nav-1-4-2"><a href="{{url('/new/men/ethnic-meanswear')}}" ><span>Ethnic Menswear</span></a></li>
                                <li  class="level2 nav-1-2-3 last">&nbsp;</li>
                                <li  class="level2 nav-1-2-3 last">&nbsp;</li>
                                <?php
                                    $product_name = str_replace(' ', '-', $men->product_name);
                                    $product_url = strtolower($product_name);
                                    ?>
                                <div>
                                   <a href="{{url('new/men')}}" ><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $men->productimg_img; ?>" class="img-responsive menu-image"/></a>
                                </div>
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <li style="width: 25%;">
                            <a href="{{url('new/kids')}}" ><span>Kids</span></a>
                            <ul>
                                @foreach($campaigns as $campaign)
                                   @if($campaign->category == 3 || $campaign->category==6)
                                   <li  class="level2 nav-1-1-1 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                   @endif
                                @endforeach
                                <li  class="level2 nav-1-2-2"><a href="{{url('/new/kids/boys')}}" ><span>Boys</span></a></li>
                                <li  class="level2 nav-1-2-3 last"><a href="{{url('/new/kids/girls')}}" ><span>Girls</span></a></li>
                                <li  class="level2 nav-1-2-3 last">&nbsp;</li>
                                <?php
                                    $product_name = str_replace(' ', '-', $kids->product_name);
                                    $product_url = strtolower($product_name);
                                    ?>
                                <div>
                                   <a href="{{url('new/kids')}}" ><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $kids->productimg_img; ?>" class="img-responsive menu-image"/></a>
                                </div>
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <li style="width: 25%;"><a href="{{url('new/accessories')}}" ><span>Accessories</span></a>
                            <ul><li  class="level2 nav-1-3-1 first"><a href="{{url('/new/women/accessories')}}" ><span>Woman</span></a></li>
                                <li  class="level2 nav-1-3-2 last"><a href="{{url('/new/kids/accessories')}}" ><span>Kids</span></a></li>
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        
                    </ul>
                </li>
                
                <li><a href="#"  class="level-top" ><span>Occasions</span></a>
                    <ul>
                        <li style="width: 25%;"><a href="{{url('occasions/festive-wear')}}" ><span>Festive Wear</span></a>
                            <ul>
                                <li  class="level2 nav-4-1-5">
                                    <?php
                                    if($festive){
                                    $product_name = str_replace(' ', '-', $festive->product_name);
                                    $product_url = strtolower($product_name);
                                    ?>
                                    <a href="{{url('occasions/festive-wear')}}" ><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $festive->productimg_img; ?>" class="img-responsive menu-image"/></a>
                                    <?php } ?>
                                </li>
                            </ul>
                        </li>
                        <li style="width: 25%;"><a href="{{url('occasions/ready-to-wear')}}" ><span>Ready To Wear</span></a>
                            <ul>
                               <li  class="level2 nav-4-1-5">
                                    
                            <?php
                            $product_name = str_replace(' ', '-', $ready->product_name);
                            $product_url = strtolower($product_name);
                            ?>
                            <a href="{{url('occasions/ready-to-wear')}}"><img class="img-responsive menu-image" src="{{url('/')}}/storage/app/public/pgallery/<?php echo $ready->productimg_img; ?>"/></a>
                            
                                </li>
                            </ul>
                           
                        </li>
                        <li style="width: 25%;"><a href="{{url('occasions/casual-wear')}}" ><span>Casual Wear</span></a>
                            <ul>
                            <li  class="level2 nav-4-1-5">
                            <?php
                            $product_name = str_replace(' ', '-', $casual->product_name);
                            $product_url = strtolower($product_name);
                            ?>
                            <a href="{{url('occasions/casual-wear')}}"><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $casual->productimg_img; ?>" class="img-responsive menu-image"/></a>
                            
                                </li>
                            </ul>
                        </li>
                        <li style="width: 25%;"><a href="{{url('occasions/office-wear')}}" ><span>Office Wear</span></a>
                            <ul>
                                <li  class="level2 nav-4-1-5">
                                    
                            <?php
                            $product_name = str_replace(' ', '-', $office->product_name);
                            $product_url = strtolower($product_name);
                            ?>
                            <a href="{{url('/office-wear')}}" ><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $office->productimg_img; ?>" class="img-responsive menu-image"/></a>
                                </li>
                            </ul>
                            
                        </li>
                    </ul>
                </li>
                <li><a href="{{url("/women")}}"  class="level-top" ><span>Women</span></a>
                    <ul>
                        <li>
                            <a href='{{url("/women/sari/")}}' >
                                Sari
                            </a>
                            <ul>
                                <?php foreach(Subcat::where('cat',1)->orderBy('id','ASC')->get() as $menu){
									$subproname=str_replace('-', '_', $menu->name);
									$subproname=str_replace(' ', '-', $subproname);
									$subproname=strtolower($subproname);
									//echo $subproname;
								?>
								 <li  class="level2 nav-2-1-1 first"><a class="main-category" href='{{url("/women/sari/{$subproname}")}}'><span>{{$menu->name}}</span></a></li>
                                <?php } ?>
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <li>
                            <a href='{{url("/women/unstitched")}}' >
                               Unstitched
                            </a>
                            <ul>
                               <?php foreach(Subcat::where('cat',2)->orderBy('id','ASC')->get() as $menu){
									$subproname=str_replace('-', '_', $menu->name);
									$subproname=str_replace(' ', '-', $subproname);
									$subproname=strtolower($subproname);
									//echo $subproname;
								?>
								<li  class="level2 nav-2-1-1 first"><a class="main-category" href='{{url("/women/unstitched/{$subproname}")}}'><span>{{$menu->name}}</span></a></li>
                                <?php } ?>
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <li><a href='{{url("/women/ready-to-wear/")}}' >
                                Ready-To-Wear
                            </a>
                            <ul>
                                <?php foreach(Subcat::where('cat',3)->where('active',1)->orderBy('id','ASC')->get() as $menu){
									$subproname=str_replace('-', '_', $menu->name);
									$subproname=str_replace(' ', '-', $subproname);
									$subproname=strtolower($subproname);
									//echo $subproname;
								?>
							<li  class="level2 nav-2-1-1 first"><a class="main-category" href='{{url("/women/ready-to-wear/{$subproname}")}}'><span>{{$menu->name}}</span></a></li>
                                <?php } ?>
                            </ul>
                            <div class="bottomstatic" ></div></li>
                        <li><a href='{{url("/women/accessories")}}' >
                               Accessories
                            </a>
                            <ul>
							<?php foreach(Subcat::where('cat',4)->orderBy('id','ASC')->get() as $menu){
									$subproname=str_replace('-', '_', $menu->name);
									$subproname=str_replace(' ', '-', $subproname);
									$subproname=strtolower($subproname);
								?>
							<li  class="level2 nav-2-1-1 first"><a class="main-category" href='{{url("/women/accessories/{$subproname}")}}'><span>{{$menu->name}}</span></a></li>
                                <?php } ?>
                            </ul>
                            <div class="bottomstatic" ></div>
						</li>
                        <li>
                            <a href="#" ><span>Collection</span></a>
                            <ul>
                                @foreach($campaigns as $campaign)
                                   @if($campaign->category == 1 || $campaign->category==6)
                                   <li  class="level2 nav-2-1-1 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                   @endif
                                @endforeach
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                    </ul>
                </li>
                <li><a href="{{url('/men')}}"  class="level-top" ><span>Men</span></a>
                    <ul>
                        <li>
                            <a href='{{url("/men/ethnic-meanswear")}}' >
                               Ethnic Meanswear
                            </a>
                            <ul>
                                <?php foreach(Subcat::where('cat',5)->orderBy('id','ASC')->get() as $menu){
									$subproname=str_replace('-', '_', $menu->name);
									$subproname=str_replace(' ', '-', $subproname);
									$subproname=strtolower($subproname);
								?>
								<li  class="level2 nav-3-1-2 first"><a class="main-category" href='{{url("/men/ethnic-meanswear/{$subproname}")}}'><span>{{$menu->name}}</span></a></li>
                                <?php } ?>
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <li><a href="#" ><span>Collection</span></a>
                        <ul>
                            @foreach($campaigns as $campaign)
                               @if($campaign->category == 2 || $campaign->category==6)
                               <li  class="level2 nav-3-1-3 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                               @endif
                            @endforeach
                        </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <div class="bottomstatic">
                            <h2>New In</h2>
                            <?php
                            $product_name = str_replace(' ', '-', $men->product_name);
                            $product_url = strtolower($product_name);
                            ?>
                            <div>
                                <center>
                                    <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $men->productimg_img; ?>" class="img-responsive" style="border:1px solid #eee;"/>
                                </center>
                            </div>
                            <p><a class="btn" href="{{url("shop/{$product_url}/color-{$men->productalbum_name}/{$men->id}")}}">Shop Now</a></p>
                        </div>
                    </ul>
                </li>
                <li><a href="{{url('/kids')}}"  class="level-top" ><span>Kids</span></a>
                    <ul>
                        <li><a href='{{url("/kids/girls/")}}' ><span>Girls</span></a><ul>
                                <?php foreach(Subcat::where('cat',8)->orderBy('id','ASC')->get() as $menu){
									$subproname=str_replace('-', '_', $menu->name);
									$subproname=str_replace(' ', '-', $subproname);
									$subproname=strtolower($subproname);
								?>
								    <li  class="level2 nav-3-1-1 first"><a href='{{url("/kids/girls/{$subproname}")}}' ><span>{{$menu->name}}</span></a></li>
                                <?php } ?>
                                
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <li><a href="{{url('/kids/boys')}}" ><span>Boys</span></a>
                            <ul>
                                <?php foreach(Subcat::where('cat',9)->orderBy('id','ASC')->get() as $menu){
									$subproname=str_replace('-', '_', $menu->name);
									$subproname=str_replace(' ', '-', $subproname);
									$subproname=strtolower($subproname);
								?>
                                <li  class="level2 nav-3-2-1 first"><a href='{{url("/kids/boys/{$subproname}")}}' ><span>{{$menu->name}}</span></a></li>
                                <?php } ?>
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <li><a href="#" ><span>Collection</span></a>
                            <ul>
                               @foreach($campaigns as $campaign)
                                   @if($campaign->category == 3 || $campaign->category==6)
                                   <li  class="level2 nav-3-2-2 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                   @endif
                                @endforeach
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <div class="bottomstatic">
                            <h2>New In</h2>
                            <?php
                            $product_name = str_replace(' ', '-', $kids->product_name);
                            $product_url = strtolower($product_name);
                            ?>
                            <div>
                                <center>
                                    <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $kids->productimg_img; ?>" class="img-responsive" style="border:1px solid #eee;"/>
                                </center>
                            </div>
                            <p><a class="btn" href="{{url("shop/{$product_url}/color-{$kids->productalbum_name}/{$kids->id}")}}">Shop Now</a></p>
                        </div>
                    </ul>
                </li>
                <li><a href="#"  class="level-top" ><span>Accessories</span></a>
                    <ul>
                        <li><a href="#" ><span>women</span></a>
                            <ul>
                                <li  class="level2 nav-4-1-5"><a href="{{url("/women/accessories/jewelry")}}" ><span>Jewellery</span></a></li>
                            </ul><div class="bottomstatic" ></div>
                        </li>
                        <li><a href="#" ><span>Kids</span></a>
                            <ul>
                                <li  class="level2 nav-4-2-3"><a href="{{url("/kids/girls/jewelry")}}" ><span>Jewellery</span></a></li>
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <div class="bottomstatic" ><h2>New In</h2>
                            <?php
                            $product_name = str_replace(' ', '-', $accessories->product_name);
                            $product_url = strtolower($product_name);
                            ?>
                            <div>
                                <center>
                                    <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $accessories->productimg_img; ?>" class="img-responsive" style="border:1px solid #eee;"/>
                                </center>
                            </div>
                            <p><a class="btn" href="{{url("shop/{$product_url}/color-{$accessories->productalbum_name}/{$accessories->id}")}}">Shop Now</a></p>
                        </div>
                    </ul>
                </li>
                
                <li><a href="{{url('/homes')}}"  class="level-top" ><span>Homes</span></a>
                    <ul>
                        <li>
                            <a href="{{url('homes/cushion')}}" ><span>Cushion Covers</span></a>
                            <ul>
                                <?php foreach(Subcat::where('cat',10)->orderBy('id','ASC')->get() as $menu){
                                  $subproname=str_replace('-', '_', $menu->name);
                                  $subproname=str_replace(' ', '-', $subproname);
                                  $subproname=strtolower($subproname);
                                ?>
                                <li  class="level2 nav-5-2-1 first"><a href="{{url("/homes/cushion/{$subproname}")}}" ><span>{{ ucwords($menu->name) }}</span></a></li>
                                <?php } ?>
                            </ul>
                            <div class="bottomstatic" ></div>
                        </li>
                        <div class="bottomstatic" ><h2>New In</h2>
                            <?php
                            $product_name = str_replace(' ', '-', $home->product_name);
                            $product_url = strtolower($product_name);
                            ?>
                            <div>
                                <center>
                                    <img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $home->productimg_img; ?>" style="width: 150px; height: 200px;border:1px solid #eee;"/>
                                </center>
                            </div>
                            <p><a class="btn" href="{{url("shop/{$product_url}/color-{$home->productalbum_name}/{$home->id}")}}">Shop Now</a></p>
                        </div>
                    </ul>
                </li>
                <!--<li><a href="#"  class="level-top" ><span>Crafted</span></a></li>-->
            </ul>
        </div>
        <div class="menu-mobile">
            <div class="mobile-holder">
                <ul>
                    <li  class="level0 nav-1 first level-top parent"><a href="{{url('/new')}}"  class="level-top" ><span>New In</span></a>
                        <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-346')"></button>
                        <div class="dropdown" id="drop-category-node-346"><ul class="level0 ">
                                <li  class="level1 nav-1-1 first parent">
                                    <a href="{{url('new/women')}}" ><span>Women</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-347')"></button>
                                    <div class="dropdown" id="drop-category-node-347">
                                        <ul class="level1 ">
                                            @foreach($campaigns as $campaign)
                                               @if($campaign->category == 1 || $campaign->category==6)
                                                <li  class="level2 nav-1-1-1 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                               @endif
                                            @endforeach
                                            <li  class="level2 nav-1-1-3"><a href="{{url('/new/women/signature')}}" ><span>Signature</span></a></li>
                                            <li  class="level2 nav-1-1-4"><a href="{{url('/new/women/classic')}}" ><span>Classic</span></a></li>
                                            <li  class="level2 nav-1-1-5"><a href="{{url('/new/women/pride-girls')}}" ><span>Pride Girls</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-1-4 parent"><a href="{{url('new/men')}}" ><span>Mens</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-351')"></button>
                                    <div class="dropdown" id="drop-category-node-351">
                                        <ul class="level1 ">
                                             @foreach($campaigns as $campaign)
                                               @if($campaign->category == 2 || $campaign->category==6)
                                                <li  class="level2 nav-1-4-2 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                               @endif
                                            @endforeach
                                            <li  class="level2 nav-1-4-3"><a href="{{url('/new/men/ethnic')}}" ><span>Ethnic Menswear</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-1-2 parent"><a href="{{url('new/kids')}}" ><span>Kids</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-349')"></button>
                                    <div class="dropdown" id="drop-category-node-349">
                                        <ul class="level1 ">
                                             @foreach($campaigns as $campaign)
                                               @if($campaign->category == 3 || $campaign->category==6)
                                                <li  class="level2 nav-1-2-1 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                               @endif
                                            @endforeach
                                            <li  class="level2 nav-1-2-3"><a href="{{url('/new/kids/boys')}}" ><span>Boys</span></a></li>
                                            <li  class="level2 nav-1-2-4 last"><a href="{{url('/new/kids/girls')}}" ><span>Girls</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-1-3 parent"><a href="{{url('new/accessories')}}" ><span>Accessories</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-350')"></button>
                                      <div class="dropdown" id="drop-category-node-350"><ul class="level1 ">
                                            <li  class="level2 nav-1-3-1 first"><a href="{{url('/new/women/accessories')}}" ><span>women</span></a></li>
                                            <li  class="level2 nav-1-3-2 last"><a href="{{url('/new/kids/accessories')}}" ><span>Kids</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li  class="level0 nav-7 level-top parent"><a href="#"  class="level-top" ><span>Occasions</span></a>
                        <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-55')"></button>
                        <div class="dropdown" id="drop-category-node-55">
                            <ul class="level0 ">
                                <li  class="level1 nav-7-1 first parent"><a href="{{url('/festive-wear')}}" ><span>Festive Wear</span></a>
                                 <?php
                                    if($festive){
                                    $product_name = str_replace(' ', '-', $festive->product_name);
                                    $product_url = strtolower($product_name);
                                    ?>
                                    <a href="{{url('/festive-wear')}}" ><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $festive->productimg_img; ?>" class="img-responsive menu-image"/></a>
                                    <?php } ?>
                                </li>
                                <li  class="level1 nav-7-2 parent"><a href="{{url('/ready-to-wear')}}" ><span>Ready To Wear</span></a>
                                <?php
                                    $product_name = str_replace(' ', '-', $ready->product_name);
                                    $product_url = strtolower($product_name);
                                    ?>
                                    <a href="{{url('/ready-to-wear')}}"><img class="img-responsive menu-image" src="{{url('/')}}/storage/app/public/pgallery/<?php echo $ready->productimg_img; ?>"/></a>
                                </li>
                                <li  class="level1 nav-7-3"><a href="{{url('occasions/casual-wear')}}" ><span>Casual Wear</span></a>
                                  <?php
                                    $product_name = str_replace(' ', '-', $casual->product_name);
                                    $product_url = strtolower($product_name);
                                    ?>
                                    <a href="{{url('occasions/casual-wear')}}"><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $casual->productimg_img; ?>" class="img-responsive menu-image"/></a>
                                </li>
                                <li  class="level1 nav-7-4 last parent"><a href="{{url('/office-wear')}}" ><span>Office Wear</span></a>
                                <?php
                                $product_name = str_replace(' ', '-', $office->product_name);
                                $product_url = strtolower($product_name);
                                ?>
                                <a href="{{url('/office-wear')}}" ><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $office->productimg_img; ?>" class="img-responsive menu-image"/></a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li  class="level0 nav-2 level-top parent"><a href='{{url("/women")}}'  class="level-top" ><span>Women</span></a>
                        <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-20')"></button>
                        <div class="dropdown" id="drop-category-node-20"><ul class="level0 ">
                                <li  class="level1 nav-2-1 first parent">
                                    <a href='{{url("/women/sari/")}}' ><span>Sari</span></a><button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-185')"></button>
                                    <div class="dropdown" id="drop-category-node-185">
                                        <ul class="level1">
                                             <?php foreach(Subcat::where('cat',1)->orderBy('id','ASC')->get() as $menu){
												$subproname=str_replace('-', '_', $menu->name);
												$subproname=str_replace(' ', '-', $subproname);
												$subproname=strtolower($subproname);
												//echo $subproname;
											?>
											 <li  class="level2 nav-2-1-1 first"><a href='{{url("/women/sari/{$subproname}")}}'><span>{{$menu->name}}</span></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-2-2 parent">
                                    <a href='{{url("/women/unstitched")}}' ><span>Unstitched</span></a><button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-139')"></button>
                                    <div class="dropdown" id="drop-category-node-139">
                                        <ul class="level1 ">
                                            <?php foreach(Subcat::where('cat',2)->orderBy('id','ASC')->get() as $menu){
												$subproname=str_replace('-', '_', $menu->name);
												$subproname=str_replace(' ', '-', $subproname);
												$subproname=strtolower($subproname);
												//echo $subproname;
											?>
											<li  class="level2 nav-2-2-1 first"><a href='{{url("/women/unstitched/{$subproname}")}}'><span>{{$menu->name}}</span></a></li>
											<?php } ?>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-2-3 parent"><a href='{{url("/women/ready-to-wear")}}' ><span>Ready To Wear</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-21')"></button>
                                    <div class="dropdown" id="drop-category-node-21"><ul class="level1 ">
                                        <?php foreach(Subcat::where('cat',3)->where('active',1)->orderBy('id','ASC')->get() as $menu){
												$subproname=str_replace('-', '_', $menu->name);
												$subproname=str_replace(' ', '-', $subproname);
												$subproname=strtolower($subproname);
												//echo $subproname;
											?>
											<li  class="level2 nav-2-3-1 first"><a href='{{url("/women/ready-to-wear/{$subproname}")}}'><span>{{$menu->name}}</span></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-2-4 last parent"><a href='{{url("/women/accessories")}}' ><span>Accessories</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-42')"></button>
                                    <div class="dropdown" id="drop-category-node-42"><ul class="level1 ">
                                        <?php foreach(Subcat::where('cat',4)->orderBy('id','ASC')->get() as $menu){
												$subproname=str_replace('-', '_', $menu->name);
												$subproname=str_replace(' ', '-', $subproname);
												$subproname=strtolower($subproname);
												//echo $subproname;
											?>
											<li  class="level2 nav-3-4-1 first"><a href='{{url("/women/accessories/{$subproname}")}}'><span>{{$menu->name}}</span></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-2-4 last parent"><a href="#" ><span>Collection</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-43')"></button>
                                    <div class="dropdown" id="drop-category-node-43">
                                        <ul class="level1 ">
                                             @foreach($campaigns as $campaign)
                                               @if($campaign->category == 1 || $campaign->category==6)
                                                <li  class="level2 nav-3-4-1 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                               @endif
                                            @endforeach
                                         <!--<li  class="level2 nav-3-4-1 first"><a href="{{url('/eid-collection/woman/9')}}" ><span>Eid Collection 2021</span></a></li>-->
                                         
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li  class="level0 nav-8 last level-top parent"><a href='{{url("/men")}}'  class="level-top" ><span>Men</span></a>
                        <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-456')"></button>
                        <div class="dropdown" id="drop-category-node-456"><ul class="level0 ">
                                <li  class="level1 nav-8-1 first parent">
                                    <a href='{{url("/men/ethnic-meanswear/")}}' ><span>Ethnic Meanswear</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-459')"></button>
                                    <div class="dropdown" id="drop-category-node-459"><ul class="level1 ">
                                        <?php foreach(Subcat::where('cat',5)->orderBy('id','ASC')->get() as $menu){
												$subproname=str_replace('-', '_', $menu->name);
												$subproname=str_replace(' ', '-', $subproname);
												$subproname=strtolower($subproname);
											?>
                                            <li  class="level2 nav-3-1-2"><a href='{{url("/men/ethnic-meanswear/{$subproname}")}}' ><span>{{$menu->name}}</span></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-8-3 last parent"><a href="#" ><span>Collection</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-461')"></button>
                                    <div class="dropdown" id="drop-category-node-461">
                                        <ul class="level1 ">
                                            @foreach($campaigns as $campaign)
                                               @if($campaign->category == 2 || $campaign->category==6)
                                                <li  class="level2 nav-3-4-1 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                               @endif
                                            @endforeach
                                            <!--<li  class="level2 nav-3-4-1 first"><a  href="{{url('/eid-collection/man/17')}}" ><span>Eid Collection 2021</span></a></li> -->
                                           
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="level0 nav-3 level-top parent"><a href="{{url('/kids')}}"  class="level-top" ><span>Kids</span></a>
                        <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-3')"></button>
                        <div class="dropdown" id="drop-category-node-3"><ul class="level0 ">
                                <li  class="level1 nav-3-1 first parent"><a href="{{url("/kids/girls")}}" ><span>Girls</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-5')"></button>
                                    <div class="dropdown" id="drop-category-node-5"><ul class="level1 ">
                                        <?php foreach(Subcat::where('cat',8)->orderBy('id','ASC')->get() as $menu){
            									$subproname=str_replace('-', '_', $menu->name);
            									$subproname=str_replace(' ', '-', $subproname);
            									$subproname=strtolower($subproname);
            								?>
            								    <li  class="level2 nav-3-1-1 first"><a href='{{url("/kids/girls/{$subproname}")}}' ><span>{{$menu->name}}</span></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-3-2 parent"><a href="{{url('/kids/boys')}}" ><span>Boys</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-171')"></button>
                                    <div class="dropdown" id="drop-category-node-171">
                                        <ul class="level1 ">
                                            <?php foreach(Subcat::where('cat',9)->orderBy('id','ASC')->get() as $menu){
												$subproname=str_replace('-', '_', $menu->name);
												$subproname=str_replace(' ', '-', $subproname);
												$subproname=strtolower($subproname);
											?>
											<li  class="level2 nav-3-2-1 first"><a href='{{url("/kids/boys/{$subproname}")}}' ><span>{{$menu->name}}</span></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-3-3 parent"><a href="#" ><span>Collection</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-6')"></button>
                                    <div class="dropdown" id="drop-category-node-6"><ul class="level1 ">
                                           @foreach($campaigns as $campaign)
                                               @if($campaign->category == 3 || $campaign->category==6)
                                                <li  class="level2 nav-3-4-1 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                               @endif
                                            @endforeach
                                           <!--<li  class="level2 nav-3-4-1 first"><a  href="{{url('/eid-collection/kids/6')}}" ><span>Eid Collection 2021</span></a></li> -->
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li  class="level0 nav-4 level-top parent"><a href="{{url('/all/accessories')}}"  class="level-top" ><span>Accessories</span></a>
                        <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-54')"></button>
                        <div class="dropdown" id="drop-category-node-54">
                            <ul class="level0 ">
                                <li  class="level1 nav-4-1 first parent"><a href="{{url('/women/accessories')}}" ><span>women</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-165')"></button>
                                    <div class="dropdown" id="drop-category-node-165">
                                        <ul class="level1 ">
                                            <li  class="level2 nav-4-1-5"><a href="{{url('/women/accessories/jewelry')}}" ><span>Jewellery</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-4-2 parent"><a href="{{url('/kids/girls/jewelry')}}" ><span>Kids</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-1787')"></button>
                                    <div class="dropdown" id="drop-category-node-1787">
                                        <ul class="level1 ">
                                            <li  class="level2 nav-4-2-3"><a href="{{url('/kids/girls/jewelry')}}" ><span>Jewellery</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-4-3 last parent"><a href="#" ><span>Collection</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-301')"></button>
                                    <div class="dropdown" id="drop-category-node-301"><ul class="level1 ">
                                           @foreach($campaigns as $campaign)
                                               @if($campaign->category == 4 || $campaign->category==6)
                                                <li  class="level2 nav-3-4-1 first"><a href='{{url("/collection/{$campaign->slug}")}}' ><span>{{$campaign->name}}</span></a></li>
                                               @endif
                                            @endforeach
                                            <!--<li  class="level2 nav-3-4-1 first"><a href="#" ><span>Spring 2019</span></a></li>-->
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li  class="level0 nav-5 level-top parent"><a href="{{url('/all/home')}}"  class="level-top" ><span>Pride Homes</span></a>
                        <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-1024')"></button>
                        <div class="dropdown" id="drop-category-node-1024"><ul class="level0 ">
                                <li  class="level1 nav-5-1 first parent"><a href="{{url('home/home')}}" ><span>Cushion Covers</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-1103')"></button>
                                    <div class="dropdown" id="drop-category-node-1103">
                                        <ul class="level1 ">
                                            <?php foreach(Subcat::where('cat',10)->orderBy('id','ASC')->get() as $menu){
                                              $subproname=str_replace('-', '_', $menu->name);
                                              $subproname=str_replace(' ', '-', $subproname);
                                              $subproname=strtolower($subproname);
                                            ?>
                                            <li  class="level2 nav-5-1-3"><a href='{{url("/homes/cushion/{$subproname}")}}' ><span>{{ ucwords($menu->name) }}</span></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                                <li  class="level1 nav-5-3 parent"><a href="#" ><span>Collection</span></a>
                                    <button class="open-close plus" value="Show/Hide" onclick="showHideDiv('drop-category-node-1104')"></button>
                                    <div class="dropdown" id="drop-category-node-1104">
                                        <ul class="level1 ">
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--<li  class="level0 nav-6 level-top"><a href="#"  class="level-top" ><span>Crafted</span></a></li>-->
                </ul>
            </div>
        </div>
    </nav>

