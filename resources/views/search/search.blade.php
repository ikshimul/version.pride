<?php
$count = count($search);
if ($count > 0) {
    ?>
    <div class="products" data-bind="visible: result.products.data().length > 0" style="">
        <div class="title">
            <span>Products</span>
            <a class="see-all" data-bind="attr: {href: result.products.url}" href="{{url("/search-view/{$key}")}}">
                <span>See All</span>
                <span data-bind="text: result.products.size"><?php echo $count; ?></span>
            </a>
        </div>
        <ul id="products" role="listbox" data-bind="foreach: result.products.data">
            <?php
            foreach ($search as $product) {
                $product_name = str_replace(' ', '-', $product->product_name);
                $product_url = strtolower($product_name);
                $product_code = strtolower($product->product_styleref);
                $color_album=str_replace('/','-',$product->productalbum_name);
                $color_album = strtolower($color_album);
                ?>
                <li>
                    <div class="qs-option-image" data-bind="visible: image">
                        <a data-bind="attr: { href: url, title: name }" href="{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}" title="{{$product->product_name}}">
                            <img data-bind="attr: { src: image, title: name }" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img; ?>" title="{{$product->product_name}}">
                        </a>
                    </div>
                    <div class="qs-option-info" data-bind="css: {noimage: !image}">
                        <div class="qs-option-title" data-bind="visible: name">
                            <a data-bind="attr: { href: url, title: name }, text: name" href="{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}" title="{{$product->product_name}}">{{$product->product_name}}</a>
                        </div>
                        <div class="qs-option-reviews" data-bind="html: reviews_rating, visible: reviews_rating" style="display: none;"></div>
                        <div class="qs-option-sku" data-bind="visible: sku"><span>Code</span>: <span data-bind="text: sku"><?php echo $product->product_styleref; ?></span></div>
                        <div class="qs-option-shortdescription" data-bind="text: short_description, visible: short_description" style="display: none;"></div>
                        <div class="qs-option-description" data-bind="text: description, visible: description" style="display: none;"></div>
                        <div class="qs-option-price" data-bind="html: price, visible: price"><div class="price-box price-final_price" data-role="priceBox" data-product-id="{{$product->id}}" data-price-box="product-id-{{$product->id}}">


                                <span class="price-container price-final_price tax weee">
                                    <span id="product-price-{{$product->product_price}}" data-price-amount="{{$product->product_price}}" data-price-type="finalPrice" class="price-wrapper ">
                                        <span class="price">Tk {{$product->product_price}}</span>    
                                    </span>
                                </span>

                            </div></div>
                        <div class="qs-option-addtocart" data-bind="with: add_to_cart, visible: add_to_cart" style="display: none;"></div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } else { ?>
    <div class="no-result" data-bind="visible: !anyResultCount()" style="display: block;color:red;"><!-- ko i18n: 'No Result'--><span>No Result</span><!-- /ko --></div>
<?php } ?>


