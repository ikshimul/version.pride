<?php
use App\Http\Controllers\product\ProductController;
use App\Models\Productalbum;
use App\Models\Subcat;
use App\Models\Campaign;
?>
@extends('layouts.app')
@section('title','Search Result')
@section('content')
<link rel="stylesheet" href="{{asset('assets/css/list.css')}}?17" />
<style>
table {
	border-collapse: collapse;
	border-spacing: 0;
}

a {
	text-decoration: none;
}
 .page-main{
     padding-top: 0px;
 }
strong {
  font-weight: bold;
}
.sidebar .filter-holder ul li .opener-cate {
    font-family: open\ sans\ condensed,sans-serif!important;
    font-size: 14px;
}
 .loading-modal {
            display:    none;
            position:   fixed;
            z-index:    1000;
            top:        0;
            left:       0;
            height:     100%;
            width:      100%;
            background: rgba( 255, 255, 255, .8 ) 
                        url("{{url('/')}}/storage/app/public/pride-loading-gif_new.gif") 
                        50% 50% 
                        no-repeat;
        }
</style>
<main id="maincontent" class="page-main">
    <a id="contentarea" tabindex="-1"></a>
    <div class="beadcumarea">
        <div class="container"><div class="row"><div class="col-xs-12"><div class="breadcrumbs">
                        <ul class="items">
                            <li class="item home">
                                <a href="{{url('/')}}" title="Go to Home Page">Search Title</a>
                            </li>
                            <li class="item">
                                <a href="{{url("/search-view/{$key}")}}" title=""><?php echo $key;?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="sidebar">
                    <div class="block block-banner">
                        <div class="block-content">
                            <a href="#" class="opener-filter">filters</a>
                            <div class="filter-holder">
                                <div class="filter-content">
                                    <ul class="list-inline">
                                        <!--<li>
                                            <a class="opener-cate" href="#"> Category <span class="icon-down"></span></a> 
                                            <div class="filter-block">

                                                <ol class="m-filter-item-list">
                                                    
                                                </ol>
                                            </div>
                                        </li>-->
                                        <li id='filter-price'>
                                            <a class="opener-cate" href="#"> Price <span class="icon-down"></span></a>
                                            <div class="filter-block">
                                                <?php
                                                    if(!isset($lower_price))
                                                        $lower_price = 0;
                                                    if(!isset($upper_price))
                                                        $upper_price = 0;
                                                ?>
                                                <ol class="m-filter-item-list">
                                                    <li class="item"><a class="<?php if($lower_price == 500 && $upper_price == 1000) echo 'active'; ?>" href="{{ url("search-by-price/{$key}/500/1000") }}"><span>Tk 500</span> - <span>Tk 1,000</span></a></li>
                                                    <li class="item"><a class="<?php if($lower_price == 1000 && $upper_price == 1500) echo 'active'; ?>" href="{{ url("search-by-price/{$key}/1000/1500") }}"><span>Tk 1,000</span> - <span>Tk 1,500</span></a></li>
                                                    <li class="item"><a class="<?php if($lower_price == 1500 && $upper_price == 2000) echo 'active'; ?>" href="{{ url("search-by-price/{$key}/1500/2000") }}"><span>Tk 1,500</span> - <span>Tk 2,000</span></a></li>
                                                    <li class="item"><a class="<?php if($lower_price == 2000 && $upper_price == 2500) echo 'active'; ?>" href="{{ url("search-by-price/{$key}/2000/2500") }}"><span>Tk 2,000</span> - <span>Tk 2,500</span></a></li>
                                                    <li class="item"><a class="<?php if($lower_price == 2500 && ($upper_price == 0 || $upper_price == 99999)) echo 'active'; ?>" href="{{ url("search-by-price/{$key}/2500") }}"><span>Tk 2,500</span> and above</a></li>
                                                </ol>
                                            </div>
                                        </li>
                                        <li id='filter-size'>
                                            <a class="opener-cate" href="#"> Size <span class="icon-down"></span></a>
                                            <div class="filter-block">
                                                <?php
                                                    if(!isset($size))
                                                        $size = null;
                                                ?>
                                                <ol class="m-filter-item-list">
                                                    <li class="item"><a class="<?php if($size == '38') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/38") }}"><span>38</span></a></li>
                                                    <li class="item"><a class="<?php if($size == '40') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/40") }}"><span>40</span></a></li>
                                                    <li class="item"><a class="<?php if($size == '42') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/42") }}"><span>42</span></a></li>
                                                    <li class="item"><a class="<?php if($size == '44') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/44") }}"><span>44</span></a></li>
                                                    <li class="item"><a class="<?php if($size == '46') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/46") }}"><span>46</span></a></li>
                                                    <li class="item"><a class="<?php if($size == '48') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/48") }}"><span>48</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'XS') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/XS") }}"><span>XS</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'S') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/S") }}"><span>S</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'M') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/M") }}"><span>M</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'L') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/L") }}"><span>L</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'XL') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/XL") }}"><span>XL</span></a></li>
                                                    <li class="item"><a class="<?php if($size == 'XXL') echo 'active'; ?>" href="{{ url("search-by-size/{$key}/XXL") }}"><span>XXL</span></a></li>
                                                </ol>
                                            </div>
                                        </li>
                                        <li id='filter-color'>
                                            <a class="opener-cate" href="#"> Color <span class="icon-down"></span></a>
                                            <div class="filter-block">
                                                <?php
                                                    if(!isset($color_name))
                                                        $color_name = null;
                                                    if(!isset($color_names))
                                                        $color_names = [];
                                                ?>
                                                <ol class="m-filter-item-list">
                                                    @foreach($color_names as $color)
                                                    <li class="item"><a class="<?php if($color_name == $color) echo 'active'; ?>" href="{{ url("search-by-color/{$key}/{$color}") }}"><span>{{ $color }}</a></li>
                                                    @endforeach
                                                </ol>
                                            </div>
                                        </li>

                                    </ul>
                                </div>   
                            </div>



                        </div>
                    </div>



                </div>
            </div>
            <div class="col-md-10">
                <input name="form_key" type="hidden" value="" />
                <div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
                </div>



                <style>
                    .product-items .product-item-photo .product-image-wrapper {
                        display: block;
                        overflow: hidden;
                        position: relative;
                        border: 0px solid #00000012;
                    }
                </style>
                <div class="products wrapper grid products-grid">
                    <ol class="products list items product-items">
                        @foreach($search as $product)
                        <?php
                        $product_name = str_replace(' ', '-', $product->product_name);
                        $product_url = strtolower($product_name);
                        $pro_album = Productalbum::where('product_id',$product->id)->orderBy('id','DESC')->first();
                        $sold_out = ProductController::ProductWiseQty($product->id);
                        ?>
                        <li class="item product product-item">    
                           <?php
                            date_default_timezone_set('Asia/Dhaka');
    						$today=date('d-m-Y');
    						$insert_date=$product->created_at;
    						$datetime1 = new DateTime($today);
    						$datetime2 = new DateTime($insert_date);
    						$interval = $datetime1->diff($datetime2);
    						$date_difference=$interval->format('%a');
    						$product_code = strtolower($product->product_styleref);
    						$color_album=str_replace('/','-',$product->productalbum_name);
    						$color_album = strtolower($color_album);
                           if($sold_out <= 0){ ?>
                             <!--<span class="sprice-tag">Sold Out</span> --->
                             <span class="sold-out">Sold Out</span>
                           <?php }else if($product->product_pricediscounted > 1){ ?>
							   <span class="sprice-tag"><?php echo $product->product_pricediscounted;?>% Off</span>
						   <?php  } if($date_difference < 31){ ?>
                             <div class="tag_container round_tag_lt bg_red t_white"><span class="ttl_header">New</span></div> 
                           <?php } ?>
                            <div class="product-item-info" data-container="product-grid">
                                <?php if($product->subcat==22){?>
                                <a href='{{url("shop/{$product_url}/color-{$color_album}/{$product->id}")}}'>
                                <?php }else{ ?>
                                <a href='{{url("shop/{$product_url}/{$product_code}/{$color_album}")}}'>
                                <?php } ?>
                                    <span class="product-image-container">
                                        <span class="product-image-wrapper" >
                                            <span class="custom-carousel" onmouseenter="fadeImages(this)" onmouseleave="removeTimer(this)">
                                                <?php $images = ProductController::GetProductImageByColorAlbum($pro_album->id); ?>
                                                @php($i = 0)
                                                @foreach($images as $image)
                                                <img class="item large_img<?php if($i==0) echo ' active';?> swatch_img_<?php echo $product->id;?>" src="{{ URL::to('') }}/storage/app/public/pgallery/{{ $product->productimg_img_medium }}" alt="No Image Found"/>
                                                @php($i++)
                                                @endforeach
                                            </span>
                                        </span>
                                    </span>
                                    <div class="mz-hint hidden-xs" style="display: block;">
										<a class="mz-hint-message btn-view hidden-xs" title='{{$product->product_name}}' data-id="<?php echo $product->id;?>" data-color="<?php echo $product->productalbum_name;?>">
										  <img style="opacity: 0;" src="{{ URL::to('') }}/storage/app/public/len.webp" alt="Click to zoom"/>Click to zoom
										</a>
								   </div>
                                </a>
                                <div class="product details product-item-details">
                                    <div class="info-holder">
                                        <strong class="product name product-item-name">
                                            <a class="product-item-link"
                                               href='{{url("shop/{$product_url}/{$product_code}/{$color_album}")}}'>
                                                {{$product->product_name}}
                                            </a>
                                        </strong>
										 <!-- swatch --->
                                        <?php
										$data = ProductController::GetProductColorAlbum($product->id);
										?>
										<div class="ws_100 swatch round show_desktop show_tablet">
											<ul class="color">
													<li class="multi">
														<span class="selected">
															<input type="hidden" id="color_album" value="{{$product->productalbum_name}}"/>
															<input type="hidden" id="id" value="{{$product->id}}"/>
															<img  id="{{$product->id}}"  src="{{ URL::to('') }}/storage/app/public/pgallery/{{$product->productalbum_img}}" data-color="01" data-modal-target="#quickview_pop">
														</span>
													</li>
											</ul>
										</div>
										 <!-- swatch --->
                                    </div>
                                    <div class="info-holder">
                                       <?php if ($product->product_pricediscounted < 1) { ?>
										<div class="price-box price-final_price" data-role="priceBox">
											<span class="price-container price-final_price tax weee">
												<span class="price-label">Regular Price</span>
												<span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
													<span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
											</span>
										</div>
										<?php }else{?>
										<div class="price-box price-final_price" data-role="priceBox" data-product-id="84726" data-price-box="product-id-84726">
											<span class="normal-price">
												<span class="price-container price-final_price tax weee">
														<span id="product-price-84726" data-price-amount="600" data-price-type="finalPrice" class="price-wrapper ">
															<span class="price">Tk &nbsp;{{$product->discount_product_price}}</span>
														</span>
												</span>
											</span>
										</div>                                                            
										<div class="price-box price-final_price" data-role="priceBox">
												<span class="old-price">
													<span class="price-container price-final_price tax weee">
														<span class="price-label">Regular Price</span>
														<span id="old-price-84726" data-price-amount="1400" data-price-type="oldPrice" class="price-wrapper ">
															<span class="price">Tk &nbsp;{{$product->product_price}}</span></span>
													</span>
												</span>
										</div>
									<?php } ?>                                                      
                                        <div class="product-item-inner">
                                            <div class="product actions product-item-actions">
                                                <div class="actions-primary">
                                                    <a href='{{url("shop/{$product_url}/{$product_code}/{$color_album}")}}' class="action tocart primary"><span>Shop Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                </div>
                <div class="toolbar toolbar-products" data-mage-init='{""}'>
                    <div class="modes"></div>
                    <p class="toolbar-amount" id="toolbar-amount">
                        Items <span class="toolbar-number">1</span>-<span class="toolbar-number"><?php echo $search->perPage();?></span> of <span class="toolbar-number"><?php echo $search->total();?></span>    </p>
                    <div class="pages">
					     <center>{{{ $search->links() }}} </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div id="quick-view"></div>
<script>
jQuery(window).ready(function($){
	$('.btn-view').on('click', function(){
	//var product_id=700;
	//$(window).scrollTop(0);
	var product_id = $(this).data('id');
	var product_color = $(this).data('color');
	var url_op = base_url + "/ajax/ajax-product-details";
	$(".loading-modal").show();
	$.ajax({
			url: url_op,
			type: 'GET',
			data: {id: product_id,color:product_color},
			success: function (html) {
			   //alert(html);
			   if(html=='null'){
				   $("#quick-view").html("<p style='text-align:center;color:red;padding-top:20px;'>Server Error.</p>");
			   }else{
			     
				 $("#quick-view").html(html);
				 $(".loading-modal").hide();
				 var product_id =$('#productid').val();
                 var color_name = product_color;
				 var productSize = $('#product-size-input').val();
				 $('.swatch-element-size').click(function () {
				     //alert($(this).data('value'));
                    $('#product-size-input').val($(this).data('value'));
                    
                });
                getQtyByColorSize(product_id, color_name, productSize);
                $('#pqty').on('change', function (e) {
                    var pqty = e.target.value;
                    $('#productQty').val(pqty);
                });
                $('.swatch-element-size').on('click', function (e) {
        			var productSize = $('#product-size-input').val();
        			$(".loading-modal").show();
                    getQtyByColorSize(product_id, color_name, productSize);
                });
                $('#colorName').on('change', function (e) {
                    var productColor = e.target.value;
                    $('#productColor').val(productColor);
                });
        
                function  getQtyByColorSize(product_id, color_name, productSize) {
                    var url_op = base_url + "/ajaxcall-getQuantityByColor/" + product_id + '/' + productSize + '/' + color_name;
                    $.ajax({
                        url: url_op,
                        type: 'GET',
                        dataType: 'json',
                        data: '',
                        success: function (stock) {
                           //  alert(html);
                           $(".loading-modal").hide();
                            var qty = stock;
                          //  alert(qty);
                            if (qty > 0) {
                                $('#qty').attr({"max": qty});
        						$("#sold_out_msg").html(" ");
        						$(".product-inventory").show();
        						$("#stock-msg").html("<span style='color:green;'>In stock</span>");
        						$('#qty').val(1);
        						$('#product-add-to-cart').val("Add to Cart");
                                document.getElementById("product-add-to-cart").disabled = false;
                            } else {
        						$('#qty').val(0);
        						$('#qty').attr({"max": 0});
        						$("#sold_out_msg").html("<span style='color:red;font-weight:700;padding-bottom:10px;'>" + productSize + " size has sold out. Please select another size.</span>");
        						$(".product-inventory").show();
        						$("#stock-msg").html("<span style='color:red;'>Out of stock</span>");
        						$('#product-add-to-cart').val("Out of stock");
        						document.getElementById("product-add-to-cart").disabled = true;
        					}
                        }
                    });
                }
				 $('.close-modal').on('click', function(){
					$('.ajax-quickview').fadeOut();
				});
				  
			   }
			}
		});
		
	});
		
});
</script>
<script>
jQuery(document).ready(function($){
	jQuery('.swatch .color .multi').click(function () {
		var id = $(this).find('span img').attr('id');
		var rel = $(this).find('span img').attr('rel');
		 //alert(id);
		//console.log(id);
		jQuery('.swatch_img_'+ id).attr('src',"https://pride-limited.com/storage/app/public/loader.gif");
		jQuery('.swatch_img_'+ id).attr('src',rel);
		//jQuery(this).parents('div:eq(0)').parents('div:eq(0)').find('.fancybox').attr('href', jQuery(this).attr('rel'));
		return false;
	});
	jQuery(".fancybox").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false
	});
});
var timer1;
var timer2;
var counter=1;
var running = false;
(function() {
	
})();
function updateActive(items, i) {
	i = i%(items.length);
	items[i>0?i-1:items.length-1].classList.remove('active');
	items[i].classList.add('active');
	counter++;
	if(!running) {
    	timer2 = setInterval(function() {
    		updateActive(items, counter);
    	}, 1500);
    	running = true;
    	clearInterval(timer1);
	}
}
function fadeImages(element) {
	var items = element.getElementsByClassName('item');
	for (var i = 0; i < items.length; i++) {
		items[i].classList.remove('active');
	}
	items[1].classList.add('active');
	timer1 = setInterval(function() {
		updateActive(items, counter);
	}, 100);
}
function removeTimer(element) {
    clearInterval(timer1);
	clearInterval(timer2);
	counter=1;
	var items = element.getElementsByClassName('item');
	for (var i = 0; i < items.length; i++) {
		items[i].classList.remove('active');
	}
	items[0].classList.add('active');
	running = false;
}
jQuery('.show_tablet img').click(function () {
	var id = jQuery(this).attr('id');
	 //alert(id);
	// console.log(jQuery(this));
	jQuery('.swatch_img_'+ id).attr('src', jQuery(this).attr('rel'));
	//jQuery(this).parents('div:eq(0)').parents('div:eq(0)').find('.fancybox').attr('href', jQuery(this).attr('rel'));
	return false;
});
/* Open filter category */
(function(){
    <?php if(!((isset($lower_price) && $lower_price !=0) || (isset($upper_price) && $upper_price !=0) || (isset($size) && $size != null) || (isset($color_name) && $color_name != null)))  {?>
        document.getElementById('filter-category').classList.add('active');
    <?php } elseif((isset($lower_price) || isset($upper_price)) && ($lower_price != 0) && ($upper_price != 0)) { echo 'console.log("'.$lower_price.' and '.$upper_price.'");';?>
        document.getElementById('filter-price').classList.add('active');
    <?php } elseif(isset($size) && $size != null) { ?>
        document.getElementById('filter-size').classList.add('active');
    <?php } elseif(isset($color_name) && $color_name != null) { ?>
        document.getElementById('filter-color').classList.add('active');
    <?php } elseif(isset($fabric) && $fabric != null) { ?>
        document.getElementById('filter-fabric').classList.add('active');
    <?php }?>
})();
</script>
@endsection