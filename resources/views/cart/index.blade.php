<?php

use App\Http\Controllers\cart\CartController;
?>
@extends('layouts.app')
@section('title','Shopping Cart')
@section('content')
<style>
    #update_tbl{
        display: none;
    }
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        font-family: monospace;
    }
    .alert-danger {
        color: #ec061c;
        background-color: #f8d7da;
        border-color: #f5c6cb;
        font-family: monospace;
    }
    .table thead th {
        vertical-align: bottom;
        border-bottom: 0px solid #dee2e6; 
        text-transform: uppercase;
        font-size: 12px;
    } 
    .table th {
        padding: .75rem;
        vertical-align: top;
        border-top: 0px solid #dee2e6;
    }
    .table > tbody > tr > td {
        vertical-align: middle;
    }
    .mt-5, .my-5 {
        margin-top: 0rem !important;
    }
    .fa {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        font-size: 13px;
        -moz-osx-font-smoothing: grayscale;
    }
    .form-nav { margin-bottom:40px; display:inline-block; width:100%; }
    .form-nav-item { position:relative; float:left; width:20%; text-align:center; font-size:13px; color:#9bb0bb; font-weight:700; }
    .form-nav-item span {
        display:inline-block;
        background:#291d88;
        color:white;
        width:35px;
        height:35px;
        text-align:center;
        border-radius:100%;
        font-size:15px;
        padding-top:7px;
        margin-bottom:7px;
        box-shadow:0px 0px 20px rgba(0, 0, 0, 0.07);
        position:relative;
        z-index:10;
    }

    .form-nav-item:after {
        content:'';
        width:120px;
        height:4px;
        display:block;
        background:white;
        position:absolute;
        right:-60px;
        top:17px;
    }
    .multi-steps > li.is-active:before, .multi-steps > li.is-active ~ li:before {
        content: counter(stepNum);
        font-family: inherit;
        font-weight: 700;
    }
    .multi-steps > li.is-active:after, .multi-steps > li.is-active ~ li:after {
        background-color: #ededed;
    }

    .multi-steps {
        display: table;
        table-layout: fixed;
        width: 100%;
    }
    .multi-steps > li {
        counter-increment: stepNum;
        text-align: center;
        display: table-cell;
        position: relative;
        color: #291d88;
    }
    .multi-steps > li:before {
        content: '\f00c';
        content: '\2713;';
        content: '\10003';
        content: '\10004';
        content: '\2713';
        display: block;
        margin: 0 auto 4px;
        background-color: #fff;
        width: 36px;
        height: 36px;
        line-height: 32px;
        text-align: center;
        font-weight: bold;
        border-width: 2px;
        border-style: solid;
        border-color: #291d88;
        border-radius: 50%;
    }
    .multi-steps > li:after {
        content: '';
        height: 2px;
        width: 100%;
        background-color: #291d88;
        position: absolute;
        top: 16px;
        left: 50%;
        z-index: -1;
    }
    .multi-steps > li:last-child:after {
        display: none;
    }
    .multi-steps > li.is-active:before {
        background-color: #fff;
        border-color: #291d88;
    }
    .multi-steps > li.is-active ~ li {
        color: #808080;
    }
    .multi-steps > li.is-active ~ li:before {
        background-color: #ededed;
        border-color: #ededed;
    }
    #shopping-cart-table .product-item-photo {
        width:100px;
    }
    .cart-container .cart-summary .action.checkout {
    background: #291e88;
   }
   .cart-container .form-cart .actions .action.continue, .cart-container .form-cart .actions .action.update{
       background: #291e889e;
   }
</style>
<div id="search_result">
    <main id="maincontent" class="page-main" style="padding-top: 62px;"><a id="contentarea" tabindex="-1"></a>
        <div class="beadcumarea">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12"><div class="breadcrumbs">
                            <ul class="items">
                                <li class="item home">
                                    <a href="{{url('/')}}" title="Go to Home Page">Home</a>
                                </li>
                                <li class="item cms_page">
                                    <strong>Shopping Cart</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="authenticationPopup" data-bind="scope:'authenticationPopup'">
					  <!--  <center>
						@if (session('update'))
						<div class="alert alert-success">
							{{ session('update') }}
						</div>
						@endif
					</center>
					<center>
						@if (session('delete'))
						<div class="alert alert-danger">
							{{ session('delete') }}
						</div>
						@endif
					</center> --->
                    </div>
                    <div class="cart-container">
                        <div class="form form-cart">
                            <div class="cart table-wrapper">
                                <table id="shopping-cart-table"
                                       class="cart items data table"
                                       data-mage-init='{"shoppingCart":{"emptyCartButton": "action.clear",
                                       "updateCartActionContainer": "#update_cart_action_container"}}'>
                                    <caption role="heading" aria-level="2" class="table-caption">Shopping Cart Items</caption>
                                    <thead>
                                        <tr>
                                            <th class="col item" scope="col"><span>Item</span></th>
                                            <th class="col price" scope="col"><span>Price</span></th>
											<th class="col color">Color</th>
											<th class="col size">Size</th>
                                            <th class="col qty" scope="col"><span>Qty</span></th>
                                            <th class="col subtotal" scope="col"><span>Subtotal</span></th>
                                        </tr>
                                    </thead>
                                    <tbody class="cart item">
                                        <?php
                                        $i = 0;
                                        foreach (Cart::instance('products')->content() as $row) :
                                            $i++;
                                            $name = $row->name;
                                            $pro_name = str_replace(' ', '-', $name);
                                            $product_url = strtolower($pro_name);
                                            $id = $row->id;
                                            $color = $row->options->color;
                                            $color_album = str_replace('/', '-', $color);
                                            ?>
                                            <tr class="item-info">
                                           <form action="{{url('/cart/update-product')}}" method="post" enctype="multipart/form-data">
                                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                            <input name="product_id" id="product_id_{{$i}}" type="hidden" value="<?php echo $row->id; ?>"/>
                                            <input type="hidden" name="image_link" value="<?php echo $row->options->product_image; ?>">
                                            <input type="hidden" name="rowid" value="<?php echo $row->rowId; ?>">
                                            <input class="form-control" type="hidden" id="ddlcolor_{{$i}}" name="colorname" value="<?php echo ($row->options->has('color') ? $row->options->color : ''); ?>">
											
                                            <td data-th="Item" class="col item">
                                                <a href="{{url("shop/{$product_url}/color-{$color_album}/{$row->id}")}}"
                                                   title="<?php echo $row->name; ?>"
                                                   tabindex="-1"
                                                   class="product-item-photo">

                                                    <span class="product-image-container">
                                                        <span class="product-image-wrapper">
                                                            <img class="product-image-photo" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo ($row->options->has('product_image') ? $row->options->product_image : ''); ?>" alt="<?php echo $row->name; ?>"/>
                                                        </span>
                                                    </span>
                                                </a>
                                                <div class="product-item-details">
                                                    <strong class="product-item-name">
                                                        <a href="{{url("shop/{$product_url}/color-{$color_album}/{$row->id}")}}"><?php echo $row->name; ?></a>
                                                    </strong>
                                                </div>
                                            </td>
                                            <td class="col price" data-th="Price">
                                                <span class="price-excluding-tax" data-label="Excl. Tax">
                                                    <span class="cart-price">
                                                        <span class="price">Tk <?php echo $row->price; ?></span>            </span>

                                                </span>
                                            </td>
											<td><?php echo ($row->options->has('color') ? $row->options->color : ''); ?></td>
											<td>
												<span id="size_show_<?php echo $i; ?>"><?php echo ($row->options->has('size') ? $row->options->size : ''); ?></span>
												<span id="size_hide_<?php echo $i; ?>" style="display:none;">
												<center>
												  <select class="form-control" name="sizename" id="ProductSize_<?php echo $i; ?>" style="max-width:130px">
														<?php
														$select_size = $row->options->size;
														$pro_id = $row->id;
														$color_name = $row->options->color;
														$sizelistlist = CartController::GetProductsizelist($pro_id, $color_name);
														foreach ($sizelistlist as $size) {
															?>
															<option value="<?php echo $size->productsize_size; ?>"<?php
															if ($size->productsize_size == $select_size) {
																echo ' selected';
															}
															?>><?php echo $size->productsize_size; ?></option>
																<?php } ?>
													</select>
												</center>
											 </span>
											</td>
                                            <td class="col qty" data-th="Qty">
                                                <div class="field qty" id="qty_show_<?php echo $i; ?>">
                                                    <label class="label" for="">
                                                        <span>Qty</span>
                                                    </label>
                                                    <div class="control qty">
                                                        <input id=""
                                                               value="<?php echo $row->qty; ?>"
                                                               type="number"
                                                               size="4"
                                                               title="Qty"
                                                               class="input-text qty"
                                                               data-validate="{required:true,'validate-greater-than-zero':true}"
                                                               data-role="cart-item-qty"/>
                                                    </div>
                                                </div>
												 <span id="qty_hide_<?php echo $i; ?>" style="display:none;">
												<center>
												 <select class="form-control" name="quantity" id="ProductQty_{{$i}}" style="max-width:130px">
													<?php
													$qty = CartController::GetProductqty($pro_id, $select_size, $color_name);
													for ($j = 1; $j <= $qty; $j++) {
														?>
														<option value="<?php echo $j; ?>" <?php
														if ($j == $row->qty) {
															echo 'selected';
														}
														?>><?php echo $j; ?></option>
															<?php } ?>
												  </select>
												</center>
											 </span>
                                            </td>
                                            <td class="col subtotal" data-th="Subtotal">
                                                <span class="price-excluding-tax" data-label="Excl. Tax">
                                                    <span class="cart-price">
                                                        <span class="price">Tk <?php echo $row->price * $row->qty; ?></span>            
                                                    </span>
                                                </span>
                                            </td>
                                            </tr>
                                            <tr class="item-actions">
                                                <td colspan="100">
                                                    <div class="actions-toolbar">
                                                        <div id="gift-options-cart-item"
                                                             data-bind="scope:'giftOptionsCartItem'"
                                                             class="gift-options-cart-item">
                                                        </div>
                                                        <a class="action action-edit" href="#"  id="show_update_tbl_<?php echo $i; ?>" title="Edit item parameters"><span>Edit</span>
                                                        </a>
														 <button  style="display:none;cursor: pointer;width:auto;padding-left:20px;padding-right:20px;" id="update_btn_<?php echo $i; ?>">Update</button>
														 </form>
                                                        <a onclick="return confirm('Are you sure you want to remove this product?');"  href='{{url("cart/delete-product/{$row->rowId}")}}'
                                                           title="Remove item"
                                                           class="action action-delete"
                                                           data-post="#">
                                                            <span>Remove item</span>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <?php if (Cart::count() == 0) { ?>
                                            <tr>
                                                <td colspan="6"><center style="color:red;">Sorry! your shopping cart is empty!</center></td>
                                            </tr>    
                                        <?php } ?>
                                        </tbody>
										
                                </table>
                            </div>
							<input type="hidden" name="total_loop" value="<?php echo $i; ?>" id="total_loop">
                            <div class="cart main actions">
                                <a class="action continue"
                                   href="{{url('/')}}"title="Continue Shopping">
                                    <span>Continue Shopping</span>
                                </a>
                                <button type="submit"
                                        name="update_cart_action"
                                        data-cart-empty=""
                                        value="empty_cart"
                                        title="Clear Shopping Cart"
                                        class="action clear" id="empty_cart_button">
                                    <span>Clear Shopping Cart</span>
                                </button>
                               <!-- <button type="submit"
                                        name="update_cart_action"
                                        data-cart-item-update=""
                                        value="update_qty"
                                        title="Update Shopping Cart"
                                        class="action update">
                                    <span>Update Shopping Cart</span>
                                </button> --->
                            </div>
                        <div class="cart-summary">
                            <strong class="summary title">Summary</strong>
                            <div id="block-shipping" class="block shipping" data-mage-init='{"collapsible":{"openedState": "active", "saveState": true}}'>
                                <div class="title" data-role="title">
                                    <strong id="block-shipping-heading" role="heading" aria-level="2">
                                        Estimate Shipping and Tax        
                                    </strong>
                                </div>
                                <div id="block-summary" data-bind="scope:'block-summary'" class="content" data-role="content" aria-labelledby="block-shipping-heading">

                                </div>
                            </div>
                            <!--<div class="block discount" id="block-discount" data-mage-init='{"collapsible":{"openedState": "active", "saveState": false}}'>
                                <div class="title" data-role="title">
                                    <strong id="block-discount-heading" role="heading" aria-level="2">Apply Discount Code</strong>
                                </div>
                                <div class="content" data-role="content" aria-labelledby="block-discount-heading">
                                        <div class="fieldset coupon">
                                            <input type="hidden" name="remove" id="remove-coupon" value="0" />
                                            <div class="field">
                                                <label for="coupon_code" class="label"><span>Enter discount code</span></label>
                                                <div class="control">
                                                    <input type="text" class="input-text" id="coupon_code" name="coupon_code" value="" placeholder="Enter discount code"  />
                                                </div>
                                            </div>
                                            <div class="actions-toolbar">
                                                <div class="primary">
                                                    <button class="action apply primary" type="button" value="Apply Discount">
                                                        <span>Apply Discount</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div> --->
                            <div class="cart-total">
                                <ul class="checkout methods items checkout-methods-items">
                                    <li class="item">    
									<?php if (Cart::count() != 0) { ?>
                                        <button type="button" id="checkout"
                                                data-role="proceed-to-checkout"
                                                title="Proceed to Checkout"
                                                data-mage-init='' 
                                                class="action primary checkout"
                                                >
                                            <span>Proceed to Checkout</span>
                                        </button>
									<?php } ?>
                                    </li>
                                </ul>
                                <div id="cart-totals" class="cart-totals" data-bind="scope:'block-totals'">
                                    <div class="table-wrapper" data-bind="blockLoader: isLoading">
                                        <table class="data table totals">
                                            <caption class="table-caption" data-bind="text: $t('Total')">Total</caption>
                                            <tbody>
                                                <tr class="totals sub">
                                                    <th class="mark" colspan="1" scope="row" data-bind="i18n: title">Subtotal</th>
                                                    <td class="amount" data-th="Subtotal">
                                                        <span class="price" data-bind="text: getValue()">Tk <?php echo Cart::subtotal(); ?></span>
                                                    </td>
                                                </tr>
                                                <tr class="grand totals">
                                                    <th class="mark" colspan="1" scope="row">
                                                        <strong data-bind="i18n: title">Order Total</strong>
                                                    </th>
                                                    <td class="amount" data-th="Order Total">
                                                        <strong><span class="price" data-bind="text: getValue()">Tk <?php echo Cart::subtotal(); ?></span></strong>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </main>
</div>
<script>
    jQuery(document).ready(function () {
    jQuery("#show_update_tbl_1").click(function () {
    jQuery('#update_tbl').show();
    jQuery('#view_tbl').hide();
    jQuery("#show_update_tbl_1").hide();
    jQuery("#update_btn_1").show();
    jQuery("#qty_show_1").hide();
    jQuery("#qty_hide_1").show();
    jQuery("#size_show_1").hide();
    jQuery("#size_hide_1").show();
    });
    jQuery("#show_update_tbl_2").click(function () {
    jQuery('#update_tbl').show();
    jQuery('#view_tbl').hide();
    jQuery("#show_update_tbl_2").hide();
    jQuery("#update_btn_2").show();
    jQuery("#qty_show_2").hide();
    jQuery("#qty_hide_2").show();
    jQuery("#size_show_2").hide();
    jQuery("#size_hide_2").show();
    });
    jQuery("#show_update_tbl_3").click(function () {
    jQuery('#update_tbl').show();
    jQuery('#view_tbl').hide();
    jQuery("#show_update_tbl_3").hide();
    jQuery("#update_btn_3").show();
    jQuery("#qty_show_3").hide();
    jQuery("#qty_hide_3").show();
    jQuery("#size_show_3").hide();
    jQuery("#size_hide_3").show();
    });
    jQuery("#show_update_tbl_4").click(function () {
    jQuery('#update_tbl').show();
    jQuery('#view_tbl').hide();
    jQuery("#show_update_tbl_4").hide();
    jQuery("#update_btn_4").show();
    jQuery("#qty_show_4").hide();
    jQuery("#qty_hide_4").show();
    jQuery("#size_show_4").hide();
    jQuery("#size_hide_4").show();
    });
    jQuery("#update_cart_full").click(function () {
    jQuery('#update_tbl').show();
    jQuery('#view_tbl').hide();
    jQuery("#qty_show").hide();
    jQuery("#qty_hide").show();
    jQuery("#size_show").hide();
    jQuery("#size_hide").show();
    });
    jQuery("#clear_cart").submit(function() {
    if (!confirm('Do you really want to clear your shopping cart !'))
            event.preventDefault();
    });
    });
    </script>
<script>
    jQuery(document).ready(function ($) {
    var total_loop = jQuery("#total_loop").val();
    for (var i = 1; i <= total_loop; i++){
    // var ProColor = $('#ddlcolor' + '_' + i).val();
    jQuery('#ProductSize' + '_' + i).on('change', function(){
    var rownowithid = jQuery(this).attr('id');
    var rowno = rownowithid.split('_');
    var field_no = rowno[1];
    var ProSize = jQuery('#ProductSize' + '_' + rowno[1]).val();
    jQuery('#Update' + '_' + rowno[1]).show();
    var ProQty = jQuery('#ProductQty' + '_' + rowno[1]).val();
    var ProColor = jQuery('#ddlcolor' + '_' + rowno[1]).val();
    var productid = jQuery('#product_id' + '_' + rowno[1]).val();
    // alert(ProColor);
    getQtyByColorSize(productid, ProColor, ProSize, field_no)
    });
    function  getQtyByColorSize(productid, ProColor, ProSize, field_no){
    var url_op = base_url + "/ajaxcall-getQuantityByColor/" + productid + '/' + ProSize + '/' + ProColor;
    jQuery.ajax({
    url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (html) {
            var qty = html[0].SizeWiseQty;
           //  alert(field_no);
            if (qty > 0){
            jQuery('#ProductQty' + '_' + field_no).find('option').remove();
            for (var v = 1; v <= qty; v++){
            jQuery('#ProductQty' + '_' + field_no).append('<option value="' + v + '">' + v + '</option>');
             }
             jQuery('#update_btn' + '_' + field_no).removeAttr('disabled');
            }else{
            jQuery('#update_btn' + '_' + field_no).attr('disabled','disabled');
            alert(ProSize + ' size has sold out. Please select another size.');
            jQuery('#ProductQty' + '_' + field_no).find('option').remove();
            jQuery('#ProductQty' + '_' + field_no).append('<option value="' + 0 + '">' + 0 + '</option>');
            //alert(jQuery('#update_btn_' + '_' + field_no));
             }
          }
    });
    }
    }
    });
</script>
<script>
    jQuery(function () {
    jQuery("#checkout").on('click', function () {
    window.location = '{{ url('/checkout')}}';
    });
    });
</script>
@endsection