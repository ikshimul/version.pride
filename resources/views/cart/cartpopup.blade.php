
<style>
.cart_modal {
    display: block;
    overflow: hidden;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1050;
	background-color: #00000091;
    -webkit-overflow-scrolling: touch;
    outline: 0;
    align-items: center;
    justify-content: center;
}
.ajax-success-modal .content {
    background: #fff;
    bottom: auto;
    left: 50%;
    margin-left: -270px;
    margin-top: -100px;
    min-height: 200px;
    padding: 45px 40px 35px;
    position: fixed;
    right: auto;
    top: 50% !important;
    width: 535px;
}
.ajax-success-modal .ajax-left {
    float: left;
    margin-right: 20px;
    margin-bottom: 20px;
}
.ajax-success-modal .ajax-right {
    margin-left: 96px;
}
.ajax-product-title {
    text-align: left;
    margin-bottom: 12px !important;;
}
.product-title, .product-name a, .ajax-product-title {
    color: #000;
    display: block;
    margin-bottom: 6px;
    text-decoration: none!important;
}
.success-message {
    background: #f5f8f3 url(../../../storage/app/public/icons_new14.png)20px -1476px no-repeat;
    height: 40px;
    line-height: 40px;
    color: #291d88;
    padding: 0 10px 0 40px;
    margin-bottom: 20px !important;
    border-bottom: 1px solid #dde8d6;
}
.btn:hover, .btn-secondary:hover, .text-continue a, .product-item .btn, .list-category .view, .collection-title a.btn, #mc_embed_signup .input-group-btn .btn, .spr-summary-actions a, .spr-form-actions .btn:hover, .btn.spr-button, #get-rates-submit, #create_customer .action-btn input, .template-customers-addresses .grid .large--right a, .ajax-success-modal .btn-secondary, .infinite-scrolling a, .infinite-scrolling-homepage a {
    background-color: #ffffff;
    color: #291d88;
	font-weight: 600;
    border-color: #00000047;
}
.ajax-success-modal .btn-secondary {
    padding: 0 20px;
    margin-bottom: 10px;
}
button{
	  /*width: auto !important;*/
     /*border: 2px solid #291d88 !important;*/
     /*border-color: #291d88 !important;*/
}
#email-modal .window-window .window-content .btn.close, .close-window, .close-modal {
    padding: 0;
    height: 29px;
    width: 29px;
    position: absolute;
    right: -15px;
    top: -15px;
    z-index: 9;
    text-indent: -999em;
    border: 0;
    background: url(../../../storage/app/public/icons_new14.png) 0 -877px #291d88;
    cursor: pointer;
    filter: alpha(opacity=100);
    opacity: 1;
    filter: alpha(opacity=100);
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
}
@media (max-width: 767px){
	.ajax-success-modal .content {
		width: 100%;
		margin-left: 0;
		margin-top: 0;
		left: 0;
		top: 60px!important;
		padding: 20px 10px;
		min-height: 180px;
	}
	.ajax-success-modal .ajax-left {
    margin-right: 10px;
	}
	.ajax-success-modal .ajax-right {
    margin-left: 80px;
   }
   .ajax-success-modal .continue-shopping {
    margin-right: 5px;
    }
   .ajax-success-modal .btn-secondary {
    font-size: 9px!important;
    height: 34px;
    line-height: 30px;
    padding: 0 8px;
   }
   .ajax-success-modal .close-modal {
    right: 5px;
    top: -40px;
   }
   .success-message{
	   font-size:10px;
   }
}

</style>
<div class="ajax-success-modal cart_modal" style="/* display: none; */">
  <div class="overlay"></div>
  <div class="content">
      <div class="ajax-left">
        <img class="ajax-product-image" alt="&nbsp;" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product_image; ?>" style="max-width:65px; max-height:100px">
      </div>
      <div class="ajax-right">
        <p class="ajax-product-title">
              
              <span>{{$product_name}}</span>              
              
            </p>
        <p class="success-message btn-go-to-cart">is added to your shopping cart.</p>
        <p class="success-message btn-go-to-wishlist" style=" display: none;">is added to your wish list.</p>        
        <div class="actions">
          <button class="btn-secondary continue-shopping" onclick="javascript:void(0)" style="border:2px solid #291d88 !important;border-color: #291d88 !important;width: auto !important;">Continue Shopping</button>
          <button class="btn-secondary btn-go-to-cart" onclick="window.location='https://pride-limited.com/checkout'" style="border:2px solid #291d88 !important;border-color: #291d88 !important;width: auto !important;">Checkout</button>
          <button class="btn-secondary btn-go-to-wishlist" onclick="window.location='/pages/wish-list'" style="display: none;">Go To Wishlist</button>
        </div>
      </div>
    <a href="javascript:void(0)"  class="close-modal">Close</a>
  </div>    
</div>

