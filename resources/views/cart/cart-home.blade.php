<div id="minicart-content-wrapper">
    <div class="block-title">
        <strong>
            <span class="text">My Cart</span>
            <span class="qty"><?php echo Cart::instance('products')->count(); ?></span>
        </strong>
    </div>
    <div class="block-content">
        <button type="button" id="btn-minicart-close" class="action close" title="Close">
            <img class="cart-actions-close" src="{{url('/')}}/storage/app/public/close.png" />
            <span>Close</span>
        </button>
        <?php if (Cart::count() == 0) { ?>
            <strong class="empty">Sorry! your shopping cart is empty!</strong>
        <?php } else { ?>
            <a class="action viewcart"  href="{{url('/shop-cart')}}">
                <span class="minicart-label">Cart </span>
            </a>
            <div class="items-total">
                <span class="count"><?php echo Cart::instance('products')->count(); ?></span>
                <span>Item:</span>
            </div>
            <strong class="subtitle">Recently added item(s)</strong>
            <div data-action="scroll" class="minicart-items-wrapper" style="height: 95px;">
                <?php
                $i = 0;
                foreach (Cart::instance('products')->content() as $row) :
                    $i++;
                    $name = $row->name;
                    $pro_name = str_replace(' ', '-', $name);
                    $product_url = strtolower($pro_name);
                    $color = $row->options->color;
                    $color_album = str_replace('/', '-', $color);
                    ?>
                    <ol id="mini-cart" class="minicart-items" role="tablist">
                        <li class="item product product-item odd last" data-role="product-item" data-collapsible="true" role="tab" aria-selected="false" aria-expanded="false" tabindex="0">
                            <div class="product">
                                <a  tabindex="-1" target="_blank" class="product-item-photo" href="{{url("shop/{$product_url}/color-{$color_album}/{$row->id}")}}" title="<?php echo $row->name; ?>">
                                    <img class="photo image" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo ($row->options->has('product_image') ? $row->options->product_image : ''); ?>" alt="<?php echo $row->name; ?>" style="width: 85px; height: 120px;">
                                </a>
                                <div class="product-item-details">
                                    <strong class="product-item-name">
                                        <a target="_blank" href="{{url("shop/{$product_url}/color-{$color_album}/{$row->id}")}}" title="<?php echo $row->name; ?>"><?php echo $row->name; ?></a>
                                    </strong>
                                    <div data-role="content" class="content">
                                        <strong class="subtitle"><span>Options Details</span></strong>
                                        <dl class="product options list">
                                            <dt class="label" style="font-weight: 700;">Size</dt>
                                            <dd class="values">
                                                <span><?php echo ($row->options->has('size') ? $row->options->size : ''); ?></span>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="product-item-pricing">

                                        <div class="price-container">
                                            <span class="price-wrapper">
                                                <span class="price-excluding-tax">
                                                    <span class="minicart-price">
                                                        <span class="price">Tk <?php echo $row->price * $row->qty; ?></span>        
                                                    </span>
                                                </span>
                                            </span>
                                        </div>

                                        <!--<div class="details-qty qty">-->
                                        <!--    <label class="label">Qty</label>-->
                                        <!--    <span class="mini-qty-minus">-</span>-->
                                        <!--    <input type="number" size="4" class="item-qty cart-item-qty" id="cart-item-7845891-qty" value="<?php echo $row->qty; ?>"  readonly="readonly">-->
                                        <!--    <span class="mini-qty-plus" >+</span>-->
                                        <!--    <button class="update-cart-item" style="display: none" id="update-cart-item-7845891" data-cart-item="7845891" title="Update">-->
                                        <!--        <span data-bind="i18n: 'Update'">Update</span>-->
                                        <!--    </button>-->
                                        <!--</div>-->
                                    </div>
                                    <!--<div class="product actions">-->
                                    <!--    <div class="secondary">-->
                                    <!--        <a href="{{url('/cart/delete')}}/<?php echo $row->rowId; ?>"  class="action delete ik-shopcart-product-close" data-cart-item="<?php echo $row->rowId; ?>" title="Remove item">-->
                                    <!--            <img class="product-actions-delete" src="{{url('/')}}/storage/app/public/delete-icon.svg" />-->
                                    <!--            <span>Remove</span>-->
                                    <!--        </a>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                </div>
                            </div>
                        </li>
                    </ol>
                <?php endforeach; ?>
            </div>
            <div class="subtotal mincartpoup">
                <span class="label">Total </span>
                <span class="total-price grand-total" data-bind="html: getCartParam('grand_total')"><span class="price">Tk <?php echo Cart::subtotal(); ?></span></span>
            </div>
        <?php } ?>
    </div>
     <?php if (Cart::count() != 0) { ?>
    <div class="actions">
        <div class="view-cart">
            <button id="top-cart-btn-cart" type="button" class="action primary checkout" data-action="close" title="Shopping Cart">View Cart</button>
        </div>
        <div class="primary">
            <button id="top-cart-btn-checkout" type="button" class="action primary checkout" data-action="close" title="Checkout">Checkout</button>
        </div>
    </div>
     <?php } ?>
</div>