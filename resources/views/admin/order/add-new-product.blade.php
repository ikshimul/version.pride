<style>
.searchsuite-autocomplete {
		background-color: #fff;
		left: 12px;
		margin-top: 2px;
		width: 230px;
		position: absolute;
		z-index: 9999999;
		-webkit-box-shadow: 0 2px 0 0 rgb(127 127 127 / 10%);
		box-shadow: 0 2px 0 0 rgb(127 127 127 / 10%);
	}
	.searchsuite-autocomplete .products ul {
		background: #f1f1f1;
		padding: 10px;
		height: 333px;
		overflow: auto;
	}
	.searchsuite-autocomplete ul li {
		color: #000;
		cursor: unset;
		padding: 5px 0;
		font-weight: 600;
		font-size: 12px;
	}
	.searchsuite-autocomplete .products ul li {
		border-bottom: 1px solid #d4d2d2;
	}
	.searchsuite-autocomplete ul li .qs-option-image {
		max-width: 82px;
		width: 30%;
		margin: 0 auto 10px;
	}
	.searchsuite-autocomplete ul li .qs-option-info {
		color: #000;
		padding: 0 0 5px;
		text-align: center;
	}
	.modal-body{
	    min-height:500px;
	}
	.loading-modal {
        display:    none;
        position:   fixed;
        z-index:    1000;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, .8 ) 
                    url("{{url('/')}}/storage/app/public/pride-loading-gif_new.gif") 
                    50% 50% 
                    no-repeat;
    }
</style>
<div class="modal fade" id="add-new-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">{{$order->conforder_tracknumber}}</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="loading-modal"></div>
            <div class="callout callout-success" style="display:none;">
              <h5><i class="fas fa-info"></i> Note:</h5>
              <span id="success-message"></span>
            </div>
            <div class="callout callout-danger" style="display:none;">
              <h5><i class="fas fa-info"></i> Note:</h5>
              <span id="error-message"></span>
            </div>
          <div class="form-search">
              <div class="input-group input-group-sm">
                  <input type="hidden" value="{{$shoppingcart_id}}" name="shoppingcart_id" id="shoppingcart_id" class="form-control">
                  <input type="search" value="" name="question" id="question" autocomplete="off" placeholder="Search product here" class="form-control">
                  <span class="input-group-append">
                    <button type="button" class="btn btn-outline-secondary bg-white border-start-0 border ms-n3">
                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="14" height="14" data-icon="magnifying-glass" class="iconic-magnifying-glass iconic-size-sm injected-svg iconic" viewBox="0 0 16 16" data-src="/iconic/svg/magnifying-glass-sm.svg">
        				  <g class="iconic-metadata">
        					<title>Magnifying Glass</title>
        				  </g>
        				  <g class="iconic-container" data-width="16" data-height="16">
        					<path stroke="#000" stroke-width="3" stroke-linecap="round" class="iconic-magnifying-glass-handle iconic-property-stroke" fill="none" d="M11 11l3.5 3.5"></path>
        					<circle stroke="#000" stroke-width="2" cx="6.5" cy="6.5" r="5.5" class="iconic-magnifying-glass-rim iconic-property-stroke" fill="none"></circle>
        				  </g>
        				</svg>
                    </button>
                  </span>
              </div>
		  </div>
		  <div id="searchsuite_autocomplete" class="searchsuite-autocomplete" data-bind="visible: showPopup()" style="display: block;"></div>
		   <div class="product-details"></div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="{{ asset('assets/js/exchange.js') }}?5"></script>
<script>
$(document).ready(function () {
	$("#question").keyup(function () {
		var question = $("#question").val();
		//  alert(question);
		if (question == '') {
			$("#searchsuite_autocomplete").css("display", "none");
			$('.product-details').css("display", "none");
		} else {
			question = $("#question").val();
			var url = base_url + "/admin/order/exchange/search/" + question;
			$.ajax({
				url: url,
				type: 'GET',
				success: function (data) {
					//  alert(data);
					$("#searchsuite_autocomplete").css("display", "block");
					$('.searchsuite-autocomplete').html(data);
					$(".get_name").click(function(){
					    event.preventDefault();
					    $(".loading-modal").show();
						stylecode = $(this).attr('data-value');
						var question = $("#question").val('');
						var question = $("#question").val(stylecode);
						var shoppingcart_id = $("#shoppingcart_id").val();
						//alert(stylecode);
						$("#searchsuite_autocomplete").css("display", "none");
						var product = base_url + "/admin/order/exchange/search-product-details/" + stylecode+ "/"+shoppingcart_id;
						$.ajax({
							url: product,
							type: 'GET',
							success: function (details) {
							    $("#searchsuite_autocomplete").css("display", "none");
							    $(".loading-modal").hide();
								$(".product-details").css("display", "block");
								$('.product-details').html(details);
							}
						});
					});
				}
			});
		}
    });
});
function AddNew(){
    event.preventDefault();
	$("#loading-modal").show();
	var datastring = $("#add-new-product-invoice").serialize();
	//alert(datastring);
	var url = base_url + "/admin/order/add/new/product/save";
	$.ajax({
		url:url,
		type:'POST',
		data:datastring,
		success:function(result){
		    if(result==1){
		        $('.product-details').css("display", "none");
		        $(".callout-danger").hide();
		        $(".callout-success").show();
		        $("#success-message").html("New product added done.");
		        alert('After 5s page will auto reload.');
                setTimeout(function() {
                    location.reload();
                }, 5000);
		    }else if(result==0){
		        $(".callout-success").hide();
		        $(".callout-danger").show();
		        $("#error-message").html("Stock not available for this size.");
		    }else{
		        $(".callout-success").hide();
		        $(".callout-danger").show();
		        $("#error-message").html("Server Error.");
		    }
		}
	});
}
</script>