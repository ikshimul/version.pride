@extends('admin.layouts.app')
@section('title', 'Pride Limited | Incomplete Order')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Incomplete Order(s)</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item">Order</li>
            <li class="breadcrumb-item active">Incomplete</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Incomplete Order(s)</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        <div class="box-body">
            <div class="row">
                <div class="col-12">
                @if (session('update'))
                    <div class="callout callout-success">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('update') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="callout callout-danger">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('error') }}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="color:black;">SL</th>
                                    <th style="color:black;">Customer Name</th>
                                    <th style="color:black;">Order NO#</th>   
                                    <th style="color:black;">Order Placed</th> 
                                    <th>Pos Entry</th>
                                    <th style="color:black;">Delivery Date</th>  
                                    <th style="color:black;">Delivery By</th>  
                                    <th style="color:black;">Payment Method</th>
                                    <th style="color:black;">Order Status</th>  
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                ?>
                                @foreach($orders as $orderinfo)
                                <tr>                        	
                                    <td style="color:black" width="5%">{{$i++}}</td>
                                    <td style="color:black">{{$orderinfo->Shipping_txtfirstname}} {{$orderinfo->Shipping_txtlastname}}</td>
                                    <td style="color:black">
                                        <a href='{{url("admin/order/details?id={$orderinfo->id}")}}' target="__self" class="info">{{$orderinfo->conforder_tracknumber}}</a>
                                    </td>
                                    <td style="color:black">
                                       <?php 
                                                $order_date=strtotime($orderinfo->created_at); 
                                                echo date('d M , Y h:s a',$order_date);
                                            ?>
                                    </td>
                                    <td>
                                         <?php if($orderinfo->pos_entry_date !=''){
                                                $order_date=strtotime($orderinfo->pos_entry_date); 
                                                echo date('d M , Y',$order_date);
                                         }
                                            ?>
                                    </td>
                                    <td style="color:black">
                                      <?php 
                                         if($orderinfo->conforder_deliverydate !=null){
                                                $order_date=strtotime($orderinfo->conforder_deliverydate); 
                                                echo date('d M , Y',$order_date);
                                         }else{
                                             
                                         }
                                            ?>
                                    </td>
                                    <td style="color:black">{{$orderinfo->delivery_by}}</td>
                                    <td style="color:black"><?php echo $orderinfo->payment_method;?></td>
                                    <td>
                                         <?php if ($orderinfo->conforder_status == "Order_Verification_Pending") { ?>
                                            <span class="badge badge-info"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Pending_Dispatch") {
                                            ?>
                                            <span class="badge badge-primary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Dispatched") {
                                            ?>
                                            <span class="badge badge-secondary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Invalidate" || $orderinfo->conforder_status == "Cancelled") {
                                            ?>
                                            <span class="badge badge-danger"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                        <?php } elseif ($orderinfo->conforder_status == "Bkash_Payment_Receive") { ?>
                                            <span class="badge badge-secondary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                        <?php }elseif($orderinfo->conforder_status == "Closed"){ ?>
                                            <span class="badge badge-success"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                        <?php } else { ?>
                                            <span class="badge badge-primary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                        <?php } ?>
                                    </td>
                                     <td> <a href='{{url("admin/order/invoice?id={$orderinfo->id}")}}' target="_blank" class="btn btn-default btn-sm"><i class="fa fa-fw fa-print"></i> Print Invoice</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection


