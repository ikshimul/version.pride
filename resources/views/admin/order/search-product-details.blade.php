<?php

use App\Http\Controllers\user\ExchangeController;
?>
<link  href="{{ asset('assets/css/order_details.css') }}?9" rel="stylesheet">
<style>
button, input, select, textarea {
    appearance: listbox;
    -webkit-appearance: listbox;
    -moz-appearance: listbox;
    -ms-expand: none;
    -o-expand: none;
}
#loading-image {
    position:fixed;
    left:0;
    top:0;
    width:100vw;
    height:100vh;
    background:#333;
    opacity:0.8;
    margin:0;
    z-index:999;
}
#loading-image img {
    position:absolute;
    left:50%;
    top:50%;
    transform:translate(-50%, -50%);
    opacity:1;
}
@-webkit-keyframes blinker {
    from {opacity: 1.0;}
    to {opacity: 0.0;}
}
.blink{
    color: green;
    font-weight:bold;
    text-decoration: blink;
    -webkit-animation-name: blinker;
    -webkit-animation-duration: 1.8s;
    -webkit-animation-iteration-count:infinite;
    -webkit-animation-timing-function:ease-in-out;
    -webkit-animation-direction: alternate;
}
</style>
<?php $i=1; ?>
<div style="display:none;" id="loading-image"><img src="{{url('/')}}/storage/app/public/loader.gif" Alt="Loading..." /></div>
<div class="mi-orderdetails" id="exchangecontent_{{$order_id}}_{{$i}}">
    <div class="mi-orderdetails-body">
        <div class="mi-orderdetails-products">
			<div class="row">
				<div class="col-md-12 col-xs-12">
				    <div id="exchangemessage_{{$order_id}}_{{$i}}"></div>
					<div class="mi-orderdetails-product">
						<div class="mi-orderdetails-product-image" id="content_img_{{$order_id}}_{{$i}}">
							<img src="{{url('/')}}/storage/app/public/pgallery/{{$product_info->product_img_thm}}" width="100%" alt="product image">
						</div>
						<div class="mi-orderdetails-product-content" id="content_edit_{{$order_id}}_{{$i}}">
						  <form action="{{url('/admin/order/add/new/product')}}" method="post" id="add-new-product-invoice">
						    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
							<h5 class="mi-orderdetails-product-title"><a href="#">{{$product_info->product_name}}</a></h5>
							<strong>Price: </strong>{{$product_info->original_price}} Tk
							<br>
							<span class="mi-orderdetails-product-edit">Color: 
							   <?php $Colorlist = ExchangeController::GetColorListByProductId($product_info->id);
							   ?>
                                <select class="form-control" name="new_productalbum_name" id="exchangeColorId_{{$order_id}}_{{$i}}" onchange="ExchangeProductcolor(<?php echo $product_info->id;?>,<?php echo $order_id;?>,<?php echo $i;?>)">
                                    <?php
                                    foreach ($Colorlist as $color) {
										
                                        ?>
                                        <option value="<?php echo $color->productalbum_name; ?>">
										<?php echo $color->productalbum_name; ?></option>
                                                <?php
                                            }
										$productalbum_name=$color->productalbum_name;
                                            ?>										 
                                </select>
							</span>
							<span class="mi-orderdetails-product-edit">Size : 
							   <select class="form-control" name="new_prosize_name" id="exchangeSizeId_{{$order_id}}_{{$i}}" onchange="ExchangeProductsize(<?php echo $product_info->id;?>,<?php echo $order_id;?>,<?php echo $i;?>)">
                                    <?php
                                    $Sizelist = ExchangeController::GetSizeListByProductId($product_info->id, $productalbum_name);
                                    foreach ($Sizelist as $Size) {
                                        $prosize_name=$Size->productsize_size;
                                        ?>
                                        <option value="<?php echo $Size->productsize_size; ?>"><?php echo $Size->productsize_size; ?></option>
                                                <?php
                                       }
										
                                    ?>
                                </select>
							</span>
							<span class="mi-orderdetails-product-edit">Quantity : 
							<?php
							$Qtys = ExchangeController::GetQtyProductId($product_info->id, $productalbum_name, $prosize_name);
							  //echo  $product_info->id.' '.$productalbum_name.' '.$prosize_name;
                              //  echo $Qtys;
                             ?>
								<select class="form-control" name="new_shoppinproduct_quantity" id="exchangeQtyId_{{$order_id}}_{{$i}}">
                                    <?php
                                    $Qtys = ExchangeController::GetQtyProductId($product_info->id, $productalbum_name, $prosize_name);
                                    echo $Qtys;
                                    for ($q = 1; $q <= $Qtys + 1; $q++) {
                                        ?>
                                        <option value="<?php echo $q; ?>"><?php echo $q; ?></option>
                                    <?php } ?>
                                </select>
							</span>
							<input type="hidden" name="product_name"  id="exchangeproduct_name_<?php echo $i; ?>" value="{{$product_info->product_name}}">
							<input type="hidden" name="product_id"  id="exchangeproduct_id_<?php echo $i; ?>" value="{{$product_info->id}}">
							<input type="hidden" name="product_price"  id="product_price_<?php echo $i; ?>" value="{{$product_info->original_price}}">
							<input type="hidden" name="shoppingcart_id"  id="shoppingcart_id_<?php echo $i; ?>" value="{{$shoppingcart_id}}">
							<a type="submit" class="btn btn-primary btn-sm custom-btn-edit" href="#" id="exchangeupdate_{{$order_id}}_{{$i}}" onclick="AddNew(<?php echo $order_id;?>,<?php echo $i;?>)">Add New</a>
							<a class="btn btn-danger btn-sm custom-btn-edit" href="#" id="exchangeexit_{{$order_id}}_{{$i}}" onclick="exchangeexit(<?php echo $order_id;?>,<?php echo $i;?>)">Exit</a>
						 </form>
						</div>
					</div>
				</div>
 			</div>
        </div>
    </div>
</div>