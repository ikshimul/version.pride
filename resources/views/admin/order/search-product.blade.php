<?php
use App\Http\Controllers\product\ProductController;
$count = count($search);
if ($count > 0) {
    ?>
    <div class="products" data-bind="visible: result.products.data().length > 0" style="">
        <div class="title">
            <span>Search products list</span>
        </div>
        <ul id="products" role="listbox" data-bind="foreach: result.products.data">
            <?php
            foreach ($search as $product) {
                $pro_name = str_replace(' ', '-', $product->product_name);
				$cate = str_replace(' ', '-', $product->name);
				$main_cate = strtolower($cate);
				$checksub = $product->cat;
                ?>
				<li>
					<div class="qs-option-image">
						<a target="_blank" href='{{url("/product/{$main_cate}/{$pro_name}/{$product->productalbum_name}/{$product->id}")}}' target="_blank" title="{{$product->product_name}}">
						<img src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img; ?>" width="100%" title="{{$product->product_name}}">
						</a>
					</div>
					<div class="qs-option-info">
						<div class="qs-option-title">
							<a href="#" class="get_name" data-value="<?php echo $product->product_styleref; ?>" title="{{$product->product_name}}">{{$product->product_name}}</a>
						</div>
						<div><span>Code</span>: <span>
						    <?php echo $product->product_styleref; ?>
							<input type="hidden" value="<?php echo $product->product_styleref; ?>" id="stylecode"/>
						</span>
						</div>
						<div class="qs-option-price">
							<div class="price-box price-final_price">
								<span class="price-container price-final_price tax weee">
									<span id="product-price-{{$product->product_price}}" data-price-amount="{{$product->product_price}}" class="price-wrapper ">
									   <span class="price">Tk {{$product->product_price}}</span>
									</span>
								</span>
							</div>
						</div>
					</div>
				</li>
            <?php } ?>
        </ul>
    </div>
<?php } else { ?>
    <div class="no-result" data-bind="visible: !anyResultCount()" style="display: block;color:red;"><!-- ko i18n: 'No Result'--><span>No Result</span><!-- /ko --></div>
<?php } ?>


