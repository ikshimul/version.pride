<?php

use App\Http\Controllers\admin\ManageOrder;
use App\Http\Controllers\user\ExchangeController;
?>
@extends('admin.layouts.app')
@section('title', $shipping_address_details->conforder_tracknumber)
@section('content')
<?php
$subtotal = 0;
$update_subtotal = 0;
$totalprice = 0;
$shipping_area = '';
$totalamount = 0;
$shoppingcartid = 0;
$shipping_charge = $shipping_address_details->shipping_charge;
$extra_amount = 0;
?>
<style>
    .lead {
        font-size: 14px;
        font-weight: 700;
    }
    .table > tbody > tr > td {
        vertical-align: middle;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Order</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item">Order</li>
                    <li class="breadcrumb-item active">Details</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if (session('update'))
                <div class="callout callout-info">
                    <h5><i class="fas fa-info"></i> Note:</h5>
                    {{ session('update') }}
                </div>
                @endif
                <!-- Main content -->
                <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                <img src="{{ URL::to('') }}/assets_admin/images/logo.png" alt="User Avatar" class="img-size-50 mr-3 img-circle"> Pride Limited
                                <small class="float-right">
                                    Date: <?php
                                    date_default_timezone_set("Asia/Dhaka");
                                    echo date("M  d, Y h:i:sa");
                                    ?>
                                </small>
                            </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info pb-3">
                        <div class="col-sm-4 invoice-col">
                            Shipping Address
                            <address id='address-text' class="d-block">
                                <strong>{{ $shipping_address_details->Shipping_txtfirstname }} {{$shipping_address_details->Shipping_txtlastname }}</strong><br>
                                <?php
                                if ($shipping_address_details->Shipping_txtaddressname == '') {
                                    echo $shipping_address_details->Shipping_txtaddress1;
                                } else {
                                    echo $shipping_address_details->Shipping_txtaddressname;
                                }
                                ?><br>
                                {{ $shipping_address_details->name }} ({{ $shipping_address_details->region_name }}) </span>-<span id='text-zip'>{{ $shipping_address_details->Shipping_txtzipcode }}</span><br>
                                Phone: {{ $shipping_address_details->Shipping_txtphone }}<br>
                                Email: {{ $shipping_address_details->email }}<br>
                                Loyalty Card Number: {{$card_number}}
                            </address>
                            <address id ='address-field' class="d-none">
                                <input type="hidden" value="{{ $shipping_address_details->ordershipping_id }}" id="address-ordershipping-id">
                                <input type="hidden" value="{{ $shipping_address_details->city_id }}" id="address-destination-id">
                                <div class="form-group">
                                    <label for="address-fname">First name: </label>
                                    <input type="text" value="{{ $shipping_address_details->Shipping_txtfirstname }}" class="form-control" id="address-fname">
                                </div>
                                <div class="form-group">
                                    <label for="address-lname">Last name: </label>
                                    <input type="text" value="{{$shipping_address_details->Shipping_txtlastname }}" class="form-control" id="address-lname">
                                </div>
                                <div class="form-group">
                                    <label for="address-address">Address: </label>
                                    <textarea rows="4" class="form-control" id="address-address">{{{ $shipping_address_details->Shipping_txtaddressname }}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="address-city">City: </label>
                                    <select name="route_region" class="form-control select2" id="route-region">
                                        <option value="1" selected>Dhaka</option> 
                                        <option value="2" selected>Chittagong</option>
                                        <option value="3" selected>Barisal</option>
                                        <option value="4" selected>Khulna</option> 
                                        <option value="5" selected>Mymensingh</option>
                                        <option value="6" selected>Rajshahi</option>
                                        <option value="7" selected>Rangpur</option> 
                                        <option value="8" selected>Sylhet</option>
                                        <option value="" selected>&nbsp;--- Select City ---&nbsp;&nbsp;</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="address-city">Area: </label>
                                    <select name="city" class="form-control select2" id="address-city">
                                        <option value="">--Select--</option>
                                    </select>
                                <!--<input type="text" value="{{ $shipping_address_details->name }}" class="form-control" id="address-city">-->
                                </div>

                                <div class="form-group">
                                    <label for="address-zip">ZIP code: </label>
                                    <input type="number" value="{{ $shipping_address_details->Shipping_txtzipcode }}" class="form-control" id="address-zip">
                                </div>
                                <div class="form-group">
                                    <label for="address-phone">Phone: </label>
                                    <input type="tel" value="{{ $shipping_address_details->Shipping_txtphone }}" class="form-control" id="address-phone">
                                </div>
                            </address>
                            <?php if ($shipping_address_details->conforder_status == 'Order_Verification_Pending' || $shipping_address_details->conforder_status == 'Pending_Dispatch') { ?>
                                <button id="edit-address" type="button" class="btn btn-default btn-xs pull-left show">
                                    <i class="fas fa-edit"></i> Edit Address
                                </button>
                            <?php } ?>
                            <button id="save-address" type="button" class="btn btn-default btn-xs pull-left d-none">
                                <i class="fas fa-save"></i> Save Address
                            </button>
                            <br>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Shipping Destination
                            <address id="route-text">
                                <p id="route-text-default"><strong><?php echo $shipping_address_details->name; ?></strong></p>
                                 <?php if ($shipping_address_details->conforder_status == 'Order_Verification_Pending' || $shipping_address_details->conforder_status == 'Pending_Dispatch') { ?>
                                <button id="show-route-form" type="button" class="btn btn-default btn-xs pull-left">
                                    <i class="fas fa-edit"></i> Edit Route
                                </button>
                                <?php } ?>
                            </address>
                            <div class="d-none" id="route-form">
                                <div id="route-form">
                                    <div class="form-group">
                                        <label>City Name</label>
                                        <select name="shipping-region" class="form-control select2" id="shipping-region">
                                            <option value="1" selected>Dhaka</option> 
                                            <option value="2" selected>Chittagong</option>
                                            <option value="3" selected>Barisal</option>
                                            <option value="4" selected>Khulna</option> 
                                            <option value="5" selected>Mymensingh</option>
                                            <option value="6" selected>Rajshahi</option>
                                            <option value="7" selected>Rangpur</option> 
                                            <option value="8" selected>Sylhet</option>
                                            <option value="" selected>&nbsp;--- Select City ---&nbsp;&nbsp;</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Area (Route)</label>
                                        <select name="shipping-city" class="form-control select2" id="shipping-city">
                                            <option value="">--Select--</option>
                                        </select>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <button id="save-route-form" type="button" class="btn btn-default btn-xs pull-left">
                                        <i class="fas fa-save"></i> Save Route
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <b>{{ $shipping_address_details->conforder_tracknumber }}</b><br>
                            <br>
                            <b>Order ID:</b> {{ $shipping_address_details->conforder_tracknumber }}<br>
                            <b>3pl Status: </b>{{ $shipping_address_details->order_threepldlv }}<br>
                            <b>Order Status:</b> <?php echo $shipping_address_details->conforder_status; ?>
                            <br>
                            <b>Order Placed Date:</b> <?php
                            $place_date = strtotime($shipping_address_details->created_at);
                            echo date('d M, Y  h:i A', $place_date);
                            ?><br>
                            <b>Delivery Date:</b> <?php echo $shipping_address_details->conforder_deliverydate; ?>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    <!-- Table row -->
                    <div class="row">
                        <?php if ($exchange_requests->isNotEmpty()) { ?>
                        <div class="col-12" style="padding-bottom:30px;">
                            <div class="position-relative p-3" style="height: 280px;background-color: #291d8826;">
                              <div class="ribbon-wrapper ribbon-xl">
                                <div class="ribbon bg-primary">
                                  Exchnage Request
                                </div>
                              </div>
                              <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Style</th>
                                            <th>Color</th>
                                            <th>Size</th>
                                            <th>Qty</th>
                                            <th>Orig. Price</th>
                                            <th>Offer Price</th>
                                            <th>Total Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
    									    $extra_amount=0; $update_subtotal=0; $i=0; 
    									    foreach($exchange_requests as $exorderinfo){ $i++;
                                            $shopinfo = ExchangeController::GetPriceVariation($exorderinfo->shoppinproduct_id);
    										$previous_price=+$shopinfo->product_price;
    										$new_price=+$exorderinfo->product_price;
    										$extra_amount=+$new_price-$previous_price;
    									 ?>
    									 <tr>
    									    <?php
                                            $img_big = ManageOrder::GetProductImageBig($exorderinfo->product_id, $exorderinfo->productalbum_name);
                                            ?>
                                            <td>
                                                <a class="example-image-link" href="{{url('/')}}/storage/app/public/pgallery/{{$img_big->productimg_img}}" data-lightbox="example-1">
                                                    <img class="example-image" src="{{url('/')}}/storage/app/public/pgallery/{{$exorderinfo->cart_image}}" height="150" width="120" alt="image-1" />
                                                </a>
                                            </td>
                                            <td style="color:black">{{$exorderinfo->product_name}}</td>
                                            <td style="color:black">{{$exorderinfo->product_styleref}}</td>
                                            <td style="color:black">{{$exorderinfo->productalbum_name}}</td>
                                            <td>{{$exorderinfo->prosize_name}}</td>
                                            <td>{{$exorderinfo->shoppinproduct_quantity}}</td>
                                            <td>{{$exorderinfo->original_price}}</td>
                                            <td>{{$exorderinfo->product_price}}</td>
                                            <td>{{$exorderinfo->product_price*$exorderinfo->shoppinproduct_quantity}}</td>
                                            <td>
                                                <a onclick="return confirm('Are you sure you want to approved this exchange product?');" href='{{url("admin/order/user/request/exchange/approved?shoppinproduct_id={$exorderinfo->shoppinproduct_id}")}}' class="btn btn-sm btn-primary">Approved</a> |  
                                                <a onclick="return confirm('Are you sure you want to cancel this exchange product?');" href='{{url("admin/order/user/request/exchange/cancel?shoppinproduct_id={$exorderinfo->shoppinproduct_id}")}}' class="btn btn-sm btn-danger">Cancel</a>
                                            </td>
    									 </tr>
    									 <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-12 table-responsive" id="ViewTable">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Style</th>
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>Qty</th>
                                        <th>Orig. Price</th>
                                        <th>Offer Price</th>
                                        <th>Total Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $k = 1; ?>
                                    @foreach($order_products as $orderinfo)   						
                                    <tr>
                                        <?php
                                        $subtotal = $subtotal + ($orderinfo->shoppinproduct_quantity * $orderinfo->product_price);
                                        $img_big = ManageOrder::GetProductImageBig($orderinfo->product_id, $orderinfo->productalbum_name);
                                        ?>
                                        <td>
                                            <a class="example-image-link" href="{{url('/')}}/storage/app/public/pgallery/{{$img_big->productimg_img}}" data-lightbox="example-1">
                                                <img class="example-image" src="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}" height="150" width="120" alt="image-1" /></a>
                                        </td>
                                        <td style="color:black">{{$orderinfo->product_name}}</td>
                                        <td style="color:black">{{$orderinfo->product_styleref}}</td>
                                        <td style="color:black">{{$orderinfo->productalbum_name}}</td>
                                        <td>{{$orderinfo->prosize_name}}</td>
                                        <td>{{$orderinfo->shoppinproduct_quantity}}</td>
                                        <td>{{$orderinfo->original_price}}</td>
                                        <td>{{$orderinfo->product_price}}</td>
                                        <td>{{$orderinfo->product_price*$orderinfo->shoppinproduct_quantity}}</td>
                                        <td>
                                            <input type="hidden" name="product_id"  id="product_id_<?php echo $k; ?>" value="{{$orderinfo->product_id}}">
                                            <input type="hidden" name="shoppinproduct_id"  id="shoppinproduct_id_<?php echo $k; ?>" value="{{$orderinfo->shoppinproduct_id}}">
                                            <input type="hidden"  id="Color_<?php echo $k; ?>" value="{{$orderinfo->productalbum_name}}">
                                            <input type="hidden"  id="ProSize_<?php echo $k; ?>" value="{{$orderinfo->prosize_name}}">
                                            <input type="hidden"  id="ProQty_<?php echo $k; ?>" value="{{$orderinfo->shoppinproduct_quantity}}">
                                            <?php if ($shipping_address_details->conforder_status == 'Order_Verification_Pending' || $shipping_address_details->conforder_status == 'Pending_Dispatch') {
                                            if($orderinfo->exchange==1){ ?>
                                                <span class="badge badge-warning">Exchange Product</span>
                                            <?php }else{
                                            ?>
                                            
                                            <a href="#" id="btnexchange_<?php echo $k; ?>" class="btn btn-sm btn-primary exchange">Exchanged</a> |  
                                            <input type="submit" name="cancle" id="cancel_<?php echo $k; ?>" class="btn btn-sm btn-danger" value="Cancel"/>
                                            <?php } }else{ ?>
                                               <span class="badge badge-warning"><?php echo str_replace('_', ' ', $shipping_address_details->conforder_status); ?></span>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php $k++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                            <input type="hidden" name="total_loop" id="total_loop" value="<?php echo $k - 1; ?>">	
                            <?php if ($shipping_address_details->conforder_status == 'Order_Verification_Pending' || $shipping_address_details->conforder_status == 'Pending_Dispatch') { ?>
                                <a id="add-new" data="{{ $shipping_address_details->shoppingcart_id }}" type="button" class="btn btn-default btn-xs pull-left show">
                                    <i class="far fa-plus-square"></i> &nbsp;Add New Product
                                </a>
                            <?php } ?>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-12 table-responsive" id="EditTable" style="display:none;">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Style</th>
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $m = 1;
                                    ?>
                                    @foreach($order_products as $orderinfo)   
                                <form action="{{url('admin/order/exchange')}}" method="post">
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                    <tr style="display:none;" id="rowId_<?php echo $m; ?>">
                                        <?php
                                        $update_subtotal = $update_subtotal + ($orderinfo->shoppinproduct_quantity * $orderinfo->product_price);
                                        ?>

                                        <td style="color:black"  width="5%"><img src="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}" width="100"/></td>
                                        <td style="color:black">{{$orderinfo->product_name}}</td>
                                        <td style="color:black">{{$orderinfo->product_styleref}}</td>
                                        <td style="color:black">
                                            <?php $Colorlist = ManageOrder::GetColorListByProductId($orderinfo->product_id);
                                            ?>
                                            <select class="form-control" name="new_productalbum_name" id="ColorId_<?php echo $m; ?>">
                                                <?php
                                                foreach ($Colorlist as $color) {
                                                    ?>
                                                    <option value="<?php echo $color->productalbum_name; ?>"<?php
                                                    if ($color->productalbum_name == $orderinfo->productalbum_name) {
                                                        echo ' selected';
                                                    }
                                                    ?>><?php echo $color->productalbum_name; ?></option>
                                                            <?php
                                                        }
                                                        ?>										 
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control" name="new_prosize_name" id="SizeId_<?php echo $m; ?>">
                                                <?php
                                                $Sizelist = ManageOrder::GetSizeListByProductId($orderinfo->product_id, $orderinfo->productalbum_name);
                                                foreach ($Sizelist as $Size) {
                                                    ?>
                                                    <option value="<?php echo $Size->productsize_size; ?>"<?php
                                                    if ($Size->productsize_size == $orderinfo->prosize_name) {
                                                        echo ' selected';
                                                    }
                                                    ?>><?php echo $Size->productsize_size; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control" name="new_shoppinproduct_quantity" id="QtyId_<?php echo $m; ?>">
                                                <?php
                                                $Qtys = ManageOrder::GetQtyProductId($orderinfo->product_id, $orderinfo->productalbum_name, $orderinfo->prosize_name);
                                                for ($q = 1; $q <= $Qtys + 1; $q++) {
                                                    ?>
                                                    <option value="<?php echo $q; ?>" <?php
                                                    if ($q == $orderinfo->shoppinproduct_quantity) {
                                                        echo ' selected';
                                                    }
                                                    ?>><?php echo $q; ?></option>
                                                        <?php }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>{{$orderinfo->product_price}}</td>
                                        <td>
                                            <input type="hidden" name="product_id"  id="product_id_<?php echo $k; ?>" value="{{$orderinfo->product_id}}">
                                            <input type="hidden" name="shoppinproduct_id"  id="shoppinproduct_id_<?php echo $k; ?>" value="{{$orderinfo->shoppinproduct_id}}">
                                            <input type="hidden" name="productalbum_name"  id="Color_<?php echo $k; ?>" value="{{$orderinfo->productalbum_name}}">
                                            <input type="hidden" name="prosize_name"  id="ProSize_<?php echo $k; ?>" value="{{$orderinfo->prosize_name}}">
                                            <input type="hidden" name="shoppinproduct_quantity"  id="ProQty_<?php echo $k; ?>" value="{{$orderinfo->shoppinproduct_quantity}}">
                                            <input type="submit"  id="updatechange_<?php echo $m; ?>" class="btn-sm btn-primary exchange" value="Exchanged">
                                        </td>
                                    </tr>
                                </form>
                                <?php $m++; ?>								
                                @endforeach
                                </tbody>
                            </table>
                            <input type="hidden" name="total_loop" id="update_total_loop" value="<?php echo $m - 1; ?>">
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-6">
                            <span class="lead">Payment Methods:</span>
                            <span id="show-payment-mode">
                                <?php
                                if ($shipping_address_details->payment_method == 'cDelivery' || $shipping_address_details->payment_method == 'cs') {
                                    echo 'Cash on delivery';
                                    $payment_mode = 'COD';
                                } else if ($shipping_address_details->payment_method == 'iPay') {
                                    echo 'iPay';
                                    $payment_mode = 'MPAY';
                                } elseif ($shipping_address_details->payment_method == 'ssl') {
                                    echo 'SSL';
                                    $payment_mode = 'CCRD';
                                } else if ($shipping_address_details->payment_method == 'bk' || $shipping_address_details->payment_method == 'bKash') {
                                    echo 'bKash';
                                    $payment_mode = 'MPAY';
                                } else {
                                    $payment_mode = '';
                                    echo 'not found';
                                }
                                ?>
                            </span>
                            <span>
                                <?php
                                if ($shipping_address_details->payment_method == 'bKash') {
                                    if ($bkash) {
                                        echo '(<strong> trxID: ' . $bkash->trxID . ' </strong>)';
                                    }
                                } elseif ($shipping_address_details->payment_method == 'ssl') {
                                    if ($ssl) {
                                        echo '(<strong> Bank trxID : ' . $ssl->bank_tran_id . ' </strong>)';
                                    }
                                }
                                ?>
                            </span>
                            <br>
                            <?php if ($shipping_address_details->conforder_status == 'Order_Verification_Pending' || $shipping_address_details->conforder_status == 'Pending_Dispatch') { ?>
                                <a class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-payment-mode" href='#'>Change Payment Mode</a>
                            <?php } ?>
                            <div class="modal fade" id="modal-payment-mode">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Change Payment Mode</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                       <div class="form-group">
                                            <label for="sel1">Payment Method</label>
                                            <select class="form-control select2" name="change_payment_mode" id="change_payment_mode">
                                                <option value=''> -- Select payment mode -- </option>
                                                <option value='bKash'>bKash</option>
                                                <option value='ssl'>SSL</option>
                                                <option value='cDelivery'>Cash on Delivery</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary" id="save-payment-mode">Save changes</button>
                                    </div>
                                  </div>
                                  <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                              </div>
                              <!-- /.modal -->
                            <p >
                                <span class="lead">Requested Delivery Notes:</span>
                                <span class="lead"><?php echo $shipping_address_details->conforder_deliverynotes; ?></span>
                            </p>
                            <input type="hidden" name="payment_method" value="<?php echo $payment_mode; ?>"/>
                        </div>
                        <!-- /.col -->
                        <div class="col-6">
                            <p class="lead">Amount</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th style="width:50%">Subtotal:</th>
                                        <td>BDT {{ $subtotal }}</td>
                                    </tr>
                                    <tr>
                                        <th><?php
                                            if ($subtotal >= 3000) {
                                                echo "Shipping Charge";
                                            } else {
                                                if ($shipping_charge == 70 || $shipping_charge == 100) {
                                                    echo "Shipping Charge(Inside Dhaka): ";
                                                } else if ($shipping_charge == 120) {
                                                    echo "Shipping Charge(One Day Delivery): ";
                                                } else if ($shipping_charge == 130) {
                                                    echo "Shipping Charge(Same Day Delivery): ";
                                                } else {
                                                    echo "Shipping Charge(Outside Dhaka): ";
                                                }
                                            }
                                            ?></th>
                                        <td><?php echo 'BDT ' . $shipping_charge; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Extra Charge:</th>
                                        <td>
                                            <span id="extra-charge-text">BDT {{ $shipping_address_details->extra_charge}}</span>
                                            @if($shipping_address_details->extra_charge == 0)
                                             <a href="#">&nbsp;&nbsp;&nbsp;<i data-toggle="modal" data-target="#modal-default" class="fas fa-plus"></i></a>
                                            @else
                                             <a href="#"><i data-toggle="modal" data-target="#modal-default" class="fas fa-edit"></i></a>
                                            @endif
                                            <div class="modal fade" id="modal-default">
                                                <div class="modal-dialog">
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <h4 class="modal-title">Extra Charge</h4>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="sel1">Extra charge:</label>
                                                            <input type="number" value="{{ $shipping_address_details->extra_charge}}" class="form-control" id="extra-charge-amount">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      <button type="button" class="btn btn-primary" id="save-extra-charge-form">Save changes</button>
                                                    </div>
                                                  </div>
                                                  <!-- /.modal-content -->
                                                </div>
                                            <!-- /.modal-dialog -->
                                          </div>
                                          <!-- /.modal -->
                                        </td>
                                    </tr>
                                    <?php if ($shipping_address_details->voucher_id == 1) { ?>
                                    <tr>
                                        <th>Discount : 
                                            <br><small>(10% discount for First Purchase)</small></th>
                                            <td><?php echo $discount = $subtotal * 10 / 100;?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <?php if ($shipping_address_details->voucher_id == 1) { ?>
                                            <th>Grand Total : </th>
                                            <?php
                                            if (isset($shipping_address_details->extra_charge)) {
                                                $extra_amount = $shipping_address_details->extra_charge;
                                            } else {
                                                $extra_amount = 0;
                                            }
                                            ?>
                                            <td><span>BDT <?php
                                                    $totalamount = $subtotal + $shipping_charge;
                                                    $discount = $subtotal * 10 / 100;
                                                    echo $totalamount = $totalamount - $discount + $extra_amount;
                                                    ?></span>
                                            </td>
                                        <?php } else { ?>
                                            <th>Grand Total:</th>
                                            <td><span id="end_total">BDT <?php echo $totalamount = $subtotal + $shipping_charge + $shipping_address_details->extra_charge; ?></span></td>
                                        <?php } ?>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                    
                    <div class="row" <?php if($shipping_address_details->conforder_status =='Closed'){echo 'style="display:none;"';}?>>
                        <div class="col-3 col-xs-12">
                            <form name="singleform" action="{{url('admin/order/update-status')}}" method="POST">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                <input type="hidden" name="payment_method" value="<?php echo $payment_mode; ?>"/>
                                <input type="hidden" name="total_loop" id="update_total_loop" value="<?php echo $m - 1; ?>">
                                <input type="hidden" name="conforder_id" value="<?php echo $shipping_address_details->id; ?>" />
                                <input type="hidden" name="total_amount" value="<?php echo $totalamount; ?>" />
                                <div class="form-group">
                                    <label>Order Status</label>
                                    <div class="input-group">
                                        <?php $orderstatus = $shipping_address_details->conforder_status; ?>
                                        <select name="order_status" class="form-control select2" id="order_status">
                                            <option value="<?php echo $orderstatus ?>"><?php echo $orderstatus ?></option>
                                            <option value="Invalidate">Invalidate</option>
                                            <option value="Pending_Dispatch">Verified</option>
                                            <option value="Dispatched">Confirm Dispatch</option>
                                            <option value="Payment_Pending">Delivered</option>
                                            <option value="Closed">Payment Received</option>
                                            <option value="Bkash_Payment_Receive">Bkash Payment Received</option>
                                            <option value="Cancelled">Cancelled</option>
                                            <option value="Exchange_Pending">Exchange Pending</option>
                                            <option value="Exchange_Dispatched">Exchange Dispatched</option>
                                            <option value="Exchanged">Exchanged</option>
                                            <option value="Returned">Returned</option>
                                        </select>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group d-none" id="expenses_amount">
                                    <label>Expense Amount</label>
                                    <input type="number" class="form-control" name="expenses_amount" value="<?php if (isset($shipping_address_details->expense_amount)) echo $shipping_address_details->expense_amount; ?>"/>
                                </div>
                        </div>
                        <div class="col-5 col-xs-12">
                            <div class="row">
                                <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                    <label>3pl Status</label>
                                    <?php $threepldlvshow = $shipping_address_details->order_threepldlv; ?>
                                    <select name="ddlthreepldlv" id="TPL" class="form-control select2">
                                        <option value="<?php echo $threepldlvshow; ?>">
                                            <?php echo $threepldlvshow; ?>
                                        </option>
                                        <option value="eCourier">eCourier</option>
                                        <option value="Pathao">Pathao</option>
                                        <option value="Bidduyt">Bidduyt</option>
                                        <option value="Sundorbon">Sundorbon</option>
                                        <option value="Hand Delivery">Hand Delivery</option>
                                    </select> 
                                    <!--</div> -->
                                </div>
                                <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                    <label>Pos Entry Date</label>
                                    <input type="date" class="form-control" name="pos_entry_date" id="pos_entry_date" value="<?php echo isset($shipping_address_details->pos_entry_date) ? date('Y-m-d', strtotime($shipping_address_details->pos_entry_date)) : ''; ?>"/>
                                </div>
                                <div class="form-group col-md-4 col-sm-4 col-xs-12">
                                    <input type="hidden" name="tplname" id="TPLNAME" value="" />
                                    <label>Delivery date</label>
                                    <input type="date" class="form-control" name="delivery_date" id="delivery_date" value="<?php echo isset($shipping_address_details->conforder_deliverydate) ? date('Y-m-d', strtotime($shipping_address_details->conforder_deliverydate)) : ''; ?>"/>
                                </div>
                                <div class="form-group col-md-12 col-xs-12">
                                    <!--- delivery person --->
                                    <div class="form-group d-none" id="delivered_person">
                                        <label>Delivered by</label>
                                        <select name="delivered_by" class="form-control select2" id="delivered_by">
                                            <option value="">--Select--</option>
                                            @foreach($riders as $rider)
                                              <option value="{{ $rider->id }}" <?php if (isset($shipping_address_details->delivery_by) && ($rider->id == $shipping_address_details->delivery_by)) echo 'selected'; ?>>{{ $rider->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <!--- delivery person ---->
                                    <!-- Pathao delivery --->
                                    <div class="form-group d-none" id="pathao_city">
                                        <label>Pathao City</label>
                                        <select name="pathao_city_id" class="form-control select2" id="pathao_city_id">
                                            <option value="">--Select--</option>
                                            
                                        </select>
                                     </div>
                    				 <div class="form-group d-none" id="pathao_zone">
                                        <label>Pathao Zone</label>
                                        <select name="pathao_zone_id" class="form-control select2" id="pathao_zone_id">
                                            <option value="">--Select--</option>
                                        </select>
                                     </div>
                    				 <div class="form-group d-none" id="pathao_area">
                                        <label>Pathao Area</label>
                                        <select name="pathao_area_id" class="form-control select2" id="pathao_area_id">
                                            <option value="">--Select--</option>
                                        </select>
                                     </div>
                                      <!-- end Pathao delivery --->
                                    <!-- ecourier package list --->
                                    <div class="form-group col-md-12 col-xs-12 d-none" id="package_list">
                                        <label>Package List</label>
                                        <select name="package_code" class="form-control select2" id="package_code">
                                            <option value="">--Select--</option>
                                            @foreach($package_list as $package)
                                            <option value="{{ $package->package_code }}">{{ $package->package_name }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12 d-none" id="ecourier_city">
                                        <label>eCourier City</label>
                                        <select name="ecourier_city" class="form-control select2" id="ecourier_city_id">
                                            <option value="">--Select--</option>
                                            @foreach($ecourier_city as $city)
                                            <option value="{{ $city->value }}">{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12 d-none" id="ecourier_thana">
                                        <label>eCourier Thana</label>
                                        <select name="ecourier_thana" class="form-control select2" id="ecourier_thana_id">
                                            <option value="">--Select--</option>

                                        </select>
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12 d-none" id="ecourier_postcode">
                                        <label>eCourier Postcode</label>
                                        <select name="ecourier_postcode" class="form-control select2" id="ecourier_postcode_id">
                                            <option value="">--Select--</option>

                                        </select>
                                    </div>

                                    <div class="form-group col-md-12 col-xs-12 d-none" id="ecourier_area">
                                        <label>eCourier Area</label>
                                        <select name="ecourier_area" class="form-control select2" id="ecourier_area_id">
                                            <option value="">--Select--</option>

                                        </select>
                                    </div>
                                    <!-- ecourier package list --->
                                    <?php 
                                    if($three_pl)
                                    {
                                    if ($three_pl->delivey_by=='eCourier') { ?>
                                        <?php if ($shipping_address_details->conforder_status == 'Order_Verification_Pending' || $shipping_address_details->conforder_status == 'Pending_Dispatch') { ?>
                                            <a class="btn btn-default btn-sm" href='{{url("admin/order/cancel-ecourier/$three_pl->ecr_number")}}'>Cancel eCourier</a>
                                        <?php } ?>
                                    <?php } 
                                    } ?>
                                </div>   
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <div class="form-group">
                                <label>Any comment (Optional)</label>
                                <textarea name="orderstatusnote" class="form-control"><?php echo $shipping_address_details->conforder_statusdetails; ?></textarea><br />
                                <!-- /.input group -->
                            </div>
                            <!--<div class="form-group">-->
                            <!--    <label>Admin comment for this user (Optional)</label>-->
                            <!--    <textarea name="admin_comment" class="form-control">{{ $shipping_address_details->admin_comment or '' }}</textarea>-->
                            <!--    <input type="hidden" name="user_id" value="{{ $shipping_address_details->user_id or '' }}" />-->
                            <!--</div>-->
                        </div>
                        
                    </div>
                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-12">
                            <button type="submit"  class="btn btn-default" <?php if($shipping_address_details->conforder_status =='Closed'){echo 'style="display:none;"';}?>><i class="fas fa-edit"></i> Update Order</button>
                            <a href='{{url("admin/order/invoice?id={$shipping_address_details->id}")}}' target="_blank" class="btn btn-success float-right"><i class="fas fa-print"></i> Print
                                Order
                            </a>
                            <a href='{{url("admin/order/invoice/pdf?id={$shipping_address_details->id}")}}' class="btn btn-primary float-right" target="_blank" style="margin-right: 5px;">
                                <i class="fas fa-download"></i> Generate PDF
                            </a>
                        </div>
                    </div>
                    </form>
                </div>
                <!-- /.invoice -->
                <div id="add-new-view"></div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<?php
$conforder_id = $shipping_address_details->id;
?>
<script>
      <?php
      if ($three_pl === null) { ?>
          //alert('null');
      <?php }else{ ?>
          document.getElementById("package_code").value = "<?php echo $three_pl->package_code; ?>";
      <?php   }  ?>
      
    document.getElementById("route-region").value = "<?php echo $shipping_address_details->region_id; ?>";
    document.getElementById("shipping-region").value = "<?php echo $shipping_address_details->region_id; ?>";
    
    var confoorderid =<?php echo $conforder_id; ?>;
    $(document).ready(function () {
        var total_loop = $("#total_loop").val();
        var update_total_loop = $("#update_total_loop").val();
        for (var m = 1; m <= total_loop; m++) {
            $('#updatechange' + '_' + m).on('click', function () {
                var rownowithid = $(this).attr('id');
                var rowno = rownowithid.split('_');
                var field_no = rowno[1];
            });
        }

        for (var j = 1; j <= update_total_loop; j++) {
            $('#btnexchange' + '_' + j).on('click', function () {
                var rownowithid = $(this).attr('id');
                var rowno = rownowithid.split('_');
                var field_no = rowno[1];
                $("#ViewTable").hide();
                $("#EditTable").show();
                $("#rowId_" + field_no).show();
                //   alert(pre_Qty);
                rowidpass(field_no);
            });
        }
        function rowidpass(field_no) {
            $('#SizeId' + '_' + field_no).on('change', function () {
                var pre_color = $("#ColorId_" + field_no + " option:selected").val();
                var pre_Size_ = $("#SizeId_" + field_no + " option:selected").val();
                var pre_Qty = $("#QtyId_" + field_no + " option:selected").val();
                var product_id = $("#product_id_" + field_no).val();
                //  alert(product_id);
                getQtyByColorSize(product_id, pre_color, pre_Size_, field_no)
            });

            $('#ColorId' + '_' + field_no).on('change', function () {
                var pre_color = $("#ColorId_" + field_no + " option:selected").val();
                var product_id = $("#product_id_" + field_no).val();
                getSizeByColor(product_id, pre_color, field_no);
                //  alert(product_id)
            });
        }

        function  getQtyByColorSize(productid, ProColor, ProSize, field_no) {
            var url_op = base_url + "/ajaxcall-getQuantityByColor/" + productid + '/' + ProSize + '/' + ProColor;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (qty) {
                    var qty = qty;
                    if (qty >= 1) {
                        $('#updatechange_' + field_no).show();
                        $('#QtyId_' + field_no).find('option').remove();
                        for (var v = 1; v <= qty; v++) {
                            $('#QtyId_' + field_no).append('<option value="' + v + '">' + v + '</option>');
                        }
                    } else {
                        $('#updatechange_' + field_no).hide();
                        $('#QtyId_' + field_no).find('option').remove();
                        $('#QtyId_' + field_no).append('<option value="' + 0 + '">' + 0 + '</option>');
                    }
                }
            });
        }

        function getSizeByColor(product_id, color_name, field_no) {
            var url_op = base_url + "/getSizeByColor/" + product_id + '/' + color_name;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data)
                {
                    //  alert(data);
                    $("#SizeId_" + field_no).empty();
                    $("#SizeId_" + field_no).append('<option value="">Select Size</option>');
                    $.each(data, function (index, subcatobj) {
                        $("#SizeId_" + field_no).append('<option value="' + subcatobj.productsize_size + '">' + subcatobj.productsize_size + '</option>');
                    });
                }
            });
        }

        for (var i = 1; i <= total_loop; i++) {
            $('#cancel' + '_' + i).on('click', function () {
                var result = confirm("Are you sure to remove this product?");
                if (result) {
                var rownowithid = $(this).attr('id');
                var rowno = rownowithid.split('_');
                var field_no = rowno[1];
                var ProSize = $('#ProSize' + '_' + rowno[1]).val();
                var ProQty = $('#ProQty' + '_' + rowno[1]).val();
                var ProColor = $('#Color' + '_' + rowno[1]).val();
                var productid = $('#product_id' + '_' + rowno[1]).val();
                var shoppinproduct_id = $('#shoppinproduct_id' + '_' + rowno[1]).val();
                var url_op = base_url + "/admin/order/cancel_product/" + productid + "/" + ProColor + "/" + ProSize + "/" + ProQty + "/" + shoppinproduct_id;
                $.ajax({
                    url: url_op,
                    type: 'GET',
                    success: function (data) {
                        if (data == 1) {
                            location.reload();
                        } else {

                        }
                    }
                });
              }
            });

        }
        $('#add-new').on('click', function () {
            var shoppingcart_id = $(this).attr('data');
            var data = {
                shoppingcart_id: {{ $shipping_address_details->shoppingcart_id }}
            };
            $(".loading-modal").show();
            $.ajax({
                    url: base_url + "/admin/order/add/new/product/",
                    type: 'GET',
                    data: data,
                    success: function (data) {
                        $(".loading-modal").hide();
                        if (data != null) {
                            $("#add-new-view").html(data);
                            $('#add-new-modal').modal('show');
                        } else {

                        }
                    }
                });
                
            //alert(shoppingcart_id);
        });
       

      /*  $("#order_status").on('change', function () {
            var order_status = $(this).val();
           // var url_op = base_url + "/update_order_status/" + confoorderid + "/" + order_status;
            $.ajax({
                url: url_op,
                type: 'GET',
                success: function (data) {
               //     location.reload();
                }
            });
        });
        $("#TPL").on('change', function () {
            var threeplselectedid = $(this).val();
            // alert(threeplselectedid);
        //    var url = base_url + "/update_threepl_status/" + confoorderid + "/" + threeplselectedid;
            $.ajax({
                url: url,
                type: 'GET',
                success: function (data) {
                    //   alert(data);
                 //   location.reload();
                }
            });
        }); */
        $("#cancel").on("click", function () {
            var shoppinproduct_id = $("#shoppinproduct_id").val();
            alert(shoppinproduct_id);

        });
        var arr = [];
        $('#ColorId_1 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_2 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_3 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_4 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_5 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_6 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });
        var arr = [];
        $('#ColorId_7 option').each(function () {
            var val = $(this).text().trim();
            if (!arr.includes(val)) {
                arr.push(val)
            } else {
                $(this).hide();
            }

        });

    });
</script>

<script>
    /*
        Showing delivered_by dropdown
    */
    (function() {
        document.getElementById('TPL').onchange = showRiders;
        document.getElementById('order_status').onchange = expensesAmount;
        if(document.getElementById('TPL').value == 'Hand Delivery') {
            document.getElementById('delivered_person').classList.remove('d-none');
            document.getElementById('delivered_person').classList.add('d-block');
        }
        if(document.getElementById('order_status').value == 'Closed') {
            document.getElementById('expenses_amount').classList.remove('d-none');
            document.getElementById('expenses_amount').classList.add('d-block');
        }
        if(document.getElementById('TPL').value == 'eCourier') {
            document.getElementById('package_list').classList.remove('d-none');
            document.getElementById('package_list').classList.add('d-block');
        }
    })();
    function showRiders() {
        if(document.getElementById('TPL').value == 'Hand Delivery') {
            document.getElementById('delivered_person').classList.remove('d-none');
            document.getElementById('delivered_person').classList.add('d-block');
            document.getElementById('package_list').classList.remove('d-block');
            document.getElementById('package_list').classList.add('d-none');
            document.getElementById('ecourier_city').classList.remove('d-block');
            document.getElementById('ecourier_city').classList.add('d-none');
            document.getElementById('ecourier_thana').classList.remove('d-block');
            document.getElementById('ecourier_thana').classList.add('d-none');
            document.getElementById('ecourier_postcode').classList.remove('d-block');
            document.getElementById('ecourier_postcode').classList.add('d-none');
            document.getElementById('ecourier_area').classList.remove('d-block');
            document.getElementById('ecourier_area').classList.add('d-none');
            
        }else if(document.getElementById('TPL').value == 'eCourier'){
            document.getElementById('package_list').classList.remove('d-none');
            document.getElementById('package_list').classList.add('d-block');
            document.getElementById('ecourier_city').classList.remove('d-none');
            document.getElementById('ecourier_city').classList.add('d-block');
            document.getElementById('ecourier_thana').classList.remove('d-none');
            document.getElementById('ecourier_thana').classList.add('d-block');
            document.getElementById('ecourier_postcode').classList.remove('d-none');
            document.getElementById('ecourier_postcode').classList.add('d-block');
            document.getElementById('ecourier_area').classList.remove('d-none');
            document.getElementById('ecourier_area').classList.add('d-block');
            document.getElementById('delivered_person').classList.remove('d-block');
            document.getElementById('delivered_person').classList.add('d-none');
        }else if(document.getElementById('TPL').value =='Pathao'){
            document.getElementById('pathao_city').classList.remove('d-none');
			document.getElementById('pathao_city').classList.add('d-block');
			document.getElementById('pathao_zone').classList.remove('d-none');
			document.getElementById('pathao_zone').classList.add('d-block');
			document.getElementById('pathao_area').classList.remove('d-none');
			document.getElementById('pathao_area').classList.add('d-block');
			document.getElementById('package_list').classList.remove('d-block');
            document.getElementById('package_list').classList.add('d-none');
			document.getElementById('ecourier_city').classList.remove('d-block');
            document.getElementById('ecourier_city').classList.add('d-none');
            document.getElementById('ecourier_thana').classList.remove('d-block');
            document.getElementById('ecourier_thana').classList.add('d-none');
            document.getElementById('ecourier_postcode').classList.remove('d-block');
            document.getElementById('ecourier_postcode').classList.add('d-none');
            document.getElementById('ecourier_area').classList.remove('d-block');
            document.getElementById('ecourier_area').classList.add('d-none');
        } else {
            document.getElementById('delivered_person').classList.remove('d-block');
            document.getElementById('delivered_person').classList.add('d-none');
            document.getElementById('package_list').classList.remove('d-block');
            document.getElementById('package_list').classList.add('d-none');
            document.getElementById('ecourier_city').classList.remove('d-block');
            document.getElementById('ecourier_city').classList.add('d-none');
            document.getElementById('ecourier_thana').classList.remove('d-block');
            document.getElementById('ecourier_thana').classList.add('d-none');
            document.getElementById('ecourier_postcode').classList.remove('d-block');
            document.getElementById('ecourier_postcode').classList.add('d-none');
            document.getElementById('ecourier_area').classList.remove('d-block');
            document.getElementById('ecourier_area').classList.add('d-none');
        }
    }
    function expensesAmount() {
        if(document.getElementById('order_status').value == 'Closed') {
            document.getElementById('expenses_amount').classList.remove('d-none');
            document.getElementById('expenses_amount').classList.add('d-block');
        } else {
            document.getElementById('expenses_amount').classList.remove('d-block');
            document.getElementById('expenses_amount').classList.add('d-none');
        }
    }
</script>
<script>
    /*
        Showing delivery route dropdown
    */
    (function() {
        $('#show-route-form').click(function() {
            showRouteForm();
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
         $('#save-route-form').click(function() {
            var data = {
                ordershipping_id:$('#address-ordershipping-id').val(),
                route_city: $('#shipping-city').val(),
                shoppingcart_id: {{ $shipping_address_details->shoppingcart_id }},
            };
            $.ajax({
                type: 'POST',
                url: base_url+"/admin/order/update-city",
                data: data,
                success: function(d) {
                    var route_city_element = document.getElementById('shipping-city');
                    var selectedText = route_city_element.options[route_city_element.selectedIndex].text;
                    document.getElementById('route-text-default').innerHTML =selectedText;
                    hideRouteForm();
                    alert('Update success.After 5s page will auto reload.');
                    setTimeout(function() {
                        location.reload();
                    }, 5000);
                },
                error:function(e) {
                }
            });
        });
        function showRouteForm() {
            document.getElementById('route-text').classList.remove('d-block');
            document.getElementById('route-text').classList.add('d-none');
            document.getElementById('route-form').classList.remove('d-none');
            document.getElementById('route-form').classList.add('d-block');
        }
        function hideRouteForm() {
            document.getElementById('route-text').classList.remove('d-none');
            document.getElementById('route-text').classList.add('d-block');
            document.getElementById('route-form').classList.remove('d-block');
            document.getElementById('route-form').classList.add('d-none');
        }
    })();
</script>
<script>
    $(document).ready(function () {
        var RegionId=$("#route-region").val();
        var url_op = base_url + "/get-citylist/" + RegionId;
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (data) {
                $('#address-city').empty();
                $('#address-city').append('<option value="">--- Select City ---</option>');
                $.each(data, function (index, cityobj) {
                    $('#address-city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
                });
               document.getElementById("address-city").value = "<?php echo $shipping_address_details->city_id; ?>";
               $('#shipping-city').empty();
                $('#shipping-city').append('<option value="">--- Select City ---</option>');
                $.each(data, function (index, cityobj) {
                    $('#shipping-city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
                });
               document.getElementById("shipping-city").value = "<?php echo $shipping_address_details->city_id; ?>";
            }
        });
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $('select#route-region').on('change', function () {
            var RegionId = this.value;
            var url_op = base_url + "/get-citylist/" + RegionId;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    $('#address-city').empty();
                    $('#address-city').append('<option value="">--- Select City ---</option>');
                    $.each(data, function (index, cityobj) {
                        $('#address-city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
                    });
                }
            });
        });
        $('select#shipping-region').on('change', function () {
            var RegionId = this.value;
            var url_op = base_url + "/get-citylist/" + RegionId;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    $('#shipping-city').empty();
                    $('#shipping-city').append('<option value="">--- Select City ---</option>');
                    $.each(data, function (index, cityobj) {
                        $('#shipping-city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
                    });
                }
            });
        });
        var url_op = base_url + "/ajax/pathao/city-list";
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (data) {
                $('#pathao_city_id').empty();
                $('#pathao_city_id').append('<option value="">--- Select City ---</option>');
                $.each(data, function (index, cityobj) {
                    $('#pathao_city_id').append('<option value="' + cityobj.city_id + '">' + cityobj.city_name + '</option>');
                });
            }
        });
        $('#pathao_city_id').on('change', function () {
            var city_id = $("#pathao_city_id").val();
            var url_op = base_url + "/ajax/pathao/zone-list/" + city_id;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    $('#pathao_zone_id').empty();
                    $('#pathao_zone_id').append('<option value="">--- Select Zone ---</option>');
                    $.each(data, function (index, zoneobj) {
                        $('#pathao_zone_id').append('<option value="' + zoneobj.zone_id + '">' + zoneobj.zone_name + '</option>');
                    });
                }
            });
        });
        
        $('#pathao_zone_id').on('change', function () {
            var zone_id = $("#pathao_zone_id").val();
            var url_op = base_url + "/ajax/pathao/area-list/"+zone_id;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    $('#pathao_area_id').empty();
                    $('#pathao_area_id').append('<option value="">--- Select Area ---</option>');
                    $.each(data, function (index, areaobj) {
                        $('#pathao_area_id').append('<option value="' + areaobj.area_id + '">' + areaobj.area_name + '</option>');
                    });
                }
            });
        });
        $('#ecourier_city_id').on('change', function () {
            var city = $("#ecourier_city_id").val();
            var url_op = base_url + "/ajax/ecourier/thana-list/" + city;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    $('#ecourier_thana_id').empty();
                    $('#ecourier_thana_id').append('<option value="">--- Select thana ---</option>');
                    $.each(data, function (index, thanaobj) {
                        $('#ecourier_thana_id').append('<option value="' + thanaobj.value + '">' + thanaobj.name + '</option>');
                    });
                }
            });
        });
        
        $('#ecourier_thana_id').on('change', function () {
            var city = $("#ecourier_city_id").val();
            var thana = $("#ecourier_thana_id").val();
            var url_op = base_url + "/ajax/ecourier/postcode-list/" + city+'/'+thana;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    $('#ecourier_postcode_id').empty();
                    $('#ecourier_postcode_id').append('<option value="">--- Select postcode ---</option>');
                    $.each(data, function (index, thanaobj) {
                        $('#ecourier_postcode_id').append('<option value="' + thanaobj.value + '">' + thanaobj.name + '</option>');
                    });
                }
            });
        });
        
        $('#ecourier_postcode_id').on('change', function () {
            var postcode_id = $("#ecourier_postcode_id").val();
            var url_op = base_url + "/ajax/ecourier/area-list/" + postcode_id;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    $('#ecourier_area_id').empty();
                    $('#ecourier_area_id').append('<option value="">--- Select area ---</option>');
                    $.each(data, function (index, thanaobj) {
                        $('#ecourier_area_id').append('<option value="' + thanaobj.value + '">' + thanaobj.name + '</option>');
                    });
                }
            });
        });
        
    });
</script>
<script>
    /*
        Extra delivery charge
    */
    (function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#save-extra-charge-form').click(function() {
            var data = {
                charge: $('#extra-charge-amount').val(),
                comment: $('#extra-charge-comment').val(),
                shoppingcart_id: {{ $shipping_address_details->shoppingcart_id }},
            };
            $.ajax({
                type: 'POST',
                url: base_url+"/admin/order/extra-charge",
                data: data,
                success: function(total) {
                    var extra_charge= (parseInt($('#extra-charge-amount').val()));
                    var end_total=total;
                    document.getElementById('extra-charge-text').innerHTML='Tk '+$('#extra-charge-amount').val();
                    document.getElementById('end_total').innerHTML='Tk '+end_total;
                    alert('Update success.After 2s page will auto reload.');
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                },
                error:function(e) {
                }
            });
        });
    })();
    
    (function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#save-payment-mode').click(function() {
            var data = {
                payment_mode: $('#change_payment_mode').val(),
                shoppingcart_id: {{ $shipping_address_details->shoppingcart_id }},
            };
            $.ajax({
                type: 'POST',
                url: base_url+"/admin/order/change-payment-mode",
                data: data,
                success: function(d) {
                    document.getElementById('show-payment-mode').innerHTML=$('#change_payment_mode').val();
                    document.getElementById("change_payment_mode").value=$('#change_payment_mode').val();
                    alert('Update success.After 2s page will auto reload.');
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                },
                error:function(e) {
                }
            });
        });
    })();
</script>
<script src="{{asset('assets/admin/js/custom-script.js')}}?2"></script>
@endsection