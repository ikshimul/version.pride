@extends('admin.layouts.app')
@section('title', 'Product Add Form')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Product</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item">Product</li>
                    <li class="breadcrumb-item active">Add</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Add Product</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <form name="add_product" id="myform" action="{{url('/admin/product/save')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <!-- SELECT2 EXAMPLE -->
                        <div class="box box-default">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3">&nbsp;</div>
                                    <div class="col-md-6">
                                        @if (session('save'))
                                        <div class="callout callout-success">
                                          <h5><i class="fas fa-info"></i> Note:</h5>
                                          {{ session('save') }}
                                        </div>
                                        @endif
                                        @if (session('error'))
                                        <div class="callout callout-danger">
                                          <h5><i class="fas fa-info"></i> Note:</h5>
                                          {{ session('error') }}
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-md-3">&nbsp;</div>
                                    <div class="col-md-6">
                                        <fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
                                            <legend style="padding:5px 20px; text-align:center; width:auto">Item Definitions</legend>
                                            	<div class="form-group">
                                					<label for="maincat">Parent Category</label>
                                					<select type="text" class="form-control select2"  name="maincat" id="maincat"  required>
                                						<option value="">-- select parent category --</option>
                                						@foreach($maincats as $maincat)
                                						<option value="{{$maincat->id}}">{{$maincat->name}}</option>
                                						@endforeach
                                					</select>
                                				</div>
                                				<div class="form-group row">
                                					<div class="col-sm-6">
                                					    <label for="cat">Main Category</label>
                                					    <select type="text" class="form-control select2"  name="cat" id="cat"  required>
                                						<option value="">-- select category --</option>
                                						<!--@foreach($cats as $procat)-->
                                						<!--<option value="{{$procat->id}}">{{$procat->name}}</option>-->
                                						<!--@endforeach-->
                                					</select>
                                					</div>
                                					<div class="col-sm-6">
                                					    <label for="subcat">Sub Category</label>
                                						<select class="form-control select2" type="text" name="subcat" id="subcat" required>
                                							<option value="">-- select sub category --</option>
                                							<!--@foreach($subcats as $subprocat)-->
                                    			<!--			<option value="{{$subprocat->id}}">{{$subprocat->name}}</option>-->
                                    			<!--			@endforeach-->
                                						</select>
                                					</div>
                                				</div>
                                            <div class="form-group">
                                                <label for="txtproductname">Product Name</label>
                                                <input class="form-control" type="text" name="txtproductname" id="txtproductname" value="{{ old('txtproductname') }}" required>
                                                <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                                            </div>
                                            <!--<div class="form-group">
                                                    <label for="txtproductname">Product Barcode</label>
                                                    <input class="form-control" type="text" name="product_barcode" id="product_barcode" value="{{ old('product_barcode') }}">
                                                    <span class="help-text">{{ $errors->first('product_barcode') }}</span>
                                            </div> --->
                                            <div class="form-group">
                                                <label for="txtstyleref">Style Code/Ref</label>
                                                <input class="form-control" type="text" id="required-input" name="txtstyleref" id="txtstyleref" required>
                                                <span class="help-text">{{ $errors->first('txtstyleref') }}</span>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtprice">Price</label>
                                                <input class="form-control" type="number"  name="txtprice" id="in-range-input" min="1" max="20000"  required>
                                                <span class="help-text">{{ $errors->first('txtprice') }}</span>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtpricediscounted">Discount Percentage</label>
                                                <input class="form-control" type="number" name="txtpricediscounted" id="optional-input" min="0" max="200" >
                                                <span class="help-text"></span>
                                            </div>
                                            <!--<div class="form-group">
                                                    <label for="ddlfilter">Price Filter</label>
                                                    <select class="form-control" name="ddlfilter" id="optional-input">
                                                            <option>-- Select Filter --</option>
                                                            <option value="high">High</option>
                                                            <option value="medium">Medium</option>
                                                            <option value="low">Low</option>
                                                    </select>
                                                    <span class="help-text"></span>
                                            </div>-->
                                            <div class="form-group">
                                                <label for="txtorder">Product Order</label>
                                                <input class="form-control" type="number" name="txtorder" id="optional-input" min="1" max="1000" value="<?php echo $total_product + 1; ?>" readonly>
                                                <span class="help-text"><!-- Only Numbers [ <strong>Total Product Added: <?php echo $total_product; ?></strong> ] ----></span>
                                            </div>
                                            <div class="form-group clearfix">
                            				    <label for="check-option-1">Collection</label>
                                				  @foreach($campaigns as $campaign)
                                                  <div class="icheck-primary">
                                                    <input type="radio" id="campaign{{$campaign->id}}" value="{{$campaign->id}}" name="campaign">
                                                    <label for="campaign{{$campaign->id}}" style="font-weight: 500;">
                                                        {{$campaign->name}}
                                                    </label>
                                                  </div>
                                                  @endforeach
                                            </div>
                            				<div class="form-group clearfix">
                            				  <label for="check-option-1">Occassion</label>
                                              <div class="icheck-primary">
                                                <input type="radio" id="occassion1" value="12" name="occassion">
                                                <label for="occassion1" style="font-weight: 500;">
                                                    Festive Wear
                                                </label>
                                              </div>
                                              <div class="icheck-primary">
                                                <input type="radio" id="occassion2" value="13" name="occassion">
                                                <label for="occassion2" style="font-weight: 500;">
                                                    Office Wear
                                                </label>
                                              </div>
                                              <div class="icheck-primary">
                                                <input type="radio" id="occassion3" value="14" name="occassion">
                                                <label for="occassion3" style="font-weight: 500;">
                                                  Casual Wear
                                                </label>
                                              </div>
                                            </div>
                                            <!--<div class="form-group">-->
                                            <!--    <label>Size Image</label>-->
                                            <!--    <input style="width:100%" type="file"  name="sizeguide_image" class="btn btn-info"/>-->
                                            <!--    <span class="help-text"></span>-->
                                            <!--</div>-->
                                            <div class="form-group">
                                                <label for="txtproductdetails">Product Description</label>
                                                <textarea class="form-control" type="text" name="txtproductdetails" id="optional-input" rows="3"></textarea>{{ old('txtproductdetails') }}
                                                <span class="help-text"></span>
                                            </div>
                                            <div class="form-group">
                        						<label for="fabric">Fabric Composition <a href="{{url('/admin/product/fabric/add')}}" target="blank"><i class="fas fa-plus-square"></i></a></label>
                        						 <select class="form-control select2" id="fabric">
                        						     <option value=""> -- Select Fabric -- </option>
                        						    @foreach($fabrics as $fabric)
                        						       <option value="{{$fabric->id}}"> {{$fabric->name}} </option>
                        						    @endforeach
                        						 </select>
                        						 <input type="hidden" name="fabric" id="fabric_add" />
                        					</div>
                        					<div class="form-group">
                            					<label for="care">Care Description</label>
                            					<textarea class="form-control" type="text" name="txtproductcare" id="care" rows="3" cols="50" required></textarea>
                            				</div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-center"><a class="add_style submitbtn btn btn-primary" href="#">Add More Color/Style</a></div>
                                        <input type="hidden" name="total_grp" id="myform" class="total_grp" value="">
                                        <input type="hidden" name="blank" id="blank" value="">
                                        <div class="clone_grp">
                                            <div class="product_style">
                                                <div id="right_part">
                                                    <div class="formcontainer">
                                                        <fieldset style="margin:10px; padding:5px 20px; border:2px solid #00c0ef44">
                                                            <legend style="padding:5px; 20px">Product Style <small class="sl"></small>: <a type="button" class="remove_style submitbtn btn btn-warning pull-right"><span class="glyphicon glyphicon-remove"></span> Remove</a></legend>
                                                            <div>
                                                                <span for="blank" style="display:none;">Available Product Size</span><span style="font-weight:700;padding-top:3px;">Available Product Size for Add </span></br>
                                                                <?php
                                                                $i = 0;
                                                                foreach ($avail_size as $size) {
                                                                    $i++;
                                                                    ?>
                                                                    <span style="border:1px solid #00c0ef33; min-width:100px; display:inline-block; margin:5px; padding:5px 2px;"><input type="checkbox" class="chksize clone_field" rel="size<?php echo $i ?>"  name="size<?php echo $i ?>" value="<?php echo $size->prosize_name; ?>" style="width:39px !important;"/> <span rel="input_size<?php echo $i ?>"  for="input_size<?php echo $i ?>" class="clone_field"><?php echo $size->prosize_name; ?></span></span>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Quantity</label>
                                                                <?php
                                                                $i = 0;
                                                                foreach ($avail_size as $size) {
                                                                    $i++;
                                                                    ?>
                                                                    <div class="form-group" style="margin-bottom:0">
                                                                        <label style="display:none"></label>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <input type="text" size="6" rel="input_size<?php echo $i ?>"  name="input_size<?php echo $i ?>" class="clone_field" id="required-input" style="display:none; margin-bottom:15px" placeholder="Qty for <?php echo $size->prosize_name; ?>" value="" />
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <input type="text" size="6" rel="barcode_input_size<?php echo $i ?>"  name="input_barcode<?php echo $i ?>" class="clone_field" id="required-input" style="display:none; margin-bottom:15px" placeholder="Barcode for <?php echo $size->prosize_name; ?>" value="" />
                                                                            </div>
                                                                        </div>												
                                                                    </div>
                                                                <?php } session(['numberofavailabesize' => $i]); ?>
                                                            </div>
                                                            <div class="form-group">
                                                                <label rel="txtcolorname"  for="txtcolorname" class="clone_field">Color / Style Name</label>
                                                                <input type="text" rel="txtcolorname" name="txtcolorname" class="form-control txtfield clone_field" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Color Thumbnail</label>
                                                                <input id="input-file" rel="file_colorthm" type="file" class="clone_field file input-file" name="file_colorthm">
                                                                <!--<input style="width:100%" type="file" rel="file_colorthm" name="file_colorthm" class="btn btn-primary clone_field" required>-->
                                                                <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 81 Px Height: 62 PX ]</small>
                                                            </div>
                                                            <div class="form-group">
                                                                <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 1</label>
                                                                <input style="width:100%" type="file" rel="file_im1" name="file_im2" class="clone_field file input-file" required>
                                                                <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</small>
                                                            </div>
                                                            <div class="form-group">
                                                                <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 2</label>
                                                                <input style="width:100%" type="file" rel="file_im2" name="file_im2" class="clone_field file input-file">
                                                            </div>
                                                            <div class="form-group">
                                                                <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 3</label>
                                                                <input style="width:100%" type="file" rel="file_im3" name="file_im3" class="clone_field file input-file">
                                                            </div>
                                                            <div class="form-group">
                                                                <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 4</label>
                                                                <input style="width:100%" type="file" rel="file_im4" name="file_im4" class="clone_field file input-file">
                                                            </div>
                                                            <div class="form-group">
                                                                <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 5</label>
                                                                <input style="width:100%" type="file" rel="file_im5" name="file_im5" class="clone_field file input-file">
                                                            </div>
                                                            <div class="form-group">
                                                                <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 6</label>
                                                                <input style="width:100%" type="file" rel="file_im6" name="file_im6" class="clone_field file input-file">
                                                            </div>
                                                        </fieldset>
                                                    </div>

                                                </div>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="dynamic">
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> <i class="fas fa-save"></i> Save Product</button>
                                </div>
                            </div>
                    </form>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>

    </div>
</section>
<!-- /.content -->
<link rel="stylesheet" href="{{asset('assets_admin/previewForm.css')}}">
<script src="{{asset('assets_admin/previewForm.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('assets_admin/dynamic_select_box.js')}}"></script> --->
<script>
    $(document).ready(function () {
        $("#fabric").change(function () {
            var fabric = $("#fabric").val();
            $(".loading-modal").show();
            var url_op = base_url + "/admin/product/fabric/view?id=" + fabric;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    $(".loading-modal").hide();
                   // console.log(data.name);
                    $('#fabric_add').empty();
                    $('#fabric_add').val(data.name);
                    $('#care').val(data.care);
                    
                }
            });
        });
        $("#maincat").change(function () {
            var maincat_id = $("#maincat").val();
             //alert(maincat_id)
            var url_op = base_url + "/ajax/get-procat/" + maincat_id;
            $.ajax({
                url: url_op,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data) {
                    // alert(html);
                    // $('#CityList').html(html);
                    $('#cat').empty();
                    $('#cat').append('<option value="">Select Category</option>');
                    $.each(data, function (index, supproobj) {
                        $('#cat').append('<option value="' + supproobj.id + '">' + supproobj.name + '</option>');
                    });
                }
            });
        });
        $("#cat").change(function(){
            var procat_id=$("#cat").val();
            //alert(procat_id);
            var url=base_url+"/ajax/get-subcats/"+procat_id;
            $.ajax({
                url:url,
                type:'GET',
                dataType:'json',
                success:function(data){
                    $('#subcat').empty();
                    $('#subcat').append('<option value="">Select Sub Category</option>');
                    $.each(data, function (index, supproobj) {
                        $('#subcat').append('<option value="' + supproobj.id + '">' + supproobj.name + '</option>');
                    });
                }
            });
        });
    });
    $(document).ready(
            function () {
                
                $('#isspecial').is(':checked');
                var grp = $('.clone_grp').clone(true);
                $('.add_style').click(function () {
                    add_grp();
                    return false;
                });
                add_grp();
                $('.remove_style').click(function () {

                    remove(this);
                    return false;
                });
                function chksize_click(obj) {
                    var input = $(obj).attr('name');
                    //alert('input[name=input_'+ input +']');
                    if ($(obj).is(':checked')) {
                        $('input[name=input_' + input + ']').show();
                        $('input[name=input_' + input + ']').addClass('form-control');
                        $('input[name=barcode_input_' + input + ']').show();
                        $('input[name=barcode_input_' + input + ']').addClass('form-control');
                    } else {
                        $('input[name=input_' + input + ']').hide();
                        $('input[name=input_' + input + ']').val("");
                        $('input[name=barcode_input_' + input + ']').hide();
                        $('input[name=barcode_input_' + input + ']').val("");
                    }

                }
                ;
                function add_grp(obj) {
                    $('.dynamic').append(grp.html());
                    grp_arng();
                    $('.remove_style').unbind("click");
                    $('.remove_style').bind("click", function () {
                        remove(this);
                        return false;
                    });
                    $('.chksize').unbind("change");
                    $('.chksize   ').bind("change", function () {
                        chksize_click(this);
                        return false;
                    });
                }//add add_subgrp
                function remove(obj) {
                    $(obj).parent().parent().parent().parent().fadeOut('slow',
                            function () {
                                $(obj).parent().parent().parent().parent().remove();
                                grp_arng();
                            }
                    );
                }//remove

                function grp_arng() {
                    var i = 0;
                    $('.product_style').each(function () {
                        i++;
                        $(this).find('.sl').html(i);
                        $(this).find('.clone_field').each(function () {
                            var name = $(this).attr('rel');
                            //alert($(this).attr('for'));

                            if ($(this).attr('for') == null) {
                                $(this).attr('name', name + '_' + i);
                            } else {
                                $(this).attr('for', name + '_' + i);
                            }
                        });
                    }); //each clone_field prod    uc    t_st    yle
                    $('.total_grp').attr('value', i);
                }//grp_arng
                $('.clone_grp').remove();
                grp_arng();
                $(".input-file").fileinput({
                    showUpload: false,
                    dropZoneEnabled: false,
                    maxFileCount: 10,
                    inputGroupClass: "input-group-md"
                });
            });
    $(document).ready(function () {
        $('#myform').previewForm();

    });
</script>
@endsection