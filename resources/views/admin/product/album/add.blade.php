@extends('admin.layouts.app')
@section('title', 'Product Album Add Form')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Product</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item">Product</li>
                    <li class="breadcrumb-item">List</li>
                    <li class="breadcrumb-item">Manage Gallery</li>
                    <li class="breadcrumb-item active">Album</li>
                    <li class="breadcrumb-item active">Add</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Add New Product Album/Style for '<?php echo $product->product_styleref; ?>'</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form name="add_product" id="myform" action="{{url('admin/product/album/save')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <!-- SELECT2 EXAMPLE -->
                        <div class="box box-default">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3">&nbsp;</div>
                                    <div class="col-md-6">
                                        @if (session('save'))
                                        <div class="callout callout-success">
                                          <h5><i class="fas fa-info"></i> Note:</h5>
                                          {{ session('save') }}
                                        </div>
                                        @endif
                                        @if (session('error'))
                                        <div class="callout callout-danger">
                                          <h5><i class="fas fa-info"></i> Note:</h5>
                                          {{ session('error') }}
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-md-3">&nbsp;</div>
                                    <div class="col-md-3">&nbsp;</div>
                                    <div class="col-md-6">
                                        <fieldset style="margin:10px;  margin-top:10px; padding:5px 20px; border:2px solid #00c0ef44">
                                        <legend style="padding:5px 20px; text-align:center; width:auto">New Album</legend>
                                        <input type="hidden" name="total_grp" id="myform" class="total_grp" value="">
                                        <input type="hidden" name="blank" id="blank" value="">
                                        <div class="clone_grp">
                                            <div class="product_style">
                                                <div id="right_part">
                                                   <div class="formcontainer">
                                                        <div class="formcontainer">
                                                            <div class="form-group formheader">
                                                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                                                            </div>
                                                            <div class="formcontainer">
                                                                <div>
                                                                    <span for="blank" style="display:none;">Available Product Size</span><span style="font-weight:700;padding-top:3px;">Available Product Size for Add </span></br>
                                                                    <?php
                                                                    $i = 0;
                                                                    foreach ($avail_size as $size) {
                                                                        $i++;
                                                                        ?>
                                                                        <span style="border:1px solid #00c0ef33; min-width:100px; display:inline-block; margin:5px; padding:5px 2px;">
                                                                        <input type="checkbox" class="chksize clone_field" rel="size<?php echo $i ?>"  name="size<?php echo $i ?>" value="<?php echo $size->prosize_name; ?>" style="width:39px !important;"/> 
                                                                        <span rel="input_size<?php echo $i ?>"  for="input_size<?php echo $i ?>" class="clone_field"><?php echo $size->prosize_name; ?></span></span>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Quantity</label>
                                                                    <?php
                                                                    $i = 0;
                                                                    foreach ($avail_size as $size) {
                                                                        $i++;
                                                                        ?>
                                                                        <div class="form-group" style="margin-bottom:0">
                                                                            <label style="display:none"></label>
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <input type="text" size="6" rel="input_size<?php echo $i ?>"  name="input_size<?php echo $i ?>" class="clone_field" id="required-input" style="display:none; margin-bottom:15px" placeholder="Qty for <?php echo $size->prosize_name; ?>" value="" />
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <input type="text" size="6" rel="barcode_input_size<?php echo $i ?>"  name="barcode_input_size<?php echo $i ?>" class="clone_field" id="required-input" style="display:none; margin-bottom:15px" placeholder="Barcode for <?php echo $size->prosize_name; ?>" value="" />
                                                                                </div>
                                                                            </div>												
                                                                        </div>
                                                                    <?php } session(['numberofavailabesize' => $i]); ?>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label rel="txtcolorname"  for="txtcolorname" class="clone_field">Color / Style Name</label>
                                                                    <input type="text" rel="txtcolorname" name="txtcolorname" class="form-control txtfield clone_field" required>
                                                                </div>
                                                            </div>
                                                         </div> 
                                                     </div>   
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="dynamic"></div>
                                        <div class="form-group">
                                            <label rel="file_colorthm" id="input-file" for="file_colorthm" class="clone_field">Color Thumbnail</label>
                                            <input id="input-file"  type="file" class="file" name="file_colorthm" data-browse-on-zone-click="true">
                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 81 px X Height: 62 px ]</small>
                                        </div>
                                        <div class="form-group">
                                            <label rel="file_colorthm" id="input-file" for="file_colorthm" class="clone_field">Image 1</label>
                                            <input id="input-file"  type="file" class="file" name="file_im1" data-browse-on-zone-click="true">
                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</small>
                                        </div>
                                        <div class="form-group">
                                            <label rel="file_colorthm" id="input-file" for="file_colorthm" class="clone_field">Image 2</label>
                                            <input id="input-file"  type="file" class="file" name="file_im2" data-browse-on-zone-click="true">
                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</small>
                                        </div>
                                        <div class="form-group">
                                            <label rel="file_colorthm" id="input-file" for="file_colorthm" class="clone_field">Image 3</label>
                                            <input id="input-file"  type="file" class="file" name="file_im3" data-browse-on-zone-click="true">
                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</small>
                                        </div>
                                        <div class="form-group">
                                            <label rel="file_colorthm" id="input-file" for="file_colorthm" class="clone_field">Image 4</label>
                                            <input id="input-file"  type="file" class="file" name="file_im4" data-browse-on-zone-click="true">
                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</small>
                                        </div>
                                        <div class="form-group">
                                            <label rel="file_colorthm" id="input-file" for="file_colorthm" class="clone_field">Image 5</label>
                                            <input id="input-file"  type="file" class="file" name="file_im5" data-browse-on-zone-click="true">
                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</small>
                                        </div>
                                        <div class="form-group">
                                            <label rel="file_colorthm" id="input-file" for="file_colorthm" class="clone_field">Image 6</label>
                                            <input id="input-file"  type="file" class="file" name="file_im6" data-browse-on-zone-click="true">
                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</small>
                                        </div>
                                        </fieldset>
                                         <div class="box-footer">
                                            <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> <i class="fas fa-save"></i> Save Album</button>
                                        </div>
                                    </div>
                                    <div class="col-md-3">&nbsp;</div>
                                </div>
                               
                            </div>
                    </form>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>

    </div>
</section>
<script>
$(document).ready(function () {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: false,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
    $('#isspecial').is(':checked');
        var grp = $('.clone_grp').clone(true);
        $('.add_style').click(function () {
            add_grp();
            return false;
        });
        add_grp();
        $('.remove_style').click(function () {
            remove(this);
            return false;
        });
        function chksize_click(obj) {
            var input = $(obj).attr('name');
             //alert('input[name=barcode_input_' + input + ']');
            if ($(obj).is(':checked')) {
                $('input[name=input_' + input + ']').show();
                $('input[name=input_' + input + ']').addClass('form-control');
                $('input[name=barcode_input_' + input + ']').show();
                $('input[name=barcode_input_' + input + ']').addClass('form-control');
            } else {
                $('input[name=input_' + input + ']').hide();
                $('input[name=input_' + input + ']').val("");
                $('input[name=barcode_input_' + input + ']').hide();
                $('input[name=barcode_input_' + input + ']').val("");
            }

        }
        ;
        function add_grp(obj) {
            $('.dynamic').append(grp.html());
            grp_arng();
            $('.remove_style').unbind("click");
            $('.remove_style').bind("click", function () {
                remove(this);
                return false;
            });
            $('.chksize').unbind("change");
            $('.chksize   ').bind("change", function () {
                chksize_click(this);
                return false;
            });
        }//add add_subgrp
        function remove(obj) {
            $(obj).parent().parent().parent().parent().fadeOut('slow',
                    function () {
                        $(obj).parent().parent().parent().parent().remove();
                        grp_arng();
                    }
            );
        }//remove

        function grp_arng() {
            var i = 0;
            $('.product_style').each(function () {
                i++;
                $(this).find('.sl').html(i);
                $(this).find('.clone_field').each(function () {
                    var name = $(this).attr('rel');
                    //alert($(this).attr('for'));

                    if ($(this).attr('for') == null) {
                        $(this).attr('name', name + '_' + i);
                    } else {
                        $(this).attr('for', name + '_' + i);
                    }
                });
            }); //each clone_field prod    uc    t_st    yle
            $('.total_grp').attr('value', i);
        }//grp_arng
        $('.clone_grp').remove();
        grp_arng();
});
</script>
@endsection