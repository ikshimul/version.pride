@extends('admin.layouts.app')
@section('title', 'Edit Album Image')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Album Image</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Product</li>
              <li class="breadcrumb-item">List</li>
              <li class="breadcrumb-item">Manage Gallery</li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Edit Album Image</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <form name="add_product" id="myform" action="{{url('/admin/product/album/update')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                 <input type="hidden" class="form-control" name="id" value="{{$album->id}}" required />
				<fieldset style="margin:10px;padding:5px 20px; border:2px solid #00c0ef44">
				<legend style="padding:5px 20px; text-align:center; width:auto">Edit Album Image</legend>
				    @if (session('save'))
                    <div class="callout callout-info">
                        <h5><i class="fas fa-info"></i> Note:</h5>
                        {{ session('save') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="callout callout-danger">
                        <h5><i class="fas fa-info"></i> Note:</h5>
                        {{ session('error') }}
                    </div>
                    @endif
					<div class="form-group">
						<label for="campus_id">Album Name</label>
						<input type="text" class="form-control" name="productalbum_name" value="{{$album->productalbum_name}}" required />
						<span class="help-text"></span>
					</div>
					<div class="form-group">
						<label for="campus_id">Album Order</label>
						<input type="text" class="form-control" name="productalbum_order" value="{{$album->productalbum_order}}" required />
						<span class="help-text"></span>
					</div>
					<div class="form-group">
                            <label>Current Image</label>
                            <div class="mb10">
                                <span class="file-input">
                                    <div class="file-preview">
                                        <div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
                                        <div class="file-preview-thumbnails">
                                            <div class="file-preview-frame" id="preview">
                                                <img class="img-responsive" src="{{url('/')}}/storage/app/public/pgallery/<?php echo $album->productalbum_img; ?>" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>   
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>
                                </span>
                            </div>
                        </div>
					<div class="form-group {{ $errors->has('productalbum_img') ? ' has-error' : '' }}">
                        <label>New Image</label>
                        <input id="input-file"  type="file" class="file" name="productalbum_img" data-browse-on-zone-click="true">
                        <span class="help-block" style="color:#06be1c">[ Image Type: jpeg or jpg, Width: 81 Px Height: 62 PX ]</span>  
                        <div class="help-block with-errors">{{ $errors->first('productalbum_img') }}</div>
                    </div>
					<div class="box-footer">
					    <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> <i class="fas fa-edit"></i> Update Album Image</button>
					</div>
					</fieldset>
					</form>
				</div>
			</div>
			
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
    <!-- Ajax modal ---->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to deactive this?</h4>
                </div>
                <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                    <a href="#" class="btn btn-sm btn-danger" id="delete_link">Confirm</a>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--- Ajax modal end ---->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection