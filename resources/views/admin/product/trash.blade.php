<?php

use App\Http\Controllers\admin\product\Managestock;
use App\Http\Controllers\admin\product\Managecolor;
?>
@extends('admin.layouts.app')
@section('title', 'Trash Product List')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Product Recycle Bin</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item">Product</li>
          <li class="breadcrumb-item active">List</li>
          <li class="breadcrumb-item active">Recycle Bin</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Product Recycle Bin</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        @if (session('update'))
        <div class="callout callout-success">
          <h5><i class="fas fa-info"></i> Note:</h5>
          {{ session('update') }}
        </div>
        @endif
        @if (session('deactive'))
        <div class="callout callout-danger">
          <h5><i class="fas fa-info"></i> Note:</h5>
          {{ session('deactive') }}
        </div>
        @endif
       <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Style Code</th>
                        <th>Category - Sub Category</th>
                        <th style="text-align:center;">Image</th>
                        <th style="text-align:center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (!empty($product_list))
                    <?php
                    $i = 0;
                    foreach ($product_list as $product) {
                        $i++;
                        ?>
                        <tr>
                            <td style="width:1%;text-align:center;"><?php echo $i; ?></td>
                            <td style="width:15%;"><?php echo $product->product_name; ?></td>
                            <td style="width:15%;"><?php echo $product->product_styleref; ?></td>
                            <td style="width:15%;"><?php echo $product->procat_name . " - " . $product->subprocat_name." - ".$product->subcat_name; ?></td>
                            <td style="text-align:center;">
                                <img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_tiny; ?>" alt=""  />
                            </td>
                            <td style="text-align:center;">
                                <a class="btn bg-info btn-flat btn-sm margin tdata" href='{{url("admin/product/edit?id={$product->id}")}}'>Edit</a> 
                                <a class="btn bg-info btn-flat btn-sm margin tdata" href='{{url("admin/product/album/view?id={$product->id}")}}'>Manage Product Gallery</a> 
                                <a class="btn bg-info btn-flat btn-sm margin tdata" href='{{url("admin/product/stock/view?id={$product->id}")}}'>Manage Stock</a> 
                                <a class="btn btn-success btn-flat btn-sm tdata" onclick="return confirm('Are you sure you want to reactive this product?');" href='{{url("admin/product/trash/reactive?id=$product->id")}}' alt="Reactive" title="Reactive">Restore</a>
                                <a class="btn btn-danger btn-flat btn-sm margin tdata" onclick="return confirm('Are you sure you want to delete this product permanently?');" href='{{url("admin/product/trash/delete?id=$product->id")}}' >Delete</a>
                            </td>
                        </tr>
                    <?php } ?>
                    @endif
                </tbody>
            </table>
          </div>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
  <!-- Ajax modal ---->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this product ?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--- Ajax modal end ---->
</section>
@endsection