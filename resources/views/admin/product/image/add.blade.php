@extends('admin.layouts.app')
@section('title', 'Add Product Image')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Product Image</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Product</li>
              <li class="breadcrumb-item">List</li>
              <li class="breadcrumb-item">Manage Gallery</li>
              <li class="breadcrumb-item active">Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Add Product Image for '<?php echo $product->product_styleref; ?>'</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <form name="add_product" id="myform" action="{{url('/admin/product/image/save')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                 <input type="hidden" class="form-control" name="album_id" value="{{$album_id}}" required />
                 <input type="hidden" name="product_id" class="form-control" value="{{$product->id}}" />
				<fieldset style="margin:10px;padding:5px 20px; border:2px solid #00c0ef44">
				<legend style="padding:5px 20px; text-align:center; width:auto"><?php echo $product->product_styleref; ?></legend>
				    @if (session('save'))
                    <div class="callout callout-info">
                        <h5><i class="fas fa-info"></i> Note:</h5>
                        {{ session('save') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="callout callout-danger">
                        <h5><i class="fas fa-info"></i> Note:</h5>
                        {{ session('error') }}
                    </div>
                    @endif
					<div class="form-group">
						<label for="campus_id">Image Order</label>
						<input type="text" class="form-control" name="productimg_order" required />
						<span class="help-text"></span>
					</div>
					<div class="form-group {{ $errors->has('filename') ? ' has-error' : '' }}">
                        <label>New Image</label>
                        <input id="input-file"  type="file" class="file" name="filename" data-browse-on-zone-click="true">
                        <span class="help-block" style="color:#06be1c">[ Image Type: jpeg or jpg, Width: 1600 Px Height: 2400 PX ]</span>  
                        <div class="help-block with-errors">{{ $errors->first('filename') }}</div>
                    </div>
					<div class="box-footer">
					    <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> <i class="fas fa-save"></i> Add Product Image</button>
					</div>
					</fieldset>
					</form>
				</div>
			</div>
			
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
    <!-- Ajax modal ---->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to deactive this?</h4>
                </div>
                <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                    <a href="#" class="btn btn-sm btn-danger" id="delete_link">Confirm</a>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--- Ajax modal end ---->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection