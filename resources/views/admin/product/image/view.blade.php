@extends('admin.layouts.app')
@section('title', $product->product_styleref)
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Product Image</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item">Product</li>
          <li class="breadcrumb-item">List</li>
          <li class="breadcrumb-item active">Product Gallery</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Manage Product Images for '<?php echo $product->product_styleref; ?>'</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        @if (session('update'))
        <div class="callout callout-success">
          <h5><i class="fas fa-info"></i> Note:</h5>
          {{ session('update') }}
        </div>
        @endif
        @if (session('deactive'))
        <div class="callout callout-danger">
          <h5><i class="fas fa-info"></i> Note:</h5>
          {{ session('deactive') }}
        </div>
        @endif
        @if (session('fav'))
        <div class="callout callout-danger">
          <h5><i class="fas fa-info"></i> Note:</h5>
          {{ session('fav') }}
        </div>
        @endif
        <div style=padding-bottom:10px;>
            <a class="btn btn-primary btn-flat btn-sm margin tdata" href='{{url("admin/product/image/add?product_id=$product->id&album_id=$album_id")}}'>Add New Image</a> 
        </div>
       <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Image Tag</th>
                        <th>Image</th>                            
                        <th>Image Order</th>                            
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 1;
                        foreach ($images as $img) {
                            ?>                       
                            <!-- data -->
                            <tr>
                                <td width="5%"><?php echo $i; ?></td>
                                <td><?php echo $img->productimg_title; ?></td>                            
                                <td><img src="{{url('/')}}/storage/app/public/pgallery/<?php echo $img->productimg_img_tiny; ?>" alt="Banner" /></td>							
                                <td><?php echo $img->productimg_order; ?></td>
                                <td width="20%">
                                    <a class="btn btn-sm btn-flat btn-primary margin tdata" href='{{url("admin/product/image/edit?id=$img->id&serial=$i&product_id=$product->id")}}'> Edit</a> 
                                    <a class="btn btn-sm btn-flat btn-danger margin tdata" onclick="return confirm('Are you sure you want to delete this image?');"  href='{{url("admin/product/image/delete?id=$img->id&serial=$i&product_id=$product->id")}}'> Delete</a>
                                </td>
                            </tr>
                            <!-- data -->
                            <?php
                            $i++;
                        } // end of while	
                        ?> 
                </tbody>
            </table>
          </div>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</section>
@endsection