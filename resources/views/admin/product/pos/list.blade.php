@extends('admin.layouts.app')
@section('title','Popup List')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Popup</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Home Page</a></li>
          <li class="breadcrumb-item"><a href="#">Popup</a></li>
          <li class="breadcrumb-item active">List</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Popup</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        <div class="box-body">
            <div class="row">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Title</th>
                                <th>Link</th>
                                <th>Image</th>
                                <th style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($popups as $popup)
                            <tr>
                                <td style="width:2%;"><?php echo $popup->id; ?></td>
                                <td><?php echo $popup->title; ?></td>
                                <td><?php echo $popup->link; ?></td>
                                <td><img src="{{ URL::to('') }}/storage/app/public/popup/<?php echo $popup->image; ?>" class="img-responsive" height="100px"></td>
                                <td>
                                    <a class="btn bg-olive btn-flat btn-sm margin tdata" href='{{url("admin/popup/edit?id={$popup->id}")}}'>Edit</a> 
                                    <?php if($popup->active == '0'){ ?>
                                    <a class="btn btn-danger btn-flat btn-sm tdata" href="{{url('admin/category/maincat/active')}}/<?php echo $popup->id; ?>"  alt="Active" title="Active">Deactive</a>
                                    <?php }else{ ?>
                                    <a class="btn btn-success btn-flat btn-sm tdata" href="{{url('admin/category/maincat/deactive')}}/<?php echo $popup->id; ?>" alt="Deactive" title="Deactive">Active</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> 
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
