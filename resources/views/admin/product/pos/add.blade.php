<?php
use App\Http\Controllers\admin\pos\Manageproduct;
?>
@extends('admin.layouts.app')
@section('title', 'Product Add Form')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Product</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item">Product</li>
                    <li class="breadcrumb-item active">Add</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Search Product</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                  <form name="singleform" action="{{url('/admin/pos/product/add')}}" method="GET">
                    <div class="form-group">
                        <label>Select Style Code</label>
                        <div class="input-group">
                            <select name="stylecode" id="stylecode"  class="form-control select2" required>
                                <option  value=""> --- Select Style Code --- </option>
                                <?php
                                foreach ($unique_style as $item) {
                                    ?>
                                    <option  value="<?php echo $item->DesignRef; ?>"><?php echo $item->DesignRef; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                          <span class="input-group-append">
                            <button class="btn btn-outline-secondary bg-white border-start-0 border ms-n3" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                        </div>
                    </div>
                </form>    
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

          </div>
          <!-- /.card-body -->
        </div>
        <?php
        if ($item_define!=null) {
        ?>
        <form name="add_product" id="myform" action="{{url('/admin/pos/product/save')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <!-- /.card -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Product Information</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                 @if (session('save'))
                <div class="callout callout-success">
                  <h5><i class="fas fa-info"></i> Note:</h5>
                  {{ session('save') }}
                </div>
                @endif
                @if (session('error'))
                <div class="callout callout-danger">
                  <h5><i class="fas fa-info"></i> Note:</h5>
                  {{ session('error') }}
                </div>
                @endif
                </div>
                <div class="col-md-12" style="text-align:center">
                <?php
				$i = 0;
				foreach ($item_color as $color_list) {
					$i++;
				?>
					<span style="border:1px solid #00c0ef33; min-width:100px; display:inline-block; margin:5px; padding:5px 2px; text-align:center">
						<input onClick="colorClicked(this);" type="checkbox" class="chksize clone_field" rel="size"  name="color_{{ $i }}"  style="width:39px !important;" <?php if($i == 1 ) echo 'checked';?>/> 
						<span rel="input_size"  for="input_size" class="clone_field"><?php echo $color_list->color; ?></span>
					</span>
				<?php
				}
				?>
				</div>
              <div class="col-md-6">
               <fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
					 <legend style="padding:5px 20px; text-align:center; width:auto">Item Definitions</legend>
					  <?php if($old_item == null){ ?>
							  <div class="form-group">
            					<label for="maincat">Parent Category</label>
            					<select type="text" class="form-control select2"  name="maincat" id="maincat"  required>
            						<option value="">-- select parent category --</option>
            						@foreach($maincats as $maincat)
            						<option value="{{$maincat->id}}">{{$maincat->name}}</option>
            						@endforeach
            					</select>
            				</div>
            				<div class="form-group row">
            					<div class="col-sm-6">
            					    <label for="cat">Main Category</label>
            					    <select type="text" class="form-control select2"  name="cat" id="cat"  required>
            						<option value="">-- select category --</option>
            					
            					</select>
            					</div>
            					<div class="col-sm-6">
            					    <label for="subcat">Sub Category</label>
            						<select class="form-control select2" type="text" name="subcat" id="subcat" required>
            							<option value="">-- select sub category --</option>
            						
            						</select>
            					</div>
            				</div>
                           
                            <div class="form-group">
                                <label for="txtproductname">Product Name</label>
                                <input class="form-control" type="text" name="txtproductname" id="txtproductname" value="{{ old('txtproductname') }}" required>
                                <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                            </div>
                            <div class="form-group">
                                <label for="txtstyleref">Style Code/Ref</label>
                                <input class="form-control" type="text" id="required-input" name="txtstyleref" id="txtstyleref" value="<?php echo $item_define->DesignRef; ?>" readonly required>
                                <span class="help-text">{{ $errors->first('txtstyleref') }}</span>
                            </div>
                            <div class="form-group">
                                <label for="txtprice">Price</label>
                                <input class="form-control" type="number"  name="txtprice" id="in-range-input" min="1" max="20000"  value="<?php echo $item_define->SellRate; ?>" readonly required>
                                <span class="help-text">{{ $errors->first('txtprice') }}</span>
                            </div>
                            <div class="form-group">
                                <label for="txtpricediscounted">Discount Percentage</label>
                                <input class="form-control" type="number" name="txtpricediscounted" id="optional-input" min="0" max="100" value="0" />
                                <span class="help-text"></span>
                            </div>
                            
                            <div class="form-group">
                                <label for="txtorder">Product Order</label>
                                <input class="form-control" type="number" name="txtorder" id="optional-input" min="1" max="1000" value="<?php echo $total_product + 1; ?>" readonly>
                                <span class="help-text"><!-- Only Numbers [ <strong>Total Product Added: <?php echo $total_product; ?></strong> ] ----></span>
                            </div>
                            <div class="form-group clearfix">
            				    <label for="check-option-1">Collection</label>
                				  @foreach($campaigns as $campaign)
                                  <div class="icheck-primary">
                                    <input type="radio" id="campaign{{$campaign->id}}" value="{{$campaign->id}}" name="campaign">
                                    <label for="campaign{{$campaign->id}}" style="font-weight: 500;">
                                        {{$campaign->name}}
                                    </label>
                                  </div>
                                  @endforeach
                            </div>
            				<div class="form-group clearfix">
            				  <label for="check-option-1">Occassion</label>
                              <div class="icheck-primary">
                                <input type="radio" id="occassion1" value="12" name="occassion">
                                <label for="occassion1" style="font-weight: 500;">
                                    Festive Wear
                                </label>
                              </div>
                              <div class="icheck-primary">
                                <input type="radio" id="occassion2" value="13" name="occassion">
                                <label for="occassion2" style="font-weight: 500;">
                                    Office Wear
                                </label>
                              </div>
                              <div class="icheck-primary">
                                <input type="radio" id="occassion3" value="14" name="occassion">
                                <label for="occassion3" style="font-weight: 500;">
                                  Casual Wear
                                </label>
                              </div>
                            </div>
                            <div class="form-group">
                                <label for="txtproductdetails">Product Description</label>
                                <textarea class="form-control" type="text" name="txtproductdetails" id="optional-input" rows="3"><?php echo $item_define->ItemDescription; ?></textarea>
                                <span class="help-text"></span>
                            </div>
                            <div class="form-group">
        						<label for="fabric">Fabric Composition <a href="{{url('/admin/product/fabric/add')}}" target="blank"><i class="fas fa-plus-square"></i></a></label>
        						 <select class="form-control select2" id="fabric">
        						     <option value=""> -- Select Fabric -- </option>
        						    @foreach($fabrics as $fabric)
        						       <option value="{{$fabric->id}}"> {{$fabric->name}} </option>
        						    @endforeach
        						 </select>
        						 <input type="hidden" name="fabric" id="fabric_add" />
        					</div>
        					<div class="form-group">
            					<label for="care">Care Description</label>
            					<textarea class="form-control" type="text" name="txtproductcare" id="care" rows="3" cols="50" required></textarea>
            				</div>
					<?php }else{ ?>
							<div class="alert alert-info alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-info"></i> Alert! <i class="fa fa-fw fa-hand-o-right"></i></h4>
								This item define already in our database.please add only images for new color.
							 </div>
							 <img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $old_item->product_img_thm; ?>" alt=""  height="200" width="160" />
						    <div class="form-group">
                                <label for="txtproductname">Category</label>
                                <input class="form-control" type="text" name="main_category" id="txtproductname" value="{{$old_item->procat_name}}" readonly>
                                <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                            </div>
                            <div class="form-group">
                                <label for="txtproductname">Sub Category</label>
                                <input class="form-control" type="text" name="sub_category" id="txtproductname" value="{{$old_item->subprocat_name}}" readonly>
                                <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                            </div>
                            <div class="form-group">
                                <label for="txtproductname">Product Name</label>
                                <input class="form-control" type="text" name="txtproductname" id="txtproductname" value="{{$old_item->product_name}}" readonly>
                                <span class="help-text">{{ $errors->first('txtproductname') }}</span>
                            </div>
                            <div class="form-group">
                                <label for="txtstyleref">Style Code/Ref</label>
                                <input class="form-control" type="text" id="required-input" name="txtstyleref" id="txtstyleref" value="<?php echo $item_define->DesignRef; ?>" readonly required>
                                <span class="help-text">{{ $errors->first('txtstyleref') }}</span>
                            </div>
                            <div class="form-group">
                                <label for="txtprice">Price</label>
                                <input class="form-control" type="number"  name="txtprice" id="in-range-input" min="1" max="20000"  value="<?php echo $item_define->SellRate; ?>" readonly required>
                                <span class="help-text">{{ $errors->first('txtprice') }}</span>
                            </div>
                            <div class="form-group">
                                <label for="txtpricediscounted">Discount Percentage</label>
                                <input class="form-control" type="number" name="txtpricediscounted" id="optional-input" min="0" max="100" value="{{$old_item->discount_product_price}}" readonly />
                                <span class="help-text"></span>
                            </div>
							<div class="form-group">
                                <label for="txtproductdetails">Product Description</label>
                                <textarea class="form-control" type="text" name="txtproductdetails" id="optional-input" rows="3" readonly><?php echo $old_item->product_description; ?></textarea>
                                <span class="help-text"></span>
                            </div>
					<?php } ?>
                 </fieldset>
              </div>
              <!-- /.col -->
              
				<?php
                    $c=0;
                     foreach ($item_color as $color_list) { 
                         $c++;
                ?>
              <div class="col-md-6" id = "color_{{ $c }}" style="display:<?php if($c == 1 ) echo 'block;'; else echo 'none'; ?>">
                    <input type="hidden" name="total_grp" class="total_grp" value="<?php echo $c;?>">
                    <input type="hidden" name="blank" id="blank" value="">
                    <div class="clone_grp">
                        <div class="product_style">
                            <div id="right_part">
                                <div class="formcontainer">
                                    <fieldset style="margin:10px; padding:5px 20px; border:2px solid #00c0ef44">
                                        <legend style="padding:5px 20px; text-align:center; width:auto">Product Style for <span style="color:<?php echo $color_list->color;?>; text-shadow: 0 0 5px #555;"><?php echo $color_list->color; ?></span></legend>
                                        <?php  $data = Manageproduct::GetSizeBarcode($item_define->DesignRef,$color_list->color);
                                        $s=0;
                                        foreach($data as $stock){ 
                                           $s++; 
                                            ?>
                                        <div class="form-group" style="margin-bottom:0">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Size</label>
                                                         <input type="text" size="6" rel="input_size"  name="size<?php echo $s;?>_<?php echo $c;?>" class="form-control clone_field" id="required-input" style="margin-bottom:15px" placeholder="Qty for" value="<?php echo $stock->size;?>" required/>
                                                    </div>
                                                   <div class="col-md-4">
                                                    <label>Stock</label>
                                                     <input type="text" size="6" rel="input_size"  name="input_size<?php echo $s;?>_<?php echo $c;?>" class="form-control clone_field" id="required-input" style="margin-bottom:15px" placeholder="Qty for" value="<?php echo $stock->CurrentStock;?>"/>
                                                   </div>
                                                   <div class="col-md-4">
                                                        <label>Barcode</label>
                                                     <input type="text" size="6" rel="barcode_input_size"  name="barcode_input_size<?php echo $s;?>_<?php echo $c;?>" class="form-control clone_field" id="required-input" style="margin-bottom:15px" placeholder="Barcode for " value="<?php echo $stock->Barcode;?>" readonly/>
                                                   </div>
                                                </div>												
                                        </div>
                                    <?php } session(['numberofavailabesize' => 10]); ?>
                                        <div class="form-group">
                                            <label rel="txtcolorname"  for="txtcolorname" class="clone_field">Color / Style Name</label>
                                            <input class="form-control txtfield clone_field" type="text" rel="txtcolorname" name="txtcolorname_<?php echo $c;?>" value="<?php echo $color_list->color;?>">
                                        </div>
										<div class="form-group">
											<label>Color Thumbnail</label>
											<input id="input-file" type="file" class="file input-file" name="file_colorthm_<?php echo $c;?>" data-browse-on-zone-click="true">
											<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 82 px X Height: 61 px ]</span>  
											<span class="text-danger">{{ $errors->first('filename') }}</span>
										</div>
                                        <!--<div class="form-group">
                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Color Thumbnail</label>
                                            <input style="width:100%" type="file" rel="file_colorthm" name="file_colorthm_<?php echo $c;?>" class="btn btn-info clone_field">
                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 81 Px Height: 62 PX ]</small>
                                        </div> -->
										<div class="form-group">
											<label>Image 1</label>
											<input id="input-file" type="file" class="file input-file" name="file_im1_<?php echo $c;?>" data-browse-on-zone-click="false">
											<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
										</div>
                                        <!--<div class="form-group">
                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 1</label>
                                            <input id="input-file" style="width:100%" type="file" rel="file_im1" name="file_im1_<?php echo $c;?>" class="btn btn-info clone_field">
                                            <small style="color:#06be1c;">[ Image Type: jpeg or jpg, Width: 1600 px X Height: 2400 px ]</small>
                                        </div> -->
										<div class="form-group">
											<label>Image 2</label>
											<input id="input-file" type="file" class="file input-file" name="file_im2_<?php echo $c;?>" data-browse-on-zone-click="true">
											<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
										</div>
                                        <!--<div class="form-group">
                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 2</label>
                                            <input style="width:100%" type="file" rel="file_im2" name="file_im2_<?php echo $c;?>" class="btn btn-info clone_field">
                                        </div> -->
										<div class="form-group">
											<label>Image 3</label>
											<input id="input-file" type="file" class="file input-file" name="file_im3_<?php echo $c;?>" data-browse-on-zone-click="true">
											<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
										</div>
                                       <!-- <div class="form-group">
                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 3</label>
                                            <input style="width:100%" type="file" rel="file_im3" name="file_im3_<?php echo $c;?>" class="btn btn-info clone_field">
                                        </div> --->
										<div class="form-group">
											<label>Image 4</label>
											<input id="input-file" type="file" class="file input-file" name="file_im4_<?php echo $c;?>" data-browse-on-zone-click="true">
											<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
										</div>
                                        <!--<div class="form-group">
                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 4</label>
                                            <input style="width:100%" type="file" rel="file_im4" name="file_im4_<?php echo $c;?>" class="btn btn-info clone_field">
                                        </div> -->
										<div class="form-group">
											<label>Image 5</label>
											<input id="input-file" type="file" class="file input-file" name="file_im5_<?php echo $c;?>" data-browse-on-zone-click="true">
											<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
										</div>
                                        <!--<div class="form-group">
                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 5</label>
                                            <input style="width:100%" type="file" rel="file_im5" name="file_im5_<?php echo $c;?>" class="btn btn-info clone_field">
                                        </div> -->
										<div class="form-group">
											<label>Image 6</label>
											<input id="input-file" type="file" class="file input-file" name="file_im6_<?php echo $c;?>" data-browse-on-zone-click="true">
											<span class="help-block" style="color:#00b161;">[ Image Type: jpeg or jpg, Width: 1300 px X Height: 1667 px ]</span>  
										</div>
                                        <!--<div class="form-group">
                                            <label rel="file_colorthm"  for="file_colorthm" class="clone_field">Image 6</label>
                                            <input style="width:100%" type="file" rel="file_im6" name="file_im6_<?php echo $c;?>" class="btn btn-info clone_field">
                                        </div> -->
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dynamic">
                    </div>
                </div>
                <?php } ?>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <button type="submit" name="btnsubmit" class="submitbtn btn bg-olive btn-flat btn-sm margin tdata float-right"><i class="fa fa-fw fa-save"></i> Add Product</button>
          </div>
          </form>
        </div>
        <!-- /.card -->
        <?php } ?>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
   </section>
    <!-- /.content -->
<script>
     document.getElementById("stylecode").value = "<?php echo $stylecode; ?>";
	(function() {
		document.querySelector('[name=file_colorthm_1]').required = true;
		document.querySelector('[name=file_im1_1]').required = true;
	})();
	function colorClicked(element, i) {
		if (element.checked) {
			document.getElementById(element.name).style.display="block";
			document.querySelector('[name=file_colorthm_'+i+']').required = true;
			document.querySelector('[name=file_im1_'+i+']').required = true;
		} else {
			document.getElementById(element.name).style.display="none";
			document.querySelector('[name=file_colorthm_'+i+']').required = false;
			document.querySelector('[name=file_im1_'+i+']').required = false;
		}
	}
	
$(document).ready(function () {
    $(".input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: false,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
   $("#fabric").change(function(){
		var fabric=$("#fabric").val();
		$(".loading-modal").show();
		var url_op = base_url + "/admin/product/fabric/view?id=" + fabric;
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (data) {
                $(".loading-modal").hide();
                $('#fabric_add').empty();
                $('#fabric_add').val(data.name);
                $('#care').val(data.care);
                
            }
        });
	});
    $("#maincat").change(function () {
        var maincat_id = $("#maincat").val();
         //alert(maincat_id)
        var url_op = base_url + "/ajax/get-procat/" + maincat_id;
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (data) {
                // alert(html);
                // $('#CityList').html(html);
                $('#cat').empty();
                $('#cat').append('<option value="">Select Category</option>');
                $.each(data, function (index, supproobj) {
                    $('#cat').append('<option value="' + supproobj.id + '">' + supproobj.name + '</option>');
                });
            }
        });
    });
    $("#cat").change(function(){
        var procat_id=$("#cat").val();
        //alert(procat_id);
        var url=base_url+"/ajax/get-subcats/"+procat_id;
        $.ajax({
            url:url,
            type:'GET',
            dataType:'json',
            success:function(data){
                $('#subcat').empty();
                $('#subcat').append('<option value="">Select Sub Category</option>');
                $.each(data, function (index, supproobj) {
                    $('#subcat').append('<option value="' + supproobj.id + '">' + supproobj.name + '</option>');
                });
            }
        });
    });
});
</script>

@endsection