<?php

use App\Http\Controllers\admin\product\Managestock;
use App\Http\Controllers\admin\product\Managecolor;
?>
@extends('admin.layouts.app')
@section('title', 'Product List')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Product List</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item">Product</li>
          <li class="breadcrumb-item active">List</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Product List</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        @if (session('update'))
        <div class="callout callout-success">
          <h5><i class="fas fa-info"></i> Note:</h5>
          {{ session('update') }}
        </div>
        @endif
        @if (session('deactive'))
        <div class="callout callout-danger">
          <h5><i class="fas fa-info"></i> Note:</h5>
          {{ session('deactive') }}
        </div>
        @endif
        @if (session('fav'))
        <div class="callout callout-danger">
          <h5><i class="fas fa-info"></i> Note:</h5>
          {{ session('fav') }}
        </div>
        @endif
       <form name="singleform" action="{{url('/admin/product/list')}}" method="GET">
            <div class="form-group">
                <label>Select Category</label>
                <div class="input-group">
                    <select name="scid" id="scid"  class="form-control select2">
                        <option value=""> --- select Category --- </option>
                        <?php
                        foreach ($sub_category as $sub) {
                            ?>
                            <option  value="<?php echo $sub->id; ?>"><?php echo $sub->maincat_name." > " . $sub->cat_name . " > " . $sub->name; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <span class="input-group-append">
                        <button class="btn btn-outline-secondary bg-white border-start-0 border ms-n3" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /.input group -->
            </div>
        </form>   
       <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Style Code</th>
                        <th>Category - Sub Category</th>
                        <th style="text-align:center;">Image</th>
                        <th style="text-align:center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($product_list as $product) {
                        $i++;
                        ?>
                        <tr>
                            <td style="width:1%;text-align:center;"><?php echo $i; ?></td>
                            <td style="width:15%;"><?php echo $product->product_name; ?></td>
                            <td style="width:15%;"><?php echo $product->product_styleref; ?></td>
                            <td style="width:15%;"><?php echo $product->procat_name . " - " . $product->subprocat_name." - ".$product->subcat_name; ?></td>
                            <td style="text-align:center;">
                                <img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_tiny; ?>" alt=""  />
                            </td>
                            <td style="text-align:center;">
                                <a class="btn bg-info btn-flat btn-sm margin tdata" href="{{url("admin/product/edit?id={$product->id}")}}">Edit</a> 
                                <a class="btn bg-info btn-flat btn-sm margin tdata" href="{{url("admin/product/album/view?id={$product->id}")}}">Manage Product Gallery</a> 
                                <a class="btn bg-info btn-flat btn-sm margin tdata" href="{{url("admin/product/stock/view?id={$product->id}")}}">Manage Stock</a> 
                                <!--<a class="btn bg-info btn-flat btn-sm margin tdata" href="{{url("/add-product-style/{$product->id}")}}">Add More</a>-->
                                <!--<a class="btn bg-info btn-flat btn-sm margin tdata" href="{{url("/add-related-product/{$product->id}")}}">Add Related Product</a>-->
                                <?php if($product->product_active_deactive == 0){ ?>
                                <a class="btn btn-success btn-flat btn-sm tdata" onclick="return confirm('Are you sure you want to deactive this product?');" href='{{url("admin/product/deactive?id=$product->id")}}' alt="Deactive" title="Deactive">Active</a>
                                <?php }else{ ?>
                                  <a class="btn btn-danger  btn-flat btn-sm tdata" onclick="return confirm('Are you sure you want to active this product?');" href='{{url("admin/product/active?id=$product->id")}}'  alt="Active" title="Active">Deactive</a>
                                <?php } ?>
                                <a class="btn btn-danger btn-flat btn-sm margin tdata" onclick="return confirm('Are you sure you want to delete this product?');" href='{{url("admin/product/delete?id=$product->id")}}' >Delete</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Style Code</th>
                        <th>Category - Sub Category</th>
                        <th style="text-align:center;">Image</th>
                        <th style="text-align:center;">Action</th>
                    </tr>
                </tfoot> 
            </table>
          </div>
       
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
  <!-- Ajax modal ---->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to delete this product ?</h4>
            </div>
            <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                <a href="#" class="btn btn-sm btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--- Ajax modal end ---->
</section>
<script>
    document.getElementById("scid").value = "<?php echo $sub_pro_selected; ?>";
</script>
<script>
    $(document).ready(function () {
        $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
            var data_id = '';
            if (typeof $(this).data('id') !== 'undefined') {
        
              data_id = $(this).data('id');
            }
         //   alert(data_id);
            
        var total_loop = $("#total_loop").val();
       // alert(total_loop);
        for (var i = 1; i <= total_loop; i++) {
            $('#qty' + '_' + data_id + '_'+ i+', #barcode' + '_' + data_id + '_'+ i).on('blur', function () {
               // alert('ok');
                var rownowithid = $(this).attr('id');
                var rowno = rownowithid.split('_');
              //  alert(rowno[2]);
                var qty = $('#qty' + '_' + data_id + '_' + rowno[2]).val();
                var prosize_id = $('#prosize' + '_' + data_id + '_' + rowno[2]).val();
                var barcode = $('#barcode' + '_' + data_id + '_' + rowno[2]).val();
                //alert(barcode);
                var url_op = base_url + "/update-quantity-save-by-ajax/" + prosize_id + '/' + qty + '/' + barcode;
                $.ajax({
                    url: url_op,
                    type: 'GET',
                    success: function (html) {
                    //   alert(html);
                    }
                });
            });
        }
        $('#update_qty' + '_' + data_id).on('click',function(){
			alert("Quantity Add Successfully!");
		});
     });
     
    });
</script>
@endsection