@extends('admin.layouts.app')
@section('title', 'Manage Fabric Composition')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manage Fabric Composition</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Fabric Composition</li>
              <li class="breadcrumb-item active">Fabric Composition</li>
              <li class="breadcrumb-item active">Create</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Fabric Composition</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          
          <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  <button type="button" class="btn btn-tool update_fabric_loading d-none" data-card-widget="card-refresh" data-source="{{url('/')}}" data-source-selector="#card-refresh-content">
                    <i class="fas fa-sync-alt"></i>
                  </button>
                <form name="update_fabric" id="fabric-update" action="{{url('admin/product/fabric/update')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" name="id" id="id" value="{{$fabric->id}}" required />
				<fieldset style="margin:10px;padding:5px 20px; border:2px solid #00c0ef44">
    				<legend style="padding:5px 20px; text-align:center; width:auto">Fabric Composition Create</legend>
    			       @if (session('save'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('save') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                        <div id="message"></div>
    					<div class="form-group">
        					<label for="name">Fabric Composition</label>
        					<input type="text" class="form-control" name="name" id="name" value="{{$fabric->name}}" required />
        					<span class="help-text"></span></td>
        				</div>
        
        				<div class="form-group">
        					<label for="txtproductdetails">Care Description</label>
        					<textarea class="form-control" type="text" name="care" id="care" rows="3" cols="50" required>{{$fabric->care}}</textarea>
        				</div>
    					<div class="box-footer">
    						<button type="submit" name="btnsubmit"  class="btn btn-flat bg-navy pull-right">Save Fabric Composition</button>
    					</div>
					</fieldset>
					</form>
				</div>
			</div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
    <script>
        $(document).ready(function ($) {
            $("#update-fabric").click(function(e){
                 e.preventDefault();
                 $(".update_fabric_loading").removeClass("d-none");
                 var datastring = $("#fabric-update").serialize();
                 //alert(datastring);
                 var url = base_url + "/admin/product/fabric/update";
                  $.ajax({
                        url:url,
        				type:'POST',
        				data:datastring,
        				success:function(result){
        				    $(".update_fabric_loading").addClass("d-none");
        				    //$(".update_fabric_loading").hide();
        				    if(result==1){
        				        $("#message").html("<p style='color:green;'>Update successfully.</p>")
        				        // setTimeout(function(){
                    //                 window.location.href = base_url+'/admin/product/fabric/add';
                    //              }, 5000);
        				    }
        				}
                  });
            });
        });
    </script>
@endsection