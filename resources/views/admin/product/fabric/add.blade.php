@extends('admin.layouts.app')
@section('title', 'Manage Fabric Composition')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manage Fabric Composition</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Fabric Composition</li>
              <li class="breadcrumb-item active">Fabric Composition</li>
              <li class="breadcrumb-item active">Create</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Fabric Composition</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          
          <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <form name="add_fabric" id="fabric-add" action="{{url('admin/product/fabric/save')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
				<fieldset style="margin:10px;padding:5px 20px; border:2px solid #00c0ef44">
    				<legend style="padding:5px 20px; text-align:center; width:auto">Fabric Composition Create</legend>
    			       @if (session('save'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('save') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                        <div id="message"></div>
    					<div class="form-group">
        					<label for="name">Fabric Composition</label>
        					<input type="text" class="form-control" name="name" id="name" required />
        					<span class="help-text"></span></td>
        				</div>
        
        				<div class="form-group">
        					<label for="txtproductdetails">Care Description</label>
        					<textarea class="form-control" type="text" name="care" id="care" rows="3" cols="50" required></textarea>
        				</div>
    					<div class="box-footer">
    						<button type="submit" name="btnsubmit" class="btn btn-flat bg-navy pull-right">Save Fabric Composition</button>
    					</div>
					</fieldset>
					</form>
				</div>
			</div>
			
			<div class="pb-5">
				<div class="co-12">
                        <!-- /.box-header -->
                             @if (session('update'))
                                <div class="callout callout-success">
                                  <h5><i class="fas fa-info"></i> Note:</h5>
                                  {{ session('update') }}
                                </div>
                                @endif
                                @if (session('error'))
                                <div class="callout callout-danger">
                                  <h5><i class="fas fa-info"></i> Note:</h5>
                                  {{ session('error') }}
                                </div>
                                @endif
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Fabric Composition</th>
                                            <th>Care Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($fabrics as $fabric)
                                           <tr>
                                               <td>{{$fabric->id}}</td>
                                               <td>{{$fabric->name}}</td>
                                               <td>{{$fabric->care}}</td>
                                               <td width=15%>
                                                   <a class="btn bg-info btn-flat btn-sm margin tdata" href='{{url("admin/product/fabric/edit?id=$fabric->id")}}'>Edit</a> 
                                                   <?php if($fabric->active==1){?>
                                                     <a class="btn btn-success btn-flat btn-sm tdata" href='{{url("admin/product/fabric/deactive?id=$fabric->id")}}' alt="Deactive" >Active</a> 
                                                   <?php }else{ ?>
                                                     <a class="btn btn-danger  btn-flat btn-sm tdata" href='{{url("admin/product/fabric/active?id=$fabric->id")}}' alt="Active" >Deactive</a> 
                                                   <?php } ?>
                                                   <a class="btn btn-danger btn-flat btn-sm margin tdata" onclick="return confirm('Are you sure you want to delete this fabric?');" href='{{url("admin/product/fabric/delete?id=$fabric->id")}}' >Delete</a>
                                               </td>
                                           </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
                        <!-- /.box-body -->
                    <!-- /.box -->
                </div>
			</div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
    <script>
        $(document).ready(function ($) {
            $("#add-fabric").click(function(e){
                 e.preventDefault();
                 $(".loading-modal").show();
                 var datastring = $("#fabric-add").serialize();
                 //alert(datastring);
                 var url = base_url + "/admin/product/fabric/save";
                  $.ajax({
                        url:url,
        				type:'POST',
        				data:datastring,
        				success:function(result){
        				    $(".loading-modal").hide();
        				    if(result==1){
        				        $('#fabric_name').val('');
        				        $('#care').val('');
        				        $("#message").html("<p style='color:green;'>Create successfully.</p>")
        				        setTimeout(function() {
                                    location.reload();
                                }, 5000);
        				    }
        				}
                  });
            });
        });
    </script>
@endsection