@extends('admin.layouts.app')
@section('title', $product->product_styleref)
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Product</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item">Product</li>
          <li class="breadcrumb-item">List</li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
<form name="add_product" id="myform" action="{{url('/admin/product/update')}}" method="post" enctype="multipart/form-data">
     {{ csrf_field() }}
    <input type="hidden" class="form-control" name="id" value="{{$product->id}}" required />
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Item Definitions Update for '{{$product->product_styleref}}'</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">&nbsp;</div>
            <div class="col-md-6">
                @if (session('update'))
                <div class="callout callout-success">
                  <h5><i class="fas fa-info"></i> Note:</h5>
                  {{ session('update') }}
                </div>
                @endif
                @if (session('error'))
                <div class="callout callout-danger">
                  <h5><i class="fas fa-info"></i> Note:</h5>
                  {{ session('error') }}
                </div>
                @endif
    			<fieldset style="margin:10px; padding:5px 20px; border:2px solid #00c0ef44;width:100%">
    				<legend style="padding:5px 20px; text-align:center; width:auto">Item Definitions for '{{$product->product_styleref}}'</legend>
    				<div class="form-group">
    					<label for="ddlprocat">Parent Category</label>
    					<select type="text" class="form-control select2"  name="maincat" id="maincat"  required>
    						<option value="">-- select parent category --</option>
    						@foreach($maincats as $maincat)
    						<option value="{{$maincat->id}}">{{$maincat->name}}</option>
    						@endforeach
    					</select>
    				</div>
    				<div class="form-group row">
    					<div class="col-sm-6">
    					    <label for="cat">Main Category</label>
    					    <select type="text" class="form-control select2"  name="cat" id="cat"  required>
    						<option value="">-- select category --</option>
    						@foreach($cats as $procat)
    						<option value="{{$procat->id}}">{{$procat->name}}</option>
    						@endforeach
    					</select>
    					</div>
    					<div class="col-sm-6">
    					    <label for="subcat">Sub Category</label>
    						<select class="form-control select2" type="text" name="subcat" id="subcat" required>
    							<option value="">-- select sub category --</option>
    							@foreach($subcats as $subprocat)
        						<option value="{{$subprocat->id}}">{{$subprocat->name}}</option>
        						@endforeach
    						</select>
    					</div>
    				</div>
    				<div class="form-group row">
    				    <div class="col-sm-6">
    					    <label for="txtstyleref">Product Code</label>
    					    <input class="form-control" type="text" id="required-input" name="txtstyleref" id="txtstyleref" value="{{ $product->product_styleref }}" required>
    					    <span class="help-text">{{ $errors->first('txtstyleref') }}</span>
    					</div>
    					<div class="col-sm-6">
    					    <label for="txtproductname">Product Name</label>
    				    	<input class="form-control" type="text" name="txtproductname" id="txtproductname" value="{{ $product->product_name }}" required>
    					    <span class="help-text">{{ $errors->first('txtproductname') }}</span>
    					</div>
    				</div>
    				<div class="form-group">
    					<label for="txtprice">Price</label>
    					<input class="form-control" type="number"  name="txtprice" id="in-range-input" min="1" max="2000000" value="{{ $product->product_price }}"  required>
    					<span class="help-text">{{ $errors->first('txtprice') }}</span>
    				</div>
    				
    				<div class="form-group">
    					<label for="txtpricediscounted">Discount Percentage</label>
    					<input class="form-control" type="number" name="txtpricediscounted" id="optional-input" min="0" max="200" value="{{ $product->product_pricediscounted }}" />
    					<span class="help-text"></span>
    				</div>
    				<div class="form-group">
    					<label for="check-option-1">Collection</label>
    					<?php foreach($campaigns as $campaign){ ?>
                           <div style="padding-left:10px"><input type="radio" name="campaign" id="check-option-1" value="{{$campaign->id}}" <?php if($product->isSpecial==$campaign->id){ echo 'checked';}?> style="width:16px;"/> {{$campaign->name}}</div>
                        <?php } ?>
    					<span class="help-text"></span>
    				</div>
    				<div class="form-group">
    					<label for="check-option-1">Occassion</label>
                           <div style="padding-left:10px"><input type="radio" name="occassion" id="check-option-1" value="12" <?php if($product->spcollection==12){ echo 'checked';}?> style="width:16px;"/> Festive Wear</div>
                           <div style="padding-left:10px"><input type="radio" name="occassion" id="check-option-1" value="13" <?php if($product->spcollection==13){ echo 'checked';}?> style="width:16px;"/> Office Wear</div>
                           <div style="padding-left:10px"><input type="radio" name="occassion" id="check-option-1" value="14" <?php if($product->spcollection==14){ echo 'checked';}?> style="width:16px;"/> Casual Wear</div>
    					<span class="help-text"></span>
    				</div>
    				<div class="form-group">
    					<label for="txtproductdetails">Discription</label>
    					<textarea class="form-control" type="text" name="txtproductdetails" id="optional-input" rows="3">{{$product->product_description}}</textarea>
    					<span class="help-text"></span>
    				</div>
					<div class="form-group">
						<label for="fabric">Fabric Composition <a href="{{url('/admin/fabric/add')}}" target="blank" class="myModal-item glyphicon glyphicon-plus-sign"></a></label>
						<textarea class="form-control"  type="text" name="fabric" id="optional-input" rows="3">{{$product->fabric}}</textarea> 
					</div>
					<div class="form-group">
    					<label for="care">Care Description</label>
    					<textarea class="form-control" type="text" name="txtproductcare" id="care" rows="3" cols="50" required>{{$product->product_care}}</textarea>
    				</div>
    			</fieldset>
            </div>
            <div class="col-md-3">&nbsp;</div>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> <i class="fas fa-edit"></i> Update Product</button>
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->
</form>
</section>
<!-- /.content -->
<script>
document.getElementById("maincat").value = "<?php echo $product->maincat; ?>";
document.getElementById("cat").value = "<?php echo $product->cat; ?>";
document.getElementById("subcat").value = "<?php echo $product->subcat; ?>";
$(document).ready(function () {
    $("#maincat_id").change(function () {
        var maincat_id = $("#maincat_id").val();
         //alert(maincat_id)
        var url_op = base_url + "/ajax/get-procat/" + maincat_id;
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (data) {
                // alert(html);
                // $('#CityList').html(html);
                $('#procat_id').empty();
                $('#procat_id').append('<option value="">Select Category</option>');
                $.each(data, function (index, supproobj) {
                    $('#procat_id').append('<option value="' + supproobj.id + '">' + supproobj.name + '</option>');
                });
            }
        });
    });
    $("#procat_id").change(function(){
        var procat_id=$("#procat_id").val();
        //alert(procat_id);
        var url=base_url+"/ajax/get-subcats/"+procat_id;
        $.ajax({
            url:url,
            type:'GET',
            dataType:'json',
            success:function(data){
                $('#subcat_id').empty();
                $('#subcat_id').append('<option value="">Select Sub Category</option>');
                $.each(data, function (index, supproobj) {
                    $('#subcat_id').append('<option value="' + supproobj.id + '">' + supproobj.name + '</option>');
                });
            }
        });
    });
});
</script>
@endsection