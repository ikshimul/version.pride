<?php

use App\Http\Controllers\admin\product\Managestock;
use App\Http\Controllers\admin\product\Managecolor;
?>
@extends('admin.layouts.app')
@section('title', $product->product_styleref)
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Product Stock</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item">Product</li>
          <li class="breadcrumb-item">List</li>
          <li class="breadcrumb-item active">Stock</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">{{$product->product_styleref}}</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <input type="hidden" name="productid" value="<?php echo $product->id; ?>" />
                <input type="hidden" name="productstyle" value="<?php echo $product->product_styleref; ?>" />
                <!--  <div class="alert alert-info alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-info"></i></h4>
                      <p>Product Name</p>
                  </div> ---->
                  <center><img class="thumbnail  img-responsive" src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->product_img_thm; ?>" alt=""  height="200" width="160" /></center>
                <table class="table table-striped">
                    <tr>
                        <th>Product Details</th>
                        <th>&nbsp;</th>
                    </tr>
                    <tr>
                        <td>Product Name : </td>
                        <td><?php echo $product->product_name; ?></td>
                    </tr>
                    <tr>
                        <td>Product Code : </td>
                        <td><?php echo $product->product_styleref; ?></td>
                    </tr>
                    <tr>
                        <td>Product Price : </td>
                        <td><?php echo $product->product_price; ?></td>
                    </tr>
                    <tr>
                        <td>Product Maincat : </td>
                        <td> <?php echo $product_maincat->name; ?></td>
                    </tr>
                    <tr>
                        <td>Product Category : </td>
                        <td> 
                          <?php echo $product_procat->name; ?>
                          <input type="hidden" value="<?php echo $product_procat->id; ?>" id="procat_id"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Product Sub Category : </td>
                        <td><?php echo $product_subprocat->name; ?></td>
                    </tr>
                </table>
                <div class="row">
                    <div class="col-md-12">
                        @if (session('update'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('update') }}
                        </div>
                        @endif
                        @if (session('deactive'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('deactive') }}
                        </div>
                        @endif
                      <form action="{{url('admin/product/stock/update')}}" method="post">
                        {{ csrf_field() }}
                       <fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
                         <legend style="padding:5px 20px; text-align:center; width:auto">Update Quantity</legend>
                        <table class="table table-condensed">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>[Color] / [Size]</th>
                                <th>Barcode</th>
                                <th style="width: 10%px">Quantity</th>
                                
                                <th></th>
                            </tr>
                            <?php
                            $i = 0;
                           $size_qty = Managestock::GetSizeQty($product->id);
                            foreach ($size_qty as $prosize) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?>.</td>
                                    <td><?php echo '[' . $prosize->color_name . ']' . ' / [' . $prosize->productsize_size . ']'; ?></td>
                                    <td>
                                        <input class="form-control" type="text" name="barcode[]" value="{{ $prosize->barcode }}" placeholder="Barcode" required/>
                                    </td>
                                    <td>
                                        <input class="form-control" type="number" name="qty[]"  value="<?php echo $prosize->SizeWiseQty; ?>">
                                        <input class="form-control" type="hidden" name="id[]" value="<?php echo $prosize->id; ?>">
                                    </td>
                                    <td>
                                        <a class="btn btn-danger btn-flat btn-sm tdata" href='{{url("admin/product/stock/delete/{$prosize->id}")}}' >Delete</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        <input type="hidden" id="total_loop" value="<?php echo $i; ?>">
                        </fieldset>
                        <div class="box-footer">
                            <div class="col-sm-12 text-right">
                                    <button type="submit"  class="btn bg-navy btn-flat margin" >Update Quantity </button>
                            </div>
                        </div>
                      </form>
                    </div>
                    <div class="col-md-12">
                        @if (session('save'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('save') }}
                        </div>
                        @endif
                        <fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
                         <legend style="padding:5px 20px; text-align:center; width:auto">Add New Size and Quantity</legend>
                        <table class="table table-condensed">
                            <tr>
                                <th>Color</th>
                                <th>Size</th>
                                <th style="width: 10%px">Quantity</th>
                                <th>Barcode</th>
                                <th>&nbsp;</th>
                            </tr>
                            <?php
                            $j = 0;
                            $color_wise_prosize = Managestock::GetColorProSize($product->id);
                            foreach ($color_wise_prosize as $color) {
                                $j++;
                               // $prosize = Managestock::GetExpectSize($color->product_id, $color->color_name);
                                ?>
                                <form action="{{url('/admin/product/stock/add')}}" method="post">
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                    <input type="hidden" name="product_id" value="<?php echo $product->id; ?>" />
                                    <tr>
                                        <td>
                                            <input type="hidden" name="color_name" value=" <?php echo $color->color_name; ?>" />
                                            <?php echo $color->color_name; ?>
                                        </td>
                                        <td>
                                            <select name="productsize_size" class="form-control" id="sizelist" required>
                                                <option value="">Select size</option>
                                                <?php foreach ($prosizes as $sizes) { ?>
                                                    <option value="<?php echo $sizes->prosize_name; ?>"><?php echo $sizes->prosize_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>

                                        <td>
                                            <input class="form-control" type="number" name="SizeWiseQty" placeholder="0"  required/>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" name="barcode" placeholder="Barcode" required/>
                                        </td>
                                        <td>
                                            <button type="submit"  class="btn bg-navy btn-flat margin" >Add New Size</button>
                                        </td>
                                    </tr>
                                </form>
                            <?php } ?>
                        </table>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</section>
<script>
    $(document).ready(function () {
        var procat_id=$("#procat_id").val();
        // var url=base_url+"/ajax/get-size-list/"+procat_id;
        //   $.ajax({
        //         url:url,
        //         type:'GET',
        //         success:function(data){
        //             console.log(data);
        //             $('#sizelist').empty();
        //             $('#sizelist').append('<option value="">Select Size</option>');
        //             $.each(data, function (index, supproobj) {
        //                 $('#sizelist').append('<option value="' + supproobj.prosize_name + '">' + supproobj.prosize_name + '</option>');
        //             });
        //         }
        //   });
    });
</script>
@endsection