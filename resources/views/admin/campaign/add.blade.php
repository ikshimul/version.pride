@extends('admin.layouts.app')
@section('title','Add Collection')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Collection</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item">Collection</li>
          <li class="breadcrumb-item active">Add</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Add Collection</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <form name="add_subpro" action="{{url('/admin/campaign/save')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
			<fieldset style="margin:10px; padding:5px 20px; border:2px solid #00c0ef44">
			<legend style="padding:5px 20px; text-align:center; width:auto">Add Collection</legend>
			    @if (session('save'))
                <div class="callout callout-info">
                    <h5><i class="fas fa-info"></i> Note:</h5>
                    {{ session('save') }}
                </div>
                @endif
                @if (session('error'))
                <div class="callout callout-danger">
                    <h5><i class="fas fa-info"></i> Note:</h5>
                    {{ session('error') }}
                </div>
                @endif
                <div class="form-group">
                    <label>Main Category</label>
                    <select name="category" class="form-control select2" style="width: 100%;">
                        <option value=""> ---- Select Main Category ---- </option>
                        <option value="1"> Women </option>
                        <option value="2"> Men </option>
                        <option value="3"> Kids </option>
                        <option value="4"> Accessories </option>
                        <option value="5"> Homes </option>
                        <option value="6"> All </option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name"/>
                </div>
                <div class="form-group">
                    <label>Slug Name</label>
                    <input type="text" class="form-control" name="slug"/>
                </div>
                <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" class="form-control" name="starts_at" />
                    <span class="help-block" style="color:#f39c12;">Campaign start date</span>
                </div>
                <div class="form-group">
                    <label>Start Date</label>
                    <input type="date" class="form-control" name="expires_at" />
                    <span class="help-block" style="color:#f39c12;">Campaign end date</span>
                </div>
				<div class="box-footer">
				     <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-left"> <i class="fas fa-save"></i> Add Campaign</button>
				</div>
				</fieldset>
				</form>
			</div>
		</div>
		
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</section>
<!-- /.content -->
@endsection