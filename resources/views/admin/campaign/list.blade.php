@extends('admin.layouts.app')
@section('title', 'Manage Campaign')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Manage Campaign</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item">Campaign</li>
          <li class="breadcrumb-item active">Manage</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Manage Campaign</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
		<div class="pb-5">
			<div class="co-12">
                    <!-- /.box-header -->
                     @if (session('update'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('update') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Category</th>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Created By</th>
                                        <th style="text-align:center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($list as $campaign)
                                    <tr>
                                        <td style="width:2%;">{{$campaign->id}}</td>
                                        <td style="width:2%;"><?php if($campaign->category=='6'){echo 'All';}else{echo $campaign->category_name;}?></td>
                                        <td>{{$campaign->name}}</td>
                                        <td>{{$campaign->slug}}</td>
                                        <td><?php
                                            $up_date = strtotime($campaign->starts_at);
                                            echo date('M d, Y h:m a', $up_date);
                                            ?></td>
                                        <td><?php
                                            $up_date = strtotime($campaign->expires_at);
                                            echo date('M d, Y h:m a', $up_date);
                                            ?></td>
                                        <td>{{$campaign->admin_name}}</td>
                                        <td><a class="btn btn-info btn-flat btn-sm margin tdata" href="{{url("/admin/campaign/edit/{$campaign->id}")}}">Edit</a>
                                        <a class="btn btn-danger btn-flat btn-sm margin tdata" onclick="return confirm('Are you sure you want to delete this item?');" href="{{url("/admin/campaign/delete/{$campaign->id}")}}">delete</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div> 
                    <!-- /.box-body -->
                <!-- /.box -->
            </div>
		</div>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</section>
<!-- /.content -->
@endsection