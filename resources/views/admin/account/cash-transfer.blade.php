@extends('admin.layouts.app')
@section('title','Pride Limited | Cash Transfer')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Slider</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Order Section</a></li>
          <li class="breadcrumb-item"><a href="#">Cashbook</a></li>
          <li class="breadcrumb-item active">Cash Transfer</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Cash Transfer</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
       <form name="add_subpro" action="{{url('admin/accounts/cash/transfer')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                         @if (session('save'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('save') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                        @php($balance=0)
                        @php($balance_show=0)
                        @foreach($cash_history as $cash)
                        <?php 
                            if($cash->rider_id == 4){
                                $balance -=$cash->expense_amount;
                            }elseif($cash->rider_id == 3){
                                
                            }else if($cash->rider_id == 5){
                                
                            }else if($cash->payment_method == 'ssl'){
                                
                            }else if($cash->payment_method == 'bKash'){
                                
                            }else{
                                if ($cash->transaction_type === 'cash_out')
                                    $balance -= ($cash->transation_amount - $cash->expense_amount);
                                else
                                    $balance += ($cash->transation_amount - $cash->expense_amount);
                              }
                            ?>
                        @endforeach
                        <div class="form-group {{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label>Amount</label>
                            <input type="hidden" name="available_amount" class="form-control" value="<?php echo $balance;?>" required>
                            <input type="number" class="form-control" name="amount" value="{{old('amount')}}" max="{{$balance}}" min="1000" />
                            <span class="help-block" style="color:#06be1c">[<strong> Available amount <?php echo number_format($balance);?> Tk </strong>]</span>  
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('amount') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('transfer_to') ? ' has-error' : '' }}">
                            <label>Transfer to</label>
                            <input type="text" class="form-control" name="transfer_to" value="{{old('transfer_to')}}"/>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('transfer_to') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                            <label>Description</label>
                            <textarea type="text" class="form-control" name="description" rows="3"></textarea>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('description') }}</div>
                        </div>
                        <div class="form-group">
                             <label for="customFile">Voucher Image</label> 
                            <div class="custom-file">
                              <input type="file" class="file" name="voucher_image" id="input-file"  data-browse-on-zone-click="true">
                              <span class="help-block" style="color:#06be1c">only .jpg image is allowed</span>  
                              <div class="help-block with-errors invalid-feedback">{{ $errors->first('voucher_image') }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Save Cash Transfer" />
                </div>
            </div>
        </form>    
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
