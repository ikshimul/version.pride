@extends('admin.layouts.app')
@section('title', 'Cashbook')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Cashbook</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item">Order Section</li>
          <li class="breadcrumb-item active">Cashbook</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Manage Cashbook</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
		<div class="pb-5">
			<div class="co-12">
                    <!-- /.box-header -->
                     @if (session('update'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('update') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table id="" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Order No#</th>
                                        <th>Customer Name</th>
                                        <th>Delivery</th>
                                        <th>Payment</th>
                                        <th>Invoice Amount</th>
                                        <th>Expenses Amount</th>
                                        <th>Total</th>
                                        <th>Balance</th>
                                        <th style="text-align:center;">Issue By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    $total_receive=0;
                                    $total_expanse=0;
                                    $total_transation=0;
                                    ?>
                                    @php($balance=0)
                                    @php($balance_show=0)
                                    @foreach($cash_history as $cash)
                                    <tr>
                                        <td width="5%">{{$i++}}</td>
                                        <td>
                                            <?php 
                                                $order_date=strtotime($cash->created_at); 
                                                echo date('d M , Y',$order_date);
                                            ?>
                                        </td>
                                        <td><span class="badge badge-success"><?php if($cash->transaction_type === 'cash_in'){ echo 'Credited';}?></span></td>
                                       <?php if($cash->transaction_type === 'cash_out'){ ?>
                                        <td>{{ $cash->description }}</td>
                                        <td></td>
                                        <td></td>
                                       <?php }else{ ?>
                                        <td>
                                            <a href='{{url("admin/order/invoice?id={$cash->id}")}}' target="__self" class="info">{{$cash->conforder_tracknumber}}</a>
                                        </td>
                                        <td> {{ $cash->Shipping_txtfirstname  }} {{ $cash->Shipping_txtlastname }} </td>
                                        <td>{{$cash->delivery_by}}</td>
                                        <?php } ?>
                                        <td>
                                            <?php
                                            if ($cash->payment_method == 'cDelivery') {
                                                echo 'Cash on Delivery';
                                            } else if ($cash->payment_method == 'ssl') {
                                                echo 'Bank';
                                            } elseif ($cash->payment_method == 'bKash') {
                                                echo 'bKash';
                                            } else {
                                                echo $cash->payment_method;
                                            }
                                            ?>
                                        </td>
                                        <td>
                                             {{ number_format($cash->transation_amount) }}
                                             <?php 
                                             if ($cash->transaction_type != 'cash_out'){
                                             $total_transation+=$cash->transation_amount;
                                             }
                                             ?>
                                        </td>
                                        <td>
                                             {{ number_format($cash->expense_amount) }}
                                             <?php $total_expanse+=$cash->expense_amount;?>
                                        </td>
                                        <td>
                                            {{ $cash->transation_amount - $cash->expense_amount }}
                                            <?php $total_receive+=$cash->transation_amount - $cash->expense_amount;?>
                                        </td>
                                        <td>
                                            <?php 
                                            if($cash->rider_id == 4){
                                                $balance -=$cash->expense_amount;
                                            }elseif($cash->rider_id == 3){
                                                
                                            }else if($cash->rider_id == 5){
                                                
                                            }else if($cash->payment_method == 'ssl'){
                                                
                                            }else if($cash->payment_method == 'bKash'){
                                                
                                            }else{
                                                if ($cash->transaction_type === 'cash_out')
                                                    $balance -= ($cash->transation_amount - $cash->expense_amount);
                                                else
                                                    $balance += ($cash->transation_amount - $cash->expense_amount);
                                                    $balance_show = ($cash->transation_amount - $cash->expense_amount);
                                              }
                                            ?>
                                            {{ number_format($balance_show) }}
                                        </td>
                                        
                                        <td>{{ $cash->admin_username }}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="font-weight: bold">Total</td>
                                        <td style="font-weight: bold">{{ number_format($total_transation) }}</td>
                                        <td style="font-weight: bold">{{ number_format($total_expanse) }}</td>
                                        <td style="font-weight: bold">{{ number_format($total_receive) }}</td>
                                        <td style="font-weight: bold">{{ number_format($balance) }}</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    <!-- /.box-body -->
                <!-- /.box -->
            </div>
		</div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
       <a href='{{url("admin/accounts/report")}}' name="btnsubmit" class="submitbtn btn btn-primary float-left"> <i class="fas fa-download"></i> Generate PDF</a>
    </div>
  </div>
  <!-- /.card -->
</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#cash-report").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,"ordering": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
     }).buttons().container().appendTo('#cash-report_wrapper .col-md-6:eq(0)');
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection