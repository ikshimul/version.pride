<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cash Book Report</title>
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
</head>
<style>
   body {
        font-family: 'PT Sans Narrow', sans-serif; 
    }
    table {
      border-collapse: collapse;
    }
    table, th, td {
      font-family: 'PT Sans Narrow', sans-serif; 
      border: 1px solid #eee;
    }
    th {
    font-family: 'PT Sans Narrow', sans-serif; 
    }
    .heading{
       font-weight:500;
       font-family: 'PT Sans Narrow', sans-serif;  
    }
</style>
<body>
    <h1 class="heading">Cashbook Report</h1>
    <table class="table table-bordered table-striped" style="font-family: 'PT Sans Narrow', sans-serif !important; font-size:12px;">
        <thead>
            <tr style="'PT Sans Narrow', sans-serif;">
                <th>SL</th>
                <th>Date</th>
                <th>Type</th>
                <th>Order No#</th>
                <th>Customer Name</th>
                <th>Delivery</th>
                <th>Payment</th>
                <th>Invoice Amount</th>
                <th>Expenses Amount</th>
                <th>Total</th>
                <th>Balance</th>
                <th style="text-align:center;">Issue By</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            $total_receive=0;
            $total_expanse=0;
            $total_transation=0;
            ?>
            @php($balance=0)
            @foreach($cash_history as $cash)
            <tr>
                <td width="5%">{{$i++}}</td>
                <td>
                    <?php 
                        $order_date=strtotime($cash->created_at); 
                        echo date('d M , Y',$order_date);
                    ?>
                </td>
                <td><span class="badge badge-success"><?php if($cash->transaction_type === 'cash_in'){ echo 'Credited';}?></span></td>
               <?php if($cash->transaction_type === 'cash_out'){ ?>
                <td>{{ $cash->description }}</td>
                <td></td>
                <td></td>
               <?php }else{ ?>
                <td>
                    {{$cash->conforder_tracknumber}}
                </td>
                <td> {{ $cash->Shipping_txtfirstname  }} {{ $cash->Shipping_txtlastname }} </td>
                <td>{{$cash->delivery_by}}</td>
                <?php } ?>
                <td>
                    <?php
                    if ($cash->payment_method == 'cDelivery') {
                        echo 'Cash on Delivery';
                    } else if ($cash->payment_method == 'ssl') {
                        echo 'Bank';
                    } elseif ($cash->payment_method == 'bKash') {
                        echo 'bKash';
                    } else {
                        echo $cash->payment_method;
                    }
                    ?>
                </td>
                <td>
                     {{ number_format($cash->transation_amount) }}
                     <?php $total_transation+=$cash->transation_amount;?>
                </td>
                <td>
                     {{ number_format($cash->expense_amount) }}
                     <?php $total_expanse+=$cash->expense_amount;?>
                </td>
                <td>
                    {{ $cash->transation_amount - $cash->expense_amount }}
                    <?php $total_receive+=$cash->transation_amount - $cash->expense_amount;?>
                </td>
                <td>
                    <?php 
                    if($cash->rider_id == 4){
                        $balance -=$cash->expense_amount;
                    }elseif($cash->rider_id == 3){
                        
                    }else if($cash->rider_id == 5){
                        
                    }else if($cash->payment_method == 'ssl'){
                        
                    }else if($cash->payment_method == 'bKash'){
                        
                    }else{
                        if ($cash->transaction_type === 'cash_out')
                            $balance -= ($cash->transation_amount - $cash->expense_amount);
                        else
                            $balance += ($cash->transation_amount - $cash->expense_amount);
                      }
                    ?>
                    {{ number_format($balance) }}
                </td>
                
                <td>{{ $cash->admin_username }}</td>
            </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-weight: bold">Total</td>
                <td style="font-weight: bold">{{ number_format($total_transation) }}</td>
                <td style="font-weight: bold">{{ number_format($total_expanse) }}</td>
                <td style="font-weight: bold">{{ number_format($total_receive) }}</td>
                <td style="font-weight: bold">{{ number_format($balance) }}</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</body>
</html>