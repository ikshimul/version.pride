@extends('admin.layouts.app')
@section('title','Pride Limited | Admin Profile')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
    .description-block>.description-text {
        text-transform: none;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Profile</h1>
      </div>
      <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Profile</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="row">
          <div class="col-md-4">
            <!-- Widget: user widget style 1 -->
            <div class="card card-widget widget-user shadow">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-info">
                <h3 class="widget-user-username">{{$admin->name}}</h3>
                <h5 class="widget-user-desc">{{$admin->admin_type}}</h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{ URL::to('') }}/storage/app/public/admin-user/{{ $admin->image }}" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header"><i class="fas fa-mobile margin-r-5"></i></h5>
                      <span class="description-text">{{$admin->mobile}}</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header"><i class="fas fa-street-view margin-r-5"></i></h5>
                      <span class="description-text">{{$admin->address}}</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header"><i class="fas fa-history"></i></h5>
                      <span class="description-text">
                          <?php 
                          $today=date('Y-m-d');
                          $datetime1 = new DateTime($admin->created_at);
                          $datetime2 = new DateTime($today);
                          $difference = $datetime1->diff($datetime2);
                          echo $difference->y.' years, ' 
                                       .$difference->m.' months, ' 
                                       .$difference->d.' days';
                          ?>
                      </span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->
          <div class="col-md-4">
            <form action="{{url('admin/user/profile/update')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$admin->id}}" class="form-control form-control-sm" required />
            <!-- Box Comment -->
            <div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <img class="img-circle" src="{{ URL::to('') }}/storage/app/public/admin-user/{{ $admin->image }}" alt="User Image">
                  <span class="username"><a href="#">{{$admin->name}}</a></span>
                  <span class="description">Last Login - <?php $time = strtotime($admin->updated_at);
                                    echo date('d M , Y h:i A', $time); ?></span>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" title="Mark as read">
                    <i class="far fa-circle"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                 @if (session('info'))
                <div class="callout callout-success">
                  <h5><i class="fas fa-info"></i> Note:</h5>
                  {{ session('info') }}
                </div>
                @endif
                @if (session('error'))
                <div class="callout callout-danger">
                  <h5><i class="fas fa-info"></i> Note:</h5>
                  {{ session('error') }}
                </div>
                @endif
                <!-- post text -->
                <form action="#" method="post">
                    <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" name="name" value="{{$admin->name}}" class="form-control form-control-sm" required />
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" value="{{$admin->email}}" class="form-control form-control-sm" required />
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    </div>
                    <div class="form-group">
                        <label>User Name</label> 
                        <input type="text" name="username" value="{{$admin->username}}" class="form-control form-control-sm" required />
                        <span class="text-danger">{{ $errors->first('username') }}</span>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea type="text" name="address" class="form-control form-control-sm" rows="3" required>{{$admin->address}}</textarea>
                        <span class="text-danger">{{ $errors->first('address') }}</span>
                    </div>
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" name="mobile" value="{{$admin->mobile}}" class="form-control form-control-sm" required />
                        <span class="text-danger"></span>
                    </div>
                    <div class="form-group">
                        <label>Current Image</label>
                        <div class="mb10">
                            <span class="file-input">
                                <div class="file-preview">
                                    <div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
                                    <div class="file-preview-thumbnails">
                                        <div class="file-preview-frame" id="preview">
                                            <img class="img-responsive" src="{{ URL::to('') }}/storage/app/public/admin-user/{{$admin->image}}" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>   
                                    <div class="file-preview-status text-center text-success"></div>
                                    <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                </div>

                            </span>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                        <label>New Image</label>
                        <input id="input-file" type="file" class="file form-control-sm" name="photo" data-preview-file-type="text"/>
                        <span class="help-block" style="color:#06be1c"><small>only .jpg image is allowed Size (Width: 100px X Height: 100px)</small></span>  
                        <div class="help-block with-errors">{{ $errors->first('image') }}</div>
                    </div>
                </form>
              </div>
              <!-- /.card-body -->
              <!-- /.card-footer -->
              <div class="card-footer">
                  <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary btn-sm float-right"> <i class="fas fa-edit"></i> Update Information</button>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
            </form>
          </div>
          <!-- /.col -->
          <div class="col-md-4">
             <form action="{{url('admin/user/change/password')}}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$admin->id}}" class="form-control form-control-sm" required />
            <!-- Box Comment -->
            <div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <img class="img-circle" src="{{ URL::to('') }}/storage/app/public/admin-user/{{ $admin->image }}" alt="User Image">
                  <span class="username"><a href="#">{{$admin->name}}</a></span>
                  <span class="description">Last Login - <?php $time = strtotime($admin->updated_at);
                                    echo date('d M , Y h:i A', $time); ?></span>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" title="Mark as read">
                    <i class="far fa-circle"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if (session('pass'))
                    <div class="callout callout-success">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('pass') }}
                    </div>
                    @endif
                    @if (session('pass_error'))
                    <div class="callout callout-danger">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('pass_error') }}
                    </div>
                    @endif
                <!-- post text -->
                    <div class="form-group">
                        <label>Old Password</label>
                        <input type="password" name="current-password"  class="form-control form-control-sm" required />
                        <span class="text-danger">{{ $errors->first('current-password') }}</span>
                    </div>
                    <div class="form-group">
                        <label>New Password</label> 
                        <input type="password" name="new-password"  class="form-control form-control-sm" required />
                        <span class="text-danger">{{ $errors->first('new-password') }}</span>
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="password_confirmation"  class="form-control form-control-sm" required />
                        <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                    </div>
                    
                </form>
              </div>
              <!-- /.card-body -->
              <!-- /.card-footer -->
              <div class="card-footer">
                  <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary btn-sm float-right"> <i class="fas fa-edit"></i> Change Password</button>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
            </form>
          </div>
          <!-- /.col -->
        </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: false,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
