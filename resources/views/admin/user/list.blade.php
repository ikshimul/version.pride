@extends('admin.layouts.app')
@section('title','Admin List')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Manage</h1>
      </div>
      <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item">Admin</li>
            <li class="breadcrumb-item">Manage</li>
            <li class="breadcrumb-item active">List</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Admin List</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        <div class="box-body">
            <div class="row">
                <div class="col-12">
                @if (session('update'))
                    <div class="callout callout-success">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('update') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="callout callout-danger">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('error') }}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Full Name</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user_list)
                                <tr>
                                    <td><img src="{{url('/')}}/storage/app/public/admin-user/{{$user_list->image}}" height="50" width="50"/></td>
                                    <td>{{$user_list->name}}</td>
                                    <td>{{$user_list->username}}</td>
                                    <td>{{$user_list->email}}</td>
                                    <td>{{$user_list->mobile}}</td>
                                    <td>{{$user_list->address}}</td>
                                    <td>{{$user_list->admin_type}}</td>
                                    <td>
                                        <a href='{{url("/admin/user/edit?id={$user_list->id}")}}' class="btn btn-sm btn-primary">Edit</a>
                                    <?php if($user_list->active==1){ ?>
                                        <a class="btn btn-sm btn-flat btn-success margin tdata" onclick="return confirm('Are you sure you want to block this admin?');"  href='{{url("admin/user/deactive?id=$user_list->id")}}' title='Block admin'> Unblock</a>
                                     <?php } else{ ?>
                                      <a class="btn btn-sm btn-flat btn-danger margin tdata" onclick="return confirm('Are you sure you want to unblock this admin?');"  href='{{url("admin/user/active?id=$user_list->id")}}' title='Unblock admin'> Block</a>
                                     <?php } ?>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
