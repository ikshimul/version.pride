@extends('admin.layouts.app')
@section('title','Pride Limited | Role Permission')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Role Permission</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Admin</a></li>
          <li class="breadcrumb-item"><a href="#">Role Manage</a></li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="card">
    <form action="{{url('/admin/user/role/update')}}" method="post">
    {{csrf_field()}}
    <input type="hidden" name="id" value="{{$role->id}}" class="form-control" required />
        <div class="card-header">
          <h3 class="card-title">Role & Permission Edit</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
            <!-- Minimal style -->
            <div class="row">
                    <div class="col-sm-6">
                        @if (session('save'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('save') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                        <div class="form-group">
                            <label>Role Name</label>
                            <input type="text" name="role_name" value="{{$role->role_name}}" class="form-control" required />
                            <span class="text-danger">{{ $errors->first('role_name') }}</span>
                        </div>
                    </div>
                     <div class="col-sm-6">&nbsp;</div>
                  <div class="col-sm-3">
                      <div class="user_access pb-3">
                         <h5 class="pb-2">Dashboard</h5>
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                          <div class="icheck-primary">
                            <input type="checkbox" id="Dashboard" name="permission[]" value="admin" calss="checkBoxClass" @if(in_array('admin',$permission)) checked @endif />
                            <label for="Dashboard">
                                Dashboard
                            </label>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="user_access">
                         <h5 class="pb-2">Home Page</h5>
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                          <div class="icheck-primary">
                            <input type="checkbox" id="homepage_manage" name="permission[]" value="homepage" calss="checkBoxClass" @if(in_array('homepage',$permission)) checked @endif />
                            <label for="homepage_manage">
                                Manage Homepage
                            </label>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="user_access">
                         <h5 class="pb-2">Category</h5>
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                          <div class="icheck-primary">
                            <input type="checkbox" id="category_manage" name="permission[]" value="category" calss="checkBoxClass" @if(in_array('category',$permission)) checked @endif />
                            <label for="category_manage">
                                Manage Category
                            </label>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="user_access">
                         <h5 class="pb-2">Product</h5>
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                          <div class="icheck-primary">
                            <input type="checkbox" id="product_manage" name="permission[]" value="product" calss="checkBoxClass" @if(in_array('product',$permission)) checked @endif />
                            <label for="product_manage">
                                Manage Product
                            </label>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="user_access">
                         <h5 class="pb-2">Order</h5>
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                          <div class="icheck-primary">
                            <input type="checkbox" id="order_manage" name="permission[]" value="order" calss="checkBoxClass" @if(in_array('order',$permission)) checked @endif />
                            <label for="order_manage">
                                Manage Order
                            </label>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="user_access">
                         <h5 class="pb-2">Customers</h5>
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                          <div class="icheck-primary">
                            <input type="checkbox" id="customer_manage" name="permission[]" value="customer" calss="checkBoxClass" @if(in_array('customer',$permission)) checked @endif />
                            <label for="customer_manage">
                                Manage Customers
                            </label>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="user_access">
                         <h5 class="pb-2">Marketing</h5>
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                          <div class="icheck-primary">
                            <input type="checkbox" id="marketing_manage" name="permission[]" value="marketing" calss="checkBoxClass" @if(in_array('marketing',$permission)) checked @endif />
                            <label for="marketing_manage">
                                Manage Marketing
                            </label>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="user_access">
                         <h5 class="pb-2">Admin User</h5>
                        <!-- checkbox -->
                        <div class="form-group clearfix">
                          <div class="icheck-primary">
                            <input type="checkbox" id="admin_manage" name="permission[]" value="admin/user" calss="checkBoxClass" @if(in_array('admin/user',$permission)) checked @endif />
                            <label for="admin_manage">
                                Manage Admin
                            </label>
                          </div>
                        </div>
                      </div>
                  </div>
            </div>
          <!-- /.card-body -->
        </div>
         <div class="card-footer">
            <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> <i class="fas fa-save"></i> Update Role</button>
         </div>
     </form>
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
