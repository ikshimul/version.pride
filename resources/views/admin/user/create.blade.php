@extends('admin.layouts.app')
@section('title','Pride Limited | Admin User')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Admin</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Admin</a></li>
          <li class="breadcrumb-item"><a href="#">Manage</a></li>
          <li class="breadcrumb-item active">Add</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Admin Create</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
       <form name="add_subpro" action="{{url('/admin/user/save')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                         @if (session('save'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('save') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" name="full_name" value="<?php echo old('full_name') ?>" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('full_name') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="<?php echo old('email') ?>" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('email') ?></span>
                        </div>
                        <div class="form-group">
                            <label>User Name</label>
                            <input type="text" name="username" value="<?php echo old('username')?>" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('username') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="mobile" value="<?php echo old('mobile')?>" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('mobile') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea type="text" name="address" class="form-control" rows="3"><?php echo old('address') ?></textarea>
                            <span class="text-danger"><?php echo $errors->first('address') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password"  class="form-control">
                            <span class="text-danger"><?php echo $errors->first('password') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="c_password" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('c_password')?></span>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <select name="role_id" class="form-control select2">
                                <option value="">---- select role ----</option>
                                <?php foreach($roles as $role) { ?>
                                <option value="<?php echo $role->id ?>"> <?php echo $role->role_name ?></option>
                               <?php } ?>
                            </select>
                             <span class="text-danger"><?php echo $errors->first('role_id')?></span>
                        </div>
                        <div class="form-group">
                             <label for="customFile">Profile Image</label> 
                            <div class="custom-file">
                              <input type="file" class="file" name="photo" id="input-file"  data-browse-on-zone-click="true">
                              <span class="help-block" style="color:#06be1c">only .jpg image is allowed Size (Width: 100px X Height: 100px)</span>  
                              <div class="help-block with-errors invalid-feedback">{{ $errors->first('photo') }}</div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-md-offset-1">
                                <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> <i class="fas fa-save"></i> Add Admin</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </form>    
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
