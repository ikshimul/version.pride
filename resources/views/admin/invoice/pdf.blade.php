
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Pride Limited | {{ $shipping_address_details->conforder_tracknumber }}</title>
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/invoice-pdf.css')}}?2">
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="{{url('/')}}/storage/app/public/logo.png">
      </div>
      <div id="company">
        <h2 class="name">Pride Limited</h2>
        <div>Level 5, Pride Hamza, House 54, Road 1, Sector 6, Uttara, Dhaka</div>
        <div>0966-910-0216</div>
        <div><a href="mailto:care@pride-limited.com">care@pride-limited.com</a></div>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">INVOICE TO:</div>
          <h2 class="name">{{ $shipping_address_details->Shipping_txtfirstname }} {{$shipping_address_details->Shipping_txtlastname }}</h2>
          <div class="address">{!! $shipping_address_details->Shipping_txtaddressname !!}, {{ $shipping_address_details->Shipping_txtcity }}</div>
          <div class="email"><a href="mailto:{{ $shipping_address_details->email }}">{{ $shipping_address_details->email }}</a></div>
          <div class="phone"><a href="tel:{{ $shipping_address_details->Shipping_txtphone }}">{{ $shipping_address_details->Shipping_txtphone }}</a></div>
        </div>
        <div id="invoice">
          <h1>{{ $shipping_address_details->conforder_tracknumber }}</h1>
          <div class="date">Date of Order: <?php $place_date=strtotime($shipping_address_details->created_at); echo date('d M, Y',$place_date); ?></div>
          <div class="payment">Payment By: <?php
            if ($shipping_address_details->payment_method == 'cDelivery') {
                echo 'Cash on delivery';
            }elseif($shipping_address_details->payment_method =='iPay'){
                 echo 'iPay';
            }else if($shipping_address_details->payment_method =='ssl'){
                 echo 'Credit/ Debit Card';
            } elseif($shipping_address_details->payment_method =='bKash'){
                echo 'bKash';
            }else{
                echo $shipping_address_details->payment_method;
            }
            
            ?></div>
            <div>Delivery By: {{ $shipping_address_details->delivery_by }}</div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="desc">DESCRIPTION</th>
            <th class="unit">UNIT PRICE</th>
            <th class="qty">QUANTITY</th>
            <th class="total">TOTAL</th>
          </tr>
        </thead>
        <tbody>
            @php $i=0;
            $subtotal = 0;
			@endphp
            @foreach($total_incomplete_order_info as $orderinfo)
            @php $i++; @endphp
                <?php
    				$subtotal = $subtotal + ($orderinfo->shoppinproduct_quantity * $orderinfo->product_price);
    				$shipping_charge = $shipping_address_details->shipping_charge;
    			?>
              <tr>
                <td class="no">{{$i}}</td>
                <td class="desc"><h3><img src="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}" width="25"/></h3>
                Size : {{ $orderinfo->prosize_name }}</td>
                <td class="unit">Tk {{$orderinfo->product_price}}</td>
                <td class="qty">{{ $orderinfo->shoppinproduct_quantity }}</td>
                <td class="total">Tk {{ $orderinfo->shoppinproduct_quantity*$orderinfo->product_price }}</td>
              </tr>
           @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">SUBTOTAL</td>
            <td>Tk {{ $subtotal }}</td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">Shipping</td>
            <td><?php
				if ($subtotal >= 3000) {
					echo $shipping_charge;
				} else {
					if ($shipping_charge == 70 || $shipping_charge == 100) {
						echo 'TK. '.$shipping_charge;
					}else if($shipping_charge == 85){
					    	echo 'TK. '.$shipping_charge;
					}else if($shipping_charge == 130){
					    echo 'TK. '.$shipping_charge;
					} else {
						echo 'TK. '.$shipping_charge;
					}
				}
			?></td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">GRAND TOTAL <?php  if($shipping_address_details->voucher_id==1){ ?> (10% Discount) <?php } ?></td>
            <td><?php if($shipping_address_details->voucher_id==1){
                    $totalamount=$subtotal + $shipping_charge+$shipping_address_details->extra_charge; 
                    $discount=$subtotal*10/100; 
                    echo ' TK. '.$totalamount=$totalamount-$discount; ?>
                <?php }else{ ?> TK. {{ $subtotal+$shipping_charge }} <?php } ?></td>
          </tr>
        </tfoot>
      </table>
      <div id="thanks">Thank you for your order</div>
      <div id="notices">
        <div>Pride Group</div>
        <div class="notice">Made in Bangladesh With Pride</div>
      </div>
    </main>
    <footer>
      Invoice was created on a computer and is valid without the customer signature.
    </footer>
  </body>
</html>