<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Pride Limited | {{ $shipping_address_details->conforder_tracknumber }}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{asset('assets_admin/bootstrap/css/bootstrap.min.css')}}">

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		<style>
			p {
				margin-bottom:5px;
				font-size:12px;
				letter-spacing:1px;
			}
			td {
				font-size:12px;
			}
			div, p, td, h3, span, strong, b {
			   font-family: myriad-pro !important;
			}
			hr {
			    margin-top:10px;
			    margin-bottom:10px;
			}
			p.text-small {
				font-size:10px;
				letter-spacing:2px;
			}
			b {
				font-weight:500;
			}
			h3 {
				margin-top:0;
			}
			.table-borderless > tbody > tr > td,
			.table-borderless > tbody > tr > th,
			.table-borderless > tfoot > tr > td,
			.table-borderless > tfoot > tr > th,
			.table-borderless > thead > tr > td,
			.table-borderless > thead > tr > th {
				border: none;
			}
			.light-border {
				border-bottom:2px solid #BBB;
			}
			.border-no-left {
				border: 1px solid #888 !important;
				border-left:none !important;
				border-top:none !important;
			}
			.border-no-right {
				border: 1px solid #888 !important;
				border-right:none !important;
				border-top:none !important;
			}
			.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
			    padding:3px;
			}
			svg {
			height: 100%;
			max-width: 50%;
		}
		</style>
	</head>
	@php($dmselect = '')
	@php($shoppinproduct_quantity = '')
	@php($shipping_charge = 0)
	<body onload="window.print();"  id="print_div">
		<div class="container-fluid" style="padding-top:0px">
			<div class="row">
				<div  class="col-xs-2">
					<img src="{{url('/')}}/storage/app/public/logo.png" style="max-width:100%; max-height:50px;">
				</div>
				<div  class="col-xs-6">
					<p class="text-small" style="line-height:20px">Level 3, Mirandel, House 3, Road 5, Block J, Baridhara,Dhaka</p>
					<p class="text-small" style="line-height:20px">Phone:0966-910-0216, Email:care@pride-limited.com</p>
				</div>
				<div  class="col-xs-4">
					<p class="text-small" style="line-height:20px; text-align:right">Invoice ID: {{ $shipping_address_details->conforder_tracknumber }}</p>
					<p class="text-small" style="line-height:20px; text-align:right">Date: <?php date_default_timezone_set("Asia/Dhaka"); echo date('d M, Y h:i A'); ?> ({{ $shipping_address_details->delivery_by }})</p>
				</div>
			</div>
			<hr style="border-bottom:1px solid gray"/>
			<div class="row">
				<div class="col-xs-4" style="height:160px">
					<h3>Order Info:</h3>
					<p><b>Order ID: {{ $shipping_address_details->conforder_tracknumber }}</b></p>
					<p><b>Placed: <?php $place_date=strtotime($shipping_address_details->created_at); echo date('d M, Y h:i:sa',$place_date); ?></b></p>
					<p><b>Payment method:
						<?php
                        if ($shipping_address_details->payment_method == 'cDelivery') {
                            echo 'Cash on delivery';
                        }elseif($shipping_address_details->payment_method =='iPay'){
                             echo 'iPay';
                        }else if($shipping_address_details->payment_method =='ssl'){
                             echo 'Credit/ Debit Card';
                        } elseif($shipping_address_details->payment_method =='bKash'){
                            echo 'bKash';
                        }else{
                            echo $shipping_address_details->payment_method;
                        }
                        ?></b>
					<p><b>Total product: <?php echo count($total_incomplete_order_info);?></b></p>
					<p><b>Delivery: Pride Limited</b></p>
				</div>
				<div class="col-xs-4" style="border:0px solid #CCC; text-align:center; margin-top:0; height:150px;box-sizing:border-box; padding:1px;">
					<!--<span style="max-width:50%;height:100%"> {!! QrCode::size(200)->generate("Welcome to Urban Truth!.Your Track ID $shipping_address_details->conforder_tracknumber."); !!}</sapn> -->
					<img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')
                        ->size(500)->errorCorrection('H')->merge('https://version.pride-limited.com/storage/app/public/logo_new.png', .3, true)
                        ->generate("Invoice ID : $shipping_address_details->conforder_tracknumber.  https://version.pride-limited.com/admin/order/invoice?id=$order_id,sms:01990409336")) !!} " style="max-width:50%;height:100%">
				</div>
				<div class="col-xs-4">
					<h3>Delivery Address:</h3>
					<p><b>Name:</b> {{ $shipping_address_details->Shipping_txtfirstname }} {{$shipping_address_details->Shipping_txtlastname }}</p>
					<p><b>Address:</b> {!! $shipping_address_details->Shipping_txtaddressname !!}, {{ $shipping_address_details->Shipping_txtcity }}</p>
					<p><b>Phone: {{ $shipping_address_details->Shipping_txtphone }}</b></>
					<p><b>Email: {{ $shipping_address_details->email }}</b></p>
				</div>
			</div>
			<div class="row">
				<table class="table table-borderless" style="margin-top:0px">
					<thead class="light-border">
						<tr style="border:2px solid #CCC">
							<th>SL</th>
							<th>Image</th>
							<th>Product</th>
							<th>Size</th>
							<th>Quantity</th>
							<th>Unit Price</th>
							<th>Total Price</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$subtotal = 0;
							$i=1;
						?>
						
						@foreach($total_incomplete_order_info as $orderinfo)
						@if($orderinfo->shoppinproduct_quantity !=0 )
						<tr style="border:1px solid #CCC">
							<?php
								$subtotal = $subtotal + ($orderinfo->shoppinproduct_quantity * $orderinfo->product_price);
								$shipping_charge = $shipping_address_details->shipping_charge;
							?>
							<td>{{ $i }}</td>
							<td><img src="{{url('/')}}/storage/app/public/pgallery/{{$orderinfo->cart_image}}" width="15"/></td>
							<td>{{ $orderinfo->product_name }}</td>
							<td>{{ $orderinfo->prosize_name }}</td>
							<td>{{ $orderinfo->shoppinproduct_quantity }}</td>
							<td><!--<span style="text-decoration: line-through; color:#888">123</span> --><span>TK. {{$orderinfo->product_price}}</span></td>
							<td>TK. {{ $orderinfo->shoppinproduct_quantity*$orderinfo->product_price }}</td>
							@php($i++)
						</tr>
						@endif
                        @endforeach
						<tr>
							<td colspan="5" rowspan="4" style="position:relative;border-right:2px solid #CCC !important">
							    <span style="font-size:17px; position:absolute;left:0%;top:-16%;transform:translate(2%, 46%);margin:0">Made in Bangladesh<br/>With Pride</span>
							    <span style="font-size:16px; position:absolute;right:10%;top:40%;transform:translate(2%, 46%);margin:0">________________<br/>customer signature.</span>
							 </td>
							<td class="border-no-right">Subtotal</td>
							<td class="border-no-left">TK. {{ $subtotal }}</td>
						</tr>
						<tr>
							<td class="border-no-right">Shipping</td>
							<td class="border-no-left">
								<?php
									if ($subtotal >= 3000) {
										echo $shipping_charge;
									} else {
										if ($shipping_charge == 70 || $shipping_charge == 100) {
											echo 'TK. '.$shipping_charge." (Inside Dhaka)";
										}else if($shipping_charge == 85){
										    	echo 'TK. '.$shipping_charge." (One Day)";
										}else if($shipping_charge == 130){
										    echo 'TK. '.$shipping_charge." (Same Day)";
										} else {
											echo 'TK. '.$shipping_charge." (Outside Dhaka)";
										}
									}
								?>
							</td>
						</tr>
						<tr>
							<td class="border-no-right">Total <?php  if($shipping_address_details->voucher_id==1){ ?> (10% Discount) <?php } ?></td>
							<td class="border-no-left"><?php if($shipping_address_details->voucher_id==1){
                                        $totalamount=$subtotal + $shipping_charge+$shipping_address_details->extra_charge; 
                                        $discount=$subtotal*10/100; 
                                        echo ' TK. '.$totalamount=$totalamount-$discount; ?>
                                    <?php }else{ ?> TK. {{ $subtotal+$shipping_charge }} <?php } ?></td>
						</tr>
						<!--<tr>
							<td class="border-no-right">Customer Payable <?php if($shipping_address_details->voucher_id==1){ ?> (10% Discount) <?php } ?></td>
							<td class="border-no-left"><?php if($shipping_address_details->voucher_id==1){
                                        $totalamount=$subtotal + $shipping_charge+$shipping_address_details->extra_charge; 
                                        $discount=$subtotal*10/100; 
                                        echo ' TK. '.$totalamount=$totalamount-$discount; ?>
                                    <?php }else{ ?> TK. {{ $subtotal+$shipping_charge }} <?php } ?></td>
						</tr> -->
					</tbody>
				</table>
			</div>
			<div class="row" style="border-top:1px solid gray">
				<p class="text-small"><span>www.pride-limited.com</span> is a <i>Pride<sup>@</sup> Group Venture</i></p>
				<p class="text-small">Thank you for your order</p>
			</div>
		</div>
		<script type="text/javascript">
			function printDiv() {    
				var printContents = document.getElementById('print_div').innerHTML;
				var originalContents = document.body.innerHTML;
				document.body.innerHTML = printContents;
				window.print();
				document.body.innerHTML = originalContents;
			}
		</script>
	</body>
</html>