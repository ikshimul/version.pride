@extends('admin.layouts.app')
@section('title','Add Slider')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Slider</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Home Page</a></li>
          <li class="breadcrumb-item"><a href="#">Slider</a></li>
          <li class="breadcrumb-item active">Create</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Slider</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
       <form name="add_subpro" action="{{url('/admin/slider/save')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                         @if (session('save'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('save') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                        <div class="form-group {{ $errors->has('slider_caption') ? ' has-error' : '' }}">
                            <label>Slider Caption</label>
                            <input type="text" class="form-control" name="slider_caption" value="{{old('slider_caption')}}"/>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('slider_caption') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('slider_occasion') ? ' has-error' : '' }}">
                            <label>Occasion</label>
                            <input type="text" class="form-control" name="slider_occasion" value="{{old('slider_occasion')}}"/>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('slider_occasion') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('slider_order') ? ' has-error' : '' }}">
                            <label>Slider Position</label>
                            <input type="number" class="form-control" name="slider_order" min="1" max="10" value="{{old('slider_order')}}"/>
                            <span class="help-block" style="color:#06be1c;">Only Numbers</span>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('slider_order') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('url_link') ? ' has-error' : '' }}">
                            <label>Slider Url</label>
                            <input type="url" class="form-control" name="url_link" value="{{old('url_link')}}"/>
                            <span class="help-block" style="color:#06be1c;">Only Url</span>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('url_link') }}</div>
                        </div>
                        <div class="form-group">
                             <label for="customFile">Slider Image</label> 
                            <div class="custom-file">
                              <input type="file" class="file" name="slider_image" id="input-file"  data-browse-on-zone-click="true">
                              <span class="help-block" style="color:#06be1c">only .jpg image is allowed Size (Width: 1920px X Height: 750px)</span>  
                              <div class="help-block with-errors invalid-feedback">{{ $errors->first('slider_image') }}</div>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('device') ? ' has-error' : '' }}">
                            <label>Device to show</label>
                            <select id="device" class="form-control" name="device">
                                <option value="">--Select Device--</option>
                                <option value="desktop">Desktop</option>
                                <option value="mobile">Mobile</option>
                            </select>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('device') }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-offset-1">
                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Add Slider" />
                </div>
            </div>
        </form>    
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
