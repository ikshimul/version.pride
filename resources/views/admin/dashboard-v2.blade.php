@extends('admin.layouts.app')
@section('title', 'Dashboard')
@section('content')
<style>
    .products-list .product-img img {
        height: 75px;
        width: 50px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{$totalorder}}</h3>
                    <p>Total Orders</p>
                </div>
                <div class="icon">
                    <i class="fas fa-shopping-cart"></i>
                </div>
                <a href="{{url('admin/order/list')}}" target="_blank" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3><?php  $totalorder=$guest_order+$register_order;
                    $bounce=($totalbouncedorderrate * 100) / $totalorder;
                    echo number_format($bounce, 2)
                    ?><sup style="font-size: 20px">%</sup></h3>
                    <p>Bounce Rate</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{url('admin/order/list')}}" target="_blank" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{ $totalregisteruser }}</h3>

                    <p>User Registrations</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-plus"></i>
                </div>
                <a href="{{url('admin/customer/list')}}" target="_blank" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{$total_qty}}</h3>

                    <p>Remaining Stock</p>
                </div>
                <div class="icon">
                    <i class="fas fa-chart-pie"></i>
                </div>
                <a href="{{url('admin/product/list')}}" target="_blank" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">New Orders</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Customer Name</th>
                                <th>Order NO</th>                        	
                                <th>Order Placed</th>
                                <th>Delivery Method</th>
                                <th>Payment Method</th>
                                <th>Order Status</th> 
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($total_incomplete_order_info as $orderinfo)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$orderinfo->Shipping_txtfirstname}} {{$orderinfo->Shipping_txtlastname}}</td>
                                <td> <a href='{{url("admin/order/details?id={$orderinfo->id}")}}' target="__blank" class="info">{{$orderinfo->conforder_tracknumber}}</a></td>
                                <td> <?php
                                    $order_date = strtotime($orderinfo->created_at);
                                    echo date('d M , Y h:i A', $order_date);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($orderinfo->shipping_charge == 130) {
                                        echo "<span class='blink'>Same Day Delivery</span>";
                                    } else if ($orderinfo->shipping_charge == 120) {
                                        echo "<span class='blink' style='color:orange;'>Next Day Delivery";
                                    } else {
                                        echo "Standard Delivery";
                                    }
                                    ?>
                                </td>
                                <td><?php
                                    if ($orderinfo->payment_method == 'cDelivery') {
                                        echo 'Cash on Delivery';
                                    } else if ($orderinfo->payment_method == 'ssl') {
                                        echo 'Bank Payment';
                                    } elseif ($orderinfo->payment_method == 'bKash') {
                                        echo 'bKash';
                                    } else {
                                        echo $orderinfo->payment_method;
                                    }
                                    ?></td>
                                <td>
                                    <?php if ($orderinfo->conforder_status == "Order_Verification_Pending") { ?>
                                        <span class="badge badge-info"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } else if ($orderinfo->conforder_status == "Pending_Dispatch") {
                                        ?>
                                        <span class="badge badge-primary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } else if ($orderinfo->conforder_status == "Dispatched") {
                                        ?>
                                        <span class="badge badge-secondary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } else if ($orderinfo->conforder_status == "Invalidate" || $orderinfo->conforder_status == "Cancelled") {
                                        ?>
                                        <span class="badge badge-danger"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } elseif ($orderinfo->conforder_status == "Bkash_Payment_Receive") { ?>
                                        <span class="badge badge-secondary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php }elseif($orderinfo->conforder_status == "Closed"){ ?>
                                        <span class="badge badge-success"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } else { ?>
                                        <span class="badge badge-primary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } ?>
                                </td>
                                <td><a href='{{url("admin/order/invoice?id={$orderinfo->id}")}}' target="__blank" class="btn btn-default btn-sm"><i class="fa fa-fw fa-print"></i> Print Invoice</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-md-6">
            <!-- DONUT CHART -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Monthly Sale Report</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                  <div class="chart">
                  <canvas id="monthlySale" height="280" width="600"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- PIE CHART -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Monthly Order Report</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="monthlyOrder" height="280" width="600"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            
          </div>
          <div class="col-md-6">
            <!-- AREA CHART -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Monthly User Report</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="canvas" height="280" width="600"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- PIE CHART -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Channel Wise Payment Report</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="monthlyPayment" height="280" width="600"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-12 col-xs-12">
              <!-- solid sales graph -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  Yearly Sales Comprison Report
                </h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-sm" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <canvas class="chart" id="line-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-4 col-xs-12">
              <!-- Product info -->
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Recently Added Products</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body p-0">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                    @foreach($last_ten_products as $product)
                      <li class="item">
                        <div class="product-img">
                          <img src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->product_img_thm; ?>" alt="Product Image" class="img-size-70">
                        </div>
                        <div class="product-info">
                          <a href="javascript:void(0)" class="product-title">{{$product->product_name}}
                            <span class="badge badge-warning float-right">Tk {{number_format($product->product_price,2)}}</span></a>
                          <span class="product-description">
                            <?php echo $product->product_description; ?><br>
                            <?php echo $product->product_styleref; ?><br>
                            <?php $add_date=strtotime($product->created_at); echo date('d M , Y h:i A',$add_date);?>
                          </span>
                        </div>
                      </li>
                      <!-- /.item -->
                      @endforeach
                    </ul>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer text-center">
                    <a href='{{url("admin/product/list")}}' target="_blank">View All Products</a>
                  </div>
                  <!-- /.card-footer -->
                </div>
              <!-- /.card -->
          </div>
          <div class="col-md-4 col-xs-12">
              <!-- Product info -->
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Recent 30 days most saleing products</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body p-0">
                    <ul class="products-list product-list-in-card pl-2 pr-2">
                    @foreach($recent_sale as $product)
                      <li class="item">
                        <div class="product-img">
                          <img src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->cart_image; ?>" alt="Product Image" class="img-size-70">
                        </div>
                        <div class="product-info">
                          <a href="javascript:void(0)" class="product-title">{{$product->product_name}}
                            <span class="badge badge-warning float-right">Tk {{number_format($product->product_price,2)}}</span></a>
                          <span class="product-description">
                            <?php echo $product->product_description; ?><br>
                            <?php echo $product->product_styleref; ?><br>
                            <?php $add_date=strtotime($product->created_at); echo date('d M , Y h:i A',$add_date);?>
                          </span>
                        </div>
                      </li>
                      <!-- /.item -->
                      @endforeach
                    </ul>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer text-center">
                    <a href='{{url("admin/product/list")}}' target="_blank">View All Products</a>
                  </div>
                  <!-- /.card-footer -->
                </div>
            <!-- /.card -->
          </div>
          <div class="col-md-4 col-xs-12">
              <!-- USERS LIST -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Latest Members</h3>

                <div class="card-tools">
                  <span class="badge badge-danger">8 New Members</span>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <ul class="users-list clearfix">
                 @foreach($site_user as $user)
                  <li>
                    <img src="{{url('/')}}/storage/app/public/user_black.svg" alt="User Image" class="img-size-50">
                    <a class="users-list-name" href="#">{{$user->name}}</a>
                    <span class="users-list-date"><?php $regi_date=strtotime($user->created_at); echo date('d M , Y h:i A',$regi_date);?></span>
                    {{$user->address}}
                  </li>
                  @endforeach
                </ul>
                <!-- /.users-list -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer text-center">
                <a href='{{url("admin/customer/list")}}' target="_blank">View All Users</a>
              </div>
              <!-- /.card-footer -->
            </div>
            <!--/.card -->
          </div>
          
    </div>
</section>
<!-- /.content -->
<!--<script src="https://code.highcharts.com/highcharts.src.js"></script>-->
<script>
$(document).ready(function () {
    var months = <?php echo $months; ?>;
    var user = <?php echo $users; ?>;
    var saleReport = <?php echo $saleReport; ?>;
    var orderReport = <?php echo $orderReport; ?>;
    var paymentReport = <?php echo $paymentreport; ?>;
    var comparison = <?php echo $comparison; ?>;
    console.log(comparison);
    //monthly users
    var barChartData = {
        labels: months,
        datasets: [{
            label: 'User',
            backgroundColor: '#007bff',
            data: user
        }]
    };
     //monthly sale
        var barSalechart = {
        labels: months,
        datasets: [{
            label: 'Sale Report',
            backgroundColor: '#ffae00',
            data: saleReport
        }]
      };
      //monthly order
        var barOrderchart = {
        labels: months,
        datasets: [{
            label: 'Monthly Order Report',
            backgroundColor: '#17a2b8',
            data: orderReport
        }]
      };
      //monthly payment type
        var DoughnutPamentchart = {
        labels: ['bKash','Bank','Cash'],
        datasets: [{
            label: 'Monthly Payment Channel Report',
            backgroundColor: ['#17a2b8','blue','red'],
            data: paymentReport
        }]
      };
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 1,
                        borderColor: '#007bff',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Monthly User Joined'
                }
            }
        });
        
        var sale = document.getElementById("monthlySale").getContext("2d");
        window.myBar = new Chart(sale, {
            type: 'bar',
            data: barSalechart,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 1,
                        borderColor: '#ffae00',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Monthly Sale Report'
                }
            }
        });
        
        var order = document.getElementById("monthlyOrder").getContext("2d");
        window.myBar = new Chart(order, {
            type: 'bar',
            data: barOrderchart,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 1,
                        borderColor: '#17a2b8',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Monthly Order Report'
                }
            }
        });
        
        var payment = document.getElementById("monthlyPayment").getContext("2d");
        window.myBar = new Chart(payment, {
            type: 'doughnut',
            data: DoughnutPamentchart,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 1,
                        borderColor: '#17a2b8',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Monthly Payment Report'
                }
            }
        });
        
    };
    // Sales graph chart
      var salesGraphChartCanvas = $('#line-chart').get(0).getContext('2d')
      // $('#revenue-chart').get(0).getContext('2d');
    
      var salesGraphChartData = {
        labels: months,
        datasets: comparison
      }
    
      var salesGraphChartOptions = {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
          display: true
        },
        scales: {
          xAxes: [{
            ticks: {

            },
            gridLines: {
              display: false,
              drawBorder: false
            }
          }],
          yAxes: [{
            ticks: {
              stepSize: 5000,
            },
            gridLines: {
              display: true,
              drawBorder: false
            }
          }]
        }
      }
    
      // This will get the first returned node in the jQuery collection.
      // eslint-disable-next-line no-unused-vars
      var salesGraphChart = new Chart(salesGraphChartCanvas, { // lgtm[js/unused-local-variable]
        type: 'line',
        data: salesGraphChartData,
        options: salesGraphChartOptions
      })
});   
</script>
@endsection