@extends('admin.layouts.app')
@section('title', 'Manage Banner')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Banner</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Home Page</li>
              <li class="breadcrumb-item">Banner</li>
              <li class="breadcrumb-item active">Manage</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Banner</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
			<div class="pb-5">
				<div class="co-12">
                        <!-- /.box-header -->
                            @if (session('update'))
                            <div class="callout callout-success">
                              <h5><i class="fas fa-info"></i> Note:</h5>
                              {{ session('update') }}
                            </div>
                            @endif
                            @if (session('error'))
                            <div class="callout callout-danger">
                              <h5><i class="fas fa-info"></i> Note:</h5>
                              {{ session('error') }}
                            </div>
                            @endif
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Title</th>
                                            <th>Link</th>
                                            <th>Position</th>
                                            <th>image</th>
                                            <th style="text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($banners as $banner)
                                        <tr>
                                            <td style="width:2%;"><?php echo $banner->id; ?></td>
                                            <td><?php echo $banner->banner_title; ?></td>
                                            <td><?php echo $banner->banner_link; ?></td>
                                            <td><?php echo $banner->banner_order; ?></td>
                                            <td><img src="{{ URL::to('') }}/storage/app/public/banner/<?php echo $banner->banner_image; ?>" class="img-responsive" height="50px"></td>
                                            <td>
                                                <a class="btn bg-olive btn-flat btn-sm margin tdata" href='{{url("admin/banner/edit?id={$banner->id}")}}'>Edit</a> 
                                                <?php if($banner->active == '0'){ ?>
                                                <a class="btn btn-danger btn-flat btn-sm tdata" href="{{url('admin/banner/active')}}/<?php echo $banner->id; ?>"  alt="Active" title="Active">Deactive</a>
                                                <?php }else{ ?>
                                                <a class="btn btn-success btn-flat btn-sm tdata" href="{{url('admin/banner/deactive')}}/<?php echo $banner->id; ?>"  alt="Deactive" title="Deactive">Active</a>
                                                <?php } ?>
                                                <a class="btn btn-sm btn-flat btn-danger margin tdata" onclick="return confirm('Are you sure you want to delete this banner?');"  href='{{url("admin/banner/delete?id=$banner->id")}}'> Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
                        <!-- /.box-body -->
                    <!-- /.box -->
                </div>
			</div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
    <!-- Ajax modal ---->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to deactive this?</h4>
                </div>
                <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                    <a href="#" class="btn btn-sm btn-danger" id="delete_link">Confirm</a>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--- Ajax modal end ---->
@endsection