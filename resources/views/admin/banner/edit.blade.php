@extends('admin.layouts.app')
@section('title','Edit Banner')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Banner</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Home Page</a></li>
          <li class="breadcrumb-item"><a href="#">Banner</a></li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Banner</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
       <form name="add_subpro" action="{{url('/admin/banner/update')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" class="form-control" name="id" value="{{$banner->id}}"/>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-6">
                         @if (session('save'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('save') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                        <div class="form-group {{ $errors->has('banner_title') ? ' has-error' : '' }}">
                            <label>Banner Title</label>
                            <input type="text" class="form-control" name="banner_title" value="{{$banner->banner_title}}"/>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('banner_title') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('banner_pos') ? ' has-error' : '' }}">
                            <label>Row</label>
                            <select class="form-control select2" name="banner_pos" id="banner_pos">
                                <option value=""> --- Select Row --- </option>
                                <option value="1">Row 1</option>
                                <option value="2">Row 2</option>
                                <option value="3">Row 3</option>
                            </select>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('banner_pos') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('banner_order') ? ' has-error' : '' }}">
                            <label>Banner Position</label>
                            <input type="number" class="form-control" name="banner_order" min="1" max="10" value="{{$banner->banner_order}}"/>
                            <span class="help-block" style="color:#06be1c;">Only Numbers</span>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('banner_order') }}</div>
                        </div>
                        <div class="form-group {{ $errors->has('banner_link') ? ' has-error' : '' }}">
                            <label>Banner Link</label>
                            <input type="url" class="form-control" name="banner_link" value="{{$banner->banner_link}}"/>
                            <span class="help-block" style="color:#06be1c;">Only Url</span>
                            <div class="help-block with-errors invalid-feedback">{{ $errors->first('banner_link') }}</div>
                        </div>
                        <div class="form-group">
                            <label>Current Image</label>
                            <div class="mb10">
                                <span class="file-input">
                                    <div class="file-preview">
                                        <div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
                                        <div class="file-preview-thumbnails">
                                            <div class="file-preview-frame" id="preview">
                                                <img class="img-responsive" src="{{url('/')}}/storage/app/public/banner/<?php echo $banner->banner_image; ?>" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>   
                                        <div class="file-preview-status text-center text-success"></div>
                                        <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                             <label for="customFile">New Image</label> 
                            <div class="custom-file">
                              <input type="file" class="file" name="banner_image" id="input-file"  data-browse-on-zone-click="true">
                              <span class="help-block" style="color:#06be1c">only .jpg image is allowed Size (Width: 1110px X Height: 550px)</span>  
                              <div class="help-block with-errors invalid-feedback">{{ $errors->first('banner_image') }}</div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="col-md-offset-1">
                                <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> <i class="fas fa-edit"></i> Update Banner</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </form>    
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
document.getElementById("banner_pos").value = "<?php echo $banner->banner_pos; ?>";
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
