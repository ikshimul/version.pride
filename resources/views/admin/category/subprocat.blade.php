@extends('admin.layouts.app')
@section('title', 'Manage Sub Category')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manage Sub Category</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Sub Category</li>
              <li class="breadcrumb-item active">Manage</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Sub Category</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          
          <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <form name="add_product" id="myform" action="{{url('/admin/category/subprocat/save')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
    			<fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
    			<legend style="padding:5px 20px; text-align:center; width:auto">Create Sub Category</legend>
                    @if (session('save'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('save') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                    <div class="form-group">
    					<label for="campus_id">Parent Category</label>
    					<select class="form-control" type="text"  name="maincat_id" id="maincat_id"  required>
    						<option value="">-- Select Parent Category --</option>
    						@foreach($maincats as $maincat)
    					    	<option value="{{$maincat->id}}">{{$maincat->name}}</option>
    						@endforeach
    					</select>
    					<span class="help-text"></span>
    				</div>
    				<div class="form-group">
    					<label for="campus_id">Main Category</label>
    					<select class="form-control" type="text"  name="procat_id" id="procat_id"  required>
    						<option value="">-- Select Main Category --</option>
    						
    					</select>
    					<span class="help-text"></span>
    				</div>
    				<div class="form-group">
    					<label for="campus_id">Sub Category Name</label>
    					<input type="text" class="form-control" name="name" required />
    					<span class="help-text"></span>
    				</div>
    				<div class="form-group">
    					<label for="campus_id">Slug name</label>
    					<input type="text" class="form-control" name="slug" required />
    					<span class="help-text"></span>
    				</div>
                    <div class="form-group">
                         <label for="customFile">Image</label> 
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" name="image" id="customFile">
                          <label class="custom-file-label" for="customFile">Choose file</label>
                          <div class="help-block with-errors invalid-feedback">{{ $errors->first('icon') }}</div>
                        </div>
                    </div>
    				<div class="box-footer">
    					<button type="submit" name="btnsubmit" class="btn btn-flat bg-navy pull-right" style="height: 3.5rem;">Add Sub Category</button>
    				</div>
    				</fieldset>
    				</form>
    			</div>
    		</div>
    		
    		<div class="pb-5">
				<div class="col-xs-12">
                              <!-- /.box-header -->
                              @if (session('update'))
                                <div class="callout callout-success">
                                  <h5><i class="fas fa-info"></i> Note:</h5>
                                  {{ session('update') }}
                                </div>
                                @endif
                                @if (session('deactive'))
                                <div class="callout callout-danger">
                                  <h5><i class="fas fa-info"></i> Note:</h5>
                                  {{ session('deactive') }}
                                </div>
                                @endif
                                @if (session('fav'))
                                <div class="callout callout-danger">
                                  <h5><i class="fas fa-info"></i> Note:</h5>
                                  {{ session('fav') }}
                                </div>
                                @endif
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Main Category</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th style="text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($list as $procat)
                                        <tr>
                                            <td style="width:2%;"><?php echo $procat->id; ?></td>
                                            <td><?php echo $procat->maincat_name; ?></td>
                                            <td><?php echo $procat->procat_name; ?></td>
                                            <td><?php echo $procat->name; ?></td>
                                            <td>
                                                <a class="btn bg-olive btn-flat btn-sm margin tdata" href="{{url('admin/category/subprocat/edit')}}/<?php echo $procat->id; ?>">Edit</a> 
                                                <?php if($procat->active == 0){ ?>
                                                <a class="btn btn-danger btn-flat btn-sm tdata" href="{{url('admin/category/subprocat/active')}}/<?php echo $procat->id; ?>"  alt="Active" title="Active">Deactive</a>
                                                <?php }else{ ?>
                                                <a class="btn btn-success btn-flat btn-sm tdata" href="{{url('admin/category/subprocat/deactive')}}/<?php echo $procat->id; ?>"  alt="Deactive" title="Deactive">Active</a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
                        <!-- /.box-body -->
                    <!-- /.box -->
                </div>
			</div>
          
        </div>
        <!-- /.card-body -->
        
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
<script>
$(document).ready(function () {
    $("#maincat_id").change(function () {
        var maincat_id = $("#maincat_id").val();
         //alert(maincat_id)
        var url_op = base_url + "/ajax/get-procat/" + maincat_id;
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (data) {
                // alert(html);
                // $('#CityList').html(html);
                $('#procat_id').empty();
                $('#procat_id').append('<option value="">Select Category</option>');
                $.each(data, function (index, supproobj) {
                    $('#procat_id').append('<option value="' + supproobj.id + '">' + supproobj.name + '</option>');
                });
            }
        });
    });
    $("#procat_id").change(function(){
        var procat_id=$("#procat_id").val();
        //alert(procat_id);
        var url=base_url+"/ajax/get-subcats/"+procat_id;
        $.ajax({
            url:url,
            type:'GET',
            dataType:'json',
            success:function(data){
                $('#subcat_id').empty();
                $('#subcat_id').append('<option value="">Select Sub Category</option>');
                $.each(data, function (index, supproobj) {
                    $('#subcat_id').append('<option value="' + supproobj.id + '">' + supproobj.name + '</option>');
                });
            }
        });
    });
});
</script>
@endsection