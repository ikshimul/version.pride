@extends('admin.layouts.app')
@section('title', 'Manage Main  Category')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manage Category</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Category</li>
              <li class="breadcrumb-item active">Manage</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Category</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
           <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <form name="add_product" id="myform" action="{{url('/admin/category/procat/save')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
				<fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
				<legend style="padding:5px 20px; text-align:center; width:auto">Create  Category</legend>
                        @if (session('save'))
                        <div class="callout callout-success">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('save') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="callout callout-danger">
                          <h5><i class="fas fa-info"></i> Note:</h5>
                          {{ session('error') }}
                        </div>
                        @endif
                    <div class="form-group">
						<label for="campus_id">Main Category</label>
						<select class="form-control" type="text"  name="maincat_id" id="maincat_id"  required>
							<option value="">-- select Main Category --</option>
							@foreach($maincats as $maincat)
						    	<option value="{{$maincat->id}}">{{$maincat->name}}</option>
							@endforeach
						</select>
						<span class="help-text"></span>
					</div>
					<div class="form-group">
						<label for="campus_id">Name</label>
						<input type="text" class="form-control" name="name" required />
						<span class="help-text"></span>
					</div>
					<div class="form-group">
						<label for="campus_id">Slug Name</label>
						<input type="text" class="form-control" name="slug"  required />
						<span class="help-text"></span>
					</div>
					<div class="box-footer">
					    <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> <i class="fas fa-save"></i> Save Category</button>
					</div>
					</fieldset>
					</form>
				</div>
			</div>
			
			<div class="pb-5">
				<div class="col-xs-12">
                        <!-- /.box-header -->
                            @if (session('update'))
                                <div class="callout callout-success">
                                  <h5><i class="fas fa-info"></i> Note:</h5>
                                  {{ session('update') }}
                                </div>
                                @endif
                                @if (session('deactive'))
                                <div class="callout callout-danger">
                                  <h5><i class="fas fa-info"></i> Note:</h5>
                                  {{ session('deactive') }}
                                </div>
                                @endif
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Main Category</th>
                                            <th>Sub Category</th>
                                            <th style="text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($procats as $procat)
                                        <tr>
                                            <td style="width:2%;"><?php echo $procat->id; ?></td>
                                            <td><?php echo $procat->maincat_name; ?></td>
                                            <td><?php echo $procat->name; ?></td>
                                            <td>
                                                <a class="btn bg-olive btn-flat btn-sm margin tdata" href="{{url('admin/category/procat/edit')}}/<?php echo $procat->id; ?>">Edit</a> 
                                                <?php if($procat->active ==0){ ?>
                                                <a class="btn btn-danger btn-flat btn-sm tdata" href="{{url('admin/category/procat/active')}}/<?php echo $procat->id; ?>"  alt="Active" title="Active">Deactive</a>
                                                <?php }else{ ?>
                                                <a class="btn btn-success btn-flat btn-sm tdata" href="{{url('admin/category/procat/deactive')}}/<?php echo $procat->id; ?>"  alt="Deactive" title="Deactive">Active</a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
                        <!-- /.box-body -->
                    <!-- /.box -->
                </div>
			</div>
			
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
    <!-- Ajax modal ---->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to deactive this?</h4>
                </div>
                <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                    <a href="#" class="btn btn-sm btn-danger" id="delete_link">Confirm</a>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--- Ajax modal end ---->
@endsection