@extends('admin.layouts.app')
@section('title', 'Manage Sub Category')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manage Sub Category</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Sub Category</li>
              <li class="breadcrumb-item active">Manage</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Manage Sub Category</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          
          <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <form name="add_product" id="myform" action="{{url('/admin/category/subprocat/update')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                <input type="hidden" class="form-control" name="id" value="{{$subprocat->id}}" required />
    			<fieldset style="margin:10px;  margin-top:44px; padding:5px 20px; border:2px solid #00c0ef44">
    			<legend style="padding:5px 20px; text-align:center; width:auto">Update Sub Category</legend>
                    <center>
                        @if (session('save'))
                        <div class="alert alert-success">
                            {{ session('save') }}
                        </div>
                        @endif
                    </center>
                    <center>
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                    </center>
                    <div class="form-group">
    					<label for="campus_id">Parent Category</label>
    					<select class="form-control" type="text"  name="maincat_id" id="maincat_id"  required>
    						<option value="">-- Select Parent Category --</option>
    						@foreach($maincats as $maincat)
    					    	<option value="{{$maincat->id}}">{{$maincat->name}}</option>
    						@endforeach
    					</select>
    					<span class="help-text"></span>
    				</div>
    				<div class="form-group">
    					<label for="campus_id">Main Category</label>
    					<select class="form-control" type="text"  name="procat_id" id="procat_id"  required>
    						<option value="">-- Select Main Category --</option>
    						@foreach($procats as $procat)
    					    	<option value="{{$procat->id}}">{{$procat->name}}</option>
    						@endforeach
    					</select>
    					<span class="help-text"></span>
    				</div>
    				<div class="form-group">
    					<label for="campus_id">Sub Category Name</label>
    					<input type="text" class="form-control" name="name"  value="{{$subprocat->name}}" required />
    					<span class="help-text"></span>
    				</div>
    				<div class="form-group">
    					<label for="campus_id">Slug name</label>
    					<input type="text" class="form-control" name="slug" value="{{$subprocat->slug}}" required />
    					<span class="help-text"></span>
    				</div>
                    <div class="form-group">
                        <label>Current Image</label>
                        <div class="mb10">
                            <span class="file-input">
                                <div class="file-preview">
                                    <div class="close fileinput-remove text-right"><a href="#" onclick="confirm_modal('#');">×</a></div>
                                    <div class="file-preview-thumbnails">
                                        <div class="file-preview-frame" id="preview">
                                            <img class="img-responsive" src="{{$subprocat->image}}" class="file-preview-image" title="" alt="" style="width:auto;height:160px;">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>   
                                    <div class="file-preview-status text-center text-success"></div>
                                    <div class="kv-fileinput-error file-error-message" style="display: none;"></div>
                                </div>

                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="customFile">New Image</label> 
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" name="image"  id="customFile">
                          <label class="custom-file-label" for="customFile">Choose file</label>
                          <div class="help-block with-errors invalid-feedback">{{ $errors->first('image') }}</div>
                        </div>
                    </div>
    				<div class="box-footer">
    					<button type="submit" name="btnsubmit" class="btn btn-flat bg-navy pull-right" style="height: 3.5rem;">Update Sub Category</button>
    				</div>
    				</fieldset>
    				</form>
    			</div>
    		</div>
          
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
    <!-- Ajax modal ---->
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;color:black;">Are you sure to deactive this?</h4>
                </div>
                <div class="modal-footer" style="margin:0px;border-top:0px;text-align:center;">
                    <a href="#" class="btn btn-sm btn-danger" id="delete_link">Confirm</a>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--- Ajax modal end ---->
<script>
    document.getElementById("maincat_id").value = "<?php echo $subprocat->maincat; ?>";
    document.getElementById("procat_id").value = "<?php echo $subprocat->cat; ?>";
    
</script>
@endsection