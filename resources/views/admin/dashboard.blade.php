@extends('admin.layouts.app')
@section('title', 'Pride Limited')
@section('content')
<!-- Content Header (Page header) -->
<style>
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: 600;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0px;
}
 @-webkit-keyframes blinker {
    from {opacity: 1.0;}
    to {opacity: 0.0;}
}
.blink{
    color: red;
    font-weight:bold;
    text-decoration: blink;
    -webkit-animation-name: blinker;
    -webkit-animation-duration: 0.8s;
    -webkit-animation-iteration-count:infinite;
    -webkit-animation-timing-function:ease-in-out;
    -webkit-animation-direction: alternate;
}
.products-list .product-img img {
    width: 50px;
    height: 75px;
}
</style>
<section class="content-header">
    <h1>
        Dashboard 
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="row">
        
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-gray">
                <div class="inner">
                    <h3>{{$totalorder}}</h3>
                    <h4>Total Orders</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="{{url('/all-order')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-gray">
                <div class="inner">
                    <h3>{{ $totalregisteruser }}</h3>
                    <h4>User Registrations</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="{{url('/site-user')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- /.col -->
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box small-box bg-gray">
                <div class="inner">
                    <h3>{{number_format($totalbouncedorderrate, 2) }}
                        <sup style="font-size: 20px">%</sup>
                    </h3>
                    <h4>Order Bounce Rate</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="small-box bg-gray">
                <div class="inner">
                    <h3>{{$total_qty - $total_sale_product}}</h3>
                    <h4>Remaining Stock</h4>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-ios-cart-outline"></i></span>
        
                <div class="info-box-content">
                    <span class="info-box-text">Guest Orders</span>
                    <span class="info-box-number">{{$guest_order}}</span>
                    <?php
                    $percentage = $guest_order;
                    $totalWidth = 100;
                    $new_width = ($percentage / 100) * $totalWidth;
                    ?>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{$new_width}}%"></div>
                    </div>
                    <span class="progress-description">
                        <?php echo date('F Y');?>  Report
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="ion ion-ios-cart-outline"></i></span>
        
                <div class="info-box-content">
                    <span class="info-box-text">Register user orders</span>
                    <span class="info-box-number">{{$register_order}}</span>
                   <?php
                    $percentage = $register_order;
                    $totalWidth = 100;
                    $new_width = ($percentage / 100) * $totalWidth;
                    ?>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{$new_width}}%"></div>
                    </div>
                    <span class="progress-description">
                       <?php echo date('F Y');?>  Report
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
            <span class="info-box-icon"><i class="ion ion-ios-cart-outline"></i></span>
    
                <div class="info-box-content">
                    <span class="info-box-text">Cancel Order</span>
                    <span class="info-box-number">{{$cancel_order}}</span>
                    <?php
                    $percentage = $cancel_order;
                    $totalWidth = 100;
                    $new_width = ($percentage / 100) * $totalWidth;
                    ?>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{$new_width}}%"></div>
                    </div>
                    <span class="progress-description">
                        <?php echo date('F Y');?>  Report
                    </span>
                </div>
            </div>
       </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion-ios-telephone"></i></span>
        
                <div class="info-box-content">
                    <span class="info-box-text">Phone Request</span>
                    <span class="info-box-number">{{$phone_request}}</span>
                    <?php
                    $percentage = $phone_request;
                    $totalWidth = 100;
                    $new_width = ($percentage / 100) * $totalWidth;
                    ?>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{$new_width}}%"></div>
                    </div>
                    <span class="progress-description">
                        <?php echo date('F Y');?>  Report
                    </span>
                </div>
            </div>
        </div>
        
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Orders</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                   <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped dataTable no-footer">
                            <thead>
                                <tr>
                                    <th width="5%">SL</th>
                                    <th>Customer Name</th>
                                    <th>Order NO</th>                        	
                                    <th>Order Placed</th>
                                    <th>Delivery Method</th>
                                    <th style="color:black;">Payment Method</th>
                                    <th width="15%">Order Status</th> 
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                ?>
                                @foreach($total_incomplete_order_info as $orderinfo)
                                <tr>                        	
                                    <td  width="5%">{{$i++}}</td>
                                    <td>{{$orderinfo->Shipping_txtfirstname}} {{$orderinfo->Shipping_txtlastname}}</td>
                                      <td>
                                        <a href='{{url("admin/order/details/{$orderinfo->id}")}}' target="__blank" class="info">{{$orderinfo->conforder_tracknumber}}</a>
                                    </td>
                                    <td>
                                        <?php 
                                            $order_date=strtotime($orderinfo->created_at); 
                                            echo date('d M , Y h:i A',$order_date);
                                        ?>
                                    </td>
                                    <td>
                                        <?php if($orderinfo->shipping_charge ==130){
                                        echo "<span class='blink'>Same Day Delivery</span>";
                                        }else if($orderinfo->shipping_charge ==85) {
                                        echo "<span class='blink' style='color:orange;'>Next Day Delivery";
                                        }else{
                                        echo "Standard Delivery";
                                        }
                                        ?>
                                    </td>
                                    <td style="color:black"><?php if($orderinfo->payment_method=='cDelivery'){
                                        echo 'Cash on Delivery';}else if($orderinfo->payment_method=='ssl'){
                                            echo 'Bank Payment';
                                        }elseif($orderinfo->payment_method=='bKash'){
                                            echo 'bKash';
                                        }else{
                                            echo $orderinfo->payment_method;
                                        }
                                        
                                        ?></td>
                                    <td>
                                        <?php if ($orderinfo->conforder_status == "Order_Verification_Pending") { ?>
                                            <span class="label label-danger" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Pending_Dispatch") {
                                            ?>
                                            <span class="label label-warning" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                        <?php } else if ($orderinfo->conforder_status == "Dispatch") {
                                            ?>
                                            <span class="label label-success" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                            <?php
                                        } else if($orderinfo->conforder_status == "Invalidate" || $orderinfo->conforder_status == "Cancelled"){ ?>
                                            <span class="label label-danger" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                    <?php }elseif($orderinfo->conforder_status == "Bkash_Payment_Receive"){ ?>
                                     <span class="label label-success" style="font-size:12px;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                    <?php  }else{  ?>
                                       <span class="label label-default" style="font-size:12px;color: black;"><?php echo str_replace('_',' ',$orderinfo->conforder_status); ?></span>
                                    <?php } ?>
                                        </td>
                                        <td><a href='{{url("admin/order/invoice?id={$orderinfo->id}")}}' target="__blank" class="btn btn-default btn-sm"><i class="fa fa-fw fa-print"></i> Print Invoice</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="{{url('manage-incomplete-order')}}" class="btn btn-sm btn-info btn-flat pull-left">Go to incomplete order page</a>
                    <a href="{{url('manage-all-order')}}" class="btn btn-sm btn-default btn-flat pull-right">Go to all orders page</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
        <div class="row">
                <div class="col-md-7 col-xs-12 connectedSortable">
                    <div class="box box-solid ">
                        <div class="box-header with-border">
                            <span class="box-title">Select Year</span>
                            <select id="SelectedYear" style="width:50%;">
                              <?php
                                 for($i=date("Y");$i>=2017;$i--){?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option> 
                                <?php }
                               ?>
                           </select>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                         
                        <div class="box-body no-padding">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div id="linechart_container"></div>
                                    <!--<button class="btn btn-sm btn-info btn-flat margin" id="plain">Plain</button>-->
                                    <!--<button class="btn btn-sm btn-info btn-flat margin" id="inverted">Inverted</button>-->
                                    <!--<button id="polar">Polar</button>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-xs-12 connectedSortable">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <!--<i class="fa fa-th"></i>-->
                            <h3 class="box-title">Monthly Order Report</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                        
                        
                    </div>
               </div>
            <div class="col-md-8 col-xs-12">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Users Report</h3>
    
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                     
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <!--<i class="fa fa-th"></i>-->
                        <h3 class="box-title">Payment Report (<?php echo date('F Y');?>)</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
     
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Recently Added Products</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        
                        <div class="box-body">
                            <ul class="products-list product-list-in-box">
                                <?php foreach($last_ten_products as $product){?>
                                <li class="item">
                                    <div class="product-img">
                                        <img src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_medium; ?>" alt="Product Image">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title"><?php echo $product->product_name; ?>
                                            <span class="label label-warning pull-right">Tk <?php echo $product->product_price; ?></span></a>
                                        <span class="product-description">
                                            <?php echo $product->product_description; ?><br>
                                            <?php echo $product->product_styleref; ?><br>
                                            <?php $add_date=strtotime($product->created_at); echo date('d M , Y h:i A',$add_date);?>
                                        </span>
                                    </div>
                                </li>
                               <?php } ?>
                            </ul>
                        </div>
                        <div class="box-footer text-center">
                            <a href="{{url('/admin/product/list')}}" class="uppercase">View All Products</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
         <div class="col-md-4 col-xs-12">
             <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent 30 days most sale products</h3>
    
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        <?php foreach($recent_sale as $product){?>
                        <li class="item">
                            <div class="product-img">
                                <img src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->productimg_img_thm; ?>" alt="<?php echo $product->product_name; ?>">
                            </div>
                            <div class="product-info">
                                <a href="javascript:void(0)" class="product-title"><?php echo $product->product_name; ?>
                                    <span class="label label-warning pull-right">Tk <?php echo $product->product_price; ?></span></a>
                                <span class="product-description">
                                    <?php echo $product->product_description; ?><br>
                                    <?php echo $product->product_styleref; ?><br>
                                    <?php $order_date=strtotime($product->created_at); echo date('d M , Y h:i A',$order_date);?>
                                </span>
                            </div>
                        </li>
                       <?php } ?>
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="{{url('/product-manage')}}" class="uppercase">View All Products</a>
                </div>
            </div>
           </div>

        <div class="col-md-4 col-xs-12">
            <div class="box box-solid">
                    <div class="box-header with-border">
                        <!--<i class="fa fa-th"></i>-->
                        <h3 class="box-title">Browser Report</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!--<div id="container"></div>-->
                    <!--<hr>-->
                    
                </div>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Members</h3>
                    <div class="box-tools pull-right">
                        <span class="label label-danger">8 New Members</span>
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    
                    <ul class="users-list clearfix">
                        <?php
                        foreach($site_user as $r_user){ ?>
                        <li>
                            <img src="{{url('/')}}/storage/app/public/user.png" alt="User Image">
                            <a class="users-list-name" href="#">{{$r_user->name}}</a>
                            <span class="users-list-date"><?php $regi_date=strtotime($r_user->created_at); echo date('d M , Y h:i A',$regi_date);?></span>
                            <small></small>
                        </li>
                        <?php } ?>
                    </ul>
                </div> 
                <div class="box-footer text-center">
                    <a href="{{url('/site-user')}}" class="uppercase">View All Users</a>
                </div> 
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.row -->
    <div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Yearly Sales Comprison Report</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            
            <div class="box-body">
                    <div id="yearlysellcomprisoncontainer" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                </div>
            </div>
        </div>
</div>
<section id="auth-button"></section>
<section id="view-selector"></section>
<section id="timeline"></section>
</section>
<script>
    $(document).ready(function () {
    var url_op = base_url + "/pride-admin/getComprisonChartData";
		$.ajax({
			url: url_op,
			type: 'GET',
			dataType: 'json',
			data: '',
			success: function (response)
			{
				//  alert(response);
				  getComprisonChartData(response);
			}
		});
     var url_op = base_url + "/pride-admin/gebrowserdata";
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (response)
            {
                browserData(response);
            }
        });
        
       
    var sdyear =   $("#SelectedYear option:selected").val(); 
    $("#SelectedYear").change(function(){
     sdyear =   $("#SelectedYear").val(); 
        var url_op1 = base_url + "/pride-admin/getmonthlysalesdata/"+sdyear;
        $.ajax({
            url: url_op1,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (response)
            {
               monthlysalesData(response,sdyear);
            }
        }); 
     });
     var url_op1 = base_url + "/pride-admin/getmonthlysalesdata/"+sdyear;
        $.ajax({
            url: url_op1,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (response)
            {
               // alert(response);
               monthlysalesData(response,sdyear);
            }
        }); 
        
        
        
        
    });
    


    function browserData(response) {
        var pieColors = (function () {
            var colors = [],
                    base = Highcharts.getOptions().colors[0],
                    i;
            for (i = 0; i < 10; i += 1) {
                colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
            }
            return colors;
        }());
        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Browsers use in Pride Limited website, 2018'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    colors: pieColors,
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                        distance: -50,
                        filter: {
                            property: 'percentage',
                            operator: '>',
                            value: 4
                        }
                    }
                }
            },
            series: [{
                    name: 'Browser',
                    data: response
                }]
        });

    }
    
    
    
    function  monthlysalesData(response,sdyear){

        var chart = Highcharts.chart('linechart_container', {
            title: {
                text: 'Monthly Sales Graph:' + sdyear
            },
            // subtitle: {
            //     text: 'Monthly Sales Graph:' + sdyear
            // },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','July','Aug','Sep','Oct','Nov','Dec']
            },
            series: [{
                    type: 'column',
                    colorByPoint: true,
                    data:  $.parseJSON(response),
                    showInLegend: false
                }]

        });


        $('#plain').click(function () {
            chart.update({
                chart: {
                    inverted: false,
                    polar: false
                },
                subtitle: {
                    text: 'Plain'
                }
            });
        });

        $('#inverted').click(function () {
            chart.update({
                chart: {
                    inverted: true,
                    polar: false
                },
                subtitle: {
                    text: 'Inverted'
                }
            });
        });

        $('#polar').click(function () {
            chart.update({
                chart: {
                    inverted: false,
                    polar: true
                },
                subtitle: {
                    text: 'Polar'
                }
            });
        });
      
     }
     
     function getComprisonChartData(response){
		
		console.log(response);
		
		 Highcharts.chart('yearlysellcomprisoncontainer', {
		chart: {
			type: 'line'
		},
		title: {
			text: 'Yearly Sales Comprison Report'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text: 'TK (BDT)'
			}
		},
		plotOptions: {
			line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series:  response.Series
	});  
							
	}
</script>               
<script>
gapi.analytics.ready(function() {
  /**
   * Authorize the user immediately if the user has already granted access.
   * If no access has been created, render an authorize button inside the
   * element with the ID "embed-api-auth-container".
   */
  gapi.analytics.auth.authorize({
    container: 'embed-api-auth-container',
    clientid: '368187051828-vpgo48coc87l66aefc2032d3ogi8hmhr.apps.googleusercontent.com'
  });


  /**
   * Create a new ViewSelector instance to be rendered inside of an
   * element with the id "view-selector-container".
   */
  var viewSelector = new gapi.analytics.ViewSelector({
    container: 'view-selector-container'
  });

  // Render the view selector to the page.
  viewSelector.execute();


  /**
   * Create a new DataChart instance with the given query parameters
   * and Google chart options. It will be rendered inside an element
   * with the id "chart-container".
   */
  var dataChart = new gapi.analytics.googleCharts.DataChart({
    query: {
      metrics: 'ga:sessions',
      dimensions: 'ga:date',
      'start-date': '30daysAgo',
      'end-date': 'yesterday'
    },
    chart: {
      container: 'chart-container',
      type: 'LINE',
      options: {
        width: '100%'
      }
    }
  });


  /**
   * Render the dataChart on the page whenever a new view is selected.
   */
  viewSelector.on('change', function(ids) {
    dataChart.set({query: {ids: ids}}).execute();
  });

});
</script>
@endsection
