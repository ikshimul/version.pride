@extends('admin.layouts.app')
@section('title', 'Dashboard')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{$totalorder}}</h3>

                    <p>Total Orders</p>
                </div>
                <div class="icon">
                    <i class="fas fa-shopping-cart"></i>
                </div>
                <a href="#" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Pending Order(s)</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Customer Name</th>
                                <th>Order NO</th>                        	
                                <th>Order Placed</th>
                                <th>Delivery Method</th>
                                <th>Payment Method</th>
                                <th>Order Status</th> 
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($total_incomplete_order_info as $orderinfo)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$orderinfo->Shipping_txtfirstname}} {{$orderinfo->Shipping_txtlastname}}</td>
                                <td> <a href='{{url("admin/order/details?id={$orderinfo->id}")}}' target="__blank" class="info">{{$orderinfo->conforder_tracknumber}}</a></td>
                                <td> <?php
                                    $order_date = strtotime($orderinfo->created_at);
                                    echo date('d M , Y h:i A', $order_date);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($orderinfo->shipping_charge == 130) {
                                        echo "<span class='blink'>Same Day Delivery</span>";
                                    } else if ($orderinfo->shipping_charge == 120) {
                                        echo "<span class='blink' style='color:orange;'>Next Day Delivery";
                                    } else {
                                        echo "Standard Delivery";
                                    }
                                    ?>
                                </td>
                                <td><?php
                                    if ($orderinfo->payment_method == 'cDelivery') {
                                        echo 'Cash on Delivery';
                                    } else if ($orderinfo->payment_method == 'ssl') {
                                        echo 'Bank Payment';
                                    } elseif ($orderinfo->payment_method == 'bKash') {
                                        echo 'bKash';
                                    } else {
                                        echo $orderinfo->payment_method;
                                    }
                                    ?></td>
                                <td>
                                    <?php if ($orderinfo->conforder_status == "Order_Verification_Pending") { ?>
                                        <span class="badge badge-info"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } else if ($orderinfo->conforder_status == "Pending_Dispatch") {
                                        ?>
                                        <span class="badge badge-primary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } else if ($orderinfo->conforder_status == "Dispatched") {
                                        ?>
                                        <span class="badge badge-secondary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } else if ($orderinfo->conforder_status == "Invalidate" || $orderinfo->conforder_status == "Cancelled") {
                                        ?>
                                        <span class="badge badge-danger"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } elseif ($orderinfo->conforder_status == "Bkash_Payment_Receive") { ?>
                                        <span class="badge badge-secondary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php }elseif($orderinfo->conforder_status == "Closed"){ ?>
                                        <span class="badge badge-success"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } else { ?>
                                        <span class="badge badge-primary"><?php echo str_replace('_', ' ', $orderinfo->conforder_status); ?></span>
                                    <?php } ?>
                                </td>
                                <td><a href='{{url("admin/order/invoice?id={$orderinfo->id}")}}' target="__blank" class="btn btn-default btn-sm"><i class="fa fa-fw fa-print"></i> Print Invoice</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</section>
<!-- /.content -->
@endsection