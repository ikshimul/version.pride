@extends('admin.layouts.app')
@section('title','Pride Limited | Sale Report')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Sale Report</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Order</a></li>
          <li class="breadcrumb-item"><a href="#">Report</a></li>
          <li class="breadcrumb-item active">Sale</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
<form name="sale-report" action="{{url('/admin/report/sale')}}" method="get" enctype="multipart/form-data">
  <!-- Default box -->
  <div class="card">
        <div class="card-header">
          <h3 class="card-title">Sale Report</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
           <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
    			<fieldset style="margin:10px; padding:5px 20px; border:2px solid #00c0ef44">
    			<legend style="padding:5px 20px; width:auto">Sale Report</legend>
    			    @if (session('save'))
                    <div class="callout callout-info">
                        <h5><i class="fas fa-info"></i> Note:</h5>
                        {{ session('save') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="callout callout-danger">
                        <h5><i class="fas fa-info"></i> Note:</h5>
                        {{ session('error') }}
                    </div>
                    @endif
                    <div class="form-group">
                        <label>Form Date</label>
                        <input type="date" name="from" id="date" value="<?php echo isset($from)?date('Y-m-d',strtotime($from)):''; ?>" autocomplete="off" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>To Date</label>
                        <input type="date" name="to" id="ToDate"  value="<?php echo isset($to)?date('Y-m-d',strtotime($to)):''; ?>" autocomplete="off" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Order status</label>
                        <select name="order_status" id="order_status" class="form-control select2">
                            <option value="">--Select--</option>
                            <option value="Pending_Dispatch">Verified</option>
                            <option value="Dispatched">Confirm Dispatch</option>
                            <option value="Payment_Pending">Delivered</option>
                            <option value="Closed">Closed</option>
                            <option value="Bkash_Payment_Receive">Bkash Payment Received</option>
                            <option value="Cancelled">Cancelled</option>
                            <option value="Exchange_Pending">Exchange Pending</option>
                            <option value="Exchange_Dispatched">Exchange Dispatched</option>
                            <option value="Exchanged">Exchanged</option>
                            <option value="Returned">Returned</option>
                            <option value="Order_Verification_Pending">Order Verification Pending</option>
                            <option value="%%">All Orders</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Payment Mode</label>
                        <select name="ddlthreepldlv" id="ddlthreepldlv" class="form-control select2">
                            <option value="">--Select--</option>
                            <option value="ssl">Bank</option>
                            <option value="bKash">Bkash</option>
                            <option value="cDelivery">Cash On Delivery</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit"  class="submitbtn btn btn-primary float-right"> <i class="fas fa-search"></i> Search</button>
                    </div>
    			</fieldset>
    		</div>
    		<div class="col-12">
    		    <table id="sale-report" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>SL</th>
                            <th>User Track Number</th>
                            <th>Customer Name</th>
                            <th>Order Placed</th>
                            <th>Pos Date</th>
                            <th>Delivery Date</th>
                            <th>Payment</th>
                            <th>Delivery Charge</th>
                            <th>Expense</th>
                            <th>Sub Total</th>
                            <th>Total</th>
                            <th>Discounted(10%)</th> 
                            <th>Discount Amount</th>
                            <!--<th>Order Status</th> --->
                        </tr>
                    </thead>
                    @php($total_amount = 0)
                    @php($total_delivery = 0)
                    @php($subtotal = 0)
                    @php($total_expense = 0)
                    @php($discounted_total = 0)
                    @php($i = 1)
                    <tbody>
                        @foreach($sale_reports as $report)
                        <tr>
                            <td>{{ $i }}</td>
                            <td><a href='{{url("admin/order/details?id={$report->id}")}}' target="__self" class="info">{{$report->conforder_tracknumber}}</a></td>
                            <td>{{ $report->Shipping_txtfirstname }} {{ $report->Shipping_txtlastname }}</td>
                            <td><?php echo date('d M , Y h:i A', strtotime($report->created_at)); ?></td>
                            <td><?php if(isset($report->pos_entry_date) && !(empty($report->pos_entry_date))) echo date('M j, Y', strtotime($report->pos_entry_date)); ?></td>
                            <td><?php if(isset($report->conforder_deliverydate) && !(empty($report->conforder_deliverydate))) echo date('M j, Y', strtotime($report->conforder_deliverydate)); ?></td>
                            <td>{{ $report->payment_method }}</td>
                            <td>
                                <?php if($report->extra_charge > 0){ ?>
                                {{ $report->shipping_Charge }} + {{$report->extra_charge}}
                                <?php }else{ ?>
                                {{$report->shipping_Charge }}
                                <?php } ?>
                            </td>
                            <td>{{ $report->amount }}</td>
                            <td>{{ $report->shoppingcart_subtotal }}</td>
                            <td>{{ $report->shipping_Charge + $report->shoppingcart_subtotal }}</td>
                            <td>
                            <?php
                            
                                $total = $report->shoppingcart_subtotal + $report->shipping_Charge + $report->extra_charge;
                                echo $distotal=$total-$report->shoppingcart_total;
                                $discounted_total+=$distotal;
                                
                            ?>
                            </td>
                            <td><?php echo $report->shipping_Charge + $report->extra_charge + $report->shoppingcart_subtotal - $distotal;?></td>
                            <!--<td>
                                <?php if ($report->conforder_status == "Order_Verification_Pending") { ?>
                                    <span class="label label-primary" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                                <?php } else if ($report->conforder_status == "Pending_Dispatch") {
                                    ?>
                                    <span class="label label-warning" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                                <?php } else if ($report->conforder_status == "Dispatch" || $report->conforder_status == "Closed") {
                                    ?>
                                    <span class="label label-success" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                                    <?php
                                } else if($report->conforder_status == "Cancelled" || $report->conforder_status == "Invalidate") {?>
                                    <span class="label label-danger" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                                <?php }else if($report->conforder_status == "Bkash_Payment_Receive") { ?>
                                     <span class="label label-info" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                                <?php }else{
                                  echo str_replace("_"," ",$report->conforder_status);
                                  }
                                ?>
                            </td> --->
                           
                            @php($total_delivery += $report->shipping_Charge + $report->extra_charge)
                            @php($subtotal += $report->shoppingcart_subtotal)
                            @php($total_amount= $total_delivery+$subtotal)
                            @php($total_expense = $total_expense+$report->amount)
                            @php($i++)
                        </tr>
                        @endforeach
                        <tr>
                            <!--<td colspan="5"></td> --->
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="font-weight: bold">Total Amount</td>
                            <td style="font-weight: bold">{{ $total_delivery }}</td>
                            <td style="font-weight: bold">{{ $total_expense }}</td>
                            <td style="font-weight: bold">{{ $subtotal }}</td>
                            <td style="font-weight: bold">{{ $total_amount }}</td>
                            <td style="font-weight: bold">{{ $discounted_total }}</td>  
                            <td style="font-weight: bold">{{ $total_amount-$discounted_total }}</td>  
                            <!--<td></td> -->
                        </tr>
                    </tbody>
                </table>
    		</div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
           <a href='{{url("admin/report/sale/pdf?from=$from&to=$to&order_status=$order_status&ddlthreepldlv=$order_threepldlv")}}' name="btnsubmit" class="submitbtn btn btn-primary float-left"> <i class="fas fa-download"></i> Generate PDF</a>
        </div>
    <!-- /.card-footer-->
    </div>
  <!-- /.card -->
 </form> 
</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#sale-report").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,"ordering": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
     }).buttons().container().appendTo('#sale-report_wrapper .col-md-6:eq(0)');
     var select_value = '{{$order_status}}';
     if(select_value)
    document.getElementById('order_status').value = '{{$order_status}}';
    var select_value = '{{$order_threepldlv}}';
    if(select_value)
    document.getElementById('ddlthreepldlv').value = '{{$order_threepldlv}}';
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
