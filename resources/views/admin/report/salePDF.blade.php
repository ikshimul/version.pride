<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Sale Report</title>
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
</head>
<style>
   body {
        font-family: 'PT Sans Narrow', sans-serif; 
    }
    table {
      border-collapse: collapse;
    }
    table, th, td {
      font-family: 'PT Sans Narrow', sans-serif; 
      border: 1px solid #eee;
    }
    th {
    font-family: 'PT Sans Narrow', sans-serif; 
    }
    .heading{
       font-weight:500;
       font-family: 'PT Sans Narrow', sans-serif;  
    }
</style>
<body>
    <h1 class="heading">Sale Report</h1>
    <table class="table table-bordered table-striped" style="font-family: 'PT Sans Narrow', sans-serif !important; font-size:12px;">
        <thead>
            <tr style="'PT Sans Narrow', sans-serif;">
                <th class="heading">SL</th>
                <th class="heading">Order Number</th>
                <th class="heading">Customer Name</th>
                <th class="heading">Order Placed</th>
                <th class="heading">Delivery Date</th>
                <th class="heading">Payment</th>
                <th class="heading">Delivery Charge</th>
                <th class="heading">Expense</th>
                <th class="heading">Sub Total</th>
                <th class="heading">Total</th>
                <th class="heading">Discounted(10%)</th> 
                <th class="heading">Discount Amount</th>
                <!--<th>Order Status</th> --->
            </tr>
        </thead>
        @php($total_amount = 0)
        @php($total_delivery = 0)
        @php($subtotal = 0)
        @php($total_expense = 0)
        @php($discounted_total = 0)
        @php($i = 1)
        <tbody>
            @foreach($sale_reports as $report)
            <tr>
                <td>{{ $i }}</td>
                <td>{{$report->conforder_tracknumber}}</td>
                <td>{{ $report->Shipping_txtfirstname }} {{ $report->Shipping_txtlastname }}</td>
                <td><?php echo date('d M , Y h:i A', strtotime($report->created_at)); ?></td>
                <td><?php if(isset($report->conforder_deliverydate) && !(empty($report->conforder_deliverydate))) echo date('M j, Y', strtotime($report->conforder_deliverydate)); ?></td>
                <td>{{ $report->payment_method }}</td>
                <td>
                    <?php if($report->extra_charge > 0){ ?>
                    {{ $report->shipping_Charge }} + {{$report->extra_charge}}
                    <?php }else{ ?>
                    {{$report->shipping_Charge }}
                    <?php } ?>
                </td>
                <td>{{ $report->amount }}</td>
                <td>{{ $report->shoppingcart_subtotal }}</td>
                <td>{{ $report->shipping_Charge + $report->shoppingcart_subtotal }}</td>
                <td>
                <?php
                
                    $total = $report->shoppingcart_subtotal + $report->shipping_Charge + $report->extra_charge;
                    echo $distotal=$total-$report->shoppingcart_total;
                    $discounted_total += $distotal;
                    
                ?>
                </td>
                <td><?php echo $report->shipping_Charge + $report->extra_charge + $report->shoppingcart_subtotal - $distotal;?></td>
                <!--<td>
                    <?php if ($report->conforder_status == "Order_Verification_Pending") { ?>
                        <span class="label label-primary" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                    <?php } else if ($report->conforder_status == "Pending_Dispatch") {
                        ?>
                        <span class="label label-warning" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                    <?php } else if ($report->conforder_status == "Dispatch" || $report->conforder_status == "Closed") {
                        ?>
                        <span class="label label-success" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                        <?php
                    } else if($report->conforder_status == "Cancelled" || $report->conforder_status == "Invalidate") {?>
                        <span class="label label-danger" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                    <?php }else if($report->conforder_status == "Bkash_Payment_Receive") { ?>
                         <span class="label label-info" style="font-size:12px;"><?php echo str_replace("_"," ",$report->conforder_status); ?></span>
                    <?php }else{
                      echo str_replace("_"," ",$report->conforder_status);
                      }
                    ?>
                </td> --->
               
                @php($total_delivery += $report->shipping_Charge + $report->extra_charge)
                @php($subtotal += $report->shoppingcart_subtotal)
                @php($total_amount= $total_delivery+$subtotal)
                @php($total_expense = $total_expense+$report->amount)
                @php($i++)
            </tr>
            @endforeach
            <tr>
                <!--<td colspan="5"></td> --->
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="font-weight: bold">Total Amount</td>
                <td style="font-weight: bold">{{ $total_delivery }}</td>
                <td style="font-weight: bold">{{ $total_expense }}</td>
                <td style="font-weight: bold">{{ $subtotal }}</td>
                <td style="font-weight: bold">{{ $total_amount }}</td>
                <td style="font-weight: bold">{{ $discounted_total }}</td>  
                <td style="font-weight: bold">{{ $total_amount-$discounted_total }}</td>  
                <!--<td></td> -->
            </tr>
        </tbody>
    </table>
</body>
</html>