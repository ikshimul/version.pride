<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="icon" type="image/png" href="{{asset('assets_admin/images/logo.png')}}">
        <!-- Google Font: Source Sans Pro -->
        <!--<link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:wght@300&display=swap" rel="stylesheet"> -->
        <!--<link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:wght@300&display=swap" rel="stylesheet">-->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
        <!-- Font Awesome -->
        <link  href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
        <link  href="{{ asset('assets/admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
        <!-- DataTables -->
        <link  href="{{ asset('assets/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
        <link  href="{{ asset('assets/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
        <link  href="{{ asset('assets/admin/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
        <!-- Theme style -->
        <link  href="{{ asset('assets/admin/fileinput/css/fileinput.min.css') }}" rel="stylesheet">
        <link  href="{{ asset('assets/admin/fileinput/css/fileinput-rtl.css') }}" rel="stylesheet">
        <link  href="{{ asset('assets/admin/css/adminlte.min.css') }}" rel="stylesheet">
        <!-- summernote -->
        <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.css') }}">
        <!-- jquery ui -->
        <link rel="stylesheet" href="{{ asset('assets/admin/plugins/jquery-ui/jquery-ui.min.css') }}">
        <!-- jQuery -->
        <script src="{{asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
        <style>
            body {
                /*font-family: open sans condensed,sans-serif!important;*/
                font-family: 'PT Sans Narrow', sans-serif; 
            }
            .loading-modal {
                display:    none;
                position:   fixed;
                z-index:    1000;
                top:        0;
                left:       0;
                height:     100%;
                width:      100%;
                background: rgba( 255, 255, 255, .8 ) 
                            url("{{url('/')}}/storage/app/public/pride-loading-gif_new.gif") 
                            50% 50% 
                            no-repeat;
            }
        </style>
         <script>
              var base_url = "{{ URL::to('') }}";
              var csrf_token = "{{ csrf_token() }}";
        </script>
    </head>
    <?php 
      $permission = getPermission(Auth::guard('admin')->user()->role_id);
      $newOrder = newOrder();
      $newOrderdetails = newOrderDiscription();
    ?>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="#" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="#" class="nav-link">Contact</a>
                    </li>
                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <!-- Navbar Search -->
                    <li class="nav-item">
                        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                            <i class="fas fa-search"></i>
                        </a>
                        <div class="navbar-search-block">
                            <form class="form-inline">
                                <div class="input-group input-group-sm">
                                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                                    <div class="input-group-append">
                                        <button class="btn btn-navbar" type="submit">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" id="datasync" role="button">
                            <i class="fas fa-sync"></i>
                        </a>
                    </li>
                    <!-- Messages Dropdown Menu -->
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="far fa-comments"></i>
                            <span class="badge badge-danger navbar-badge"></span>
                        </a>
                        <!--<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">-->
                        <!--    <a href="#" class="dropdown-item">-->
                                <!-- Message Start -->
                        <!--        <div class="media">-->
                        <!--            <img src="{{ URL::to('') }}/assets_admin/images/logo.png" alt="User Avatar" class="img-size-50 mr-3 img-circle">-->
                        <!--            <div class="media-body">-->
                        <!--                <h3 class="dropdown-item-title">-->
                        <!--                    User-->
                        <!--                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>-->
                        <!--                </h3>-->
                        <!--                <p class="text-sm">Call me whenever you can...</p>-->
                        <!--                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>-->
                        <!--            </div>-->
                        <!--        </div>-->
                                <!-- Message End -->
                        <!--    </a>-->
                        <!--    <div class="dropdown-divider"></div>-->
                        <!--    <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>-->
                        <!--</div>-->
                    </li>
                    <!-- Notifications Dropdown Menu -->
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="far fa-bell"></i>
                            <span class="badge badge-warning navbar-badge" id="new-order">{{$newOrder}}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <span class="dropdown-item dropdown-header">{{$newOrder}} new order(s)</span>
                            <div class="dropdown-divider"></div>
                            @foreach($newOrderdetails as $new)
                            <a href='{{url("admin/order/details?id={$new->id}")}}' target="__blank" class="dropdown-item">
                                <i class="fas fa-shopping-bag"></i> {{$new->conforder_tracknumber}}
                                <span class="float-right text-muted text-sm">
                                    <?php $orderTime = strtotime($new->created_at);
                                    $difference =time() -$orderTime;
                                    $diff = $new->created_at->diffForHumans(null, true, true, 2);
                                    echo str_replace(['h', 'm'], ['hrs', 'mins'], $diff); 
                                    ?></span>ago
                            </a>
                            @endforeach
                            <div class="dropdown-divider"></div>
                            <a href="{{url('admin/order/incomplete')}}" class="dropdown-item dropdown-footer">See All New Order</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown user-menu">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ URL::to('') }}/storage/app/public/admin-user/{{ Auth::guard('admin')->user()->image }}" class="user-image img-circle elevation-2" alt="User Image">
                            <span class="d-none d-md-inline">{{ Auth::guard('admin')->user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <!-- User image -->
                            <li class="user-header bg-primary">
                                <img src="{{ URL::to('') }}/storage/app/public/admin-user/{{ Auth::guard('admin')->user()->image }}" class="img-circle elevation-2" alt="User Image">

                                <p>
                                    {{ Auth::guard('admin')->user()->name }} - Web Developer
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                                <a href="{{url('/admin/logout')}}" class="btn btn-default btn-flat float-right">Sign out</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </li>
                    <!--<li class="nav-item">
                      <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="fas fa-th-large"></i>
                      </a>
                    </li> -->
                </ul>
            </nav>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="#" class="brand-link">
                    <img src="{{ URL::to('') }}/assets_admin/images/logo.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">Pride Limited</span>
                </a>
                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="{{ URL::to('') }}/storage/app/public/admin-user/{{ Auth::guard('admin')->user()->image }}" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block">{{ Auth::guard('admin')->user()->name }}</a>
                        </div>
                    </div>
                    <!-- SidebarSearch Form -->
                    <div class="form-inline">
                        <div class="input-group" data-widget="sidebar-search">
                            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-sidebar">
                                    <i class="fas fa-search fa-fw"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
                                 with font-awesome or any other icon font library -->
                            @if (in_array('admin', $permission))
                            <li class="nav-item {{ Request::is('admin') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link {{ Request::is('admin') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Dashboard
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview {{ Request::is('admin') ? 'active' : '' }} treeview">
                                    <li class="nav-item">
                                        <a href="{{url('/admin')}}" class="nav-link {{ Request::is('admin') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Dashboard </p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                            @if (in_array('homepage', $permission))
                            <li class="nav-header">HOME PAGE SECTION</li>
                            <li class="nav-item {{ Request::is('admin/slider/add') ? 'menu-open' : '' }}{{ Request::is('admin/slider/edit') ? 'menu-open' : '' }}{{ Request::is('admin/slider/list') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon far fa-image"></i>
                                    <p>
                                        Slider
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('admin/slider/add')}}" class="nav-link {{ Request::is('admin/slider/add') ? 'active' : '' }}">
                                            <i class="far fa-plus-square nav-icon"></i>
                                            <p>Add</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('admin/slider/list')}}" class="nav-link {{ Request::is('admin/slider/list') ? 'active' : '' }}">
                                            <i class="fas fa-cog nav-icon"></i>
                                            <p>Manage</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ Request::is('admin/banner/add') ? 'menu-open' : '' }}{{ Request::is('admin/banner/edit') ? 'menu-open' : '' }}{{ Request::is('admin/banner/list') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon far fa-image"></i>
                                    <p>
                                        Banner
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('admin/banner/add')}}" class="nav-link {{ Request::is('admin/banner/add') ? 'active' : '' }}">
                                            <i class="far fa-plus-square nav-icon"></i>
                                            <p>Add</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('admin/banner/list')}}" class="nav-link {{ Request::is('admin/banner/list') ? 'active' : '' }}">
                                            <i class="fas fa-cog nav-icon"></i>
                                            <p>Manage</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ Request::is('admin/popup/add') ? 'menu-open' : '' }}{{ Request::is('admin/popup/edit') ? 'menu-open' : '' }}{{ Request::is('admin/popup/list') ? 'menu-open' : '' }}{{ Request::is('admin/popup/edit') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon far fa-image"></i>
                                    <p>
                                        Popup
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('admin/popup/add')}}" class="nav-link {{ Request::is('admin/banner/list') ? 'active' : '' }}">
                                            <i class="far fa-plus-square nav-icon"></i>
                                            <p>Add</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('admin/popup/list')}}" class="nav-link {{ Request::is('admin/popup/list') ? 'active' : '' }}">
                                            <i class="fas fa-cog nav-icon"></i>
                                            <p>Manage</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ Request::is('admin/campaign/add') ? 'menu-open' : '' }}{{ Request::is('admin/campaign/list') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>
                                        Collection
                                        <i class="fas fa-angle-left right"></i>
                                        <span class="right badge badge-danger">New</span>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('/admin/campaign/add')}}" class="nav-link {{ Request::is('admin/campaign/add') ? 'active' : '' }}">
                                            <i class="far fa-plus-square nav-icon"></i>
                                            <p>Add</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('/admin/campaign/list')}}" class="nav-link {{ Request::is('admin/campaign/list') ? 'active' : '' }}">
                                            <i class="fas fa-cog nav-icon"></i>
                                            <!--<i class="fas fa-cogs"></i>-->
                                            <p>Manage</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                             @if (in_array('product', $permission))
                            <li class="nav-header">PRODUCT SECTION</li>
                            @if (in_array('category', $permission))
                            <li class="nav-item  {{ Request::is('admin/category/maincat') ? 'menu-open' : '' }}{{ Request::is('admin/category/maincat/edit') ? 'menu-open' : '' }}{{ Request::is('admin/category/procat') ? 'menu-open' : '' }}{{ Request::is('admin/category/subprocat') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-list"></i>
                                    <p>
                                        Category
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('/admin/category/maincat')}}" class="nav-link {{ Request::is('admin/category/maincat') ? 'active' : '' }}">
                                            <i class="fas fa-cog nav-icon"></i>
                                            <p>Main Category</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('/admin/category/procat')}}" class="nav-link {{ Request::is('admin/category/procat') ? 'active' : '' }}">
                                            <i class="fas fa-cog nav-icon"></i>
                                            <p>Manage Category</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('/admin/category/subprocat')}}" class="nav-link {{ Request::is('admin/category/subprocat') ? 'active' : '' }}">
                                            <i class="fas fa-cog nav-icon"></i>
                                            <p>Manage Sub Category</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                            <li class="nav-item {{ Request::is('admin/product/fabric/add') ? 'menu-open' : '' }}{{ Request::is('admin/product/fabric/edit') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-tshirt"></i>
                                    <p>
                                        Fabric
                                        <i class="fas fa-angle-left right"></i>
                                        <span class="right badge badge-danger">New</span>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('admin/product/fabric/add')}}" class="nav-link {{ Request::is('admin/product/fabric/add') ? 'active' : '' }}">
                                            <i class="fas fa-cog nav-icon"></i>
                                            <p>Manage</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ Request::is('admin/product/add') ? 'menu-open' : '' }}{{ Request::is('admin/product/edit') ? 'menu-open' : '' }}{{ Request::is('admin/pos/product/add') ? 'menu-open' : '' }}{{ Request::is('admin/product/edit') ? 'menu-open' : '' }}{{ Request::is('admin/product/list') ? 'menu-open' : '' }}{{ Request::is('admin/product/trash') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link {{ Request::is('admin/product/add') ? 'active' : '' }}{{ Request::is('admin/pos/product/add') ? 'active' : '' }}{{ Request::is('admin/product/list') ? 'active' : '' }}">
                                    <i class="nav-icon fab fa-product-hunt"></i>
                                    <p>
                                        Product
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('admin/product/add')}}" class="nav-link {{ Request::is('admin/product/add') ? 'active' : '' }}">
                                            <i class="far fa-plus-square nav-icon"></i>
                                            <p>Add</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('admin/pos/product/add')}}" class="nav-link {{ Request::is('admin/pos/product/add') ? 'active' : '' }}">
                                            <i class="far fa-plus-square nav-icon"></i>
                                            <p>Add By Pos</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('admin/product/list')}}" class="nav-link {{ Request::is('admin/product/list') ? 'active' : '' }}">
                                            <i class="fas fa-list nav-icon"></i>
                                            <p>List</p>
                                        </a>
                                    </li>
                                    <!--<li class="nav-item">-->
                                    <!--    <a href="#" class="nav-link">-->
                                    <!--        <i class="fas fa-layer-group nav-icon"></i>-->
                                    <!--        <p>Stock Update</p>-->
                                    <!--    </a>-->
                                    <!--</li>-->
                                    <!--<li class="nav-item">-->
                                    <!--    <a href="#" class="nav-link">-->
                                    <!--        <i class="far fa-circle nav-icon"></i>-->
                                    <!--        <p>Deactive</p>-->
                                    <!--    </a>-->
                                    <!--</li>-->
                                    <li class="nav-item">
                                        <a href="{{url('admin/product/trash')}}" class="nav-link {{ Request::is('admin/product/trash') ? 'active' : '' }}">
                                            <i class="fas fa-trash nav-icon"></i>
                                            <p>Recycle bin</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <!--<li class="nav-item">-->
                            <!--    <a href="#" class="nav-link">-->
                            <!--        <i class="nav-icon fas fa-chart-bar"></i>-->
                            <!--        <p>-->
                            <!--            Analysis-->
                            <!--            <i class="fas fa-angle-left right"></i>-->
                            <!--        </p>-->
                            <!--    </a>-->
                            <!--    <ul class="nav nav-treeview">-->
                            <!--        <li class="nav-item">-->
                            <!--            <a href="#" class="nav-link">-->
                            <!--                <i class="far fa-circle nav-icon"></i>-->
                            <!--                <p>Analysis</p>-->
                            <!--            </a>-->
                            <!--        </li>-->

                            <!--    </ul>-->
                            <!--</li>-->
                            @endif
                            @if (in_array('order', $permission))
                            <li class="nav-header">ORDER SECTION</li>
                            <li class="nav-item {{ Request::is('admin/order/incomplete') ? 'menu-open' : '' }}{{ Request::is('admin/order/details') ? 'menu-open' : '' }}{{ Request::is('admin/order/list') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link {{ Request::is('admin/order/incomplete') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-cart-plus"></i>
                                    <p>
                                        Manage
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('admin/order/incomplete')}}" class="nav-link {{ Request::is('admin/order/incomplete') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Incomplete Order(s)</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('admin/order/list')}}" class="nav-link {{ Request::is('admin/order/list') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Order List</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ Request::is('admin/accounts') ? 'menu-open' : '' }}{{ Request::is('admin/accounts/cash/transfer') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-hand-holding-usd"></i>
                                    <p>
                                        Cashbook
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('admin/accounts')}}" class="nav-link {{ Request::is('admin/accounts') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Cash History</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('admin/accounts/cash/transfer')}}" class="nav-link {{ Request::is('admin/accounts/cash/transfer') ? 'active' : '' }}"> 
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Cash Transfer</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ Request::is('admin/report/sale') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-chart-pie"></i>
                                    <p>
                                        Report
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href='{{url("admin/report/sale")}}' class="nav-link {{ Request::is('admin/report/sale') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Sale</p>
                                        </a>
                                    </li>
                                    <!--<li class="nav-item">-->
                                    <!--    <a href="#" class="nav-link">-->
                                    <!--        <i class="far fa-circle nav-icon"></i>-->
                                    <!--        <p>Custom</p>-->
                                    <!--    </a>-->
                                    <!--</li>-->
                                </ul>
                            </li>
                            @endif
                            @if (in_array('customer', $permission))
                            <li class="nav-header">CUSTOMERS SECTION</li>
                            <li class="nav-item">
                                <a href="{{url('admin/customer/list')}}" class="nav-link {{ Request::is('admin/customer/list') ? 'active' : '' }}{{ Request::is('admin/customer/edit') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>List</p>
                                </a>
                            </li>
                            @endif
                            @if (in_array('marketing', $permission))
                            <li class="nav-header">MARKETING</li>
                            <li class="nav-item">
                                <a href='{{url("admin/send/sms")}}' class="nav-link {{ Request::is('admin/send/sms') ? 'active' : '' }}">
                                    <i class="fas fa-sms nav-icon"></i>
                                    <p>Send SMS</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href='{{url("admin/send/email")}}' class="nav-link {{ Request::is('admin/send/email') ? 'active' : '' }}">
                                    <i class="fas fa-envelope nav-icon"></i>
                                    <p>Send Email</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href='{{url("admin/customer/phone/request")}}' class="nav-link {{ Request::is('admin/customer/phone/request') ? 'active' : '' }}">
                                    <i class="fas fa-phone nav-icon"></i>
                                    <p>Phone Request</p>
                                </a>
                            </li>
                            @endif
                            @if (in_array('admin/user', $permission))
                            <li class="nav-header">ADMIN SECTION</li>
                            <li class="nav-item {{ Request::is('admin/user/create') ? 'menu-open' : '' }}{{ Request::is('admin/user/edit') ? 'menu-open' : '' }}{{ Request::is('admin/user/list') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>
                                        Manage
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('admin/user/create')}}" class="nav-link {{ Request::is('admin/user/create') ? 'active' : '' }}">
                                            <i class="far fa-plus-square nav-icon"></i>
                                            <p>Add</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('admin/user/list')}}" class="nav-link {{ Request::is('admin/user/list') ? 'active' : '' }}">
                                            <i class="fas fa-cog nav-icon"></i>
                                            <p>Manage</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ Request::is('admin/user/role/create') ? 'menu-open' : '' }}{{ Request::is('admin/user/role/edit') ? 'menu-open' : '' }}{{ Request::is('admin/user/role/list') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-user-tag"></i>
                                    <p>
                                        Role Permission
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{url('admin/user/role/create')}}" class="nav-link {{ Request::is('admin/user/role/create') ? 'active' : '' }}">
                                            <i class="far fa-plus-square nav-icon"></i>
                                            <p>Add</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url('admin/user/role/list')}}" class="nav-link {{ Request::is('admin/user/role/list') ? 'active' : '' }}">
                                            <i class="fas fa-cog nav-icon"></i>
                                            <p>Manage</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                            <li class="nav-item">
                                <a href="{{url('admin/user/profile/manage')}}" class="nav-link {{ Request::is('admin/user/profile/manage') ? 'active' : '' }}">
                                    <i class="fas fa-id-card nav-icon"></i>
                                    <p>Profile</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="loading-modal"></div>
                @yield('content')
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="float-right d-none d-sm-block">
                    <b>Version</b> 3.1.0
                </div>
                <strong>Copyright &copy; 2021 <a href="#">Pride Limited</a>.</strong> All rights reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        <!-- jQuery UI 1.11.4 -->
        <script src="{{asset('assets/admin/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
        <!-- Bootstrap 4 -->
        <script src="{{asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <!-- DataTables  & Plugins -->
        <script src="{{asset('assets/admin/plugins/select2/js/select2.full.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/jszip/jszip.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/pdfmake/pdfmake.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/pdfmake/vfs_fonts.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
        <!-- Bootstrap Switch -->
        <script src="{{asset('assets/admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
        <!-- ChartJS -->
        <script src="{{asset('assets/admin/plugins/chart.js/Chart.min.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{asset('assets/admin/js/adminlte.min.js')}}"></script>
        <!-- Summernote -->
        <script src="{{asset('assets/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
        <script src="{{asset('assets/admin/fileinput/js/fileinput.min.js')}}"></script>
        <script>
            $(function () {
                var Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timerProgressBar: true,
                  timer: 3000
                });
                $('#summernote').summernote();
                $("input[data-bootstrap-switch]").each(function(){
                  $(this).bootstrapSwitch('state', $(this).prop('checked'));
                })
                $("#input-file").fileinput({
                    showUpload: false,
                    dropZoneEnabled: true,
                    maxFileCount: 10,
                    inputGroupClass: "input-group-md"
                });
                 $('.select2').select2()
                 bsCustomFileInput.init();
                $("#example1").DataTable({
                    "responsive": true, "lengthChange": false, "autoWidth": false,
                    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
                }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                });
                $("#datasync").click(function(){
                 //   alert('ok');
                 var url = base_url + "/admin/pos/product/addformpos";
                 $(".loading-modal").show();
                 $.ajax({
                     url:url,
                     type: 'GET',
                     success: function (data) {
                        $(".loading-modal").hide();
                        //alert(data); 
                          Toast.fire({
                            icon: 'success',
                            title: data
                          })
                        location.reload();
                     }
                   });
                });
                function newOrder()
                {
                    $.ajax({
                     url:base_url + "/admin/order/notify",
                     type: 'GET',
                     success: function (data) {
                        console.log(data); 
                        if(data > 0){
                          var str="&nbsp; You have pending "+ data + "&nbsp;new order(s)";
                          Toast.fire({
                            icon: 'info',
                            title: str
                          })
                        $("#new-order").html(data)
                        }
                     }
                   });
                }
                function newExchange()
                {
                    $.ajax({
                     url:base_url + "/admin/order/exchange/notify",
                     type: 'GET',
                     success: function (data) {
                        console.log(data); 
                        if(data > 0){
                          var str="&nbsp; You have pending "+ data + "&nbsp;exchange request";
                          Toast.fire({
                            icon: 'warning',
                            title: str
                          })
                        }
                     }
                   });
                }
                setInterval(function()
                {
                    newOrder();
                }, 8000); //300000 is 5minutes in ms
                setInterval(function()
                {
                    newExchange();
                }, 300000); //300000 is 5minutes in ms
            });
        </script>
    </body>
</html>
