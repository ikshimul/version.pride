<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <link rel="icon" type="image/png" href="{{asset('assets_admin/images/logo.png')}}">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{asset('assets_admin/bootstrap/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

        <link rel="stylesheet" href="{{asset('assets_admin/plugins/iCheck/all.css')}}">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/colorpicker/bootstrap-colorpicker.min.css')}}">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/timepicker/bootstrap-timepicker.min.css')}}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/datatables/dataTables.bootstrap.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('assets_admin/dist/css/fileinput.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/dist/css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/dist/css/fileinput.min.css')}}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{asset('assets_admin/dist/css/skins/_all-skins.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/pace/pace.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/morris/morris.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
        <!-- daterange picker -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/daterangepicker/daterangepicker.css')}}">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/datepicker/datepicker3.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_admin/jquery-ui.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/toastr.css')}}" />

        <script src="{{asset('assets_admin/highcharts.js')}}"></script>
        <script src="{{asset('assets_admin/highcharts-more.js')}}"></script>
        <script src="{{asset('assets_admin/exporting.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/jQuery/jquery-3.1.1.min.js')}}"></script>
        
       <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script> -->

        <!-- iCheck for checkboxes and radio inputs -->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <script>
var base_url = "{{ URL::to('') }}";
var csrf_token = "{{ csrf_token() }}";
        </script>
        <style>
            .bv-form .help-block {
                margin-bottom: 0;
            }
            .bv-form .tooltip-inner {
                text-align: left;
            }
            .nav-tabs li.bv-tab-success > a {
                color: #3c763d;
            }
            .nav-tabs li.bv-tab-error > a {
                color: #a94442;
            }

            .bv-form .bv-icon-no-label {
                top: 0;
            }

            .bv-form .bv-icon-input-group {
                top: 0;
                z-index: 100;
            }
            .error{
                color: red;
                padding: 5px;
            }
            .loading-modal {
            display:    none;
            position:   fixed;
            z-index:    1000;
            top:        0;
            left:       0;
            height:     100%;
            width:      100%;
            background: rgba( 255, 255, 255, .8 ) 
                        url("{{url('/')}}/storage/app/public/loading.gif") 
                        50% 50% 
                        no-repeat;
        }
        button.toast-close-button {
            height: 0px;
        }
        .skin-yellow-light .main-header .logo {
            background-color: #f39c12c9;
        }
        
        span.logo-lg{
            position: relative;
            top: -2px;
            left: 0%;
           }
        img.logo-tab{
               width: 42%;
           }
        @media only screen and (max-width: 600px) {
          span.logo-lg{
            position: relative;
            top: 0px;
            left: 0%;
           }
           img.logo-tab{
               width: 27%;
           }
        }
        .bg-gray {
            color: white;
            background-color: #3b3092 !important;
        }
        </style>
        <script>
            (function(w,d,s,g,js,fs){
              g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
              js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
              js.src='https://apis.google.com/js/platform.js';
              fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
            }(window,document,'script'));
       </script>
    </head>
    <?php
      use App\Http\Controllers\admin\ManageOrder;
      use App\Http\Controllers\admin\DashboardController;
      $permission = DashboardController::getRolePermission();
      $total_incomplete_order = ManageOrder::getTotlaIncomplte_order();
      $total_phone_request = ManageOrder::getNewPhoneRequest();
      $total_phone_request_list = ManageOrder::getNewPhoneRequestList();
    ?>
    <!--hold-transition skin-blue sidebar-mini -->
    <body ng-app="crudApp" ng-controller="crudController" class="sidebar-mini skin-yellow-light">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="{{url('/pride-admin')}}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>PRI</b>DE</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img class="logo-tab" src="{{url('/')}}/storage/app/public/logo_new.png"/></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li>
                                <a href="#" id="datasync"> Data Sync <i class="fa fa-refresh" aria-hidden="true"></i></a>
                            </li>
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-phone"></i>
                                    <span class="label label-danger"><?php echo $total_phone_request;?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have <?php echo $total_phone_request;?> phone request(s)</li>
                                    <li>
                                        <ul class="menu">
                                            <?php foreach($total_phone_request_list as $p_list) { ?>
                                            <li>
                                                <a href="#">
                                                <div class="pull-left">
                                                    <?php echo $p_list->phnumber_name;?>
                                                </div>
                                                <p><?php echo $p_list->phnumber_number;?></p>
                                                <p><?php echo $p_list->comment_box;?></p>
                                              </a>
                                            </li>
                                          <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="footer">
                                        <a href="{{url('/phone-request')}}">See All Phone request(s)</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-cart-plus"></i>
                                    <span class="label label-danger">{{$total_incomplete_order}}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have {{$total_incomplete_order}} new order</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                  
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="{{url('manage-incomplete-order')}}"> Go to incomplete order page</a></li>
                                </ul>
                            </li>
                            <!-- Tasks: style can be found in dropdown.less -->
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{ URL::to('') }}/storage/app/public/admin-user/{{ Auth::guard('admin')->user()->image }}" class="user-image" alt="User Image">
                                    <span class="hidden-xs">{{ Auth::guard('admin')->user()->name }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="{{ URL::to('') }}/storage/app/public/admin-user/{{ Auth::guard('admin')->user()->image }}" class="img-circle" alt="User Image">
                                        <p>
                                            {{ Auth::guard('admin')->user()->name }}
                                            <small>Since <?php $join=strtotime(Auth::guard('admin')->user()->created_at); 
                                            echo date('d M , Y',$join);?></small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="{{url('/admin-profile')}}" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{url('/admin/logout')}}" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button 
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>-->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{ URL::to('') }}/storage/app/public/admin-user/{{ Auth::guard('admin')->user()->image }}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>{{ Auth::guard('admin')->user()->name }}</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <br>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header"></li>
                        <?php if (in_array('admin', $permission)) { ?>
                        <li class="{{ Request::is('admin') ? 'active' : '' }} treeview">
						  <a href="#">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
							<span class="pull-right-container">
							  <i class="fa fa-angle-left pull-right"></i>
							</span>
						  </a>
						  <ul class="treeview-menu">
							<li class="{{ Request::is('admin') ? 'active' : '' }}"><a href="{{url('/admin')}}"><i class="fa fa-circle-o"></i> Dashboard</a></li>
						  </ul>
						</li>
						<?php } ?>
						<?php if (in_array('category-menu', $permission)) { ?>
                        <li class="{{ Request::is('manage-category') ? 'active' : '' }}{{ Request::is('add-subcat') ? 'active' : '' }}{{ Request::is('manage-subpro') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-list" aria-hidden="true"></i> <span>Category</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('manage-category') ? 'active' : '' }}"><a href="{{url('/manage-category')}}"><i class="fa fa-circle-o"></i> Manage Category</a></li>
                                <li class="{{ Request::is('add-subcat') ? 'active' : '' }}"><a href="{{url('/add-subcat')}}"><i class="fa fa-circle-o"></i> Add New Sub Category</a></li>
                                <li class="{{ Request::is('manage-subpro') ? 'active' : '' }}"><a href="{{url('/manage-subpro')}}"><i class="fa fa-circle-o"></i> Browse All Sub Category</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                        <li class="{{ Request::is('admin/campaign/add') ? 'active' : '' }}{{ Request::is('admin/campaign/list') ? 'active' : '' }}{{ Request::is('admin/campaign/edit') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-list" aria-hidden="true"></i> <span>Campaign</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('admin/campaign/add') ? 'active' : '' }}"><a href="{{url('admin/campaign/add')}}"><i class="fa fa-circle-o"></i> Add Campaign</a></li>
                                <li class="{{ Request::is('admin/campaign/list') ? 'active' : '' }}"><a href="{{url('admin/campaign/list')}}"><i class="fa fa-circle-o"></i> Manage Campaign</a></li>
                            </ul>
                        </li>
                        <!--<li class="treeview">
                            <a href="#">
                                <i class="fa fa-arrows-alt" aria-hidden="true"></i> <span>Manage Size</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Add New Product Size</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Browse Product Sizes</a></li>
                            </ul>
                        </li> --->
                        <?php if (in_array('product-menu', $permission)) { ?>
                        <li class="{{ Request::is('admin/product/add') ? 'active' : '' }}{{ Request::is('admin/product/list') ? 'active' : '' }}{{ Request::is('show-deactive-product') ? 'active' : '' }}{{ Request::is('update-quantity-search') ? 'active' : '' }}{{ Request::is('show-trash-product') ? 'active' : '' }}{{ Request::is('admin/pos/product/add') ? 'active' : '' }}{{ Request::is('get-item-list') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-product-hunt" aria-hidden="true"></i>
                                <span>Product</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('admin/product/add') ? 'active' : '' }}"><a href="{{url('admin/product/add')}}"><i class="fa fa-circle-o"></i> Add Product</a></li>
                                <li class="{{ Request::is('admin/pos/product/add') ? 'active' : '' }}"><a href="{{url('admin/pos/product/add')}}"><i class="fa fa-circle-o"></i> Add Product By POS</a></li>
                                <li class="{{ Request::is('admin/product/list') ? 'active' : '' }}"><a href="{{url('admin/product/list')}}"><i class="fa fa-circle-o"></i> View All Product</a></li>
                                <li class="{{ Request::is('update-quantity-search') ? 'active' : '' }}"><a href="{{url('/update-quantity-search')}}"><i class="fa fa-circle-o"></i> Update Quntity</a></li>
                                <li class="{{ Request::is('show-deactive-product') ? 'active' : '' }}"><a href="{{url('/show-deactive-product')}}"><i class="fa fa-circle-o"></i> Deactive Products</a></li>
                                <li class="{{ Request::is('show-trash-product') ? 'active' : '' }}"><a href="{{url('show-trash-product')}}"><i class="fa fa-circle-o"></i>Recycle bin</a></li>
                                
                                <li class="{{ Request::is('get-item-list') ? 'active' : '' }}"><a href="{{url('/get-item-list')}}"><i class="fa fa-circle-o"></i> POS Item List</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if (in_array('product-analysis', $permission)) { ?>
                        <li class="{{ Request::is('product-analysis') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-product-hunt" aria-hidden="true"></i>
                                <span>Product Analysis</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('product-analysis') ? 'active' : '' }}"><a href="{{url('product-analysis')}}"><i class="fa fa-circle-o"></i> Product Analysis</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if (in_array('manage-order-menu', $permission)) { ?>
                        <li class="{{ Request::is('admin/order/incomplete') ? 'active' : '' }}{{ Request::is('manage-all-order') ? 'active' : '' }}{{ Request::is('order-report') ? 'active' : '' }}{{ Request::is('search-order') ? 'active' : '' }}{{ Request::is('all-order') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                <span>Manage Order</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                    <small class="label pull-right bg-red">
                                       {{$total_incomplete_order}}
                                    </small>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('admin/order/incomplete') ? 'active' : '' }}"><a href="{{url('admin/order/incomplete')}}"><i class="fa fa-circle-o"></i> Incomplete Order 	
                                        <span class="pull-right-container">
                                            <small class="label pull-right bg-blue">{{$total_incomplete_order}}</small>
                                        </span></a></li>
                                <li class="{{ Request::is('all-order') ? 'active' : '' }}"><a href="{{url('all-order')}}"><i class="fa fa-circle-o"></i> All Order</a></li>
                                <!--<li class="{{ Request::is('manage-all-order') ? 'active' : '' }}"><a href="{{url('manage-all-order')}}"><i class="fa fa-circle-o"></i>Order Filtering</a></li>-->
                                <li class="{{ Request::is('order-report') ? 'active' : '' }}"><a href="{{url('/order-report')}}"><i class="fa fa-circle-o"></i> Today Report</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if (in_array('cashbook', $permission)) { ?>
                        <li class="{{ Request::is('cash-book-histroy') ? 'active' : '' }}{{ Request::is('cash-out') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-money" aria-hidden="true"></i>
                                <span>CashBook</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                    <!--<small class="label pull-right bg-red">
                                        </small> --->
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('cash-book-histroy') ? 'active' : '' }}"><a href="{{url('/cash-book-histroy')}}"><i class="fa fa-circle-o"></i>Cashbook 	
                                        <span class="pull-right-container">
                                            <small class="label pull-right bg-blue"></small>
                                        </span></a></li>
                                    <li class="{{ Request::is('cash-out') ? 'active' : '' }}"><a href="#"><i class="fa fa-circle-o"></i>Cash Transfer</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                        <!--    <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-database" aria-hidden="true"></i> <span> Store/Outlet</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href=""><i class="fa fa-circle-o"></i> Add New Store/Outlet</a></li>
                                    <li><a href=""><i class="fa fa-circle-o"></i> Browse All Store/Outlet</a></li>
                                </ul>
                            </li> --->
                            <?php if (in_array('manage-content', $permission)) { ?>
                            <li class="{{ Request::is('add-slider') ? 'active' : '' }}{{ Request::is('manage-slider') ? 'active' : '' }}{{ Request::is('add-banner') ? 'active' : '' }}{{ Request::is('manage-banner') ? 'active' : '' }}{{ Request::is('add-popup') ? 'active' : '' }}{{ Request::is('manage-popup') ? 'active' : '' }} treeview">
                                <a href="#">
                                    <i class="fa fa-home" aria-hidden="true"></i> <span>Manage Home Page</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="{{ Request::is('add-slider') ? 'active' : '' }}{{ Request::is('manage-slider') ? 'active' : '' }} treeview">
                                        <a href="#"><i class="fa fa-circle-o"></i> Slider
                                            <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                        </a>
                                        <ul class="treeview-menu">
                                            <li class="{{ Request::is('add-slider') ? 'active' : '' }}"><a href="{{url('/add-slider')}}"><i class="fa fa-circle-o"></i> Add Slider</a></li>
                                            <li class="{{ Request::is('manage-slider') ? 'active' : '' }}"><a href="{{url('/manage-slider')}}"><i class="fa fa-circle-o"></i> Manage Slider</a></li>
                                        </ul>
                                    </li>
                                    <li class="{{ Request::is('add-banner') ? 'active' : '' }}{{ Request::is('manage-banner') ? 'active' : '' }} treeview">
                                        <a href="#"><i class="fa fa-circle-o"></i> Banner
                                            <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                        </a>
                                        <ul class="treeview-menu">
                                            <li class="{{ Request::is('add-banner') ? 'active' : '' }}"><a href="{{url('/add-banner')}}"><i class="fa fa-circle-o"></i> Add Banner</a></li>
                                            <li class="{{ Request::is('manage-banner') ? 'active' : '' }}"><a href="{{url('/manage-banner')}}"><i class="fa fa-circle-o"></i> Manage Banner</a></li>
                                        </ul>
                                    </li>
                                    <li class="{{ Request::is('add-popup') ? 'active' : '' }}{{ Request::is('manage-popup') ? 'active' : '' }} treeview">
                                        <a href="#"><i class="fa fa-circle-o"></i> Popup
                                            <span class="pull-right-container">
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                        </a>
                                        <ul class="treeview-menu">
                                            <li class="{{ Request::is('add-popup') ? 'active' : '' }}"><a href="{{url('/add-popup')}}"><i class="fa fa-circle-o"></i> Add Popup</a></li>
                                            <li class="{{ Request::is('manage-popup') ? 'active' : '' }}"><a href="{{url('/manage-popup')}}"><i class="fa fa-circle-o"></i> Manage Popup</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <?php } ?>
                            <!--- <li>
                                <a href="">
                                    <i class="fa fa-link" aria-hidden="true"></i> <span>Manage All Inner Page</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-cog" aria-hidden="true"></i> <span>Site Setting</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Site Configuation</a></li>
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Site Appearance</a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-user" aria-hidden="true"></i> <span>Admin Manager</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li><a href=""><i class="fa fa-circle-o"></i> Add New User</a></li>
                                    <li><a href=""><i class="fa fa-circle-o"></i> Manage All User</a></li>
                                </ul>
                            </li>-->
                            <?php if (in_array('user-menu', $permission)) { ?>
                            <li class="{{ Request::is('add-user') ? 'active' : '' }}{{ Request::is('view-user') ? 'active' : '' }}{{ Request::is('add-role') ? 'active' : '' }}{{ Request::is('view-role') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-user"></i> <span>Admin User</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('add-user') ? 'active' : '' }}{{ Request::is('view-user') ? 'active' : '' }} treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i> Manage User
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="{{ Request::is('add-user') ? 'active' : '' }}"><a href="{{url('/add-user')}}"><i class="fa fa-circle-o"></i> Add User</a></li>
                                        <li class="{{ Request::is('view-user') ? 'active' : '' }}"><a href="{{url('/view-user')}}"><i class="fa fa-circle-o"></i> View User</a></li>
                                    </ul>
                                </li>
                                <li class="{{ Request::is('add-role') ? 'active' : '' }}{{ Request::is('view-role') ? 'active' : '' }} treeview">
                                    <a href="#"><i class="fa fa-circle-o"></i> Manage Permission Role
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li class="{{ Request::is('add-role') ? 'active' : '' }}"><a href="{{url('/add-role')}}"><i class="fa fa-circle-o"></i> Add Role</a></li>
                                        <li class="{{ Request::is('view-role') ? 'active' : '' }}"><a href="{{url('/view-role')}}"><i class="fa fa-circle-o"></i> View Role</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>
                        <!--<li class="{{ Request::is('site-user') ? 'active' : '' }}">
                            <a href="{{url('/site-user')}}">
                                <i class="fa fa-users" aria-hidden="true"></i> <span>Site User</span>
                            </a>
                        </li> --->
                         <?php if (in_array('manage-customer', $permission)) { ?>
                        <li class="{{ Request::is('site-user') ? 'active' : '' }}{{ Request::is('customer-send-message-form') ? 'active' : '' }}{{ Request::is('customer-send-sms') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>Manage Customer</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                    <!--<small class="label pull-right bg-red">
                                        </small> --->
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('site-user') ? 'active' : '' }}"><a href="{{url('/site-user')}}"><i class="fa fa-circle-o"></i>Customer List	
                                    <span class="pull-right-container">
                                        <small class="label pull-right bg-blue"></small>
                                    </span>
                                </a></li>
                                <li class="{{ Request::is('customer-send-message-form') ? 'active' : '' }}"><a href="{{url('/customer-send-message-form')}}"><i class="fa fa-circle-o"></i>Send Email</a></li>
                                <li class="{{ Request::is('customer-send-sms') ? 'active' : '' }}"><a href="{{url('/customer-send-sms')}}"><i class="fa fa-circle-o"></i>Send SMS</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                         <?php if (in_array('phone-request', $permission)) { ?>
                        <li class="{{ Request::is('phone-request') ? 'active' : '' }}">
                            <a href="{{url('/phone-request')}}">
                                <i class="fa fa-phone" aria-hidden="true"></i> <span>Phone Request</span>
                            </a>
                        </li> 
                        <?php } ?>
                        <?php if (in_array('Reports', $permission)) { ?>
                        <li class="{{ Request::is('sale-reports') ? 'active' : '' }}{{ Request::is('custom-sale-reports') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                <span>Reports</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                    <!--<small class="label pull-right bg-red">
                                        </small> --->
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('sale-reports') ? 'active' : '' }}"><a href="#"><i class="fa fa-circle-o"></i>Sales Report	
                                    <span class="pull-right-container">
                                        <small class="label pull-right bg-blue"></small>
                                    </span>
                                </a>
                                </li>
                                <li class="{{ Request::is('custom-sale-reports') ? 'active' : '' }}"><a href="#"><i class="fa fa-circle-o"></i>Custom Report	
                                    <span class="pull-right-container">
                                        <small class="label pull-right bg-blue"></small>
                                    </span>
                                </a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if (in_array('promo-code', $permission)) { ?>
                        <li class="{{ Request::is('coupon-generate') ? 'active' : '' }}{{ Request::is('manage-promcodes') ? 'active' : '' }} treeview">
                            <a href="#">
                                <i class="fa fa-money" aria-hidden="true"></i>
                                <span>Promo Codes</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                    <!--<small class="label pull-right bg-red">
                                        </small> --->
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::is('coupon-generate') ? 'active' : '' }}"><a href="{{url('/coupon-generate')}}"><i class="fa fa-circle-o"></i>Generate</li>
                                <li class="{{ Request::is('manage-promcodes') ? 'active' : '' }}"><a href="{{url('/manage-promcodes')}}"><i class="fa fa-circle-o"></i>Manage</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                        <li class="header"></li>
                       <li class="{{ Request::is('admin-profile') ? 'active' : '' }}"><a href="{{url('/admin-profile')}}"><i class="fa fa-circle-o text-red"></i> <span>Profile Manage</span></a></li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Cont                                                                                                                                                                                                        ent Wrapper. Contai                                                                                                                                                                                                        ns page content -->
            <div class="content-wrapper">
                <div class="loading-modal"></div>
                <!-- Main content -->
                @yield('content')
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Laravel Version</b> {{ App::VERSION() }}
                </div>
                <strong>Copyright &copy; <?php echo date('Y');?> <a href="#">Pride Limited</a>.</strong> All rights
                reserved.
            </footer>
        </div>

        <!--<script src="{{asset('assets_admin/jquery-ui.min.js')}}"></script>-->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
$.widget.bridge('uibutton', $.ui.button);</script>
        <script src="{{asset('assets_admin/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/pace/pace.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/select2/select2.full.min.js')}}"></script>
        <!-- InputMask -->
        <script src="{{asset('assets_admin/plugins/input-mask/jquery.inputmask.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
        <script src="{{asset('assets_admin/raphael-min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/morris/morris.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/knob/jquery.knob.js')}}"></script>
        <script src="{{asset('assets_admin/moment.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/fastclick/fastclick.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/adminlte.min.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/fileinput.min.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/pages/dashboard.js')}}"></script>
        <script src="{{asset('assets_admin/plugins/chartjs/Chart.min.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/pages/dashboard2.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/demo.js')}}"></script>
        <script src="{{asset('assets_admin/dist/js/form_validation.js')}}"></script>
        <script src="{{asset('assets_admin/ajax_modal.js')}}"></script>
        <script src="{{ asset('assets_admin/custom-script.js') }}?4"></script>
        <script src="{{asset('assets/js/toastr.js')}}"></script>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false
            });
            $("#datasync").click(function(){
             //   alert('ok');
             var url = base_url + "/save-item-list/";
             $(".loading-modal").show();
             $.ajax({
                 url:url,
                 type: 'GET',
                 success: function (data) {
                    $(".loading-modal").hide();
                    alert(data); 
                    location.reload();
                 }
               });
            });
        });
        </script>
        <script>
            $(function () {
                //Initialize Select2 Elements
                $(".select2").select2();
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                            startDate: moment().subtract(29, 'days'),
                            endDate: moment()
                        },
                        function (start, end) {
                            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                        }
                );

                //Date picker
                $('#datepicker').datepicker({
                    autoclose: true
                });

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });

                //Colorpicker
                $(".my-colorpicker1").colorpicker();
                //color picker with addon
                $(".my-colorpicker2").colorpicker();

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });
        </script>
        @yield('appended.script')
        @toastr_render
    </body>
</html>
