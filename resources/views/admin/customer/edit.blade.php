@extends('admin.layouts.app')
@section('title','Pride Limited | Customer')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Customer</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Customer</a></li>
          <li class="breadcrumb-item"><a href="#">Edit</a></li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Customer Information Update</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
       <form name="add_subpro" action="{{url('/admin/customer/update')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$user->id}}" class="form-control" required />
        <input type="hidden" name="details_id" value="{{$user->details_id}}" class="form-control" required />
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                 @if (session('save'))
                    <div class="callout callout-success">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('save') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="callout callout-danger">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('error') }}
                    </div>
                    @endif
                </div>
                <div class="col-md-6">
                     <fieldset style="margin:10px; padding:5px 20px; border:2px solid #00c0ef44">
                         <legend style="padding:5px 20px; text-align:center; width:auto">Basic Information</legend>
                        
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" name="name" value="{{$user->name}}" class="form-control" required />
                            <span class="text-danger"><?php echo $errors->first('name') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="{{$user->email}}" class="form-control" required />
                            <span class="text-danger"><?php echo $errors->first('email') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Mobile No.</label>
                            <input type="text" name="mobile_no" value="{{$user->mobile_no}}" class="form-control" readonly />
                            <span class="text-danger"><?php echo $errors->first('mobile_no') ?></span>
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset style="margin:10px; padding:5px 20px; border:2px solid #00c0ef44">
                    <legend style="padding:5px 20px; text-align:center; width:auto">Details Information</legend>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone" value="{{$user->phone}}" class="form-control" readonly />
                            <span class="text-danger"><?php echo $errors->first('phone') ?></span>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea type="text" name="address" class="form-control" rows="3">{{$user->address}}</textarea>
                            <span class="text-danger"><?php echo $errors->first('address') ?></span>
                        </div>
                        <div class="form-group row">
        					<div class="col-sm-6">
        					    <label for="region">Region</label>
        					    <select type="text" class="form-control select2"  name="region" id="region"  required>
            						<option value="">-- select region --</option>
            						@foreach($regions as $region)
									  <option value="{{$region->id}}">{{$region->name}}</option>
									@endforeach
            					</select>
            					<span class="text-danger"><?php echo $errors->first('region') ?></span>
        					</div>
        					<div class="col-sm-6">
        					    <label for="city">Sub Category</label>
        						<select class="form-control select2" type="text" name="city" id="city" required>
        							<option value="">-- select sub category --</option>
        						</select>
        						<span class="text-danger"><?php echo $errors->first('city') ?></span>
        					</div>
        				</div>
        				<div class="form-group">
                            <label>Zip</label>
                            <input type="text" name="zip" value="{{$user->zip}}" class="form-control">
                            <span class="text-danger"><?php echo $errors->first('zip') ?></span>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
          <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> <i class="fas fa-save"></i> Update Info</button>
    </div>
    </form>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
document.getElementById('region').value = "<?php echo $user->region; ?>"
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
    var region_id =$("#region").val();
		var url_op = base_url + "/get-citylist/" + region_id;
		$.ajax({
			url: url_op,
			type: 'GET',
			dataType: 'json',
			data: '',
			success: function (data) {
				//alert(data);
				$('#city').empty();
				$('#city').append('<option value=""> select city </option>');
				$.each(data, function (index, cityobj) {
					$('#city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
				});
				// $('#city').focus().select();
				 document.getElementById('city').value = "<?php echo $user->city; ?>"
				 
			}
		});
	   $("#region").on('change',function(){
	    var region_id =$("#region").val();
		var url_op = base_url + "/get-citylist/" + region_id;
		$.ajax({
			url: url_op,
			type: 'GET',
			dataType: 'json',
			data: '',
			success: function (data) {
				//alert(data);
				$('#city').empty();
				$('#city').append('<option value=""> select city </option>');
				$.each(data, function (index, cityobj) {
					$('#city').append('<option value="' + cityobj.id + '">' + cityobj.name + '</option>');
				});
				 $('#city').focus().select();
				 document.getElementById('city').value = "<?php echo $user->city; ?>"
				 
			}
		});
    });
});
</script>
@endsection
