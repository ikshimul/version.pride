@extends('admin.layouts.app')
@section('title','Phone Request')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Phone Request</h1>
      </div>
      <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item">Maketing</li>
            <li class="breadcrumb-item active">Phone Request</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Phone Requests</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12">
            @if (session('update'))
                <div class="callout callout-success">
                  <h5><i class="fas fa-info"></i> Note:</h5>
                  {{ session('update') }}
                </div>
                @endif
                @if (session('error'))
                <div class="callout callout-danger">
                  <h5><i class="fas fa-info"></i> Note:</h5>
                  {{ session('error') }}
                </div>
                @endif
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Customer Name</th>
                                <th>Phone Number</th>
                                <th>Message</th>
                                <th>Message Time</th>
                            </tr>
                        </thead>
                        <tbody>
                           @php $i=0 @endphp
                           @foreach($numbers as $number)
                           @php $i++ @endphp
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $number->phnumber_name; ?></td>
                                <td><?php echo $number->phnumber_number; ?></td>
                                <td><?php echo $number->comment_box; ?></td>
                                <td><?php $time = strtotime($number->created_at);
                                    echo date('d M , Y h:i A', $time); ?></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> 
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
