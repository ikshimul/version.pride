@extends('admin.layouts.app')
@section('title', 'Pride Limited | Send Email')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Send Email</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item">Marketing</li>
          <li class="breadcrumb-item active">Send Email</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Send Email</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
     <form action="{{url('admin/send/email')}}" method="post"  enctype="multipart/form-data" id="myForm">
    {{ csrf_field() }}
    <div class="card-body">
		<div class="pb-5">
			<div class="co-12">
                 @if (session('success'))
                    <div class="callout callout-success">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('success') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="callout callout-danger">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('error') }}
                    </div>
                    @endif

                      <div class="form-group">
                            <label for="check-option-1">Select User Type</label>
                              <div class="icheck-primary">
                                <input type="radio" id="user_1" value="single" name="user" required />
                                <label for="user_1" style="font-weight: 500;">
                                    Single User
                                </label>
                              </div>
                             <div class="icheck-primary">
                                <input type="radio" id="user_2" value="multi" name="user">
                                <label for="user_2" style="font-weight: 500;">
                                    All user
                                </label>
                              </div>
                            <span class="help-text"></span>
                      </div>
                    <div class="form-group" id="user_type">
                      <input type="email" class="form-control" name="emailto" id="q" placeholder="Email to:">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" name="subject" placeholder="Subject">
                    </div>
                     <div class="form-group">
                        <label>File Attach</label>
                        <div class="custom-file">
                          <input type="file" class="file" name="file" id="input-file"  data-browse-on-zone-click="false">
                          <span class="help-block" style="color:#06be1c">only .pdf file allow</span>  
                          <div class="help-block with-errors invalid-feedback">{{ $errors->first('file') }}</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <!--<textarea id="message" name="message" required ></textarea>-->
                        <textarea  placeholder="Message" name="message"
                            style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                </div>
            </div>
		</div>
		<div class="card-footer">
		   <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary float-right"> Send <i class="fa fa-arrow-circle-right"></i></button>
		</div>
		</form>   
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</section>
<!-- /.content -->
<script>
    $(document).ready(function(){
        $("#input-file").fileinput({
            showUpload: false,
            dropZoneEnabled: false,
            maxFileCount: 10,
            inputGroupClass: "input-group-md"
        });
        $('#message').summernote({
             height: 125,
             placeholder: 'Enter your email here'
        });
        $("#user_type").hide();
        $('#myForm input').on('change', function() {
           var user=($('input[name=user]:checked', '#myForm').val()); 
            if(user=='single'){
               $("#user_type").show(); 
            }else{
                $("#user_type").hide();
            }
        });
       
        $("#q").autocomplete({
    	  source: base_url+"/admin/send/email/search",
    	  minLength: 1,
    	  select: function(event, ui) {
    	  	$('#q').val(ui.item.value);
    	  	$('#idno').val(ui.item.idno);
    	  }
    	});
    });
</script>
@endsection