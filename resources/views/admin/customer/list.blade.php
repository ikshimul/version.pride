@extends('admin.layouts.app')
@section('title','Customer List')
@section('content')
<style>
    .invalid-feedback {
        display: block;
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Customer List</h1>
      </div>
      <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item">Customer</li>
            <li class="breadcrumb-item active">List</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Customer List</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
        <div class="box-body">
            <div class="row">
                <div class="col-12">
                @if (session('update'))
                    <div class="callout callout-success">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('update') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="callout callout-danger">
                      <h5><i class="fas fa-info"></i> Note:</h5>
                      {{ session('error') }}
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th style="text-align:center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               @php $i = 0;  @endphp
                               @foreach($users as $user)
                               @php $i++; @endphp
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $user->name; ?></td>
                                    <td><?php echo $user->email; ?></td>
                                    <td><?php echo $user->mobile_no; ?></td>
                                    <td><?php echo $user->address; ?></td>
                                    <td>
                                        <a class="btn bg-olive btn-flat btn-sm margin tdata" href='{{url("admin/customer/edit?id={$user->id}")}}'>Edit</a> 
                                        <?php if($user->active == 1){ ?>
                                           <a class="btn btn-success btn-flat btn-sm tdata" href='{{url("admin/popup/deactive?id={$user->id}")}}' alt="Deactive" title="Deactive">Active</a>
                                        <?php }else{ ?>
                                           <a class="btn btn-danger btn-flat btn-sm tdata" href='{{url("admin/popup/active?id={$user->id}")}}'  alt="Active" title="Active">Deactive</a>
                                        <?php } ?>
                                        <a class="btn btn-sm btn-flat btn-danger margin tdata" onclick="return confirm('Are you sure you want to delete this popup?');"  href='{{url("admin/popup/delete?id=$user->id")}}'> Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $("#input-file").fileinput({
        showUpload: false,
        dropZoneEnabled: true,
        maxFileCount: 10,
        inputGroupClass: "input-group-md"
    });
});
</script>
@endsection
