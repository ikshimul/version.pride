@extends('layouts.app')
@section('title','Pride Limited | A Pride Group venture')
@section('content')
<main id="maincontent">
    <section class="slider-section">
        <!-- desktop slider start  --->
        <div id="desktop-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @php($i=0)
                @foreach($slides as $slide)
                @if($slide->device == 'desktop')
                <div class="item <?php if ($i == 0) echo 'active'; ?>">
                    <a href='{{ $slide->url_link }}'><img src="{{url('/')}}/storage/app/public/slider/{{ $slide->slider_image }}" alt="Pride" style="width:100%;"></a>
                       <a class="btn shop-now" href="{{ $slide->url_link }}">Shop Now</a>
                </div>
                @php($i++)
                @endif
                @endforeach
            </div>
            <a class="left carousel-control" href="#desktop-carousel" data-slide="prev">
                <img class="prev_arrow" src="{{url('/')}}/storage/app/public/icon_prev.svg"/>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#desktop-carousel" data-slide="next">
                <img class="next_arrow" src="{{url('/')}}/storage/app/public/icon_next.svg"/>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- desktop slider end --->
        
        <!-- tablet slider start --->
            <div id="tablet-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @php($i=0)
                    @foreach($slides as $slide)
                    @if($slide->device == 'desktop')
                    <div class="item <?php if($i==0) echo 'active'; ?>">
                        <img src="{{url('/')}}/storage/app/public/slider/{{ $slide->slider_image }}" alt="Pride" style="width:100%;">
                          <a class="btn shop-now" href="{{ $slide->url_link }}">Shop Now</a>
                    </div>
                    @php($i++)
                    @endif
                    @endforeach
                </div>
                <a class="left carousel-control" href="#mobile-carousel" data-slide="prev">
                    <img class="prev_arrow" src="{{url('/')}}/storage/app/public/icon_prev.svg"/>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#mobile-carousel" data-slide="next">
                    <img class="next_arrow" src="{{url('/')}}/storage/app/public/icon_next.svg"/>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- tablet slider end --->
        
        <!-- mobile slider start  --->
        <div id="mobile-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @php($j=0)
                @foreach($slides as $slide)
                @if($slide->device == 'mobile')
                <div class="item <?php if ($j == 0) echo 'active'; ?>">
                    <img src="{{url('/')}}/storage/app/public/slider/{{ $slide->slider_image }}" alt="Pride" style="width:100%;">
                       <a class="btn shop-now" href="{{ $slide->url_link }}">Shop Now</a>
                </div>
                @php($j++)
                @endif
                @endforeach
            </div>
            <a class="left carousel-control" href="#mobile-carousel" data-slide="prev">
                <img class="prev_arrow" src="{{url('/')}}/storage/app/public/icon_prev.svg"/>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#mobile-carousel" data-slide="next">
                <img class="next_arrow" src="{{url('/')}}/storage/app/public/icon_next.svg"/>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- mobile slider end --->
        
        <!---<div id="desktop-carousel" class="slick-slider">
		<?php $i=0; foreach($slides as $slide){ $i++;
          if($slide->device == 'desktop'){ ?>
			<div>
			   <a  href="{{ $slide->url_link }}">
				  <img src="{{url('/')}}/storage/app/public/slider/{{ $slide->slider_image }}" alt="Pride" style="width:100%;">
			   </a>
			    <?php if($i !=4 && $i !=6){ ?>
			      <a class="btn shop-now" href="{{ $slide->url_link }}">Shop Now</a>
			   <?php } ?>
		   </div>
		  <?php } } ?>
		</div>
		<div id="mobile-carousel" class="slick-slider">
		<?php $j=0; foreach($slides as $slide){ $j++;
          if($slide->device == 'mobile'){ ?>
			<div>
			   <a href="{{ $slide->url_link }}">
				  <img src="{{url('/')}}/storage/app/public/slider/{{ $slide->slider_image }}" alt="Pride" style="width:100%;">
			   </a>
			  <?php if($j !=3 && $j !=5){ ?>
			      <a class="btn shop-now mobile_btn" href="{{ $slide->url_link }}">Shop Now</a>
			   <?php } ?>
		   </div>
		  <?php } } ?>
		</div>  --->
		
    </section>
    <section class="category-area mobile-margin">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
                    <div class="category-holder hidden-xs hidden-sm">
                        <div class="category-gallery">
                            <div class="mask">
                                <div class="slideset">
                                    <?php foreach ($banner_list as $banner) { ?>
                                        <div class="product">
                                            <div class="holder">
                                                <div class="img"><a href="<?php echo $banner->banner_link; ?>"><img src="{{ URL::to('') }}/storage/app/public/banner/<?php echo $banner->banner_image; ?>" alt="" width="100%" height="auto" /></a></div>
                                                <div class="info"><strong style="font-size:11pt;"><?php echo $banner->banner_title; ?></strong> <a class="btn btn-default" href="<?php echo $banner->banner_link; ?>">shop now</a></div>
                                            </div> 
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="category-holder hidden-lg">
                        <div class="row">
                            <?php foreach ($banner_list as $banner) { ?>
                                <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                    <div class="product add">
                                        <div class="holder">
                                            <div class="img"><a href="<?php echo $banner->banner_link; ?>"><img src="{{ URL::to('') }}/storage/app/public/banner/<?php echo $banner->banner_image; ?>" alt="" width="100%" height="auto" /></a></div>
                                            <div class="info"><strong style="font-size:11pt;"><?php echo $banner->banner_title; ?></strong> <a class="btn btn-default" href="<?php echo $banner->banner_link; ?>">shop now</a></div>
                                        </div> 
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="padding-top: 16px;padding-bottom: 16px;">
        <a href="{{url("/classic/classic-sari/7/40")}}"><img src="{{ URL::to('') }}/storage/app/public/spring-banner.jpg" class="img-responsive" /></a>
    </section>
    <section>
        <div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="row">
                            <div class="panel-block-row trending col-md-12">
                                <div class="content-block attribute-product-cat-block">
                                    <div class="content-heading">
                                        <h3 class="title">TRENDING NOW</h3>
                                        <!--<div class="block-note">Top views of the week</div>-->
                                        <div class="block-note">New arrivals of this week</div>
                                    </div>

                                    <div class="block widget mgs-products attribute-products-grid products-grid ">

                                        <div id="attribute" class="items row ">
                                            <?php
                                            foreach ($product_list as $product) {
                                                $product_name = str_replace(' ', '-', $product->product_name);
                                                $product_url = strtolower($product_name);
                                                $product_code = strtolower($product->product_styleref);
                                                $color_album = str_replace('/', '-', $product->productalbum_name);
                                                $color_album = strtolower($color_album);
                                                ?>
                                                <div class="product product-item ">
                                                    <div class="product-item-info " data-container="product-grid">
                                                        <div class="product-top">
                                                            <a href='{{url("shop/{$product_url}/{$product_code}/{$color_album}")}}'>
                                                                <img src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->product_img_thm; ?>"  alt="{{$product->product_name}}" class="img-responsive product-image-photo img-thumbnail  lazy" data-src="{{ URL::to('') }}/storage/app/public/pgallery/<?php echo $product->product_img_thm; ?>">                                                    
                                                            </a>
                                                        </div>
                                                        <div class="product details product-item-details">
                                                            <h5 class="product name product-item-name">
                                                                <a class="product-item-link" href='{{url("shop/{$product_url}/{$product_code}/{$color_album}")}}'>
                                                                    {{$product->product_name}}			
                                                                </a>
                                                            </h5>
                                                            <div class="swatch-opt-48902" data-role="swatch-option-{{$product->id}}"></div>
                                                            <div class="price-box price-final_price" data-role="priceBox" data-product-id="{{$product->id}}" data-price-box="product-id-{{$product->id}}">
                                                                <span class="normal-price">
                                                                    <span class="price-container price-final_price tax weee">
                                                                        <span class="price-label">As low as</span>
                                                                        <span id="product-price-{{$product->id}}" data-price-amount="{{$product->product_price}}" data-price-type="finalPrice" class="price-wrapper ">
                                                                            <span class="price">Tk&nbsp;{{number_format($product->product_price)}}</span></span>
                                                                    </span>
                                                                </span>
                                                            </div>        
                                                        </div>
                                                    </div>													
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            //require(['jquery', 'mgs/slick'], function ($) {
            jQuery(document).ready(function () {
              jQuery('.slick-slider').slick({
                      dots: false,
                      autoplay: true,
                      infinite: true,
                      speed: 500,
                      fade: true,
                      slidesToShow: 1,
                      //prevArrow: '<span class="pe-7s-angle-left"><img class="arrow-left" src="{{url('/')}}/storage/app/public/icon_prev.svg"></span>',
                      //nextArrow: '<span class="pe-7s-angle-right"><img class="arrow-right" src="{{url('/')}}/storage/app/public/icon_next.svg"></span>',
                      prevArrow:false,
                      nextArrow:false
                    });
                jQuery(".panel-block-row.trending .items.row").slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 1000,
                    arrows: true,
                    prevArrow: '<span class="pe-7s-angle-left"><img class="arrow-left" src="{{url('/')}}/storage/app/public/icon_prev.svg"></span>',
                    nextArrow: '<span class="pe-7s-angle-right"><img class="arrow-right" src="{{url('/')}}/storage/app/public/icon_next.svg"></span>',
                    responsive: [
                        {
                            breakpoint: 1199,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 360,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });

            });
            //});

        </script>
    </section>
    <?php if (!empty($popup)) { ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="authenticationPopup"  style="display: block;">
                        <div class="checkout-popup" style="display: block;">
                            <div class="popup-holder">
                                <div class="popup-frame">
                                    <div class="popup-block"><a class="popup-close" style="cursor: pointer;">
                                            <img src="{{url('/')}}/storage/app/public/popup-close.png" alt="close" />
                                        </a>
                                        <div class="img-area"><a href="{{url('/new-arrival')}}">
                                                <img src="{{url('/')}}/storage/app/public/popup/<?php echo $popup->image; ?>" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <style>
                                .beadcumarea{display:none !important;}
                                .checkout-popup {
                                    position: fixed;
                                    left: 0;
                                    top: 0;
                                    bottom: 0;
                                    right: 0;
                                    z-index: 999;
                                    overflow: hidden;
                                    background: rgba(40, 40, 40, 0.7); }
                                .popup-open .checkout-popup {
                                    display: block; 
                                }
                                .checkout-popup .popup-holder {
                                    display: table;
                                    width: 100%;
                                    height: 100%; }
                                .checkout-popup .popup-frame {
                                    display: table-cell;
                                    vertical-align: middle; }
                                @media (max-width: 500px) {
                                    .checkout-popup .popup-frame {
                                        vertical-align: top;
                                        padding: 50px 20px 0; } }
                                .checkout-popup .popup-block {
                                    max-width: 450px;
                                    margin: 0 auto;
                                    background: #fff;
                                    position: relative;
                                    text-align: center;
                                    color: #000;
                                    font-size: 15px;
                                    line-height: 18px;
                                    text-transform: uppercase;
                                    padding: 10px; }
                                .checkout-popup .popup-close {
                                    position: absolute;
                                    right: 0;
                                    top: 0;
                                    width: 30px;
                                    opacity: 0.7;
                                    margin: 2px 2px 0 0; }
                                @media (max-width: 500px) {
                                    .checkout-popup .popup-close {
                                        width: 25px; } 
                                }
                                .checkout-popup .popup-close:hover {
                                    opacity: 1; 
                                }
                                .checkout-popup img {
                                    display: block;
                                    width: 100%; 
                                }
                                .products-grid .product-item-info .product-item-details .product-item-name a{
                                    font-weight:600;
                                }
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</main>
@endsection

@section('appended.style')
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/owl.carousel.min.css')}}" />
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/owl.theme.default.min.css')}}" />
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/quickview-style.css')}}" />
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/home.css')}}?1" />
@endsection

@section('appended.script')
<script  type="text/javascript"  src="{{asset('assets/js/owl.js')}}"></script>
<script  type="text/javascript"  src="{{asset('assets/js/home.js')}}"></script>
@endsection
