<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\user\UserController;
use App\Http\Controllers\user\TrackOrderController;
use App\Http\Controllers\user\DashboardController;
use App\Http\Controllers\user\ExchangeController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AjaxCallController;
use App\Http\Controllers\product\UpdateProductController;
// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

//signup
Route::prefix('mobile')->group(function(){
   Route::get('/verify', [RegisterController::class, 'VerifyMobile']); 
   Route::post('otp/send', [RegisterController::class, 'SendOTP']); 
   Route::get('/otp/verify', [RegisterController::class, 'OTPverify'])->name('otp.verify'); 
   Route::post('/otp/confirm', [RegisterController::class, 'OTPConfirm'])->name('otp.verify.confirm'); 
});
Route::get('signup', [RegisterController::class, 'Signup'])->name('signup');
Route::get('register', [UserController::class, 'Register'])->name('register');
Route::post('custom-login', [LoginController::class, 'customLogin']);
//Social Login
Route::get('login/{service}', [LoginController::class, 'redirectToProvider']);
Route::get('login/{service}', [LoginController::class, 'redirectToProvider']);
Route::get('login/{service}/callback', [LoginController::class, 'handleProviderCallback']);

Route::get('auth/{service}', 'App\Http\Controllers\Auth\SocialLoginController@redirectToProvider');
Route::get('auth/{service}/callback', 'App\Http\Controllers\Auth\SocialLoginController@handleProviderCallback');
Route::post('google-login', 'App\Http\Controllers\Auth\SocialLoginController@googleNew');
Route::get('user/logout', 'App\Http\Controllers\HomeController@userlogout')->name('user.logout');

//footer page
Route::get('/work-with-us', 'App\Http\Controllers\HomeController@WorkWithUs')->name('WorkWithUs');
Route::get('/contact-us', 'App\Http\Controllers\HomeController@ContactUs')->name('ContactUs');
Route::get('/exchange-policy', 'App\Http\Controllers\HomeController@ExchangePolicy')->name('ExchangePolicy');
Route::get('/faq', 'App\Http\Controllers\HomeController@Faq')->name('Faq');
Route::get('/store-locator', 'App\Http\Controllers\HomeController@StoreLocator')->name('StoreLocator');
Route::get('/privacy-cookies', 'App\Http\Controllers\HomeController@PrivacyCookies')->name('PrivacyCookies');
Route::get('/about-us', 'App\Http\Controllers\HomeController@AboutUs')->name('AboutUs');
Route::get('/size-guide', 'App\Http\Controllers\HomeController@SizeGuide')->name('SizeGuide');
Route::get('/how-to-order/{id?}', 'App\Http\Controllers\HomeController@HowToOrder')->name('how.to');
Route::get('/how-to-order/product', 'App\Http\Controllers\HomeController@HowToOrder')->name('how.to');
Route::get('/how-to-order/place-order', 'App\Http\Controllers\HomeController@HowToOrder')->name('how.to');
Route::get('/how-to-order/registration', 'App\Http\Controllers\HomeController@HowToOrder')->name('how.to');
Route::get('/how-to-order/payment', ' App\Http\Controllers\HomeController@HowToOrder')->name('how.to');
Route::get('/how-to-order/{delivery?}', 'App\Http\Controllers\HomeController@HowToOrder')->name('how.to');
//loyalty program
Route::get('/loyalty-terms-&-conditions', 'App\Http\Controllers\HomeController@LoyaltyTerms')->name('loyalty.terms');
Route::get('/insider-rewards', 'App\Http\Controllers\HomeController@InsideRewards')->name('loyalty.insider');
Route::get('/insider-rewards-faq', 'App\Http\Controllers\HomeController@InsideRewardsFaq')->name('loyalty.insider.faq');

//product search
Route::get('/search/{id}', 'App\Http\Controllers\product\ProductController@search')->name('search');
Route::get('/search-new/{id}', 'App\Http\Controllers\product\ProductController@SearchNew')->name('search.new');
Route::get('/search-view/{id}', 'App\Http\Controllers\product\ProductController@SearchResultView')->name('search.view');

//product list
Route::get('all/{id}/', 'App\Http\Controllers\product\ProductController@Products')->name('products');
Route::get('new', 'App\Http\Controllers\product\ProductController@ProductsNew')->name('product.new');
Route::get('new/{id}/', 'App\Http\Controllers\product\ProductController@ProductsNew')->name('product.new');
Route::get('new/{id}/{id2}', 'App\Http\Controllers\product\ProductController@ProductsNewCatewise')->name('product.new.cat');
Route::get('women/{id}/', 'App\Http\Controllers\product\ProductController@ProductAll')->name('product.all');
Route::get('men/{id}/', 'App\Http\Controllers\product\ProductController@ProductAll')->name('product.all');
Route::get('kids/{id}/', 'App\Http\Controllers\product\ProductController@ProductAll')->name('product.all');
Route::get('homes/{id}/', 'App\Http\Controllers\product\ProductController@ProductAll')->name('product.all');

Route::get('women/{id1}/{id2}/', 'App\Http\Controllers\product\ProductController@ProductList')->name('product.list');
Route::get('men/{id1}/{id2}/', 'App\Http\Controllers\product\ProductController@ProductList')->name('product.list');
Route::get('kids/{id1}/{id2}/', 'App\Http\Controllers\product\ProductController@ProductList')->name('product.list');
Route::get('pride/{id1}/{id2}/', 'App\Http\Controllers\product\ProductController@ProductList')->name('product.list');

//product list new
//women
Route::get('demo-women/', 'App\Http\Controllers\product\UpdateProductController@ProductAllMaincatwiseDemo')->name('product.all');
Route::get('women/', 'App\Http\Controllers\product\UpdateProductController@ProductAllMaincatwise')->name('product.all');
Route::get('women/{id}/', 'App\Http\Controllers\product\UpdateProductController@ProductAll')->name('product.all');
Route::get('women/{id1}/{id2}/', 'App\Http\Controllers\product\UpdateProductController@ProductList')->name('product.list');
//mens
Route::get('men/', 'App\Http\Controllers\product\UpdateProductController@ProductAllMaincatwise')->name('product.all');
Route::get('men/{id}/', 'App\Http\Controllers\product\UpdateProductController@ProductAll')->name('product.all');
Route::get('men/{id1}/{id2}/', 'App\Http\Controllers\product\UpdateProductController@ProductList')->name('product.list');
//kids
Route::get('kids/', 'App\Http\Controllers\product\UpdateProductController@ProductAllMaincatwise')->name('product.all');
Route::get('kids/{id}/', 'App\Http\Controllers\product\UpdateProductController@ProductAll')->name('product.all');
Route::get('kids/{id1}/{id2}/', 'App\Http\Controllers\product\UpdateProductController@ProductList')->name('product.list');

Route::get('homes/', 'App\Http\Controllers\product\UpdateProductController@ProductAllMaincatwise')->name('product.all');
Route::get('homes/{id}', 'App\Http\Controllers\product\UpdateProductController@ProductAll')->name('product.all');
Route::get('homes/{id1}/{id2}/', 'App\Http\Controllers\product\UpdateProductController@ProductList')->name('product.all');

//occasions
Route::prefix('occasions')->group(function(){
   Route::get('/{id}', [UpdateProductController::class, 'Occasions']); 
});
//collection
Route::prefix('collection')->group(function(){
   Route::get('/{id?}', [UpdateProductController::class, 'Collection']); 
});

//product details
Route::get('/shop-old/{id1}/color-{id2}/{id3}', 'App\Http\Controllers\product\ProductController@details')->name('product.details');
Route::get('/shop/{id1}/color-{id2}/{id3}', 'App\Http\Controllers\product\UpdateProductController@details')->name('product.details.v');
Route::get('/shop/{id1}/{id2}/{id3}', 'App\Http\Controllers\product\UpdateProductController@detailsv2')->name('product.details.v');
Route::get('/GetTotalForAlert/{id1}', 'App\Http\Controllers\product\ProductController@ProductTotalQty')->name('ProductTotalQty');

//get find in store city list
Route::get('/find-get-city/{id}', 'App\Http\Controllers\product\FindInStore@getcity')->name('get.city');
Route::get('/store-list/{district}/{barcode}', 'App\Http\Controllers\product\FindInStore@ShowroomStock')->name('store.list');
Route::get('/showroom-wise-stock-by-designref/{id}/{id1}','App\Http\Controllers\product\FindInStore@ShowroomStockByDesignRef')->name('ShowroomStockByDesignRef');

//ajax call
Route::get('/get-citylist/{id}', 'App\Http\Controllers\AjaxCallController@getregionwisecity')->name('get.city');
Route::get('/ajaxcall-getQuantityByColor/{id1}/{id2}/{id3}', 'App\Http\Controllers\AjaxCallController@getQuantityByColor')->name('GetQty');
Route::get('/ajaxcall-getBarcode/{id1}/{id2}/{id3}', 'App\Http\Controllers\AjaxCallController@GetProductBarcodeBySize')->name('GetBarcode');
Route::prefix('ajax')->group(function(){
    Route::get('/ajax-product-details', 'App\Http\Controllers\AjaxCallController@ProductDetails')->name('ProductDetails');
    Route::get('/get-subcats/{id1}', 'App\Http\Controllers\AjaxCallController@GetProWiseSubcats')->name('get.subpro');
    Route::get('/get-procat/{id1}', 'App\Http\Controllers\AjaxCallController@getProcat')->name('get.procat');
    Route::get('/get-sizes/{id}', 'App\Http\Controllers\AjaxCallController@getSizes')->name('get.sizes');
    Route::get('/get-subpro/{id1}', 'App\Http\Controllers\AjaxCallController@GetProWiseSubpro')->name('get.subpro');
    Route::get('/ecourier/thana-list/{id1}', 'App\Http\Controllers\AjaxCallController@eCourierThana')->name('get.ecourier.thana');
    Route::get('/ecourier/postcode-list/{id1}/{id2}', 'App\Http\Controllers\AjaxCallController@eCourierPostcode')->name('get.ecourier.postcode');
    Route::get('/ecourier/area-list/{id1}', [AjaxCallController::class, 'eCourierArea'])->name('get.ecourier.area');
    Route::get('search-store-loc', [AjaxCallController::class, 'Searchstore'])->name('search.store');
    Route::get('user-order-details-by-tracknumber', [AjaxCallController::class, 'UserTrackOrderDetails'])->name('track.order.details');
    Route::get('monthly-sale/{id}', [AjaxCallController::class, 'getmonthlysalesdata'])->name('monthly.sale.report');
    //pathao
    Route::get('/pathao/city-list', 'App\Http\Controllers\AjaxCallController@PathaogetCity')->name('pathao.get.city');
    Route::get('/pathao/zone-list/{id}', 'App\Http\Controllers\AjaxCallController@PathaogetZone')->name('pathao.get.zone');
    Route::get('/pathao/area-list/{id}', 'App\Http\Controllers\AjaxCallController@PathaogetArea')->name('pathao.get.area');
});

//cart
Route::get('/shop-cart', 'App\Http\Controllers\cart\CartController@index');
Route::post('/cart/add-to-cart', 'App\Http\Controllers\cart\CartController@addProduct');
Route::post('/cart/popup-add-to-cart', 'App\Http\Controllers\cart\CartController@PopupaddProduct');
Route::get('/cart/reload', 'App\Http\Controllers\cart\CartController@CartReload');
Route::get('/cart/cart-destroy', 'App\Http\Controllers\cart\CartController@cartDestroy');
Route::get('/cart/delete-product/{id1}', 'App\Http\Controllers\cart\CartController@cartProductDelete');
Route::post('/cart/update-product', 'App\Http\Controllers\cart\CartController@cartProductUpdate');
Route::post('/cart/update-cart', 'App\Http\Controllers\cart\CartController@orderpreviewUpdate');
Route::post('/cart/add-to-cart', 'App\Http\Controllers\cart\CartController@addProduct');
Route::get('/cart/item-count', 'App\Http\Controllers\cart\CartController@CartItemCount');
Route::get('/cart-restore', 'App\Http\Controllers\cart\CartController@Cartrestore');

//cart previous data
Route::post('/cart/save-to-database', 'App\Http\Controllers\cart\CartController@SaveToTempcart');
Route::get('/cart/previous_data/{id}', 'App\Http\Controllers\cart\CartController@AutoAddtoCart');

//User Sign up
Route::post('/create/user', 'App\Http\Controllers\user\UserController@create');
Route::get('/user-details/{id}', 'App\Http\Controllers\user\UserController@UserDetails')->name('user-details');
Route::post('/user-details-save', 'App\Http\Controllers\user\UserController@SaveUserDetails')->name('UserDetailsSave');
Route::post('/update-shipping-info', 'App\Http\Controllers\user\UserController@UpdateShippingInfo')->name('ShippingInfo');
Route::post('/reset-password','App\Http\Controllers\user\UserController@ResetPassword')->name('ResetPassword');
Route::post('/update-password','App\Http\Controllers\user\UserController@UpdatePassword')->name('UpdatePassword');
Route::get('/forgot-password/{id}','App\Http\Controllers\user\UserController@ForgotPassword')->name('ForgetPassword');
Route::post('/create/form', 'App\Http\Controllers\user\UserController@showRegisterForm')->name('user.create.form');
Route::get('/create/form-fields', 'App\Http\Controllers\user\UserController@showRegisterForm');
Route::get('/create/form', 'App\Http\Controllers\user\UserController@showRegisterFormBadRequest');

//User Account
Route::prefix('user')->group(function(){
    Route::get('dashboard', [DashboardController::class, 'Dashboard'])->name('user.dashboard')->middleware('auth');
    Route::get('profile', [DashboardController::class, 'Profile'])->name('user.profile')->middleware('auth');
    Route::get('profile/edit', [DashboardController::class, 'ProfileEdit'])->name('user.profile.edit')->middleware('auth');
    Route::post('profile/update', [DashboardController::class, 'ProfileUpdate'])->name('user.profile.update')->middleware('auth');
    Route::get('address/edit', [DashboardController::class, 'AddressEdit'])->name('user.address.edit')->middleware('auth');
    Route::post('address/update', [DashboardController::class, 'AddressUpdate'])->name('user.address.update')->middleware('auth');
    Route::get('/change-password', [DashboardController::class, 'ChangePassword'])->name('user.change.password')->middleware('auth');
    Route::post('/change-password', [DashboardController::class, 'ChangePasswordUpdate'])->name('user.change.password.update')->middleware('auth');
    Route::get('orders', [DashboardController::class, 'MyOrders'])->name('user.orders')->middleware('auth');
    Route::get('order', [DashboardController::class, 'Orderdetailsv2'])->name('user.order.details')->middleware('auth');
    Route::get('order/details/{id}', [DashboardController::class, 'OrderDetails'])->name('user.orders.details')->middleware('auth');
    Route::get('order/exchange', [ExchangeController::class, 'index'])->name('user.order.exchange')->middleware('auth');
    Route::get('order/exchange/different', [ExchangeController::class, 'different'])->name('user.order.exchange.different')->middleware('auth');
    Route::get('order/exchange/search/{id}', [ExchangeController::class, 'ExchangeSearchProduct'])->name('user.order.exchange.different.search')->middleware('auth');
    Route::get('order/exchange/search-product-details/{id}/{id1}', [ExchangeController::class, 'SearchProductinfo'])->name('user.order.exchange.different.search.details')->middleware('auth');
    Route::post('order/exchange/save', [ExchangeController::class, 'ExchangeRequestSave'])->name('user.order.exchange.save')->middleware('auth');
    Route::get('track/order', [TrackOrderController::class, 'index'])->name('track.order');
});
Route::get('/track-order', 'App\Http\Controllers\user\TrackOrderController@UserTrackOrder');
Route::get('/user-orders', 'App\Http\Controllers\user\TrackOrderController@MyOrders');
Route::get('/order-preview/{id1}/{id2}', 'App\Http\Controllers\user\TrackOrderController@orderPreview')->name('Preview');
Route::get('/my-account', 'App\Http\Controllers\user\TrackOrderController@MyAccount')->name('my.account');
Route::get('/my-account-info', 'App\Http\Controllers\user\TrackOrderController@MyAccountInfo')->name('account.info');
Route::post('/account-edit', 'App\Http\Controllers\user\TrackOrderController@EditAccount')->name('account.edit');
Route::get('/account/change-email', 'App\Http\Controllers\user\TrackOrderController@AccountEmailChange');
Route::post('/account/update-email', 'App\Http\Controllers\user\TrackOrderController@UpdateAccountEmail');
Route::get('/account/change-password', 'App\Http\Controllers\user\TrackOrderController@AccountChangePassword');
Route::post('/account/update-password', 'App\Http\Controllers\user\TrackOrderController@UpdatePassword');
Route::get('/set-default-address/{id}', 'App\Http\Controllers\user\TrackOrderController@SetDefaultAddress')->name('address.create');
Route::get('/address-delete/{id}', 'App\Http\Controllers\user\TrackOrderController@AddressDelete')->name('address.delete');
Route::get('/eidt-additional-address/{id}', 'App\Http\Controllers\user\TrackOrderController@EditAdditionalAddress')->name('address.additional.edit');
Route::post('/additional-address-update', 'App\Http\Controllers\user\TrackOrderController@UpdateAdditionalAddress')->name('update.additional.address');

//checkout
Route::get('/checkout', 'App\Http\Controllers\CheckoutController@index')->name('checkout');
Route::post('/saveorder', 'App\Http\Controllers\CheckoutController@saveorder')->name('save.order');

//guest checkout
Route::get('/guest-checkout', 'App\Http\Controllers\GuestCheckout@index')->name('checkout.guest');
Route::post('/guest-saveorder', 'App\Http\Controllers\GuestCheckout@saveorder')->name('checkout.guest.save');

//digital payment
Route::get('/digital-payment','App\Http\Controllers\DigitalPayment@index')->name('digital.payment');
Route::get('/digital-payment','App\Http\Controllers\DigitalPayment@index')->name('digital.payment');
Route::get('/digital-payment-v2','App\Http\Controllers\DigitalPayment@indexv2')->name('digital.payment');
Route::post('/bKash-token-new', 'App\Http\Controllers\DigitalPayment@updateToken')->name('bkash.token');
Route::get('/bkash-create', 'App\Http\Controllers\DigitalPayment@BkashCreate')->name('bkash.create');
Route::get('/bKash-execute-new','App\Http\Controllers\DigitalPayment@ExecutePayment')->name('bkash.execute');
Route::get('/payment-success','App\Http\Controllers\DigitalPayment@bKashPaymentSuccess')->name('bkash.success');
Route::post('/SSL-create','App\Http\Controllers\DigitalPayment@SSLCreate')->name('ssl.create');
Route::post('/SSL-success','App\Http\Controllers\DigitalPayment@SSLSuccess')->name('ssl.success');
Route::post('/SSL-failure','App\Http\Controllers\DigitalPayment@SSLFailure')->name('ssl.failure');
Route::post('/SSL-cancel','App\Http\Controllers\DigitalPayment@SSLCancel')->name('ssl.cancel');

//admin
Route::prefix('admin')->group(function(){
   Route::get('/login','App\Http\Controllers\Auth\AdminLoginController@ShowLoginForm')->name('admin.login');
   Route::post('/login','App\Http\Controllers\Auth\AdminLoginController@Login')->name('admin.login.submit');
   Route::get('/', 'App\Http\Controllers\admin\DashboardController@index')->name('admin.dashboard');	
   Route::get('/v2', 'App\Http\Controllers\admin\DashboardController@indexv2')->name('admin.dashboard.v2');
   Route::get('/logout', 'App\Http\Controllers\Auth\AdminLoginController@logout')->name('admin.logout');
   Route::POST('/password/email','App\Http\Controllers\Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
   Route::GET('/password/reset','App\Http\Controllers\Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request'); 
   Route::POST('/password/reset', 'App\Http\Controllers\Auth\AdminResetPasswordController@reset');
   Route::GET('/password/reset/{token}','App\Http\Controllers\Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
});
//admin order
Route::prefix('admin/order')->group(function(){
    Route::get('/details','App\Http\Controllers\admin\ManageOrder@details')->name('order.details');
    Route::post('/update/address','App\Http\Controllers\admin\ManageOrder@addressupdate')->name('order.address.update');
    Route::post('/update-city','App\Http\Controllers\admin\ManageOrder@shipupdate')->name('order.ship.update');
    Route::post('/update-status','App\Http\Controllers\admin\ManageOrder@statusupdate')->name('order.status.update');
    Route::post('/exchange','App\Http\Controllers\admin\ManageOrder@exchange')->name('order.exchange.update');
    Route::post('/change-payment-mode','App\Http\Controllers\admin\ManageOrder@changepayment')->name('order.change.payment');
    Route::post('/extra-charge','App\Http\Controllers\admin\ManageOrder@extracharge')->name('order.change.payment');
    Route::get('/cancel_product/{id1}/{id2}/{id3}/{id4}/{id5}','App\Http\Controllers\admin\ManageOrder@CanceIndivisuallQty')->name('cancel.product');
    Route::get('/cancel-ecourier/{id}','App\Http\Controllers\admin\ManageOrder@eCourierCancelOrder')->name('cancel.ecourier');
    Route::get('/incomplete','App\Http\Controllers\admin\ManageOrder@incompleteorder')->name('order.incomplete');
    Route::get('/list','App\Http\Controllers\admin\ManageOrder@orderlist')->name('order.list');
    Route::get('/invoice','App\Http\Controllers\admin\Invoice@index')->name('order.invoice');
    Route::get('/invoice/pdf','App\Http\Controllers\admin\Invoice@generatePDF')->name('order.invoice.pdf');
    Route::get('/user/request/exchange/approved','App\Http\Controllers\admin\ManageOrder@UserExchangeApproved')->name('user.request.exchange.approved');
    Route::get('/user/request/exchange/cancel','App\Http\Controllers\admin\ManageOrder@UserExchangeCancel')->name('user.request.exchange.cancel');
    Route::get('/add/new/product/','App\Http\Controllers\admin\ManageOrder@AddNewProduct')->name('admin.add.new.product');
    Route::get('/exchange/search/{id}','App\Http\Controllers\admin\ManageOrder@ExchangeSearchProduct')->name('admin.order.exchange.different.search');
    Route::get('/exchange/search-product-details/{id}/{id2}','App\Http\Controllers\admin\ManageOrder@SearchProductinfo')->name('admin.order.exchange.different.search');
    Route::post('/add/new/product/save','App\Http\Controllers\admin\ManageOrder@AddNewProductSave')->name('admin.add.new.product.save');
    Route::get('/notify','App\Http\Controllers\admin\ManageOrder@newOrderNotify')->name('admin.new.order.notify');
    Route::get('/exchange/notify','App\Http\Controllers\admin\ManageOrder@exchangeOrderNotify')->name('admin.exchange.order.notify');
});
//admin product
Route::prefix('admin/product')->group(function(){
    Route::get('/add','App\Http\Controllers\admin\Manageproduct@create')->name('product.add');
    Route::post('/save','App\Http\Controllers\admin\Manageproduct@store')->name('product.store');
    Route::get('/list','App\Http\Controllers\admin\Manageproduct@list')->name('product.list'); 
    Route::get('/edit','App\Http\Controllers\admin\Manageproduct@edit')->name('product.edit'); 
    Route::post('/update','App\Http\Controllers\admin\Manageproduct@update')->name('product.update'); 
    Route::get('/active','App\Http\Controllers\admin\Manageproduct@active')->name('product.active'); 
    Route::get('/deactive','App\Http\Controllers\admin\Manageproduct@deactive')->name('product.deactive'); 
    Route::get('/delete','App\Http\Controllers\admin\Manageproduct@delete')->name('product.delete'); 
    Route::get('/trash','App\Http\Controllers\admin\Manageproduct@trashProducts')->name('product.trash'); 
    Route::get('/trash/reactive','App\Http\Controllers\admin\Manageproduct@trashProductreactive')->name('product.trash.reactive'); 
    Route::get('/trash/delete','App\Http\Controllers\admin\Manageproduct@trashProductdelete')->name('product.trash.delete'); 
});
//admin product pos
Route::prefix('admin/pos/product')->group(function(){
    Route::get('/addformpos','App\Http\Controllers\admin\pos\Manageproduct@AddPosItemList')->name('product.add.topos');
    Route::get('/add','App\Http\Controllers\admin\pos\Manageproduct@create')->name('product.add');
    Route::post('/save','App\Http\Controllers\admin\pos\Manageproduct@store')->name('product.store');
    Route::get('/list','App\Http\Controllers\admin\pos\Manageproduct@list')->name('product.list');
});
//product fabric
Route::prefix('admin/product/fabric')->group(function(){
    Route::get('/add','App\Http\Controllers\admin\product\FabricController@create')->name('fabric.add');
    Route::post('/save','App\Http\Controllers\admin\product\FabricController@store')->name('fabric.store');
    Route::get('/edit','App\Http\Controllers\admin\product\FabricController@edit')->name('fabric.edit'); 
    Route::post('/update','App\Http\Controllers\admin\product\FabricController@update')->name('fabric.update'); 
    Route::get('/active','App\Http\Controllers\admin\product\FabricController@active')->name('fabric.active'); 
    Route::get('/deactive','App\Http\Controllers\admin\product\FabricController@deactive')->name('fabric.deactive'); 
    Route::get('/delete','App\Http\Controllers\admin\product\FabricController@delete')->name('fabric.delete'); 
    Route::get('/view','App\Http\Controllers\admin\product\FabricController@view')->name('fabric.view'); 
});
//product stock
Route::prefix('admin/product/stock')->group(function(){
    Route::get('/view','App\Http\Controllers\admin\product\Managestock@view')->name('product.stcok.view');
    Route::post('/update','App\Http\Controllers\admin\product\Managestock@update')->name('product.stcok.update');
    Route::get('/delete/{id}','App\Http\Controllers\admin\product\Managestock@delete')->name('product.stcok.delete');
    Route::post('/add','App\Http\Controllers\admin\product\Managestock@add')->name('product.stcok.add');
});
//product album
Route::prefix('admin/product/album')->group(function(){
    Route::get('/view','App\Http\Controllers\admin\product\Managecolor@view')->name('product.album.view');
    Route::get('/edit','App\Http\Controllers\admin\product\Managecolor@Edit')->name('product.album.edit');
    Route::post('/update','App\Http\Controllers\admin\product\Managecolor@update')->name('product.album.update');
    Route::get('/delete','App\Http\Controllers\admin\product\Managecolor@delete')->name('product.album.delete');
    Route::get('/add','App\Http\Controllers\admin\product\Managecolor@add')->name('product.album.add');
    Route::post('/save','App\Http\Controllers\admin\product\Managecolor@save')->name('product.album.save');
});
//product image
Route::prefix('admin/product/image')->group(function(){
    Route::get('/view','App\Http\Controllers\admin\product\Manageimage@view')->name('product.image.view');
    Route::get('/edit','App\Http\Controllers\admin\product\Manageimage@Edit')->name('product.image.edit');
    Route::post('/update','App\Http\Controllers\admin\product\Manageimage@update')->name('product.image.update');
    Route::get('/delete/{id}','App\Http\Controllers\admin\product\Manageimage@delete')->name('product.image.delete');
    Route::get('/add','App\Http\Controllers\admin\product\Manageimage@add')->name('product.image.add');
    Route::post('/save','App\Http\Controllers\admin\product\Manageimage@save')->name('product.image.save');
    Route::get('/delete','App\Http\Controllers\admin\product\Manageimage@delete')->name('product.image.delete');
});
//campaign
Route::prefix('admin/campaign')->group(function(){
    Route::get('/add','App\Http\Controllers\admin\CampaignManage@create')->name('campaign.add');
    Route::post('/save','App\Http\Controllers\admin\CampaignManage@store')->name('campaign.store');
    Route::get('/list','App\Http\Controllers\admin\CampaignManage@list')->name('campaign.list');
    Route::get('/edit/{id}','App\Http\Controllers\admin\CampaignManage@edit')->name('campaign.edit');
    Route::post('/update','App\Http\Controllers\admin\CampaignManage@update')->name('campaign.update');
    Route::get('/delete/{id}','App\Http\Controllers\admin\CampaignManage@delete')->name('campaign.delete');
});
//category
Route::prefix('admin/category')->group(function(){
    Route::get('/maincat','App\Http\Controllers\admin\category\CategoryController@Maincat')->name('category.maincat');
    Route::post('/maincat/save','App\Http\Controllers\admin\category\CategoryController@MaincatSave')->name('category.maincat.save');
    Route::get('/maincat/edit/{id}','App\Http\Controllers\admin\category\CategoryController@MaincatEdit')->name('category.maincat.edit');
    Route::post('/maincat/update','App\Http\Controllers\admin\category\CategoryController@Maincatupdate')->name('category.maincat.update');
    Route::get('/maincat/active/{id}','App\Http\Controllers\admin\category\CategoryController@Maincatactive')->name('category.maincat.active');
    Route::get('/maincat/deactive/{id}','App\Http\Controllers\admin\category\CategoryController@Maincatdeactive')->name('category.maincat.deactive');
    Route::get('/procat','App\Http\Controllers\admin\category\CategoryController@Procat')->name('category.procat');
    Route::post('/procat/save','App\Http\Controllers\admin\category\CategoryController@ProcatSave')->name('category.procat.save');
    Route::get('/procat/edit/{id}','App\Http\Controllers\admin\category\CategoryController@Procatedit')->name('procat.maincat.edit');
    Route::post('/procat/update','App\Http\Controllers\admin\category\CategoryController@Procatupdate')->name('procat.maincat.update');
    Route::get('/procat/active/{id}','App\Http\Controllers\admin\category\CategoryController@Procatactive')->name('procat.maincat.active');
    Route::get('/procat/deactive/{id}','App\Http\Controllers\admin\category\CategoryController@Procatdeactive')->name('procat.maincat.deactive');
    Route::get('/subprocat','App\Http\Controllers\admin\category\CategoryController@Subprocat')->name('category.subprocat');
    Route::post('/subprocat/save','App\Http\Controllers\admin\category\CategoryController@SubprocatSave')->name('category.subprocat.save');
    Route::get('/subprocat/edit/{id}','App\Http\Controllers\admin\category\CategoryController@Subprocatedit')->name('subprocat.maincat.edit');
    Route::post('/subprocat/update','App\Http\Controllers\admin\category\CategoryController@Subprocatupdate')->name('subprocat.maincat.update');
    Route::get('/subprocat/active/{id}','App\Http\Controllers\admin\category\CategoryController@Subprocatactive')->name('subprocat.maincat.active');
    Route::get('/subprocat/deactive/{id}','App\Http\Controllers\admin\category\CategoryController@Subprocatdeactive')->name('subprocat.maincat.deactive');
});
//slider
Route::prefix('admin/slider')->group(function(){
    Route::get('/add','App\Http\Controllers\admin\ContentHandleController@slidercreate')->name('slider.add');
    Route::post('/save','App\Http\Controllers\admin\ContentHandleController@sliderstore')->name('slider.store');
    Route::get('/list','App\Http\Controllers\admin\ContentHandleController@sliderlist')->name('slider.list');
    Route::get('/edit/','App\Http\Controllers\admin\ContentHandleController@slideredit')->name('slider.edit');
    Route::post('/update','App\Http\Controllers\admin\ContentHandleController@sliderupdate')->name('slider.update');
    Route::get('/delete','App\Http\Controllers\admin\ContentHandleController@sliderdelete')->name('slider.delete');
    Route::get('/active','App\Http\Controllers\admin\ContentHandleController@slideractive')->name('slider.active');
    Route::get('/deactive','App\Http\Controllers\admin\ContentHandleController@sliderdeactive')->name('slider.deactive');
});
//banner
Route::prefix('admin/banner')->group(function(){
    Route::get('/add','App\Http\Controllers\admin\ContentHandleController@bannercreate')->name('banner.add');
    Route::post('/save','App\Http\Controllers\admin\ContentHandleController@bannerstore')->name('banner.store');
    Route::get('/list','App\Http\Controllers\admin\ContentHandleController@bannerlist')->name('banner.list');
    Route::get('/edit','App\Http\Controllers\admin\ContentHandleController@banneredit')->name('slider.edit');
    Route::post('/update','App\Http\Controllers\admin\ContentHandleController@bannerupdate')->name('slider.update');
    Route::get('/delete','App\Http\Controllers\admin\ContentHandleController@bannerdelete')->name('slider.delete');
    Route::get('/active/{id}','App\Http\Controllers\admin\ContentHandleController@banneractive')->name('banner.active');
    Route::get('/deactive/{id}','App\Http\Controllers\admin\ContentHandleController@bannerdeactive')->name('banner.deactive');
});
//popup
Route::prefix('admin/popup')->group(function(){
    Route::get('/add','App\Http\Controllers\admin\ContentHandleController@popupcreate')->name('popup.add');
    Route::post('/save','App\Http\Controllers\admin\ContentHandleController@popupstore')->name('popup.store');
    Route::get('/list','App\Http\Controllers\admin\ContentHandleController@popuplist')->name('popup.list');
    Route::get('/edit','App\Http\Controllers\admin\ContentHandleController@popupedit')->name('popup.edit');
    Route::post('/update','App\Http\Controllers\admin\ContentHandleController@popupupdate')->name('popup.update');
    Route::get('/delete','App\Http\Controllers\admin\ContentHandleController@popupdelete')->name('popup.delete');
    Route::get('/active','App\Http\Controllers\admin\ContentHandleController@popupactive')->name('popup.active');
    Route::get('/deactive','App\Http\Controllers\admin\ContentHandleController@popupdeactive')->name('popup.deactive');
});
//admin create
Route::prefix('admin/user')->group(function(){
    Route::get('/create','App\Http\Controllers\admin\user\UserController@create')->name('admin.user.create');
    Route::post('/save','App\Http\Controllers\admin\user\UserController@store')->name('admin.user.store');
    Route::get('/list','App\Http\Controllers\admin\user\UserController@list')->name('admin.user.list');
    Route::get('/edit','App\Http\Controllers\admin\user\UserController@edit')->name('admin.user.edit');
    Route::post('/update','App\Http\Controllers\admin\user\UserController@update')->name('admin.user.update');
    Route::get('/active','App\Http\Controllers\admin\user\UserController@active')->name('admin.user.active');
    Route::get('/deactive','App\Http\Controllers\admin\user\UserController@deactive')->name('admin.user.deactive');
    Route::get('/profile/manage','App\Http\Controllers\admin\user\UserController@Profile')->name('admin.user.profile');
    Route::post('/profile/update','App\Http\Controllers\admin\user\UserController@ProfileUpdate')->name('admin.user.profile.update');
    Route::post('/change/password','App\Http\Controllers\admin\user\UserController@changePassword')->name('admin.user.change.password');
});
//admin user role
Route::prefix('admin/user/role')->group(function(){
    Route::get('/create','App\Http\Controllers\admin\user\RoleController@create')->name('role.create');
    Route::post('/save','App\Http\Controllers\admin\user\RoleController@store')->name('role.store');
    Route::get('/list','App\Http\Controllers\admin\user\RoleController@list')->name('role.list');
    Route::get('/edit','App\Http\Controllers\admin\user\RoleController@edit')->name('role.edit');
    Route::post('/update','App\Http\Controllers\admin\user\RoleController@update')->name('role.update');
    Route::get('/active','App\Http\Controllers\admin\user\RoleController@active')->name('role.active');
    Route::get('/deactive','App\Http\Controllers\admin\user\RoleController@deactive')->name('role.deactive');
});
//admin customer manage
Route::prefix('admin/customer')->group(function(){
    Route::get('/create','App\Http\Controllers\admin\CustomerController@create')->name('customer.create');
    Route::post('/save','App\Http\Controllers\admin\CustomerController@store')->name('customer.store');
    Route::get('/list','App\Http\Controllers\admin\CustomerController@list')->name('customer.list');
    Route::get('/edit','App\Http\Controllers\admin\CustomerController@edit')->name('customer.edit');
    Route::post('/update','App\Http\Controllers\admin\CustomerController@update')->name('customer.update');
    Route::get('/active','App\Http\Controllers\admin\CustomerController@active')->name('customer.active');
    Route::get('/deactive','App\Http\Controllers\admin\CustomerController@deactive')->name('customer.deactive');
});
//admin report
Route::prefix('admin/report')->group(function(){
    Route::get('/sale','App\Http\Controllers\admin\ReportController@sale')->name('sale.report');
    Route::get('/sale/pdf','App\Http\Controllers\admin\ReportController@salePdf')->name('sale.report.pdf');
});
//admin send sms & email
Route::prefix('admin/send')->group(function(){
    Route::get('/sms','App\Http\Controllers\admin\CustomerController@sendSMSForm')->name('send.sms.form');
    Route::get('/mobile/search','App\Http\Controllers\admin\CustomerController@SearchMobile')->name('mobile.number.search');
    Route::post('/sms','App\Http\Controllers\admin\CustomerController@sendSMS')->name('send.sms');
    Route::get('/email','App\Http\Controllers\admin\CustomerController@sendEmailForm')->name('send.email.form');
    Route::get('/email/search','App\Http\Controllers\admin\CustomerController@SearchEmail')->name('email.search');
    Route::post('/email','App\Http\Controllers\admin\CustomerController@sendEmail')->name('send.email');
});
//customer phone request
Route::get('admin/customer/phone/request','App\Http\Controllers\admin\CustomerController@PhoneRequest')->name('phone.request');
//cashbook
Route::prefix('admin/accounts')->group(function(){
    Route::get('/','App\Http\Controllers\admin\AccountsController@index')->name('accounts.view');
    Route::get('/report','App\Http\Controllers\admin\AccountsController@reportpdf')->name('accounts.report');
    Route::get('/cash/transfer','App\Http\Controllers\admin\AccountsController@cashTransfer')->name('accounts.cash.transfer');
    Route::post('/cash/transfer','App\Http\Controllers\admin\AccountsController@cashTransfersave')->name('accounts.cash.transfer');
});
Auth::routes();


