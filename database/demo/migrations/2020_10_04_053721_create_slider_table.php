<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
			$table->string('slider_caption')->nullable();
			$table->string('slider_occasion')->nullable();
			$table->integer('slider_order');
			$table->string('device')->default('desktop');
			$table->string('slider_image');
			$table->string('url_link')->default('#');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->string('created_ip')->nullable();
			$table->string('updated_ip')->nullable();
			$table->tinyInteger('slider_status')->default(1);
			$table->tinyInteger('deleted_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
