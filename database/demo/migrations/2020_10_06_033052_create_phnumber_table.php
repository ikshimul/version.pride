<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhnumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phnumbers', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('phnumber_name')->nullable();
			$table->string('phnumber_number')->nullable();
			$table->text('comment_box')->nullable();
			$table->tinyInteger('phone_status')->default(1);
			$table->tinyInteger('view')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phnumbers');
    }
}
