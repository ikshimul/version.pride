<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('procat_id')->unsigned();
			$table->integer('subprocat_id')->unsigned();
			$table->string('product_styleref');
			$table->string('product_name');
			$table->Integer('product_price');
			$table->Integer('product_pricediscounted')->default(0);
			$table->Integer('discount_product_price')->default(0);
			$table->string('product_pricefilter')->nullable();
			$table->string('fabric')->nullable();
			$table->string('product_description');
			$table->string('product_img_thm')->nullable();
			$table->string('product_care')->nullable();
			$table->tinyInteger('product_active_deactive')->default(0);
			$table->tinyInteger('spcollection')->default(0);
			$table->tinyInteger('isSpecial')->default(0)->comment('2 for Eid Collection,3 for puja 18,4 for falgun 19,5 for Amar Ekushay 2019,6 for Independence day,7 for Boishak,8 summer,9 Capsule,10 popart,11 zodiac');
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->tinyInteger('trash')->default(0);
			$table->Integer('sale')->default(0);
			$table->Integer('view')->default(0);
            $table->timestamps();
			$table->dateTime('deleted_at')->nullable();
			
			$table->foreign('procat_id')->references('id')->on('procats');
			$table->foreign('subprocat_id')->references('id')->on('subprocats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
