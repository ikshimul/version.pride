<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productsizes', function (Blueprint $table) {
            $table->increments('id');
			$table->bigInteger('product_id')->unsigned();
			$table->string('barcode')->nullable();
			$table->string('productsize_size');
			$table->string('color_name');
			$table->integer('SizeWiseQty');
			$table->tinyInteger('status')->default(0);
            $table->timestamps();
			
			$table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productsizes');
    }
}
