<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBkashpaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bkashpayments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('conforder_id')->unsigned();
            $table->string('trxID');
            $table->string('paymentID');
            $table->float('amount', 10, 2);
            $table->timestamps();
            
            $table->foreign('conforder_id')->references('id')->on('conforders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bkashpayments');
    }
}
