<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procats', function (Blueprint $table) {
            $table->increments('id');
			$table->string('procat_name');
			$table->string('procat_banner')->nullable();
			$table->tinyInteger('status')->default(0);
			$table->tinyInteger('deleted_status')->default(0);
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->string('created_ip')->nullable();
			$table->string('updated_ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procats');
    }
}
