<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uservouchers', function (Blueprint $table) {
            $table->increments('id');
			$table->integer( 'user_id' )->unsigned( );
            $table->bigInteger( 'voucher_id' )->unsigned( );
            $table->timestamps();
			
			$table->unique( [ 'user_id', 'voucher_id' ] );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_voucher');
    }
}
