<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userdetails', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('phone');
			$table->string('address');
			$table->string('country')->nullable();
			$table->integer('region')->nullable();
			$table->integer('city')->nullable();
			$table->string('zip')->nullable();
			$table->ipAddress('user_ip')->nullable();
			$table->string('admin_comment')->nullable();
            $table->timestamps();
			
			$table->foreign('user_id')->references('id')->on('users');
			//$table->foreign('region')->references('id')->on('regions');
			//$table->foreign('city')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userdetails');
    }
}
