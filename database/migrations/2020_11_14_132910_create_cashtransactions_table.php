<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashtransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashtransactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('conforder_id')->unsigned();
            $table->string('transaction_type')->nullable();
            $table->string('transfer_mode')->nullable();
            $table->string('website_code')->nullable();
            $table->bigInteger('amount');
            $table->string('transfer_to')->nullable();
            $table->text('description')->nullable();
            $table->text('remark')->nullable();
            $table->integer('issued_by')->nullable();
            $table->timestamps();
            
            $table->foreign('conforder_id')->references('id')->on('conforders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashtransactions');
    }
}
