<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubprocatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subprocats', function (Blueprint $table) {
            $table->increments('id');
			$table->Integer('procat_id')->unsigned();
			$table->string('subprocat_name');
			$table->integer('subprocat_order')->nullable();
			$table->string('subprocat_img')->nullable();
			$table->string('subprocat_banner')->nullable();
			$table->tinyInteger('status')->default(1);
			$table->tinyInteger('deleted_status')->default(0);
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->string('created_ip')->nullable();
			$table->string('updated_ip')->nullable();
            $table->timestamps();
			
			$table->foreign('procat_id')->references('id')->on('procats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subprocats');
    }
}
