<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positems', function (Blueprint $table) {
            $table->id();
            $table->string('Barcode');
            $table->string('ItemMasterID');
            $table->string('DesignRef');
            $table->string('Brand');
             $table->string('ItemDescription')->nullable();
            $table->string('Department')->nullable();
            $table->string('Category');
            $table->string('subcategory');
            $table->string('itemGroup');
            $table->string('color');
            $table->string('size');
            $table->float('SellRate', 10, 2);
            $table->integer('CurrentStock');
            $table->tinyInteger('add_status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('positems');
    }
}
