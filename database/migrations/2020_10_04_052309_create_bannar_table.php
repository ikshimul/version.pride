<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
			$table->string('banner_title')->nullable();
			$table->string('banner_image');
			$table->tinyInteger('banner_pos');
			$table->integer('banner_order');
			$table->string('banner_link')->nullable();
			$table->string('banner_target')->nullable();
			$table->tinyInteger('banner_status')->default(1);
			$table->tinyInteger('banner_deleted_status')->default(0);
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('created_ip')->nullable();
			$table->integer('updated_ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
