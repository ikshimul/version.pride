<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdershippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordershippings', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('conforder_id')->unsigned();
			$table->integer('user_id')->unsigned();	 
			$table->bigInteger('shoppingcart_id')->unsigned();
			$table->string('email')->nullable();
			$table->string('Shipping_txtaddressname')->nullable();
			$table->string('Shipping_ddlcountry')->default('Bangladesh');
			$table->string('Shipping_txtfirstname')->nullable();
			$table->string('Shipping_txtlastname')->nullable();
			$table->string('Shipping_txtcity')->nullable();
			$table->string('Shipping_txtzipcode')->nullable();
			$table->string('Shipping_txtphone')->nullable();
			$table->string('billing_txtaddressname')->nullable();
			$table->string('billing_ddlcountry')->nullable();
			$table->string('billing_txtfirstname')->nullable();
			$table->string('billing_txtlastname')->nullable();
			$table->string('billing_txtcity')->nullable();
			$table->string('billing_txtzipcode')->nullable();
			$table->string('billing_txtphone')->nullable();
            $table->timestamps();
			
			$table->foreign('conforder_id')->references('id')->on('conforders');
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('shoppingcart_id')->references('id')->on('shoppingcarts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordershippings');
    }
}
