<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoppinproducts', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('shoppingcart_id')->unsigned();
			$table->bigInteger('product_id')->unsigned();
			$table->string('product_barcode');
			$table->string('prosize_name');
			$table->string('productalbum_name');
			$table->integer('product_price');
			$table->integer('shoppinproduct_quantity');
			$table->string('cart_image');
            $table->timestamps();
			
			$table->foreign('shoppingcart_id')->references('id')->on('shoppingcarts');
			$table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoppinproducts');
    }
}
