<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->text('image')->nullable();
            $table->text('address')->nullable();
            $table->string('mobile')->nullable();
            $table->string('admin_type')->nullable();
            $table->dateTime('admin_lastlogin')->nullable();
            $table->string('admin_ip')->nullable();
            $table->tinyInteger('admin_status')->default('1');
            $table->timestamps();
            $table->integer('updated_by')->nullable();

			$table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
