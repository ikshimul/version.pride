<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashexpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashexpenses', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('conforder_id')->unsigned();
            $table->integer('expense_type')->default('1');
            $table->mediumInteger('amount');
            $table->text('remark')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
            
            $table->foreign('conforder_id')->references('id')->on('conforders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashexpenses');
    }
}
