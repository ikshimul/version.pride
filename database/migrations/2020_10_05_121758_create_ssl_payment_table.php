<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSslPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sslpayments', function (Blueprint $table) {
            $table->increments('id');
			$table->bigInteger('conforder_id')->unsigned();
			$table->string('bank_tran_id');
			$table->string('refund_ref_id')->nullable();
			$table->float('total_amount', 10, 2);
			$table->float('store_amount', 10, 2);
			$table->string('card_type');
			$table->dateTime('tran_date')->nullable();
			$table->string('val_id');
			$table->string('card_issuer');
			$table->float('discount_amount', 10, 2)->nullable();
			$table->float('discount_percentage', 10, 2)->nullable();
			$table->string('discount_remarks')->nullable();
			$table->tinyInteger('status')->default(0);
            $table->timestamps();
			
			$table->foreign('conforder_id')->references('id')->on('conforders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ssl_payments');
    }
}
